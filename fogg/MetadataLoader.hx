import flash.accessibility.Accessibility;
import flash.events.Event;
// MetadataLoader Main class

class MetadataLoader {
	var player	  : Player;
	var ui		  : UI;
	var counter	  : Int;
	var delay     : Int;
	var fileLoader: flash.net.URLLoader;
	var proxy	  : String;
	var metadataSource : String;
	
    public function new(player:Player,ui:UI,interval:Int,metadataSource:String,proxy:String=null){
		this.player=player;
		this.ui = ui;
		this.metadataSource = metadataSource;
		if(proxy!=null){
			this.proxy=proxy;
		}
		delay=FFMp3.FPS*interval;
		counter=delay-Math.round(FFMp3.FPS/2);
		ui.addEventListener(flash.events.Event.ENTER_FRAME,loop);
	}
	
	public function loop(_){
		counter++;
		if(counter>=delay && player.isPlaying()){
			counter = 0;
			var url:String = player.getCurrentUrl();
			if(this.metadataSource == "icecast"){
				url+= ".xspf";
			} else {
				url = StringTools.replace(url, ';', '');
			}
			if(proxy!='' && proxy!=null){
				url='proxy.php?url='+StringTools.replace(url,':','%3A');
			}
			loadMetadata(url);
			//loadXSPF('test.mp3.xspf');
		}
	}

	function loadMetadata(url:String){
		var urlRequest : flash.net.URLRequest=new flash.net.URLRequest();
		fileLoader = new flash.net.URLLoader();
		urlRequest.url = url;
		if(this.metadataSource == "icecast"){
			fileLoader.addEventListener( flash.events.Event.COMPLETE, loadIcecastEvent );
		} else {
			fileLoader.addEventListener( flash.events.Event.COMPLETE, loadShoutcastEvent );
		}
		fileLoader.load(urlRequest);
	}
	
	function loadShoutcastEvent(e: Event) {
		var loader=cast(e.target,flash.net.URLLoader);
		var data = loader.data;
		var variable:Array<String> = data.split("<font class=default>Current Song: </font></td><td><font class=default><b>");
		variable = variable[1].split("</b>");
		ui.setMetadataFromString(variable[0]);
	}
	
	function loadIcecastEvent(e: Event){
		var loader=cast(e.target,flash.net.URLLoader);
		var xspf=Xml.parse(loader.data);
		for( base in xspf.elements() ) {
			if(base.nodeName.toLowerCase()!='playlist'){ // CHECK THIS IS A VALID XML
				return;
			}
			for( elem in base.elements() ) {
				if(elem.nodeName.toLowerCase()=='tracklist'){
					for(track in elem.elements()){
						if(track.nodeName.toLowerCase()=='track'){
							parseTrack(track);
							return;
						}
					}
				}				
			}
		}
	}
	
	function parseTrack(track:Xml){
		for(elem in track.elements()){
			if(elem.nodeName.toLowerCase()=='title'){
				ui.setMetadataFromString(elem.firstChild().toString());
				return;
			}
		}
	}
	
}
