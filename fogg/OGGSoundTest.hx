/**
 * ...
 * @author Sang Tian
 */

package ;
import flash.events.Event;
import flash.Lib;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLLoaderDataFormat;
import flash.utils.ByteArray;

class OGGSoundTest
{
	public static function main()
	{
		new OGGSoundTest();
	}
	
	public function new() 
	{
		//var urlLoader:URLLoader = new URLLoader();
		//urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
		//urlLoader.addEventListener(Event.COMPLETE, handleLoadComple);
		//urlLoader.load(new URLRequest("Song1.ogg"));
		var ogg:OGGSound = new OGGSound();
		ogg.load(new URLRequest("Song8.ogg"));
		ogg.setVolume(1);
		ogg.play();
	}
	
	function handleLoadComple(evt:Event){
		var ogg:OGGSound = new OGGSound();
		ogg.loadBytes(cast(evt.target.data, ByteArray));
		ogg.setVolume(1);
		ogg.play();
	}
}