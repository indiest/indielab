package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Vector3D;
	
	/**
	 * ...
	 * @author 
	 */
	public class PerspectiveProjectionTest extends Sprite 
	{
		private var _container:Sprite = new Sprite();
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			this.transform.perspectiveProjection.fieldOfView = 100;
			var fl:Number = this.transform.perspectiveProjection.focalLength = 1;
			var w:Number = this.transform.perspectiveProjection.projectionCenter.x;
			var h:Number = this.transform.perspectiveProjection.projectionCenter.y;
			trace(this.transform.perspectiveProjection.projectionCenter);
			graphics.lineStyle(2, 0x00ff00);
			for (var z:Number = 1; z < 10; z += 1)
			{
				var s1:Sprite = new Sprite();
				s1.graphics.beginFill(0xff0000);
				s1.graphics.drawCircle(0, 0, 4);
				s1.graphics.endFill();
				s1.x = -10;
				s1.y = h;
				s1.z = z;
				//trace(s1.transform.matrix3D.rawData);
				var sx:Number = s1.x / s1.z / fl;
				trace("x:", sx);
				_container.addChild(s1);
				
				var s2:Sprite = new Sprite();
				s2.graphics.beginFill(0x00ff00);
				s2.graphics.drawCircle(0, 0, 4);
				s2.graphics.endFill();
				s2.x = w;
				s2.y = 10;
				s2.z = z;
				var sy:Number = s2.y / s2.z / fl;
				trace("y:", sy);
				_container.addChild(s2);
				
				graphics.lineTo(w, sy);
			}
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			addChild(_container);
		}
		
		private var _mouseX:Number;
		private var _mouseY:Number;
		private function handleMouseMove(e:MouseEvent):void 
		{
			if (!e.buttonDown)
				return;
			if (!_mouseX || !_mouseY)
			{
				_mouseX = e.stageX;
				_mouseY = e.stageY;
				return;
			}
			//_container.rotationY += (e.stageX - _mouseX);
			//_container.rotationX += (e.stageY - _mouseY);
			for (var i:Number = 0; i < _container.numChildren; i++)
			{
				var s:DisplayObject = _container.getChildAt(0);
				s.transform.matrix3D.appendRotation(e.stageX - _mouseX, Vector3D.Y_AXIS);
			}
			_mouseX = e.stageX;
			_mouseY = e.stageY;
		}
		
	}
	
}