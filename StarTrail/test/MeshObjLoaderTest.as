package  
{
	import flash.display.Sprite;
	import me.sangtian.geom.Mesh;
	import me.sangtian.geom.MeshObjLoader;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class MeshObjLoaderTest extends Sprite 
	{
		[Embed(source="../assets/Torus2.obj", mimeType="application/octet-stream")]
		private static const MESH:Class;
		
		public function MeshObjLoaderTest() 
		{
			var mesh:Mesh = MeshObjLoader.load(new MESH());
			trace(mesh.vertices.length);
			trace(mesh.faces.length);
		}
		
	}

}