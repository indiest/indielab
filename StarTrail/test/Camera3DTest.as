package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.Stage3D;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.text.TextField;
	import me.sangtian.common.Input;
	import me.sangtian.common.Time;
	import me.sangtian.geom.Mesh;
	import me.sangtian.geom.MeshFace;
	import me.sangtian.geom.MeshObjLoader;
	import me.sangtian.graphics.Camera3D;
	import net.hires.debug.Stats;
	
	/**
	 * ...
	 * @author 
	 */
	public class Camera3DTest extends Sprite 
	{
		[Embed(source="../assets/hxlogo.png")]
		private static const TEXTURE_BOX:Class;
		
		[Embed(source="../assets/Sphere.obj", mimeType="application/octet-stream")]
		private static const MESH:Class;
		
		private var _cam:Camera3D;
		private var _infoText:TextField;
		
		public function Camera3DTest() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			var canvas:Bitmap = new Bitmap(new BitmapData(800, 600));
			addChild(canvas);
			_cam = new Camera3D(canvas);
			//_cam.transform.up.setTo(0, 1, 0);
			//_cam.transform.translate(0, 0, 2);
			//_cam.transform.lookAt(new Vector3D(0, 0, 0));
			
			/* Two Pyramids
			_cam.setVertices(0, 1, 2, -1, -1, 1, 1, -1, 1, 0, 0, 3,
							4, 1, 2, 3, -1, 1, 5, -1, 1, 4, 0, 3);
			_cam.setIndices(0, 1, 2, 0, 1, 3, 0, 2, 3, 1, 2, 3,
							4, 5, 6, 4, 5, 7, 4, 6, 7, 5, 6, 7);
			*/
							
			/* A cube
			*/
			_cam.setVertices(
				0,0,0,
				1,0,0,
				1,0,1,
				0,0,0,
				1,0,1,
				0,0,1,
				1,0,0,
				1,1,0,
				1,1,1,
				1,0,0,
				1,1,1,
				1,0,1,
				0,0,1,
				1,0,1,
				1,1,1,
				0,0,1,
				1,1,1,
				0,1,1,
				0,0,0,
				0,1,1,
				0,1,0,
				0,0,0,
				0,0,1,
				0,1,1,
				0,1,0,
				1,1,1,
				1,1,0,
				0,1,0,
				0,1,1,
				1,1,1,
				0,0,0,
				1,1,0,
				1,0,0,
				0,0,0,
				0,1,0,
				1,1,0
			);
			_cam.setUV(
				0,0,
				1,0,
				1,1,
				0,0,
				1,1,
				0,1,
				1,0,
				0,0,
				0,1,
				1,0,
				0,1,
				1,1,
				0,0,
				1,0,
				1,1,
				0,0,
				1,1,
				0,1,
				0,0,
				1,1,
				1,0,
				0,0,
				0,1,
				1,1,
				1,0,
				0,1,
				0,0,
				1,0,
				1,1,
				0,1,
				0,0,
				1,1,
				1,0,
				0,0,
				0,1,
				1,1
			);
			_cam.setIndices(
				0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35
			);
			
			var bitmap:Bitmap = new TEXTURE_BOX() as Bitmap;
			_cam.setTexture(bitmap.bitmapData);
			
			/* A sphere mesh
			var mesh:Mesh = MeshObjLoader.load(new MESH());
			_cam.addMesh(mesh);
			*/
			
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
			
			Input.initialize(stage);
			
			addChild(new Stats());
			_infoText = new TextField();
			_infoText.textColor = 0xffffff;
			_infoText.wordWrap = true;
			_infoText.multiline = true;
			_infoText.width = 200;
			_infoText.x = stage.stageWidth - 200;
			addChild(_infoText);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			Time.tick();
			Input.update();
			
			//_cam.transform.rotateAround(Input.mouseDelta.x, _cam.transform.up);
			//_cam.transform.rotateAround(Input.mouseDelta.y, _cam.transform.right);
			
			_cam.transform.rotate(new Vector3D(-Input.mouseDelta.y / 10, -Input.mouseDelta.x / 10));
			//Input.mouseDelta.x = 100;
			//_cam.transform.lookAt(_cam.transform.forward.add(new Vector3D(Input.mouseDelta.x / 100, Input.mouseDelta.y / 100)));
			
			_cam.transform.translateByVector(
				_cam.transform.right,//Vector3D.X_AXIS,
				Input.getAxis(Input.AXIS_HORIZONTAL) * 0.1);
			_cam.transform.translateByVector(
				_cam.transform.forward,//Vector3D.Z_AXIS,
				Input.getAxis(Input.AXIS_VERTICAL) * 0.1);
			
			//_cam.transform.moveAxis(Input.getAxis(Input.AXIS_HORIZONTAL) * 0.1, 0, Input.getAxis(Input.AXIS_VERTICAL) * 0.1);
			
			//_cam.zoomX -= Input.getAxis(Input.AXIS_VERTICAL) * 0.1;
			//_cam.zoomY -= Input.getAxis(Input.AXIS_VERTICAL) * 0.1;
			
			//trace(_cam.transform.forward);
			//trace(_cam.transform);
			_infoText.text = _cam.transform.toString();
			_cam.update();
		}
		
	}

}