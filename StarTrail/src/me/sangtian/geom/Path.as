package me.sangtian.geom 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author 
	 */
	public class Path 
	{
		private var _nodes:Vector.<Point>;
		private var _color:uint;
		
		public function get nodes():Vector.<Point> 
		{
			return _nodes;
		}
		
		public function get length():int 
		{
			return _nodes.length;
		}
		
		public function get color():uint 
		{
			return _color;
		}
		
		public function Path(color:uint = 0xffffff, ...points)
		{
			_color = color;
			_nodes = new Vector.<Point>();
			for each(var point:Point in points)
			{
				_nodes.push(point);
			}
		}
		
		public function toString():String
		{
			return _nodes.toString();
		}
	}

}