package me.sangtian.geom 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class MeshFace 
	{
		public var vertexIndices:Vector.<int>;
		public var textureIndices:Vector.<int>;
		public var normalIndices:Vector.<int>;
	}

}