package me.sangtian.geom 
{
	import flash.geom.Point;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Mesh 
	{
		public var vertices:Vector.<Vector3D>;
		public var faces:Vector.<MeshFace>;
		
		public var normals:Vector.<Vector3D>;
		public var textureCoords:Vector.<Point>;
	}

}