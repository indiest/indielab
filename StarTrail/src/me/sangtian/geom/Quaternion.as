package me.sangtian.geom 
{
	import flash.geom.Vector3D;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Quaternion 
	{
		public var x:Number;
		public var y:Number;
		public var z:Number;
		public var w:Number;
		
		public function Quaternion(x:Number = 0, y:Number = 0, z:Number = 0, w:Number = 0) 
		{
			setTo(x, y, z, w);
		}
		
		public function setTo(x:Number = 0, y:Number = 0, z:Number = 0, w:Number = 0) 
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}
		
		public function toEulerAngles():Vector3D
		{
			// TODO
			return null;
		}
		
		private function get magnitude():Number
		{
			return Math.sqrt(x * x + y * y + z * z + w * w);
		}
		
		public static const identity:Quaternion = new Quaternion(0, 0, 0, 1);
		
		public static function dot(a:Quaternion, b:Quaternion):Number
		{
			return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
		}
		
		public static function multiply(a:Quaternion, b:Quaternion):Quaternion
		{
			// Hamilton product
			return new Quaternion(
				a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y,
				a.w * b.y + a.y * b.w + a.z * b.x - a.x * b.z,
				a.w * b.z + a.z * b.w + a.x * b.y - a.y * b.x,
				a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z
			);
		}
		
		public static function inverse(q:Quaternion):Quaternion
		{
			var mag:Number = 1 / q.magnitude;
			return new Quaternion( -q.x * mag, -q.y * mag, -q.z * mag, q.w * mag);
		}
		
		public static function angleBetween(a:Quaternion, b:Quaternion):Number
		{
			var d:Number = Math.abs(dot(a, b));
			return Math.acos(Math.min(d, 1)) * MathUtil.RADIAN_ANGLE * 2;
		}
		
		public static function angleAxis(angle:Number, axis:Vector3D):Quaternion
		{
			// TODO
			return null;
		}
	}

}