package me.sangtian.geom 
{
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public interface ITransform3D 
	{
		function get position():Vector3D;
		function get right():Vector3D;
		function get up():Vector3D;
		function get forward():Vector3D;
		
		function translate(x:Number, y:Number, z:Number):void;
		function translateByVector(v:Vector3D, multiplier:Number = 1):void;
		function moveAxis(x:Number, y:Number, z:Number):void;
		
		function rotate(eulerAngles:Vector3D):void;
		function rotateAround(angles:Number, axis:Vector3D, pivotPoint:Vector3D = null):void;
		function lookAt(p:Vector3D):void;
		
		function transformVector(v:Vector3D, positionMultiplier:Number = 1):Vector3D;
		function toMatrix():Matrix3D;
		function toString():String;
	}
	
}