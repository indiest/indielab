package me.sangtian.geom 
{
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author 
	 */
	public class Polar3D 
	{
		public var radius:Number;
		public var heading:Number;
		public var pitch:Number;
		
		public function Polar3D(radius:Number = 0, heading:Number = 0, pitch:Number = 0) 
		{
			this.radius = radius;
			this.heading = heading;
			this.pitch = pitch;
		}
		
		public function toVector3D(v:Vector3D = null):Vector3D
		{
			v = v || new Vector3D();
			v.x = radius * Math.cos(pitch) * Math.sin(heading);
			v.y = -radius * Math.sin(pitch);
			v.z = radius * Math.cos(pitch) * Math.cos(heading);
			return v;
		}
		
		public function setFromVector3D(v:Vector3D):Polar3D
		{
			radius = v.length;
			if (radius > 0)
			{
				pitch = Math.asin( -v.y / radius);
				if (Math.abs(pitch) >= Math.PI / 2)
					heading = 0;
				else
					heading = Math.atan2(v.z, v.x);
			}
			else
				heading = pitch = 0;
			return this;
		}
	}

}