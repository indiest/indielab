package me.sangtian.geom 
{
	import flash.geom.Matrix3D;
	import flash.geom.Orientation3D;
	import flash.geom.Vector3D;
	
	/**
	 * ...
	 * @author 
	 */
	public class UpTargetTransform3D implements ITransform3D
	{
		private var _position:Vector3D;
		private var _up:Vector3D;
		private var _target:Vector3D;
		private var _scale:Vector3D;
		
		public function get position():Vector3D
		{
			return _position;
		}
		
		public function set position(value:Vector3D):void
		{
			_position = value;
		}
		
		public function get rotation():Vector3D
		{
			// TODO
			return up;
		}
		
		public function get eulerAngles():Vector3D
		{
			// TODO
			return up;
		}
		
		public function get scale():Vector3D
		{
			return _scale;
		}
		
		public function set scale(value:Vector3D):void
		{
			_scale = value;
		}
		
		public function get right():Vector3D
		{
			return _up.crossProduct(forward);
		}
		
		public function get up():Vector3D
		{
			return _up;
		}
		
		public function get forward():Vector3D
		{
			return _target.subtract(_position);
		}
		
		public function UpTargetTransform3D(position:Vector3D = null, up:Vector3D = null, target:Vector3D = null, scale:Vector3D = null) 
		{
			_position = position || new Vector3D();
			_up = up || new Vector3D();
			_target = target || new Vector3D();
			_scale = scale || new Vector3D();
		}
		
		public function transformVector(v:Vector3D, positionMultiplier:Number = 1):Vector3D
		{
			var ax:Vector3D = right.clone();
			ax.normalize();
			var ay:Vector3D = up.clone();
			ay.normalize();
			var az:Vector3D = forward.clone();
			az.normalize();
			var x:Number = ax.x * v.x + ax.y * v.x + ax.z * v.x + position.x * positionMultiplier;
			var y:Number = ay.x * v.y + ay.y * v.y + ay.z * v.y + position.y * positionMultiplier;
			var z:Number = az.x * v.z + az.y * v.z + az.z * v.z + position.z * positionMultiplier;
			return new Vector3D(x, y, z);
		}
		
		public function deltaTransformVector(v:Vector3D):Vector3D
		{
			// TODO
			throw new Error();
		}
		
		public function rotate(eulerAngles:Vector3D):void
		{
			// TODO
			throw new Error();
		}
		
		public function rotateAround(angles:Number, axis:Vector3D, pivotPoint:Vector3D = null):void
		{
			// TODO
			throw new Error();
		}
		
		public function translate(x:Number, y:Number, z:Number):void
		{
			_position.x += x;
			_position.y += y;
			_position.z += z;
		}
		
		public function moveAxis(dx:Number, dy:Number, dz:Number):void
		{
			translateByVector(transformVector(new Vector3D(dx, dy, dz), 1));
		}
		
		public function translateByVector(v:Vector3D, multiplier:Number = 1):void
		{
			translate(v.x * multiplier, v.y * multiplier, v.z * multiplier);
		}
		
		public function lookAt(target:Vector3D):void
		{
			_target = target;
		}
		
		public function toString():String
		{
			return "Position:" + position + " Rotation:" + rotation + " Scale:" + scale;
		}
	}

}