package me.sangtian.geom 
{
	import flash.geom.Matrix3D;
	import flash.geom.Orientation3D;
	import flash.geom.Vector3D;
	
	/**
	 * ...
	 * @author 
	 */
	public class EulerTransform3D implements ITransform3D
	{
		private var _position:Vector3D;
		// euler angles
		private var _rotation:Vector3D;
		private var _scale:Vector3D;
		
		private function get heading():Number
		{
			return _rotation.y;
		}
		
		private function get pitch():Number
		{
			return _rotation.x;
		}
		
		private function get bank():Number
		{
			return _rotation.z;
		}
		
		public function get position():Vector3D
		{
			return _position;
		}
		
		public function set position(value:Vector3D):void
		{
			_position = value;
		}
		
		public function get rotation():Vector3D
		{
			return _rotation;
		}
		
		public function get scale():Vector3D
		{
			return _scale;
		}
		
		public function set scale(value:Vector3D):void
		{
			_scale = value;
		}
		
		public function get right():Vector3D
		{
			return up.crossProduct(forward);
		}
		
		public function get up():Vector3D
		{
			var sinh:Number = Math.sin(heading + Math.PI * 0.5);
			var cosh:Number = Math.cos(heading + Math.PI * 0.5);
			var sinp:Number = Math.sin(pitch + Math.PI * 0.5);
			var cosp:Number = Math.cos(pitch + Math.PI * 0.5);
			return new Vector3D(cosp * sinh, -sinp, cosp * cosh);
		}
		
		public function get forward():Vector3D
		{
			var sinh:Number = Math.sin(heading);
			var cosh:Number = Math.cos(heading);
			var sinp:Number = Math.sin(pitch);
			var cosp:Number = Math.cos(pitch);
			return new Vector3D(cosp * sinh, -sinp, cosp * cosh);
		}
		
		public function EulerTransform3D(position:Vector3D = null, rotation:Vector3D = null, scale:Vector3D = null) 
		{
			_position = position || new Vector3D();
			_rotation = rotation || new Vector3D();
			_scale = scale || new Vector3D();
		}
		
		public function transformVector(v:Vector3D, positionMultiplier:Number = 1):Vector3D
		{
			var m:Matrix3D = toMatrix();
			var p:Vector3D = m.position;
			p.scaleBy(positionMultiplier);
			m.position = p;
			return m.transformVector(v);
		}
		
		public function toMatrix():Matrix3D
		{
			var ch:Number = Math.cos(heading);
			var sh:Number = Math.sin(heading);
			var cp:Number = Math.cos(pitch);
			var sp:Number = Math.sin(pitch);
			var cb:Number = Math.cos(bank);
			var sb:Number = Math.sin(bank);
			
			var m11:Number = ch * cb + sh * sp * sb;
			var m12:Number = sb * cp;
			var m13:Number = -sh * cb + ch * sp * sb;
			var m21:Number = -ch * sb + sh * sp * cb;
			var m22:Number = cb * cp;
			var m23:Number = sb * sh + ch * sp * cb;
			var m31:Number = sh * cp;
			var m32:Number = -sp;
			var m33:Number = ch * cp;
			
			// TODO: implement scale factor
			
			return new Matrix3D(new <Number>[
				//m11, m12, m13, 0,
				//m21, m22, m23, 0,
				//m31, m32, m33, 0,
				m11, m21, m31, 0,
				m12, m22, m32, 0,
				m13, m23, m33, 0,
				position.x,  position.y, position.z, position.w
			]);
		}

		public function rotate(eulerAngles:Vector3D):void
		{
			_rotation.incrementBy(eulerAngles);
		}
		
		public function rotateAround(angles:Number, axis:Vector3D, pivotPoint:Vector3D = null):void
		{
			// TODO
			throw new Error();
		}
		
		public function translate(x:Number, y:Number, z:Number):void
		{
			_position.x += x;
			_position.y += y;
			_position.z += z;
		}
		
		public function moveAxis(dx:Number, dy:Number, dz:Number):void
		{
			translateByVector(transformVector(new Vector3D(dx, dy, dz), 0));
		}
		
		public function translateByVector(v:Vector3D, multiplier:Number = 1):void
		{
			translate(v.x * multiplier, v.y * multiplier, v.z * multiplier);
		}
		
		public function lookAt(target:Vector3D):void
		{
			var v:Vector3D = target.subtract(position);
			var h:Number = Math.atan2(v.x, v.z);
			var p:Number = Math.asin( -v.y);
			_rotation.x = p;
			_rotation.y = h;
		}
		
		public function toString():String
		{
			return "Position:" + position + " Rotation:" + rotation + " Scale:" + scale;
		}
	}

}