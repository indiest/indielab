package me.sangtian.geom 
{
	import flash.geom.Matrix;
	import flash.geom.Matrix3D;
	import flash.geom.Orientation3D;
	import flash.geom.Vector3D;
	
	/**
	 * ...
	 * @author 
	 */
	public class MatrixTransform3D implements ITransform3D
	{
		private var _matrix:Matrix3D;
		
		public function get position():Vector3D
		{
			return _matrix.position;
		}
		
		public function set position(value:Vector3D):void
		{
			_matrix.position = position;
		}
		
		public function get rotation():Vector3D
		{
			return _matrix.decompose(Orientation3D.QUATERNION)[1];
		}
		
		public function get eulerAngles():Vector3D
		{
			return _matrix.decompose(Orientation3D.EULER_ANGLES)[1];
		}
		
		public function get scale():Vector3D
		{
			return _matrix.decompose()[2];
		}
		
		public function get right():Vector3D
		{
			return _matrix.deltaTransformVector(Vector3D.X_AXIS);
		}
		
		public function get up():Vector3D
		{
			return _matrix.deltaTransformVector(Vector3D.Y_AXIS);
		}
		
		public function get forward():Vector3D
		{
			return _matrix.deltaTransformVector(Vector3D.Z_AXIS);
		}
		
		public function MatrixTransform3D(matrix:Matrix3D = null) 
		{
			_matrix = matrix || new Matrix3D();
		}
		
		public function transformVector(v:Vector3D, positionMultiplier:Number = 1):Vector3D
		{
			if (positionMultiplier == 0)
			{
				return project3x3(v, _matrix);
			}
			else
			{
				return _matrix.transformVector(v);
			}
		}
		
		public function deltaTransformVector(v:Vector3D):Vector3D
		{
			return _matrix.deltaTransformVector(v);
		}
		
		public function rotate(eulerAngles:Vector3D):void
		{
			rotateAround(eulerAngles.x, Vector3D.X_AXIS);
			rotateAround(eulerAngles.y, Vector3D.Y_AXIS);
			rotateAround(eulerAngles.z, Vector3D.Z_AXIS);
		}
		
		public function rotateAround(angles:Number, axis:Vector3D, pivotPoint:Vector3D = null):void
		{
			_matrix.prependRotation(angles, axis, pivotPoint);
		}
		
		public function rotateHeading(angles:Number):void
		{
			rotateAround(angles, up);
		}
		
		public function rotatePitching(angles:Number):void
		{
			rotateAround(angles, right);
		}
		
		public function translate(x:Number, y:Number, z:Number):void
		{
			//trace(x, y, z);
			_matrix.prependTranslation(x, y, z);
		}
		
		private static function project3x3(v:Vector3D, m:Matrix3D):Vector3D
		{
			return new Vector3D(
				v.x * m.rawData[0] + v.y * m.rawData[4] + v.z * m.rawData[8],
				v.x * m.rawData[1] + v.y * m.rawData[5] + v.z * m.rawData[9],
				v.x * m.rawData[2] + v.y * m.rawData[6] + v.z * m.rawData[10]
			);
		}
		
		public function moveAxis(dx:Number, dy:Number, dz:Number):void
		{
			var dv:Vector3D = project3x3(new Vector3D(dx, dy, dz), _matrix);
			_matrix.position = _matrix.position.add(dv);
		}
		
		public function translateByVector(v:Vector3D, multiplier:Number = 1):void
		{
			translate(v.x * multiplier, v.y * multiplier, v.z * multiplier);
		}
		
		public function lookAt(pos:Vector3D):void
		{
			_matrix.pointAt(pos, null, up);
		}
		
		public function toMatrix():Matrix3D
		{
			return _matrix.clone();
		}
		
		public function toString():String
		{
			return "Position:" + position + " Rotation:" + rotation + " Scale:" + scale;
		}
	}

}