package me.sangtian.startrail 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import me.sangtian.common.Input;
	import me.sangtian.common.state.StateManager;
	import me.sangtian.common.Time;
	
	/**
	 * ...
	 * @author 
	 */
	public class Game extends Sprite 
	{
		public static var current:Game;
		public static var stage:Stage;
		
		public var stageWidth:int = 1024;
		public var stageHeight:int = 576;

		private var _stateLayer:Sprite;
		private var _stateManager:StateManager;
		
		public function get stateManager():StateManager 
		{
			return _stateManager;
		}
		
		public function Game() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			Game.current = this;
			Game.stage = this.stage;
			
			stageWidth = stage.stageWidth;
			stageHeight = stage.stageHeight;
			
			_stateLayer = new Sprite();
			addChild(_stateLayer);
			
			_stateManager = new StateManager(_stateLayer);
			_stateManager.loadState(DrawState);
			
			Input.initialize(stage);
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			Time.tick();
			Input.update();
			_stateManager.update();
		}
		
	}

}