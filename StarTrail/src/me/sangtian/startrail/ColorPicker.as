package me.sangtian.startrail 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author 
	 */
	public class ColorPicker extends Sprite 
	{
		private var _presetColors:Vector.<uint> = new Vector.<uint>();
		private var _selectedColor:uint;
		private var _bitmapData:BitmapData;
		
		public function get selectedColor():uint 
		{
			return _selectedColor;
		}
		
		public function set selectedColor(value:uint):void 
		{
			_selectedColor = value;
			graphics.clear();
			graphics.beginFill(value);
			graphics.drawRect(512, 0, 20, 20);
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function ColorPicker() 
		{
			_bitmapData = new BitmapData(512, 20, true, 0);
			addChild(new Bitmap(_bitmapData));
			
			var rect:Rectangle = new Rectangle(0, 0, 1, 20);
			for (var r:uint = 0; r < 255; r+=32)
			{
				for (var g:uint = 0; g < 255; g += 32)
				{
					for (var b:uint = 0; b < 255; b += 32)
					{
						var color:uint = ((r << 16) | (g << 8) | b);
						_presetColors.push(color);
						_bitmapData.fillRect(rect, 0xff000000 | color);
						rect.x++;
					}
				}
			}
			
			addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
		
		private function handleMouseUp(e:MouseEvent):void 
		{
			selectedColor = _presetColors[e.localX];
		}
		
	}

}