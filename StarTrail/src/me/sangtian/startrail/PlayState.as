package me.sangtian.startrail 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	import me.sangtian.common.Input;
	import me.sangtian.common.state.IState;
	import me.sangtian.graphics.ProjectionType;
	
	/**
	 * ...
	 * @author 
	 */
	public class PlayState extends Sprite implements IState
	{
		public static const X_AXIS:Vector3D = new Vector3D(1, 0, 0);
		public static const Y_AXIS:Vector3D = new Vector3D(0, 1, 0);
		public static const Z_AXIS:Vector3D = new Vector3D(0, 0, 1);

		public static var radius:Number = 1;
		public static var zDepth:Number = 0;// 10;
		public static var focalLength:Number = 200;
		public static var centerX:Number = Game.current.stageWidth / 2;
		public static var centerY:Number = Game.current.stageHeight / 2;
		public static var rotationSpeed:Number = 0.1;
		public static var headingStep:Number = 0.3;
		public static var pitchStep:Number = 0.1;
		
		private var _stars:Vector.<Vector.<Vector3D>> = new Vector.<Vector.<Vector3D>>();
		private var _dh:Number = 0;
		private var _dp:Number = 0;
		private var _lastX:Number;
		private var _lastY:Number;
		private var _projectionType:ProjectionType = ProjectionType.ORTHOGRAPHIC;
		private var _projectionMatrix:Matrix3D = new Matrix3D();
		
		public function get stars():Vector.<Vector.<Vector3D>>
		{
			return _stars;
		}
		
		public function PlayState() 
		{
			//_projectionMatrix.appendRotation(30, Y_AXIS, new Vector3D());
			//trace(_projectionMatrix.rawData);
			
			//createSphere();
			
			//createLines(-10, -10);
			//createLines(-10, 10);
			//createLines(10, -10);
			//createLines(10, 10);
		}
		
		public function enter():void 
		{
			_projectionMatrix.identity();
			_projectionMatrix.appendRotation(Math.random() * 360, Y_AXIS);
			_projectionMatrix.appendRotation(Math.random() * 360, X_AXIS);

			Game.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			Game.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
		
		public function exit():void 
		{
			Game.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			Game.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
		
		private function handleMouseUp(e:MouseEvent):void 
		{
			_lastX = NaN;
			_lastY = NaN;
		}
		
		private function handleMouseMove(e:MouseEvent):void 
		{
			if (!e.buttonDown)
				return;
			
			if (isNaN(_lastX))
				_lastX = e.stageX;
			if (isNaN(_lastY))
				_lastY = e.stageY;
			_dh = (e.stageX - _lastX) * rotationSpeed;
			_dp = (e.stageY - _lastY) * rotationSpeed;
			_lastX = e.stageX;
			_lastY = e.stageY;
			
			_projectionMatrix.appendRotation(_dh, Y_AXIS);
			_projectionMatrix.appendRotation(_dp, X_AXIS);
			//setProjectionMatrix(_dh);
			
			//_stars.length = 0;
			//createSphere();
		}
		
		private function setProjectionMatrix(heading:Number):void 
		{
			var rawData:Vector.<Number> = _projectionMatrix.rawData;
			var sinh:Number = Math.sin(heading);
			var cosh:Number = Math.cos(heading);
			rawData[0] = 1 - sinh * sinh;
			rawData[2] = -sinh * cosh;
			rawData[5] = 1;
			rawData[8] = -sinh * cosh;
			rawData[10] = 1 - cosh * cosh;
			_projectionMatrix.rawData = rawData;
		}
		
		private function createLines(x:Number, y:Number):void 
		{
			var points:Vector.<Vector3D> = new Vector.<Vector3D>();
			for (var z:Number = 1; z < 10; z++)
			{
				var v:Vector3D = new Vector3D(x, y, z);
				points.push(v);
				trace(v);
			}
			_stars.push(points);
		}
		
		private function createSphere():void 
		{
			for (var heading:Number = 0; heading < Math.PI * 2; heading += headingStep)
			{
				var sinh:Number = Math.sin(heading + _dh);
				var cosh:Number = Math.cos(heading + _dh);
				var points:Vector.<Vector3D> = new Vector.<Vector3D>();
				for (var pitch:Number = 0; pitch < Math.PI * 2; pitch += pitchStep)
				{
					// Spherical coordinates to 3D Cartesian coordinates
					var v:Vector3D = new Vector3D(
						radius * Math.cos(pitch + _dp) * sinh,
						-radius * Math.sin(pitch + _dp),
						radius * Math.cos(pitch + _dp) * cosh + zDepth);
					//trace(v);
					points.push(v);
				}
				_stars.push(points);
			}
		
		}
		
		public function update():void 
		{
			//_stars.length = 0;
			//createSphere();
			//_dh += 0.01;
			
			//_projectionMatrix.appendRotation(1, Y_AXIS);
			
			if (Input.getKeyUp(Keyboard.P))
				_projectionType = ProjectionType.PERSPECTIVE;
			else if (Input.getKeyUp(Keyboard.O))
				_projectionType = ProjectionType.ORTHOGRAPHIC;
			_projectionMatrix.appendTranslation(Input.getAxis(Input.AXIS_HORIZONTAL), -Input.getAxis(Input.AXIS_VERTICAL), 0);

			graphics.clear();
			graphics.beginFill(0xffffff);
			for each(var points:Vector.<Vector3D> in _stars)
			{
				var z:Number = 1;
				for (var i:uint = 0; i < points.length; i++)
				{
					var v:Vector3D = points[i];
					// w as line color
					graphics.lineStyle(1, v.w);
					v = _projectionMatrix.transformVector(v);
					if (_projectionType == ProjectionType.PERSPECTIVE)
						z = 1 / v.z;
					// 3D to 2D screen Orthographics/Perspective projection
					var x:Number = centerX + focalLength * v.x * z;
					var y:Number = centerY - focalLength * v.y * z;
					//trace(x, y);
					if (i == 0)
						graphics.moveTo(x, y);
					else
						graphics.lineTo(x, y);
					graphics.drawCircle(x, y, 2);
				}
			}
			graphics.endFill();
		}

		public function get isTransientState():Boolean 
		{
			return false;
		}
		
	}

}