package me.sangtian.startrail 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	import me.sangtian.common.Input;
	import me.sangtian.common.state.IState;
	import me.sangtian.geom.Path;
	
	/**
	 * ...
	 * @author 
	 */
	public class DrawState extends Sprite implements IState 
	{
		public static var drawRadius:Number = 200;
		
		private var _paths:Vector.<Path> = new Vector.<Path>();
		private var _currentPath:Path;
		private var _colorPicker:ColorPicker;
		
		public function DrawState() 
		{
			_colorPicker = new ColorPicker();
			_colorPicker.selectedColor = 0xffffff;
			_colorPicker.addEventListener(Event.CHANGE, handleColorChange);
			addChild(_colorPicker);
			
			graphics.clear();
			graphics.lineStyle(5, 0xffffff, 0.6);
			//graphics.drawCircle(Game.current.stageWidth / 2, Game.current.stageHeight / 2, drawRadius);
			graphics.drawRect(Game.current.stageWidth / 2 - drawRadius, Game.current.stageHeight / 2 - drawRadius, drawRadius * 2, drawRadius * 2);
		}
		
		private function handleColorChange(e:Event):void 
		{
			if (_currentPath.length > 0)
				beginNewPath();
		}

		public function enter():void 
		{
			_paths.length = 0;
			beginNewPath();
			
			Game.stage.addEventListener(MouseEvent.CLICK, handleMouseClick);
		}
		
		public function update():void 
		{
			if (Input.getKeyUp(Keyboard.SPACE))
				beginNewPath();
			if (Input.getKeyUp(Keyboard.ENTER))
				finishDrawing();
		}
		
		private function finishDrawing():void 
		{
			var playState:PlayState = Game.current.stateManager.loadState(PlayState) as PlayState;
			for each(var path:Path in _paths)
			{
				var path3D:Vector.<Vector3D> = new Vector.<Vector3D>();
				playState.stars.push(path3D);
				for each(var point:Point in path.nodes)
				{
					path3D.push(new Vector3D(point.x / drawRadius, point.y / drawRadius, Math.random() * 2 - 1,
						// color as w
						path.color));
				}
				trace(path3D);
			}
		}
		
		private function beginNewPath():void 
		{
			graphics.lineStyle(2, _colorPicker.selectedColor, 1);
			_currentPath = new Path(_colorPicker.selectedColor);
			_paths.push(_currentPath);
		}
		
		public function exit():void 
		{
			Game.stage.removeEventListener(MouseEvent.CLICK, handleMouseClick);
		}
		
		public function get isTransientState():Boolean 
		{
			return false;
		}
		
		private function handleMouseClick(e:MouseEvent):void 
		{
			var x:Number = e.stageX - Game.current.stageWidth / 2;
			if (Math.abs(x) > drawRadius)
				return;
			var y:Number = e.stageY - Game.current.stageHeight / 2;
			if (Math.abs(y) > drawRadius)
				return;
			if (_currentPath.length == 0)
				graphics.moveTo(e.stageX, e.stageY);
			else
				graphics.lineTo(e.stageX, e.stageY);
			graphics.drawCircle(e.stageX, e.stageY, 2);
			
			_currentPath.nodes.push(new Point(x, y));
		}
	}

}