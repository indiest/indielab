package me.sangtian.graphics 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.GraphicsPathCommand;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.TriangleCulling;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.system.ApplicationDomain;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.TimeTracker;
	import me.sangtian.common.util.ArrayUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.geom.ITransform3D;
	import me.sangtian.geom.MatrixTransform3D;
	import me.sangtian.geom.Mesh;
	import me.sangtian.geom.MeshFace;
	import me.sangtian.geom.EulerTransform3D;
	import me.sangtian.geom.UpTargetTransform3D;
	
	/**
	 * ...
	 * @author 
	 */
	public class Camera3D
	{
		private var _vertexBuffer:Vector.<Vector3D> = new Vector.<Vector3D>();
		private var _vertices:Vector.<Number> = new Vector.<Number>();
		private var _indexBuffer:Vector.<int> = new Vector.<int>();
		private var _texture:BitmapData;
		private var _uvData:Vector.<Point>;
		
		private var _shape:Shape = new Shape();
		private var _canvas:Bitmap;
		private var _bgColor:uint = 0xff000000;
		
		private var _centerX:Number;
		private var _centerY:Number;
		private var _projectionType:ProjectionType = ProjectionType.PERSPECTIVE;
		private var _transform:ITransform3D = new MatrixTransform3D();
		private var _projectionMatrix:Matrix3D = new Matrix3D();
		private var _worldToCamMatrix:Matrix3D;
		private var _nearClipping:Number = 0.1;
		private var _farClipping:Number = 1000;
		private var _zoom:Number = 1;
		private var _aspectRatio:Number;

		
		private var _depthBuffer:Vector.<Number>;
		private var _pixelBuffer:Vector.<uint>;
		
		private var _tt:TimeTracker = new TimeTracker();
		
		public function get projectionType():ProjectionType 
		{
			return _projectionType;
		}
		
		public function set projectionType(value:ProjectionType):void 
		{
			_projectionType = value;
		}
		
		//private var _focalLength:Number = 200;
		//public function get focalLength():Number 
		//{
			//return _focalLength;
		//}
		//
		//public function set focalLength(value:Number):void 
		//{
			//_focalLength = value;
		//}
		
		public function get bgColor():uint 
		{
			return _bgColor;
		}
		
		public function set bgColor(value:uint):void 
		{
			_bgColor = value;
		}
		
		public function get nearClipping():Number 
		{
			return _nearClipping;
		}
		
		public function set nearClipping(value:Number):void 
		{
			_nearClipping = value;
			updateProjectionMatrix();
		}
		
		public function get farClipping():Number 
		{
			return _farClipping;
		}
		
		public function set farClipping(value:Number):void 
		{
			_farClipping = value;
			updateProjectionMatrix();
		}
		
		public function get transform():ITransform3D 
		{
			return _transform;
		}
		
		public function get zoom():Number 
		{
			return _zoom;
		}
		
		public function set zoom(value:Number):void 
		{
			_zoom = value;
			updateProjectionMatrix();
		}
		
		public function get aspectRatio():Number 
		{
			return _aspectRatio;
		}
		
		public function set aspectRatio(value:Number):void 
		{
			_aspectRatio = value;
		}
		
		
		public function Camera3D(canvas:Bitmap) 
		{
			_canvas = canvas;
			_centerX = canvas.width / 2;
			_centerY = canvas.height / 2;
			_aspectRatio = canvas.width / canvas.height;
			
			_depthBuffer = new Vector.<Number>(canvas.width * canvas.height, true);
			_pixelBuffer = new Vector.<uint>(canvas.width * canvas.height, true);
			
			updateProjectionMatrix();
		}
		
		public function setVertices(...data):void 
		{
			_vertexBuffer.length = 0;
			var i:uint;
			for (i = 0; i < data.length; i += 3)
			{
				_vertexBuffer.push(new Vector3D(data[i], data[i + 1], data[i + 2]));
			}
		}
		
		public function setIndices(...data):void
		{
			_indexBuffer.length = 0;
			for (var i:uint = 0; i < data.length; i ++)
			{
				if (data[i] >= _vertexBuffer.length)
					throw new ArgumentError("Index " + data[i] + " is out of bound");
				_indexBuffer.push(data[i]);
			}
		}
		
		public function setUV(...data):void
		{
			if (_uvData == null)
				_uvData = new Vector.<Point>();
			for (var i:uint = 0; i < data.length; i += 2)
			{
				_uvData.push(new Point(data[i], data[i + 1]));
			}
		}
		
		public function setTexture(bitmapData:BitmapData):void
		{
			_texture = bitmapData;
		}
		
		public function addMesh(mesh:Mesh):void
		{
			if (mesh.textureCoords != null)
				_uvData = new Vector.<Point>();
				
			for (var i:uint = 0; i < mesh.vertices.length; i++)
			{
				_vertexBuffer.push(mesh.vertices[i]);
				if (mesh.textureCoords != null)
				{
					if (i < mesh.textureCoords.length)
						_uvData.push(mesh.textureCoords[i]);
					else // FIX ME
						_uvData.push(new Point(1, 0));
				}
			}
			
			for each(var f:MeshFace in mesh.faces)
			{
				if (f.vertexIndices.length == 3)
				{
					_indexBuffer.push(f.vertexIndices[0], f.vertexIndices[1], f.vertexIndices[2]);
				}
				else
				{
					_indexBuffer.push(f.vertexIndices[0], f.vertexIndices[1], f.vertexIndices[2],
						f.vertexIndices[1], f.vertexIndices[3], f.vertexIndices[2]);
				}
			}
		}
		
		private function updateProjectionMatrix():void
		{
			var rawData:Vector.<Number> = _projectionMatrix.rawData;
			rawData[0] = _zoom / _aspectRatio;
			rawData[5] = _zoom;
			rawData[10] = -(_farClipping + _nearClipping) / (_farClipping - _nearClipping);
			rawData[11] = - 2 * _farClipping * _nearClipping / (_farClipping - _nearClipping);
			rawData[14] = -1;
			_projectionMatrix.rawData = rawData;
			//Log.debug("Updated projection matrix:", _projectionMatrix.rawData);
		}
		
		private function transformToClip():void
		{
			_vertices.length = 0;
			//var z:Number = 1;
			for each(var vertex:Vector3D in _vertexBuffer)
			{
				vertex = _worldToCamMatrix.transformVector(vertex);
				
				// 3D to 2D screen Orthographics/Perspective projection
				/*
				if (_projectionType == ProjectionType.PERSPECTIVE)
				{
					z = vertex.z;
					if (z < nearClipping)
						z = nearClipping;
					else if (z > farClipping)
						z = farClipping;
					z = 1 / z;
				}
				_vertices.push(_centerX + focalLength * vertex.x * z);
				_vertices.push(_centerY - focalLength * vertex.y * z);
				*/
				
				//vertex.w = 1;
				vertex = _projectionMatrix.transformVector(vertex);
				//if (vertex.z < -vertex.w || vertex.z > vertex.w)
				//	Log.debug(vertex);
				_vertices.push(_centerX + vertex.x * _centerX / vertex.w);
				_vertices.push(_centerY - vertex.y * _centerY / vertex.w);
			}
		}
		
		public function update():void
		{
			_worldToCamMatrix = transform.toMatrix();
			//trace(_worldToCamMatrix.rawData);
			_worldToCamMatrix.invert();
			//trace(_worldToCamMatrix.rawData);
			//trace(_projectionMatrix.rawData);
			
			//transformToClip();
			//renderGraphics();

			//_tt.resetAll();
			
			renderPipeline();
			
			//trace(_tt.toString());
		}
		
		private function resetBuffer():void 
		{
			var len:uint = _canvas.width * _canvas.height;
			for (var i:uint = 0; i < len; i++)
			{
				_depthBuffer[i] = Number.MAX_VALUE;
				_pixelBuffer[i] = _bgColor;
			}
		}
		
		private function renderPipeline():void 
		{
			//resetVector(_depthBuffer, Number.MAX_VALUE);
			//_tt.startRecording("resetBuffer");
			resetBuffer();
			//_tt.endRecording("resetBuffer");
			
			_canvas.bitmapData.fillRect(_canvas.bitmapData.rect, _bgColor);
			_canvas.bitmapData.lock();
			
			for (var ii:uint = 0; ii < _indexBuffer.length; ii += 3)
			{
				var i:uint = _indexBuffer[ii];
				
				// Clip space positions
				//_tt.startRecording("vertexShader");
				var v0:Vector3D = vertexShader(_vertexBuffer[i]);
				var v1:Vector3D = vertexShader(_vertexBuffer[i + 1]);
				var v2:Vector3D = vertexShader(_vertexBuffer[i + 2]);
				//_tt.endRecording("vertexShader", 3);
				
				if (ii == 0)
					trace(v0, v1, v2);
				
				// Backface culling
				// TODO: pre-compute normals
				var normal:Vector3D = v1.subtract(v0).crossProduct(v2.subtract(v0));
				if (normal.dotProduct(transform.forward) >= 0)
					continue;
				
				// Clipping
				var p0:Vector3D = clip(v0);
				var p1:Vector3D = clip(v1);
				var p2:Vector3D = clip(v2);
				
				if (p0 && p1 && p2)
				{
					rasterizeTriangle(p0, p1, p2, _uvData[i], _uvData[i + 1], _uvData[i + 2]);
				}
			}
			
			//_tt.startRecording("bitmapData.setVector");
			_canvas.bitmapData.setVector(_canvas.bitmapData.rect, _pixelBuffer);
			//_tt.endRecording("bitmapData.setVector");
			_canvas.bitmapData.unlock();
		}
		
		private function clip(v:Vector3D):Vector3D
		{
			// FIXME
			//if (v.x < -1 || v.x > 1 || v.y < -1 || v.y > 1)
				//return null;
			
			var x:Number = _centerX + v.x * _centerX / v.w;
			var y:Number = _centerY - v.y * _centerY / v.w;
			return new Vector3D(x, y, v.z, v.w);
		}
		
		private var p01:Vector3D = new Vector3D();
		private var p012:Vector3D = new Vector3D();
		
		/*
		 * What operations are slow?
		 * 1. Call a function, create a variable in it, and return the variable
		 * 2. Big Array/Vector iteration
		 * 
		 * MathUtil.fastInterpolateVector3D and depthTest takes 20% CPU time separately
		 */
		
		private function rasterizeTriangle(p0:Vector3D, p1:Vector3D, p2:Vector3D, uv0:Point, uv1:Point, uv2:Point):void
		{
			//_tt.startRecording("rasterizeTriangle");
			var mag01:Number = Vector3D.distance(p0, p1);
			for (var i:Number = 0; i < mag01; i+=1)
			{
				MathUtil.fastInterpolateVector3D(p0, p1, i / mag01, p01);
				var uv01:Point = Point.interpolate(uv0, uv1, i / mag01);
				
				//_tt.startRecording("rasterizeLine");
				var mag012:Number = Vector3D.distance(p01, p2);
				for (var j:Number = 0; j < mag012; j+=0.5)
				{
					MathUtil.fastInterpolateVector3D(p01, p2, j / mag012, p012);
				
					if (p012.x < 0 || p012.x >= _canvas.width ||
						p012.y < 0 || p012.y >= _canvas.height)
						continue;
					var bufferIndex:int = int(p012.x) + int(p012.y) * _canvas.width;
						
					// Depth test
					if (p012.z > _depthBuffer[bufferIndex])
						continue;
					_depthBuffer[bufferIndex] = p012.z;
			
					var uv012:Point = Point.interpolate(uv01, uv2, j / mag012);
					
					//_tt.startRecording("fragmentShader");
					var color:uint = fragmentShader(uv012);
					//_tt.endRecording("fragmentShader");
					
					// TODO: Alpha test
					
					// TODO: Alpha blend
					
					//_tt.startRecording("setPixel");
					_pixelBuffer[bufferIndex] = color;
					//_tt.endRecording("setPixel");
					
					//_canvas.bitmapData.setPixel(p012.x, p012.y, color); 
				}
				//_tt.endRecording("rasterizeLine");
				
			}
			//_tt.endRecording("rasterizeTriangle");
		}
		
		private function rasterizeLine(p0:Vector3D, p1:Vector3D, uv0:Point, uv1:Point):void
		{
			
		}
		
		private function vertexShader(pos:Vector3D):Vector3D
		{
			return _projectionMatrix.transformVector(_worldToCamMatrix.transformVector(pos));
		}
		
		private function fragmentShader(uv:Point):uint
		{
			if (_texture == null)
			{
				return 0xffffff * uv.length;
			}
			// TODO: Clamp / Repeat
			return _texture.getPixel32(uv.x * _texture.width, uv.y * _texture.height);
		}
		
		protected function renderGraphics():void 
		{
			_canvas.bitmapData.fillRect(_canvas.bitmapData.rect, bgColor);

			_shape.graphics.clear();
			if (_texture == null)
			{
				_shape.graphics.lineStyle(1, 0xffffff);
			}
			else
			{
				_shape.graphics.beginBitmapFill(_texture);
			}
			_shape.graphics.drawTriangles(_vertices, _indexBuffer);// , _uvData, TriangleCulling.NONE);
			
			_canvas.bitmapData.draw(_shape);
		}
	}

}