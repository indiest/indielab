package me.sangtian.graphics 
{
	
	/**
	 * ...
	 * @author 
	 */
	public interface IObject3D 
	{
		public function get x():Number;
		public function set x(value:Number);
		public function get y():Number;
		public function set y(value:Number);
		public function get z():Number;
		public function set z(value:Number);
	}
	
}