package  
{
	import fl.transitions.Photo
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class WarpGate extends SpaceBody 
	{
		override protected function stepUpdate():void 
		{
			var level:SpaceRecyclerLevel = world as SpaceRecyclerLevel;
			if (level.junker != null && hitTestObject(level.junker)) {
				level.destroying = true;
				LevelManager.getInstance().loadNextLevel();
			}
		}
	}

}