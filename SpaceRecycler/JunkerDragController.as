package  
{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class JunkerDragController extends PhyMouseMotionController
	{
		override protected function handleMouseEvent(e:MouseEvent):void 
		{
			if (e.buttonDown == false)
			{
				var ship:Junker = parent as Junker;
				ship.dragTarget = null;
				ship.stopDraggingSound();
			}
			super.handleMouseEvent(e);
		}
		
		override protected function updateTarget(target:PhyBody, e:MouseEvent):void 
		{
			var ship:Junker = target as Junker;
			if (ship.dragTarget == null)
			{
				var power:Number = 0;
				for each(var phyBody:PhyBody in ship.world.bodies)
				{
					if (phyBody == ship)
						continue;
					if (phyBody.body.IsStatic())
						continue;
					if (phyBody.body.GetMass() > ship.maxDragMass)
						continue;
					var dist:Number = Util.getBodyDistance(phyBody.body, ship.body);
					if (dist > ship.maxDragDist)
						continue;
					var p:Number = /*phyBody.body.GetMass()*/ 1 / dist;
					if (p > power)
					{
						power = p;
						ship.dragTarget = phyBody;
					}
				}
				
				if (ship.dragTarget != null)
					ship.playDraggingSound();
			}
			if (ship.dragTarget != null)
			{
				if (ship.dragTarget.destroying)
				{
					ship.dragTarget = null;
				}
				else
				{
					super.updateTarget(ship.dragTarget, e);
				}
			}
		}
		
	}

}