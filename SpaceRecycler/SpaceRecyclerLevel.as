package  
{
	import flash.display.DisplayObjectContainer;
	import flash.display.Stage;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class SpaceRecyclerLevel extends PhyWorld
	{
		[Inspectable(name="junkObjective", type=int, defaultValue=0)]
		public var junkObjective:int = 0;
		
		public var junker:Junker;
		public var warpGate:WarpGate;
		public var objJunk:Junk;
		
		override protected function initWorld():void 
		{
			super.initWorld();
			
			var stage:Stage = SceneManager.getInstance().stage;
			SceneManager.getInstance().scrollRect.width = Math.max(this.width, stage.stageWidth);
			SceneManager.getInstance().uiLayer.junkObjective = junkObjective;
			SceneManager.getInstance().uiLayer.junkCollected = 0;

			// 传送门默认不可见，未激活
			warpGate.visible = false;
			warpGate.activated = false;
		}
		
		override protected function destroy():void 
		{
			super.destroy();
			SceneManager.getInstance().root.removeChild(this);
		}
	}

}