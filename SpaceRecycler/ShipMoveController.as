package  
{
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ShipMoveController extends PhyMouseMotionController
	{
		override protected function updateTarget(target:PhyBody, e:MouseEvent):void
		{
			super.updateTarget(target, e);
			var ship:Ship = target as Ship;
			ship.faceTo(e.localX, e.localY);
		}
	}

}