package  
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class JunkContactListener extends PhyBodyContactListener
	{
		override protected function handleContactEvent(target:PhyBody, e:ContactEvent):void 
		{
			if (e.type == ContactEvent.ADD)
			{
				//trace("contacting", target, "mass:", target.body.GetMass());
				var self:Junk = getBody() as Junk;
				var other:Junk = target as Junk;
				if (self.destroying == false && self.body.GetMass() >= other.body.GetMass())
				{
					other.destroying = true;
					self.expand(other.body.GetMass());
					//self.combine(other, e.point);
					SoundManager.getInstance().playSound(JunkContactSound);
					
					// 任务目标计数
					self.massCounting += other.massCounting;
					if (self.massCounting > SceneManager.getInstance().uiLayer.junkCollected)
					{
						SceneManager.getInstance().uiLayer.junkCollected = self.massCounting;
						if (self.massCounting >= SceneManager.getInstance().uiLayer.junkObjective)
						{
							var level:SpaceRecyclerLevel = self.world as SpaceRecyclerLevel;
							level.objJunk = self;
							var warpGate:WarpGate = level.warpGate;
							// 显示并激活传送门
							warpGate.visible = true;
							warpGate.activated = true;
						}
					}
				}
			}
		}
	}

}