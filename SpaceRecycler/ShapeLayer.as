package  
{
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ShapeLayer extends Sprite 
	{
		private var shapes:Object = { };
		
		public function getGraphics(name:String):Graphics
		{
			var shape:Shape = shapes[name];
			if (!shape)
			{
				shape = new Shape();
				shapes[name] = shape;
				addChild(shape);
			}
			return shape.graphics;
		}
	}

}