package  
{
	import com.adobe.serialization.json.JSON;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.getDefinitionByName;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class LevelManager 
	{
		private static var _instance:LevelManager = new LevelManager();
		public static function getInstance():LevelManager 
		{
			return _instance;
		}
		
		private var _currentLevel:int = 0;
		private var _currentLevelObject:SpaceRecyclerLevel;
		private var _loader:Loader = new Loader();
		private var _urlLoader:URLLoader = new URLLoader();
		
		public function LevelManager()
		{ 
			if (_instance != null)
				throw new Error("This class can only be used as singleton!");
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadLevelComplete);
			_urlLoader.addEventListener(Event.COMPLETE, handleLoadLevelComplete);
			_urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
		}
		
		private function handleLoadLevelComplete(e:Event):void 
		{
			var data:String = _urlLoader.data as String;
			var levelObj:Object = JSON.decode(data);
			_urlLoader.close();
			var LevelClass:Class = getDefinitionByName(levelObj["class"]) as Class;
			var level:SpaceRecyclerLevel = new LevelClass() as SpaceRecyclerLevel;
			for (var levelKey:String in levelObj)
			{
				if (levelKey == "bodies")
				{
					var bodies:Array = levelObj[levelKey] as Array;
					for each(var bodyObj:Object in bodies)
					{
						var BodyClass:Class = getDefinitionByName(bodyObj["class"]) as Class;
						var body:PhyBody = new BodyClass() as PhyBody;
						for (var bodyKey:String in bodyObj)
						{
							var bodyVal:* = bodyObj[bodyKey];
							if (bodyKey == "components")
							{
								var components:Array = bodyVal as Array;
								for each(var compObj:Object in components)
								{
									var comp:DisplayObject = body.getChildByName(compObj["name"]);
									if (comp == null)
									{
										// TODO: 为位图导出类
										var CompClass:Class = getDefinitionByName(compObj["class"]) as Class;
										comp = new CompClass() as DisplayObject;
									}
									for (var compKey:String in compObj)
									{
										if (comp.hasOwnProperty(compKey))
										{
											comp[compKey] = compObj[compKey];
										}
									}
									body.addChild(comp);
								}
							}
							else if (body.hasOwnProperty(bodyKey))
							{
								body[bodyKey] = bodyVal;
							}
						}
						level.addChild(body);
						// instance binding
						if (body.name && level.hasOwnProperty(body.name))
							level[body.name] = body;
					}
				}
				else if (level.hasOwnProperty(levelKey))
				{
					level[levelKey] = levelObj[levelKey];
				}
			}
			addLevel(level);
		}
		
		public function get currentLevel():int
		{
			return _currentLevel;
		}
		
		public function get currentLevelObject():SpaceRecyclerLevel
		{
			return _currentLevelObject;
		}
		
		public function loadLevel(level:int):void
		{
			_currentLevel = level;
			trace("Loading level", level);
			_loader.load(new URLRequest("Level" + level + ".swf"));
			//_urlLoader.load(new URLRequest("Level" + level + ".json"));
		}
		
		public function loadNextLevel():void
		{
			loadLevel(_currentLevel + 1);
		}
		
		private function onLoadLevelComplete(evt:Event):void
		{
			var swf:Sprite = _loader.content as Sprite;
			var level:SpaceRecyclerLevel = swf.getChildAt(0) as SpaceRecyclerLevel;
			addLevel(level);
		}
		
		public function addLevel(level:SpaceRecyclerLevel):void 
		{
			//level.initWorld();
			_currentLevelObject = level;
			level.y = SceneManager.getInstance().uiLayer.height;
			SceneManager.getInstance().levelLayer.addChild(level);
		}
	}

}