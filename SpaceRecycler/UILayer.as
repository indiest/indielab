package  
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import ui.HpBar;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class UILayer extends Sprite
	{
		public var hpBar:HpBar;
		public var txtObjective:TextField;
		
		private var _junkCollected:int;
		private var _junkObjective:int;
		public function get junkObjective():int 
		{
			return _junkObjective;
		}
		
		public function set junkObjective(value:int):void
		{
			_junkObjective = value;
			updateObjectiveStatus();
		}
		
		public function get junkCollected():int 
		{
			return _junkCollected;
		}
		
		public function set junkCollected(value:int):void 
		{
			_junkCollected = value;
			updateObjectiveStatus();
		}
		
		private function updateObjectiveStatus():void 
		{
			if (_junkObjective > 0)
				txtObjective.text = "JUNK COLLECTED: " + _junkCollected + " / " + _junkObjective;
			else
				txtObjective.text = "";
		}
		
	}

}