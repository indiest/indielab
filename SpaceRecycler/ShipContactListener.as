package  
{
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Common.Math.b2Vec2;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ShipContactListener extends PhyBodyContactListener
	{
		private var velocityBeforeContact:b2Vec2;
		
		override protected function handleContactEvent(target:PhyBody, e:ContactEvent):void 
		{
			var ship:Ship = getBody() as Ship;
			if (e.type == ContactEvent.ADD)
			{
				velocityBeforeContact = ship.body.GetLinearVelocity().Copy();
			}
			else if (e.type == ContactEvent.REMOVE)
			{
				var velocityAfterContact:b2Vec2 = ship.body.GetLinearVelocity();
				//var dvx:Number = Math.abs(velocityBeforeContact.x) - Math.abs(velocityAfterContact.x);
				//var dvy:Number = Math.abs(velocityBeforeContact.y) - Math.abs(velocityAfterContact.y);
				var damage:Number = 5 * ship.body.GetMass() * Math.abs(
					velocityBeforeContact.LengthSquared() - velocityAfterContact.LengthSquared());
				//trace(velocityBeforeContact.LengthSquared(), velocityAfterContact.LengthSquared());
				
				ship.damage(damage);
			}
		}
	}

}