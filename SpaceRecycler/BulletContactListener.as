package  
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class BulletContactListener extends PhyBodyContactListener
	{
		private var _exploding:Boolean = false;
		
		override protected function handleContactEvent(target:PhyBody, e:ContactEvent):void 
		{
			if (_exploding)
				return;
			//getBody().destroying = true;
			var body:Bullet = getBody() as Bullet;
			if (target is Ship)
			{
				(target as Ship).damage(20);
				body.explode();
				_exploding = true;
			}
			else 
			{
				body.destroying = true;
			}
		}
	}

}