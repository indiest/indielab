package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.geom.Point;
	import ui.HpBar;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Ship extends SpaceBody
	{
		public var maxHp:Number = 100;
		private var _hp:Number;
		
		protected var _hpBar:HpBar;
		
		public function get hp():Number 
		{
			return _hp;
		}
		
		public function damage(value:Number):void
		{
			_hp = Math.max(0, _hp - value);
			_hpBar.updateHp(_hp, maxHp);
			if (_hp <= 0)
			{
				destroying = true;
				
				// TODO Animate explosion!!!
			}
		}
		
		public function faceTo(targetX:int, targetY:int):void {
			var pos:b2Vec2 = body.GetPosition();
			var v:b2Vec2 = new b2Vec2(targetX / Global.RATIO - pos.x, targetY / Global.RATIO - pos.y);
			var theta = Math.atan(Math.abs(v.y / v.x));
			if (v.x < 0 && v.y > 0)
				theta = Math.PI - theta;
			else if (v.x < 0 && v.y < 0)
				theta = Math.PI + theta;
			else if (v.x > 0 && v.y < 0)
				theta = -theta;
			//trace("theta", theta);
			body.m_sweep.a = theta;
			// 飞船在开始移动后,不再有(碰撞导致的)角速度
			body.SetAngularVelocity(0);
			body.SynchronizeTransform();
			//target.body.GetXForm().R.Set(theta);
		}
		
		protected function initHpBar():void
		{
			_hpBar = new HpBar();
			_hpBar.width = 40;
			_hpBar.height = 5;
			updateHpBarPos();
			world.addChild(_hpBar);
		}
		
		override public function addedToWorld():void 
		{
			initHpBar();
			_hp = maxHp;
		}
		
		override protected function stepUpdate():void 
		{
			updateHpBarPos();
		}
		
		private function updateHpBarPos():void 
		{
			_hpBar.x = this.x - _hpBar.width / 2;
			_hpBar.y = this.y - 20;
		}
		
		override public function removingFromWorld():void 
		{
			world.removeChild(_hpBar);
		}
	}

}