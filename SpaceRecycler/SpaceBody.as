package  
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class SpaceBody extends PhyBody 
	{
		
		override public function step():void 
		{
			// 飞出屏幕上方或下方后，会从另一端飞入屏幕
			if (y < - height)
			{
				body.GetWorldCenter().y = SceneManager.getInstance().scrollRect.height / Global.RATIO;
			}
			else if (y > SceneManager.getInstance().scrollRect.height)
			{
				body.GetWorldCenter().y = - height / Global.RATIO;
			}
			super.step();
		}
	}

}