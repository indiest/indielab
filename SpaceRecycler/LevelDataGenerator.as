package 
{
	import com.adobe.serialization.json.JSON;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class LevelDataGenerator extends Sprite 
	{
		private var _loader:Loader = new Loader();
		
		public function LevelDataGenerator()
		{
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleLoadComplete);
			_loader.load(new URLRequest("Level1.swf"));
		}
		
		private function handleLoadComplete(e:Event):void 
		{
			var level:SpaceRecyclerLevel = (_loader.content as Sprite).getChildAt(0) as SpaceRecyclerLevel;
			var levelObj:Object = new Object();
			levelObj["class"] = getQualifiedSuperclassName(level);
			levelObj["name"] = level.name;
			levelObj["junkObjective"] = level.junkObjective;
			levelObj["doSleep"] = level.doSleep;
			levelObj["gravityX"] = level.gravityX;
			levelObj["gravityY"] = level.gravityY;
			levelObj["stepTime"] = level.stepTime;
			levelObj["useDebugDraw"] = level.useDebugDraw;
			var bodies:Array = [];
			levelObj["bodies"] = bodies;
			for each(var body:PhyBody in level.bodies)
			{
				var bodyObj = new Object();
				bodyObj["class"] = getQualifiedClassName(body);
				bodyObj["name"] = body.name;
				bodyObj["x"] = body.x;
				bodyObj["y"] = body.y;
				bodyObj["width"] = body.width;
				bodyObj["height"] = body.height;
				bodies.push(bodyObj);
			}
			trace(JSON.encode(levelObj));

			level.destroying = true;
		}
		
	}

}