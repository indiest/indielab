package  
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class SceneManager
	{
		private static var _instance:SceneManager = new SceneManager();
		public static function getInstance():SceneManager
		{
			return _instance;
		}
		
		public function SceneManager()
		{
			if (_instance != null)
				throw new Error("This class can only be used as singleton!");	
		}
		
		public var stage:Stage;
		public var root:Sprite;
		public var scrollRect:Rectangle;

		public var levelLayer:Sprite = new Sprite();
		public var uiLayer:UILayer;
		
		public function rearrangeLayers():void
		{
			root.addChildAt(levelLayer, 0);
			//root.addChildAt(uiLayer, 1);
		}
	}

}