package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.geom.Point;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class WarShip extends Ship
	{
		[Inspectable(name="bulletSpeed", type=Number, defaultValue=2.0)]
		public var bulletSpeed:Number = 8.0;
		[Inspectable(name="fireCooldown", type=int, defaultValue=1000)]
		public var fireCooldown:int = 1000;
		
		private var _lastFireTime:int = 0;

		protected function canFire():Boolean {
			return (getTimer() - _lastFireTime >= fireCooldown);
		}
		
		public function fire(muzzleX:Number, muzzleY:Number):void {
			if (canFire() == false)
				return;
			_lastFireTime = getTimer();
			var bullet:Bullet = new Bullet();
			var p:Point = localToGlobal(new Point(muzzleX, muzzleY));
			bullet.x = p.x + SceneManager.getInstance().scrollRect.x;
			bullet.y = p.y;
			bullet.rotation = this.rotation;
			//var v:b2Vec2 = body.GetLinearVelocity().Copy();
			var v:b2Vec2 = new b2Vec2(Math.cos(body.GetAngle()), Math.sin(body.GetAngle()));
			//v.Normalize();
			v.Multiply(bulletSpeed);
			v.Add(body.GetLinearVelocity());
			bullet.initVelocityX = v.x;
			bullet.initVelocityY = v.y;
			//bullet.initVelocity = v;
			world.addPhyBody(bullet);
		}
		
	}

}