package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class DasherSprintController extends PhyMouseMotionController
	{
		override protected function updateTarget(target:PhyBody, e:MouseEvent):void
		{
			var p:Point = target.localToGlobal(new Point(this.x, this.y));
			e.localX = p.x;// target.body.GetLinearVelocity().x;
			e.localY = p.y;// target.body.GetLinearVelocity().y;
			//trace("dasher pos:", target.x, target.y, "mouse local pos:", e.localX, e.localY, "mouse stage pos:", e.stageX, e.stageY);
			super.updateTarget(target, e);
		}
	}

}