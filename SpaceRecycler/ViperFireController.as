package  
{
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ViperFireController extends PhyMouseController
	{
		override protected function updateTarget(target:PhyBody, e:MouseEvent):void 
		{
			var viper:Viper = target as Viper;
			viper.fire(x, y);
		}
	}

}