package  
{
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * 
	 * @author Sang Tian
	 */
	public class SoundManager 
	{
		private static var _instance:SoundManager = new SoundManager();
		public static function getInstance():SoundManager
		{
			return _instance;
		}
		
		public function SoundManager()
		{
			if (_instance != null)
				throw new Error("This class can only be used as singleton!");	
		}
		
		private var sndCache:Dictionary = new Dictionary();
		private var sndChannels:Object = { };
		
		public function playSound(sndClass:Class, channelName:String = null, loops:int = 0):void
		{
			var snd:Sound = sndCache[sndClass] || new sndClass();
			sndCache[sndClass] = snd;
			
			var sndChannel:SoundChannel = snd.play(0, loops);
			if (channelName)
			{
				stopSound(channelName);
				sndChannels[channelName] = sndChannel;
			}
		}
		
		public function stopSound(channelName:String):void
		{
			var sndChannel:SoundChannel = sndChannels[channelName];
			if (sndChannel)
			{
				sndChannel.stop();
				delete sndChannels[channelName];
			}
		}
	}

}