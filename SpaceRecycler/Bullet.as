package  
{
	import Box2D.Common.Math.b2Vec2;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Bullet extends PhyBody
	{
		public function Bullet()
		{
			isBullet = true;
		}
		
		public function explode()
		{
			gotoAndPlay("explode");
		}
	}

}