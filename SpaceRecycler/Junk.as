package  
{
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Junk extends SpaceBody
	{
		[Inspectable(name="massCounting", type=int, defaultValue=1)]
		public var massCounting:int = 1;
		
		private var _expandingMass:Number = 0;
		
		public function Junk()
		{
			randomDraw(20);
		}
		
		private function randomDraw(count:uint):void
		{
			graphics.clear();
			for (var i:int = 0; i < count; i++)
			{
				var r:Number = Math.random() * this.width / 2;
				var a:Number = Math.random() * Math.PI * 2;
				var x:Number = r * Math.cos(a);
				var y:Number = r * Math.sin(a);
				var c:uint = Util.random(100, 255);
				graphics.beginFill(Util.getColor(c, c, c));
				graphics.drawRect(x, y, 2, 2);
			}
		}

		public function expand(mass:Number):void 
		{
			//trace("expand mass:", mass);
			_expandingMass = mass;
		}
		
		override protected function stepUpdate():void 
		{
			if (_expandingMass > 0)
			{
				var circleShape:b2CircleShape = body.GetShapeList() as b2CircleShape;
				var newRadius:Number = Math.sqrt((body.GetMass() + _expandingMass) / Math.PI / circleShape.m_density);
				var scale:Number = newRadius / circleShape.m_radius;
				//trace("expand scale:", scale);
				circleShape.m_radius = newRadius;
				body.SetMassFromShapes();
				this.scaleX *= scale;
				this.scaleY *= scale;
				//randomDraw(body.GetMass() * 10);
				_expandingMass = 0;
				body.GetMass()
			}
			
/*			if (_otherJunk != null)
			{
				var offset:b2Vec2 = Util.subVector(_otherJunk.body.GetPosition(), this.body.GetPosition());
				for (var shape:b2Shape = _otherJunk.body.GetShapeList(); shape != null; shape = shape.GetNext())
				{
					if (shape.GetType() == b2Shape.e_circleShape)
						b2CircleShape(shape).GetLocalPosition().Add(offset);
					else if (shape.GetType() == b2Shape.e_polygonShape)
						b2PolygonShape(shape).GetCentroid().Add(offset);
					
					// === code from b2Body.createShape ===
					shape.m_next = body.m_shapeList;
					body.m_shapeList = shape;
					++body.m_shapeCount;
					
					shape.m_body = body;
					
					// Add the shape to the world's broad-phase.
					//shape.CreateProxy(body.m_world.m_broadPhase, body.m_xf);
					
					// Compute the sweep radius for CCD.
					//shape.UpdateSweepRadius(body.m_sweep.localCenter);
					// === end code ===
				}
				body.SetMassFromShapes();
				_otherJunk.destroying = true;
				_otherJunk = null;
				_contactPoint = null;
			}
*/		}
		
		//private var _otherJunk:Junk;
		//private var _contactPoint:b2ContactPoint;
		//
		//public function combine(other:Junk, point:b2ContactPoint):void 
		//{
			//_otherJunk = other;
			//_contactPoint = point;
		//}
	}

}