package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class AI_Viper extends WarShip
	{
		static private const ERROR_RANGE:Number = 0.1;
		private var _targetShip:Ship;
		//private var _targetSafe:PhyBody;
		private var _nextFindBlockTime:int;
		
		override protected function stepUpdate():void 
		{
			super.stepUpdate();
			if (_targetShip == null)
			{
				_targetShip = findTargetShip();
				if (_targetShip == null)
				{
					trace("cannot find target ship, deactivated")
					activated = false;
					return;
				}
				//trace("target ship found:", _targetShip);
			}
			if (getTimer() < _nextFindBlockTime)
				return;
			var block:PhyBody = findBlock();
			if (block != null)
			{
				_nextFindBlockTime = getTimer() + 1000;
				var v:b2Vec2 = new b2Vec2(x - _targetShip.x, y - _targetShip.y);
				v.CrossFV(Util.randomPositiveOrNagative());
				faceTo(v.x, v.y);
				v.Multiply(0.2 / Global.RATIO);
				this.body.SetLinearVelocity(v);
				//this.body.ApplyForce(v, this.body.GetPosition());
				//trace("target ship blocked, move towards:", v.x, v.y);
			}
			else {
				if (canFire())
				{
					this.body.GetLinearVelocity().Multiply(0.5);
					faceTo(_targetShip.x, _targetShip.y);
					fire(30, 0);
					//trace("fire to:", _targetShip.x, _targetShip.y);
				}
			}
		}
		
		private function findBlock():PhyBody {
			var dx:Number = x - _targetShip.x;
			var dy:Number = y - _targetShip.y;
			var sx:int = Util.sign(dx);
			var sy:int = Util.sign(dy);
			var angle:Number = Util.getAngleInRadians(dx, dy);
			for each(var body:PhyBody in world.bodies) 
			{
				if (body == this || body == _targetShip)
					continue;
				var bdx:Number = x - body.x;
				var bdy:Number = y - body.y;
				var bsx:int = Util.sign(bdx);
				var bsy:int = Util.sign(bdy);
				if (bdx * bsx > dx * sx || bdy * bsy > dy * sy)
					continue;
				var ba:Number = Util.getAngleInRadians(bdx, bdy);
				if (Math.abs(angle - ba) <= ERROR_RANGE)
					return body;
			}
			return null;
		}
		
		protected function findTargetShip():Ship 
		{
			for each(var body:PhyBody in world.bodies)
			{
				if (body is Junker && body != this)
				{
					return body as Ship;
				}
			}
			return null;
		}
	}

}