package ui 
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class HpBar extends Sprite
	{
		public var innerBar:Sprite;
		
		public function updateHp(hp:Number, hpMax:Number):void
		{
			innerBar.scaleX = hp / hpMax;
		}
	}

}