package  
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Junker extends Ship
	{
		public var maxDragMass:Number = 20;
		public var maxDragDist:Number = 10;

		// Don't have to declare these controllers as the ship count be taken by AI.
		//public var moveController:ShipMoveController;
		//public var dragController:JunkerDragController;
		public var dragTarget:PhyBody = null;
		
		override protected function initHpBar():void 
		{
			_hpBar = SceneManager.getInstance().uiLayer.hpBar;
		}
		
		public function playDraggingSound():void
		{
			SoundManager.getInstance().playSound(JunkerDraggingSound, this.name, int.MAX_VALUE);
		}
		
		public function stopDraggingSound():void
		{
			SoundManager.getInstance().stopSound(this.name);
		}
		
		override protected function stepUpdate():void 
		{
			var g:Graphics = world.shapeLayer.getGraphics(this.name);
			//var g:Graphics = (world.container as Sprite).graphics;
			g.clear();
			if (dragTarget != null && dragTarget.destroying == false)
			{
				var dist:Number = Util.getBodyDistance(dragTarget.body, body);
				if (dist > maxDragDist)
				{
					dragTarget = null;
					return;
				}
				g.moveTo(x, y);
				g.lineStyle(1, 0x00ff00, Math.max(0.2, 1 - dist / maxDragDist));
				g.lineTo(dragTarget.x, dragTarget.y);
				
				//var d:Number = dist * Global.RATIO;
				//var r:Number = Util.anglesToRadians(rotation);
				//g.lineTo(d * Math.cos(r), d * Math.sin(r));
				
				//g.lineStyle(1, /*Util.getColor(0, 0, 219)*/0x00ff00, 1 - dist / maxDragDist);
				//g.lineTo(dragTarget.x - x, dragTarget.y - y);
				//trace("Junker at", x, y, " drag target at", dragTarget.x, dragTarget.y);
			}
			
			var scrollRect:Rectangle = SceneManager.getInstance().scrollRect;
			//trace("scrollRect:", scrollRect);
			var vx:Number = body.GetLinearVelocity().x;
			if (vx != 0.0)
			{
				//scrollRect.x += Util.sign(vx);
				//if (scrollRect.right > world.container.width)
				//{
					//scrollRect.x = world.container.width - scrollRect.width;
				//}
				//else if (scrollRect.left < 0)
				//{
					//scrollRect.x = 0;
				//}
				var stage:Stage = SceneManager.getInstance().stage;
				scrollRect.x = x - stage.stageWidth / 2;
				if (scrollRect.x < 0)
					scrollRect.x = 0;
				if (scrollRect.x > scrollRect.width - stage.stageWidth)
					scrollRect.x = scrollRect.width - stage.stageWidth;
				world.container.scrollRect = scrollRect;
			}
		}
		
		override public function removingFromWorld():void 
		{
			// 覆盖掉子类的方法
		}
	}

}