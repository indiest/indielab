package  
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.system.System;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class SpaceRecycler extends Sprite
	{
		
		public var uiLayer:UILayer;
		
		public var level0:SpaceRecyclerLevel;
		
		public function SpaceRecycler()
		{
			addEventListener(Event.ACTIVATE, handleActivate);
			addEventListener(Event.DEACTIVATE, handleDeactivate);

			SceneManager.getInstance().stage = stage;
			SceneManager.getInstance().root = this;
			SceneManager.getInstance().uiLayer = uiLayer;
			SceneManager.getInstance().scrollRect = new Rectangle(0, uiLayer.height, stage.stageWidth, stage.stageHeight - uiLayer.height);
			addChildAt(SceneManager.getInstance().levelLayer, 0);
			
			level0 = new Level0();
			level0.junkObjective = 5;
			LevelManager.getInstance().addLevel(level0);
			
			//SceneManager.getInstance().rearrangeLayers();
			
			//LevelManager.getInstance().loadNextLevel();
		}
		
		private function handleActivate(e:Event):void 
		{
			System.resume();
		}
		
		private function handleDeactivate(e:Event):void 
		{
			System.pause();
		}
	}

}