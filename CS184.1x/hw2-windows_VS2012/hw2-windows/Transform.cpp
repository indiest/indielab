// Transform.cpp: implementation of the Transform class.

// Note: when you construct a matrix using mat4() or mat3(), it will be COLUMN-MAJOR
// Keep this in mind in readfile.cpp and display.cpp
// See FAQ for more details or if you're having problems.

#include "Transform.h"

// Helper rotation function.  Please implement this.  
mat3 Transform::rotate(const float degrees, const vec3& axis) 
{
	vec3 a = glm::normalize(axis);
	float c = glm::cos(degrees * pi / 180);
	float s = glm::sin(degrees * pi / 180);
	return mat3(a.x * a.x * (1 - c) + c, a.x * a.y * (1 - c) + a.z * s, a.x * a.z * (1 - c) - a.y * s,
	          a.x * a.y * (1 - c) - a.z * s, a.y * a.y * (1 - c) + c, a.y * a.z * (1 - c) + a.x * s,
			  a.x * a.z * (1 - c) + a.y * s, a.y * a.z * (1 - c) - a.x * s, a.z * a.z * (1 - c) + c);
}

void Transform::left(float degrees, vec3& eye, vec3& up) 
{
	eye = rotate(degrees, up) * eye;
}

void Transform::up(float degrees, vec3& eye, vec3& up) 
{
	vec3 left = glm::cross(eye, up);
	eye = rotate(degrees, left) * eye;
	up = rotate(degrees, left) * up;
}

mat4 Transform::lookAt(const vec3 &eye, const vec3 &center, const vec3 &up) 
{
  vec3 w = glm::normalize(eye);
  vec3 u = glm::normalize(glm::cross(up, w));
  vec3 v = glm::cross(w, u);
  mat3 r = glm::transpose(mat3(u, v, w));
  vec3 t = r * (-eye);

  return glm::transpose(mat4(
			u.x, u.y, u.z, t.x,
	        v.x, v.y, v.z, t.y,
			w.x, w.y, w.z, t.z,
			0, 0, 0, 1));
}

mat4 Transform::perspective(float fovy, float aspect, float zNear, float zFar)
{
	float d = tan(fovy * pi / 360.0);
    mat4 ret = mat4(0);
	ret[0][0] = 1.0 / (d * aspect);
	ret[1][1] = 1.0 / d;
	ret[2][2] = -(zFar + zNear) / (zFar - zNear);
	ret[2][3] = -1;
	ret[3][2] = -2.0 * zFar * zNear / (zFar - zNear);
    return ret;
}

mat4 Transform::scale(const float &sx, const float &sy, const float &sz) 
{
    return mat4(sx, 0, 0, 0,
				0, sy, 0, 0,
				0, 0, sz, 0,
				0, 0, 0, 1);
}

mat4 Transform::translate(const float &tx, const float &ty, const float &tz) 
{
    return mat4(1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				tx, ty, tz, 1);
}

// To normalize the up direction and construct a coordinate frame.  
// As discussed in the lecture.  May be relevant to create a properly 
// orthogonal and normalized up. 
// This function is provided as a helper, in case you want to use it. 
// Using this function (in readfile.cpp or display.cpp) is optional.  

vec3 Transform::upvector(const vec3 &up, const vec3 & zvec) 
{
    vec3 x = glm::cross(up,zvec); 
    vec3 y = glm::cross(zvec,x); 
    vec3 ret = glm::normalize(y); 
    return ret; 
}


Transform::Transform()
{

}

Transform::~Transform()
{

}
