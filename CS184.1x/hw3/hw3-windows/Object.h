#pragma once

#include "Transform.h"

enum shape {sphere, triangle} ;

struct Ray
{
	vec3 origin;
	vec3 dir;
};

class Object
{
protected:
	Object(shape t): type(t){};

public:
	struct HitInfo
	{
		Object* obj;
		float t;
		vec3 pos;
		vec3 normal;
	};

	shape type; 
	vec4 ambient; 
	vec4 diffuse; 
	vec4 specular; 
	vec4 emission; 
	float shininess;
	float refractivity;
	mat4 transform; 

	virtual ~Object(){};
	virtual bool raycast(const Ray& ray, HitInfo& hitinfo);
};

class Sphere : public Object
{
private:
	mat4* inverseTrans;
	mat3* transposeInverseTrans;
public:
	vec3 center;
	float radius;

	Sphere(const vec3 c, const float r) : Object(sphere), center(c), radius(r), inverseTrans(NULL) {};
	~Sphere(){};
	bool raycast(const Ray& ray, HitInfo& hitinfo);
};

class Triangle : public Object
{
private:
	mat4* inverseTrans;
	vec3 transNormal;
	vec3 transVertices[3];
public:
	vec3 vertices[3];
	vec3 normal;

	Triangle(const vec3 v[3]);
	~Triangle(){};
	bool raycast(const Ray& ray, HitInfo& hitinfo);
};

