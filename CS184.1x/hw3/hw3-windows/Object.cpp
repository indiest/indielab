#include "Object.h"

inline bool Object::raycast(const Ray& ray, HitInfo& hitinfo)
{
	return false;
}


bool Sphere::raycast(const Ray& ray, HitInfo& hitinfo)
{
	// Optimization: matrix inversing is slow, pre-calculate it
	if (inverseTrans == NULL)
	{
		inverseTrans = new mat4(glm::inverse(transform));
		transposeInverseTrans = new mat3(glm::transpose(*inverseTrans));
	}
	vec3 p0 = vec3((*inverseTrans) * vec4(ray.origin, 1.0f));
	vec3 p1 = vec3((*inverseTrans) * vec4(ray.dir, 0.0f));
	vec3 v = this->center - p0;

	float e = glm::length(v);
	// check if ray origin is inside the sphere
	if (e <= radius)
		return false;

	float dot = glm::dot(v, p1);
	if (dot < 0)
		return false;

	float d = glm::length(p1);
	float a = dot / d;
	float f = radius * radius - (e * e - a * a);
	if (f <= 0)
		return false;

	float t = (a - sqrt(f)) / d;
	vec3 p = p0 + p1 * t;
	hitinfo.obj = this;
	hitinfo.pos = vec3(transform * vec4(p, 1.0f));
	hitinfo.t = t;//glm::length(ray.origin - hitinfo.pos);
	vec3 n = glm::normalize(p - this->center);
	hitinfo.normal = glm::normalize((*transposeInverseTrans) * n);
	return true;
}

Triangle::Triangle(const vec3 v[3]) : Object(triangle)
{
	for (int i = 0; i < 3; i++)
	{
		vertices[i] = v[i];//vec3(transform * vec4(v[i], 1.0f));//
	}
	normal = glm::normalize(glm::cross(vertices[1] - vertices[0], vertices[2] - vertices[0]));
	inverseTrans = NULL;
}

bool Triangle::raycast(const Ray& ray, HitInfo& hitinfo)
{
	// Optimization: matrix inversing is slow, pre-calculate it
	if (inverseTrans == NULL)
	{
		inverseTrans = new mat4(glm::inverse(transform));
		transNormal = glm::normalize(vec3(glm::transpose(*inverseTrans) * vec4(normal, 0.0f)));
	}
	vec3 p0 = vec3((*inverseTrans) * vec4(ray.origin, 1.0f));//ray.origin;//
	vec3 p1 = vec3((*inverseTrans) * vec4(ray.dir, 0.0f));//ray.dir;//

	float dot = glm::dot(normal, p1);
	// check side
	if (dot >= 0)
		return false;

	float t = (glm::dot(normal, vertices[0]) - glm::dot(normal, p0)) / dot;
	if (t <= 0)
		return false;

	vec3 p = p0 + p1 * t;

	float u0,u1,u2,v0,v1,v2;
	if (abs(normal.x) > abs(normal.y))
	{
		if (abs(normal.x) > abs(normal.z))
		{
			u0 = p.y - vertices[0].y; v0 = p.z - vertices[0].z;
			u1 = vertices[1].y - vertices[0].y; v1 = vertices[1].z - vertices[0].z;
			u2 = vertices[2].y - vertices[0].y; v2 = vertices[2].z - vertices[0].z;
		}
		else
		{
			u0 = p.x - vertices[0].x; v0 = p.y - vertices[0].y;
			u1 = vertices[1].x - vertices[0].x; v1 = vertices[1].y - vertices[0].y;
			u2 = vertices[2].x - vertices[0].x; v2 = vertices[2].y - vertices[0].y;
		}
	}
	else
	{
		if (abs(normal.y) > abs(normal.z))
		{
			u0 = p.x - vertices[0].x; v0 = p.z - vertices[0].z;
			u1 = vertices[1].x - vertices[0].x; v1 = vertices[1].z - vertices[0].z;
			u2 = vertices[2].x - vertices[0].x; v2 = vertices[2].z - vertices[0].z;
		}
		else
		{
			u0 = p.x - vertices[0].x; v0 = p.y - vertices[0].y;
			u1 = vertices[1].x - vertices[0].x; v1 = vertices[1].y - vertices[0].y;
			u2 = vertices[2].x - vertices[0].x; v2 = vertices[2].y - vertices[0].y;
		}
	}

	float denominator = u1 * v2 - v1 * u2;
	if (denominator == 0)
		return false;

	float alpha = (u0 * v2 - v0 * u2) / denominator;
	if (alpha < 0)
		return false;

	float beta = (u1 * v0 - v1 * u0) / denominator;
	if (beta < 0)
		return false;

	float gamma  = 1.0f - alpha - beta;
	if (gamma < 0)
		return false;

	/*
	vec3 v0 = vertices[0] - p;
	vec3 v1 = vertices[1] - p;
	vec3 n = glm::normalize(glm::cross(v1, v0));
	if (glm::dot(p, n) - glm::dot(ray.origin, n) < 0)
		return false;
	*/

	hitinfo.obj = this;
	hitinfo.pos = vec3(transform * vec4(p, 1.0f));//p;//
	hitinfo.t = t;//glm::length(ray.origin - hitinfo.pos);
	hitinfo.normal = transNormal;//normal;//

	return true;
}