/*****************************************************************************/
/* This is the program skeleton for homework 2 in CS 184 by Ravi Ramamoorthi */
/* Extends HW 1 to deal with shading, more transforms and multiple objects   */
/*****************************************************************************/

// This file is display.cpp.  It includes the skeleton for the display routine

// Basic includes to get this file to work.  
#include <iostream>
#include <time.h>
#include <process.h>
#include <Windows.h>
#include <WinBase.h>
#include <string>
#include <fstream>
#include <sstream>
#include <deque>
#include <stack>
#include <vector>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Transform.h"
#include "Object.h"

using namespace std; 
#include "variables.h"

#define MULTITHREAD
#define START_X 0
#define START_Y 0
#define END_X	w
#define END_Y	h

const vec4 BLACK = vec4(0.0f, 0.0f, 0.0f, 1.0f);
const float INFINITY = std::numeric_limits<float>::max();
// Extract screen x/y variable for debugging
int scrx, scry;
vec3 waxis, uaxis, vaxis;
int depth;

bool iszero(vec4 v)
{
	return v.x == 0.0f && v.y == 0.0f && v.z == 0.0f;
}

void setPixel(const int x, const int y, const vec4 color)
{
	int index = ((h-1-y)*w+x)*3;
	pixels[index++] = color.r;
	pixels[index++] = color.g;
	pixels[index++] = color.b;
}

void drawRectangle(const int left, const int top, const int width, const int height, const vec4 color)
{
	for (int y = top; y < top + height; y++)
	{
		for (int x = left; x < left + width; x++)
		{
			setPixel(x, y, color);
		}
	}
}

inline bool raycastAllObjects(const Ray& ray, Object::HitInfo& nearestHit, float minDist, const Object* ignoreObj)
{
	Object::HitInfo hit;
	for (int i = 0; i < objects.size(); i++)
	{
		Object* obj = objects[i];
		if (obj == ignoreObj)
			continue;
		if (obj->raycast(ray, hit))
		{
			//if (scrx == 316 && scry == 80 && depth == 1)	__debugbreak();
			if (hit.t < minDist)
			{
				minDist = hit.t;
				nearestHit = hit;
			}
		}
	}
	return nearestHit.t == minDist;
}

inline bool raycastOneObject(const Ray& ray, float minDist, const Object* ignoreObj)
{
	Object::HitInfo hit;
	for (int i = 0; i < objects.size(); i++)
	{
		Object* obj = objects[i];
		if (obj == ignoreObj)
			continue;
		if (obj->raycast(ray, hit))
		{
			if (hit.t < minDist)
			{
				return true;
			}
		}
	}
	return false;
}

inline vec4 computeLight(const Light& light, const vec3& dir, const vec3& eyepos, float d, const Object::HitInfo* hit)
{
	//float d = glm::length(vec3(light.pos) - hit->pos);
	vec4 lightColor = light.color / (light.attenuation[0] + light.attenuation[1] * d + light.attenuation[2] * d * d);

	float diffuseFactor = glm::dot(dir, hit->normal);
	if (diffuseFactor <= 0.0f)
		return BLACK;
	vec4 diffuse = lightColor * hit->obj->diffuse * max(diffuseFactor, 0.0f);

	vec3 eyedir = glm::normalize(eyepos - hit->pos);
	vec3 half = glm::normalize(eyedir + dir);
	float specularFactor = glm::dot(half, hit->normal);
	vec4 specular = lightColor * hit->obj->specular * pow(max(specularFactor, 0.0f), hit->obj->shininess);

	return diffuse + specular;
}

vec4 findColor(const Ray& ray, const vec3& eyepos)
{
	depth++;
	if (depth > maxdepth)
		return BLACK;

	Object::HitInfo hit;
	if (!raycastAllObjects(ray, hit, INFINITY, NULL))
	{
		return BLACK;
	}

	// Calculate lighting/shadow
	vec4 color = hit.obj->ambient + hit.obj->emission;
	Object::HitInfo lightHit;
	for (int j = 0; j < numused; j++)
	{
		Light light = lights[j];
		vec3 dir;
		// If the interaction is behind the point light source, it won't produce shadow
		float distToLight;
		if (light.pos.w == 0)
		{
			dir = -light.dir;
			distToLight = INFINITY;
		}
		else
		{
			dir = vec3(light.pos) / light.pos.w - hit.pos;
			distToLight = glm::length(dir);
			dir = dir / distToLight;
		}
		Ray lightRay = {hit.pos, dir};
		if (!raycastOneObject(lightRay, distToLight, hit.obj))
			color += computeLight(light, dir, eyepos, distToLight, &hit);
	}

	// Reflection ray
	if (!iszero(hit.obj->specular))
	{
		Ray reflectionRay = {hit.pos, 
			2.0f * glm::dot(hit.normal, -ray.dir) * hit.normal + ray.dir};
			//glm::normalize(hit.normal * 2.0f + ray.dir)};
		vec4 rc = hit.obj->specular * findColor(reflectionRay, hit.pos);
		color += rc;
	}

	// Refraction ray
	if (hit.obj->refractivity > 0)
	{
		Ray refractionRay = {hit.pos,
			ray.dir};// * (-hit.normal) * hit.obj->refraction
		color += hit.obj->refractivity * findColor(refractionRay, hit.pos);
	}

	return color;
}

Ray shootRay(int x, int y, const vec3& uaxis, const vec3& vaxis, const vec3& waxis)
{
	const float dy = tan(fovy * pi / 360);
	const float dx = dy * (float)w / (float)h;
	// shoot in the middle of each pixel
	float alpha = dx * (w * 0.5f - (x + 0.5f)) / (w * 0.5f);
	float beta  = dy * (h * 0.5f - (y + 0.5f)) / (h * 0.5);

	vec3 dir = alpha * uaxis + beta * vaxis + waxis;
	Ray r = {eye, glm::normalize(dir)};
	return r;
}

#ifdef MULTITHREAD
void raytraceLine(void* param)
{
	int y = *(int*)param;
	for (int x = START_X; x < END_X; x++)
	{
		Ray ray = shootRay(x, y, uaxis, vaxis, waxis);
		depth = 0;
		vec4 color = findColor(ray, eye);
		setPixel(x, y, color);
	}
	glutPostRedisplay();
	//std::cout<< y << "\n" ;
	delete(param);
}
#endif

void raytrace()
{
//	drawRectangle(100, 100, 200, 300, vec4(1.0f, 0.0f, 0.0f, 3.0f));
//	drawRectangle(400, 400, 300, 20, vec4(1.0f, 1.0f, 0.0f, 0.1f));
//	return;
	clock_t t = clock();
	waxis = glm::normalize(center - eye);
	uaxis = glm::normalize(glm::cross(up, waxis));
	vaxis = glm::cross(waxis, uaxis);

#ifdef MULTITHREAD
	HANDLE handles[1000];
	for (int y = START_Y; y < END_Y; y++)
	{
		int* yp = new int;
		*yp = y;
		handles[y] = (HANDLE*)_beginthread(raytraceLine, 0, yp);
	}
	WaitForMultipleObjects(END_Y - START_Y, handles, true, 0);
#else		
	//#pragma omp parallel 
	{
		//#pragma omp for schedule(dynamic)
		for (scry = START_Y; scry < END_Y; scry++)
		{
			for (scrx = START_X; scrx < END_X; scrx++)
			{
				Ray ray = shootRay(scrx, scry, uaxis, vaxis, waxis);
				depth = 0;
				vec4 color = findColor(ray, eye);
				setPixel(scrx, scry, color);
			}
		}
	}
#endif
	std::cout << "Rendered in " << clock() - t << "ms\n";
}

void display() 
{
	glClearColor(1, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/*
	if (scry < h)
	{
		Ray ray = shootRay(scrx, scry, uaxis, vaxis, waxis);
		depth = 0;
		vec4 color = findColor(ray);
		setPixel(scrx, scry, color);
		scrx++;
		if (scrx >= w)
		{
			scrx = 0;
			scry++;
		}
		glutPostRedisplay();
	}
	*/
	glDrawPixels(w, h, GL_RGB, GL_FLOAT, pixels);

	glutSwapBuffers();
}
