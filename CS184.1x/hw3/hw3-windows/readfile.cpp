/*****************************************************************************/
/* This is the program skeleton for homework 2 in CS 184 by Ravi Ramamoorthi */
/* Extends HW 1 to deal with shading, more transforms and multiple objects   */
/*****************************************************************************/

/*****************************************************************************/
// This file is readfile.cpp.  It includes helper functions for matrix 
// transformations for a stack (matransform) and to rightmultiply the 
// top of a stack.  These functions are given to aid in setting up the 
// transformations properly, and to use glm functions in the right way.  
// Their use is optional in your program.  
  

// The functions readvals and readfile do basic parsing.  You can of course 
// rewrite the parser as you wish, but we think this basic form might be 
// useful to you.  It is a very simple parser.

// Please fill in parts that say YOUR CODE FOR HW 2 HERE. 
// Read the other parts to get a context of what is going on. 
  
/*****************************************************************************/

// Basic includes to get this file to work.  
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <deque>
#include <stack>
#include <vector>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Transform.h" 
#include "variables.h" 
#include "readfile.h"
#include "Object.h"

using namespace std;

// Function to read the input data values
// Use is optional, but should be very helpful in parsing.  
bool readvals(stringstream &s, const int numvals, GLfloat* values) 
{
    for (int i = 0; i < numvals; i++) {
        s >> values[i]; 
        if (s.fail()) {
            cout << "Failed reading value " << i << " will skip\n"; 
            return false;
        }
    }
    return true; 
}

void readfile(const char* filename) 
{
    string str, cmd; 
    ifstream in;
    in.open(filename); 
    if (in.is_open()) {

        // I need to implement a matrix stack to store transforms.  
        // This is done using standard STL Templates 
        stack <mat4> transfstack; 
        transfstack.push(mat4(1.0));  // identity

		//objects = new vector<Object*>;
		vec3 attenuation = vec3(1.0f, 0.0f, 0.0f);
		vec3* vertices;
		int vertexIndex = 0;
		refractivity = 0.0f;
		maxdepth = 5;
		output = std::string(filename);
		output.replace(output.end() - 4, output.end(), "png");
		bool blockcomment = false;

        getline (in, str); 
        while (in) {
			if ((str.find_first_not_of(" \t\r\n") != string::npos)) {
                // Ruled out comment and blank lines 

                stringstream s(str);
                s >> cmd; 
                GLfloat values[10]; // Position and color for light, colors for others
                                    // Up to 10 params for cameras.  
                bool validinput; // Validity of input 

				if (cmd == "##") {
					blockcomment = !blockcomment;
				}
				if (blockcomment || str[0] == '#') {
					getline (in, str); 
					continue;
				}

				if (cmd== "output") {
					s >> output;
				}
				else if (cmd == "attenuation") {
                    validinput = readvals(s, 3, values); 
                    if (validinput) {
						attenuation = vec3(values[0], values[1], values[2]);
                     }
				}
                // Process the light, add it to database.
                // Lighting Command
                else if (cmd == "directional" || cmd == "point") {
                    if (numused == numLights) { // No more Lights 
                        cerr << "Reached Maximum Number of Lights " << numused << " Will ignore further lights\n";
                    } else {
                        validinput = readvals(s, 6, values); // Position/color for lts.
                        if (validinput) {
							lights[numused].pos = vec4(values[0], values[1], values[2], cmd == "directional" ? 0.0 : 1.0);
							lights[numused].color = vec4(values[3], values[4], values[5], 1.0);
							lights[numused].attenuation = attenuation;
							if (cmd == "directional") {
								lights[numused].dir = glm::normalize(vec3(lights[numused].pos));
							}
                            ++numused; 
                        }
                    }
                }

                // Material Commands 
                // Ambient, diffuse, specular, shininess properties for each object.
                // Filling this in is pretty straightforward, so I've left it in 
                // the skeleton, also as a hint of how to do the more complex ones.
                // Note that no transforms/stacks are applied to the colors. 

                else if (cmd == "ambient") {
                    validinput = readvals(s, 3, values); // colors 
                    if (validinput) {
						ambient = vec4(values[0], values[1], values[2], 1.0);
                    }
                } else if (cmd == "diffuse") {
                    validinput = readvals(s, 3, values); 
                    if (validinput) {
						diffuse = vec4(values[0], values[1], values[2], 1.0);
                     }
                } else if (cmd == "specular") {
                    validinput = readvals(s, 3, values); 
                    if (validinput) {
						specular = vec4(values[0], values[1], values[2], 1.0);
                    }
                } else if (cmd == "emission") {
                    validinput = readvals(s, 3, values); 
                    if (validinput) {
						emission = vec4(values[0], values[1], values[2], 1.0);
                    }
                } else if (cmd == "shininess") {
                    validinput = readvals(s, 1, values); 
                    if (validinput) {
                        shininess = values[0]; 
                    }
                } else if (cmd == "refractivity") {
                    validinput = readvals(s, 1, values); 
                    if (validinput) {
						refractivity = values[0];
                    }
                } else if (cmd == "size") {
                    validinput = readvals(s,2,values); 
                    if (validinput) { 
                        w = (int) values[0]; h = (int) values[1]; 
                    } 
				} else if (cmd == "maxdepth") {
					validinput = readvals(s,1,values);
					if (validinput) {
						maxdepth = (int) values[0];
					}
                } else if (cmd == "camera") {
                    validinput = readvals(s,10,values); // 10 values eye cen up fov
                    if (validinput) {

                        // YOUR CODE FOR HW 2 HERE
                        // Use all of values[0...9]
                        // You may need to use the upvector fn in Transform.cpp
                        // to set up correctly. 
                        // Set eyeinit upinit center fovy in variables.h 
						eyeinit = vec3(values[0], values[1], values[2]);
						center = vec3(values[3], values[4], values[5]);
						upinit = vec3(values[6], values[7], values[8]);
						fovy = values[9];
                    }
                } else if (cmd == "maxverts") {
                    validinput = readvals(s,1,values);
					if (validinput) {
						vertices = new vec3[(int) values[0]];
						vertexIndex = 0;
					}
				} else if (cmd == "vertex") {
                    validinput = readvals(s,3,values);
					if (validinput) {
						vertices[vertexIndex] = vec3(values[0], values[1], values[2]);
						++vertexIndex;
					}
				}

                // I've left the code for loading objects in the skeleton, so 
                // you can get a sense of how this works.  
                // Also look at demo.txt to get a sense of why things are done this way.
                else if (cmd == "sphere" || cmd == "tri") {
					if (cmd == "sphere") {
						validinput = readvals(s, 4, values); 
					} else{
						validinput = readvals(s, 3, values);
					}
                    if (validinput) {
						Object* obj;
                        // Set the object's type
                        if (cmd == "sphere") {
                            obj = new Sphere(vec3(values[0], values[1], values[2]), values[3]);
                        } else if (cmd == "tri") {
							vec3 v[3];
							for (int i = 0; i < 3; i++) {
								v[i] = vertices[(int) values[i]];
							}
                            obj = new Triangle(v);
                        } else {
							cerr << "Invalid object type:" << cmd << std::endl;
							continue;
						}

                        // Set the object's light properties
                        obj->ambient = ambient; 
                        obj->diffuse = diffuse; 
                        obj->specular = specular; 
                        obj->emission = emission;
						obj->shininess = shininess;
						obj->refractivity = refractivity;

                        // Set the object's transform
                        obj->transform = transfstack.top(); 

						objects.push_back(obj);
                    }
                    ++numobjects; 
                }

                else if (cmd == "translate") {
                    validinput = readvals(s,3,values); 
                    if (validinput) {

                        // YOUR CODE FOR HW 2 HERE.  
                        // Think about how the transformation stack is affected
                        // You might want to use helper functions on top of file. 
                        // Also keep in mind what order your matrix is!
						mat4 top = transfstack.top();
						transfstack.pop();
						transfstack.push(
							//glm::translate(top, vec3((values[0], values[1], values[2]))));
							top * Transform::translate(values[0], values[1], values[2]));
                    }
                }
                else if (cmd == "scale") {
                    validinput = readvals(s,3,values); 
                    if (validinput) {

                        // YOUR CODE FOR HW 2 HERE.  
                        // Think about how the transformation stack is affected
                        // You might want to use helper functions on top of file.  
                        // Also keep in mind what order your matrix is!
						mat4 top = transfstack.top();
						transfstack.pop();
						transfstack.push(
							//glm::scale(top, vec3((values[0], values[1], values[2]))));
							top * Transform::scale(values[0], values[1], values[2]));
                    }
                }
                else if (cmd == "rotate") {
                    validinput = readvals(s,4,values); 
                    if (validinput) {

                        // YOUR CODE FOR HW 2 HERE. 
                        // values[0..2] are the axis, values[3] is the angle.  
                        // You may want to normalize the axis (or in Transform::rotate)
                        // See how the stack is affected, as above.  
                        // Note that rotate returns a mat3. 
                        // Also keep in mind what order your matrix is!
						mat4 top = transfstack.top();
						transfstack.pop();
						mat3 mrot = Transform::rotate(values[3], vec3(values[0], values[1], values[2]));
						transfstack.push(
							//glm::rotate(top, values[3], vec3(values[0], values[1], values[2])));
							top * mat4(mrot));
                    }
                }

                // I include the basic push/pop code for matrix stacks
                else if (cmd == "pushTransform") {
                    transfstack.push(transfstack.top()); 
                } else if (cmd == "popTransform") {
                    if (transfstack.size() <= 1) {
                        cerr << "Stack has no elements.  Cannot Pop\n"; 
                    } else {
                        transfstack.pop(); 
                    }
                }

                else {
                    cerr << "Unknown Command: " << cmd << " Skipping \n"; 
                }
            }
            getline (in, str); 
        }

        // Set up initial position for eye, up and amount
        // As well as booleans 

        eye = eyeinit; 
        up = upinit; 
        amount = 5;
        sx = sy = 1.0;  // keyboard controlled scales in x and y 
        tx = ty = 0.0;  // keyboard controllled translation in x and y  
        useGlu = false; // don't use the glu perspective/lookat fns

        glEnable(GL_DEPTH_TEST);
    } else {
        cerr << "Unable to Open Input Data File " << filename << "\n"; 
        throw 2; 
    }
}
