package
{
	import flash.Lib;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.AccelerometerEvent;
	import flash.events.Event;
	import flash.events.GestureEvent;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.events.TransformGestureEvent;
	import flash.filesystem.File;
	import flash.globalization.StringTools;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.sensors.Accelerometer;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.ByteArray;
	
	import haxe.io.Bytes;
	
	import org.xiph.fogg.Buffer;
	import org.xiph.fogg.Page;
	import org.xiph.foggy.Demuxer;
	import org.xiph.fvorbis.FuncFloor;
	import org.xiph.fvorbis.FuncMapping;
	import org.xiph.fvorbis.FuncResidue;
	import org.xiph.fvorbis.FuncTime;
	import org.xiph.fvorbis.Info;

//	[SWF(width="640", height="480", backgroundColor="#000000")]
	public class HelloMobile extends MovieClip
	{
		private var tf:TextField = new TextField();
		
		public function HelloMobile()
		{
			super();
			
			// 支持 autoOrient
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			tf.background = true;
			tf.backgroundColor = 0xbbbbff;
			var format:TextFormat = tf.defaultTextFormat;
			format.size = 32;
			tf.defaultTextFormat = format;
			tf.setTextFormat(format);
			tf.type = TextFieldType.DYNAMIC;
			addChild(tf);
			
			testFlaUI();
			
		}
		
		private function testFlaUI():void
		{
			var testPanel:TestPanel = new TestPanel();
			testPanel.btnOk.addEventListener(MouseEvent.CLICK, function(evt:MouseEvent):void
			{
				testPanel.txtHello.text = "Fuck you man, fuck u!";
			});
			addChild(testPanel);
		}
		
		private function testAccelerometer():void
		{
			stage.frameRate = 1;
			if (Accelerometer.isSupported)
			{
				var acc:Accelerometer = new Accelerometer();
				acc.addEventListener(AccelerometerEvent.UPDATE, function(evt:AccelerometerEvent):void
				{
					tf.text = "\nx: " + evt.accelerationX + "\ny: " + evt.accelerationY + "\nz: " + evt.accelerationZ;
					trace(tf.text);
					tf.width = tf.textWidth + 5;
					tf.height = tf.textHeight + 5;
				});
			}
		}
		
		private function testOgg():void
		{
//			addChild(new Stats());

			Lib.current = this;
			Buffer._s_init();
			Page.__static_init__();
			FuncFloor._s_init();
			FuncMapping._s_init();
			FuncResidue._s_init();
			FuncTime._s_init();
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			urlLoader.addEventListener(Event.COMPLETE, function(evt:*):void{
				var ogg:OGGSound = new OGGSound();
				ogg.loadBytes(urlLoader.data as ByteArray);
				ogg.setVolume(1);
				ogg.play();
				ogg.setVolume(1);
			});
			urlLoader.load(new URLRequest("Song8.ogg"));
			
//			var ogg:OGGSound = new OGGSound();
//			ogg.load(new URLRequest("Song8.ogg"));
//			ogg.setVolume(1);
//			ogg.play();
//
		}
		
		private function testCapabilities():void
		{
			tf.text = "Stage size: " + stage.stageWidth + "x" + stage.stageHeight + "\n"
				+ "Screen res: " + Capabilities.screenResolutionX + "x" + Capabilities.screenResolutionY + "\n"
				+ "DPI: " + Capabilities.screenDPI + "\n"
				+ "App storage at: " + File.applicationStorageDirectory.nativePath + "\n"
				+ "Documents at: " + File.documentsDirectory.nativePath;
			tf.width = tf.textWidth + 5;
			tf.height = tf.textHeight + 5;
		}
		
		private function testGestures():void
		{
			if (Multitouch.supportsGestureEvents)
			{
				trace(Multitouch.supportedGestures);
//				Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
//				sprite.addEventListener(TouchEvent.TOUCH_BEGIN, function(evt:TouchEvent):void
//				{
//					sprite.startTouchDrag(evt.touchPointID);
//				});
//				sprite.addEventListener(TouchEvent.TOUCH_END, function(evt:TouchEvent):void
//				{
//					sprite.stopTouchDrag(evt.touchPointID);
//				});
				
				Multitouch.inputMode = MultitouchInputMode.GESTURE;
				tf.addEventListener(TransformGestureEvent.GESTURE_ZOOM, function(evt:TransformGestureEvent):void
				{
					tf.scaleX *= evt.scaleX;
					tf.scaleY *= evt.scaleY;
				});
			}
			else
			{
				trace("Multitouch unsupported!");
			}

		}
		
		private function testGraphic():void
		{
			var sprite:Sprite = new Sprite();
			with (sprite.graphics)
			{
				beginFill(0xff0000);
				drawCircle(200, 200, 50);
				endFill();
			}
			addChild(sprite);
		}
	}
}