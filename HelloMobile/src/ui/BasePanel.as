package ui
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class BasePanel extends Sprite
	{
		public var btnClose:DisplayObject;
		
		public function BasePanel()
		{
			btnClose.addEventListener(MouseEvent.CLICK, handleClose);
		}
		
		protected function handleClose(evt:MouseEvent):void
		{
			if (parent != null)
				parent.removeChild(this);
		}
	}
}