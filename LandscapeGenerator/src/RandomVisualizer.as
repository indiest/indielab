package  
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import me.sangtian.common.util.Random;
	
	/**
	 * ...
	 * @author 
	 */
	[SWF(width="800", height="600", frameRate="30", backgroundColor="0xffffff")]
	public class RandomVisualizer extends Sprite 
	{
		private var _textFactor0:TextField = new TextField();
		private var _textFactor1:TextField = new TextField();
		private var _textFactor2:TextField = new TextField();
		private var _textGapX:TextField = new TextField();
		private var _buttonRegen:Sprite = new Sprite();
		private var _shape:Shape = new Shape();
		
		public function RandomVisualizer() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			_textFactor0.text = "100";
			_textFactor0.type = TextFieldType.INPUT;
			_textFactor0.x = 720;
			_textFactor0.y = 20;
			addChild(_textFactor0);
			
			_textFactor1.text = "0.1";
			_textFactor1.type = TextFieldType.INPUT;
			_textFactor1.x = 720;
			_textFactor1.y = 40;
			addChild(_textFactor1);
			
			_textFactor2.text = "0.01";
			_textFactor2.type = TextFieldType.INPUT;
			_textFactor2.x = 720;
			_textFactor2.y = 60;
			addChild(_textFactor2);
			
			_textGapX.text = "1";
			_textGapX.type = TextFieldType.INPUT;
			_textGapX.x = 720;
			_textGapX.y = 80;
			addChild(_textGapX);
			
			_buttonRegen.graphics.beginFill(0xcfcfcf);
			_buttonRegen.graphics.drawRoundRect(0, 0, 80, 30, 5);
			_buttonRegen.graphics.endFill();
			_buttonRegen.x = 720;
			_buttonRegen.y = 100;
			addChild(_buttonRegen);
			_buttonRegen.addEventListener(MouseEvent.CLICK, handleClick);
			
			_shape.x = 0;
			_shape.y = stage.stageHeight >> 1;
			addChild(_shape);
			generate();
		}
		
		private function handleClick(e:MouseEvent):void 
		{
			generate();
		}
		
		private function generate():void 
		{
			_shape.graphics.clear();
			var x:int;
			var factor0:Number = Number(_textFactor0.text);
			var factor1:Number = Number(_textFactor1.text);
			var factor2:Number = Number(_textFactor2.text);
			var value0:Number = 0;
			var value1:Number = 0;
			var value2:Number = 0;
			var dx:int = int(_textGapX.text);
			for (x = 0; x < 700; x += dx)
			{
				_shape.graphics.moveTo(x, value0);
				value0 = Random.rangeNumber( -factor0, factor0);
				_shape.graphics.lineStyle(1, 0xff0000);
				_shape.graphics.lineTo(x + dx, value0);
				
				_shape.graphics.moveTo(x, value1);
				value1 = value1 + value0 * factor1;
				_shape.graphics.lineStyle(1, 0x00ff00);
				_shape.graphics.lineTo(x + dx, value1);
				
				_shape.graphics.moveTo(x, value2);
				value2 = value2 + value1 * factor2;
				_shape.graphics.lineStyle(1, 0x0000ff);
				_shape.graphics.lineTo(x + dx, value2);
				
				//trace(value0, value1, value2);
			}
			
		}
	}

}