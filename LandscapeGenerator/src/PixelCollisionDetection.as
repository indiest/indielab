package
{
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.geom.Transform;
	
	public class PixelCollisionDetection
	{
		private const identity: Matrix = new Matrix();
		
		private var collisionMask: BitmapData;
		private var object: DisplayObject;
		private var objectMask: BitmapData;
		
		private var difference: BitmapData;
		
		public function PixelCollisionDetection( collisionMask: BitmapData, object: DisplayObject, objectMask: BitmapData )
		{
			this.collisionMask = collisionMask;
			this.object = object;
			this.objectMask = objectMask;
			
			difference = new BitmapData( collisionMask.width, collisionMask.height, false, 0 );
		}
		
		public function getOverlapRegion(): Rectangle
		{
			var objectTransform: Transform = object.transform;
			var objectPixelBounds: Rectangle = objectTransform.pixelBounds;
			
			difference.fillRect( difference.rect, 0 );
			difference.copyPixels( collisionMask, objectPixelBounds, objectPixelBounds.topLeft );
			difference.draw( object, objectTransform.matrix, null, BlendMode.SUBTRACT );
			difference.draw( collisionMask, identity, null, BlendMode.DIFFERENCE, objectPixelBounds, false );
			
			return difference.getColorBoundsRect( 0xffffff, 0, false );
		}
	}
}









//-- only for non rotated shapes (2 bitmapdata)
//differenceMap.copyPixels
//(
//	collisionMask, collisionObjectRectangle, collisionObjectRectangle.topLeft, collisionObject, origin, false
//);