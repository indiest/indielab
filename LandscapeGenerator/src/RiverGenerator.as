package  
{
	import flash.display.BitmapData;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	
	/**
	 * ...
	 * @author 
	 */
	public class RiverGenerator 
	{
		public var waterColor:uint = 0xff00ccff;
		public var bankColor:uint =  0xff111111;
		public var riverWidthMin:int = 120;
		public var riverWidthMax:int = 240;
		
		public function RiverGenerator() 
		{
			
		}
		
		public function generateToBitmapData(bd:BitmapData):void
		{
			bd.lock();
			var bankWidthMin:Number = (bd.width - riverWidthMax) >> 1;
			var bankWidthMax:Number = (bd.width - riverWidthMin) >> 1;

			var bankRandomSpeed:Number = 0;
			var bankWidth:Number = (bd.width - riverWidthMax) >> 1;
			var riverRandomSpeed:Number = 0;
			var riverWidth:Number = Random.rangeNumber(riverWidthMin, riverWidthMax);
			for (var y:int = 0; y < bd.height; y++)
			{
				bankRandomSpeed += Random.rangeNumber( -3, 3);
				bankWidth += bankRandomSpeed * 0.01;
				if (bankWidth < bankWidthMin || bankWidth > bankWidthMax)
				{
					bankRandomSpeed = bankRandomSpeed * 0.1;//0;// 
					//riverWidth = Random.rangeNumber(widthMin, widthMax);
				}

				riverRandomSpeed += Random.rangeNumber( -2, 2);
				riverWidth += riverRandomSpeed * 0.01;
				if (riverWidth < riverWidthMin || riverWidth > riverWidthMax)
				{
					riverRandomSpeed = riverRandomSpeed * 0.1;//0;
				}
				
				//trace(bankWidth, riverWidth);
				for (var x:int = 0; x < bd.width; x++)
				{
					if (x >= bankWidth && x < bankWidth + riverWidth)
						bd.setPixel32(x, y, waterColor);
					else
						bd.setPixel32(x, y, bankColor);
				}
			}
			bd.unlock();
		}
		
	}

}