package  
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.Label;
	import com.bit101.components.List;
	import com.bit101.components.Slider;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.display.BlendMode;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.Point;
	import me.sangtian.common.ui.RadioButtonGroup;
	import me.sangtian.common.ui.UIUtil;
	import me.sangtian.common.util.ArrayUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	import me.sangtian.gengraphy.CloudsGenerator;
	
	/**
	 * ...
	 * @author 
	 */
    [SWF(width="800", height="600", frameRate="30", backgroundColor="0xffffff")]
	public class CloudGeneratorTest extends Sprite 
	{
		private var _bg:Bitmap;
        private var _bitmapClouds:Bitmap;
		private var _cg:CloudsGenerator;
         
        [Embed(source="/../assets/mountains.jpg")]
        private var Picture:Class;
		private var _blendModes:Vector.<String> = ObjectUtil.getConstantNames(BlendMode);
		private var _renderOnBg:Boolean;
		
		public function CloudGeneratorTest() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
             
            // Background Picture
			_bg = new Picture();
            addChild(_bg);
             
            // Clouds
            _bitmapClouds = new Bitmap(new BitmapData(800, 200, true, 0));
            addChild(_bitmapClouds);
			_cg = new CloudsGenerator();
			//_cg.grayScale = false;
            
			UIUtil.addPropertySlider(this, 10, 400, _cg, "cloudMaxWidth", 0, stage.width);
			UIUtil.addPropertySlider(this, 10, 420, _cg, "cloudMaxHeight", 0, stage.height);
			UIUtil.addPropertySlider(this, 10, 440, _cg, "numOctaves", 0, 10);
			UIUtil.addPropertyCheckbox(this, 10, 460, _cg, "grayScale");
			UIUtil.addPropertyCheckbox(this, 10, 480, this, "renderOnBg");
			var list:List = new List(this, 10, 600, ArrayUtil.fromIterable(_blendModes));
			list.addEventListener(Event.SELECT, list_select);
			
            stage.addEventListener(Event.ENTER_FRAME, update);
        }
		
		private function list_select(e:Event):void 
		{
			var list:List = e.currentTarget as List;
			_bitmapClouds.blendMode = BlendMode[list.selectedItem];
		}

        public function update(e:Event):void
        {
            _cg.offset.x -= 2;
			if ( _cg.offset.x % 10 != 0)
				return;
			_cg.generateToBitmapData(renderOnBg ? _bg.bitmapData : _bitmapClouds.bitmapData);
        }
		
		public function get renderOnBg():Boolean 
		{
			return _renderOnBg;
		}
		
		public function set renderOnBg(value:Boolean):void 
		{
			_renderOnBg = value;
			_bitmapClouds.visible = !value;
		}
	}

}