package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Transform;
	import flash.utils.getTimer;
	import me.sangtian.common.Input;
	import me.sangtian.common.ProcedureProfiler;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.MathUtil;
	
	/**
	 * ...
	 * @author 
	 */
	[SWF(width="800", height="640", frameRate="30", backgroundColor="0xffffff")]
	public class RiverGeneratorTest extends Sprite 
	{
		private var _rg:RiverGenerator = new RiverGenerator();
		private var _bitmap:Bitmap = new Bitmap(new BitmapData(360, 640));
		private var _boatBitmap:Bitmap = new Bitmap(new BitmapData(40, 80, true, 0xffffff99));
		private var _boat:Sprite = new Sprite();

		private var _diff:BitmapData = new BitmapData(_bitmap.width, _bitmap.height, false, 0);
		private var _substract:BitmapData;
		
		private var _diffProfiler:ProcedureProfiler = new ProcedureProfiler();
		private var _hitTestProfiler:ProcedureProfiler = new ProcedureProfiler();

		public function RiverGeneratorTest()
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			addChild(new Bitmap(new BitmapData(360, 640, false, 0xff00ccff)));
			
			_rg.waterColor = 0;
			_rg.bankColor = 0xff111111;
			var time:int = getTimer();
			_rg.generateToBitmapData(_bitmap.bitmapData);
			trace(getTimer() - time);
			addChild(_bitmap);
			
			_boatBitmap.x = -_boatBitmap.width * 0.5;
			_boatBitmap.y = -_boatBitmap.height * 0.5;
			_boat.addChild(_boatBitmap);
			_boat.x = 180;
			_boat.y = 500;
			addChild(_boat);
			//_boat.startDrag(true);
			
			/*
			_substract = _diff.clone();
			var subBitmap:Bitmap = new Bitmap(_substract);
			subBitmap.x = 380;
			subBitmap.scaleX = subBitmap.scaleY = 0.5;
			addChild(subBitmap);
			var diffBitmap:Bitmap = new Bitmap(_diff);
			diffBitmap.x = 580;
			diffBitmap.scaleX = diffBitmap.scaleY = 0.5;
			addChild(diffBitmap);
			*/
			
			Input.initialize(stage);
			
			stage.addEventListener(MouseEvent.CLICK, onClick);
			stage.addEventListener(Event.ENTER_FRAME, onUpdate);
		}
		
		private var _boatVelocity:Point = new Point();
		private var _boatHitTestPoint:Point = new Point();
		private var _bitmapHitTestPoint:Point = new Point();
		
		private function onUpdate(e:Event):void 
		{
			Time.tick();
			Input.update();
			
			_boatVelocity.x = MathUtil.clamp(_boatVelocity.x + Input.getAxis(Input.AXIS_HORIZONTAL) * 100, -200, 200);
			_boatVelocity.y = MathUtil.clamp(_boatVelocity.y + Input.getAxis(Input.AXIS_VERTICAL) * 100, -100, 100);
			
			_boat.x += _boatVelocity.x * Time.deltaSecond;
			_boat.y += _boatVelocity.y * Time.deltaSecond;
			
			_diffProfiler.startTiming();
			var boatTransform:Transform = _boat.transform;
			var boatPixelBounds:Rectangle = boatTransform.pixelBounds;
			//boatPixelBounds.offset(_boatBitmap.x, _boatBitmap.y);
			//boatTransform.matrix.translate(_boatBitmap.x, _boatBitmap.y);
			_diff.fillRect(_diff.rect, 0);
			_diff.copyPixels(_bitmap.bitmapData, boatPixelBounds, boatPixelBounds.topLeft);
			_diff.draw(_boat, boatTransform.matrix, null, BlendMode.SUBTRACT);
			//_substract.copyPixels(_diff, _diff.rect, new Point());
			_diff.draw(_bitmap.bitmapData, null, null, BlendMode.DIFFERENCE, boatPixelBounds, false);
			var hitRect:Rectangle = _diff.getColorBoundsRect(0xffffff, 0, false);
			_diffProfiler.countTimes();
			if (!hitRect.isEmpty())
			{
				_boat.x += hitRect.width * MathUtil.sign(_boat.x - hitRect.x);
				//_boat.y += hitRect.height* MathUtil.sign(_boat.y - hitRect.y);
				//_boatVelocity.x = 10 * (_boat.x - (hitRect.x + hitRect.width * 0.5)) * hitRect.width;
				//_boatVelocity.y = 10 * (_boat.y - (hitRect.y + hitRect.height* 0.5)) * hitRect.height;
				//trace("Diff detected:", hitRect);
			}
			
			
			/*
			hitRect = PixelPerfectCollisionDetection.getCollisionRect(_bitmap, _boatBitmap, this, true)
			if (hitRect != null)
			{
				trace("Alpha detected:", hitRect);
			}
			*/
			
			
			_hitTestProfiler.startTiming();
			_boatHitTestPoint.setTo(_boat.x + _boatBitmap.x, _boat.y + _boatBitmap.y);
			var hit:Boolean = _boatBitmap.bitmapData.hitTest(
				_boatHitTestPoint, 1, 
				_bitmap, _bitmapHitTestPoint, 1);
			_hitTestProfiler.countTimes();
			
			if (hit)
			{
				//trace("HitTest detected");
				_boatVelocity.x = -_boatVelocity.x * 0.2;
				_boatVelocity.y = _boatVelocity.y * 0.5;
				Input.setAxis(Input.AXIS_HORIZONTAL, 0);
			}
			else
			{
				
			}
		}
		
		private function onClick(e:MouseEvent):void 
		{
			_boat.x = 180;
			_boat.y = 500;
			_rg.generateToBitmapData(_bitmap.bitmapData);
			trace("Diff:", _diffProfiler.getAverageMillisecond());
			trace("HitTest:", _hitTestProfiler.getAverageMillisecond());
		}
	}

}