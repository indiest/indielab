package 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.utils.getTimer;
	import me.sangtian.common.util.ColorUtil;
	import me.sangtian.gengraphy.MountainGenerator;
	
	/**
	 * ...
	 * @author 
	 */
    [SWF(width="800", height="600", frameRate="30", backgroundColor="0xD2B6A0")]
	public class MountainGeneratorTest extends Sprite 
	{
		private var mg:MountainGenerator;
		private var bitmap:Bitmap;
		
		private var _textTopColor:TextField = new TextField();
		private var _textBottomColor:TextField = new TextField();
		private var _textMinHeight:TextField = new TextField();
		private var _textMaxHeight:TextField = new TextField();
		private var _textSlope0:TextField = new TextField();
		private var _textSlope1:TextField = new TextField();
		private var _textTreeDensity:TextField = new TextField();
		private var _buttonRegen:Sprite = new Sprite();
		
		public function MountainGeneratorTest():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			mg = new MountainGenerator();
			bitmap = new Bitmap(new BitmapData(800, 600, true, 0));
			bitmap.y = stage.stageHeight - bitmap.height;
			addChild(bitmap);
			
			_textTopColor.text = "#716B6F";
			_textTopColor.type = TextFieldType.INPUT;
			_textTopColor.x = 20;
			_textTopColor.y = 20;
			addChild(_textTopColor);
			
			_textBottomColor.text = "#475465";
			_textBottomColor.type = TextFieldType.INPUT;
			_textBottomColor.x = 180;
			_textBottomColor.y = 20;
			addChild(_textBottomColor);
			
			_textMinHeight.text = "200";
			_textMinHeight.type = TextFieldType.INPUT;
			_textMinHeight.x = 20;
			_textMinHeight.y = 40;
			addChild(_textMinHeight);
			
			_textMaxHeight.text = "400";
			_textMaxHeight.type = TextFieldType.INPUT;
			_textMaxHeight.x = 180;
			_textMaxHeight.y = 40;
			addChild(_textMaxHeight);
			
			_textSlope0.text = "3.0";
			_textSlope0.type = TextFieldType.INPUT;
			_textSlope0.x = 20;
			_textSlope0.y = 60;
			addChild(_textSlope0);
			
			_textSlope1.text = "1.0";
			_textSlope1.type = TextFieldType.INPUT;
			_textSlope1.x = 180;
			_textSlope1.y = 60;
			addChild(_textSlope1);
			
			_textTreeDensity.text = "20";
			_textTreeDensity.type = TextFieldType.INPUT;
			_textTreeDensity.x = 20;
			_textTreeDensity.y = 80;
			addChild(_textTreeDensity);
			
			_buttonRegen.graphics.beginFill(0xcfcfcf);
			_buttonRegen.graphics.drawRoundRect(0, 0, 80, 30, 5);
			_buttonRegen.graphics.endFill();
			_buttonRegen.x = 700;
			_buttonRegen.y = 20;
			addChild(_buttonRegen);
			_buttonRegen.addEventListener(MouseEvent.CLICK, handleClick);
		}
		
		private function handleClick(e:MouseEvent):void 
		{
			generateMoutainFromSettings();
		}
		
		private function generateMoutainFromSettings():void
		{
			bitmap.bitmapData.fillRect(bitmap.bitmapData.rect, 0);
			mg.topColor = ColorUtil.setAlpha(ColorUtil.fromString(_textTopColor.text));
			mg.bottomColor = ColorUtil.setAlpha(ColorUtil.fromString(_textBottomColor.text));
			mg.maxHeight = uint(_textMaxHeight.text);
			mg.minHeight = uint(_textMinHeight.text);
			mg.slope0 = Number(_textSlope0.text);
			mg.slope1 = Number(_textSlope1.text);
			mg.treeDensity = uint(_textTreeDensity.text);
			mg.treeGenerator.heightMin = 30;
			mg.treeGenerator.heightMax = 40;
			mg.treeGenerator.thickness = 1;
			mg.generateToBitmapData(bitmap.bitmapData);
		}
		
	}
	
}