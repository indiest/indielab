package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.ContextMenu;
	import me.sangtian.common.util.MathUtil;
	
	/**
	 * ...
	 * @author 
	 */
    [SWF(width="800", height="600", frameRate="10", backgroundColor="0x000000")]
	public class FractalTest extends Sprite 
	{
		
		public function FractalTest() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			graphics.lineStyle(1, 0xffffff);
			//graphics.moveTo(_currentPos.x, _currentPos.y);
			//stage.addEventListener(Event.ENTER_FRAME, update);
			drawBranch(iterationTimes, 400, 300, initLength, -Math.PI * 0.5);
			drawBranch(iterationTimes, 400, 300, initLength, -Math.PI * 0.5 + radianInc * 2);
			drawBranch(iterationTimes, 400, 300, initLength, -Math.PI * 0.5 - radianInc * 2);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
		}
		
		private function onMouseWheel(e:MouseEvent):void 
		{
			scaleX += e.delta / 100;
			scaleY += e.delta / 100;
		}
		
		public var initLength:Number = 100;
		public var lengthRatio:Number = 0.618;
		public var radianInc:Number = MathUtil.angleToRadian(60);
		public var iterationTimes:uint = 12;

		/*
		private var _currentItr:uint = 1;
		private var _currentPos:Point = new Point(400, 300);
		private var _currentLen:Number = initLength;
		private var _currentRad:Number = 0;
		
		private function update(e:Event):void 
		{
			if (_currentItr > iterationTimes)
			{
				stage.removeEventListener(Event.ENTER_FRAME, update);
				return;
			}
			var x:Number = _currentPos.x + _currentLen * Math.cos(_currentRad);
			var y:Number = _currentPos.y + _currentLen * Math.sin(_currentRad);
			graphics.lineTo(x, y);
			
			_currentPos.x = x;
			_currentPos.y = y;
			_currentLen *= lengthRatio;
			_currentRad += radianInc;// / _currentItr;
			_currentItr++;
		}
		*/
		
		public function drawBranch(maxDepth:int, startX:Number, startY:Number, length:Number, radians:Number):void
		{
			if (maxDepth == 0)
				return;
			graphics.moveTo(startX, startY);
			startX = startX + length * Math.cos(radians);
			startY = startY + length * Math.sin(radians);
			graphics.lineTo(startX, startY);
			drawBranch(maxDepth - 1, startX, startY, length * lengthRatio, radians + radianInc);
			drawBranch(maxDepth - 1, startX, startY, length * lengthRatio, radians - radianInc);
		}
	}

}
import flash.display.Graphics;
import flash.geom.Point;

class Factal
{
	public var depth:int;
	public var startX:Number;
	public var startY:Number;
	public var length:Number;
	public var radians:Number;
	
	public function update(g:Graphics):void
	{
		for each(var branch:Factal in _branches)
		{
			branch.drawCurrent(g);
		}
	}
	
	public function drawCurrent(g:Graphics):void
	{
		g.moveTo(startX, startY);
		g.lineTo(startX + length * Math.cos(radians), startY + length * Math.sin(radians));
	}
	
	private var _branches:Vector.<Factal> = new Vector.<Factal>();
	
	public function create(depth:int, startX:Number, startY:Number, length:Number, radians:Number):Factal
	{
		var branch:Factal = new Factal();
		branch.depth = depth;
		branch.startX = startX;
		branch.startY = startY;
		branch.length = length;
		branch.radians = radians;
		_branches.push(branch);
		return branch;
	}
}