package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import me.sangtian.common.Input;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.gengraphy.CloudsGenerator;
	import me.sangtian.gengraphy.MountainGenerator;
	
	/**
	 * ...
	 * @author 
	 */
    [SWF(width="800", height="600", frameRate="30", backgroundColor="0xD2B6A0")]
	public class Main extends Sprite 
	{
		private var _mg:MountainGenerator = new MountainGenerator();
		private var _cg:CloudsGenerator = new CloudsGenerator();
		private var _bitmapMountainFar:Bitmap = new Bitmap(new BitmapData(800+400, 500, true, 0));
		private var _bitmapMountainMid:Bitmap = new Bitmap(new BitmapData(800+800, 400, true, 0));
		private var _bitmapMountainNear:Bitmap = new Bitmap(new BitmapData(800+1200, 200, true, 0));
		private var _bitmapCloudsMid:Bitmap = new Bitmap(new BitmapData(800, 200, true, 0));
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			generateMountains();
			generateClouds();
			
			_bitmapMountainFar.y = stage.stageHeight - _bitmapMountainFar.height;
            addChild(_bitmapMountainFar);
			
			_bitmapCloudsMid.y = 200;
			_bitmapCloudsMid.blendMode = BlendMode.MULTIPLY;
			addChild(_bitmapCloudsMid);
			
			_bitmapMountainMid.y = stage.stageHeight - _bitmapMountainMid.height;
			addChild(_bitmapMountainMid);
			
			_bitmapMountainNear.y = stage.stageHeight - _bitmapMountainNear.height;
			addChild(_bitmapMountainNear);
			
            stage.addEventListener(Event.ENTER_FRAME, update);
			Input.initialize(stage);
        }
         
		private var _scrollRect:Rectangle = new Rectangle(0, 0, 800, 600);
		private var _scrollX:Number = 0;
		
        public function update(e:Event):void
        {
			Time.tick();
			Input.update();
			_scrollX += Input.getAxis(Input.AXIS_HORIZONTAL);
			_scrollRect.x = _scrollX;
			_bitmapMountainFar.scrollRect = _scrollRect;
			_scrollRect.x = _scrollX * 2;
			_bitmapMountainMid.scrollRect = _scrollRect;
			_scrollRect.x = _scrollX * 3;
			_bitmapMountainNear.scrollRect = _scrollRect;
			
			generateClouds();
		}
		
		private function generateClouds():void
		{
			//DisplayObjectUtil.clearBitmap(_bitmapCloudsMid);
            _cg.offset.x -= 2;
			if ( _cg.offset.x % 10 != 0)
				return;
			_cg.generateToBitmapData(_bitmapCloudsMid.bitmapData);
		}
		
		private function generateMountains():void 
		{
			DisplayObjectUtil.clearBitmap(_bitmapMountainFar);
			_mg.topColor = 0xFF908583;
			_mg.bottomColor = 0xFF73747B;
			_mg.maxHeight = _bitmapMountainFar.height;
			_mg.minHeight = 300;
			_mg.treeDensity = 0;
			_mg.generateToBitmapData(_bitmapMountainFar.bitmapData);
			
			DisplayObjectUtil.clearBitmap(_bitmapMountainMid);
			_mg.topColor = 0xFF716B6F;
			_mg.bottomColor = 0xFF475465;
			_mg.maxHeight = _bitmapMountainMid.height;
			_mg.minHeight = 200;
			_mg.treeDensity = 0;
			_mg.generateToBitmapData(_bitmapMountainMid.bitmapData);
			
			DisplayObjectUtil.clearBitmap(_bitmapMountainNear);
			_mg.topColor = 0xFF424B5A;
			_mg.bottomColor = 0xFF3E4B5B;
			_mg.maxHeight = _bitmapMountainNear.height - 40;
			_mg.minHeight = 100;
			_mg.treeDensity = 20;
			_mg.treeGenerator.heightMin = 30;
			_mg.treeGenerator.heightMax = 40;
			_mg.treeGenerator.thickness = 1;
			DisplayObjectUtil.tint(_mg.treeGenerator, 0, _mg.topColor);
			_mg.generateToBitmapData(_bitmapMountainNear.bitmapData);
		}
		
	}

}