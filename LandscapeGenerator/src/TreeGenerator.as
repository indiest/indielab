package  
{
	import flash.display.Shape;
	import me.sangtian.common.util.Random;
	/**
	 * ...
	 * @author 
	 */
	public class TreeGenerator extends Shape
	{
		public var trunkLength:Number = 50;
		public var initLength:Number = 30;
		public var lengthRatioMin:Number = 0.318;
		public var lengthRatioMax:Number = 0.618;
		public var branchNumMin:uint = 3;
		public var branchNumMax:uint = 6;
		public var radianInc:Number = 0.618;
		public var iterationTimes:uint = 6;

		public function TreeGenerator() 
		{
			
		}
		
		private function drawBranch(depth:int, startX:Number, startY:Number, length:Number, radians:Number):void
		{
			if (depth == 0)
				return;
			graphics.moveTo(startX, startY);
			startX = startX + length * Math.cos(radians);
			startY = startY + length * Math.sin(radians);
			graphics.lineTo(startX, startY);
			//drawBranch(depth - 1, startX, startY, Random.rangeNumber(length * lengthRatioMin, length * lengthRatioMax), radians + radianInc * Math.random());
			//drawBranch(depth - 1, startX, startY, Random.rangeNumber(length * lengthRatioMin, length * lengthRatioMax), radians - radianInc * Math.random());
			drawBranches(Random.rangeInt(branchNumMin, branchNumMax), depth - 1, startX, startY, Random.rangeNumber(length * lengthRatioMin, length * lengthRatioMax), radians - radianInc, radians + radianInc);
		}
		
		private function drawBranches(num:uint, depth:int, startX:Number, startY:Number, length:Number, radiansMin:Number, radiansMax:Number):void
		{
			for (var i:int = 0; i < num; i++)
			{
				drawBranch(depth, startX, startY, length, Random.rangeNumber(radiansMin, radiansMax));
			}
		}
		
		private function drawTrunk():void 
		{			
			graphics.lineStyle(5);
			drawBranches(5, 1, 0, -trunkLength, trunkLength, Math.PI * 0.5 - 0.01, Math.PI * 0.5 + 0.01);
		}
		
		public function generate():void
		{
			graphics.clear();
			drawTrunk();
			
			graphics.lineStyle(1);
			drawBranches(
				Random.rangeInt(branchNumMin, branchNumMax),
				iterationTimes,
				0, -trunkLength,
				Random.rangeNumber(initLength * lengthRatioMin, initLength * lengthRatioMax),
				-Math.PI, 0
			);
		}
	}

}