package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author 
	 */
    [SWF(width="800", height="600", frameRate="30", backgroundColor="0xffffff")]
	public class TreeGeneratorTest extends Sprite 
	{
		private var _atg:AnimatedTreeGenerator = new AnimatedTreeGenerator();
		
		public function TreeGeneratorTest() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			_atg.x = 400;
			_atg.y = 500;
			_atg.heightMin = 30;
			_atg.heightMax = 40;
			_atg.thickness = 1;
			_atg.generate();
			addChild(_atg);
			
			stage.addEventListener(MouseEvent.CLICK, onClick);
			stage.addEventListener(Event.ENTER_FRAME, onUpdate);
		}
		
		private function onUpdate(e:Event):void 
		{
			_atg.update();
		}
		
		private function onClick(e:MouseEvent):void 
		{
			//_tg.generate();
			_atg.generate();
			_atg.finish();
		}
		
	}

}