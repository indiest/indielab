package  
{
	import adobe.utils.CustomActions;
	import Box2D.Dynamics.b2Body;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyBodyManager
	{
		private var _container:DisplayObjectContainer;
		private var _init:Boolean = false;
		public function get isInitialized():Boolean
		{
			return _init;
		}
		
		public function PhyBodyManager(container:DisplayObjectContainer) 
		{
			_container = container;
			//container.addEventListener(Event.ADDED_TO_STAGE, handleAdded);
		}
		
		public function initialize():void
		{
			for (var i:int = 0; i < _container.numChildren; i++)
			{
				var obj:DisplayObject = _container.getChildAt(i);
				trace("stage object:", obj);
				if (obj is PhyBody)
				{
					initPhyBody(obj as PhyBody);
				}
			}
			_init = true;
		}
		
		private function handleAdded(e:Event):void 
		{
			if (e.currentTarget is PhyBody)
			{
				var phyBody:PhyBody = e.currentTarget as PhyBody;
				initPhyBody(phyBody);
			}
		}
		
		private function initPhyBody(phyBody:PhyBody):void 
		{
			var body:b2Body = Global.world.CreateBody(phyBody.createBodyDef());
			trace("shape count:", phyBody.numChildren);
			for (var i:int = 0; i < phyBody.numChildren; i++)
			{
				var obj:DisplayObject = phyBody.getChildAt(i);
				if (obj is PhyShape)
				{
					var phyShape:PhyShape = obj as PhyShape;
					body.CreateShape(phyShape.createShapeDef());
				}
			}
			body.SetMassFromShapes();
			trace("mass:", body.GetMass());
			//phyBody.parent.removeChild(phyBody);
			phyBody.visible = false;
		}
		
		
		
	}

}