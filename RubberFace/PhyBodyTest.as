﻿package  
{
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Collision.Shapes.b2ShapeDef;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2World;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyBodyTest extends Sprite
	{
		private var _bodyMgr:PhyBodyManager;
		
		public function PhyBodyTest() 
		{
			//trace(this.numChildren);
			var universeSize:b2AABB = new b2AABB();
			universeSize.lowerBound.Set( -100, -100);
			universeSize.upperBound.Set(100, 100);
			Global.world = new b2World(universeSize, new b2Vec2(0, 9.8), false);
			
			var draw:b2DebugDraw = new b2DebugDraw();
			var drawSprite:Sprite = new Sprite();
			addChild(drawSprite);
			draw.m_sprite = drawSprite;
			draw.m_drawScale = Global.RATIO;
			draw.m_drawFlags = b2DebugDraw.e_shapeBit;
			Global.world.SetDebugDraw(draw);

			_bodyMgr = new PhyBodyManager(this);
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			if (!_bodyMgr.isInitialized)
			{
				_bodyMgr.initialize();
			}
			Global.world.Step(1.0 / stage.frameRate, 10);
		}
		
		
		
	}

}