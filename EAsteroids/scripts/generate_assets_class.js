/**
 * This NodeJS script can generate Assets.as class by scanning specified directory.
 * Dependent libraries: Eco
 *
 * @author Nicolas
 */
 
var fs = require('fs');
var path = require('path');
var eco = require('eco');

var assetsRoot = '../assets';
var codeRoot = '../src';
// assets and directories that are not included
var ignoreList = [
	'.svn',
	'.wav'
];
// assets with special embedding options
var embedOptions = {
	'.json': 'mimeType="application/octet-stream"',
	'.csv': 'mimeType="application/octet-stream"'
}
var assetsList = [];

// recursively scan directories and put assets' path into the list
function generateAssetsList(dir) {
	// filter unnecessary files
	for (var i = 0; i < ignoreList.length; i++) {
		if (dir.match(ignoreList[i]))
			return;
	}
	
	var files;
	try {
		files = fs.readdirSync(path.resolve(assetsRoot, dir));
	} catch(err) {
		// not a directory
		var item = {
			name: dir.replace(path.extname(dir), '').replace('\\', '_').toUpperCase(),
			path: dir.replace('\\', '/')
		};
		for (var key in embedOptions) {
			if (dir.match(key)) {
				item.embedOptions = embedOptions[key];
				break;
			}
		}
		assetsList.push(item);
		return;
	}
	for (var i = 0; i < files.length; i++) {
		generateAssetsList(path.join(dir, files[i]));
	}
}

(function() {
	generateAssetsList(".");
//	console.log(assetsList);
	var template = fs.readFileSync('Assets.as.eco', 'utf8');
	var code = eco.render(template, {items: assetsList});
	var classPath = path.resolve(codeRoot, 'Assets.as');
//	console.log(code);
	try {
		// backup the class if it's existing
		fs.renameSync(classPath, classPath + '.bak');
		console.info('Backup to Assets.as.bak');
	} catch(err) {}
	fs.writeFileSync(classPath, code);
	console.info('Class generated.');
})();