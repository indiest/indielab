package  
{
	import org.flixel.FlxEmitter;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author ...
	 */
	public class SpaceShip extends GameObject 
	{
		protected var _emitter:FlxEmitter;
		
		public function get radians():Number
		{
			return angle * Math.PI / 180;
		}
		
		public function get radius():Number
		{
			return Math.max(frameWidth, frameHeight) * 0.5;
		}
		
		public function SpaceShip() 
		{
			
		}
		
		override public function update():void 
		{
			super.update();
		}
		
		public function shoot():void
		{
			var projectile:Projectile = state.spawnProjectile();
			initProjectile(projectile);
			FlxG.play(Assets.SOUNDS_SHOOT);
		}
		
		protected function initProjectile(projectile:Projectile):void 
		{
			projectile.x = x + frameWidth * 0.5 + radius * Math.cos(radians);
			projectile.y = y + frameHeight * 0.5 + radius * Math.sin(radians);
			projectile.velocity.x = velocity.x + Projectile.SPEED * Math.cos(radians);
			projectile.velocity.y = velocity.y + Projectile.SPEED * Math.sin(radians);
		}
		
		override public function onCollision(other:GameObject):void 
		{
			super.onCollision(other);
			explode();
		}
		
		public function explode():void
		{
			// show particles
			if (_emitter == null)
			{
				_emitter = new FlxEmitter();
				_emitter.particleClass = ExplosionParticle;
				_emitter.makeParticles(Assets.IMAGES_VIPER_EXPLOSION, 50, 0, false, 0);
				state.add(_emitter);
			}
			_emitter.x = x;
			_emitter.y = y;
			_emitter.start(true, 1);
			FlxG.play(Assets.SOUNDS_VIPER_EXPLOSION);
			kill();
		}
	}

}