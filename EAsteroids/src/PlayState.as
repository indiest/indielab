package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxState;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class PlayState extends FlxState 
	{
		public static const VIPER_SPAWN_X:Number = 160;
		public static const VIPER_SPAWN_Y:Number = 120;
		public static const REVIVE_COUNTDOWN:Number = 2;

		private var _viper:Viper;
		private var _asteriods:FlxGroup;
		private var _enemies:FlxGroup;
		private var _projectiles:FlxGroup;
		private var _uiLayer:UILayer;
		
		// asteroids, ships
		private var _collideGroupAS:FlxGroup;
		// projectiles, ships
		private var _collideGroupPS:FlxGroup;
		
		private var _reviveRemaining:Number;
		
		public function get viper():Viper
		{
			return _viper;
		}
		
		public function get ui():UILayer 
		{
			return _uiLayer;
		}
		
		public function resetReviveTiming():void
		{
			_reviveRemaining = REVIVE_COUNTDOWN
		}
		
		public function PlayState() 
		{
			_viper = new Viper();
			_asteriods = new FlxGroup();
			_enemies = new FlxGroup();
			_projectiles = new FlxGroup();
			_uiLayer = new UILayer();
			
			_collideGroupAS = new FlxGroup();
			_collideGroupPS = new FlxGroup();
		}
		
		override public function create():void 
		{
			super.create();
			_viper.x = VIPER_SPAWN_X;
			_viper.y = VIPER_SPAWN_Y;
			_uiLayer.setLives(_viper.lives);
			_uiLayer.setScore(_viper.score);
			
			add(_asteriods);
			add(_viper);
			add(_enemies);
			add(_projectiles);
			add(_uiLayer);
			
			_collideGroupAS.add(_asteriods);
			_collideGroupAS.add(_viper);
			_collideGroupAS.add(_enemies);
			
			_collideGroupPS.add(_projectiles);
			_collideGroupPS.add(_viper);
			_collideGroupPS.add(_enemies);
			
			for (var i:uint = 0; i < 6; i++)
				spawnAsteroid();
		}
		
		public function spawnAsteroid():Asteroid
		{
			var asteroid:Asteroid = _asteriods.recycle(Asteroid) as Asteroid;
			asteroid.revive();
			
			// randomize position and velocity
			asteroid.scaleXY = 1 + Math.random();
			var xFactor:uint = Util.randomBit();
			var yFactor:uint = Util.randomBit();
			asteroid.x = xFactor ? FlxG.width : -asteroid.width;
			asteroid.velocity.x = Asteroid.MAX_SPEED * Math.random() * (xFactor ? -1 : 1);
			asteroid.y = yFactor ? FlxG.height : -asteroid.height;
			asteroid.velocity.y = Asteroid.MAX_SPEED * Math.random() * (yFactor ? -1 : 1);
			//trace(asteroid.x, asteroid.y, asteroid.velocity.x, asteroid.velocity.y);
			asteroid.angle = Math.random() * 360;
			asteroid.angularVelocity = Math.random() * 30;
			return asteroid;
		}
		
		public function spawnProjectile():Projectile
		{
			var projectile:Projectile = _projectiles.recycle(Projectile) as Projectile;
			projectile.revive();
			return projectile;
		}
		
		public function spawnEnemy():Enemy
		{
			var enemy:Enemy = _enemies.recycle(Enemy) as Enemy;
			enemy.revive();
			enemy.x = -FlxG.width;
			enemy.y = Math.random() * FlxG.height;
			enemy.velocity.make(100, 0);
			return enemy;
		}
		
		override public function update():void 
		{
			if (!active)
				return;
			super.update();
			//FlxG.collide(_viper, this, onViperCollision);
			//FlxG.collide(_asteriods, _projectiles, onAsteroidExplode);
			FlxG.collide(_collideGroupAS, _collideGroupPS, onCollision);

			if (!_viper.alive)
			{
				if (_viper.lives > 0)
				{
					// revive timing
					_reviveRemaining -= FlxG.elapsed;
					if (_reviveRemaining <= 0)
						_viper.reset(VIPER_SPAWN_X, VIPER_SPAWN_Y);
				}
				else
				{
					if (FlxG.keys.ENTER)
						FlxG.resetState();
				}
			}
			
			if (FlxG.debug && FlxG.keys.justPressed("U"))
				spawnEnemy();
				
			if (_asteriods.countLiving() <= _asteriods.length / 2)
			{
				spawnAsteroid();
				spawnEnemy();
			}
		}
		
		private var _hitTestBitmapData1:BitmapData = new BitmapData(FlxG.width, FlxG.height, true, 0);
		private var _hitTestBitmapData2:BitmapData = new BitmapData(FlxG.width, FlxG.height, true, 0);
		private var _drawMatrix:Matrix = new Matrix();
		private var _hitTestPoint1:Point = new Point();
		private var _hitTestPoint2:Point = new Point();
		
		private function drawToBitmapData(obj:GameObject, bitmapData:BitmapData):void 
		{
			bitmapData.fillRect(bitmapData.rect, 0);
			_drawMatrix.identity();
			_drawMatrix.translate(-obj.origin.x, -obj.origin.y);
			_drawMatrix.scale(obj.scale.x, obj.scale.y);
			_drawMatrix.rotate(obj.angle * Math.PI / 180);
			_drawMatrix.translate(obj.x + obj.origin.x, obj.y + obj.origin.y);
			bitmapData.draw(obj.framePixels, _drawMatrix);
		}
		
		private function onCollision(obj1:GameObject, obj2:GameObject):void
		{
			if (obj1 is Asteroid && obj2 is Viper)
			{
				drawToBitmapData(obj1, _hitTestBitmapData1);
				drawToBitmapData(obj2, _hitTestBitmapData2);
				//_hitTestPoint1.x = obj1.x;
				//_hitTestPoint1.y = obj1.y;
				//_hitTestPoint2.x = obj2.x;
				//_hitTestPoint2.y = obj2.y;
				if (!_hitTestBitmapData1.hitTest(_hitTestPoint1, 0, _hitTestBitmapData2, _hitTestPoint2, 0))
				//if (!_hitTestBitmapData2.hitTest(_hitTestPoint2, 1, FlxG.camera.buffer, _hitTestPoint1))
					return;
				
				/*
				active = false;
				FlxG.camera.active = false;
				var buf:BitmapData = new BitmapData(320, 240, true, 0xffffff00);//FlxG.camera.buffer;
				//buf.fillRect(buf.rect, 0);
				//buf.copyPixels(_hitTestBitmapData1, _hitTestBitmapData1.rect, _hitTestPoint1);
				//buf.copyPixels(_hitTestBitmapData2, _hitTestBitmapData2.rect, _hitTestPoint2);
				_drawMatrix.identity();
				_drawMatrix.translate(_hitTestPoint1.x, _hitTestPoint1.y);
				buf.draw(_hitTestBitmapData1, _drawMatrix);
				_drawMatrix.identity();
				_drawMatrix.translate(_hitTestPoint2.x, _hitTestPoint2.y);
				buf.draw(_hitTestBitmapData2, _drawMatrix);
				Main.game.addChild(new Bitmap(buf));
				return;
				*/
			}
			
			obj1.onCollision(obj2);
			obj2.onCollision(obj1);
		}
	}

}