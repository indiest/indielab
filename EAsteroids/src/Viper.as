package  
{
	import adobe.utils.CustomActions;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import org.flixel.FlxEmitter;
	import org.flixel.FlxG;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class Viper extends SpaceShip
	{
		public static const SPEED_FACTOR:Number = 5;
		public static const PROJECTILE_COOLDOWN:Number = 0.2;
		
		public var lives:uint = 3;
		public var score:uint = 0;
		
		private var _projectileCooldownRemaining:Number = 0;
		
		public function Viper() 
		{
			loadGraphic(Assets.IMAGES_VIPER, true, false, 20, 10);
			addAnimation("normal", [0]);
			addAnimation("propel", [1, 2], 20);
			maxAngular = 360;
			angularDrag = 1800;
			maxVelocity.make(100, 100);
			drag.make(SPEED_FACTOR * 5, SPEED_FACTOR * 5);
		}
		
		override public function update():void 
		{
			super.update();
			play("normal");
			if (FlxG.keys.LEFT) {
				angularVelocity = -maxAngular;
			}
			if (FlxG.keys.RIGHT) {
				angularVelocity = maxAngular;
			}
			if (FlxG.keys.UP) {
				play("propel");
				FlxG.play(Assets.SOUNDS_VIPER_PROPELLING);
				velocity.x += Math.cos(radians) * SPEED_FACTOR;
				velocity.y += Math.sin(radians) * SPEED_FACTOR;
			}
			//if (FlxG.keys.DOWN) {
				//velocity.x -= Math.cos(radians) * SPEED_FACTOR;
				//velocity.y -= Math.sin(radians) * SPEED_FACTOR;
			//}
			if (FlxG.keys.SPACE) {
				_projectileCooldownRemaining -= FlxG.elapsed;
				if (_projectileCooldownRemaining <= 0)
				{
					_projectileCooldownRemaining = PROJECTILE_COOLDOWN;
					shoot();
				}
			}
			
			// wrap
			if (x < 0)
				x = FlxG.width;
			else if (x > FlxG.width)
				x = 0;
			if (y < 0)
				y = FlxG.height;
			else if (y > FlxG.height)
				y = 0;
		}
		
		override protected function initProjectile(projectile:Projectile):void 
		{
			super.initProjectile(projectile);
			projectile.shooter = this;
		}
		
		override public function explode():void 
		{
			super.explode();
			FlxG.shake();
			lives--;
			state.ui.setLives(lives);
			if (lives == 0)
			{
				state.ui.toggleGameOver(true);
			}
			else
			{
				state.resetReviveTiming();
			}
		}
	}

}