package  
{
	import org.flixel.FlxEmitter;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class Asteroid extends GameObject 
	{
		public static const MAX_SPEED:Number = 60;
		
		private var _emitter:FlxEmitter;
		
		public function Asteroid() 
		{
			loadGraphic(Assets.IMAGES_ASTEROID);
			//offset.make(4, 4);
			//width = height = 24;
		}
		
		
		override public function update():void 
		{
			super.update();
			if (x < -FlxG.width || x > FlxG.width || y < -FlxG.height || y > FlxG.height)
			{
				kill();
				state.spawnAsteroid();
			}
		}
		
		override public function onCollision(other:GameObject):void 
		{
			super.onCollision(other);
			//if (other is Projectile)
			//{
				explode();
			//}
		}
		
		public function explode():void
		{
			if (scaleXY > 1)
			{
				//FlxG.shake(0.01);
				// respawn two smaller asteroids at the same position
				for (var i:uint = 0; i < 2; i++)
				{
					var asteroid:Asteroid = state.spawnAsteroid();
					asteroid.x = x;
					asteroid.y = y;
					//asteroid.velocity.x = velocity.x * 1.5;
					//asteroid.velocity.y = velocity.y * 1.5;
					asteroid.scaleXY = scaleXY * 0.5;
				}
			}
			
			// show particles
			if (_emitter == null)
			{
				_emitter = new FlxEmitter();
				_emitter.particleClass = ExplosionParticle;
				_emitter.makeParticles(Assets.IMAGES_ASTEROID_EXPLOSION, 20, 0, false, 0);
				state.add(_emitter);
			}
			_emitter.x = x;
			_emitter.y = y;
			_emitter.start(true, 0.5);
			
			FlxG.play(Assets.SOUNDS_ASTEROID_EXPLOSION);
			
			kill();
		}
	}

}