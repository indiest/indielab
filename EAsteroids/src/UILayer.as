package  
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class UILayer extends FlxGroup 
	{
		private var _livesText:FlxText;
		private var _scoreText:FlxText;
		private var _gameOverText:FlxText;
		private var _pressKeyText:FlxText;
		
		public function UILayer() 
		{
			_livesText = addText(280, 0, 100);
			_scoreText = addText(0, 0, 120);
			_gameOverText = addText(150, 100, 100, 2, 0xffffff, "GAME OVER");
			_pressKeyText = addText(100, 150, 150, 1, 0xffffff, "Press ENTER to restart");
			toggleGameOver(false);
		}
		
		public function toggleGameOver(visible:Boolean):void
		{
			_pressKeyText.visible = _gameOverText.visible = visible;
		}
		
		public function setLives(lives:uint):void
		{
			_livesText.text = "Lives: " + lives;
		}
		
		public function setScore(score:uint):void
		{
			_scoreText.text = "Score: " + score;
		}
		
		private function addText(x:Number, y:Number, width:Number, scale:Number = 1, color:uint = 0xffffff, defaultText:String = null):FlxText
		{
			var textBox:FlxText = new FlxText(x, y, width, defaultText);
			textBox.scale.make(scale, scale);
			textBox.color = color;
			textBox.scrollFactor.make();
			add(textBox);
			return textBox;
		}
	}

}