package  
{
	import org.flixel.FlxG;
	import org.flixel.FlxSound;
	import org.flixel.FlxU;
	/**
	 * ...
	 * @author Nicolas
	 */
	public class Enemy extends SpaceShip 
	{
		public var shootAngle:Number = -90;
		public var shootAngularVelocity:Number = 180;
		public var shootInteval:Number = 0.2;
		
		private var _shootTiming:Number = 0;
		private var _sound:FlxSound;
		
		override public function get radians():Number 
		{
			return shootAngle * Math.PI / 180;
		}
		
		public function Enemy() 
		{
			loadGraphic(Assets.IMAGES_UFO);
		}
		
		override public function update():void 
		{
			super.update();
			shootAngle += shootAngularVelocity * FlxG.elapsed;
			_shootTiming += FlxG.elapsed;
			if (_shootTiming >= shootInteval)
			{
				_shootTiming = 0;
				if (onScreen())
					shoot();
			}
			//if (!_sound.alive)
			//	_sound.play(true);
			//_sound.x = x;
			//_sound.y = y;
			_sound.volume = 1 - Math.abs(x - state.viper.x) / FlxG.width;
			if (x > FlxG.width * 2)
				kill();
		}
		
		override public function revive():void 
		{
			super.revive();
//			if (_sound == null)
				_sound = FlxG.play(Assets.SOUNDS_UFO, 1, true, false);
//			_sound.play(true);
		}
		
		override public function kill():void 
		{
			super.kill();
			_sound.kill();
		}
	}

}