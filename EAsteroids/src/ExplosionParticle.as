package  
{
	import org.flixel.FlxParticle;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class ExplosionParticle extends FlxParticle 
	{
		private var _maxLifespan:Number;
		
		public function ExplosionParticle()
		{
			super();
		}
		
		override public function update():void 
		{
			super.update();
			alpha = lifespan / _maxLifespan;
		}
		
		override public function onEmit():void 
		{
			super.onEmit();
			alpha = 1;
			_maxLifespan = lifespan;
		}
	}

}