package  
{
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class GameObject extends FlxSprite 
	{
	
		public function get state():PlayState
		{
			return FlxG.state as PlayState;
		}
		
		public function get scaleXY():Number
		{
			return scale.x || scale.y;
		}
		
		public function set scaleXY(value:Number):void
		{
			scale.x = scale.y = value;
			//width = frameWidth * value;
			//height = frameHeight * value;
		}
		
		public function GameObject() 
		{
			super();
		}
		
		public function onCollision(other:GameObject):void
		{
			//trace(this, "collides", other);
		}
	}

}