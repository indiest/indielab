package  
{
	import org.flixel.FlxEmitter;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	/**
	 * ...
	 * @author Nicolas
	 */
	public class Projectile extends GameObject 
	{
		public static const SPEED:Number = 300;
		
		public var shooter:GameObject;
		
		public function Projectile(color:uint = 0xffffff00) 
		{
			makeGraphic(2, 2, color);
		}
		
		override public function onCollision(other:GameObject):void 
		{
			super.onCollision(other);
			if (shooter is Viper)
			{
				if (other is Enemy)
				{
					state.viper.score += 1000;
				}
				else if (other is Asteroid)
				{
					state.viper.score += 100;
				}
				state.ui.setScore(state.viper.score);
			}
			kill();
		}
		
		override public function update():void 
		{
			super.update();
			if (!onScreen())
				kill();
		}
		
		override public function kill():void 
		{
			super.kill();
			shooter = null;
		}
	}

}