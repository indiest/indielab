

groups = DAME.GetGroups()
groupCount = as3.tolua(groups.length) -1

DAME.SetFloatPrecision(0)

tab1 = "\t"
tab2 = "\t\t"
tab3 = "\t\t\t"
tab4 = "\t\t\t\t"

propertiesString = "%%proploop%% %propname%=\"%propvalue%\"%%proploopend%%"
creationText = tab2.."<%spritename% x=\"%xpos%\" y=\"%ypos%\""..propertiesString.."/>\n" 


dataDir = as3.tolua(VALUE_DataDir)
levelName = as3.tolua(VALUE_LevelName)

-- Output tilemap data
-- slow to call as3.tolua many times.

function exportMapLayer( mapLayer, layerSimpleName )
	-- get the raw mapdata. To change format, modify the strings passed in (rowPrefix,rowSuffix,columnPrefix,columnSeparator,columnSuffix)
	mapText = as3.tolua(DAME.ConvertMapToText(mapLayer,"","\n","",",",""))
	mapText = tab1.."<mapLayer name=\""..layerSimpleName
		.."\" width=\""..as3.tolua(mapLayer.map.width).."\" height=\""..as3.tolua(mapLayer.map.height)
		.."\">\n"..mapText..tab1.."</mapLayer>\n"
	return mapText
end

function exportSpriteLayer( spriteLayer, layerSimpleName )

	mapText = as3.tolua(DAME.CreateTextForSprites(spriteLayer,creationText,"Avatar"))
	mapText = tab1.."<spriteLayer name=\""..layerSimpleName.."\">\n"..mapText..tab1.."</spriteLayer>\n"
	return mapText
end

-- This is the file for the map level class.
fileText = ""

maps = {}
spriteLayers = {}

masterLayerAddText = ""
stageAddText = tab3.."if ( addToStage )\n"
stageAddText = stageAddText..tab3.."{\n"

for groupIndex = 0,groupCount do
	group = groups[groupIndex]
	groupName = as3.tolua(group.name)
	groupName = string.gsub(groupName, " ", "_")
	
	
	layerCount = as3.tolua(group.children.length) - 1
	
	fileText = "<level>\n"
	
	
	-- Go through each layer and store some tables for the different layer types.
	for layerIndex = 0,layerCount do
		layer = group.children[layerIndex]
		isMap = as3.tolua(layer.map)~=nil
		layerSimpleName = as3.tolua(layer.name)
		--layerName = groupName..string.gsub(layerSimpleName, " ", "_")
		if isMap == true then
			
			fileText = fileText..exportMapLayer(layer, layerSimpleName);
	
		elseif as3.tolua(layer.IsSpriteLayer()) == true then
		
			fileText = fileText..exportSpriteLayer(layer, layerSimpleName);
			
		end
	end
end


fileText = fileText.."</level>\n"
	
-- Save the file!

DAME.WriteFile(dataDir.."/"..levelName..".xml", fileText )




return 1
