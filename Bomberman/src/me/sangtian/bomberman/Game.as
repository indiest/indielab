package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.test.XmlLevelTestState;
	import org.flixel.FlxGame;
	import me.sangtian.bomberman.*;
	import me.sangtian.bomberman.loot.*;
	import me.sangtian.bomberman.enemy.*;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Game extends FlxGame
	{
		public static const ROOM_WIDTH:uint = 160;
		public static const ROOM_HEIGHT:uint = 120;
		public static const ZOOM:uint = 4;
		public static const TILE_WIDTH:uint = 8;
		public static const TILE_HEIGHT:uint = 8;
		public static const ROOM_WIDTH_IN_TILES:uint = ROOM_WIDTH / TILE_WIDTH;
		public static const ROOM_HEIGHT_IN_TILES:uint = ROOM_HEIGHT / TILE_HEIGHT;
		public static const OBJECT_MAP:Object = 
		{
			1: Bomber,
			13: Ally,
			16: Crate,
			17: Stone,
			18: Chest,
			32: Bomb,
			52: Pitfall,
			64: Coin,
			65: SpeedLevelUp,
			66: BombLevelUp,
			67: StrengthLevelUp,
			68: HealthLevelUp,
			69: BombMaxLevelUp,
			80: Heart,
			96: Key,
			112: Lock,
			113: RoadSign,
			114: CheckPoint,
			115: HealPoint,
			116: Trigger,
			128: Enemy1,
			130: Enemy2
		};
		public static const TILE_INDEX_COLLISION_START:uint = 16;
		public static const TILE_INDEX_WATER:uint = 48;
		public static const TILE_INDEX_WATER_STONE:uint = 3;
			
		public function Game() 
		{
			super(ROOM_WIDTH, ROOM_HEIGHT, PlayState, ZOOM, 60, 60);
		}
		
	}

}