package me.sangtian.bomberman 
{
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public interface ISaveObject 
	{
		function getSaveData():Object;
		
		function loadSaveData(data:Object):void;
	}
	
}