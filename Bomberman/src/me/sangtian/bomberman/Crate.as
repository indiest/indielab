package me.sangtian.bomberman 
{
	import flash.utils.getDefinitionByName;
	import me.sangtian.bomberman.Bomber;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Crate extends Pushable implements ISaveObject
	{	
		public var lootName:String;
		
		public function Crate(x:Number, y:Number)
		{
			super(x, y);
			strengthToPush = 1;
			setAnimation([16], 1);
		}

		override public function blast(damage:uint):void 
		{
			if (damage > 0)
				kill();
			if (lootName)
			{
				state.addLoot(state.createObjectByClassName(lootName, x, y));
				lootName = null;
			}
		}
		
		/* INTERFACE me.sangtian.bomberman.ISaveObject */
		
		public function getSaveData():Object 
		{
			return { lootName: lootName };
		}
		
		public function loadSaveData(data:Object):void 
		{
			lootName = data.lootName;
		}
	}

}