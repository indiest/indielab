package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Loot;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Fin extends Loot
	{
		
		public function Fin(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([70], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.canSwim = true;
		}
	}

}