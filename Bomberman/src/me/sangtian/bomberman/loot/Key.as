package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.GameObject;
	import me.sangtian.bomberman.Loot;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Key extends Loot 
	{
		
		public function Key(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([96], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.hasKey = true;
		}
		
		override public function toString():String 
		{
			return "the key to unlock the door of this level!"
		}
	}

}