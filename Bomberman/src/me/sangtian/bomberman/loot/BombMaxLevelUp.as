package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Loot;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class BombMaxLevelUp extends Loot 
	{
		
		public function BombMaxLevelUp(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([69], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.bombMax++;
		}
		
		override public function toString():String 
		{
			return "a pocket for more bombs!";
		}
	}

}