package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Loot;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class BombLevelUp extends Loot 
	{
		
		public function BombLevelUp(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([66], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.bombLevel++;
		}
		
		override public function toString():String 
		{
			return "more powerful bomb!"
		}
	}

}