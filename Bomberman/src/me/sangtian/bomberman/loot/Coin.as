package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Loot;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Coin extends Loot
	{
		public var worth:uint = 1;
		
		public function Coin(x:Number, y:Number)
		{
			super(x, y);
			setAnimation([64], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			trace("pick up a coin!");
		}
	}

}