package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Loot;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class StrengthLevelUp extends Loot 
	{
		
		public function StrengthLevelUp(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([67], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.strength++;
		}
		
		override public function toString():String 
		{
			return "a strength levelup! Now you can push heavier stuff.";
		}
	}

}