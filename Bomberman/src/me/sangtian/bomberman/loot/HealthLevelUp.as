package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Loot;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class HealthLevelUp extends Loot 
	{
		
		public function HealthLevelUp(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([68], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.hpMax++;
			bomber.health = bomber.hpMax;
		}
		
		override public function toString():String 
		{
			return "a Heart Container!";
		}
	}

}