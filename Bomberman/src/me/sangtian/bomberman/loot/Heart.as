package me.sangtian.bomberman.loot
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.GameObject;
	import me.sangtian.bomberman.Loot;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Heart extends Loot
	{
		
		public function Heart(x:Number, y:Number)
		{
			super(x, y);
			setAnimation([80], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.heal(1);
		}
	}

}