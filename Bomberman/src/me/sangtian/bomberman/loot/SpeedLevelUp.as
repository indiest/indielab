package me.sangtian.bomberman.loot 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Loot;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class SpeedLevelUp extends Loot 
	{
		
		public function SpeedLevelUp(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([65], 1);
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.speedLevel++;
		}
		
		override public function toString():String 
		{
			return "the Boost of Speed!"
		}
		
	}

}