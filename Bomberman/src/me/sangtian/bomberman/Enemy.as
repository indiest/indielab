package me.sangtian.bomberman 
{
	import flash.display.BitmapData;
	import me.sangtian.bomberman.ai.ChaseAI;
	import me.sangtian.bomberman.ai.OrderDirectionAI;
	import me.sangtian.bomberman.ai.PatrolAI;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Enemy extends Creature
	{
		public var patrolAI:EnemyAI = PatrolAI.INSTANCE;
		public var chaseAI:EnemyAI = ChaseAI.INSTANCE;
		
		public var speed:Number = Bomber.INIT_SPEED * 0.8;
		public var sight:Number = 40;
		public var damage:Number = 1;
		
		private var _alertImage:BitmapData = FlxG.addBitmap(Assets.ALERT);
		private var _alert:Boolean = false;
		public function get alert():Boolean
		{
			return _alert;
		}
		public function set alert(value:Boolean):void
		{
			if (_alert != value)
				dirty = true;
			_alert = value;
		}
				
		public function Enemy(x:Number, y:Number)
		{
			super(x, y);
		}
		
		override public function update():void
		{
			super.update();
			
			patrolAI.update(this);
			chaseAI.update(this);
		}
		
		public function onCollide(obj:FlxObject):void
		{
			patrolAI.onCollide(this, obj);
			chaseAI.onCollide(this, obj);
		}
		
		override protected function drawFramePixels():void 
		{
			super.drawFramePixels();
			if (alert)
			{
				//framePixels.copyPixels(_alertImage, _alertImage.rect, _flashPointZero);
				framePixels.draw(_alertImage);
			}
		}
		
		override public function reload():void 
		{
			resetPosition();
			revive();
		}
	}

}