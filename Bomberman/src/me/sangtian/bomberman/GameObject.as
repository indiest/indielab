package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.GameObject;
	import org.flixel.FlxG;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class GameObject extends FlxSprite
	{
		public var hiddenObject:GameObject;
		protected var _type:uint;
		protected var _initX:Number;
		protected var _initY:Number;
		private var _pos:FlxPoint;
				
		public function setType(objType:uint):void 
		{
			_type = objType;
		}
		
		public function get position():FlxPoint
		{
			if (_pos == null)
				_pos = new FlxPoint();
			_pos.x = x;
			_pos.y = y;
			return _pos;
		}
		
		public function get positionId():Number
		{
			return _initY * state.mapWidth + _initX;
		}
		
		public function get state():PlayState
		{
			return FlxG.state as PlayState;
		}
		
		public function get xInTile():int
		{
			return /*state.currentRoom.roomX * Game.ROOM_WIDTH_IN_TILES + */Math.round(x / Game.TILE_WIDTH);
		}
		
		public function get yInTile():int
		{
			return /*state.currentRoom.roomY * Game.ROOM_HEIGHT_IN_TILES + */Math.round(y / Game.TILE_HEIGHT);
		}
	
		public function GameObject(x:Number, y:Number)
		{
			super(x, y);
			_initX = x;
			_initY = y;
		}
		
		protected function setAnimation(frames:Array, frameRate:Number = 0):void
		{
			loadGraphic(Assets.OBJECTS, true, false, 8, 8);
			addAnimation("normal", frames, frameRate);
			play("normal");
		}
		
		public function resetPosition():void
		{
			x = _initX;
			y = _initY;
		}
		
		public function blast(damage:uint):void
		{
			// logic for blasted by bomb
		}
		
		public function reload():void 
		{
			// logic for regenerating when bomber re-enter the room
		}
		
		protected var _distX:int;
		protected var _distY:int;
		
		public function slideOneStep():void
		{
			_distX = x % Game.TILE_WIDTH;
			if (velocity.x > 0)
				_distX = Game.TILE_WIDTH - _distX;
			_distY = y % Game.TILE_HEIGHT;
			if (velocity.y > 0)
				_distY = Game.TILE_HEIGHT - _distY;
			//trace(_distX, _distY);
			drag.make(velocity.x * velocity.x / 2 / _distX, velocity.y * velocity.y / 2 / _distY);
		}
		
		public function collideBomber(bomber:Bomber):void
		{
			
		}
		
		public function isInRoom():Boolean
		{
			return x >= Game.ROOM_WIDTH * state.currentRoom.roomX && (x + width) <= Game.ROOM_WIDTH * (state.currentRoom.roomX + 1)
				&& y >= Game.ROOM_HEIGHT * state.currentRoom.roomY && (y + height) <= Game.ROOM_HEIGHT * (state.currentRoom.roomY + 1);
		}
	}

}