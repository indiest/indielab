package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class RoadSign extends GameObject 
	{
		public var text:String;
		
		public function RoadSign(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([113], 1);
			immovable = true;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			if (text)
			{
				state.uiLayer.showPrompt("The road sign reads:   " + text);
			}
		}
		
	}

}