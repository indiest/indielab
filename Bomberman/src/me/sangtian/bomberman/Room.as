package me.sangtian.bomberman 
{
	import org.flixel.FlxBasic;
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Room
	{
		public var roomX:uint;
		public var roomY:uint;
		//public var objects:TwoDimensionArray = new TwoDimensionArray();
		public var objects:Vector.<GameObject> = new Vector.<GameObject>();
		
		public function Room(roomX:uint, roomY:uint) 
		{
			this.roomX = roomX;
			this.roomY = roomY;
		}
		
	}

}