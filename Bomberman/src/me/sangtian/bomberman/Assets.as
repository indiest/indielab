package me.sangtian.bomberman 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Assets
	{
		[Embed(source="/../assets/Tiles.png")]
		public static const TILES:Class;
		
		[Embed(source="/../assets/Objects.png")]
		public static const OBJECTS:Class;
		
		[Embed(source="/../assets/Alert.png")]
		public static const ALERT:Class;
		
		[Embed(source="/../maps/WorldMap.xml", mimeType="application/octet-stream")]
		public static const WORLD_MAP_DATA:Class;
		
		[Embed(source="/../maps/WorldMap_Script.xml", mimeType="application/octet-stream")]
		public static const WORLD_MAP_SCRIPT:Class;

	}

}