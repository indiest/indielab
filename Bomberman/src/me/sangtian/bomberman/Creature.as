package me.sangtian.bomberman 
{
	import org.flixel.FlxPoint;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Creature extends GameObject
	{
		public var direction:uint = RIGHT;
		private var _directionMultiplier:FlxPoint;
		public function getDirectionMultiplier():FlxPoint
		{
			if (_directionMultiplier == null)
				_directionMultiplier = new FlxPoint();
			_directionMultiplier.make(0, 0);
			if (direction == LEFT)
			{
				_directionMultiplier.x = -1;
			}
			else if (direction == RIGHT)
			{
				_directionMultiplier.x = 1;
			}
			else if (direction == UP)
			{
				_directionMultiplier.y = -1;
			}
			else if (direction == DOWN)
			{
				_directionMultiplier.y = 1;
			}
			return _directionMultiplier;
		}
		
		public function Creature(x:Number, y:Number) 
		{
			super(x, y);			
		}
		
		override public function blast(damage:uint):void 
		{
			super.blast(damage);
			hurt(damage);
		}
		
		override public function hurt(Damage:Number):void 
		{
			if (flickering)
				return;
			super.hurt(Damage);
			if (health > 0)
			{
				flicker(1.5);
			}
		}
		
		public function hitBack(sourceX:Number, sourceY:Number, power:Number):void
		{
			if (!flickering)
				velocity.make((x - sourceX) * power * 100, (y - sourceY) * power * 100);
		}
	}

}