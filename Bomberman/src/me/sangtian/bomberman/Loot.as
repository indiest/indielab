package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Loot extends GameObject 
	{
		
		public function Loot(x:Number, y:Number) 
		{
			super(x, y);
			moves = false;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			kill();
		}
	}

}