package me.sangtian.bomberman.test
{
	import flash.display.TriangleCulling;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.getDefinitionByName;
	import me.sangtian.bomberman.GameObject;
	import org.flixel.FlxState;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class XmlLevelTestState extends FlxState
	{
		override public function create():void 
		{
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, function(evt:*):void
			{
				loadLevel(urlLoader.data);
			});
			urlLoader.load(new URLRequest("Level_Test.xml"));
		}
		
		private function loadLevel(text:String):void 
		{
			var level:XML = new XML(text);
			for each(var mapLayer:* in level.mapLayer)
			{
				trace(mapLayer.@name);
				//trace(mapLayer.text());
			}
			for each(var spriteLayer:* in level.spriteLayer)
			{
				trace(spriteLayer.@name);
				
				for each(var sprite:* in spriteLayer.children())
				{
					var objClass:Class = getDefinitionByName("me.sangtian.bomberman." + sprite.name()) as Class;
					var obj:GameObject = new objClass(sprite.@x, sprite.@y) as GameObject;
					for each (var attr:* in sprite.attributes())
					{
						var attrName:String = attr.name();
						//trace(attrName, sprite.attribute(attrName));
						obj[attrName] = sprite.attribute(attrName);
					}
					trace(obj);
				}
			}
			
		}
	}

}