package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class CheckPoint extends GameObject
	{
		
		public function CheckPoint(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([114], 1);
			solid = false;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			state.saveGame();
		}
	}

}