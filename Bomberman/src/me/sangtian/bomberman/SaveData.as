package me.sangtian.bomberman 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class SaveData
	{
		/* bomber data */
		public var bomberX:Number;
		public var bomberY:Number;
		public var health:Number;
		public var hpMax:uint;	
		public var bombLevel:uint;
		public var bombMax:uint;
		public var hasKey:Boolean;
		public var speedLevel:uint;
		public var strength:uint;
		/* other objects data */
		public var objects:Dictionary = new Dictionary();
		
		public function SaveData() 
		{
			
		}
		
	}

}