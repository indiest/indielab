package me.sangtian.bomberman.enemy 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Enemy;
	import me.sangtian.bomberman.Game;
	import org.flixel.FlxG;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Boss1 extends Enemy 
	{
		public var sprintInterval:Number = 10;
		public var sprintPrepareDuration:Number = 1;
		private var _sprintTimer:Number = 0;
		private var _sprintPrepareTimer:Number = 0;
		
		public function Boss1(x:Number, y:Number) 
		{
			super(x, y);
			// full room sight
			sight = Game.ROOM_WIDTH;
			damage = 2;
			speed = Bomber.INIT_SPEED;
			
			// TODO: bigger and more animations
			setAnimation([128, 129], 4);
			offset.make(1, 1);
			width = 6;
			height = 6;
		}
		
		override public function update():void 
		{
			
			_sprintTimer += FlxG.elapsed;
			if (_sprintTimer >= sprintInterval)
			{
				_sprintTimer = 0;
				color = 0xff0000;
			}
			else
			{
				chaseAI.update();
			}
		}
	}

}