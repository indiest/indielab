package me.sangtian.bomberman.enemy 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Enemy;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Enemy1 extends Enemy 
	{
		
		public function Enemy1(x:Number, y:Number) 
		{
			super(x, y);
			sight = 24;
			damage = 1;
			
			setAnimation([128, 129], 4);			
			offset.make(1, 1);
			width = 6;
			height = 6;
		}
		
		
	}

}