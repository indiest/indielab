package me.sangtian.bomberman.enemy 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Enemy;
	import me.sangtian.bomberman.EnemyAI;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Enemy2 extends Enemy 
	{
		
		public function Enemy2(x:Number, y:Number) 
		{
			super(x, y);
			speed = Bomber.INIT_SPEED * 2;
			sight = 0;
			damage = 1;
			setAnimation([130], 1);
		}
		
		override public function hurt(Damage:Number):void 
		{
			// Invincible
		}
	}

}