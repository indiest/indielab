package me.sangtian.bomberman 
{
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Bomb extends GameObject 
	{
		public var fusing:Boolean = false;
		public var explodeCountdown:Number = 2.0;
		public var power:uint = 3;
		
		public function Bomb(x:Number, y:Number)
		{
			super(x, y);
			loadGraphic(Assets.OBJECTS, true, false, Game.TILE_WIDTH, Game.TILE_HEIGHT);
			addAnimation("normal", [32], 1);
			play("normal");
			color = 0x888888;
			//offset.make(-1, -1);
			//width = 6;
			//height = 6;
			immovable = true;
		}
		
		override public function update():void 
		{
			super.update();
			//trace("countdown:", explodeCountdown);
			if (fusing)
			{
				explodeCountdown -= FlxG.elapsed;
				_colorTransform.redOffset = (2 - explodeCountdown) * 0x88;
				dirty = true;
			}
			
			if (explodeCountdown <= 0)
			{
				fusing = false;
				explode();
			}
		}
		
		public function explode():void 
		{
			state.explodeAt(x, y, power);
			// TODO: play explode animation
			kill();
		}
		
		override public function blast(damage:uint):void 
		{
			if (!fusing)
			{
				fusing = true;
				explodeCountdown = 0.1;
			}
		}
	}

}