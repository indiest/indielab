package me.sangtian.bomberman 
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class UILayer extends FlxGroup
	{
		private var _stat:FlxText = new FlxText(0, 0, Game.ROOM_WIDTH);
		private var _prompt:FlxText = new FlxText(0, Game.ROOM_HEIGHT, Game.ROOM_WIDTH);

		public function UILayer() 
		{
			_stat.scrollFactor.make();
			_prompt.scrollFactor.make();
			add(_stat);
			add(_prompt);
		}
		
		public function updateStat(value:String):void
		{
			_stat.text = value;
		}
		
		public function showPrompt(text:String):void
		{
			if (_prompt.y < Game.ROOM_HEIGHT)
				return;
			_prompt.text = text;
			_prompt.velocity.y = -80;
		}
		
		override public function update():void 
		{
			super.update();
			if (_prompt.y + _prompt.height < Game.ROOM_HEIGHT)
			{
				_prompt.velocity.y = 0;
				_prompt.acceleration.y = 40;
			}
			else if (_prompt.y > Game.ROOM_HEIGHT)
			{
				_prompt.velocity.y = 0;
				_prompt.acceleration.y = 0;
				_prompt.y = Game.ROOM_HEIGHT;
			}
		}
	}

}