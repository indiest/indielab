package me.sangtian.bomberman 
{
	import flash.utils.getDefinitionByName;
	import me.sangtian.bomberman.script.ScriptObject;
	import org.flixel.FlxBasic;
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxPath;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSave;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxTilemap;
	import org.flixel.FlxU;
	import org.flixel.system.FlxTile;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PlayState extends FlxState
	{
		public var uiLayer:UILayer = new UILayer();
		public var switchingRoom:Boolean = false;
		public var mapHeight:uint;
		public var mapWidth:uint;
		public var saveName:String;
		
		private var _mapCollisionLayers:FlxGroup = new FlxGroup();
		private var _mapTileLayer:FlxTilemap = new FlxTilemap();
		//private var _mapObjectLayer:FlxTilemap = new FlxTilemap();
		private var _mapObjectLayer:FlxGroup = new FlxGroup();
		private var _mapLootLayer:FlxGroup = new FlxGroup();
		private var _mapCreatureLayer:FlxGroup = new FlxGroup();
		private var _effectLayer:FlxGroup = new FlxGroup();
		private var _shadeLayer:FlxTilemap = new FlxTilemap();
		
		private var _rooms:Vector.<Vector.<Room>> = new Vector.<Vector.<Room>>();
		private var _currentRoom:Room;
		private var _bomber:Bomber;
		private var _cameraScrollX:uint;
		private var _cameraScrollY:uint;
		
		public function get bomber():Bomber
		{
			return _bomber;
		}
		
		public function get currentRoom():Room
		{
			return _currentRoom;
		}
		
		public function createObjectByClassName(className:String, x:Number, y:Number, addNamespace:Boolean = true):GameObject
		{
			if (addNamespace)
				className = "me.sangtian.bomberman." + className;
			try
			{
				var objClass:Class = getDefinitionByName(className) as Class;
				return new objClass(x, y) as GameObject;
			}
			catch (err:Error)
			{
				trace("ERROR: Can't create game object by class name " + className);
			}
			return null;
		}
		
		private function loadLevel(xml:String):void
		{
			var level:XML = new XML(xml);
			for each(var mapLayer:* in level.mapLayer)
			{
				//trace("Loading tilemap:", mapLayer.@name);
				if (mapLayer.@name == "Tiles")
				{
					_mapTileLayer.loadMap(mapLayer.text(), Assets.TILES, Game.TILE_WIDTH, Game.TILE_HEIGHT,
						FlxTilemap.OFF, 0, 1, Game.TILE_INDEX_COLLISION_START);
					
					// initialize rooms vector
					mapHeight = mapLayer.@height;
					mapWidth = mapLayer.@width;
					for (var y:uint = 0; y < mapHeight / Game.ROOM_HEIGHT; y++)
					{
						_rooms[y] = new Vector.<Room>();
						for (var x:uint = 0; x < mapWidth / Game.ROOM_WIDTH; x++)
						{
							_rooms[y][x] = new Room(x, y);
						}
					}
				}
				else if (mapLayer.@name == "Shade")
				{
					_shadeLayer.loadMap(mapLayer.text(), Assets.TILES, Game.TILE_WIDTH, Game.TILE_HEIGHT);	
				}
			}
			
			
			var objClass:Class;
			var obj:GameObject;
			var attrName:String;
			var room:Room;
			var roomX:uint;
			var roomY:uint;
			for each(var spriteLayer:* in level.spriteLayer)
			{
				//trace("Loading objects on layer", spriteLayer.@name);
				
				for each(var sprite:* in spriteLayer.children())
				{
					obj = createObjectByClassName(sprite.name(), sprite.@x, sprite.@y);
					if (obj == null)
						continue;
					for each (var attr:* in sprite.attributes())
					{
						attrName = attr.name();
						//trace(attrName, sprite.attribute(attrName));
						obj[attrName] = sprite.attribute(attrName);
					}
					
					roomX = obj.x / Game.ROOM_WIDTH;
					roomY = obj.y / Game.ROOM_HEIGHT;
					room = _rooms[roomY][roomX];
					//trace("Created game object:", obj, roomX, roomY);
					
					if (obj is Bomber)
					{
						_bomber = obj as Bomber;
					}
					else
					{
						room.objects.push(obj);
						//room.objects.set(x, y, obj);
						addObject(obj);
					}
				}
			}
		}
		
		private var _scripts:Object = { };

		private function loadScript(xml:String):void 
		{
			var scripts:XML = new XML(xml);
			for each(var script:* in scripts.script)
			{
				var scriptObj:ScriptObject = new ScriptObject(script.@id);
				scriptObj.parse(script.@text);
				_scripts[scriptObj.id] = scriptObj;
			}
		}
				
		override public function create():void 
		{
			//_mapTileLayer.loadMap(new Assets.TILE_LAYER_MAP_DATA(), Assets.TILES, Game.TILE_WIDTH, Game.TILE_HEIGHT, FlxTilemap.OFF, 0, 1, 16);
			//loadObjectsFromCSV();
			
			// TODO: load the level data file if it's specified.
			loadScript(new Assets.WORLD_MAP_SCRIPT());
			loadLevel(new Assets.WORLD_MAP_DATA());
			
			add(_mapTileLayer);
			add(_mapObjectLayer);
			add(_mapLootLayer);
			add(_mapCreatureLayer);
			add(_effectLayer);
			add(_shadeLayer);
			_mapCollisionLayers.add(_mapTileLayer);
			_mapCollisionLayers.add(_mapObjectLayer);
			//add(_mapCollisionLayers);
			_mapCreatureLayer.add(_bomber);
			add(uiLayer);
			
			/*
			if (saveName)
			{
				loadGame(saveName);
			}
			else
			{
				_flxSave.bind("QuickSave");
				if (_flxSave.data.bomber)
					loadGame();
				else
					saveGame();
			}
			*/
			loadRoom();
		}
		
		private function resetBounds():void
		{
			// add 1 more tile around the screen bound
			FlxG.camera.setBounds(FlxG.camera.scroll.x - Game.TILE_WIDTH, FlxG.camera.scroll.y - Game.TILE_HEIGHT,
				Game.ROOM_WIDTH + Game.TILE_WIDTH * 2, Game.ROOM_HEIGHT + Game.TILE_HEIGHT * 2, true);
		}
		
		public function loadRoom():void
		{
			var room:Room = _rooms[uint(_bomber.yInTile / Game.ROOM_HEIGHT_IN_TILES)][uint(_bomber.xInTile / Game.ROOM_WIDTH_IN_TILES)];
			_cameraScrollX = Game.ROOM_WIDTH * room.roomX;
			_cameraScrollY = Game.ROOM_HEIGHT * room.roomY;
			// add bomber to the end of the new room's object list.
			room.objects.push(_bomber);
			
			// scroll camera. if the target room is next to current room, roll the camera slidely.
			if (_currentRoom == null || Math.abs(_currentRoom.roomX - room.roomX) > 1 || Math.abs(_currentRoom.roomY - room.roomY) > 1)
			{
				_currentRoom = room;
				FlxG.camera.scroll.make(_cameraScrollX, _cameraScrollY);
				resetBounds();
			}
			else
			{
				// remove bomber from the end of the former room's object list.
				_currentRoom.objects.pop();
				
				_currentRoom = room;
				switchingRoom = true;
			}
			
			// remove all loots.
			_mapLootLayer.clear();
			// regenerate crates and other stuff.
			for each (var obj:GameObject in room.objects)
			{
				obj.reload();
			}
		}
		
		override public function update():void 
		{
			if (switchingRoom)
			{
				//trace(FlxG.camera.scroll.x, FlxG.camera.scroll.y, _cameraScrollX, _cameraScrollY);
				if (FlxG.camera.scroll.x != _cameraScrollX)
					FlxG.camera.scroll.x += Game.TILE_WIDTH * FlxU.sign(_cameraScrollX - FlxG.camera.scroll.x);
				else if (FlxG.camera.scroll.y != _cameraScrollY)
					FlxG.camera.scroll.y += Game.TILE_HEIGHT * FlxU.sign(_cameraScrollY - FlxG.camera.scroll.y);
				else
					switchingRoom = false;
				resetBounds();
				return;
			}

			super.update();
			
			FlxG.collide(_mapCreatureLayer, _mapCollisionLayers, collideCallback);
			FlxG.collide(_bomber, _mapLootLayer, collideCallback);
			FlxG.collide(_mapCreatureLayer, _bomber, collideCallback);
			//_mapTileLayer.overlapsWithCallback(_bomber, FlxObject.separate);
			
			uiLayer.updateStat(printf("Bombs(Lv.%d): %d/%d   HP: %d/%d",
				_bomber.bombLevel, _bomber.bombRemaining, _bomber.bombMax, _bomber.health, _bomber.hpMax));
		}
		
		private function collideCallback(obj1:FlxObject, obj2:FlxObject):void
		{
			//trace("collision:", obj1, obj2);
			if (obj1 is Bomber && obj2 is GameObject)
			{
				(obj2 as GameObject).collideBomber(_bomber);
			}
			else if (obj1 is Enemy)
			{
				(obj1 as Enemy).onCollide(obj2);
			}
		}
		
		public function addObject(obj:GameObject):void
		{
			if (obj is Enemy)
				_mapCreatureLayer.add(obj);
			else
				_mapObjectLayer.add(obj);
		}
		
		public function addLoot(obj:GameObject):void
		{
			_mapLootLayer.add(obj);
		}
		
		public function explodeAt(x:Number, y:Number, power:uint):void 
		{
			blastPoint(x, y, power + 1)
			blastLine(x + Game.TILE_WIDTH, y, power, 1, 0);
			blastLine(x - Game.TILE_WIDTH, y, power, -1, 0);
			blastLine(x, y + Game.TILE_HEIGHT, power, 0, 1);
			blastLine(x, y - Game.TILE_HEIGHT, power, 0, -1);
		}

		private function blastLine(x:Number, y:Number, power:uint, xMultiplier:int, yMultiplier:int):void
		{
			for (var i:uint = 0; i < power; i++)
			{
				if (blastPoint(x + i * xMultiplier * Game.TILE_WIDTH, y + i * yMultiplier * Game.TILE_HEIGHT, power - i))
					break;
			}
		}
		
		private function blastPoint(x:Number, y:Number, power:uint):Boolean
		{
			var flame:Flame = _effectLayer.recycle(Flame) as Flame;
			flame.revive();
			flame.x = x;
			flame.y = y;
			
			for each(var obj:GameObject in _currentRoom.objects)
			{
				if (obj.exists && (obj.x + obj.width > flame.x) && (obj.x < flame.x + flame.width) &&
					(obj.y + obj.height > flame.y) && (obj.y < flame.y + flame.height))
				{
					//trace("blast", obj);
					obj.blast(power);
					return true;
				}
			}
			if (_mapTileLayer.overlaps(flame, true))
			{
				return true;
			}
			return false;
		}
		
		public function findObject(xInTile:int, yInTile:int):GameObject
		{
			//trace("finding object at:", xInTile, yInTile);
			for each(var obj:GameObject in _currentRoom.objects)
			{
				if (!obj.exists)
					continue;
				//trace("obj:", obj, obj.xInTile, obj.yInTile);
				if (obj.xInTile == xInTile && obj.yInTile == yInTile)
					return obj;
			}
			return null;
		}
		
		public function getTile(xInTile:int, yInTile:int):FlxTile
		{
			//trace("getTile at", xInTile, yInTile);
			// TODO: bounds checking
			return _mapTileLayer.getTileInsance(xInTile, yInTile);
		}
		
		public function setTile(xInTile:uint, yInTile:uint, newIndex:uint):void
		{
			_mapTileLayer.setTile(xInTile, yInTile, newIndex);
		}
		
		public function setTileProperties(tileIndex:uint, allowCollisions:uint, callback:Function = null):void
		{
			_mapTileLayer.setTileProperties(tileIndex, allowCollisions, callback);
		}
		
		public function isEmpty(xInTile:int, yInTile:int):Boolean
		{
			return !(getTile(xInTile, yInTile).solid || findObject(xInTile, yInTile) != null);
		}
		
		public function findPath(obj1:GameObject, obj2:GameObject):FlxPath
		{
			return _mapTileLayer.findPath(obj1.position, obj2.position);
		}
		
		public function checkRay(start:FlxPoint, end:FlxPoint):Boolean
		{
			return _mapTileLayer.ray(start, end);
		}
		
		private var _flxSave:FlxSave = new FlxSave();
		
		public function saveGame(name:String = "QuickSave"):void 
		{
			_flxSave.bind(name);
			var data:Object = _flxSave.data;
			for (var roomY:uint = 0; roomY < _rooms.length; roomY++)
			{
				for (var roomX:uint = 0; roomX < _rooms[roomY].length; roomX++)
				{
					for each (var gameObj:GameObject in _rooms[roomY][roomX].objects)
					{
						if (gameObj is ISaveObject)
							data[gameObj.positionId] = (gameObj as ISaveObject).getSaveData();
					}
				}
			}
			data.bomber = bomber.getSaveData();
			
			_flxSave.data = data;
		}
		
		public function loadGame(name:String = "QuickSave"):void
		{
			_flxSave.bind(name);
			var data:Object = _flxSave.data;
			for (var roomY:uint = 0; roomY < _rooms.length; roomY++)
			{
				for (var roomX:uint = 0; roomX < _rooms[roomY].length; roomX++)
				{
					for each (var gameObj:GameObject in _rooms[roomY][roomX].objects)
					{
						var saveData:Object = data[gameObj.positionId];
						if (saveData)
						{
							(gameObj as ISaveObject).loadSaveData(saveData);
						}
					}
				}
			}
			bomber.loadSaveData(data.bomber);
		}
				
		private var _scriptContext:Object = { };

		public function executeScript(scriptId:String):void 
		{
			var s:ScriptObject = _scripts[scriptId];
			if (s != null)
				s.execute(_scriptContext);
			else
				trace("Can't find script by id: " + scriptId);
		}
	}
}