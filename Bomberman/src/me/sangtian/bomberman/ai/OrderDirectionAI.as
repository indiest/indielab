package me.sangtian.bomberman.ai 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Enemy;
	import me.sangtian.bomberman.EnemyAI;
	import me.sangtian.bomberman.PlayState;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class OrderDirectionAI extends ChaseAI 
	{
		public static const DIRECTIONS:Array = [FlxObject.RIGHT, FlxObject.DOWN, FlxObject.LEFT, FlxObject.UP];
		
		public static const DIRECTION_MULTIPLIERS:Array = [new FlxPoint(1, 0),
			new FlxPoint(0, 1), new FlxPoint( -1, 0), new FlxPoint(0, -1)];
		
		override public function startMove(enemy:Enemy):void 
		{
			//enemy.direction = getPassableDirection();
			//trace("startMove!");
			selectDirection(enemy, 0);
		}
		
		private function selectDirection(enemy:Enemy, dirIndex:uint):void
		{
			var m:FlxPoint;
			for (var i:uint = 0; i < DIRECTION_MULTIPLIERS.length; i++)
			{
				m = DIRECTION_MULTIPLIERS[dirIndex];
				dirIndex++;
				if (dirIndex >= DIRECTION_MULTIPLIERS.length)
					dirIndex = 0;
				if (enemy.state.isEmpty(enemy.xInTile + m.x, enemy.yInTile + m.y))
					break;
			}
			enemy.direction = DIRECTIONS[dirIndex];
			enemy.velocity.make(enemy.speed * m.x, enemy.speed * m.y);
		}
		
		override public function onCollide(enemy:Enemy, obj:FlxObject):void 
		{
			var dirIndex:int = DIRECTIONS.indexOf(enemy.direction);
			if (dirIndex < 0)
				dirIndex = 0;
			selectDirection(enemy, dirIndex);
		}
		
		override public function update(enemy:Enemy):void 
		{
			if (!enemy.onScreen())
				onCollide(enemy, null);
			super.update(enemy);
		}
		
		protected function getPassableDirection(enemy:Enemy):uint
		{
			var x:int = enemy.xInTile;
			var y:int = enemy.yInTile;
			var state:PlayState = enemy.state;
			if (state.isEmpty(x + 1, y))
				return FlxObject.RIGHT;
			else if (state.isEmpty(x - 1, y))
				return FlxObject.LEFT;
			else if (state.isEmpty(x, y + 1))
				return FlxObject.DOWN;
			else if (state.isEmpty(x, y - 1))
				return FlxObject.UP;
			return FlxObject.NONE;
		}
		
		
	}

}