package me.sangtian.bomberman.ai 
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class StateMachine 
	{
		public static const INIT_STATE:String = "init";
		
		// Map<stateName:String, Array<StateTransition>>
		private var _stateTransitions:Object = { };

		private var _host:Object;
		private var _currentState:State;
		
		public function get currentState():State 
		{
			return _currentState;
		}
		
		public function StateMachine(host:Object) 
		{
			_host = host;
		}

		public function setCurrentStateByName(stateName:String):void
		{
			if (!_stateTransitions[stateName])
				throw new ArgumentError("State doesn't exist:" + stateName);
			_currentState = _stateTransitions[stateName];
		}

		public function addState(state:State):void
		{
			if (_stateTransitions[state.name])
				throw new ArgumentError("State already exists");
			_stateTransitions[state.name] = [];
		}
		
		public function addStateTransition(from:String, transition:StateTransition):void
		{
			if (!_stateTransitions[from])
				throw new ArgumentError("State doesn't exist:" + stateName);
			(_stateTransitions[from] as Array).push(transition);
		}
		
		public function update():void
		{
			if (_currentState == null)
				return;
			for each(var transition:StateTransition in _stateTransitions[_currentState.name])
			{
				if (transition.check(_host))
				{
					setCurrentStateByName(transition.to);
					return;
				}
			}
			_currentState.update();
		}
		
		public static function parse(json:String):StateMachine
		{
			var data:Object = JSON.parse(json);
			var sm:StateMachine = new StateMachine();
			for each(var stateName:String in data)
			{
				
			}
			return sm;
		}
	}

}