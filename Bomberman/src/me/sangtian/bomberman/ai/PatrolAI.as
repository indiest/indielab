package me.sangtian.bomberman.ai 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Enemy;
	import me.sangtian.bomberman.EnemyAI;
	import me.sangtian.bomberman.GameObject;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PatrolAI extends EnemyAI 
	{
		public static const INSTANCE:PatrolAI = new PatrolAI();
		
		public static const DIRECTIONS:Array = [FlxObject.RIGHT, FlxObject.DOWN, FlxObject.LEFT, FlxObject.UP];
		
		public static const DIRECTION_MULTIPLIERS:Array = [new FlxPoint(1, 0),
			new FlxPoint(0, 1), new FlxPoint( -1, 0), new FlxPoint(0, -1)];
		
		private function startMove(enemy:Enemy):void 
		{
			//enemy.direction = getPassableDirection();
			//trace("startMove!");
			selectDirection(enemy, 0);
		}
		
		private function selectDirection(enemy:Enemy, dirIndex:uint):void
		{
			var xInTile:int;
			var yInTile:int;
			for (var i:uint = 0; i < DIRECTION_MULTIPLIERS.length; i++)
			{
				if (dirIndex >= DIRECTION_MULTIPLIERS.length)
					dirIndex = 0;
				xInTile = DIRECTION_MULTIPLIERS[dirIndex].x + enemy.xInTile;
				yInTile = DIRECTION_MULTIPLIERS[dirIndex].y + enemy.yInTile;
				if (!enemy.state.getTile(xInTile, yInTile).solid)
					break;
				var obj:GameObject = enemy.state.findObject(xInTile, yInTile);
				if (obj == null || obj is Bomber)
					break;
				dirIndex++;
			}
			enemy.direction = DIRECTIONS[dirIndex];
			enemy.velocity.make(enemy.speed * DIRECTION_MULTIPLIERS[dirIndex].x, enemy.speed * DIRECTION_MULTIPLIERS[dirIndex].y);
		}
		
		override public function onCollide(enemy:Enemy, obj:FlxObject):void 
		{
			var dirIndex:int = DIRECTIONS.indexOf(enemy.direction);
			if (dirIndex < 0)
				dirIndex = 0;
			selectDirection(enemy, dirIndex + 1);
		}
		
		override public function update(enemy:Enemy):void 
		{
			if (enemy.velocity.isZero())
				startMove(enemy);
			else if (!enemy.isInRoom())
				onCollide(enemy, null);
		}
		
	}

}