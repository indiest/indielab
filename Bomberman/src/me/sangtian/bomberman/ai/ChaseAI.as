package me.sangtian.bomberman.ai 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.Enemy;
	import me.sangtian.bomberman.EnemyAI;
	import me.sangtian.bomberman.GameObject;
	import org.flixel.FlxObject;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ChaseAI extends EnemyAI
	{
		public static const INSTANCE:ChaseAI = new ChaseAI();
		
		override public function update(enemy:Enemy):void 
		{
			var bomber:Bomber = enemy.state.bomber;
			var dx:Number = bomber.x - enemy.x;
			var dy:Number = bomber.y - enemy.y;
			if (Math.abs(dx) <= enemy.sight && Math.abs(dy) <= enemy.sight)
			{
				if (enemy.state.checkRay(bomber.position, enemy.position))
				{
					enemy.alert = true;
					var d:Number = Math.sqrt(dx * dx + dy * dy);
					enemy.velocity.make(enemy.speed * dx / d, enemy.speed * dy / d);
				}
			}
			else
			{
				enemy.alert = false;
			}
		}
		
		override public function onCollide(enemy:Enemy, obj:FlxObject):void 
		{
			if (obj is Bomber)
			{
				enemy.velocity.make();
				var bomber:Bomber = obj as Bomber;
				bomber.hurt(enemy.damage);
				bomber.hitBack(enemy.x, enemy.y, enemy.damage);
			}
		}
		
	}

}