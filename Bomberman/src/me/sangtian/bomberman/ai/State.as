package me.sangtian.bomberman.ai 
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class State 
	{
		public var name:String;
		
		public function State(name:String) 
		{
			this.name = name;
		}
		
		public function update():void
		{
			
		}
		
		public function toString():String
		{
			return name;
		}
	}

}