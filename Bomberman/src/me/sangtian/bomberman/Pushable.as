package me.sangtian.bomberman 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Pushable extends GameObject 
	{
		public var strengthToPush:Number = 1;
		private var _movingTimer:Number = 0;
		private var _movingX:Number;
		private var _movingY:Number;
		
		public function Pushable(x:Number, y:Number) 
		{
			super(x, y);
			drag.make(Number.MAX_VALUE, Number.MAX_VALUE);
			immovable = true;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			if (bomber.strength < strengthToPush)
				return;
			if (_movingTimer > 0)
				return;
			var targetX:int = xInTile;
			var targetY:int = yInTile;
			_movingX = _movingY = 0;
			if (touching == LEFT)
			{
				targetX += 1;
				_movingX = 1;
			}
			else if (touching == RIGHT)
			{
				targetX -= 1;
				_movingX = -1;
			}
			else if (touching == UP)
			{
				targetY += 1;
				_movingY = 1;
			}
			else if (touching == DOWN)
			{
				targetY -= 1;
				_movingY = -1;
			}
			
			_movingTimer = canPushTo(targetX, targetY) ? Game.TILE_WIDTH : 0;

		}
		
		protected function canPushTo(targetX:int, targetY:int):Boolean
		{
			var roomX:int = targetX / Game.ROOM_WIDTH_IN_TILES;
			var roomY:int = targetY / Game.ROOM_HEIGHT_IN_TILES;
			// can't push objects out of the room
			if (roomX != state.currentRoom.roomX || roomY != state.currentRoom.roomY)
				return false;
			return state.isEmpty(targetX, targetY);
		}
		
		override public function update():void 
		{
			super.update();
			if (_movingTimer > 0)
			{
				_movingTimer--;
				x += _movingX;
				y += _movingY;
				if (_movingTimer == 0)
					onMoveCompleted();
			}
		}
		
		protected function onMoveCompleted():void
		{
			// Implements the move completed logic here
		}
		
		override public function reload():void 
		{
			resetPosition();
			revive();
		}
		
	}

}