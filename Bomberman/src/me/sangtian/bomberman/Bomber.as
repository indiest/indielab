package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Enemy;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Bomber extends Creature implements ISaveObject
	{
		public static const INIT_SPEED:uint = 16;
		
		public var hpMax:uint = 3;	
		public var bombLevel:uint = 1;
		public var bombMax:uint = 1;
		public var bombRemaining:uint = bombMax;
		public var spawnCooldown:Number = 2;
		public var spawnTimer:Number = spawnCooldown;
		public var hasKey:Boolean = false;
		public var speedLevel:uint = 1;
		public var speedMultiplier:Number = 1;
		public var strength:uint = 1;
		private var _canSwim:Boolean = false;
		public function get canSwim():Boolean
		{
			return _canSwim;
		}
		public function set canSwim(value:Boolean):void
		{
			if (_canSwim != value)
			{
				_canSwim = value;
				state.setTileProperties(Game.TILE_INDEX_WATER, _canSwim ? NONE : ANY);
			}
		}
		
		public function Bomber(x:Number, y:Number)
		{
			super(x, y);
			loadGraphic(Assets.OBJECTS, true, false, Game.TILE_WIDTH, Game.TILE_HEIGHT);
			addAnimation("walk_right", [1,2], 4);
			addAnimation("walk_left", [4,5], 4);
			addAnimation("walk_up", [7,8], 4);
			addAnimation("walk_down", [10, 11], 4);
			play("walk_down");
			
			offset.make(1, 1);
			width = 6;
			height = 6;
			
			health = hpMax;
		}
		
		public function get moveUp():Boolean
		{
			return FlxG.keys.UP || FlxG.keys.W;
		}
		
		public function get moveDown():Boolean
		{
			return FlxG.keys.DOWN || FlxG.keys.S;
		}
		
		public function get moveLeft():Boolean
		{
			return FlxG.keys.LEFT || FlxG.keys.A;
		}
		
		public function get moveRight():Boolean
		{
			return FlxG.keys.RIGHT || FlxG.keys.D;
		}
		
		override public function update():void 
		{
			super.update();
			
			maxVelocity.make((INIT_SPEED + speedLevel * Game.TILE_WIDTH) * speedMultiplier,
				(INIT_SPEED + speedLevel * Game.TILE_HEIGHT) * speedMultiplier);
			drag.make(maxVelocity.x * 16, maxVelocity.y * 16);
			//velocity.make();
			acceleration.make();
			
			if (moveUp)
			{
				direction = UP;
				//velocity.y = -maxVelocity.y;
				acceleration.y = -drag.y;
				play("walk_up");
			}
			if (moveDown)
			{
				direction = DOWN;
				//velocity.y = maxVelocity.y;
				acceleration.y = drag.y;
				play("walk_down");
			}
			if (moveLeft)
			{
				direction = LEFT;
				//velocity.x = -maxVelocity.x;
				acceleration.x = -drag.x;
				play("walk_left");
			}
			if (moveRight)
			{
				direction = RIGHT;
				//velocity.x = maxVelocity.x;
				acceleration.x = drag.x;
				play("walk_right");
			}
			/*
			if (FlxG.keys.justReleased("UP") || FlxG.keys.justReleased("W") ||
				FlxG.keys.justReleased("DOWN") || FlxG.keys.justReleased("S") ||
				FlxG.keys.justReleased("LEFT") || FlxG.keys.justReleased("A") ||
				FlxG.keys.justReleased("RIGHT") || FlxG.keys.justReleased("D"))
			{
				slideOneStep();
			}
			*/
			if (!onScreen())
			{
				state.loadRoom();
				return;
			}
			
			if (FlxG.keys.justPressed("SPACE"))
			{
				spawnBomb();
			}
			
			if (bombRemaining < bombMax)
			{
				spawnTimer -= FlxG.elapsed;
				if (spawnTimer <= 0)
				{
					spawnTimer = spawnCooldown;
					bombRemaining++;
				}
			}
		}
		
		public function spawnBomb():void 
		{
			if (bombRemaining <= 0)
				return;
			
			var xMultiplier:int = 0;
			var yMultiplier:int = 0;
			if (direction == LEFT)
			{
				xMultiplier = -1;
				velocity.x = 0;
			}
			else if (direction == RIGHT)
			{
				xMultiplier = 1;
				velocity.x = 0;
			}
			else if (direction == UP)
			{
				yMultiplier = -1;
				velocity.y = 0;
			}
			else if (direction == DOWN)
			{
				yMultiplier = 1;
				velocity.y = 0;
			}
			if (!state.isEmpty(xInTile + xMultiplier, yInTile + yMultiplier))
			{
				trace("can't place bomb there");
				return;
			}
			
			bombRemaining--;
			var bomb:Bomb = new Bomb(x, y);
			bomb.x += Game.TILE_WIDTH * xMultiplier;
			bomb.y += Game.TILE_HEIGHT * yMultiplier;
			bomb.fusing = true;
			bomb.power = bombLevel;
			state.addObject(bomb);
		}
		
		public function heal(points:uint):void
		{
			health += points;
			if (health > hpMax)
				health = hpMax;
		}
		
		/* INTERFACE me.sangtian.bomberman.ISaveObject */
		
		public function getSaveData():Object 
		{
			return {
				x: x,
				y: y,
				health: health,
				hpMax: hpMax,
				bombLevel: bombLevel,
				bombMax: bombMax,
				hasKey: hasKey,
				speedLevel: speedLevel,
				strength: strength,
				canSwim: canSwim
			};
		}
		
		override public function kill():void 
		{
			//super.kill();
			state.loadGame();
		}
		
		public function loadSaveData(data:Object):void 
		{
			x = data.x;
			y = data.y;
			health = data.health;
			hpMax = data.hpMax;
			bombLevel = data.bombLevel;
			bombMax = data.bombMax;
			hasKey = data.hasKey;
			speedLevel = data.speedLevel;
			strength = data.strength;
			canSwim = data.canSwim;
		}
		
	}

}