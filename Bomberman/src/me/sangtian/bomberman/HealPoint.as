package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class HealPoint extends GameObject
	{
		
		public function HealPoint(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([115], 1);
			immovable = false;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.health = bomber.hpMax;
		}
	}

}