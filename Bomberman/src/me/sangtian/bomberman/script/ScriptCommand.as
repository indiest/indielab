package me.sangtian.bomberman.script
{
	import me.sangtian.bomberman.PlayState;
	import org.flixel.FlxG;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ScriptCommand
	{		
		public var name:String;
		
		public function get state():PlayState
		{
			return FlxG.state as PlayState;
		}
		
		public function ScriptCommand() 
		{
			
		}
		
		public function parse(strParams:Array, lines:Array, lineIndex:uint):uint
		{
			return lineIndex;
		}
		
		public function execute(ctx:Object, future:ScriptExecution):void
		{
			future.proceed();
		}
	}

}