package me.sangtian.bomberman.script
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ScriptObject implements ScriptExecution
	{
		private static const COMMAND_MAP:Object = 
		{
			create: Create
		};

		public var id:String;
		public var commands:Vector.<ScriptCommand> = new Vector.<ScriptCommand>();
		
		public function ScriptObject(id:String) 
		{
			this.id = id;
		}
		
		public function execute(ctx:Object):void
		{
			_ctx = ctx;
			trace("executing script:", id);
			proceed();
		}
		
		public function parse(str:String):void
		{
			var lines:Array = str.split("|");
			for (var i:uint = 0; i < lines.length; i++)
			{
				var matches:Array = lines[i].match(/\w+/g);
				var cmdName:String = matches[0];
				var cmdClass:Class = COMMAND_MAP[cmdName];
				if (cmdClass)
				{
					var cmd:ScriptCommand = new cmdClass() as ScriptCommand;
					cmd.name = cmdName;
					i = cmd.parse(matches.slice(1), lines, i);
					commands.push(cmd);
				}
			}
		}
		
		private var _cmdIndex:int = -1;
		private var _ctx:Object;
		
		public function proceed():void 
		{
			_cmdIndex++;
			if (_cmdIndex >= commands.length)
			{
				end();
				return;
			}
			var cmd:ScriptCommand = commands[_cmdIndex];
			trace("executing command:", cmd.name);
			cmd.execute(_ctx, this);
		}
		
		public function end():void 
		{
			_cmdIndex = -1;
		}
	}

}