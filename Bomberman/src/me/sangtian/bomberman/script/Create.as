package me.sangtian.bomberman.script 
{
	import me.sangtian.bomberman.script.ScriptExecution;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Create extends ScriptCommand
	{
		public var objClassName:String;
		public var x:Number;
		public var y:Number;
		
		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint 
		{
			objClassName = strParams[0];
			x = Number(strParams[1]);
			y = Number(strParams[2]);
			return super.parse(strParams, lines, lineIndex);
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			state.addObject(state.createObjectByClassName(objClassName, x, y));
			super.execute(ctx, future);
		}
	}

}