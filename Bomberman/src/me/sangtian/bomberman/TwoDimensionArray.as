package me.sangtian.bomberman 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class TwoDimensionArray 
	{
		private var _arr:Array = [];
		
		public function TwoDimensionArray() 
		{
			
		}
		
		public function set(x:uint, y:uint, obj:Object):void
		{
			var arr:Array = _arr[y];
			if (!arr)
			{
				arr = _arr[y] = [];
			}
			arr[x] = obj;
		}
		
		public function get(x:uint, y:uint):Object
		{
			var arr:Array = _arr[y];
			if (arr)
				return arr[x];
			return null;
		}
		
		public function find(matcher:Function):Object
		{
			for each(var arr:Array in _arr)
			{
				for each(var obj:Object in arr)
				{
					if (matcher(obj))
						return obj;
				}
			}
			return null;
		}
		
		public function findAll(matcher:Function):Array
		{
			var results:Array = [];
			for each(var arr:Array in _arr)
			{
				for each(var obj:Object in arr)
				{
					if (matcher(obj))
						results.push(obj);
				}
			}
			return results;
		}
	}

}