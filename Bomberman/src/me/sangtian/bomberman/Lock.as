package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Lock extends GameObject implements ISaveObject
	{
		
		public function Lock(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([112], 1);
			immovable = true;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			if (bomber.hasKey)
			{
				bomber.hasKey = false;
				kill();
			}
			else
			{
				state.uiLayer.showPrompt("You need a key to unlock the door!");
			}
		}
		
		/* INTERFACE me.sangtian.bomberman.ISaveObject */
		
		public function getSaveData():Object 
		{
			return { exists: exists };
		}
		
		public function loadSaveData(data:Object):void 
		{
			exists = data.exists;
		}
	}

}