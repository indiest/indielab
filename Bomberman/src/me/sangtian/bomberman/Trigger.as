package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	import me.sangtian.bomberman.script.ScriptObject;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Trigger extends GameObject
	{
		public var scriptId:String;
		public var scriptText:String;
		private var _scriptObject:ScriptObject;
		
		public function Trigger(x:Number, y:Number) 
		{
			super(x, y);
			makeGraphic(Game.TILE_WIDTH, Game.TILE_HEIGHT, 0);
			//solid = false;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			// TODO: run more than once
			kill();
			if (scriptId)
			{
				state.executeScript(scriptId);
			}
			else if (scriptText)
			{
				if (_scriptObject == null)
				{
					_scriptObject = new ScriptObject("Trigger_" + xInTile + "_" + yInTile);
					_scriptObject.parse(scriptText);
				}
				_scriptObject.execute(null);
			}
		}
	}

}