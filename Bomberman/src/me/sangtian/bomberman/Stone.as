package me.sangtian.bomberman 
{
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Stone extends Pushable
	{
		public static const BLAST_THRESHOLD:uint = 3;
		private var _inWater:Boolean = false;
		private var _waterTileX:uint;
		private var _waterTileY:uint;
		
		public function Stone(x:Number, y:Number)
		{
			super(x, y);
			strengthToPush = 2;
			setAnimation([17], 1);
		}
		
		override public function blast(damage:uint):void 
		{
			if (damage >= BLAST_THRESHOLD)
				kill();
		}
		
		override protected function canPushTo(targetX:int, targetY:int):Boolean 
		{
			// stones can be pushed into water
			return (state.getTile(targetX, targetY).index == Game.TILE_INDEX_WATER || super.canPushTo(targetX, targetY));
		}
		
		override protected function onMoveCompleted():void 
		{
			super.onMoveCompleted();
			// if the stone was pushed into water, make the tile walkable, and disable the stone.
			if (state.getTile(xInTile, yInTile).index == Game.TILE_INDEX_WATER)
			{
				state.setTile(xInTile, yInTile, Game.TILE_INDEX_WATER_STONE);
				kill();
				_inWater = true;
				_waterTileX = xInTile;
				_waterTileY = yInTile;
			}
		}
		
		override public function reload():void 
		{
			super.reload();
			// reset the water tile
			state.setTile(_waterTileX, _waterTileY, Game.TILE_INDEX_WATER);
			_inWater = false;
		}
	}

}