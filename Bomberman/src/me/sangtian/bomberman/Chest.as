package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Chest extends GameObject implements ISaveObject
	{
		public var lootName:String;
		
		public function Chest(x:Number, y:Number) 
		{
			super(x, y);
			setAnimation([18], 1);
			immovable = true;
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			kill();
			if (lootName)
			{
				state.addLoot(state.createObjectByClassName(lootName, x, y));
				lootName = null;
			}
		}
		
		/* INTERFACE me.sangtian.bomberman.ISaveObject */
		
		public function getSaveData():Object 
		{
			return { exists: exists };
		}
		
		public function loadSaveData(data:Object):void 
		{
			exists = data.exists;
		}
	}

}