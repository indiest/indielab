package me.sangtian.bomberman 
{
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Flame extends FlxSprite
	{
		
		public function Flame() 
		{
			loadGraphic(Assets.OBJECTS, true, false, Game.TILE_WIDTH, Game.TILE_HEIGHT);
			offset.make(1, 1);
			width = height = 6;
			addAnimation("normal", [48, 49, 50, 51], 8, false);
			addAnimationCallback(onAnimationChanged);
			play("normal");
		}
		
		override public function revive():void 
		{
			super.revive();
			play("normal");
		}
		
		private function onAnimationChanged(name:String, frameNumer:uint, tileIndex:uint):void
		{
			if (frameNumer == _curAnim.frames.length - 1)
			{
				_curAnim = null;
				kill();
			}
		}
	}

}