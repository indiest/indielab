package me.sangtian.bomberman 
{
	import me.sangtian.bomberman.Bomber;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Pitfall extends GameObject
	{
		public var damage:uint = 1;
		
		public function Pitfall(x:Number, y:Number) 
		{
			super(x, y);
			loadGraphic(Assets.OBJECTS, true, false, 8, 8);
			addAnimation("activate", [52, 53, 54], 4, false);
			addAnimation("deactivate", [54, 53, 52], 4, false);
			addAnimationCallback(animationCallback);
			immovable = true;
			visible = false;
		}
		
		private function animationCallback(animName:String, frameNumber:uint, tileIndex:uint):void 
		{
			if (animName == "deactivate" && frameNumber == _curAnim.frames.length - 1)
				visible = false;
		}
		
		override public function update():void 
		{
			super.update();
			if (Math.abs(state.bomber.x - x) <= 10 && Math.abs(state.bomber.y - y) <= 10)
			{
				if (!visible)
				{
					visible = true;
					play("activate");
				}
			}
			else
			{
				if (visible)
				{
					play("deactivate");
				}
			}
			
		}
		
		override public function collideBomber(bomber:Bomber):void 
		{
			super.collideBomber(bomber);
			bomber.hurt(damage);
			bomber.hitBack(x, y, damage);
		}
	}

}