package me.sangtian.bomberman 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Ally extends GameObject 
	{
		
		public function Ally(x:Number, y:Number) 
		{
			super(x, y);
			loadGraphic(Assets.OBJECTS, true, false, Game.TILE_WIDTH, Game.TILE_HEIGHT);
			addAnimation("normal", [13, 14], 4);
			play("normal");
		}
		
		
	}

}