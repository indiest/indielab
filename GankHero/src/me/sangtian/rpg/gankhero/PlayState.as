package me.sangtian.rpg.gankhero 
{
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import me.sangtian.common.state.IState;
	import me.sangtian.common.util.ObjectUtil;
	import me.sangtian.common.util.Random;
	import me.sangtian.rpg.framework.AIController;
	import me.sangtian.rpg.framework.CharacterObject;
	import me.sangtian.rpg.framework.CharacterObjectTouchController;
	import me.sangtian.rpg.framework.Data;
	import me.sangtian.rpg.framework.GameObject;
	import me.sangtian.rpg.framework.Item;
	import me.sangtian.rpg.framework.MapEvent;
	import me.sangtian.rpg.framework.MapLayer;
	import me.sangtian.rpg.framework.MapSpawner;
	import me.sangtian.rpg.framework.MapTrigger;
	import me.sangtian.rpg.framework.Obstacle;
	import me.sangtian.rpg.framework.ResourceManager;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class PlayState extends Sprite implements IState
	{
		/* Layers:
		 *
		 * MapLayer(container)
		 *    |_bgLayer
		 *    |_graphicsLayer
		 *    |_objectLayer
		 *    |_particleLayer
		 * 
		 * UILayer
		 *    |_
		 */
		private var _mapLayer:MapLayer;
		private var _uiLayer:UILayer;
		
		public static var mapName:String = "test";
		
		public function PlayState() 
		{
			_mapLayer = new MapLayer(this);
			_mapLayer.container.x = 0;
			_mapLayer.container.y = 50;
			_mapLayer.addEventListener(MapEvent.SPAWN, handleMapSpawn);
			_mapLayer.addEventListener(MapEvent.GAME_OVER, handleGameOver);
			
			_uiLayer = new UILayer();
			_uiLayer.touchController.availableTeams.push(1, 2);
			addChild(_uiLayer);
		}
		
		/* INTERFACE me.sangtian.common.state.IState */
		
		public function update():void 
		{			
			_mapLayer.update();
			_uiLayer.update();
		}
		
		public function get isTransientState():Boolean 
		{
			return false;
		}
		
		public function enter():void 
		{
			_uiLayer.touchController.initialize(_mapLayer);
			
			initMap();
		}
		
		private function initMap():void 
		{
			_mapLayer.load(ResourceManager.getResource("test.dam"));
			_mapLayer.start();
/*			
			ASSERT(mapName != null);
			var mapData:Object = Data.getMapData(mapName);
			if (mapData.bg)
			{
				_mapLayer.setBg(new Image(ResourceManager.getTexture(mapData.bg)));
			}
			ObjectUtil.applyProperties(_mapLayer.moveArea, mapData.moveArea);
			
			for each(var spriteData:Object in mapData.sprites)
			{
				if (spriteData.type == "Image")
				{
					var image:Image = new Image(ResourceManager.getTexture(spriteData.image));
					ObjectUtil.applyProperties(image, spriteData);
					_mapLayer.addSprite(image);
				}
			}
			
			for each(var obstacleData:Object in mapData.obstacles)
			{
				_mapLayer.addObject(new Obstacle(_mapLayer, new Rectangle(
					obstacleData.x, obstacleData.y,
					obstacleData.width, obstacleData.height)));
			}
			
			for each(var spawnerData:Object in mapData.spawners)
			{
				var spawner:MapSpawner = new MapSpawner(_mapLayer, spawnerData);
				if (spawner.startTime > 0 || spawner.interval > 0)
				{
					_mapLayer.addSpawner(spawner);
				}
				else
				{
					var co:CharacterObject = spawner.spawn();
					if (!spawner.hasAI)
					{
						co.inventory.addItem(new Item(Data.getItemData(1001)));
						//co.inventory.addItem(new Item(Data.getItemData(1002)));
						co.inventory.addItem(new Item(Data.getItemData(3001)));
					}
				}
			}
			
			for each(var triggerData:Object in mapData.triggers)
			{
				var trigger:MapTrigger = new MapTrigger(_mapLayer, triggerData);
				_mapLayer.addTrigger(trigger);
			}
			
			_mapLayer.start();
*/			
		}
		
		private function handleMapSpawn(e:Event):void 
		{
			_uiLayer.initCharacter(e.data as CharacterObject);
		}
		
		private function handleGameOver(e:Event):void 
		{
			trace("Game Over!");
		}
		
		public function exit():void 
		{
			_uiLayer.touchController.destroy();
		}
	}

}