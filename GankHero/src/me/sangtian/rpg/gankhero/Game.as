package me.sangtian.rpg.gankhero 
{
	import flash.utils.getDefinitionByName;
	import me.sangtian.common.game.BaseGame;
	import me.sangtian.common.Input;
	import me.sangtian.common.state.StateManager;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.ObjectUtil;
	import me.sangtian.rpg.framework.Data;
	import me.sangtian.rpg.framework.EffectStun;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class Game extends BaseGame
	{
		
		private static var _instance:Game;
		
		public static function get instance():Game 
		{
			return _instance;
		}
		
		public function Game()
		{
			_instance = this;
		}
		
		override protected function init():void 
		{
			LoadingModState.modPath = "resources.json";
			stateManager.loadState(LoadingModState);
		}
	}

}