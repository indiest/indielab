package me.sangtian.rpg.gankhero 
{
	import flash.utils.Dictionary;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.rpg.framework.AreaCastTarget;
	import me.sangtian.rpg.framework.CastTargetType;
	import me.sangtian.rpg.framework.Character;
	import me.sangtian.rpg.framework.CharacterEvent;
	import me.sangtian.rpg.framework.CharacterObject;
	import me.sangtian.rpg.framework.CharacterObjectKeyController;
	import me.sangtian.rpg.framework.CharacterObjectTouchController;
	import me.sangtian.rpg.framework.DamageEvent;
	import me.sangtian.rpg.framework.Data;
	import me.sangtian.rpg.framework.HealingEvent;
	import me.sangtian.rpg.framework.InventoryEvent;
	import me.sangtian.rpg.framework.Item;
	import me.sangtian.rpg.framework.ItemData;
	import me.sangtian.rpg.framework.ItemType;
	import me.sangtian.rpg.framework.ResourceManager;
	import me.sangtian.rpg.framework.SingleCastTarget;
	import me.sangtian.rpg.framework.Skill;
	import me.sangtian.rpg.framework.SkillData;
	import me.sangtian.rpg.framework.ui.CharacterInspector;
	import me.sangtian.rpg.framework.ui.IconButton;
	import me.sangtian.rpg.framework.ui.IconButtonBar;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.filters.FragmentFilter;
	import starling.filters.GrayscaleFilter;
	import starling.text.TextField;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	/**
	 * ...
	 * @author tiansang
	 */
	public class UILayer extends Sprite
	{
		private static const PASSIVE_SKILL_FILTER:FragmentFilter = new GrayscaleFilter();
		
		private static var _imageHightlightRing:Image = Image.fromBitmap(ResourceManager.getResource("ui/hightlight_ring.png"), false);
		private static var _textureUpgradeSkill:Texture = ResourceManager.getTexture("icon/skill_point.png");
		private static var _textureBack:Texture = ResourceManager.getTexture("icon/back.png");

		public var touchController:CharacterObjectTouchController = new CharacterObjectTouchController();
		public var selfInspector:CharacterInspector = new CharacterInspector(200, 600);
		public var targetInspector:CharacterInspector = new CharacterInspector(200, 600);
		public var skillBar:IconButtonBar = new IconButtonBar();
		private var _skillButtons:Dictionary = new Dictionary();
		public var inventoryBar:IconButtonBar = new IconButtonBar();
		private var _itemButtons:Dictionary = new Dictionary();
		private var _keyController:CharacterObjectKeyController = new CharacterObjectKeyController();
		
		private var _popupTexts:ObjectPool;
		
		private var _skillPointButton:IconButton = new IconButton(_textureUpgradeSkill);
		private var _isUpgradingSkill:Boolean = false;
		
		public function get currentCharacter():CharacterObject
		{
			return touchController.currentCharacter;
		}
		
		public function set currentCharacter(co:CharacterObject):void
		{
			touchController.currentCharacter = null;
			_keyController.currentCharacter = null;
		}
		
		public function get targetCharacter():CharacterObject
		{
			return touchController.targetCharacter;
		}
		
		public function UILayer() 
		{
			touchController.addEventListener(CharacterObjectTouchController.EVENT_SWITCH_CURRENT, handleSwitchCharacter);
			
			selfInspector.touchable = false;
			addChild(selfInspector);
			targetInspector.x = 600;
			targetInspector.touchable = false;
			addChild(targetInspector);
			
			skillBar.x = selfInspector.width;
			for each(var skillData:SkillData in Data.skills)
			{
				//var button:DynamicIconButton = new DynamicIconButton();
				//button.loadIcon(ResourceManager.getFullPath(skillData.icon));
				var button:IconButton = new IconButton(ResourceManager.getTexture(skillData.icon));
				button.addEventListener(Event.TRIGGERED, handleTriggerSkillButton);
				_skillButtons[skillData.id] = button;
			}
			addChild(skillBar);
			
			_skillPointButton.addEventListener(Event.TRIGGERED, handleTriggerSkillPointButton);
			
			inventoryBar.x = selfInspector.width;
			inventoryBar.y = 40;
			for each(var itemData:ItemData in Data.items)
			{
				button = new IconButton(ResourceManager.getTexture(itemData.icon));
				button.addEventListener(Event.TRIGGERED, handleTriggerItemButton);
				_itemButtons[itemData.id] = button;
			}
			addChild(inventoryBar);
			
			_popupTexts = new ObjectPool(20, initPopupText);
		}
		
		private function handleSwitchCharacter(e:Event):void 
		{
			skillBar.removeChildren();
			inventoryBar.removeChildren();
			_isUpgradingSkill = false;
			
			if (currentCharacter != null)
			{
				_imageHightlightRing.width = currentCharacter.hitArea.width + 10;
				_imageHightlightRing.height = currentCharacter.hitArea.height + 10;
				_imageHightlightRing.x = -_imageHightlightRing.width * 0.5;
				//_imageHightlightRing.y = -_imageHightlightRing.height * 0.2;
				currentCharacter.addChildAt(_imageHightlightRing, 0);
				
				for each(var skillId:int in currentCharacter.data.availableSkillIds)
				{
					var skill:Skill = currentCharacter.skills[skillId];
					var button:IconButton = _skillButtons[skillId];
					if (skill == null)
					{
						button.visible = false;
					}
					else if (skill.properties.isPassitive)
					{
						button.filter = PASSIVE_SKILL_FILTER;
						//button.enabled = false;
					}
					else
					{
						button.filter = null;
					}
					skillBar.addButton(skillId, button);
				}
				
				skillBar.addButton(0, _skillPointButton);
				_skillPointButton.x += 100;
				updateSkillPointButton();
				
				for (var i:int = 0; i < currentCharacter.inventory.size; i++)
				{
					var item:Item = currentCharacter.inventory.getItem(i);
					if (item == null)
						continue;
					addItemButton(item, i);
				}
			}
			else
			{
				_imageHightlightRing.removeFromParent();
			}
			
			_keyController.currentCharacter = currentCharacter;
		}
		
		private function addItemButton(item:Item, index:uint):void
		{
			var button:IconButton = _itemButtons[item.data.id];
			button.x = button.width * index;
			if (item.consumableTimes > 0)
				button.updateCount(item.consumableTimes);
			inventoryBar.addButton(item.data.id, button);
		}
		
		private function handleTriggerItemButton(e:Event):void 
		{
			var button:IconButton = e.currentTarget as IconButton;
			//var itemId:int = inventoryBar.getIdByButton(e.currentTarget as IconButton);
			var index:int = inventoryBar.getChildIndex(button);
			var item:Item = currentCharacter.inventory.getItem(index);
			ASSERT(item != null);
			if (item.data.castableSkillId == 0)
			{
				Log.info("Item is not usable:", item.data.id);
				return;
			}
			if (item.data.type == ItemType.CONSUMABLE && item.consumableTimes == 0)
			{
				Log.info("Item is used up");
				return;
			}
			
			var skillId:int = item.data.castableSkillId;
			var skill:Skill = currentCharacter.skills[skillId];
			ASSERT(skill != null, "Can't find skill by id:" + skillId);
			if (skill.properties.castTargetType == CastTargetType.OWN)
			{
				currentCharacter.useItem(index, new SingleCastTarget(currentCharacter));
			}
			else if (skill.properties.castTargetType == CastTargetType.SINGLE)
			{
				if (currentCharacter.attackTarget != null)
				{
					currentCharacter.useItem(index, new SingleCastTarget(currentCharacter.attackTarget));
				}
				// TODO: cast the skill to target until it's selected
			}
			else if (skill.properties.castTargetType == CastTargetType.OWN_AREA)
			{
				currentCharacter.useItem(index, new AreaCastTarget(currentCharacter, currentCharacter.position, skill));
			}			
		}
		
		private function handleTriggerSkillButton(e:Event):void 
		{
			var skillId:int = skillBar.getIdByButton(e.currentTarget as IconButton);
			
			if (_isUpgradingSkill)
			{
				currentCharacter.upgradeSkill(skillId);
				_skillPointButton.updateCount(currentCharacter.skillPoints);
				if (currentCharacter.skillPoints == 0)
				{
					handleTriggerSkillPointButton();
					_skillPointButton.visible = false;
				}
				return;
			}
			
			castSkill(skillId);
		}
		
		private function castSkill(skillId:int):void
		{
			var skill:Skill = currentCharacter.skills[skillId];
			ASSERT(skill != null, "Can't find skill by id:" + skillId);
			
			if (skill.properties.castTargetType == CastTargetType.OWN)
			{
				currentCharacter.cast(skillId, new SingleCastTarget(currentCharacter));
			}
			else if (skill.properties.castTargetType == CastTargetType.SINGLE)
			{
				if (currentCharacter.attackTarget != null)
				{
					currentCharacter.cast(skillId, new SingleCastTarget(currentCharacter.attackTarget));
				}
				// TODO: cast the skill to target until it's selected
			}
			else if (skill.properties.castTargetType == CastTargetType.OWN_AREA)
			{
				currentCharacter.cast(skillId, new AreaCastTarget(currentCharacter, currentCharacter.position, skill));
			}			
		}
		
		private function handleTriggerSkillPointButton(e:Event = null):void 
		{
			_isUpgradingSkill = !_isUpgradingSkill;
			updateSkillPointButton();
		}
		
		private function updateSkillPointButton():void 
		{
			if (currentCharacter.skillPoints > 0)
			{
				_skillPointButton.visible = true;
				if (_isUpgradingSkill)
				{
					_skillPointButton.upState = _textureBack;
					_skillPointButton.updateCount(0);
				}
				else
				{
					_skillPointButton.upState = _textureUpgradeSkill;
					_skillPointButton.updateCount(currentCharacter.skillPoints);
				}
			}
			else
			{
				_skillPointButton.visible = false;
			}
		}
		
		public function initCharacter(co:CharacterObject):void 
		{
			//TextFieldProvider.textWidth = co.properties.hitTestWidth;
			//TextFieldProvider.textHeight = 16;
			co.addEventListener(DamageEvent.DAMAGE, handleCharacterDamage);
			co.addEventListener(HealingEvent.HEALING, handleCharacterHealing);
			co.addEventListener(InventoryEvent.ADD_ITEM, handleInventoryAddItem);
			co.addEventListener(InventoryEvent.REMOVE_ITEM, handleInventoryRemoveItem);
			co.addEventListener(InventoryEvent.COUNT_CHANGE, handleInventoryCountChange);
			co.addEventListener(CharacterEvent.LEVEL_UP, handleCharacterLevelUp);
		}
		
		// TODO: call me
		public function clearCharacter(co:CharacterObject):void 
		{
			co.removeEventListener(DamageEvent.DAMAGE, handleCharacterDamage);
			co.removeEventListener(HealingEvent.HEALING, handleCharacterHealing);
			co.removeEventListener(InventoryEvent.ADD_ITEM, handleInventoryAddItem);
			co.removeEventListener(InventoryEvent.REMOVE_ITEM, handleInventoryRemoveItem);
			co.removeEventListener(InventoryEvent.COUNT_CHANGE, handleInventoryCountChange);
		}
		
		private function handleInventoryCountChange(e:InventoryEvent):void 
		{
			var co:CharacterObject = e.currentTarget as CharacterObject;
			if (co == currentCharacter)
			{
				var item:Item = co.inventory.getItem(e.index);
				ASSERT(item != null);
				inventoryBar.getButton(item.data.id).updateCount(item.consumableTimes);
			}
		}
		
		private function handleInventoryRemoveItem(e:InventoryEvent):void 
		{
			var co:CharacterObject = e.currentTarget as CharacterObject;
			if (co == currentCharacter)
			{
				inventoryBar.removeChild(inventoryBar.getButton(e.itemId));
			}
		}
		
		private function handleInventoryAddItem(e:InventoryEvent):void 
		{
			var co:CharacterObject = e.currentTarget as CharacterObject;
			if (co == currentCharacter)
			{
				var item:Item = co.inventory.getItem(e.index);
				ASSERT(item != null);
				addItemButton(item, e.index);
			}
		}
		
		private function handleCharacterLevelUp(e:Event):void 
		{
			if (currentCharacter != null)
				updateSkillPointButton();
		}
		
		private function initPopupText():TextField
		{
			var tf:TextField = new TextField(100, 20, "");
			//tf.fontSize = 8;
			tf.hAlign = HAlign.LEFT;
			tf.vAlign = VAlign.BOTTOM;
			return tf;
		}
		
		private function handleCharacterDamage(e:DamageEvent):void
		{
			var co:CharacterObject = e.currentTarget as CharacterObject;
			showPopupText("-" + int(e.damage),
				co.x + co.avatar.display.x, co.y + co.avatar.display.y,
				e.damageType.color, e.critical ? 12 : 8);
				
			if (co.properties.hp == 0 && currentCharacter == co)
			{
				currentCharacter = null;
			}
		}
		
		private function handleCharacterHealing(e:HealingEvent):void
		{
			var co:CharacterObject = e.currentTarget as CharacterObject;
			showPopupText("+" + int(e.healing),
				co.x + co.avatar.display.x, co.y + co.avatar.display.y,
				Color.LIME, e.critical ? 12 : 8);
		}
		
		public function showPopupText(text:String, x:Number, y:Number, color:uint, fontSize:Number = NaN):void
		{
			var tf:TextField = _popupTexts.borrowObject();
			if (tf == null)
				return;
			tf.text = text;
			tf.color = color;
			if (fontSize)
			{
				tf.fontSize = fontSize;
			}
			tf.x = x;
			tf.y = y;
			tf.alpha = 1;
			addChild(tf);
			Starling.juggler.tween(tf, 0.5, { y: y-20, alpha: 0.5, onComplete: hidePopupText, onCompleteArgs: [tf] } );
		}
		
		private function hidePopupText(tf:TextField):void
		{
			removeChild(tf);
			_popupTexts.returnObject(tf);
		}
		
		public function update():void 
		{
			if (currentCharacter == null)
			{
				selfInspector.visible = false;
				targetInspector.visible = false;
				skillBar.visible = false;
				return;
			}
			
			selfInspector.visible = true;
			selfInspector.update(currentCharacter);
			
			skillBar.visible = true;
			
			for each(var skillId:int in currentCharacter.data.availableSkillIds)
			{
				var button:IconButton = _skillButtons[skillId];
				var skill:Skill = currentCharacter.skills[skillId];
				if (_isUpgradingSkill)
				{
					button.visible = (skill == null || !skill.isMaxLevel);
					button.enabled = true;
					button.clearCooldown();
				}
				else
				{
					if (skill == null)
					{
						button.visible = false;
						continue;
					}
					button.visible = true;
					button.enabled = !skill.properties.isPassitive;
					if (!skill.properties.isPassitive)
					{
						button.updateCooldown(skill.coolingTime, skill.properties.cooldown);
					}
				}
			}
			
			for (var i:uint = 0; i < currentCharacter.inventory.size; i++)
			{
				var item:Item = currentCharacter.inventory.getItem(i);
				if (item == null)
					continue;
				if (item.data.castableSkillId == 0)
					continue;
				skill = currentCharacter.skills[item.data.castableSkillId];
				ASSERT(skill != null);
				button = inventoryBar.getButton(item.data.id);
				button.updateCooldown(skill.coolingTime, skill.properties.cooldown);
			}
			
			if (targetInspector.visible = targetCharacter != null)
			{
				targetInspector.update(targetCharacter);
			}
			
			_keyController.update();
		}
	}

}
import starling.text.TextField;

class TextFieldProvider extends TextField
{
	public static var textWidth:int = 100;
	public static var textHeight:int = 20;
	
	public function TextFieldProvider()
	{
		super(textWidth, textHeight, "");
	}
}
