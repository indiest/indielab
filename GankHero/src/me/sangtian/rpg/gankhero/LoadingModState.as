package me.sangtian.rpg.gankhero 
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.BinaryDataLoader;
	import com.greensock.loading.core.LoaderItem;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.XMLLoader;
	import dragonBones.factorys.BaseFactory;
	import dragonBones.factorys.StarlingFactory;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import me.sangtian.common.state.IState;
	import me.sangtian.rpg.framework.Avatar;
	import me.sangtian.rpg.framework.Data;
	import me.sangtian.rpg.framework.ResourceManager;
	import starling.display.Sprite;
	import starling.text.TextField;
	/**
	 * ...
	 * @author tiansang
	 */
	public class LoadingModState extends Sprite implements IState 
	{
		public static var modPath:String;
		
		private var _loaderQueue:LoaderMax = new LoaderMax();
		private var _avatarsToLoad:Vector.<ByteArray> = new Vector.<ByteArray>();
		private var _avatarsLoadCounter:int;
		private var _skeletonNameMap:Dictionary = new Dictionary();
		
		public function LoadingModState() 
		{
			LoaderMax.activate([ImageLoader, DataLoader, XMLLoader, BinaryDataLoader]);
			LoaderMax.registerFileType("dat", BinaryDataLoader);
			LoaderMax.registerFileType("json", DataLoader);
			LoaderMax.registerFileType("pex", DataLoader);
			LoaderMax.registerFileType("dam", XMLLoader);
			
			addChild(new TextField(100, 40, "Loading...", "Verdana", 20));
		}
		
		/* INTERFACE me.sangtian.common.state.IState */
		
		public function get isTransientState():Boolean 
		{
			return false;
		}
		
		public function enter():void 
		{
			ASSERT(modPath != null);
			_loaderQueue.append(new DataLoader(modPath, {format: "text", onComplete: onManifestLoadComplete } ));
			_loaderQueue.load();
		}
		
		private function onManifestLoadComplete(e:LoaderEvent):void 
		{
			var loader:LoaderItem = e.target as LoaderItem;
			var manifest:Object = JSON.parse(loader.content);
			for each(var item:Object in manifest)
			{
				_loaderQueue.append(LoaderMax.parse(getFullPath(item.path), {item: item, onComplete: onItemLoadComplete } ));
			}
			_loaderQueue.addEventListener(LoaderEvent.COMPLETE, onAllItemsLoadComplete);
			_loaderQueue.load();
		}
		
		private function getFullPath(path:String):String 
		{
			return path;
		}
		
		private function onItemLoadComplete(e:LoaderEvent):void 
		{
			var loader:LoaderItem = e.target as LoaderItem;
			var item:Object = loader.vars.item;
			if (item.cache != false)
			{
				ResourceManager.setResource(item.id || item.path, loader.content);
			}
			if (item.type == "avatar")
			{
				_avatarsToLoad.push(loader.content);
			}
			else if (item.type == "data/item")
			{
				Data.loadItem(loader.content);
			}
			else if (item.type == "data/skill")
			{
				Data.loadSkill(loader.content);
			}
			else if (item.type == "data/character")
			{
				Data.loadCharacter(loader.content);
			}
			else if (item.type == "data/map")
			{
				Data.loadMap(loader.content);
			}
			else if (item.type == "data/levelups")
			{
				Data.loadLevelUps(loader.content);
			}
		}
		
		private function onAllItemsLoadComplete(e:LoaderEvent):void 
		{
			if (_avatarsToLoad.length == 0)
			{
				loadNextState();
				return;
			}
			_avatarsLoadCounter = _avatarsToLoad.length;
			while (_avatarsToLoad.length > 0)
			{
				var factory:BaseFactory = new StarlingFactory();
				factory.addEventListener(Event.COMPLETE, onAvatarParseComplete);
				_skeletonNameMap[factory] = factory.parseData(_avatarsToLoad.shift()).name;
			}
		}
		
		private function onAvatarParseComplete(e:Event):void 
		{
			var factory:BaseFactory = e.currentTarget as BaseFactory;
			factory.removeEventListener(Event.COMPLETE, onAvatarParseComplete);
			Avatar.addFactory(factory, _skeletonNameMap[factory]);
			delete _skeletonNameMap[factory];
			_avatarsLoadCounter--;
			if (_avatarsLoadCounter == 0)
			{
				loadNextState();
			}
		}
		
		private function loadNextState():void 
		{
			Game.instance.stateManager.loadState(PlayState);
		}
		
		public function update():void 
		{
			
		}
		
		public function exit():void 
		{
		}
		
	}

}