package me.sangtian.rpg.framework 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class HealingEvent extends Event 
	{
		public static const HEALING:String = "healing";
		
		private var _previousHP:Number;
		private var _healing:Number;
		private var _critical:Boolean;
		
		public function get previousHP():Number 
		{
			return _previousHP;
		}
		
		public function get critical():Boolean 
		{
			return _critical;
		}
		
		public function get healing():Number 
		{
			return _healing;
		}
		
		public function HealingEvent(type:String, previousHP:Number, healing:Number, critical:Boolean = false) 
		{
			super(type);
			_previousHP = previousHP;
			_healing = healing;
			_critical = critical;
		}
		
	}

}