package me.sangtian.rpg.framework 
{
	import flash.geom.Rectangle;
	import me.sangtian.common.util.ObjectUtil;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Obstacle extends GameObject 
	{
		public function Obstacle(map:MapLayer, hitArea:Rectangle) 
		{
			super(map);
			this.hitArea = hitArea;
			showHitArea();
		}
		
	}

}