package me.sangtian.rpg.framework 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author tiansang
	 */
	public class CharacterStateMachine 
	{
		public static const STATE_DEAD:int = -1;
		public static const STATE_IDLE:int = 0;
		public static const STATE_MOVE:int = 1;
		public static const STATE_ATTACK:int = 2;
		public static const STATE_CAST:int = 3;
		public static const STATE_CHANNEL:int = 4;
		public static const STATE_STUN:int = 10;
		//public static const STATE_SILENCE:int = 13;
		
		public static const ACTION_START_IDLE:int = 0;
		public static const ACTION_START_MOVE:int = 1;
		public static const ACTION_START_ATTACK:int = 2;
		public static const ACTION_START_CAST:int = 3;
		public static const ACTION_START_CHANNEL:int = 4;
		public static const ACTION_UNDER_ATTACK:int = 5;
		public static const ACTION_START_STUN:int = 10;
		public static const ACTION_STOP_STUN:int = -10;
		//public static const ACTION_START_SILENCE:int = 13;
		//public static const ACTION_STOP_SILENCE:int = -13;
		public static const ACTION_KILL:int = -1;
		public static const ACTION_REVIVE:int = -2;

		private var _currentState:int = STATE_IDLE;
		
		private var _stateActionsMapping:Dictionary = new Dictionary();
		private var _actionToStateMapping:Dictionary = new Dictionary();
		
		public function get currentState():int 
		{
			return _currentState;
		}
		
		public function CharacterStateMachine() 
		{
			initStates();
		}
		
		private function initStates():void 
		{
			_stateActionsMapping[STATE_IDLE] = Vector.<int>(
				[ACTION_START_MOVE, ACTION_START_ATTACK, ACTION_START_CAST, ACTION_START_STUN, ACTION_KILL, ACTION_UNDER_ATTACK]
			);
			_stateActionsMapping[STATE_MOVE] = Vector.<int>(
				[ACTION_START_IDLE, ACTION_START_MOVE, ACTION_START_ATTACK, ACTION_START_CAST, ACTION_START_STUN, ACTION_KILL, ACTION_UNDER_ATTACK]
			);
			_stateActionsMapping[STATE_ATTACK] = Vector.<int>(
				[ACTION_START_IDLE, ACTION_START_MOVE, ACTION_START_ATTACK, ACTION_START_CAST, ACTION_START_STUN, ACTION_KILL, ACTION_UNDER_ATTACK]
			);
			_stateActionsMapping[STATE_CAST] = Vector.<int>(
				[ACTION_START_IDLE, ACTION_START_MOVE, ACTION_START_ATTACK, ACTION_START_CAST, ACTION_START_STUN, ACTION_KILL, ACTION_UNDER_ATTACK]
			);
			_stateActionsMapping[STATE_CHANNEL] = Vector.<int>(
				[ACTION_START_IDLE, ACTION_START_MOVE, ACTION_START_ATTACK, ACTION_START_CAST, ACTION_START_STUN, ACTION_KILL, ACTION_UNDER_ATTACK]
			);
			_stateActionsMapping[STATE_STUN] = Vector.<int>(
				[ACTION_STOP_STUN, ACTION_KILL, ACTION_UNDER_ATTACK]
			);
			_stateActionsMapping[STATE_DEAD] = Vector.<int>(
				[ACTION_REVIVE]
			);
			
			_actionToStateMapping[ACTION_START_IDLE] = STATE_IDLE;
			_actionToStateMapping[ACTION_START_MOVE] = STATE_MOVE;
			_actionToStateMapping[ACTION_START_ATTACK] = STATE_ATTACK;
			_actionToStateMapping[ACTION_START_CAST] = STATE_CAST;
			_actionToStateMapping[ACTION_START_CHANNEL] = STATE_CHANNEL;
			_actionToStateMapping[ACTION_START_STUN] = STATE_STUN;
			_actionToStateMapping[ACTION_STOP_STUN] = STATE_IDLE;
			_actionToStateMapping[ACTION_KILL] = STATE_DEAD;
			_actionToStateMapping[ACTION_REVIVE] = STATE_IDLE;
		}
		
		public function testAction(action:int):Boolean
		{
			var actions:Vector.<int> = _stateActionsMapping[currentState];
			if (actions == null)
				return false;
			return actions.indexOf(action) >= 0;
		}
		
		public function doAction(action:int):Boolean
		{
			if (testAction(action))
			{
				ASSERT(_actionToStateMapping[action] != null);
				var newState:int = _actionToStateMapping[action];
				//if (newState != _currentState)
				{
					_currentState = newState;
					return true;
				}
			}
			return false;
		}
		
	}

}