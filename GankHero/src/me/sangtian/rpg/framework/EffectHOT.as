package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author 
	 */
	public class EffectHOT extends SkillEffectBehavior 
	{
		
		public var totalHp:Number;
		protected var _hpPerTick:Number;
		
		override public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			_hpPerTick = totalHp / (duration / tickInteval);
		}
		
		override public function onTick(caster:CharacterObject, target:CharacterObject):void 
		{
			target.heal(_hpPerTick);
		}
	}

}