package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author 
	 */
	public class EffectInterruptableHOT extends EffectHOT 
	{
		
		override public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			super.onActivate(caster, target);
			target.addEventListener(DamageEvent.DAMAGE, onTargetDamage);
		}
		
		private function onTargetDamage(e:DamageEvent):void 
		{
			deactivateEffect(e.target as CharacterObject);
		}
		
		override public function onDeactivate(caster:CharacterObject, target:CharacterObject):void 
		{
			target.removeEventListener(DamageEvent.DAMAGE, onTargetDamage);
		}
	}

}