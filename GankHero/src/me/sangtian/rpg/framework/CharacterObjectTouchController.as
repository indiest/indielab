package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	import me.sangtian.common.logging.Log;
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.events.EventDispatcher;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.ColorMatrixFilter;
	/**
	 * ...
	 * @author tiansang
	 */
	public class CharacterObjectTouchController extends EventDispatcher
	{
		static public const EVENT_SWITCH_CURRENT:String = "switchCurrent";
		
		private static var _highlightFilter:ColorMatrixFilter = new ColorMatrixFilter();
		{
			_highlightFilter.adjustBrightness(0.5);
		}
		private var _map:MapLayer;
		private var _beganSelectedCharacter:CharacterObject;
		private var _currentCharacter:CharacterObject;
		private var _touchPoint:Point = new Point();
		private var _lastTouchPhase:String;
		private var _targetCharacter:CharacterObject;
		private var _availableTeams:Vector.<int> = new Vector.<int>();
		private var _highlightCharacter:CharacterObject;
		
		public function get currentCharacter():CharacterObject 
		{
			return _currentCharacter;
		}
		
		public function set currentCharacter(value:CharacterObject):void 
		{
			var oldCharacter:CharacterObject = _currentCharacter;
			
			if (_currentCharacter != null)
			{
				//_currentCharacter.scaleX = _currentCharacter.scaleY = 1.0;
			}
			_currentCharacter = value;
			if (_currentCharacter != null)
			{
				//_currentCharacter.scaleX = _currentCharacter.scaleY = 1.1;
			}
			
			if (_currentCharacter != oldCharacter)
			{
				dispatchEventWith(EVENT_SWITCH_CURRENT);
			}
		}
		
		public function get targetCharacter():CharacterObject 
		{
			return _targetCharacter;
		}
		
		public function get availableTeams():Vector.<int> 
		{
			return _availableTeams;
		}
		
		public function CharacterObjectTouchController() 
		{
		}
		
		public function initialize(map:MapLayer):void
		{
			_map = map;
			_map.container.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public function destroy():void
		{
			_map.container.removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function getCharacterUnderPoint(p:Point):CharacterObject
		{
			for each(var obj:GameObject in _map.objects)
			{
				if (obj is CharacterObject)// && touch.isTouching(obj))
				{
					if (obj.bounds.containsPoint(p))
					{
						return obj as CharacterObject;
					}
				}
			}
			return null;
		}
		
		private function onTouch(e:TouchEvent):void 
		{
			var touch:Touch = e.touches[0];
			var touchedCharacter:CharacterObject;
//			if (Log.isDebugEnabled && touch.phase != "hover")
//				Log.debug(touch.phase);

			if (_highlightCharacter != null)
			{
				_highlightCharacter.filter = null;
				_highlightCharacter = null;
			}

			// TODO: optimization (dont have to call every time)
			touch.getLocation(_map.container, _touchPoint);
			//_touchPoint.x = touch.globalX;
			//_touchPoint.y = touch.globalY;
			
			if (touch.phase == TouchPhase.BEGAN)
			{
				// TODO: optimize with touch area(rectangle) detection
				touchedCharacter = getCharacterUnderPoint(_touchPoint);
				if (touchedCharacter != null && isTeamAvailable(touchedCharacter))
				{
					_beganSelectedCharacter = touchedCharacter;
					_map.graphics.lineStyle(3.0, 0xffffff);
				}
				else
				{
					_beganSelectedCharacter = null;
				}
			}
			else if (touch.phase == TouchPhase.MOVED)
			{
				if (_beganSelectedCharacter != null)
				{
					_map.graphics.clear();
					_map.graphics.moveTo(_beganSelectedCharacter.x, _beganSelectedCharacter.y);
					_map.graphics.lineTo(_touchPoint.x, _touchPoint.y);
					
					touchedCharacter = getCharacterUnderPoint(_touchPoint);
					if (touchedCharacter != null)
					{
						touchedCharacter.filter = _highlightFilter;
						_highlightCharacter = touchedCharacter;
					}
				}
			}
			else if (touch.phase == TouchPhase.ENDED)
			{
				_map.graphics.clear();
				touchedCharacter = getCharacterUnderPoint(_touchPoint);
				Log.debug("Touched", _touchPoint, touchedCharacter);
				// swipe to move/attack character without switching current character
				if (_lastTouchPhase == TouchPhase.MOVED)
				{
					if (_beganSelectedCharacter != null)
					{
						if (touchedCharacter != null)
						{
							_beganSelectedCharacter.attack(touchedCharacter);
						}
						else
						{
							_beganSelectedCharacter.moveTo(_touchPoint);
						}
					}
				}
				else
				{
					if (touchedCharacter == null)
					{
						if (currentCharacter != null)
						{
							// move current character
							currentCharacter.moveTo(_touchPoint);
						}
					}
					else
					{
						if (isTeamAvailable(touchedCharacter))
						{
							// switch current character
							currentCharacter = touchedCharacter;
						}
						else
						{
							if (currentCharacter != null)
							{
								// if current character is selected, select an enemy to attack
								setAttackTarget(touchedCharacter);
							}
						}
					}
				}
			}
			
			_lastTouchPhase = touch.phase;
		}
		
		private function setAttackTarget(touchedCharacter:CharacterObject):void 
		{
			_targetCharacter = touchedCharacter;
			// TODO: heal
			currentCharacter.attack(touchedCharacter);
		}
		
		private function isTeamAvailable(co:CharacterObject):Boolean 
		{
			return availableTeams.indexOf(co.teamId) >= 0
		}
		
	}

}