package me.sangtian.rpg.framework 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class CharacterType extends Enum 
	{
		public function CharacterType(index:int)
		{
			super(index, true);
		}
		
		public static const SELF:CharacterType = new CharacterType(1);
		public static const ALLY:CharacterType = new CharacterType(2);
		public static const ENEMY:CharacterType = new CharacterType(4);
		public static const ALL:CharacterType = new CharacterType(7);
	}

}