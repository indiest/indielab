package me.sangtian.rpg.framework 
{
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.Time;
	/**
	 * ...
	 * @author tiansang
	 */
	public class MapSpawner
	{
		public var startTime:Number;
		public var interval:Number;
		public var count:int;
		public var characterId:int;
		public var teamId:int;
		public var x:Number;
		public var y:Number;
		public var aiType:String;
		public var data:Object;
		
		public function get hasAI():Boolean
		{
			return aiType != "None";
		}
		
		private var _map:MapLayer;
		private var _spawnTiming:Number = 0;
		
		public function MapSpawner(map:MapLayer, data:Object) 
		{
			_map = map;
			this.data = data;
			startTime = data.startTime;
			interval = data.interval;
			count = data.count;
			characterId = data.characterId;
			teamId = data.teamId;
			aiType = data.aiType;
			x = data.x;
			y = data.y;
		}
		
		public function update():void
		{
			if (count == 0)
				return;
			if (_map.elapsedTime >= startTime)
			{
				_spawnTiming -= Time.deltaSecond;
				if (_spawnTiming <= 0)
				{
					spawn();
					count--;
					_spawnTiming = interval;
				}
			}
		}
		
		public function spawn():CharacterObject 
		{
			var co:CharacterObject = new CharacterObject(_map, Data.getCharacter(characterId));
			co.load(data);
			co.x = x;
			co.y = y;
			co.teamId = teamId;
			_map.addObject(co);
			if (hasAI)
			{
				var aiProperties:Object = null;
				try
				{
					aiProperties = JSON.parse(String(data.aiProperties));
				}
				catch (err:Error)
				{
					Log.info("Unable to parse property for AI '" + aiType + "':", data.aiProperties);
				}
				var aiController:AIController = Data.createAIController(aiType, aiProperties);
				aiController.co = co;
				_map.addAIController(aiController);
			}
			else
			{
				var item:Item = new Item(Data.getItemData(3001));
				item.consumableTimes = 2;
				co.inventory.addItem(item);
				co.inventory.addItem(new Item(Data.getItemData(1001)));
			}
			_map.dispatchEventWith(MapEvent.SPAWN, false, co);
			return co;
		}
	}

}