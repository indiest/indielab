package me.sangtian.rpg.framework 
{
	import starling.display.Image;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author tiansang
	 */
	public class MissileObject extends GameObject 
	{
		private var _target:GameObject;
		private var _speed:Number;
		// parameters:
		//	target:GameObject
		public var hitCallback:Function;
		
		public function MissileObject(map:MapLayer, target:GameObject, speed:Number, hitCallback:Function = null) 
		{
			super(map);
			_target = target;
			_speed = speed;
			this.hitCallback = hitCallback;
			addChild(new Image(Texture.fromColor(8, 8, 0xffff0000)));
		}
		
		override public function update():void 
		{
			var scale:Number = moveStep(_target.position, _speed);
			if (scale == 0 || scale == 1)
			{
				if (hitCallback != null)
				{
					hitCallback(this, _target);
				}
				destroy();
			}
		}
	}

}