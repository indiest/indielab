package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class Inventory extends ItemContainer 
	{
		private var _co:CharacterObject;
		
		public function Inventory(co:CharacterObject, size:uint) 
		{
			super(size);
			_co = co;
		}
		
		override public function addItem(item:Item, index:int = -1):Boolean 
		{
			var result:Boolean = super.addItem(item, index);
			if (result)
			{
				_co.properties.add(item.properties);
				// FIXME: already having the item will cause error
				for each(var skillId:int in item.data.skillIds)
				{
					_co.learnSkill(skillId);
				}
				_co.dispatchEvent(new InventoryEvent(InventoryEvent.ADD_ITEM, index, item.data.id));
			}
			return result;
		}
		
		override public function removeItem(index:int):Item 
		{
			var item:Item = super.removeItem(index);
			if (item)
			{
				_co.properties.sub(item.properties);
				for each(var skillId:int in item.data.skillIds)
				{
					_co.forgetSkill(skillId);
				}
				_co.dispatchEvent(new InventoryEvent(InventoryEvent.REMOVE_ITEM, index, item.data.id));
			}
			return item;
		}
		
		public function update():void
		{
			for (var i:uint = 0; i < size; i++)
			{
				var item:Item = getItem(i);
				if (item != null)
				{
					item.update();
				}
			}
		}
	}

}