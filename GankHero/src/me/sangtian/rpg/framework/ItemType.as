package me.sangtian.rpg.framework 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class ItemType extends Enum 
	{
		
		public function ItemType(index:int)
		{
			super(index);
		}
		
		public static const WEAPON:ItemType = new ItemType(1);
		public static const ARMOR:ItemType = new ItemType(2);
		public static const CONSUMABLE:ItemType = new ItemType(3);
	}

}