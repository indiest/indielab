package me.sangtian.rpg.framework.ui 
{
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class IconButton extends Button 
	{
		private var _layer1:Sprite = new Sprite();
		private var _layer2:Sprite = new Sprite();
		private var _cooldownCover:Image;
		private var _countText:TextField;
		
		public function IconButton(upState:Texture) 
		{
			super(upState);
			addChild(_layer1);
			addChild(_layer2);
			_cooldownCover = new Image(Texture.fromColor(width, height, 0x80000000));
			_layer1.addChild(_cooldownCover);
			clearCooldown();
		}
		
		public function clearCooldown():void
		{
			_cooldownCover.visible = false;
			enabled = true;
		}
		
		public function updateCooldown(coolingTime:Number, cooldown:Number):void
		{
			_cooldownCover.visible = true;
			_cooldownCover.height = this.height * coolingTime / cooldown;
			_cooldownCover.y = upState.height - _cooldownCover.height;
			enabled = (coolingTime <= 0);
		}
		
		public function updateCount(count:uint, color:uint = 0xffffff, fontSize:uint = 9):void
		{
			if (_countText == null)
			{
				_countText = new TextField(width, height, count.toString());
				_countText.hAlign = HAlign.RIGHT;
				_countText.vAlign = VAlign.BOTTOM;
				_layer2.addChild(_countText);
			}
			if (count == 0)
			{
				_countText.visible = false;
			}
			else
			{
				_countText.visible = true;
				_countText.color = color;
				_countText.fontSize = fontSize;
				_countText.text = count.toString();
			}
		}
	}

}