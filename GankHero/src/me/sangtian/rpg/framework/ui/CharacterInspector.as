package me.sangtian.rpg.framework.ui 
{
	import me.sangtian.rpg.framework.Character;
	import me.sangtian.rpg.framework.CharacterObject;
	import me.sangtian.rpg.framework.CharacterProperties;
	import me.sangtian.rpg.framework.SkillEffect;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class CharacterInspector extends Sprite 
	{
		private var _textInfo:TextField;
		
		public function CharacterInspector(width:int, height:int) 
		{
			_textInfo = new TextField(width, height, "");
			_textInfo.color = 0xffffff;
			_textInfo.hAlign = HAlign.LEFT;
			_textInfo.vAlign = VAlign.TOP;
			addChild(_textInfo);
		}
		
		public function update(co:CharacterObject):void
		{
			var str:String = co.data.name + "(Lv." + co.level + ")\nExp:" + co.exp + "/" + co.getLevelUpExp();
			for each(var name:String in CharacterProperties.propertyNames)
			{
				str += "\n" + getDisplayName(name) + ":" + getDisplayValue(co.properties[name]);
			}
			
			for each(var effect:SkillEffect in co.effects)
			{
				str += "\nEffect:" + effect.name + "(" + getDisplayValue(effect.remainingTime) + ")";
			}
			_textInfo.text = str;
		}
		
		private function getDisplayValue(value:*):* 
		{
			if (value is Number)
				return (value as Number).toFixed(1);
			return value;
		}
		
		private function getDisplayName(name:String):String 
		{
			return name;
		}
	}

}