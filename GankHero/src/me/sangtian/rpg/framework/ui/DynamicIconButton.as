package me.sangtian.rpg.framework.ui 
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.display.ContentDisplay;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author tiansang
	 */
	public class DynamicIconButton extends IconButton 
	{
		[Embed(source="/../assets/image/skill_slot.png")]
		private static const EMPTY:Class;
		private static var DEFAULT_TEXTURE:Texture = Texture.fromBitmap(new EMPTY());
		
		private static var _loaderQueue:LoaderMax = new LoaderMax({autoLoad: true});
		
		public function DynamicIconButton(defaultTexture:Texture = null) 
		{
			super(defaultTexture || DEFAULT_TEXTURE);
		}
		
		public function loadIcon(url:String):void
		{
			_loaderQueue.append(new ImageLoader(url, { onComplete: onLoadComplete } ));
		}
		
		private function onLoadComplete(e:LoaderEvent):void
		{
			var content:ContentDisplay = e.target.content;
			upState = downState = Texture.fromBitmap(content.rawContent);
		}
	}

}