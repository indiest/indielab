package me.sangtian.rpg.framework.ui 
{
	import flash.utils.Dictionary;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class IconButtonBar extends Sprite 
	{
		private var _buttons:Dictionary = new Dictionary();
		
		public function IconButtonBar() 
		{
			
		}
		
		public function addButton(id:*, button:IconButton):void
		{
			button.x = width;
			button.y = 0;
			_buttons[id] = button;
			addChild(button);
		}
		
		public function getButton(id:*):IconButton
		{
			return _buttons[id];
		}
		
		public function getIdByButton(button:IconButton):*
		{
			for (var id:* in _buttons)
			{
				if (_buttons[id] == button)
					return id;
			}
			return null;
		}
	}

}