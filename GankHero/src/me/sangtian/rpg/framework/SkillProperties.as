package me.sangtian.rpg.framework 
{
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class SkillProperties 
	{
		public var isPassitive:Boolean;
		public var cooldown:Number;
		public var targetCharacterType:CharacterType;
		public var castTargetType:CastTargetType;
		public var castTargetMaxNumber:uint;
		public var castDistance:Number;
		public var castEffectRange:Number;
		public var casterPropertyCost:CharacterProperties;
		public var castTime:Number;
		public var channelTime:Number;
		public var effects:Vector.<SkillEffectBehavior>;
		
		private static var _propertyNames:Vector.<String>;
		
		public static function get propertyNames():Vector.<String> 
		{
			return _propertyNames;
		}
		
		public function SkillProperties() 
		{
			if (_propertyNames == null)
			{
				_propertyNames = ObjectUtil.getOwnPropertyNames(this);
				//trace(_propertyNames);
			}
		}
		
		public function clone():SkillProperties 
		{
			var prop:SkillProperties = new SkillProperties();
			for each(var key:String in propertyNames)
			{
				prop[key] = this[key];
			}
			return prop;
		}
		
	}

}