package me.sangtian.rpg.framework 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class InventoryEvent extends Event 
	{
		public static const ADD_ITEM:String = "addItem";
		public static const REMOVE_ITEM:String = "removeItem";
		public static const COUNT_CHANGE:String = "countChange";
		
		private var _index:uint;
		private var _itemId:int;
		
		public function get index():uint 
		{
			return _index;
		}
		
		public function get itemId():int 
		{
			return _itemId;
		}
		
		public function InventoryEvent(type:String, index:uint, itemId:int) 
		{
			super(type);
			_index = index;
			_itemId = itemId;
		}
		
	}

}