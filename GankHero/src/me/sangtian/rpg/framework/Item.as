package me.sangtian.rpg.framework 
{
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Item 
	{
		private var _data:ItemData;
		private var _level:uint;
		private var _properties:CharacterProperties;
		private var _consumableTimes:int;
		
		public function get data():ItemData 
		{
			return _data;
		}
		
		public function get properties():CharacterProperties 
		{
			return _properties;
		}
		
		public function get level():uint 
		{
			return _level;
		}
		
		public function get consumableTimes():int 
		{
			return _consumableTimes;
		}
		
		public function set consumableTimes(value:int):void 
		{
			_consumableTimes = value;
		}
		
		public function Item(data:ItemData) 
		{
			ASSERT(data != null);
			_data = data;
			_consumableTimes = data.consumableTimes;
			resetLevel();
		}
		
		private function resetLevel(value:uint = 1):void
		{
			_level = 1;
			if (data.levelProperties.length > 0)
			{
				_properties = data.levelProperties[0].clone();
				upgradeLevel(value - 1);
			}
			else
			{
				_properties = new CharacterProperties();
			}
		}
		
		public function upgradeLevel(value:uint = 1):void
		{
			if (value == 0)
				return;
			ASSERT(level + value <= data.levelProperties.length, "Item is at maximum level");
			for (var i:int = 0; i < value; i++)
			{
				ObjectUtil.applyProperties(_properties, data.levelProperties[level + i]);
			}
			_level += value;
		}
		
		public function update():void
		{
			// behavior update
		}
	}

}