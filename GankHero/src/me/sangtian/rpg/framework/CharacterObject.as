package me.sangtian.rpg.framework 
{
	import dragonBones.Armature;
	import dragonBones.factorys.StarlingFactory;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	import me.sangtian.common.util.Random;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.filters.GrayscaleFilter;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
    [Event(name="gainExp", type="me.sangtian.rpg.framework.CharacterEvent")]
    [Event(name="levelUp", type="me.sangtian.rpg.framework.CharacterEvent")]
    [Event(name="addItem", type="me.sangtian.rpg.framework.InventoryEvent")]
    [Event(name="removeItem", type="me.sangtian.rpg.framework.InventoryEvent")]
    [Event(name="countChange", type="me.sangtian.rpg.framework.InventoryEvent")]
    [Event(name="damage", type="me.sangtian.rpg.framework.DamageEvent")]
    [Event(name="healing", type="me.sangtian.rpg.framework.HealingEvent")]
	/**
	 * ...
	 * @author tiansang
	 */
	public class CharacterObject extends GameObject 
	{		
		// static data
		private var _data:Character;
		// dynamic data
		public var properties:CharacterProperties;
		private var _level:uint = 1;
		private var _exp:int = 0;
		public var skillPoints:uint = 0;
		// key = skillId:int, value = Skill
		public var skills:Dictionary = new Dictionary();
		public var effects:Vector.<SkillEffect> = new Vector.<SkillEffect>();
		private var _stateMachine:CharacterStateMachine = new CharacterStateMachine();
		private var _avatar:Avatar;
		private var _inventory:Inventory;
		public var gold:uint = 0;
		
		private var _teamId:uint;
		public var visibilityGroup:int = 0;
		
		override public function get hitArea():Rectangle 
		{
			_hitArea.setTo(x, y, properties.hitTestWidth, properties.hitTestHeight);
			return _hitArea;
		}
		
		public function get data():Character 
		{
			return _data;
		}
		
		public function get stateMachine():CharacterStateMachine 
		{
			return _stateMachine;
		}
		
		public function get teamId():uint 
		{
			return _teamId;
		}
		
		public function set teamId(value:uint):void 
		{
			_teamId = value;
			_avatar.tint(Team.getColor(_teamId));
		}
		
		public function get avatar():Avatar 
		{
			return _avatar;
		}
		
		public function get inventory():Inventory 
		{
			return _inventory;
		}
		
		public function get isDead():Boolean
		{
			return properties.hp <= 0;
		}
		
		public function get level():uint 
		{
			return _level;
		}
		
		public function get exp():int 
		{
			return _exp;
		}
		
		public function CharacterObject(map:MapLayer, data:Character) 
		{
			super(map);
			_data = data;
			resetLevel();
			
			/*
			var defaultImage:Image = new Image(Texture.fromColor(
				properties.hitTestWidth, properties.hitTestHeight,
				//0xff000000 + Random.rangeInt(0, 0xffffff)
				team.color
			));
			defaultImage.x = -defaultImage.width * 0.5;
			defaultImage.y = -defaultImage.height * 0.5;
			addChild(defaultImage);
			*/
			
			_avatar = new Avatar(this, data.avatarData);
			_inventory = new Inventory(this, data.inventoryCap);
			
			idle();
		}
		public function load(save:Object):void
		{
			resetLevel(save.level);
			gainExp(save.exp);
			this.skillPoints = save.skillPoints;
			properties.hp = save.hp || properties.hpMax;
			properties.mp = save.mp || properties.mpMax;
			
			for each(var skillId:int in save.skills)
			{
				learnSkill(skillId);
			}
			
			// TODO: load effects
		}
		
		public function resetLevel(value:uint = 1):void
		{
			_level = 1;
			properties = data.levelPropertyBonus[0].clone();
			upgradeLevel(value - 1);
		}
		
		public function upgradeLevel(value:uint = 1):void
		{
			if (value == 0)
				return;
			ASSERT(_level + value <= data.levelCap);
			for (var i:int = 0; i < value; i++)
			{
				ObjectUtil.applyProperties(properties, data.levelPropertyBonus[i + level]);
			}
			_level += value;
			skillPoints += value;
			dispatchEventWith(CharacterEvent.LEVEL_UP);
		}
		
		public function gainExp(value:uint):void
		{
			// Can't gain more exp since character has reached highest level
			if (level >= data.levelCap)
				return;
			_exp += value;
			dispatchEventWith(CharacterEvent.GAIN_EXP);
			var levelUpExp:uint;
			while ((levelUpExp = getLevelUpExp()) && _exp >= levelUpExp)
			{
				_exp -= levelUpExp;
				upgradeLevel();
				if (level == data.levelCap)
					break;
			}
		}
		
		public function getLevelUpExp():uint 
		{
			if (level == data.levelCap)
				return 0;
			return data.levelups[level - 1].exp;
		}
		
		public function getDyingExp():uint
		{
			return data.levelups[level - 1].dyingExp;
		}
		
		public function getDyingGold():uint
		{
			return data.levelups[level - 1].dyingGold;
		}
		
		internal function forgetSkill(skillId:int):void
		{
			var skill:Skill = skills[skillId];
			ASSERT(skill != null, "Character does not have skill:" + skillId);
			skill.resetPassiveEffects();
			delete skills[skillId];
		}
		
		internal function learnSkill(skillId:int):Skill
		{
			ASSERT(skills[skillId] == null, "Character has already learnt skill:" + skillId);
			var skill:Skill = new Skill(this, Data.getSkillData(skillId));
			skills[skillId] = skill;
			return skill;
		}
		
		public function upgradeSkill(skillId:int):void
		{
			ASSERT(skillPoints > 0, "Not enough skill point to upgrade");
			skillPoints--;
			var skill:Skill = skills[skillId];
			if (skill != null)
			{
				//forgetSkill(skillId);
				// keep the cooldown
				//learnSkill(skill.properties.nextLevelSkillId).coolingTime = skill.coolingTime;
				skill.upgradeLevel();
			}
			else
			{
				// upgrade to first level
				ASSERT(data.availableSkillIds.indexOf(skillId) >= 0);
				learnSkill(skillId);
			}
		}
		
		public function getDistance(target:GameObject):Number
		{
			var dx:Number = x - target.x;
			var dy:Number = y - target.y;
			return Math.sqrt(dx * dx + dy * dy);
		}
		
		public function getRelativeCharacterType(target:CharacterObject):CharacterType 
		{
			if (target == this)
				return CharacterType.SELF;
			else if (target.teamId == this.teamId)
				return CharacterType.ALLY;
			else
				return CharacterType.ENEMY;
		}
		
		public function isAlly(target:CharacterObject):Boolean
		{
			return getRelativeCharacterType(target) == CharacterType.ALLY;
		}
		
		public function isEnemy(target:CharacterObject):Boolean
		{
			return getRelativeCharacterType(target) == CharacterType.ENEMY;
		}
		
		public function canSee(target:CharacterObject):Boolean
		{
			return isAlly(target) || target.visibilityGroup == this.visibilityGroup;
		}
		
		// Returns all continucous effects
		public function applySkillEffect(target:CharacterObject, skill:Skill):Vector.<SkillEffect> 
		{
			//ASSERT(skill.properties.castDistance + skill.properties.castEffectRange);
			var effects:Vector.<SkillEffect> = new Vector.<SkillEffect>();
			for each(var behavior:SkillEffectBehavior in skill.properties.effects)
			{
				var effect:SkillEffect = applyEffect(target, behavior);
				if (effect != null)
					effects.push(effect);
			}
			return effects;
		}
		
		// Returns null if the effect is not continuous
		private function applyEffect(target:CharacterObject, behavior:SkillEffectBehavior):SkillEffect
		{
			// TODO: check effect conflicts
			
			if (behavior.duration > 0)
			{
				var effect:SkillEffect = new SkillEffect(behavior, this, target);
				target.effects.push(effect);
				return effect;
			}
			else
			{
				behavior.onActivate(this, target);
				return null;
			}
		}
		
		public function idle():void
		{
			_moveTarget = null;
			_attackTarget = null;
			_stateMachine.doAction(CharacterStateMachine.ACTION_START_IDLE);
		}
		
		public function moveTo(target:Point):void
		{
			if (_stateMachine.doAction(CharacterStateMachine.ACTION_START_MOVE))
			{
				if (_moveTarget == null)
					_moveTarget = target.clone();
				else
					_moveTarget.copyFrom(target);
				
				//if (data.id == 101)
				//{
					//Log.debug("Moving to", _moveTarget);
				//}
			}
		}
		
		public function attack(target:CharacterObject):void
		{
			if (!isEnemy(target))
			{
				Log.info("Can't attack oneself or ally.");
				return;
			}
			if (_stateMachine.doAction(CharacterStateMachine.ACTION_START_ATTACK))
			{
				_attackTarget = target;
			}
		}
		
		public function cast(skillId:int, target:CastTarget):Boolean 
		{
			var skill:Skill = skills[skillId];
			ASSERT(skill != null, "Character does not have skill:" + skillId);
			ASSERT(skill.properties.isPassitive == false, "Unable to cast passitive skill:" + skillId);
			ASSERT(skill.coolingTime == 0, "Skill is still in cooling");
			
			// check own state
			if (_stateMachine.testAction(CharacterStateMachine.ACTION_START_CAST) == false)
			{
				Log.info("You can't cast now");
				return false;
			}
			
			// TODO: check target's state
			
			// check cost
			var result:String = properties.testCost(skill.properties.casterPropertyCost);
			if (result != null)
			{
				Log.info("No enough", result);
				return false;
			}
			
			// check target
			if (target.check(this, skill) == false)
			{
				return false;
			}
			
			_castRemainingTime = skill.properties.castTime;
			_castTarget = target;
			_castSkill = skill;
			
			// Start cooldown
			_castSkill.startCooldown();
			
			return _stateMachine.doAction(CharacterStateMachine.ACTION_START_CAST);
		}
		
		public function useItem(inventoryIndex:int, target:CastTarget):void
		{
			var item:Item = inventory.getItem(inventoryIndex);
			ASSERT(item != null, "Item does not exist, inventory index:" + inventoryIndex);
			ASSERT(item.data.castableSkillId != 0, "Item can not be used");
			ASSERT(item.data.type != ItemType.CONSUMABLE || item.consumableTimes > 0, "Item is used up");
			// only the first skill of an item is castable
			if (cast(item.data.castableSkillId, target))
			{
				if (item.data.type == ItemType.CONSUMABLE)
				{
					item.consumableTimes--;
					if (item.consumableTimes == 0)
					{
						inventory.removeItem(inventoryIndex);
					}
					else
					{
						dispatchEvent(new InventoryEvent(InventoryEvent.COUNT_CHANGE, inventoryIndex, item.data.id));
					}
				}
			}
		}
		
		override public function update():void 
		{
			if (!isDead)
			{
				for each(var skill:Skill in skills)
				{
					skill.update();
				}
				for (var i:int = effects.length - 1; i >= 0; i--)
				{
					var effect:SkillEffect = effects[i];
					effect.update();
					if (!effect.active)
						effects.splice(i, 1);
				}
				properties.update();
			}
			
			switch(_stateMachine.currentState)
			{
				case CharacterStateMachine.STATE_IDLE:
					updateIdle();
					break;
				case CharacterStateMachine.STATE_MOVE:
					updateMove();
					break;
				case CharacterStateMachine.STATE_ATTACK:
					updateAttack();
					break;
				case CharacterStateMachine.STATE_CAST:
					updateCast();
					break;
				case CharacterStateMachine.STATE_CHANNEL:
					updateChannel();
					break;
				case CharacterStateMachine.STATE_STUN:
					updateStun();
					break;
				case CharacterStateMachine.STATE_DEAD:
					updateDead();
					break;
				default:
					break;
			}
			
			_avatar.update();
		}
		
		protected function updateIdle():void 
		{
			_avatar.play("idle");		
			_avatar.timeScale = 1;
		}
		
		private var _moveTarget:Point;
		protected function updateMove():void 
		{
			if (_moveTarget != null)
			{
				updateOrientation(_moveTarget);
				var scale:Number = moveStep(_moveTarget, properties.getActualMoveSpeed());
				//if (data.id == 101)	Log.debug("Move scale:", scale);
				if (scale == 1)
				{
					_moveTarget = null;
					_stateMachine.doAction(CharacterStateMachine.ACTION_START_IDLE);
				}
			}
		}
		
		private function updateOrientation(target:Point):void 
		{
			if (target.x - this.x < 0)
				scaleX = -1;
			else
				scaleX = 1;
		}
		
		override public function moveStep(target:Point, speed:Number):Number 
		{
			_avatar.play("move");
			_avatar.timeScale = 1 + properties.moveSpeedAmendRatio;
			return super.moveStep(target, speed);
		}
		
		private var _attackTarget:CharacterObject;
		private var _attackInterval:Number = 0;		
		public function get attackTarget():CharacterObject 
		{
			return _attackTarget;
		}
		protected function updateAttack():void 
		{
			if (_attackTarget != null)
			{
				updateOrientation(_attackTarget.position);
				if (_attackTarget._stateMachine.testAction(CharacterStateMachine.ACTION_UNDER_ATTACK) == false)
				{
					// target is not attackable (dead/invincible)
					_stateMachine.doAction(CharacterStateMachine.ACTION_START_IDLE);
					return;
				}
				
				if (_attackTarget.getDistance(this) > properties.attackRange)
				{
					// target is too far away, move a step to it, but no need to enter move state
					moveStep(_attackTarget.position, properties.getActualMoveSpeed());
					//_attackInterval = 0;
				}
				else
				{
					_attackInterval -= Time.deltaSecond;
					if (_attackInterval <= 0)
					{
						// ranged attack: fire missle
						if (properties.missileSpeed > 0)
						{
							fireMissle(_attackTarget, properties.missileSpeed);
						}
						else
						{
							calculateAttack(_attackTarget);
						}
						_attackInterval = properties.getAttackInterval();
						
						_avatar.play("attack");			
						_avatar.timeScale = 1 + properties.attackSpeedAmendRatio;
					}
				}
			}
		}
		
		private function fireMissle(target:CharacterObject, speed:Number):void 
		{
			var missle:MissileObject = new MissileObject(map, target, speed, onMissleHit);
			missle.x = x;
			missle.y = y;
			map.addObject(missle);
		}
		
		private function onMissleHit(missle:MissileObject, target:CharacterObject):void 
		{
			calculateAttack(target);
		}
		
		private function calculateAttack(target:CharacterObject):void
		{
			var ap:int = Random.rangeInt(properties.attackMin, properties.attackMax);
			var out:Number = (ap + properties.attackAmendValue) * (1 + properties.attackAmendRatio);
			var critical:Boolean = Random.randomBoolean(properties.critChance);
			if (critical)
				out *= (1 + properties.critBonusRatio);
			var damage:Number = out * (1 - target.properties.defenceAmendRatio) - target.properties.defenceAmendValue;
			target.takeDamage(this, damage, DamageType.Physical, critical);
		}
		
		private function onDying():void
		{
			_avatar.play("dead", false, onDead);
			_hitArea.setEmpty();
		}
		
		private function onDead():void
		{
			//removeFromParent(true);
			destroy();
			_avatar.dispose();
		}
		
		public function takeDamage(attacker:CharacterObject, damage:Number, type:DamageType, critical:Boolean = false):void 
		{
			var previousHP:Number = properties.hp;
			properties.hp = MathUtil.clamp(properties.hp - damage, 0, properties.hpMax);

			dispatchEvent(new DamageEvent(DamageEvent.DAMAGE, previousHP, damage, type, critical));
			
			if (properties.hp == 0)
			{
				if (_stateMachine.doAction(CharacterStateMachine.ACTION_KILL))
				{
					attacker.gainExp(this.getDyingExp());
					attacker.gold += this.getDyingGold();
					onDying();
				}
			}
		}
		
		public function heal(healing:Number, critical:Boolean = false):void 
		{
			var previousHP:Number = properties.hp;
			properties.hp = MathUtil.clamp(properties.hp + healing, 0, properties.hpMax);
			dispatchEvent(new HealingEvent(HealingEvent.HEALING, previousHP, healing, critical));
		}
		
		
		private var _castRemainingTime:Number;
		private var _castTarget:CastTarget;
		private var _castSkill:Skill;
		protected function updateCast():void 
		{
			_castRemainingTime -= Time.deltaSecond;
			if (_castRemainingTime > 0)
			{
				return;
			}
			
			// play visual effects
			map.particleSystemManager.play(_castSkill.data.particleData, x, y);
			
			// perform cost
			properties.sub(_castSkill.properties.casterPropertyCost);
			
			if (_castSkill.properties.channelTime > 0)
			{
				// TODO: start channeling
				if (_stateMachine.doAction(CharacterStateMachine.ACTION_START_CHANNEL))
				{
				}
			}
			else
			{
				// apply effects
				for each(var co:CharacterObject in _castTarget.getApplicableCharacters())
				{
					applySkillEffect(co, _castSkill);
				}
				
				if (_attackTarget != null)
				{
					attack(_attackTarget);
				}
				else
				{
					idle();
				}
			}
		}
		
		protected function updateChannel():void 
		{
			
		}
		
		protected function updateStun():void 
		{
			_avatar.stop();
		}
		
		protected function updateDead():void 
		{
			//_avatar.stop();
		}
		
		public function toString():String
		{
			return data.name + data.id;
		}
	}

}
