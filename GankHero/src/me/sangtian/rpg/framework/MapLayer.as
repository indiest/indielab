package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	import me.sangtian.rpg.framework.GameObject;
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.display.Graphics;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EventDispatcher;
	import starling.textures.Texture;
	
    [Event(name="spawn", type="starling.events.Event")]
    [Event(name="gameOver", type="starling.events.Event")]
	/**
	 * ...
	 * @author tiansang
	 */
	public class MapLayer extends EventDispatcher
	{
		private var _container:DisplayObjectContainer;
		private var _bgLayer:Sprite = new Sprite();
		// For easier sorting, put all Display Object in the object layer
		//private var _spriteLayer:Sprite = new Sprite();
		private var _objectLayer:Sprite = new Sprite();
		private var _objects:Vector.<GameObject> = new Vector.<GameObject>();
		private var _destroiedObjects:Vector.<GameObject> = new Vector.<GameObject>();
		
		private var _particleLayer:Sprite = new Sprite();
		private var _particleSystemManager:ParticleSystemManager;
		
		private var _graphicsLayer:Sprite = new Sprite();
		private var _graphics:Graphics = new Graphics(_graphicsLayer);
		
		private var _startTime:int;
		private var _spawners:Vector.<MapSpawner> = new Vector.<MapSpawner>();
		private var _triggers:Vector.<MapTrigger> = new Vector.<MapTrigger>();
		
		private var _aiControllers:Vector.<AIController> = new Vector.<AIController>();
		
		private var _moveArea:Rectangle = new Rectangle();
		
		public function get elapsedTime():Number
		{
			return (getTimer() - _startTime) * 0.001;
		}
		
		public function get container():DisplayObjectContainer 
		{
			return _container;
		}
		
		public function get objects():Vector.<GameObject> 
		{
			return _objects;
		}
		
		public function get particleSystemManager():ParticleSystemManager 
		{
			return _particleSystemManager;
		}
		
		public function get graphics():Graphics 
		{
			return _graphics;
		}
		
		public function get moveArea():Rectangle 
		{
			return _moveArea;
		}
		
		public function MapLayer(parent:DisplayObjectContainer) 
		{
			_container = new Sprite();
			parent.addChild(_container);

			_container.addChild(_bgLayer);
			_container.addChild(_graphicsLayer);
			//_container.addChild(_spriteLayer);
			_container.addChild(_objectLayer);
			
			_particleSystemManager = new ParticleSystemManager(_particleLayer);
			_container.addChild(_particleLayer);
		}
		
		public function load(xml:XML):void
		{
			var spriteEntries:Array = [];
			var entry:XML;
			for each(entry in xml.spriteEntries.sprite)
			{
				spriteEntries[entry.@idx] = entry;
			}
			
			var layers:XMLList = xml.layers.group[0].elements();
			var sprite:XML;
			var shape:XML;
			var data:Object;
			for (var i:int = 0; i < layers.length(); i++)
			{
				var layer:XML = layers[i];
				switch(layer.@name.toString())
				{
					case "Bg":
						setBg(new Image(ResourceManager.getTexture(layer.@file)));
						break;
						
					case "Sprites":
						for each(sprite in layer.sprite)
						{
							entry = spriteEntries[sprite.@idx];
							var image:Image = new Image(ResourceManager.getTexture(entry.@image));
							image.scaleX = Number(sprite.@scaleX) * (sprite.@flipped == true ? -1 : 1);
							image.scaleY = Number(sprite.@scaleY);
							image.rotation = MathUtil.angleToRadian(sprite.@angle);
							image.pivotX = entry.@anchorX;
							image.pivotY = entry.@anchorY;
							image.x = int(sprite.@x) + image.pivotX;
							image.y = int(sprite.@y) + image.pivotY;
							addSprite(image);
						}
						break;
					
					case "MoveArea":
						ObjectUtil.applyAttributes(this.moveArea, layer.shape[0]);
						break;
					
					case "Obstacles":
						for each(shape in layer.shape)
						{
							var hitArea:Rectangle = new Rectangle(shape.@x, shape.@y, shape.@width, shape.@height);
							addObject(new Obstacle(this, hitArea));
						}
						break;
					
					case "Spawners":
						for each(sprite in layer.sprite)
						{
							data = { x: sprite.@x, y: sprite.@y };
							setSpriteData(data, sprite, spriteEntries);
							addSpawner(new MapSpawner(this, data));
						}
						break;
					
					case "Triggers":
						for each(shape in layer.shape)
						{
							data = { x: shape.@x, y: shape.@y, width: shape.@width, height: shape.@height };
							for each(var prop:XML in shape.properties[0].type)
							{
								data[prop.@name] = prop.@value;
							}
							addTrigger(new MapTrigger(this, data));
						}
						break;
					
					default:
						break;
				}
			}
		}
		
		private function setSpriteData(data:Object, sprite:XML, spriteEntries:Array):void
		{
			var prop:XML;
			var entry:XML = spriteEntries[sprite.@idx];
			if (entry)
			{
				for each(prop in entry.properties[0].type)
				{
					data[prop.@name] = prop.@value;
				}
			}
			for each(prop in sprite.properties[0].dataOverride)
			{
				data[prop.@name] = prop.@value;
			}
		}
		
		public function setBg(bg:Image):void
		{
			_bgLayer.removeChildren();
			_bgLayer.addChild(bg);
		}
		
		public function addSprite(sprite:DisplayObject):void
		{
			_objectLayer.addChild(sprite);
		}
		
		public function addObject(obj:GameObject):GameObject
		{
			ASSERT(obj.parent != _objectLayer);
			_objectLayer.addChild(obj);
			_objects.push(obj);
			return obj;
		}
		
		public function removeObject(obj:GameObject):GameObject
		{
			ASSERT(obj.parent == _objectLayer);
			_objectLayer.removeChild(obj);
			_objects.splice(_objects.indexOf(obj), 1);
			return obj;
		}
		
		public function addSpawner(spawner:MapSpawner):void
		{
			_spawners.push(spawner);
		}
		
		public function addTrigger(trigger:MapTrigger):void 
		{
			_triggers.push(trigger);
		}
		
		public function addAIController(aiController:AIController):void
		{
			_aiControllers.push(aiController);
		}
		
		public function start():void
		{
			_startTime = getTimer();
		}
		
		public function update():void 
		{
			for each(var obj:GameObject in objects)
			{
				obj.collision = null;
				obj.update();
				if (obj.isDestroyed)
				{
					_destroiedObjects.push(obj);
				}
				else if (obj is CharacterObject)
				{
					for each(var trigger:MapTrigger in _triggers)
					{
						trigger.check(obj as CharacterObject);
					}
				}
			}
			
			if (_destroiedObjects.length > 0)
			{
				for (var i:int = 0; i < _destroiedObjects.length; i++)
				{
					removeObject(_destroiedObjects[i]);
				}
				_destroiedObjects.length = 0;
			}
			
			for each(var aiController:AIController in _aiControllers)
			{
				aiController.update();
			}
			
			for each(var spawner:MapSpawner in _spawners)
			{
				spawner.update();
			}
			
			//_spriteLayer.sortChildren(compareGameObjectDepth);
			_objectLayer.sortChildren(compareGameObjectDepth);
			
			_particleSystemManager.update();
		}
		
		private function compareGameObjectDepth(obj1:DisplayObject, obj2:DisplayObject):int 
		{
			//if (obj1 is GameObject && obj2 is GameObject)
			{
				//Log.debug(obj1, obj1.y, obj2, obj2.y);
				return obj1.y - obj2.y;
			}
			//return -1;
		}
		
		private function keepInMoveArea(obj:GameObject):void
		{
			obj.x = MathUtil.clamp(obj.x, moveArea.left, moveArea.right);
			obj.y = MathUtil.clamp(obj.y, moveArea.top, moveArea.bottom);
		}
		
		public function solveCollision(collider:GameObject, direction:Point = null):Boolean
		{
			var collided:Boolean = false;
			for each(var obj:GameObject in objects)
			{
				if (obj != collider)
				{
					if (obj.hitArea.isEmpty())
						continue;
					var intersection:Rectangle = obj.hitArea.intersection(collider.hitArea);
					if (!intersection.isEmpty())
					{
						//Log.debug(collider, "collides", obj, ", position:", collider.position);
						if (direction == null)
						{
							direction = new Point(MathUtil.sign(intersection.x - collider.x), MathUtil.sign(intersection.y - collider.y)); 
							if (direction.x == 0)
								direction.x = 1;
							if (direction.y == 0)
								direction.y = 1;
						}
						collider.x -= direction.x * (intersection.width + 1);
						collider.y -= direction.y * (intersection.height + 1);
						collider.collision = new Collision(obj, direction, intersection);
						obj.collision = new Collision(collider, new Point( -direction.x, -direction.y), intersection);
						collided = true;
						//Log.debug(collider, "position:", collider.position);
					}
				}
			}
			keepInMoveArea(collider);
			return collided;
		}
		
	}

}