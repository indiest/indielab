package me.sangtian.rpg.framework 
{
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Character
	{
		public var id:int;
		public var name:String;
		
		public var levelPropertyBonus:Vector.<CharacterProperties> = new Vector.<CharacterProperties>();
		//public var levelExps:Vector.<int> = new Vector.<int>();
		public var levelups:Vector.<LevelUpData> = new Vector.<LevelUpData>();
		public var levelCap:uint;
		public var inventoryCap:uint;
		public var availableSkillIds:Vector.<int> = new Vector.<int>();
		
		public var avatarData:Object;
		
		public function Character() 
		{
			
		}
	}

}