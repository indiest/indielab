package me.sangtian.rpg.framework 
{
	import me.sangtian.common.Time;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Skill 
	{
		private var _caster:CharacterObject;
		private var _data:SkillData;
		private var _level:uint;
		private var _properties:SkillProperties;
		
		private var _passitiveEffects:Vector.<SkillEffect>;
		
		public var coolingTime:Number = 0;
		
		public function get data():SkillData
		{
			return _data;
		}
		
		public function get properties():SkillProperties 
		{
			return _properties;
		}
		
		public function get level():uint 
		{
			return _level;
		}
		
		public function get isMaxLevel():Boolean
		{
			return level == data.levelProperties.length;
		}
		
		public function Skill(caster:CharacterObject, data:SkillData) 
		{
			_caster = caster;
			ASSERT(data != null);
			_data = data;
			resetLevel();
		}
		
		public function resetLevel(value:uint = 1):void
		{
			_level = 1;
			_properties = data.levelProperties[0].clone();
			upgradeLevel(value - 1);
		}
		
		public function upgradeLevel(value:uint = 1):void
		{
			if (value == 0)
				return;
			ASSERT(level + value <= data.levelProperties.length);
			for (var i:int = 0; i < value; i++)
			{
				ObjectUtil.applyProperties(_properties, data.levelProperties[level + i]);
			}
			_level += value;
			resetPassiveEffects();
		}
		
		public function update():void
		{
			if (coolingTime > 0)
			{
				coolingTime = MathUtil.clamp(coolingTime - Time.deltaSecond, 0, properties.cooldown);
			}
			
			if (properties.isPassitive)
			{
				if (_passitiveEffects == null)
				{
					_passitiveEffects = _caster.applySkillEffect(_caster, this);
				}
				// TODO: update aura effects
				if (properties.castEffectRange > 0)
				{
					
				}
			}
		}
		
		public function resetPassiveEffects():void
		{
			for each(var effect:SkillEffect in _passitiveEffects)
			{
				effect.onDeactivate();
			}
			_passitiveEffects = null;
		}
		
		public function startCooldown():void 
		{
			coolingTime = properties.cooldown;
		}
	}

}