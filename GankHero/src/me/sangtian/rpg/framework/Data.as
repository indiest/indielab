package me.sangtian.rpg.framework 
{
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;
	import me.sangtian.common.Enum;
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Data 
	{
		private static var _packageName:String;

		private static var _aiControllers:Dictionary = new Dictionary();
		private static var _skillEffectBehaviors:Dictionary = new Dictionary();
		private static var _characters:Dictionary = new Dictionary();
		private static var _skills:Dictionary = new Dictionary();
		private static var _maps:Dictionary = new Dictionary();
		private static var _items:Dictionary = new Dictionary();
		private static var _levelups:Dictionary = new Dictionary();
		
		public static function registerSkillEffectBehavior(name:String, clazz:Class):void
		{
			ASSERT(ObjectUtil.isExtendedFrom(clazz, SkillEffectBehavior), "Class is not extended from SkillEffectBehavior");
			_skillEffectBehaviors[name] = clazz;
		}
		
		public static function registerAIController(name:String, clazz:Class):void
		{
			ASSERT(ObjectUtil.isExtendedFrom(clazz, AIController), "Class is not extended from _aiControllers");
			_aiControllers[name] = clazz;
		}
		
		public static function getSkillEffectBehavior(nameOrClazz:*):SkillEffectBehavior
		{
			if (nameOrClazz is String)
			{
				return _skillEffectBehaviors[nameOrClazz];
			}
			else if (nameOrClazz is Class)
			{
				for each(var behavior:SkillEffectBehavior in _skillEffectBehaviors)
				{
					if (behavior is nameOrClazz)
						return behavior;
				}
			}
			return null;
		}
		
		private static function loadScripts():void
		{
			registerSkillEffectBehavior("AddProperty", EffectAddProperty);
			registerSkillEffectBehavior("Teleport", EffectTeleport);
			registerSkillEffectBehavior("Stun", EffectStun);
			registerSkillEffectBehavior("PhysicalDamage", EffectPhysicalDamage);
			registerSkillEffectBehavior("SpellDamage", EffectSpellDamage);
			registerSkillEffectBehavior("SlowMove", EffectSlowMove);
			registerSkillEffectBehavior("HOT", EffectHOT);
			registerSkillEffectBehavior("InterruptableHOT", EffectInterruptableHOT);
			
			registerAIController("CrazyDog", AICrazyDog);
			registerAIController("Runaway", AIRunaway);
		}
		{loadScripts()}
		
		public static function loadMap(data:String):Object
		{
			var m:Object = JSON.parse(data);
			_maps[m.name] = m;
			return m;
		}
		
		public static function loadSkill(data:String):SkillData
		{
			var skillObj:Object = JSON.parse(data);
			var s:SkillData = new SkillData();
			s.id = skillObj.id;
			s.name = skillObj.name;
			s.icon = skillObj.icon;
			s.particleData = skillObj.particle;
			for each(var p:Object in skillObj.levelProperties)
			{
				s.levelProperties.push(loadSkillProperties(p));
			}
			_skills[s.id] = s;
			return s;
		}
		
		private static function loadSkillProperties(obj:Object):SkillProperties 
		{
			var sp:SkillProperties = new SkillProperties();
			for (var key:String in obj)
			{
				var value:* = obj[key];
				if (key == "targetCharacterType")
				{
					sp.targetCharacterType = new CharacterType(value);
				}
				else if (key == "castTargetType")
				{
					sp.castTargetType = Enum.fromIndex(CastTargetType, value) as CastTargetType;
				}
				else if (key == "casterPropertyCost")
				{
					sp.casterPropertyCost = loadCharacterProperties(value);
				}
				else if (key == "effects")
				{
					sp.effects = new Vector.<SkillEffectBehavior>();
					for each(var effObj:Object in value)
					{
						sp.effects.push(loadSkillEffectBehavior(effObj));
					}
				}
				else
				{
					sp[key] = value;
				}
			}
			return sp;
		}
		
		public static function loadLevelUps(data:String):void
		{
			var lvObjs:Array = JSON.parse(data) as Array;
			for each(var lvObj:Object in lvObjs)
			{
				var levelups:Vector.<LevelUpData> = new Vector.<LevelUpData>(lvObj.exp.length, true);
				for (var i:int = 0; i < levelups.length; i++)
				{
					var lvData:LevelUpData = new LevelUpData();
					lvData.exp = lvObj.exp[i];
					lvData.dyingExp = lvObj.dyingExp[i];
					lvData.dyingGold = lvObj.dyingGold[i];
					levelups[i] = lvData;
				}
				_levelups[lvObj.id] = levelups;
			}
		}
		
		public static function loadCharacter(data:String):Character
		{
			var obj:Object = JSON.parse(data);
			var c:Character = new Character();
			c.id = obj.id;
			c.name = obj.name;
			for each(var p:Object in obj.levelProperties)
			{
				c.levelPropertyBonus.push(loadCharacterProperties(p));
			}
			c.levelups = _levelups[obj.levelUpId];
			c.levelCap = obj.levelCap;
			c.inventoryCap = obj.inventoryCap;
			c.availableSkillIds = Vector.<int>(obj.availableSkills);
			c.avatarData = obj.avatar;
			_characters[c.id] = c;
			return c;
		}
		
		private static function loadSkillEffectBehavior(effObj:Object):SkillEffectBehavior 
		{
			//var className:String = effObj.className || _packageName + "::Effect" + effObj.name;
			var clazz:Class = _skillEffectBehaviors[effObj.name];
			ASSERT(clazz != null, "Can't find SkillEffectBehavior by name: " + effObj.name);
			var behavior:SkillEffectBehavior = new clazz();
			try
			{
				//var clazz:Class = getDefinitionByName(className) as Class;
				//behavior = new clazz() as SkillEffectBehavior;
				ObjectUtil.applyProperties(behavior, effObj);
			}
			catch (err:Error)
			{
				ASSERT(false, err.message);
			}
			return behavior;
		}
		
		public static function loadCharacterProperties(propObj:Object):CharacterProperties
		{
			var cp:CharacterProperties = new CharacterProperties();
			ObjectUtil.applyProperties(cp, propObj);
			return cp;
		}
		
		public static function loadItem(data:String):ItemData
		{
			var obj:Object = JSON.parse(data);			
			var itemData:ItemData = new ItemData();
			itemData.id = obj.id;
			itemData.name = obj.name;
			itemData.type = Enum.fromIndex(ItemType, obj.type) as ItemType;
			for each(var p:Object in obj.levelProperties)
			{
				itemData.levelProperties.push(loadCharacterProperties(p));
			}
			itemData.skillIds = Vector.<int>(obj.skillIds);
			if (obj.castableSkillId)
			{
				itemData.castableSkillId = obj.castableSkillId;
			}
			else
			{
				// If castable skill is not specified, the first active skill will be assigned.
				for each(var skillId:int in itemData.skillIds)
				{
					if (!getSkillData(skillId).levelProperties[0].isPassitive)
					{
						itemData.castableSkillId = skillId;
						continue;
					}
				}
			}
			itemData.consumableTimes = obj.consumableTimes;
			itemData.icon = obj.icon;
			itemData.avatarData = obj.avatar;
			_items[itemData.id] = itemData;
			return itemData;
		}
		
		public static function getCharacter(id:int):Character
		{
			return _characters[id];
		}
		
		public static function getSkillData(skillId:int):SkillData 
		{
			return _skills[skillId];
		}
		
		public static function getMapData(name:String):Object
		{
			return _maps[name];
		}
		
		public static function getItemData(itemId:int):ItemData
		{
			return _items[itemId];
		}
		
		public static function createAIController(type:String, data:Object):AIController 
		{
			var clazz:Class = _aiControllers[type];
			ASSERT(clazz != null, "Can't find AI Controller by type:" + type);
			var controller:AIController = new clazz();
			try
			{
				ObjectUtil.applyProperties(controller, data);
			}
			catch (err:Error)
			{
				ASSERT(false, err.message);
			}
			return controller;
		}
		
		static public function get characters():Dictionary 
		{
			return _characters;
		}
		
		static public function get skills():Dictionary 
		{
			return _skills;
		}
		
		static public function get maps():Dictionary 
		{
			return _maps;
		}
		
		static public function get items():Dictionary 
		{
			return _items;
		}
	}

}