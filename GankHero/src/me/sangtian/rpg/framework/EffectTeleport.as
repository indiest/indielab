package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class EffectTeleport extends SkillEffectBehavior 
	{
		override public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			//var signX:int = MathUtil.sign(target.x - caster.x);
			//var signY:int = MathUtil.sign(target.y - caster.y);
			//caster.x = target.x - signX * target.hitArea.width;
			//caster.y = target.y - signY * target.hitArea.height;
			caster.moveStep(target.position, Number.MAX_VALUE);
		}
	}

}