package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class EffectSlowMove extends SkillEffectBehavior 
	{
		
		public var ratio:Number;
		
		override public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			target.properties.moveSpeedAmendRatio -= ratio;
		}
		
		override public function onDeactivate(caster:CharacterObject, target:CharacterObject):void 
		{
			target.properties.moveSpeedAmendRatio += ratio;
		}
	}

}