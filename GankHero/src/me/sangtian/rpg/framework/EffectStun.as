package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class EffectStun extends SkillEffectBehavior
	{
		override public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			target.stateMachine.doAction(CharacterStateMachine.ACTION_START_STUN);
		}
		
		override public function onDeactivate(caster:CharacterObject, target:CharacterObject):void 
		{
			target.stateMachine.doAction(CharacterStateMachine.ACTION_STOP_STUN);
		}
	}

}