package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import me.sangtian.common.Input;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.Time;
	import starling.core.Starling;
	/**
	 * ...
	 * @author tiansang
	 */
	public class CharacterObjectKeyController 
	{
		
		public var currentCharacter:CharacterObject;
		
		public function CharacterObjectKeyController()
		{
			Input.initialize(Starling.current.nativeStage);
		}
		
		private var _moveTarget:Point = new Point();
		
		public function update():void
		{
			if (currentCharacter == null)
				return;
			
			_moveTarget.setTo(Input.getAxis(Input.AXIS_HORIZONTAL), Input.getAxis(Input.AXIS_VERTICAL));
			if (_moveTarget.x != 0 || _moveTarget.y != 0)
			{
				var moveSpeed:Number = currentCharacter.properties.getActualMoveSpeed();
				_moveTarget.normalize(moveSpeed * Time.deltaSecond);
				_moveTarget.offset(currentCharacter.x, currentCharacter.y);
				currentCharacter.moveTo(_moveTarget);
			}
			
			if (Input.getKeyUp(Keyboard.NUMBER_1))
				currentCharacter.avatar.changeWeapon(1001);
			else if (Input.getKeyUp(Keyboard.NUMBER_2))
				currentCharacter.avatar.changeWeapon(1002);
			
		}
	}

}