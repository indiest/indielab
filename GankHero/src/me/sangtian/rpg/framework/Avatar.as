package me.sangtian.rpg.framework 
{
	import dragonBones.Armature;
	import dragonBones.Bone;
	import dragonBones.events.AnimationEvent;
	import dragonBones.factorys.BaseFactory;
	import dragonBones.factorys.StarlingFactory;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.utils.Dictionary;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.filters.ColorMatrixFilter;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Avatar 
	{
		private var _obj:GameObject;
		private var _armature:Armature;
		private var _display:DisplayObject;
		private var _data:Object;
		private var _currentAnimName:String;		
		private var _currentAnimComplateCallback:Function;
		private var _weaponName:String = "sword";
		
		private var _tintParts:Vector.<Bone> = new Vector.<Bone>();
		// Stage3D(Starling) doesn't support multi-layer filters yet.
		//private var _colorFilter:ColorMatrixFilter = new ColorMatrixFilter();
		
		public function get timeScale():Number
		{
			if (_armature == null)
				return -1;
			return _armature.animation.timeScale;
		}
		
		public function set timeScale(value:Number):void
		{
			if (_armature != null)
				_armature.animation.timeScale = value;
		}
		
		public function get display():DisplayObject 
		{
			return _display;
		}
		
		public function Avatar(obj:GameObject, data:Object) 
		{
			_obj = obj;
			_data = data;
			var factory:BaseFactory = _armatureFactorys[_data.armature];
			ASSERT(factory != null, "Can't find armature:" + _data.armature);
			_armature = factory.buildArmature(_data.armature);
			_armature.addEventListener(AnimationEvent.COMPLETE, handleAnimationComplete);
			_display = _obj.addChild(_armature.display as DisplayObject);
			_display.width = _data.width;
			_display.height = _data.height;
			//_display.pivotX = _display.width * 0.5 + _data.offsetX;
			//_display.pivotY = _display.height * 0.5 +_data.offsetY;
			//_display.x = _data.offsetX;
			//_display.y = _data.offsetY;
			
			for each(var part:String in _data.tintParts)
			{
				_tintParts.push(_armature.getBone(part));
			}
		}
		
		private function getItemDisplay(itemId:int):DisplayObject 
		{
			var itemData:ItemData = Data.getItemData(itemId);
			ASSERT(itemData != null, "Can't find item by ID: " + itemId);
			return getImage(itemData.avatarData.armature, itemData.avatarData.texture);
		}
		
		private function playWeaponAnimation(weaponBone:Bone = null):void 
		{
			if (weaponBone == null)
			{
				weaponBone = _armature.getBone("weapon");
			}
			if (weaponBone != null)
			{
				var animName:String = _currentAnimName + "_" + _weaponName;
				Log.debug("Playing weapon animation:", animName);
				weaponBone.childArmature.animation.gotoAndPlay(animName);
			}
		}
		
		public function changeWeapon(itemId:int):void
		{
			var weaponBone:Bone = _armature.getBone("weapon");
			if (weaponBone == null)
			{
				Log.info("Avatar can't change weapon because it doesn't have a weapon bone, armature:", _armature.name);
				return;
			}
				
			var itemData:ItemData = Data.getItemData(itemId);
			ASSERT(itemData != null, "Can't find item by ID: " + itemId);
			ASSERT(itemData.type == ItemType.WEAPON, "Item is not a weapon, ItemID: " + itemId);
			_weaponName = itemData.name;
			
			if (weaponBone.childArmature == null)
			{
				// weapon bone is just an image, replace it
				weaponBone.display.dispose();
				weaponBone.display = getItemDisplay(itemId);
			}
			else
			{
				// weapon bone has child armature, which means different weapon has different animation
				var childBone:Bone = weaponBone.childArmature.getBone("weapon");
				//childBone.display.dispose();
				//childBone.display = getItemDisplay(itemId);
				playWeaponAnimation(weaponBone);
			}
		}
		
		public function play(animName:String, reset:Boolean = false, completeCallback:Function = null):void
		{
			if (!reset && _currentAnimName == animName)
				return;
			if (_armature.animation.animationData.getMovementData(animName) == null)
				Log.warn("Unable to play avatar animation:", animName);
			else
				Log.debug("Playing avatar animation:", animName);
			_currentAnimName = animName;
			_currentAnimComplateCallback = completeCallback;
			_armature.animation.gotoAndPlay(animName);
			playWeaponAnimation();
		}
		
		private function handleAnimationComplete(e:AnimationEvent):void 
		{
			if (_currentAnimComplateCallback != null)
				_currentAnimComplateCallback();
		}
		
		public function update():void
		{
			_armature.advanceTime(Time.deltaSecond);
			//_armature.update();
		}
		
		public function stop():void 
		{
			_currentAnimName = null;
			_armature.animation.stop();
		}
		
		public function tint(color:uint):void
		{
			//_colorFilter.matrix = Vector.<Number>(DisplayObjectUtil.getTintColorMatrix(color));
			var cf:ColorTransform = new ColorTransform();
			cf.color = color;
			for each(var part:Bone in _tintParts)
			{
				//part.filter = _colorFilter;
				part.colorTransform = cf;
			}
		}
		
		public function dispose():void 
		{
			_armature.dispose();
			_display.dispose();
			_obj = null;
		}
		
		private static var _armatureFactorys:Dictionary = new Dictionary();
		
		public static function addFactory(factory:BaseFactory, skeletonName:String):void 
		{
			for each(var armatureName:String in factory.getSkeletonData(skeletonName).armatureNames)
			{
				ASSERT(_armatureFactorys[armatureName] == null, "Armature already exists:" + armatureName);
				_armatureFactorys[armatureName] = factory;
			}
		}

		private static function getImage(armatureName:String, textureName:String):Image
		{
			var factory:BaseFactory = _armatureFactorys[armatureName];
			if (factory == null)
			{
				Log.warn("Can't find armature by name:", armatureName);
				return null;
			}
			return factory.getTextureDisplay(textureName) as Image;//StarlingFactory.getTextureDisplay(factory.textureAtlasData, textureName);
		}
	}

}