package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author 
	 */
	public class EffectAddProperty extends SkillEffectBehavior 
	{
		
		public var properties:Object;
		private var _characterProperties:CharacterProperties;
		
		override public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			if (_characterProperties == null)
				_characterProperties = Data.loadCharacterProperties(properties);
				
			target.properties.add(_characterProperties);
		}
		
		override public function onDeactivate(caster:CharacterObject, target:CharacterObject):void 
		{
			target.properties.sub(_characterProperties);
		}
	}

}