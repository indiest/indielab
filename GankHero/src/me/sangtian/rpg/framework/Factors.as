package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class Factors 
	{
		public static var STR_TO_HP_MAX:Number = 19;
		public static var STR_TO_HP_REGEN:Number = 0.19;
		public static var AGI_TO_ARMOR:Number = 0.2;
		public static var AGI_TO_ATTACK_SPEED:Number = 0.01;
		public static var INT_TO_MP_MAX:Number = 14;
		public static var INT_TO_MP_REGEN:Number = 0.14;
		
		public static const MAX_MOVE_SPEED:Number = 500;
		
	}

}