package me.sangtian.rpg.framework 
{
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author tiansang
	 */
	public class MapTrigger 
	{
		private var _map:MapLayer;
		
		public var region:Rectangle;
		public var eventType:String;
		public var conditions:Vector.<Condition>;
		
		public function MapTrigger(map:MapLayer, data:Object) 
		{
			_map = map;
			region = new Rectangle(data.x, data.y, data.width, data.height);
			eventType = data.event;
			conditions = new Vector.<Condition>();
			if (data.conditions)
			{
				for each(var c:String in (data.conditions as String).split(";"))
				{
					conditions.push(new Condition(c));
				}
			}
		}
		
		public function check(co:CharacterObject):Boolean
		{
			if (region.intersects(co.hitArea))
			{
				for each(var condition:Condition in conditions)
				{
					if (condition.check(co) == false)
						return false;
				}
				_map.dispatchEventWith(eventType, false, co);
				return true;
			}
			return false;
		}
	}

}