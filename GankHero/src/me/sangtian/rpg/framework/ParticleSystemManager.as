package me.sangtian.rpg.framework 
{
	import flash.utils.Dictionary;
	import starling.display.DisplayObjectContainer;
	import starling.display.Sprite;
	/**
	 * ...
	 * @author tiansang
	 */
	public class ParticleSystemManager 
	{
		private var _container:DisplayObjectContainer;
		private var _particleFactorys:Vector.<ParticleFactory> = new Vector.<ParticleFactory>();
		
		public function ParticleSystemManager(container:DisplayObjectContainer) 
		{
			_container = container;
		}
		
		public function play(data:Object, x:Number, y:Number):void
		{
			if (data == null)
				return;
			var subContainer:Sprite = new Sprite();
			var factory:ParticleFactory = new ParticleFactory(subContainer, data);
			_container.addChild(subContainer);
			_particleFactorys.push(factory);
			factory.emitOnce(x, y);
		}
		
		public function update():void
		{
			for (var i:int = _particleFactorys.length - 1; i >= 0; i--)
			{
				var factory:ParticleFactory = _particleFactorys[i];
				if (factory.isPlaying)
				{
					factory.update();
				}
				else
				{
					_particleFactorys.splice(i, 1);
				}
			}
		}
	}

}