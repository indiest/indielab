package me.sangtian.rpg.framework 
{
	import me.sangtian.common.Enum;
	import starling.utils.Color;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class DamageType extends Enum 
	{
		public static const Physical:DamageType = new DamageType(1, Color.RED);
		public static const Spell:DamageType = new DamageType(2, Color.YELLOW);
		
		private var _color:uint;
		
		public function DamageType(index:int, color:uint)
		{
			super(index);
			_color = color;
		}
		
		public function get color():uint 
		{
			return _color;
		}
	}

}