package me.sangtian.rpg.framework 
{
	import me.sangtian.common.Time;
	import me.sangtian.common.util.ObjectUtil;
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;
	import starling.extensions.ParticleSystem;
	import starling.extensions.PDParticleSystem;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author tiansang
	 */
	public class ParticleFactory 
	{
		private var _container:DisplayObjectContainer;
		private var _config:XML;
		private var _texture:Texture;
		private var _num:int;
		private var _properties:Object;
		private var _particleSystems:Vector.<PDParticleSystem>;
		
		public function ParticleFactory(container:DisplayObjectContainer, data:Object) 
		{
			_container = container;
			_config = XML(ResourceManager.getResource(data.config));
			_texture = ResourceManager.getTexture(data.texture);
			_num = data.num;
			_properties = data.properties;
			_particleSystems = new Vector.<PDParticleSystem>(_num);
		}
		
		public function emitOnce(x:Number, y:Number):void
		{
			for (var i:int = 0; i < _num; i++)
			{
				var ps:PDParticleSystem = _particleSystems[i];
				if (ps == null)
				{
					ps = new PDParticleSystem(_config, _texture);
					if (_num > 1)
					{
						ps.emitAngle = 360 / i;
					}
					if (_properties)
					{
						ObjectUtil.applyProperties(ps, _properties);
					}
					ps.addEventListener(Event.COMPLETE, handleParticlesComplete);
					_particleSystems[i] = ps;
				}
				ps.x = x;
				ps.y = y;
				_container.addChild(ps);
				ps.start(ps.lifespan);
			}
		}
		
		private function handleParticlesComplete(e:Event):void 
		{
			var ps:ParticleSystem = e.currentTarget as PDParticleSystem;
			_container.removeChild(ps);
		}
		
		public function get isPlaying():Boolean
		{
			return _container.numChildren > 0;
		}
		
		public function update():void
		{
			for (var i:int = 0; i < _num; i++)
			{
				var ps:ParticleSystem = _particleSystems[i];
				if (ps.parent != null)
				{
					ps.advanceTime(Time.deltaSecond);
				}
			}
		}
	}

}