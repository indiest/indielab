package me.sangtian.rpg.framework 
{
	import com.greensock.loading.display.ContentDisplay;
	import dragonBones.factorys.BaseFactory;
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author tiansang
	 */
	public class ResourceManager
	{
		private static var _cache:Dictionary = new Dictionary();
		
		public static function setResource(id:String, data:*):void 
		{
			if (data is ContentDisplay)
			{
				data = (data as ContentDisplay).rawContent;
			}
			_cache[id] = data;
		}
		
		public static function getResource(id:String):*
		{
			return _cache[id];
		}
		
		public static function getTexture(id:String):Texture
		{
			ASSERT(_cache[id] is Bitmap);
			return Texture.fromBitmap(_cache[id]);
		}
	}

}