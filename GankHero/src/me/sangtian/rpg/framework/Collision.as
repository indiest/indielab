package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Collision 
	{
		private var _otherCollider:GameObject;
		private var _direction:Point;
		private var _intersection:Rectangle;
		
		public function Collision(otherCollider:GameObject, direction:Point, intersection:Rectangle) 
		{
			_otherCollider = otherCollider;
			_direction = direction;
			_intersection = intersection;
		}
		
		public function get otherCollider():GameObject 
		{
			return _otherCollider;
		}
		
		public function get direction():Point 
		{
			return _direction;
		}
		
		public function get intersection():Rectangle 
		{
			return _intersection;
		}
		
	}

}