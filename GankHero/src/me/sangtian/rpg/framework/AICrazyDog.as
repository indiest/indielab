package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class AICrazyDog extends AIController 
	{
		private var _target:CharacterObject;
		
		override public function update():void
		{
			if (co.isDead)
				return;
				
			if (_target == null || _target.isDead)
			{
				findTarget(co);
			}
			if (_target != null)
			{
				co.attack(_target);
			}
			else
			{
				co.idle();
			}
		}
		
		private function findTarget(co:CharacterObject):void 
		{
			var minDist:Number = Number.MAX_VALUE;
			_target = null;
			for each(var obj:GameObject in co.map.objects)
			{
				if (!obj.isDestroyed && obj is CharacterObject)
				{
					var target:CharacterObject = obj as CharacterObject;
					if (target.isDead || !co.canSee(target))
						continue;
					var dist:Number = co.getDistance(target);
					if (co.isEnemy(target) && dist < minDist)
					{
						_target = target;
						minDist = dist;
					}
				}
			}
		}
	}

}