package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class SingleCastTarget extends CastTarget 
	{
		private var _target:CharacterObject;
		
		public function SingleCastTarget(target:CharacterObject) 
		{
			_target = target;
		}
		
		override public function check(caster:CharacterObject, skill:Skill):Boolean 
		{
			return checkCharacterType(caster, _target, skill) && checkDistance(caster, _target, skill);
		}
		
		override public function getApplicableCharacters():Vector.<CharacterObject> 
		{
			return Vector.<CharacterObject>([_target]);
		}
		
	}

}