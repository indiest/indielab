package me.sangtian.rpg.framework 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class DamageEvent extends Event 
	{
		public static const DAMAGE:String = "damage";
		
		private var _previousHP:Number;
		private var _damage:Number;
		private var _damageType:DamageType;
		private var _critical:Boolean;
		
		public function get damage():Number 
		{
			return _damage;
		}
		
		public function get damageType():DamageType 
		{
			return _damageType;
		}
		
		public function get previousHP():Number 
		{
			return _previousHP;
		}
		
		public function get critical():Boolean 
		{
			return _critical;
		}
		
		public function DamageEvent(type:String, previousHP:Number, damage:Number, damageType:DamageType, critical:Boolean = false) 
		{
			super(type);
			_previousHP = previousHP;
			_damage = damage;
			_damageType = damageType;
			_critical = critical;
		}
		
	}

}