package me.sangtian.rpg.framework 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class CastTargetType extends Enum 
	{
		public function CastTargetType(index:int)
		{
			super(index);
		}
		
		public static const OWN:CastTargetType = new CastTargetType(0);
		public static const SINGLE:CastTargetType = new CastTargetType(1);
		public static const AREA:CastTargetType = new CastTargetType(2);
		public static const OWN_AREA:CastTargetType = new CastTargetType(3);
	}

}