package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class AreaCastTarget extends CastTarget 
	{
		private var _caster:CharacterObject;
		private var _targetPosition:Point;
		private var _skill:Skill;
		
		public function AreaCastTarget(caster:CharacterObject, targetPosition:Point, skill:Skill) 
		{
			_caster = caster;
			_targetPosition = targetPosition;
			_skill = skill;
		}
		
		override public function check(caster:CharacterObject, skill:Skill):Boolean 
		{
			return true;
		}
		
		override public function getApplicableCharacters():Vector.<CharacterObject> 
		{
			var targets:Vector.<CharacterObject> = new Vector.<CharacterObject>();
			for each(var obj:GameObject in _caster.map.objects)
			{
				if (obj is CharacterObject)
				{
					var target:CharacterObject = obj as CharacterObject;
					if (!checkCharacterType(_caster, target, _skill))
						continue;
					var dist:Number = target.position.subtract(_targetPosition).length;
					if (dist > _skill.properties.castEffectRange)
						continue;
					targets.push(target);
					// 0 = no number limit
					if (_skill.properties.castTargetMaxNumber > 0 && targets.length >= _skill.properties.castTargetMaxNumber)
						break;
				}
			}
			return targets;
		}
	}

}