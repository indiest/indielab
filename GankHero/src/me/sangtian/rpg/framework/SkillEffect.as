package me.sangtian.rpg.framework 
{
	import me.sangtian.common.Time;
	/**
	 * ...
	 * @author tiansang
	 */
	public class SkillEffect 
	{
		private var _active:Boolean;
		
		public function get name():String
		{
			return _behavior.name;
		}
		
		public function get active():Boolean 
		{
			return _active;
		}
		
		public function get isContinuous():Boolean
		{
			return _behavior.duration > 0;
		}
		
		public function get remainingTime():Number 
		{
			return _remainingTime;
		}
		
		internal function get behavior():SkillEffectBehavior 
		{
			return _behavior;
		}
		
		protected var _behavior:SkillEffectBehavior;
		protected var _caster:CharacterObject;
		protected var _target:CharacterObject;
		protected var _remainingTime:Number;
		protected var _tickRemainingTime:Number;
		
		public function SkillEffect(behavior:SkillEffectBehavior, caster:CharacterObject, target:CharacterObject) 
		{
			_behavior = behavior;
			_caster = caster;
			_target = target;
			
			_remainingTime = behavior.duration;
			_tickRemainingTime = 0;
			
			onActivate();
		}
		
		protected function onActivate():void 
		{
			_active = true;
			_behavior.onActivate(_caster, _target);
		}
		
		protected function onTick():void 
		{
			_behavior.onTick(_caster, _target);
		}
		
		public function onDeactivate():void 
		{
			_active = false;
			_behavior.onDeactivate(_caster, _target);
			
			_caster = null;
			_target = null;
		}
		
		public function update():void
		{
			if (_behavior.tickInteval > 0)
			{
				_tickRemainingTime -= Time.deltaSecond;
				if (_tickRemainingTime <= 0)
				{
					onTick();
					_tickRemainingTime += _behavior.tickInteval;
				}
			}
			if (_behavior.duration > 0)
			{
				_remainingTime -= Time.deltaSecond;
				if (_remainingTime <= 0)
				{
					onDeactivate();
					_target = null;
				}
			}
		}
	}

}