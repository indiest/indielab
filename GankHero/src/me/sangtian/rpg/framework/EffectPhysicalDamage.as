package me.sangtian.rpg.framework 
{
	import me.sangtian.common.util.Random;
	/**
	 * ...
	 * @author tiansang
	 */
	public class EffectPhysicalDamage extends SkillEffectBehavior 
	{
		public var damage:Number = 0;
		
		override public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			var out:Number = (damage + caster.properties.attackAmendValue) * (1 + caster.properties.attackAmendRatio);
			var critical:Boolean = Random.randomBoolean(caster.properties.critChance);
			if (critical)
				out *= (1 + caster.properties.critBonusRatio);
			var damage:Number = out * (1 - target.properties.defenceAmendRatio) - target.properties.defenceAmendValue;
			target.takeDamage(caster, damage, DamageType.Physical, critical);
		}
	}

}