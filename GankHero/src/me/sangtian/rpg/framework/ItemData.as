package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class ItemData 
	{
		public var id:int;
		public var name:String;
		public var type:ItemType;
		public var levelProperties:Vector.<CharacterProperties> = new Vector.<CharacterProperties>();
		public var skillIds:Vector.<int> = new Vector.<int>();
		public var castableSkillId:int;
		public var consumableTimes:int;
		
		public var icon:String;
		public var avatarData:Object;
		
		public function ItemData() 
		{
			
		}
		
	}

}