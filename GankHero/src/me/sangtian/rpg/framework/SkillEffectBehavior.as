package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class SkillEffectBehavior 
	{
		public var name:String;
		public var duration:Number;
		public var tickInteval:Number;
		public var propertyBonus:CharacterProperties;
		
		public function SkillEffectBehavior() 
		{
			
		}
		
		protected function deactivateEffect(target:CharacterObject):void 
		{
			for each(var effect:SkillEffect in target.effects)
			{
				if (effect.behavior == this)
				{
					effect.onDeactivate();
				}
			}
		}
		
		public function onActivate(caster:CharacterObject, target:CharacterObject):void 
		{
			
		}
		
		public function onTick(caster:CharacterObject, target:CharacterObject):void 
		{
			
		}
		
		public function onDeactivate(caster:CharacterObject, target:CharacterObject):void 
		{
			
		}
		
	}

}