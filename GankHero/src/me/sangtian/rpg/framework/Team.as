package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class Team 
	{
		private static const COLORS:Vector.<uint> = Vector.<uint>(
			// White, Red, Blue, Green, Yellow
			[0xffffffff, 0xffff0000, 0xff0000ff, 0xff00ff00, 0xffffff00]
		);
		
		private static const HUES:Vector.<Number> = Vector.<Number>(
			[0, 0 / Math.PI, 240 / Math.PI, 120 / Math.PI, 60 / Math.PI]
		);
		
		public static function getColor(teamId:uint):uint
		{
			if (teamId < COLORS.length)
			{
				return COLORS[teamId];
			}
			else
			{
				return COLORS[0];
			}
		}
		
		private var _teamId:int;
		
		public function get teamId():int 
		{
			return _teamId;
		}
		
		public function set teamId(value:int):void 
		{
			_teamId = value;
		}
		
		public function get color():uint
		{
			return COLORS[teamId];
		}
		
		public function Team(teamId:int) 
		{
			_teamId = teamId;
			ASSERT(teamId <= COLORS.length);
		}
		
		public function isAlly(team:Team):Boolean 
		{
			// TODO: check team mask
			return teamId == team.teamId;
		}
		
		public function get hue():Number 
		{
			return HUES[teamId];
		}
		
	}

}