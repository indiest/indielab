package me.sangtian.rpg.framework 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class ItemContainer
	{
		private var _slots:Vector.<Item>;
		private var _size:uint;
		
		public function get size():uint 
		{
			return _size;
		}
		
		public function ItemContainer(size:uint) 
		{
			_size = size;
			_slots = new Vector.<Item>(size);
		}
		
		public function getItem(index:uint):Item
		{
			return _slots[index];
		}
		
		protected function setItem(item:Item, index:int):void 
		{
			_slots[index] = item;
		}
		
		public function addItem(item:Item, index:int = -1):Boolean
		{
			if (index < 0)
			{
				index = getFirstEmptySlotIndex();
				if (index < 0)
					return false;
			}
			if (getItem(index) != null)
				return false;
			setItem(item, index);
			return true;
		}
		
		public function removeItem(index:int):Item
		{
			var item:Item = getItem(index);
			setItem(null, index);
			return item;
		}
		
		public function getFirstEmptySlotIndex():int 
		{
			for (var i:int = 0; i < _size; i++)
			{
				if (_slots[i] == null)
					return i;
			}
			return -1;
		}
	}

}