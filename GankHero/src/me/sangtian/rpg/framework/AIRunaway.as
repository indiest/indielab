package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author tiansang
	 */
	public class AIRunaway extends AIController 
	{
		public static const DIR_UP:int = 1;
		public static const DIR_DOWN:int = 2;
		public static const DIR_LEFT:int = 4;
		public static const DIR_RIGHT:int = 8;
		
		// FIXME: move to a game object rather than a direction
		public var direction:uint;
		
		private var _destPoint:Point = new Point();
		
		override public function update():void 
		{
			if (co.isDead)
				return;
			if (co.stateMachine.currentState != CharacterStateMachine.STATE_MOVE)
			{
				if (direction & DIR_UP)
				{
					_destPoint.setTo(co.map.moveArea.x + co.map.moveArea.width * 0.5, co.map.moveArea.y);
					co.moveTo(_destPoint);
				}
				else if (direction & DIR_DOWN)
				{
					_destPoint.setTo(co.map.moveArea.x + co.map.moveArea.width * 0.5, co.map.moveArea.y + co.map.moveArea.height);
					co.moveTo(_destPoint);
				}
				
				if (direction & DIR_LEFT)
				{
					_destPoint.setTo(co.map.moveArea.x, co.map.moveArea.y + co.map.moveArea.height * 0.5);
					co.moveTo(_destPoint);
				}
				else if (direction & DIR_RIGHT)
				{
					_destPoint.setTo(co.map.moveArea.x + co.map.moveArea.width, co.map.moveArea.y + co.map.moveArea.height * 0.5);
					co.moveTo(_destPoint);
				}
			}
			else
			{
				if (co.collision != null)
				{
					var t:Number = Math.atan(co.collision.direction.y / co.collision.direction.x);
					t += Math.PI * 0.5;
					var moveDir:Point = new Point(Math.cos(t) * co.properties.getActualMoveSpeed(), Math.sin(t) * co.properties.getActualMoveSpeed());
					//co.moveStep(co.position.add(moveDir), co.properties.getActualMoveSpeed());
					co.moveTo(co.position.add(moveDir));
				}
			}
		}
		
	}

}