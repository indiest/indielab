package me.sangtian.rpg.framework 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author tiansang
	 */
	public class SkillData 
	{
		public var id:int;
		public var name:String;
		public var levelProperties:Vector.<SkillProperties> = new Vector.<SkillProperties>();
		public var icon:String;
		public var particleData:Object;
		
		public function SkillData() 
		{
			
		}
		
	}

}