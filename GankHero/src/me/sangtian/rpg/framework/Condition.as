package me.sangtian.rpg.framework 
{
	import adobe.utils.CustomActions;	
	import flash.utils.describeType;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Condition
	{
		private static var _methodInfo:Dictionary;
		private static function parseMethodInfo():void
		{
			var typeInfo:XML = describeType(Condition);
			trace(typeInfo);
			for each(var methodInfo:XML in typeInfo.method)
			{
				var paramTable:Object = { };
				for each(var paramInfo:XML in methodInfo.parameter)
				{
					
				}
				_methodInfo[methodInfo.@name] = paramTable;
			}
		}
		
		private var _func:Function;
		private var _params:Array;
		
		public function Condition(data:String)
		{
			/*
			if (_methodInfo == null)
			{
				_methodInfo = new Dictionary();
				parseMethodInfo();
			}
			_func = Condition[data.type];
			_params = [];
			*/
			parseCondition(data as String);
		}
		
		private function parseCondition(data:String):void 
		{
			var lbIndex:int = data.indexOf("(");
			ASSERT(lbIndex > 0);
			var rbIndex:int = data.indexOf(")");
			ASSERT(rbIndex > lbIndex && rbIndex < data.length);
			var methodName:String = data.substring(0, lbIndex);
			_func = Condition[methodName];
			ASSERT(_func != null, "Can't find condition checker:" + methodName);
			_params = data.substring(lbIndex + 1, rbIndex).split(",");
		}
		
		public function check(co:CharacterObject):Boolean
		{
			var params:Array = [co];
			params.push.apply(params, _params);
			return _func.apply(this, params);
		}
		
		public static function checkTeam(co:CharacterObject, teamId:int):Boolean
		{
			return co.teamId == teamId;
		}
	}

}