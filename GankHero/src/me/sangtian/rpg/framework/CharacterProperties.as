package me.sangtian.rpg.framework 
{
	import flash.geom.Rectangle;
	import flash.utils.describeType;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * Using property model from Warcraft III
	 * 
	 * @author tiansang
	 */
	public class CharacterProperties 
	{
		public var hp:Number = 0;
		public var hpMax:Number = 0;
		public var hpRegen:Number = 0;
		public var hpRegenAmendValue:Number = 0;
		public var hpRegenAmendRatio:Number = 0;
		public var mp:Number = 0;
		public var mpMax:Number = 0;
		public var mpRegen:Number = 0;
		public var mpRegenAmendValue:Number = 0;
		public var mpRegenAmendRatio:Number = 0;
		
		// 1 str = 19 hpMax + 0.19 hpRegen
		public var strength:int = 0;
		// 1 agi = 0.2 armor + 1% attackSpeedAmendRatio
		public var agile:int = 0;
		// 1 int = 14 mpMax + 0.14 mpRegen
		public var intelligence:int = 0;
		
		// final damage = (randInt(attackMin, attackMax) + attackAmendValue - defenceAmendValue) * (1 + attackAmendRatio) * (1 + randBool(critChance) * critBonusRatio) * (1 - defenceAmendRatio)
		public var attackMin:int = 0;
		public var attackMax:int = 0;
		public var attackAmendValue:Number = 0;
		public var attackAmendRatio:Number = 0;
		public var defenceAmendValue:Number = 0;
		public var defenceAmendRatio:Number = 0;
		public var spellDamageAmendValue:Number = 0;
		public var spellDamageAmendRatio:Number = 0;
		public var spellReductionValue:Number = 0;
		public var spellReductionRatio:Number = 0;
		public var critChance:Number = 0;
		public var critBonusRatio:Number = 0;
		// attack interval = 1 sec / (attackSpeed * (1 + attackSpeedAmendRatio))
		public var attackSpeed:Number = 0;
		public var attackSpeedAmendRatio:Number = 0;
		public var attackRange:int = 0;
		// missileSpeed > 0: ranged attack
		public var missileSpeed:Number = 0;
		
		public var moveSpeedAmendValue:Number = 0;
		public var moveSpeedAmendRatio:Number = 0;
		public var hitTestWidth:int = 0;
		public var hitTestHeight:int = 0;
		public var dropExp:int = 0;
		
		private static var _propertyNames:Vector.<String>;
		
		public static function get propertyNames():Vector.<String> 
		{
			return _propertyNames;
		}
		
		public function CharacterProperties() 
		{
			if (_propertyNames == null)
			{
				_propertyNames = ObjectUtil.getOwnPropertyNames(this);
				//trace(_propertyNames);
			}
			
		}
		
		public function getAttackInterval():Number
		{
			return 1.0 / (attackSpeed * (1 + attackSpeedAmendRatio));
		}
		
		public function getActualMoveSpeed():Number
		{
			return MathUtil.clamp(moveSpeedAmendValue * (1 + moveSpeedAmendRatio), 0, Factors.MAX_MOVE_SPEED);
		}
		
		public function add(properties:CharacterProperties):void 
		{
			for each(var key:String in propertyNames)
			{
				this[key] += properties[key];
			}
		}
		
		public function sub(properties:CharacterProperties):void 
		{
			for each(var key:String in propertyNames)
			{
				this[key] -= properties[key];
			}
		}
		
		public function testCost(cost:CharacterProperties):String 
		{
			for each(var key:String in propertyNames)
			{
				if (this[key] - cost[key] < 0)
					return key;
			}
			return null;
		}
		
		public function update():void
		{
			hpRegen = strength * Factors.STR_TO_HP_REGEN;
			mpRegen = intelligence * Factors.INT_TO_MP_REGEN;
			hp = MathUtil.clamp(hp + (hpRegen + hpRegenAmendValue) * (1 + hpRegenAmendValue) * Time.deltaSecond, 0, hpMax);
			mp = MathUtil.clamp(mp + (mpRegen + mpRegenAmendValue) * (1 + mpRegenAmendValue) * Time.deltaSecond, 0, mpMax);
		}
		
		public function clone():CharacterProperties 
		{
			var prop:CharacterProperties = new CharacterProperties();
			for each(var key:String in propertyNames)
			{
				prop[key] = this[key];
			}
			return prop;
		}
	}

}