package me.sangtian.rpg.framework 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.MathUtil;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class GameObject extends Sprite 
	{
		private var _position:Point = new Point();
		private var _isDestroyed:Boolean = false;
		private var _map:MapLayer;
		
		protected var _hitArea:Rectangle = new Rectangle();
			
		private var _hitAreaImage:Image;
		
		public var collision:Collision;
		
		public function get map():MapLayer 
		{
			return _map;
		}
		
		public function get isDestroyed():Boolean 
		{
			return _isDestroyed;
		}
		
		public function get position():Point 
		{
			_position.x = x;
			_position.y = y;
			return _position;
		}
		
		public function set position(value:Point):void 
		{
			_position = value;
			x = value.x;
			y = value.y;
		}
		
		public function get hitArea():Rectangle 
		{
			return _hitArea;
		}
		
		public function set hitArea(value:Rectangle):void 
		{
			_hitArea.copyFrom(value);
		}
		
		public function GameObject(map:MapLayer) 
		{
			_map = map;
		}
		
		public function showHitArea():void
		{
			if (_hitAreaImage == null)
			{
				_hitAreaImage = new Image(Texture.fromColor(hitArea.width, hitArea.height, 0x77ff0000));
				_hitAreaImage.x = hitArea.x - x;
				_hitAreaImage.y = hitArea.y - y;
				addChild(_hitAreaImage);
			}
		}
		
		public function moveStep(target:Point, speed:Number):Number
		{
			var d:Point = target.subtract(position);
			var scale:Number = MathUtil.clamp(speed * Time.deltaSecond / d.length, 0, 1);
			var direction:Point = new Point(d.x * scale, d.y * scale);
			if (scale == 1)
			{
				x = target.x;
				y = target.y;
			}
			else
			{
				x = x + direction.x;
				y = y + direction.y;
			}
			direction.normalize(1);
			map.solveCollision(this, direction);
			return scale;
		}
		
		public function update():void 
		{
			
		}
		
		public function destroy():void
		{
			_isDestroyed = true;
			_map = null;
			dispose();
		}
	}

}