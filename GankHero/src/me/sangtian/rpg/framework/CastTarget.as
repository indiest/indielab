package me.sangtian.rpg.framework 
{
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class CastTarget 
	{
		public function check(caster:CharacterObject, skill:Skill):Boolean
		{
			return false;
		}
		
		protected function checkDistance(caster:CharacterObject, target:CharacterObject, skill:Skill):Boolean
		{
			return caster.getDistance(target) <= skill.properties.castDistance;
		}
		
		protected function checkCharacterType(caster:CharacterObject, target:CharacterObject, skill:Skill):Boolean
		{
			return skill.properties.targetCharacterType.checkBitwise(caster.getRelativeCharacterType(target));
		}
		
		public function getApplicableCharacters():Vector.<CharacterObject>
		{
			return null;
		}
	}
	
}