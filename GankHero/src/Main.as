package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import me.sangtian.rpg.gankhero.Game;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			Starling.handleLostContext = true;
			new Starling(Game, stage).start();
		}
		
	}
	
}