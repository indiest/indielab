package  
{
	import flash.ui.Keyboard;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class ParticleSystemTest extends Sprite 
	{
		[Embed(source = "/../bin/particle/particle.pex", mimeType = "application/octet-stream")]
		private static const ParticleConfig:Class;
		[Embed(source="/../bin/particle/texture.png")]
		private static const ParticleTexture:Class;
		private var _particleSystems:Vector.<PDParticleSystem> = new Vector.<PDParticleSystem>();
		
		public function ParticleSystemTest() 
		{
			var config:XML = XML(new ParticleConfig()); 
			var texture:Texture = Texture.fromBitmap(new ParticleTexture());
			for (var i:int = 0; i < 36; i++)
			{
				var ps:PDParticleSystem = new PDParticleSystem(config, texture);
				ps.x = 300;
				ps.y = 200;
				ps.emitAngle = i * 10;
				ps.start(ps.lifespan);
				addChild(ps);
				_particleSystems.push(ps);
				Starling.juggler.add(ps);
			}
			
			//Starling.current.stage.addEventListener(KeyboardEvent.KEY_UP, handlePressedKey);
			Starling.current.stage.addEventListener(TouchEvent.TOUCH, handleTouch);
		}
		
		private function handleTouch(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch(stage, TouchPhase.ENDED);
			if (touch)
			{
				for each(var ps:PDParticleSystem in _particleSystems)
				{
					ps.x = touch.globalX;
					ps.y = touch.globalY;
					ps.start(ps.lifespan);
				}
			}
		}
		
		private function handlePressedKey(e:KeyboardEvent):void 
		{
			if (e.keyCode == Keyboard.SPACE)
			{
				for each(var ps:PDParticleSystem in _particleSystems)
				{
					ps.start(ps.lifespan);
				}
			}
		}
		
	}

}