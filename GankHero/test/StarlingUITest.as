package  
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.system.System;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import flash.utils.getTimer;
	import me.sangtian.common.ui.ItemGroup;
	import me.sangtian.common.ui.ProgressBar;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.StarlingUtil;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author 
	 */
	public class StarlingUITest extends Sprite 
	{
		public var buttonPause:Button;
		public var teammate0:Sprite;
		public var teammate1:Sprite;
		public var teammate2:Sprite;
		public var teammate3:Sprite;
		private var _teammateGroup:ItemGroup;
		
		public function StarlingUITest() 
		{
			trace("Memory(after initializing Starling):", System.totalMemory / 1024, "KB");
			var ui:PlayStateUI = new PlayStateUI();
			trace("Memory(after initializing Flash UI):", System.totalMemory / 1024, "KB");
			var time:Number = getTimer();
			var starlingUI:Sprite = StarlingUtil.generateFromFlashUI(ui, this);
			trace("Generated in", time, "ms");
			trace("Memory(after generating Starling UI):", System.totalMemory / 1024, "KB");
			ui = null;
			System.pauseForGCIfCollectionImminent();
			System.gc();
			trace("Memory(after GC):", System.totalMemory / 1024, "KB");
			//starlingUI.flatten();
			addChild(starlingUI);
			Starling.current.showStats = true;
			
			buttonPause.addEventListener(Event.TRIGGERED, handlePause);
			_teammateGroup = new ItemGroup(this, "teammate");
			for (var i:int = 0; i < _teammateGroup.count; i++)
			{
				new ProgressBar(_teammateGroup.getItem(i).hpBar);
			}
			_teammateGroup.addEventListener(TouchEvent.TOUCH, handleTouchTeammate);
		}
		
		private function handleTouchTeammate(e:TouchEvent):void 
		{
			trace("Clicked teammate:", _teammateGroup.getIndex(e.currentTarget));
		}
		
		private function handlePause(e:Event):void 
		{
			trace("Paused");
		}
	}

}