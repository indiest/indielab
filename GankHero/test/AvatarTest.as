package  
{
	import me.sangtian.rpg.framework.Avatar;
	import me.sangtian.rpg.framework.GameObject;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class AvatarTest extends Sprite 
	{
		private var avatar:Avatar;
		
		public function AvatarTest() 
		{
			var obj:GameObject = new GameObject(null);
			obj.x = 100;
			obj.y = 100;
			addChild(obj);
			avatar = new Avatar(obj);
			avatar.init( { offsetX: -20, offsetY: -20, width: 40, height: 40 } );
			avatar.play("attack");
			
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			avatar.timeScale = 1.5;
			avatar.update();
		}
		
	}

}