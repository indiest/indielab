package  
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author 
	 */
	public class QuadColorTest extends Sprite 
	{
		
		public function QuadColorTest() 
		{
			// red rectangle
			var tex:Texture = Texture.fromColor(100, 100, 0xffff0000);
			var image:Image = new Image(tex);
			// tint to green
			image.color = 0x00ff00;
			// final color = texColor * tintColor = 0x000000 (black)
			addChild(image);
		}
		
	}

}