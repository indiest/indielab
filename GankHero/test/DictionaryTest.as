package  
{
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class DictionaryTest extends Sprite 
	{
	
		public static const SOME_NUMBER:Number = 2;
		public function DictionaryTest() 
		{
			var dic:Dictionary = new Dictionary();
			dic[1] = 1;
			dic[SOME_NUMBER] = 2;
			dic[this] = 3;
			dic["this"] = 4;
			for (var key:* in dic)
			{
				trace(key, typeof(key));
			}
		}
		
	}

}