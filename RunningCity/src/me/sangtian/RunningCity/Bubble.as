package me.sangtian.RunningCity 
{
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import org.bytearray.display.ScaleBitmap;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxText;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Bubble extends FlxSprite
	{
		private var _scaleBitmap:ScaleBitmap;
		private var _textField:TextField;
		
		public function Bubble() 
		{
			_scaleBitmap = new ScaleBitmap(FlxG.addBitmap(Assets.BUBBLE).clone());
			_scaleBitmap.scale9Grid = new Rectangle(8, 3, 4, 4);
			
			_textField = new TextField();
			_textField.autoSize = TextFieldAutoSize.LEFT;
			_textField.embedFonts = true;
			_textField.selectable = false;
			_textField.sharpness = 100;
			_textField.multiline = true;
			//_textField.wordWrap = true;
			var format:TextFormat = new TextFormat("system",8,0xffffff);
			_textField.defaultTextFormat = format;
			_textField.setTextFormat(format);
		}
		
		public function set text(/*width:Number, height:Number, */text:String):void
		{
			_textField.text = text;
			_scaleBitmap.setSize(_textField.width + 6, _textField.height + 10);
			pixels = _scaleBitmap.bitmapData;
			_matrix.identity();
			_matrix.translate(3, 3);
			pixels.draw(_textField, _matrix, _colorTransform);
			framePixels.copyPixels(_pixels,_flashRect,_flashPointZero);
		}
		
		public function get text():String
		{
			return _textField.text;
		}
	}

}