package me.sangtian.RunningCity 
{
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Citizen extends FlxSprite
	{
		public var falling:Boolean = false;
		
		public function Citizen() 
		{
			loadGraphic(Assets.SPRITES, true, true, 16, 16);
			width = 6;
			height = 13;
			offset.make(2, 1);
			addAnimation("stand", [0]);
			addAnimation("fall_left", [7, 8, 8, 9], 10, false);
			addAnimation("fall_right", [3, 4, 5, 6], 10, false);
		}
		
		public function fall(vx:Number):void
		{
			falling = true;
			velocity.x = vx;
			velocity.y = 0;
			acceleration.x = 0;
			drag.x = Math.abs(vx * 10);
			if (facing == FlxObject.RIGHT)
			{
				if (vx > 0)
					play("fall_right");
				else
					play("fall_left");
			}
			else
			{
				if (vx > 0)
					play("fall_left");
				else
					play("fall_right");
			}
		}
		
		public function stand():void
		{
			velocity.x = 0;
			play("stand");
		}
		
		private var _bubble:Bubble;
		
		public function say(text:String):void
		{
			var state:PlayState = FlxG.state as PlayState;
			if (_bubble == null)
			{
				_bubble = new Bubble();
				_bubble.color = color;
				state.add(_bubble);
			}
			_bubble.text = text;
			_bubble.x = x + width;
			_bubble.y = y - _bubble.height;
		}
	}

}