package me.sangtian.RunningCity 
{
	import flash.events.Event;
	import org.flixel.FlxGame;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Game extends FlxGame 
	{
		public static const GAME_WIDTH:uint = 320;
		public static const GAME_HEIGHT:uint = 240;
		public static const BOUND_UP:uint = 100;
		public static const BOUND_BOTTOM:uint = GAME_HEIGHT;
		
		public function Game()
		{
			super(GAME_WIDTH, GAME_HEIGHT, PlayState, 2);
		}
	}

}