package me.sangtian.RunningCity 
{
	import flash.geom.Rectangle;
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxRect;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PlayState extends FlxState 
	{
		private var _street:Street = new Street();
		private var _runner:Player = new Player();
		private var _others:FlxGroup = new FlxGroup();
		private var _bottomText:FlxText = new FlxText(0, Game.GAME_HEIGHT - 12, Game.GAME_WIDTH);
		private var _uiCam:FlxCamera;
		
		public function get runner():Runner
		{
			return _runner;
		}
		
		override public function create():void 
		{
			add(_street);
			
			_runner.x = Game.GAME_WIDTH / 2;
			_runner.y = Game.GAME_HEIGHT / 2;
			add(_runner);
			FlxG.camera.follow(_runner, FlxCamera.STYLE_PLATFORMER);
			FlxG.camera.deadzone.y = 0;
			FlxG.camera.deadzone.height = FlxG.camera.height;
			
			for (var i:uint = 0; i < 30; i++)
			{
				var walker:Walker = new Walker();
				walker.init();
				_others.add(walker);
			}
			add(_others);
			
			for (var x:int = 0; x < 5; x++)
			{
				for (var y:int = 0; y < x * 5 + 1; y++)
				{
					var enemy:Enemy = new Enemy();
					enemy.x = 30 - x * 7;
					enemy.y = 100 + 60 - x * 15 + y * 15;
					add(enemy);
				}
			}
			
			_bottomText.alignment = "center";
			add(_bottomText);
			_uiCam = new FlxCamera(_bottomText.x * 2, _bottomText.y * 2, _bottomText.width, _bottomText.height, 2);
			_uiCam.follow(_bottomText);
			FlxG.addCamera(_uiCam);
			
			//var cam:FlxCamera = new FlxCamera(0, 0, FlxG.width, FlxG.height, 2);
			//cam.follow(_street);
			//FlxG.addCamera(cam);

			FlxG.stream("run.mp3", 1.0, true);
		}
		
		public function setBottomText(text:String):void
		{
			_bottomText.text = text;
		}
		
		override public function update():void 
		{
			super.update();
			
			//FlxG.collide(_runner, _others, onOverlaps);
		}
		
		private function onOverlaps(obj1:FlxObject, obj2:FlxObject):void 
		{
			trace(obj1, obj2, new Date());
		}
	}

}