package me.sangtian.RunningCity 
{
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Runner extends Citizen 
	{
		
		public function Runner() 
		{
			//makeGraphic(5, 10);
			addAnimation("run", [10, 11, 12, 13], 8);
			play("run");
			
			maxVelocity.make(200, 120);
			drag.y = 1200;
		}
		
		public function run(dir:uint):void
		{
			if (dir == FlxObject.LEFT)
			{
				acceleration.x = -this.maxVelocity.x * 2;
			}
			else if (dir == FlxObject.RIGHT)
			{
				acceleration.x = this.maxVelocity.x * 2;
			}
			else if (dir == FlxObject.UP)
			{
				velocity.y = -maxVelocity.y;
			}
			else if (dir == FlxObject.DOWN)
			{
				velocity.y = maxVelocity.y;
			}
		}
		
		override public function update():void 
		{
			super.update();
			
			if (x < 0)
				x = 0;
			y = FlxU.bound(y, Game.BOUND_UP, Game.BOUND_BOTTOM);
			
			facing = velocity.x < 0 ? FlxObject.LEFT : FlxObject.RIGHT;
		}
	}

}