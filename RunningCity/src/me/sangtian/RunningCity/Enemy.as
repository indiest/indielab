package me.sangtian.RunningCity 
{
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Enemy extends Runner
	{
		
		public function Enemy() 
		{
			color = 0xaa0000;
			//FlxG.play(Assets.SOUND_RUNNING, 0.3, true);
		}
		
		override public function update():void 
		{
			super.update();
			if (falling)
				return;

			var state:PlayState = FlxG.state as PlayState;
			if (overlaps(state.runner))
			{
				fall(velocity.x);
				y += Util.randNumber( -5, 5);
				if (!state.runner.falling)
				{
					state.runner.fall(velocity.x);
				}
			}
			else
			{
				if (x > state.runner.x)
					run(FlxObject.LEFT);
				else
					run(FlxObject.RIGHT);
				
				if (Math.abs((y - state.runner.y) / (x - state.runner.x)) > maxVelocity.y / maxVelocity.x)
				{
					if (y > state.runner.y)
						run(FlxObject.UP);
					else
						run(FlxObject.DOWN);
				}
			}
		}
	}

}