package me.sangtian.RunningCity 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Util
	{
		public static function randInt(min:int, max:int):int
		{
			return min + (max - min) * Math.random();
		}
		
		public static function randNumber(min:Number, max:Number):Number
		{
			return min + (max - min) * Math.random();
		}
		
		public static function sign(n:Number):Number
		{
			return n > 0 ? 1 : (n < 0 ? -1 : 0);
		}
	}

}