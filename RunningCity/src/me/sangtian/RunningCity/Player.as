package me.sangtian.RunningCity 
{
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Player extends Runner
	{
		
		public function Player() 
		{
			maxVelocity.x = 280;
			run(FlxObject.RIGHT);
		}
		
		override public function update():void 
		{
			super.update();
			if (!moves && FlxG.keys.SPACE)
			{
				FlxG.switchState(new PlayState());
			}
			if (falling)
				return;
			
			if (FlxG.keys.LEFT || FlxG.keys.A)
			{
				run(FlxObject.LEFT);
			}
			if (FlxG.keys.RIGHT || FlxG.keys.D)
			{
				run(FlxObject.RIGHT);
			}
			if (FlxG.keys.UP || FlxG.keys.W)
			{
				run(FlxObject.UP);
			}
			if (FlxG.keys.DOWN || FlxG.keys.S)
			{
				run(FlxObject.DOWN);
			}
		}
		
		override public function fall(vx:Number):void 
		{
			super.fall(vx);
			FlxG.shake();
			FlxG.flash(0xffff0000, 0.5, onFlashComplete);
		}
		
		private function onFlashComplete():void 
		{
			moves = false;
			(FlxG.state as PlayState).setBottomText("Press SPACE to restart");
		}
	}

}