package me.sangtian.RunningCity 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Assets
	{
		[Embed(source="assets/Sprites.png")]
		public static const SPRITES:Class;
		
		[Embed(source="assets/Bubble.png")]
		public static const BUBBLE:Class;
		
		//[Embed(source="assets/running.mp3")]
		//public static const SOUND_RUNNING:Class;
	}

}