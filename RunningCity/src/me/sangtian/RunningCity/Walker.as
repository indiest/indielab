package me.sangtian.RunningCity 
{
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Walker extends Citizen 
	{
		public static const MAX_SPEED:uint = 100;
		
		public function Walker() 
		{
			addAnimation("walk", [1, 2], 8);
			color = 0x888888;
			init();
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			//moves = (Math.abs(state.runner.x - x) < Game.GAME_WIDTH * 1.5);
			if (Math.abs(state.runner.x - x) > Game.GAME_WIDTH * 2)
			{
				init();
				return;
			}
			if (!falling && !state.runner.falling && overlaps(state.runner))
			{
				// More overlapping makes more collision
				var dy:Number = height - Math.abs(y - state.runner.y);
				var ratio:Number = dy / height;
				state.runner.velocity.x *= Math.max(0.3, 1 - ratio);
				if (ratio > 0.5)
				{
					var power:Number = dy * Math.abs(velocity.x + state.runner.velocity.x);
					fall(Util.sign(state.runner.velocity.x) * power);
					//FlxG.framerate = 10;
					FlxG.camera.zoom = 3;
					FlxG.shake(0.03, 0.5, onShakeComplete, true, FlxCamera.SHAKE_VERTICAL_ONLY);
				}
				else
				{
					falling = true;
					stand();
					var words:String = WORDS[uint(Math.random() * 10)];
					if (words)
						say(words);
				}
			}
		}
		
		private static const WORDS:Array = ["Hey!", "Watch out!", "What the F..."];
		
		private function onShakeComplete():void
		{
			//FlxG.framerate = 30;
			FlxG.camera.zoom = 2;
			flicker(0.5, onFlickerComplete);
		}
		
		private function onFlickerComplete():void 
		{
			init();
			if (Math.random() > 0.5)
			{
				var state:PlayState = FlxG.state as PlayState;
				var enemy:Enemy = new Enemy();
				enemy.x = state.runner.x + Util.randNumber(Game.GAME_WIDTH, Game.GAME_WIDTH * 2);
				enemy.y = Util.randNumber(Game.BOUND_UP, Game.BOUND_BOTTOM);
				state.add(enemy);
			}
		}
		
		public function init():void
		{
			var state:PlayState = FlxG.state as PlayState;
			falling = false;
			reset(state.runner.x + Util.randNumber(Game.GAME_WIDTH, Game.GAME_WIDTH * 2),
				Util.randNumber(Game.BOUND_UP, Game.BOUND_BOTTOM));
			velocity.x = Util.randNumber( -MAX_SPEED, MAX_SPEED);
			drag.x = 0;
			facing = velocity.x < 0 ? FlxObject.LEFT : FlxObject.RIGHT;
			play("walk");
		}
	}

}