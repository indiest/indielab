package me.sangtian.RunningCity 
{
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Street extends FlxSprite
	{
		
		public function Street() 
		{
			super(0, Game.BOUND_UP);
			makeGraphic(Game.GAME_WIDTH, Game.BOUND_BOTTOM - Game.BOUND_UP, 0xff333333);
			scrollFactor.make(0, 0);
		}
		
		override public function draw():void 
		{
			//drawLine(0, Game.BOUND_UP, Game.GAME_WIDTH, Game.BOUND_UP, 0xffbbbbbb);
			super.draw();
		}
	}

}