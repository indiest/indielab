package  
{
	import flash.display.Sprite;
	import me.sangtian.unityflash.Enum;
	import me.sangtian.unityflash.LightType;
	import me.sangtian.unityflash.TextureWrapMode;
	
	/**
	 * ...
	 * @author 
	 */
	public class EnumTest extends Sprite 
	{
		
		public function EnumTest() 
		{
			var lightType:LightType = LightType.Point;
			var wrapMode:TextureWrapMode = TextureWrapMode.Repeat;
			trace(lightType);
			trace(wrapMode);
			trace(Enum.fromIndex(LightType, LightType.Point.index) == lightType);
		}
		
	}

}