package  
{
	import away3d.containers.View3D;
	import away3d.entities.Mesh;
	import away3d.lights.PointLight;
	import away3d.materials.ColorMaterial;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.SkyBox;
	import away3d.primitives.SphereGeometry;
	import away3d.textures.BitmapCubeTexture;
	import away3d.utils.Cast;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Vector3D;
	import me.sangtian.common.Input;
	import me.sangtian.common.Time;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class AwayLightingTest extends Sprite
	{
		private var view:View3D;
		private var light:PointLight;

		[Embed( source = "/../res/skybox/space_posX.jpg" )]
		protected static const EnvPosX:Class;

		[Embed( source = "/../res/skybox/space_negX.jpg" )]
		protected static const EnvNegX:Class;

		[Embed( source = "/../res/skybox/space_posY.jpg" )]
		protected static const EnvPosY:Class;

		[Embed( source = "/../res/skybox/space_negY.jpg" )]
		protected static const EnvNegY:Class;

		[Embed( source = "/../res/skybox/space_posZ.jpg" )]
		protected static const EnvPosZ:Class;

		[Embed( source = "/../res/skybox/space_negZ.jpg" )]
		protected static const EnvNegZ:Class;
		
		public function AwayLightingTest() 
		{
			view = new View3D();
			addChild(view);
			view.camera.y = 1;
			view.camera.z = -10;
			view.camera.lookAt(new Vector3D(0, 0, 0));
			
			var cubeMap:BitmapCubeTexture = new BitmapCubeTexture(
				Cast.bitmapData(EnvPosX), Cast.bitmapData(EnvNegX), 
				Cast.bitmapData(EnvPosY), Cast.bitmapData(EnvNegY), 
				Cast.bitmapData(EnvPosZ), Cast.bitmapData(EnvNegZ));
			var skybox:SkyBox = new SkyBox(cubeMap);
			view.scene.addChild(skybox);
			
			light = new PointLight();
			view.scene.addChild(light);
			var sphere:Mesh = new Mesh(new SphereGeometry(5), new ColorMaterial(0xffffff));
			view.scene.addChild(sphere);
			
			Input.initialize(stage);
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			Time.tick();
			Input.update();
			
			view.camera.rotationY += Input.mouseDelta.x * 0.5;
			view.camera.rotationX += Input.mouseDelta.y * 0.5;
			light.x = 10 * Math.cos(Time.lastTickTime / 1000);
			light.z = 10 * Math.sin(Time.lastTickTime / 1000);
			trace(light.position);
			
			view.render();
		}
		
	}

}