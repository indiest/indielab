@echo off
set EXE=D:\Flash\sdk\pixelblender3d\bin\windows\pb3dutil.exe
set FileName=%~dnp1
set Extension="%~x1"
if %Extension%==".pbvk" call %EXE% %1 %FileName%V.pbasm
if %Extension%==".pbmk" call %EXE% %1 %FileName%MV.pbasm %FileName%F.pbasm
@pause