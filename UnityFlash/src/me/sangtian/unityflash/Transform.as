package me.sangtian.unityflash 
{
	import flash.geom.Matrix;
	import flash.geom.Matrix3D;
	import flash.geom.Orientation3D;
	import flash.geom.Vector3D;
	
	/**
	 * ...
	 * @author 
	 */
	public class Transform extends Component
	{
		private static const ORIGIN:Vector3D = new Vector3D();
		private static const POINT_AT:Vector3D = new Vector3D(0, 0, 1);
		
		private var _matrix:Matrix3D;
		
		public function get localPosition():Vector3D
		{
			return _matrix.position;
		}
		
		public function set localPosition(value:Vector3D):void
		{
			_matrix.position = value;
		}
		
		public function get rotation():Vector3D
		{
			return _matrix.decompose(Orientation3D.QUATERNION)[1];
		}
		
		public function get eulerAngles():Vector3D
		{
			return _matrix.decompose(Orientation3D.EULER_ANGLES)[1];
		}
		
		public function get scale():Vector3D
		{
			return _matrix.decompose()[2];
		}
		
		public function set scale(value:Vector3D):void
		{
			var s:Vector3D = scale;
			_matrix.prependScale(value.x / s.x, value.y / s.y, value.z / s.z);
		}
		
		public function get right():Vector3D
		{
			return _matrix.deltaTransformVector(Vector3D.X_AXIS);
		}
		
		public function get up():Vector3D
		{
			return _matrix.deltaTransformVector(Vector3D.Y_AXIS);
		}
		
		public function get forward():Vector3D
		{
			return _matrix.deltaTransformVector(POINT_AT);
		}
		
		public function Transform(matrix:Matrix3D = null) 
		{
			_matrix = matrix || new Matrix3D();
		}
		
		public function transformVector(v:Vector3D, positionMultiplier:Number = 1):Vector3D
		{
			if (positionMultiplier == 0)
			{
				return _matrix.deltaTransformVector(v);
			}
			else
			{
				return _matrix.transformVector(v);
			}
		}
		
		public function deltaTransformVector(v:Vector3D):Vector3D
		{
			return _matrix.deltaTransformVector(v);
		}
		
		public function rotate(eulerAngles:Vector3D, multiplier:Number = 1):void
		{
			if (eulerAngles.y != 0)
				rotateAround(eulerAngles.y * multiplier, Vector3D.Y_AXIS);
			if (eulerAngles.x != 0)
				rotateAround(eulerAngles.x * multiplier, Vector3D.X_AXIS);
			if (eulerAngles.z != 0)
				rotateAround(eulerAngles.z * multiplier, Vector3D.Z_AXIS);
		}
		
		public function rotateAround(angles:Number, axis:Vector3D, pivotPoint:Vector3D = null):void
		{
			_matrix.prependRotation(angles, axis, pivotPoint);
		}
		
		public function translate(x:Number, y:Number, z:Number):void
		{
			//trace(x, y, z);
			_matrix.appendTranslation(x, y, z);
			//localPosition = localPosition.add(new Vector3D(x, y, z));
		}
		
		public function translateByVector(v:Vector3D, multiplier:Number = 1):void
		{
			if (multiplier != 0)
				translate(v.x * multiplier, v.y * multiplier, v.z * multiplier);
		}
		
		public function lookAtDirection(localDir:Vector3D, worldUp:Vector3D = null):void
		{
			var zVector : Vector3D = localDir;
			var upVector : Vector3D = worldUp || Vector3D.Y_AXIS;

			var xVector : Vector3D = upVector.crossProduct( zVector );
			var yVector : Vector3D = zVector.crossProduct( xVector );

			xVector.normalize();
			yVector.normalize();
			zVector.normalize();

			_matrix.rawData = Vector.<Number>( [
				xVector.x, xVector.y, xVector.z, 0.0,
				yVector.x, yVector.y, yVector.z, 0.0,
				zVector.x, zVector.y, zVector.z, 0.0,
				localPosition.x, localPosition.y, localPosition.z, 1.0
			]);
		}
				
		public function lookAt(worldPos:Vector3D, worldUp:Vector3D = null):void
		{
			_matrix.pointAt(worldPos, POINT_AT, worldUp || Vector3D.Y_AXIS);

			/*
			var pos:Vector3D = getWorldToLocalMatrix().transformVector(worldPos);
			var v : Vector3D = pos.subtract( localPosition );

			// The camera points along the negative Z axis
			v.negate();
			
			lookAtDirection(v, worldUp);
			*/
		}

		public function getWorldToLocalMatrix():Matrix3D
		{
			var matrix:Matrix3D = toMatrix();
			var p:GameObject = gameObject.parent;
			while (p != null)
			{
				// Matrix multiplication is associative
				matrix.prepend(p.transform.toMatrix());
				p = p.parent;
			}
			return matrix;
		}
		
		public function getLocalToWorldMatrix():Matrix3D
		{
			var matrix:Matrix3D = toMatrix();
			var p:GameObject = gameObject.parent;
			while (p != null)
			{
				matrix.append(p.transform.toMatrix());
				p = p.parent;
			}
			return matrix;
		}
		
		public function getWorldPosition():Vector3D
		{
			return getLocalToWorldMatrix().position;//.transformVector(localPosition);
		}
		
		public function toMatrix():Matrix3D
		{
			return _matrix.clone();
		}
		
		public function toString():String
		{
			return "Position:" + localPosition + " Rotation:" + rotation + " Scale:" + scale;
		}
	}

}