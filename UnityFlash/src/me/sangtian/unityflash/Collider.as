package me.sangtian.unityflash
{
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	import me.sangtian.common.logging.Log;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class Collider extends Behavior 
	{
		public var isTrigger:Boolean = false;
		public var material:PhysicMaterial;
		// world-space bounding volume
		public var bounds:Bounds;
		
		public function Collider() 
		{
			
		}
		
		public function raycast(ray:Ray, distance:Number = -1, hitInfo:RaycastHit = null):Boolean
		{
			throw new Error("raycast should be implemented by subclass");
		}
		
		public function onCollisionEnter(collision:Collision):void
		{
			Log.debug("CollisionEnter:", gameObject, "-", collision.collider.gameObject);
		}
		
		public function onCollisionStay(other:Collider):void
		{
			Log.debug("CollisionStay:", gameObject, "-", other.gameObject);
		}
		
		public function onCollisionExit(collision:Collision):void
		{
			Log.debug("CollisionExit:", gameObject, "-", collision.collider.gameObject);
		}
		
		protected function checkCollision(collider:Collider):Collision 
		{
			throw new Error("checkCollision should be implemented by subclass");
		}
		
		private static var _colliders:Vector.<Collider> = new Vector.<Collider>();
		
		override internal function onAddedToGameObject():void 
		{
			_colliders.push(this);
		}
		
		// key = other collider, value = collision
		private var _collisions:Dictionary = new Dictionary(true);
		
		protected function updateBounds():void
		{
			// Implement by subclass
		}
		
		private var _lastPosition:Vector3D;
		
		override public function update():void 
		{
			updateBounds();
			
			for each(var collider:Collider in _colliders)
			{
				if (!collider.enabled || collider == this || (collider.gameObject.layer & this.gameObject.layer) == 0)
					continue;
				
				var oldCollision:Collision = _collisions[collider];
				var newCollision:Collision = checkCollision(collider);
				if (oldCollision != null)
				{
					if (newCollision == null)
					{
						sendMessage("onCollisionExit", oldCollision);
						delete _collisions[collider];
					}
					else
					{
						sendMessage("onCollisionStay", collider);
					}
				}
				else
				{
					if (newCollision != null)
					{
						sendMessage("onCollisionEnter", newCollision);
						_collisions[collider] = newCollision;
						if (_lastPosition != null)
						{
							transform.localPosition = _lastPosition;
						}
					}
				}
			}
			
			_lastPosition = transform.localPosition;
		}
	}

}