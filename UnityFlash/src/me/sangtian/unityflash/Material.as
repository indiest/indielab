package me.sangtian.unityflash 
{
	import com.adobe.pixelBender3D.utils.ProgramConstantsHelper;
	import flash.display3D.Context3DProgramType;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Material 
	{
		public var shader:Shader;
		
		private var _color:Color;
		private var _mainTexture:Texture;
		
		public function get color():Color 
		{
			return _color;
		}
		
		public function set color(value:Color):void 
		{
			_color = value || Color.GREY;
			if (shader.getProgramTypeOfParameter("mainColor") != null)
			{
				shader.setColor("mainColor", _color);
			}
		}
		
		public function get mainTexture():Texture 
		{
			return _mainTexture;
		}
		
		public function set mainTexture(value:Texture):void 
		{
			_mainTexture = value || Texture.DEFAULT;
		}
		
		public function Material(shader:Shader, color:Color = null, mainTexture:Texture = null) 
		{
			this.shader = shader;
			this.color = color;
			this.mainTexture = mainTexture;
		}
		
		public function update():void 
		{
			shader.setTextureAt(0, _mainTexture);
			shader.update();
		}
	}

}