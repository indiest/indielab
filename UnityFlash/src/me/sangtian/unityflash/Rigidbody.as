package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Rigidbody extends Component 
	{
		public var velocity:Vector3D = new Vector3D();
		public var angularVelocity:Vector3D = new Vector3D();
		public var drag:Number = 0;
		public var angularDrag:Number = 0.05;
		public var mass:Number = 1;
		public var useGravity:Boolean = true;
		public var isKinematic:Boolean = false;
		
		private var _acceleration:Vector3D = new Vector3D();
		private var _angularAcceleration:Vector3D = new Vector3D();
		
		public function Rigidbody() 
		{
			
		}
		
		public function addForce(force:Vector3D, mode:ForceMode/* = ForceMode.Force*/):void
		{
			if (mode == ForceMode.Force)
			{
				var acc:Vector3D = force.clone();
				acc.scaleBy(1 / mass);
				_acceleration.incrementBy(acc);
			}
			else if (mode == ForceMode.Acceleration)
			{
				_acceleration.incrementBy(force);
			}
			else if (mode == ForceMode.Impluse)
			{
				var vel:Vector3D = force.clone();
				vel.scaleBy(1 / mass);
				velocity.incrementBy(vel);
			}
			else if (mode == ForceMode.VelocityChange)
			{
				velocity.incrementBy(force);
			}
		}
		
		override public function update():void 
		{
			transform.translateByVector(velocity, Time.deltaSecond);
			if (_acceleration.x != 0)
			{
				velocity.x += _acceleration.x * Time.deltaSecond;
			}
			else
			{
				velocity.x = MathUtil.dragNumber(velocity.x, drag);
			}
			if (_acceleration.y != 0)
			{
				velocity.y += _acceleration.y * Time.deltaSecond;
			}
			else
			{
				velocity.y = MathUtil.dragNumber(velocity.y, drag);
			}
			if (_acceleration.z != 0)
			{
				velocity.z += _acceleration.z * Time.deltaSecond;
			}
			else
			{
				velocity.z = MathUtil.dragNumber(velocity.z, drag);
			}
			
			
			transform.rotate(angularVelocity, Time.deltaSecond);
			if (_angularAcceleration.x != 0)
			{
				angularVelocity.x += _angularAcceleration.x * Time.deltaSecond;
			}
			else
			{
				angularVelocity.x = MathUtil.dragNumber(angularVelocity.x, angularDrag);
			}
			if (_angularAcceleration.y != 0)
			{
				angularVelocity.y += _angularAcceleration.y * Time.deltaSecond;
			}
			else
			{
				angularVelocity.y = MathUtil.dragNumber(angularVelocity.y, angularDrag);
			}
			if (_angularAcceleration.z != 0)
			{
				angularVelocity.z += _angularAcceleration.z * Time.deltaSecond;
			}
			else
			{
				angularVelocity.z = MathUtil.dragNumber(angularVelocity.z, angularDrag);
			}
			
		}
		
		public function onCollisionEnter(collision:Collision):void
		{
			var bounciness:Number = collider.material == null ? 0 : collider.material.bounciness;
			if (collision.collider.material != null)
			{
				if (collision.collider.material.bounceCombine == PhysicMaterialCombine.Average)
					bounciness = (bounciness + collision.collider.material.bounciness) * 0.5;
				else if (collision.collider.material.bounceCombine == PhysicMaterialCombine.Multiply)
					bounciness = bounciness * collision.collider.material.bounciness;
				else if (collision.collider.material.bounceCombine == PhysicMaterialCombine.Minimum)
					bounciness = Math.min(bounciness, collision.collider.material.bounciness);
				else if (collision.collider.material.bounceCombine == PhysicMaterialCombine.Maximum)
					bounciness = Math.max(bounciness, collision.collider.material.bounciness);
			}
			// TODO: friction
			
			if (collision.collider.rigidbody == null)
			{
				// collide with static object
				velocity.scaleBy( -1 * bounciness);
			}
			else
			{
				
			}
		}
	}

}