package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Terrain extends Behavior 
	{
		private var _renderer:MeshRenderer;
		private var _data:TerrainData;
		
		public function Terrain(data:TerrainData) 
		{
			_data = data;
		}
		
		private function initRenderer():void 
		{
			_renderer = new MeshRenderer(Primitive.createPlaneMesh(_data.width, _data.height));
			updateMesh();
			updateMaterial();
			gameObject.addComponent(_renderer);
		}
		
		private function updateMaterial():void 
		{
			_renderer.material = new Material(ShaderLib.createDiffuseShader());
			_renderer.material.mainTexture = combineDetailTexture();
		}
		
		private function combineDetailTexture():Texture 
		{
			if (_data.detailPrototypes.length == 0)
				return null;
				
			var detailTexture:Texture2D = _data.detailPrototypes[0].prototypeTexture;
			// TODO: combine / update mesh's UV
			return detailTexture;
		}
		
		override public function start():void 
		{
			initRenderer();
			
			//_renderer.start();
		}
		
		private function updateMesh():void 
		{
			var vertices:Vector.<Vector3D> = _renderer.mesh.vertices;
			for (var y:int = 0; y < _data.height; y++)
			{
				for (var x:int = 0; x < _data.width; x++)
				{
					vertices[x + y * _data.width].y = _data.getHeight(x, y);
				}
			}
			_renderer.mesh.generateFaceNormals();
		}
		
		override public function update():void 
		{
			//_renderer.update();
		}
	}

}