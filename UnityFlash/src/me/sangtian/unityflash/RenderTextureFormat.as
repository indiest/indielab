package me.sangtian.unityflash 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class RenderTextureFormat extends TextureFormat 
	{
		public function RenderTextureFormat(value:String)
		{
			super(value);
		}
		
		public static const Default:RenderTextureFormat = new RenderTextureFormat(BGRA32.value);
	}

}