package me.sangtian.unityflash 
{
	import flash.display3D.Context3DTextureFormat;
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author 
	 */
	public class TextureFormat extends Enum 
	{
		private var _value:String;
		
		public function get value():String 
		{
			return _value;
		}
		
		public function TextureFormat(value:String) 
		{
			_value = value;
		}
		
		public static const BGRA32:TextureFormat = new TextureFormat(Context3DTextureFormat.BGRA);
		public static const ATF:TextureFormat = new TextureFormat(Context3DTextureFormat.COMPRESSED);
	}

}