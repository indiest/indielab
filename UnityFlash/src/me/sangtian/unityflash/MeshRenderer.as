package me.sangtian.unityflash 
{
	import com.adobe.pixelBender3D.Constants;
	import com.adobe.pixelBender3D.Semantics;
	import com.adobe.pixelBender3D.VertexRegisterInfo;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.VertexBuffer3D;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class MeshRenderer extends Renderer 
	{
		public static const VERTEX_BUFFER_MAX_SIZE:int = 65535;
		public static const VERTEX_BUFFER_MAX_NUMBER:int = 4096;
		
		private static const FORMAT_SIZE:Object =
		{
			"float2": 2,
			"float3": 3,
			"float4": 4			
		};
		
		protected static var _maxVertexBufferSize:int = 0;
		protected var _vertexBuffers:Vector.<VertexBufferWithRegisterInfo> = new Vector.<VertexBufferWithRegisterInfo>();
		//private var _vertextBuffer:VertexBuffer3D;
		protected var _indexBuffer:IndexBuffer3D;
		
		public var mesh:Mesh;
		
		public function MeshRenderer(mesh:Mesh) 
		{
			this.mesh = mesh;
		}
		
		override public function start():void
		{
			if (material == null)
				return;
			
			if (_maxVertexBufferSize < material.shader.vertexRegisterMap.inputVertexRegisters.length)
				_maxVertexBufferSize = material.shader.vertexRegisterMap.inputVertexRegisters.length;
//			var stripSize:int = 0;
			_vertexBuffers.length = 0;
			for each (var info:VertexRegisterInfo in material.shader.vertexRegisterMap.inputVertexRegisters)
			{
				var registerSize:uint = FORMAT_SIZE[info.format];
//				stripSize += registerSize;
				var vb:VertexBuffer3D = context3D.createVertexBuffer(mesh.vertices.length, registerSize);
				_vertexBuffers.push(new VertexBufferWithRegisterInfo(vb, info));
				var vertexData:Vector.<Number> = new Vector.<Number>();
				if (info.semantics.id == "PB3D_POSITION")
				{
					for each(var pos:Vector3D in mesh.vertices)
						vertexData.push(pos.x, pos.y, pos.z);
				}
				else if (info.semantics.id == "PB3D_UV")
				{
					for each(var uv:Point in mesh.textureCoords)
						vertexData.push(uv.x, uv.y);
				}
				else if (info.semantics.id == "PB3D_NORMAL")
				{
					for each(var normal:Vector3D in mesh.normals)
						vertexData.push(normal.x, normal.y, normal.z);
				}
				else
				{
					ASSERT(false, "Unsupported semantics: " + info.semantics.id);
				}
				vb.uploadFromVector(vertexData, 0, mesh.vertices.length);
			}
//			_vertextBuffer = context3D.createVertexBuffer(mesh.vertices.length, stripSize);
//			_vertextBuffer.uploadFromVector(mesh.getVertexVectorData(), 0, mesh.vertices.length);
			
			_indexBuffer = context3D.createIndexBuffer(mesh.vertexIndices.length);
			_indexBuffer.uploadFromVector(mesh.vertexIndices, 0, mesh.vertexIndices.length);
		}
		
		protected function setParameters():void
		{
			material.shader.setMatrix("modelMatrix", transform.getLocalToWorldMatrix());
		}
		
		override public function render():void
		{
			if (material == null)
				return;
				
			setParameters();
			material.update();

			for (var index:int = 0; index < _maxVertexBufferSize; index++)
			{
				if (index < _vertexBuffers.length)
				{
					context3D.setVertexBufferAt(index, _vertexBuffers[index].vertexBuffer, 0, _vertexBuffers[index].registerInfo.format);
				}
				else
				{
					context3D.setVertexBufferAt(index, null);
				}
			}
			
			//context3D.setVertexBufferAt(0, _vertextBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
			//context3D.setVertexBufferAt(1, _vertextBuffer, 3, Context3DVertexBufferFormat.FLOAT_2);
			
			//if (_ii++ == 0)
			//{
			//var matrix:Matrix3D = gameObject.getWorldMatrix();
			//trace(MathUtil.matrixToString(matrix));
			//matrix.append(camera.viewMatrix);
			//trace(MathUtil.matrixToString(matrix));
			//matrix.append(camera.projectionMatrix);
			//trace(MathUtil.matrixToString(matrix));
			//}
			
			context3D.drawTriangles(_indexBuffer);
		}
		
		private var _ii:int = 0;
	}

}
import com.adobe.pixelBender3D.VertexRegisterInfo;
import flash.display3D.VertexBuffer3D;

class VertexBufferWithRegisterInfo
{
	public var vertexBuffer:VertexBuffer3D;
	public var registerInfo:VertexRegisterInfo;
	
	public function VertexBufferWithRegisterInfo(vertexBuffer:VertexBuffer3D, registerInfo:VertexRegisterInfo)
	{
		this.vertexBuffer = vertexBuffer;
		this.registerInfo = registerInfo;
	}
}