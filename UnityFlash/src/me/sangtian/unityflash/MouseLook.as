package me.sangtian.unityflash 
{
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import me.sangtian.common.Input;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author 
	 */
	public class MouseLook extends Behavior 
	{
		public var sensitivityX:Number = 0.5;
		public var sensitivityY:Number = 0.5;
		public var minimumY:Number = -90;
		public var maximumY:Number = 90;
		
		private var _rotateMatrix:Matrix3D = new Matrix3D();
		private var _pitch:Number = 0;
		
		override public function update():void
		{
			transform.rotateAround( -Input.mouseDelta.x * sensitivityX, Vector3D.Y_AXIS);
			// To avoid Gimbal lock we can't rotate around X axis directly
			//transform.rotateAround( Input.mouseDelta.y * sensitivityY, Vector3D.X_AXIS);
			var deltaPitch:Number = Input.mouseDelta.y * sensitivityY * MathUtil.sign(-transform.forward.z);
			_pitch += deltaPitch;
			if (_pitch < minimumY)
			{
				deltaPitch = 0;
				_pitch = minimumY;
			}
			else if (_pitch > maximumY)
			{
				deltaPitch = 0;
				_pitch = maximumY;
			}
			_rotateMatrix.identity();
			_rotateMatrix.appendRotation(deltaPitch, Vector3D.X_AXIS);
			transform.lookAtDirection(_rotateMatrix.deltaTransformVector(transform.forward));
			
			transform.translateByVector(transform.right, Input.getAxis(Input.AXIS_HORIZONTAL) * 0.1);
			transform.translateByVector(transform.forward, Input.getAxis(Input.AXIS_VERTICAL) * 0.1);
		}
	}

}