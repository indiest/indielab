package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	/**
	 * Used to get information back from a raycast.
	 * 
	 * @author tiansang
	 */
	public class RaycastHit 
	{
		// The impact point in world space where the ray hit the collider.
		public var point:Vector3D;
		// The normal of the surface the ray hit.
		public var normal:Vector3D;
		// The barycentric coordinate of the triangle we hit.
		public var barycentricCoordinate:Vector3D;
		// The distance from the ray's origin to the impact point.
		public var distance:Number;
		// The index of the triangle that was hit. Triangle index is only valid if the collider that was hit is a MeshCollider.
		public var triangleIndex:int;
		// The Collider that was hit.
		public var collider:Collider;
	}

}