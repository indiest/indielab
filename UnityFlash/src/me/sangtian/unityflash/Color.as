package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Color 
	{
		public static const WHITE:Color = new Color(1, 1, 1);
		public static const BLACK:Color = new Color(0, 0, 0);
		public static const GREY:Color = new Color(0.5, 0.5, 0.5);
		public static const RED:Color = new Color(1, 0, 0);
		public static const GREEN:Color = new Color(0, 1, 0);
		public static const BLUE:Color = new Color(0, 0, 1);
		public static const YELLOW:Color = new Color(1, 1, 0);
		
		public var r:Number;
		public var g:Number;
		public var b:Number;
		public var a:Number;
		
		public function get R():uint
		{
			return r * 0xff;
		}
		
		public function get G():uint
		{
			return g * 0xff;
		}
		
		public function get B():uint
		{
			return g * 0xff;
		}
		
		public function get A():uint
		{
			return a * 0xff;
		}
		
		public function get ARGB():uint
		{
			return A << 24 | R << 16 | G << 8 | B;
		}
		
		public function Color(r:Number = 1, g:Number = 1, b:Number = 1, a:Number = 1) 
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}
		
		public function multiply(c:Color):void
		{
			r *= c.r;
			g *= c.g;
			b *= c.b;
			a *= c.a;
		}
		
		public function add(c:Color):void
		{
			r += c.r;
			g += c.g;
			b += c.b;
			a += c.a;
		}
		
		public function toVector3D():Vector3D 
		{
			return new Vector3D(r, g, b, a);
		}
	}

}