package me.sangtian.unityflash 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author 
	 */
	public class CubemapFace extends Enum 
	{
		
		public static const PositiveX:CubemapFace = new CubemapFace();
		public static const NegativeX:CubemapFace = new CubemapFace();
		public static const PositiveY:CubemapFace = new CubemapFace();
		public static const NegativeY:CubemapFace = new CubemapFace();
		public static const PositiveZ:CubemapFace = new CubemapFace();
		public static const NegativeZ:CubemapFace = new CubemapFace();
	}

}