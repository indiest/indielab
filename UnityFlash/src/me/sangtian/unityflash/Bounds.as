package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	import me.sangtian.common.util.MathUtil;
	/**
	 * Represents an axis-aligned bounding box.
	 * 
	 * @author tiansang
	 */
	public class Bounds 
	{
		private var _center:Vector3D;
		private var _size:Vector3D;
		private var _extents:Vector3D = new Vector3D();
		private var _min:Vector3D = new Vector3D();
		private var _max:Vector3D = new Vector3D();
		
		public function get center():Vector3D 
		{
			return _center;
		}
		
		public function get extents():Vector3D
		{
			return _extents;
		}
		
		public function get size():Vector3D
		{
			return _size;
		}
		
		public function get min():Vector3D
		{
			return _min;
		}
		
		public function get max():Vector3D
		{
			return _max;
		}
		
		public function Bounds(center:Vector3D, size:Vector3D) 
		{
			setCenterSize(center, size);
		}
		
		public function setCenterSize(center:Vector3D, size:Vector3D):void
		{
			_center = center;
			_size = size;
			_extents.setTo(size.x * 0.5, size.y * 0.5, size.z * 0.5);
			_min.setTo(_center.x - _extents.x, _center.y - _extents.y, _center.z - _extents.z);
			_max.setTo(_center.x + _extents.x, _center.y + _extents.y, _center.z + _extents.z);
		}
		
		public function contains(p:Vector3D):Boolean
		{
			return p.x >= min.x && p.y >= min.y && p.z >= min.z &&
				p.x <= max.x && p.y <= max.y && p.z <= max.z;
		}
		
		public function interects(b:Bounds):Boolean
		{
			var d:Vector3D = center.subtract(b.center);
			var ext:Vector3D = extents.add(b.extents);
			if (Math.abs(d.x) <= ext.x && Math.abs(d.y) <= ext.y && Math.abs(d.z) <= ext.z)
				return true;
			return false;
		}
		
		public function intersectRay(ray:Ray, distance:Number = -1):Boolean
		{
			// http://prideout.net/blog/?p=64
			// TODO: apply 'distance' parameter
			var invR:Vector3D = new Vector3D(1 / ray.direction.x, 1 / ray.direction.y, 1 / ray.direction.z);
			var tbot:Vector3D = MathUtil.scaleVector(invR, min.subtract(ray.origin));
			var ttop:Vector3D = MathUtil.scaleVector(invR, max.subtract(ray.origin));
			var lbot:Number = tbot.length;
			var ltop:Number = ttop.length;
			var tmin:Vector3D = lbot < ltop ? tbot : ttop;
			var tmax:Vector3D = lbot > ltop ? tbot : ttop;
			var tx:Number = tmin.x;
			var ty:Number = tmin.x;
			if (tmin.y * tmin.y + tmin.z * tmin.z > tx * tx + ty * ty)
			{
				tx = tmin.y;
				ty = tmin.z;
			}
			var t0:Number = Math.max(tx, ty);
			tx = tmax.x;
			ty = tmax.x;
			if (tmax.y * tmax.y + tmax.z * tmax.z > tx * tx + ty * ty)
			{
				tx = tmax.y;
				ty = tmax.z;
			}
			var t1:Number = Math.min(tx, ty);
			return t0 <= t1;
		}
	}

}