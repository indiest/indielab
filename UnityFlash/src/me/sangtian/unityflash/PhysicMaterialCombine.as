package me.sangtian.unityflash 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class PhysicMaterialCombine extends Enum 
	{
		//	 Averages the friction / bounce of the two colliding materials.
		public static const Average:PhysicMaterialCombine = new PhysicMaterialCombine();
		//	 Multiplies the friction/bounce of the two colliding materials.
		public static const Multiply:PhysicMaterialCombine = new PhysicMaterialCombine();
		//	 Uses the smaller friction / bounce of the two colliding materials.
		public static const Minimum:PhysicMaterialCombine = new PhysicMaterialCombine();
		//	 Uses the larger friction/bounce of the two colliding materials.
		public static const Maximum:PhysicMaterialCombine = new PhysicMaterialCombine();
	}

}