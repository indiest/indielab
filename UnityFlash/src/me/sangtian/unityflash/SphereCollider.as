package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class SphereCollider extends Collider 
	{
		private var _center:Vector3D = new Vector3D();
		private var _radius:Number = 1;
		
		public function SphereCollider() 
		{
			
		}
		
		public function get center():Vector3D 
		{
			return _center;
		}
		
		public function set center(value:Vector3D):void 
		{
			_center = value;
			//updateBounds();
		}
		
		public function get radius():Number 
		{
			return _radius;
		}
		
		public function set radius(value:Number):void 
		{
			_radius = value;
			//updateBounds();
		}
		
		override protected function updateBounds():void 
		{
			var c:Vector3D = transform.getWorldPosition().add(center);
			var s:Vector3D = new Vector3D(transform.scale.x * radius * 2, transform.scale.y * radius * 2, transform.scale.z * radius * 2);
			if (bounds == null)
				bounds = new Bounds(c, s);
			else
				bounds.setCenterSize(c, s);
		}
		
		override public function raycast(ray:Ray, distance:Number = -1, hitInfo:RaycastHit = null):Boolean 
		{
			/*
			var r:Number = getRayDistance(bounds);
			if (distance < 0)
			{
				var rayToCenter:Vector3D = bounds.center.subtract(ray.origin);
				var lenRC:Number = rayToCenter.length;
				var tanRC:Number = r / lenRC;
				var cosRC:Number = ray.direction.dotProduct(rayToCenter) / lenRC;
				return Math.acos(cosRC) <= Math.atan(tanRC);
			}
			else
			{
				var p:Vector3D = ray.getPoint(distance);
				return bounds.center.subtract(p).length <= r;
			}
			*/
			
			// TODO: for transformed collider, apply inverse matrix to the ray, and then apply the inverse matrix to the hit point back.
			
			var n:Number = bounds.center.subtract(ray.origin).dotProduct(ray.direction) / ray.direction.lengthSquared;
			if (n < 0)
				return false;
			if (distance >= 0 && distance < n)
				return false;
			var d:Number = ray.getPoint(n).subtract(bounds.center).length;
			return d <= radius;
		}
		
		override protected function checkCollision(collider:Collider):Collision 
		{
			if (collider == null || collider.bounds == null)
				return null;
			var ray:Ray = new Ray(collider.bounds.center, bounds.center.subtract(collider.bounds.center));
			if (raycast(ray, getRayDistance(collider.bounds)))
			{
				return new Collision(collider);
			}
			
			return null;
		}
		
		protected function getRayDistance(bounds:Bounds):Number
		{
			return MathUtil.geometricMean(bounds.extents);
		}
	}

}