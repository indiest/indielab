package me.sangtian.unityflash 
{
	import flash.geom.Point;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class MeshObjLoader 
	{
		private var LINES_READER:Object = 
		{
			"g": read_g,
			"v": read_v,
			"vt": read_vt,
			"vn": read_vn,
			"s": read_s,
			"usemtl": read_usemtl,
			"f": read_f
		};
		
		private var _mesh:Mesh;
		
		public function MeshObjLoader(mesh:Mesh) 
		{
			_mesh = mesh;
			_mesh.vertices = new Vector.<Vector3D>();
			_mesh.vertexIndices = new Vector.<uint>();
		}
		
		public function process(data:String):void
		{
			var lines:Array = data.split(/\r?\n/g);
			for each(var line:String in lines)
			{
				var params:Array = line.split(" ");
				var func:Function = LINES_READER[params[0]];
				if (func != null)
				{
					func(params);
				}
			}
		}
		
		private function read_g(params:Array):void
		{
			//Named objects and polygon groups are specified via the 'o' or 'g' tags.
		}
		
		private function read_v(params:Array):void
		{
			_mesh.vertices.push(new Vector3D(params[1], params[2], params[3]));
		}
		
		private function read_vt(params:Array):void
		{
			if (_mesh.textureCoords == null)
				_mesh.textureCoords = new Vector.<Point>();
			_mesh.textureCoords.push(new Point(params[1], params[2]));
		}
		
		private function read_vn(params:Array):void
		{
			if (_mesh.normals == null)
				_mesh.normals = new Vector.<Vector3D>();
			_mesh.normals.push(new Vector3D(params[1], params[2], params[3]));
		}
		
		private function read_s(params:Array):void
		{
			//Smooth shading across polygons is enabled by smoothing groups.
		}
		
		private function read_usemtl(params:Array):void
		{
			//This tag specifies the material name for the element following it. The material name matches a named material definition in an external .mtl file.
		}
		
		private function read_f(params:Array):void
		{
			for (var i:int = 1; i < params.length; i++)
			{
				var param:String = params[i];
				var indices:Array = param.split("/");
				_mesh.vertexIndices.push(uint(indices[0]) - 1);
				if (indices.length > 1)
				{
					if (_mesh.textureIndices == null)
						_mesh.textureIndices = new Vector.<uint>();
					_mesh.textureIndices.push(uint(indices[1]) - 1);
				}
				if (indices.length > 2)
				{
					if (_mesh.normalIndices == null)
						_mesh.normalIndices = new Vector.<uint>();
					_mesh.normalIndices.push(uint(indices[2]) - 1);
				}
			}
			return;
		}
		
		public static function load(data:String):Mesh
		{
			var mesh:Mesh = new Mesh();
			var loader:MeshObjLoader = new MeshObjLoader(mesh);
			loader.process(data);
			return mesh;
		}
	}

}