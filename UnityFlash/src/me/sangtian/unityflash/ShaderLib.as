package me.sangtian.unityflash 
{
	import adobe.utils.CustomActions;
	/**
	 * ...
	 * @author tiansang
	 */
	public class ShaderLib 
	{
		[Embed (source="/../res/UnlitV.pbasm", mimeType="application/octet-stream")]
		private static const unlitVertexProgram : Class;

		[Embed (source="/../res/UnlitMV.pbasm", mimeType="application/octet-stream")]
		private static const unlitMaterialVertexProgram : Class;
				
		[Embed (source="/../res/UnlitF.pbasm", mimeType="application/octet-stream")]
		private static const unlitFragmentProgram : Class;

		[Embed (source="/../res/DiffuseV.pbasm", mimeType="application/octet-stream")]
		private static const diffuseVertexProgram : Class;

		[Embed (source="/../res/DiffuseMV.pbasm", mimeType="application/octet-stream")]
		private static const diffuseMaterialVertexProgram : Class;
				
		[Embed (source="/../res/DiffuseF.pbasm", mimeType="application/octet-stream")]
		private static const diffuseFragmentProgram : Class;

		[Embed (source="/../res/VertexLitV.pbasm", mimeType="application/octet-stream")]
		private static const litVertexProgram : Class;

		[Embed (source="/../res/VertexLitMV.pbasm", mimeType="application/octet-stream")]
		private static const litMaterialVertexProgram : Class;
				
		[Embed (source="/../res/VertexLitF.pbasm", mimeType="application/octet-stream")]
		private static const litFragmentProgram : Class;

		[Embed (source="/../res/RayV.pbasm", mimeType="application/octet-stream")]
		private static const rayVertexProgram : Class;

		[Embed (source="/../res/RayMV.pbasm", mimeType="application/octet-stream")]
		private static const rayMaterialVertexProgram : Class;
				
		[Embed (source="/../res/RayF.pbasm", mimeType="application/octet-stream")]
		private static const rayFragmentProgram : Class;

		public static function createDiffuseShader():Shader
		{
			return new Shader(new diffuseVertexProgram(), new diffuseMaterialVertexProgram(), new diffuseFragmentProgram());
		}
		
		public static function createVertexLitShader():Shader
		{
			return new Shader(new litVertexProgram(), new litMaterialVertexProgram(), new litFragmentProgram());
		}

		public static function createUnlitShader():Shader
		{
			return new Shader(new unlitVertexProgram(), new unlitMaterialVertexProgram(), new unlitFragmentProgram());
		}
		
		public static function createRayShader():Shader 
		{
			return new Shader(new rayVertexProgram, new rayMaterialVertexProgram, new rayFragmentProgram);
		}
	}

}