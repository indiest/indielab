package me.sangtian.unityflash 
{
	import flash.geom.Point;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author 
	 */
	public class Primitive 
	{
		[Embed(source = "/../res/Cube.obj", mimeType="application/octet-stream")]
		private static const CubeMeshData:Class;
		
		[Embed(source = "/../res/Sphere.obj", mimeType="application/octet-stream")]
		private static const SphereMeshData:Class;

		public static function createCubeMesh():Mesh
		{
			return MeshObjLoader.load(new CubeMeshData());
		}

		public static function createSphereMesh():Mesh
		{
			return MeshObjLoader.load(new SphereMeshData());
		}
		
		public static function createFaceMesh():Mesh 
		{
			var mesh:Mesh = new Mesh();
			mesh.vertices = Vector.<Vector3D>([
				new Vector3D(0, 0, 0),
				new Vector3D(1, 0, 0),
				new Vector3D(0, 1, 0),
				new Vector3D(1, 1, 0)
			]);
			mesh.vertexIndices = Vector.<uint>([
				0, 2, 1, 2, 3, 1
			]);
			mesh.normals = Vector.<Vector3D>([
				new Vector3D(0, 0, -1),
				new Vector3D(0, 0, -1),
				new Vector3D(0, 0, -1),
				new Vector3D(0, 0, -1)
			]);
			mesh.textureCoords = Vector.<Point>([
				new Point(0, 0),
				new Point(1, 0),
				new Point(0, 1),
				new Point(1, 1)
			]);
			return mesh;
		}
		
		public static function createPlaneMesh(w:int, h:int):Mesh
		{
			var mesh:Mesh = new Mesh();
			mesh.vertices = new Vector.<Vector3D>();
			mesh.vertexIndices = new Vector.<uint>();
			mesh.normals = new Vector.<Vector3D>();
			mesh.textureCoords = new Vector.<Point>();
			
			for (var y:int = 0; y < h; y++)
			{
				for (var x:int = 0; x < w; x++)
				{
					mesh.vertices.push(new Vector3D(x, 0, y));
					if (x < w - 1 && y < h - 1)
					{
						// triangle:0->2->1
						mesh.vertexIndices.push((y + 1) * w + x, y * w + x, (y + 1) * w + 1 + x);
						// triangle:2->3->1
						mesh.vertexIndices.push(y * w + x, y * w + 1 + x, (y + 1) * w + 1 + x);
					}
					
					mesh.normals.push(new Vector3D(0, 1, 0));
					mesh.textureCoords.push(new Point(x / w, 1 - y / h));
				}
			}
			
			return mesh;
		}
		
		public static function createTriangleMesh(v0:Vector3D, v1:Vector3D, v2:Vector3D):Mesh 
		{
			var mesh:Mesh = new Mesh();
			mesh.vertices = Vector.<Vector3D>([v0, v1, v2]);
			mesh.vertexIndices = Vector.<uint>([0, 1, 2]);
			return mesh;
		}
	}

}