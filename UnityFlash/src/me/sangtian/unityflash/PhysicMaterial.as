package me.sangtian.unityflash
{
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class PhysicMaterial 
	{
		// The friction used when already moving. This value has to be between 0 and 1.
		// A value of 0 feels like ice, 1 feels like rubber.
		public var dynamicFriction:Number = 0;
		
		// The friction used when an object is lying on a surface. Usually a value from 0 to 1.
		// A value of 0 feels like ice, 1 feels like rubber.
		public var staticFriction:Number = 0;
		
		// How bouncy is the surface? A value of 0 will not bounce. A value of 1 will bounce without any loss of energy.
		public var bounciness:Number = 0;
		
		// The direction of anisotropy. Anisotropic friction is enabled if the vector is not zero.
		public var frictionDirection2:Vector3D = new Vector3D();
		
		// If anisotropic friction is enabled, dynamicFriction2 will be applied along frictionDirection2.
		public var dynamicFriction2:Number = 0;
		
		// If anisotropic friction is enabled, staticFriction2 will be applied along frictionDirection2.
		public var staticFriction2:Number = 0;
		
		// Determines how the friction is combined.
		public var frictionCombine:PhysicMaterialCombine = PhysicMaterialCombine.Average;
		
		// Determines how the bounciness is combined.
		public var bounceCombine:PhysicMaterialCombine = PhysicMaterialCombine.Average;
		
		public function PhysicMaterial() 
		{
			
		}
		
		public static function get Bouncy():PhysicMaterial
		{
			var pm:PhysicMaterial = new PhysicMaterial();
			pm.dynamicFriction = 0.3;
			pm.staticFriction = 0.3;
			pm.bounciness = 1;
			pm.frictionCombine = PhysicMaterialCombine.Average;
			pm.bounceCombine = PhysicMaterialCombine.Maximum;
			pm.frictionDirection2.setTo(0, 0, 0);
			pm.dynamicFriction2 = 0;
			pm.staticFriction2 = 0;
			return pm;
		}
		
		public static function get Ice():PhysicMaterial
		{
			var pm:PhysicMaterial = new PhysicMaterial();
			pm.dynamicFriction = 0.1;
			pm.staticFriction = 0.1;
			pm.bounciness = 0;
			pm.frictionCombine = PhysicMaterialCombine.Minimum;
			pm.bounceCombine = PhysicMaterialCombine.Minimum;
			pm.frictionDirection2.setTo(0, 0, 0);
			pm.dynamicFriction2 = 0;
			pm.staticFriction2 = 0;
			return pm;
		}
		
		public static function get Metal():PhysicMaterial
		{
			var pm:PhysicMaterial = new PhysicMaterial();
			pm.dynamicFriction = 0.25;
			pm.staticFriction = 0.25;
			pm.bounciness = 0;
			pm.frictionCombine = PhysicMaterialCombine.Average;
			pm.bounceCombine = PhysicMaterialCombine.Average;
			pm.frictionDirection2.setTo(0, 0, 0);
			pm.dynamicFriction2 = 0;
			pm.staticFriction2 = 0;
			return pm;
		}
		
		public static function get Rubber():PhysicMaterial
		{
			var pm:PhysicMaterial = new PhysicMaterial();
			pm.dynamicFriction = 1;
			pm.staticFriction = 1;
			pm.bounciness = 0;
			pm.frictionCombine = PhysicMaterialCombine.Maximum;
			pm.bounceCombine = PhysicMaterialCombine.Average;
			pm.frictionDirection2.setTo(0, 0, 0);
			pm.dynamicFriction2 = 0;
			pm.staticFriction2 = 0;
			return pm;
		}
		
		public static function get Wood():PhysicMaterial
		{
			var pm:PhysicMaterial = new PhysicMaterial();
			pm.dynamicFriction = 0.45;
			pm.staticFriction = 0.45;
			pm.bounciness = 0;
			pm.frictionCombine = PhysicMaterialCombine.Average;
			pm.bounceCombine = PhysicMaterialCombine.Average;
			pm.frictionDirection2.setTo(0, 0, 0);
			pm.dynamicFriction2 = 0;
			pm.staticFriction2 = 0;
			return pm;
		}
	}
}