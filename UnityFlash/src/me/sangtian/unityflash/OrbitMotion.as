package me.sangtian.unityflash 
{
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import me.sangtian.common.Time;
	/**
	 * ...
	 * @author tiansang
	 */
	public class OrbitMotion extends Behavior
	{
		public var target:GameObject;
		public var axis:Vector3D = Vector3D.Y_AXIS;
		public var anglesPerSecond:Number = 180;
		
		private var _rotateMatrix:Matrix3D = new Matrix3D();
		
		public function OrbitMotion() 
		{
			
		}
		
		override public function start():void
		{
			ASSERT(target != null);
		}
		
		override public function update():void
		{
			_rotateMatrix.identity();
			_rotateMatrix.appendRotation(Time.deltaSecond * anglesPerSecond, axis); 
			var v0:Vector3D = transform.getWorldPosition().subtract(target.transform.getWorldPosition());
			var v1:Vector3D = _rotateMatrix.deltaTransformVector(v0);
			transform.translateByVector(v1.subtract(v0));
		}
	}

}