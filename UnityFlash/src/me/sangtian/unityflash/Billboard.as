package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Billboard extends MeshRenderer 
	{
		
		public function Billboard() 
		{
			super(Primitive.createFaceMesh());
		}
		
		override public function update():void 
		{
			super.update();
			transform.lookAt(Camera.main.transform.getWorldPosition());
		}
	}

}