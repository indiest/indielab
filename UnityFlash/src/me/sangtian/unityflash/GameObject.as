package me.sangtian.unityflash 
{
	import flash.geom.Matrix3D;
	import flash.text.ime.CompositionAttributeRange;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import me.sangtian.common.logging.Log;
	/**
	 * ...
	 * @author tiansang
	 */
	public class GameObject 
	{
		public var name:String;
		public var tag:String;
		public var layer:int = 1;

		//private var _components:Dictionary = new Dictionary();
		private var _components:Vector.<Component> = new Vector.<Component>();
		private var _children:Vector.<GameObject> = new Vector.<GameObject>();
		private var _parent:GameObject;
		
		// references to common-used components (possibly null)
		private var _transform:Transform;
		private var _camera:Camera;
		private var _renderer:Renderer;
		private var _collider:Collider;
		private var _rigidbody:Rigidbody;
		
		public function get parent():GameObject
		{
			return _parent;
		}
		
		public function get transform():Transform
		{
			return _transform;
		}
		
		public function get camera():Camera
		{
			return _camera;
		}
		
		public function get renderer():Renderer
		{
			return _renderer;
		}
		
		public function get collider():Collider 
		{
			return _collider;
		}
		
		public function get rigidbody():Rigidbody 
		{
			return _rigidbody;
		}
		
		public function GameObject(name:String) 
		{
			this.name = name;
			_transform = new Transform();
			addComponent(_transform);
		}
		
		public function addComponent(comp:Component):Component
		{
			comp._gameObject = this;
			//var type:Class = getDefinitionByName(getQualifiedClassName(comp)) as Class;
			//_components[type] = comp;
			_components.push(comp);
			
			if (comp is Camera)
			{
				_camera = comp as Camera;
			}
			else if (comp is Renderer)
			{
				_renderer = comp as Renderer;
			}
			else if (comp is Collider)
			{
				_collider = comp as Collider;
			}
			else if (comp is Rigidbody)
			{
				_rigidbody = comp as Rigidbody;
			}
			
			comp.onAddedToGameObject();
			return comp;
		}
		
		public function getComponent(type:Class):Component
		{
			//return _components[type];
			for each(var comp:Component in _components)
			{
				if (comp is type)
				{
					return comp;
				}
			}
			return null;
		}
		
		public function addChild(child:GameObject):void
		{
			_children.push(child);
			child._parent = this;
		}
		
		public function getChildByIndex(index:uint):GameObject
		{
			return _children[index];
		}
		
		public function findChildByName(name:String):GameObject
		{
			for each(var child:GameObject in _children)
			{
				if (child.name == name)
					return child;
			}
			return null;
		}
		
		// Calls the method named methodName on every Component in this game object.
		// TODO: optimize with observer pattern
		public function sendMessage(methodName:String, parameter:Object = null, options:SendMessageOptions = null/*SendMessageOptions.RequireReceiver*/):void
		{
			var hasReceiver:Boolean = false;
			for each(var comp:Component in _components)
			{
				if (comp.hasOwnProperty(methodName))
				{
					var listener:Function = comp[methodName];
					if (listener != null)
					{
						if (listener.length == 0 && parameter == null)
						{
							listener();
						}
						else if (listener.length == 1)
						{
							listener(parameter);
						}
						else
						{
							ASSERT(false, "Parameter does not match target method: " + methodName);
						}
						hasReceiver = true;
					}
				}
			}
			
			if (options == SendMessageOptions.RequireReceiver && !hasReceiver)
			{
				Log.error("Cannot find receiver of message:", methodName);
			}
		}
		
		// Calls the method named methodName on every Component in this game object or any of its children.
		public function broadcastMessage(methodName:String, parameter:Object = null, options:SendMessageOptions = null/*SendMessageOptions.RequireReceiver*/):void
		{
			for each(var child:GameObject in _children)
			{
				child.sendMessage(methodName, parameter, options);
			}
		}
		
		public function start():void
		{
			for each(var comp:Component in _components)
			{
				comp.start();
			}
			
			for each(var child:GameObject in _children)
			{
				child.start();
			}
		}
		
		public function update():void
		{
			for each(var comp:Component in _components)
			{
				if (comp is Behavior && (comp as Behavior).enabled == false)
				{
					continue;
				}
				comp.update();
			}
			
			for each(var child:GameObject in _children)
			{
				child.update();
			}
		}
		
		public function render():void
		{
			for each(var comp:Component in _components)
			{
				if (comp is Behavior && (comp as Behavior).enabled == false)
				{
					continue;
				}
				comp.render();
			}
			
			for each(var child:GameObject in _children)
			{
				child.render();
			}
		}
		
		public function toString():String
		{
			return name;
		}
	}

}