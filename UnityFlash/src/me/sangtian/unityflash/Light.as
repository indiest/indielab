package me.sangtian.unityflash 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class Light extends Behavior 
	{
		public static const MAX_LIGHT_NUMBER:uint = 3;
		internal static var allLights:Vector.<Light> = new Vector.<Light>();
		override internal function onAddedToGameObject():void 
		{
			ASSERT(allLights.length < MAX_LIGHT_NUMBER, "Only " + MAX_LIGHT_NUMBER + "lights are supported");
			_lightIndex = allLights.length;
			allLights.push(this);
		}
		
		private var _lightIndex:uint;
		
		public var lightType:LightType = LightType.Point;
		public var color:Color = Color.WHITE;
		public var intensity:Number = 1;
		
		// for point/spot light only
		public var range:Number = 10;
		// for spot light only
		public var spotAngle:Number = 60;
		
		public function Light() 
		{
			
		}
		
		override public function update():void 
		{
			Shader.setGlobalParameter("lightTypes" + _lightIndex, lightType.index);
			Shader.setGlobalParameter("lightPositions" + _lightIndex, transform.getWorldPosition());
			Shader.setGlobalParameter("lightDirections" + _lightIndex, transform.forward);
			Shader.setGlobalParameter("lightColors" + _lightIndex, color);
			Shader.setGlobalParameter("lightIntensity" + _lightIndex, intensity);
			Shader.setGlobalParameter("lightRange" + _lightIndex, range);
			Shader.setGlobalParameter("spotAngle" + _lightIndex, spotAngle);
		}
	}

}