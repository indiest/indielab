package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	/**
	 * Describes collision. 
	 * Collision information is passed to Collider.OnCollisionEnter, Collider.OnCollisionStay and Collider.OnCollisionExit events. See Also: ContactPoint.
	 * 
	 * @author tiansang
	 */
	public class Collision 
	{
		// The Collider we hit
		public var collider:Collider;
		// The contact points generated by the physics engine.
		public var contacts:Vector.<Vector3D>;
		
		public function Collision(collider:Collider)
		{
			this.collider = collider;
		}
	}

}