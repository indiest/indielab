package me.sangtian.unityflash 
{
	import away3d.materials.utils.MipmapGenerator;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display3D.textures.CubeTexture;
	import me.sangtian.common.Enum;
	/**
	 * ...
	 * @author 
	 */
	public class Cubemap extends Texture 
	{
		public function Cubemap(size:int, format:TextureFormat, mipLevel:uint = 0) 
		{
			_width = _height = size;
			_flashTexture = Game.context3D.createCubeTexture(size, format.value, false);
			_mipLevel = mipLevel;
		}
		
		public function uploadFromBitmapData(face:CubemapFace, bitmapData:BitmapData):void
		{
			//(_flashTexture as CubeTexture).uploadFromBitmapData(bitmapData, face.index, _mipLevel);
			// MipMap bitmaps have to be generated for CubeTexture
			MipmapGenerator.generateMipMaps(bitmapData, _flashTexture, null, false, face.index);
		}
		
		public static function fromBitmaps(bitmaps:Vector.<Bitmap>):Cubemap
		{
			ASSERT(bitmaps.length == 6);
			var size:int = -1;
			var cubemap:Cubemap = null;
			for (var i:int = 0; i < bitmaps.length; i++)
			{
				if (size < 0)
				{
					size = bitmaps[i].width;
					cubemap = new Cubemap(size, TextureFormat.BGRA32);
				}
				ASSERT(size == bitmaps[i].width);
				ASSERT(bitmaps[i].width == bitmaps[i].height);
				cubemap.uploadFromBitmapData(Enum.fromIndex(CubemapFace, i) as CubemapFace, bitmaps[i].bitmapData);
			}
			return cubemap;
		}
		
		public static function fromRenderTexure(renderTexture:RenderTexture):Cubemap
		{
			ASSERT(renderTexture.isCubemap);
			var cubemap:Cubemap = new Cubemap(renderTexture.width, renderTexture.format, renderTexture.antiAlias);
			cubemap._flashTexture = renderTexture.flashTexture;
			return cubemap;
		}
	}

}