package me.sangtian.unityflash 
{
	import flash.display3D.textures.CubeTexture;
	import flash.geom.Matrix3D;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	
	/**
	 * ...
	 * @author 
	 */
	public class Camera extends Behavior
	{
		public var antiAlias:int = 0;
		private var _bgColor:uint = 0xff000000;
		
		private var _canvasWidth:Number;
		private var _canvasHeight:Number;
		private var _projectionType:ProjectionType = ProjectionType.PERSPECTIVE;
		private var _projectionMatrix:Matrix3D = new Matrix3D();
		private var _nearClipping:Number = 0.1;
		private var _farClipping:Number = 1000;
		private var _zoom:Number = 1;
		private var _aspectRatio:Number;
		
		// renderToTexture
		private var _targetTexture:RenderTexture;
		
		public function get projectionType():ProjectionType 
		{
			return _projectionType;
		}
		
		public function set projectionType(value:ProjectionType):void 
		{
			_projectionType = value;
		}
		
		//private var _focalLength:Number = 200;
		//public function get focalLength():Number 
		//{
			//return _focalLength;
		//}
		//
		//public function set focalLength(value:Number):void 
		//{
			//_focalLength = value;
		//}
		
		public function get bgColor():uint 
		{
			return _bgColor;
		}
		
		public function set bgColor(value:uint):void 
		{
			_bgColor = value;
		}
		
		public function get nearClipping():Number 
		{
			return _nearClipping;
		}
		
		public function set nearClipping(value:Number):void 
		{
			_nearClipping = value;
			updateProjectionMatrix();
		}
		
		public function get farClipping():Number 
		{
			return _farClipping;
		}
		
		public function set farClipping(value:Number):void 
		{
			_farClipping = value;
			updateProjectionMatrix();
		}
		
		public function get zoom():Number 
		{
			return _zoom;
		}
		
		public function set zoom(value:Number):void 
		{
			_zoom = value;
			updateProjectionMatrix();
		}
		
		public function get aspectRatio():Number 
		{
			return _aspectRatio;
		}
		
		public function set aspectRatio(value:Number):void 
		{
			_aspectRatio = value;
		}
		
		public function get viewMatrix():Matrix3D
		{
			var viewMatrix:Matrix3D = transform.getWorldToLocalMatrix();
			viewMatrix.invert();
			return viewMatrix;
		}
		
		public function get projectionMatrix():Matrix3D 
		{
			return _projectionMatrix;
		}
		
		public function get targetTexture():RenderTexture 
		{
			return _targetTexture;
		}
		
		public function set targetTexture(value:RenderTexture):void 
		{
			_targetTexture = value;
		}
		
		public function Camera(canvasWidth:int, canvasHeight:int) 
		{
			_canvasWidth = canvasWidth;
			_canvasHeight = canvasHeight;
			_aspectRatio = canvasWidth / canvasHeight;
			
			updateProjectionMatrix();
		}
				
		private function updateProjectionMatrix():void
		{
			//var rawData:Vector.<Number> = _projectionMatrix.rawData;
			//rawData[0] = _zoom / _aspectRatio;
			//rawData[5] = _zoom;
			//rawData[10] = -(_farClipping + _nearClipping) / (_farClipping - _nearClipping);
			//rawData[11] = - 2 * _farClipping * _nearClipping / (_farClipping - _nearClipping);
			//rawData[14] = -1;
			//_projectionMatrix.rawData = rawData;
			var z:Number = (2 * _farClipping * _nearClipping) / (_nearClipping - _farClipping);
			var y:Number = 0;// z * 100;
			_projectionMatrix.rawData = Vector.<Number>(
            [
                _zoom/_aspectRatio,0,   0,          0,
                0,          _zoom,      0,          0,
                0,          0,          (_farClipping+_nearClipping)/(_nearClipping-_farClipping),  -1,
                0,          y,          z,    0
            ]);
			//Log.debug("Updated projection matrix:", _projectionMatrix.rawData);
		}
		
		override public function start():void
		{
			context3D.configureBackBuffer(_canvasWidth, _canvasHeight, antiAlias, true);
			//context3D.setScissorRectangle(new Rectangle(0, 0, _canvasWidth, _canvasHeight / 2));
		}
		
		override public function update():void
		{
		}
		
		override public function render():void
		{
			
		}
		
		private function setCameraParameters():void
		{
			Shader.setGlobalParameter("viewMatrix", viewMatrix);
			Shader.setGlobalParameter("projectionMatrix", projectionMatrix);
			Shader.setGlobalParameter("cameraPosition", transform.getWorldPosition());
		}
		
		private function renderScene():void
		{
			// TODO: object culling, etc.
			
			Scene.root.render();
		}
		
		public static function renderAll():void 
		{
			for each(var camera:Camera in allCameras)
			{
				if (!camera.enabled)
					continue;
					
				_current = camera;
				camera.setCameraParameters();
			
				if (camera._targetTexture == null)
				{
					camera.context3D.setRenderToBackBuffer();
				}
				else
				{
					if (camera._targetTexture.isCubemap)
					{
						camera.renderToCubemap(camera.targetTexture);
					}
					else
					{
						camera.renderToTexture(camera.targetTexture, camera.targetTexture.depth, camera.targetTexture.antiAlias);			
					}
				}
				
				camera.renderScene();
			}
		}
		
		private static var _main:Camera;
		private static var _current:Camera;
		private static var _allCameras:Vector.<Camera> = new Vector.<Camera>();
		public static function get main():Camera
		{
			return _main;
		}
		public static function get current():Camera
		{
			return _current;
		}
		public static function get allCameras():Vector.<Camera>
		{
			return _allCameras;
		}
		
		override internal function onAddedToGameObject():void 
		{
			if (_main == null || gameObject.tag == Tags.MAIN_CAMERA)
			{
				_main = this;
			}
			allCameras.push(this);
		}
		
		public function renderToTexture(texture:Texture, depth:Boolean = false, antiAlias:int = 0):void
		{
			context3D.setRenderToTexture(texture.flashTexture, depth, antiAlias);			
		}
		
		public function renderToCubemap(texture:Texture, depth:Boolean = false, antiAlias:int = 0):void 
		{
			ASSERT(texture.flashTexture is CubeTexture);
			
			context3D.setRenderToTexture(texture.flashTexture, depth, antiAlias, 0);
			transform.lookAtDirection(new Vector3D(-1, 0, 0));
			renderScene();
		 
			context3D.setRenderToTexture(texture.flashTexture, depth, antiAlias, 1);
			transform.lookAtDirection(new Vector3D(1, 0, 0));
			renderScene();
		 
			context3D.setRenderToTexture(texture.flashTexture, depth, antiAlias, 2);
			transform.lookAtDirection(new Vector3D(0, 1, 0.001)); //get some NaNs if z = 0 here
			renderScene();
		 
			context3D.setRenderToTexture(texture.flashTexture, depth, antiAlias, 3);
			transform.lookAtDirection(new Vector3D(0, -1, 0.001)); //get some NaNs if z = 0 here
			renderScene();
		 
			context3D.setRenderToTexture(texture.flashTexture, depth, antiAlias, 4);
			transform.lookAtDirection(new Vector3D(0, 0, 1));
			renderScene();
		 
			context3D.setRenderToTexture(texture.flashTexture, depth, antiAlias, 5);
			transform.lookAtDirection(new Vector3D(0, 0, -1));
			renderScene();
			
		}
	}

}