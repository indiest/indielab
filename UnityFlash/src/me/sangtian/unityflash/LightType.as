package me.sangtian.unityflash 
{
	import flash.utils.Dictionary;
	import me.sangtian.common.Enum;
	/**
	 * ...
	 * @author tiansang
	 */
	public class LightType extends Enum
	{
		//private static var _typeMap:Dictionary = new Dictionary();
		//
		//public function LightType(value:int) 
		//{
			//_typeMap[value] = this;
		//}
		//
		//public static function fromValue(value:int):LightType
		//{
			//return _typeMap[value];
		//}
		
		public static const Spot:LightType = new LightType();
		public static const Directional:LightType = new LightType();
		public static const Point:LightType = new LightType();
		public static const Area:LightType = new LightType();
	}

}