package me.sangtian.unityflash 
{
	import me.sangtian.common.Enum;
	/**
	 * ...
	 * @author 
	 */
	public class FilterMode extends Enum
	{
		
		public static const Point:FilterMode = new FilterMode();
		public static const Bilinear:FilterMode = new FilterMode();
		public static const Trilinear:FilterMode = new FilterMode();
	}

}