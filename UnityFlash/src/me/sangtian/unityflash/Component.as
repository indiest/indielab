package me.sangtian.unityflash 
{
	import flash.display3D.Context3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Component 
	{
		internal var _gameObject:GameObject;
		
		public function get gameObject():GameObject
		{
			return _gameObject;
		}
		
		public function get transform():Transform
		{
			return gameObject.transform;
		}
		
		public function get camera():Camera
		{
			return gameObject.camera;
		}
		public function get renderer():Renderer
		{
			return gameObject.renderer;
		}
		
		public function get collider():Collider 
		{
			return gameObject.collider;
		}
		
		public function get rigidbody():Rigidbody 
		{
			return gameObject.rigidbody;
		}
		
		internal function get context3D():Context3D
		{
			return Game.context3D;
		}
		
		public function Component()
		{
			
		}
		
		internal function onAddedToGameObject():void 
		{
			
		}
		
		public function sendMessage(methodName:String, parameter:Object = null, options:SendMessageOptions = null/*SendMessageOptions.RequireReceiver*/):void
		{
			gameObject.sendMessage(methodName, parameter, options);
		}
		
		public function broadcastMessage(methodName:String, parameter:Object = null, options:SendMessageOptions = null/*SendMessageOptions.RequireReceiver*/):void
		{
			gameObject.broadcastMessage(methodName, parameter, options);
		}
		
		// per-session
		public function start():void
		{
			
		}
		
		// per-frame
		public function update():void
		{
			
		}
		
		// 0-n times per-frame
		public function render():void
		{
			
		}
	}

}