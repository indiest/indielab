package me.sangtian.unityflash 
{
	import flash.display.Bitmap;
	import flash.display.Stage;
	import flash.display.Stage3D;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DCompareMode;
	import flash.display3D.Context3DRenderMode;
	import flash.display3D.Context3DTriangleFace;
	import flash.events.Event;
	import flash.geom.Vector3D;
	import me.sangtian.common.Input;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.Time;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Game 
	{
		[Embed( source = "/../res/hxlogo.png" )]
		protected static const hxlogoBitmap:Class;

		[Embed( source = "/../res/skybox/space_posX.jpg" )]
		protected static const skyboxBitmap0:Class;

		[Embed( source = "/../res/skybox/space_negX.jpg" )]
		protected static const skyboxBitmap1:Class;

		[Embed( source = "/../res/skybox/space_posY.jpg" )]
		protected static const skyboxBitmap2:Class;

		[Embed( source = "/../res/skybox/space_negY.jpg" )]
		protected static const skyboxBitmap3:Class;

		[Embed( source = "/../res/skybox/space_posZ.jpg" )]
		protected static const skyboxBitmap4:Class;

		[Embed( source = "/../res/skybox/space_negZ.jpg" )]
		protected static const skyboxBitmap5:Class;

		[Embed( source = "/../res/flare.jpg" )]
		protected static const flareBitmap:Class;
		
		[Embed(source="/../res/terrain.raw", mimeType="application/octet-stream")]
		protected static const terrainRawData:Class;

		private static var _stage:Stage;
		private static var _context3D:Context3D;
		
		static public function get stage():Stage 
		{
			return _stage;
		}
		
		public static function get context3D():Context3D 
		{
			return _context3D;
		}
		
		public function Game(stage:Stage) 
		{
			_stage = stage;
			var stage3D:Stage3D = stage.stage3Ds[0];
			stage3D.x = 0;
			stage3D.y = 0;
			stage3D.addEventListener( Event.CONTEXT3D_CREATE, stageNotificationHandler );
			stage3D.requestContext3D( Context3DRenderMode.AUTO);
		}
		
		private function stageNotificationHandler(e:Event):void 
		{
			var stage3D:Stage3D = e.currentTarget as Stage3D;
			if (stage3D == null)
			{
				Log.error("Stage3D is not supported!");
				return;
			}
			
			_context3D = stage3D.context3D;
			setupContext();
			initScene();
			initInput();
			start();
		}
		
		private function initInput():void 
		{
			Input.initialize(stage);
		}
		
		protected function initScene():void 
		{
			//var reflectionObj:GameObject = new GameObject("Reflection");
			//var reflectionCam:Camera = new Camera(128, 128);
			//reflectionCam.targetTexture = new RenderTexture(128, 128, true, RenderTextureFormat.Default);
			//reflectionObj.addComponent(reflectionCam);

			var camObj:GameObject = new GameObject("Main Camera");
			camObj.tag = Tags.MAIN_CAMERA;
			camObj.addComponent(new Camera(stage.stageWidth, stage.stageHeight));
			camObj.transform.translate(0, 1, -10);
			//camObj.transform.lookAt(new Vector3D(0, 0, 1));
			camObj.transform.lookAtDirection(new Vector3D(0, 0, -1));
			Scene.root.addChild(camObj);
			Scene.mainCamera = camObj.getComponent(Camera) as Camera;
			camObj.addComponent(new MouseLook());
			var skybox:Skybox = new Skybox(Cubemap.fromBitmaps(
				Vector.<Bitmap>([
					new skyboxBitmap0(), 
					new skyboxBitmap1(), 
					new skyboxBitmap2(), 
					new skyboxBitmap3(), 
					new skyboxBitmap4(), 
					new skyboxBitmap5()
				])
			));
			camObj.addComponent(new RayRenderer());
			//camObj.addComponent(skybox);
			var camCollider:SphereCollider = new SphereCollider();
			camCollider.radius = 0.1;
			camObj.addComponent(camCollider);
			
			/*
			var skyboxCube:GameObject = new GameObject("Skybox Cube");
			skyboxCube.addComponent(skybox);
			Scene.root.addChild(skyboxCube);
			*/
			
			var sphereObj:GameObject = createModel("Sphere", Primitive.createSphereMesh(), null,//new flowersBitmap(),
					ShaderLib.createDiffuseShader());
			// the "defaultValue" metadata doesn't work in PB3D
			sphereObj.renderer.material.color = Color.WHITE;
			sphereObj.transform.translate(3, 0, 0);
			sphereObj.addComponent(new SphereCollider());
			
			var cubeObj:GameObject = createModel("Cube", Primitive.createCubeMesh(), new hxlogoBitmap(),
					ShaderLib.createDiffuseShader());
			cubeObj.transform.translate( -3, 0, 0);
			//cubeObj.renderer.enabled = false;
			//var reflection:Skybox = new Skybox(Cubemap.fromRenderTexure(reflectionCam.targetTexture));
			//cubeObj.addComponent(reflection);
			
			var terrainObj:GameObject = new GameObject("Terrain");
			terrainObj.addComponent(new Terrain(TerrainData.fromRawData(new terrainRawData)));
			terrainObj.transform.translate(0, -2, -5);
			Scene.root.addChild(terrainObj);
			
			var pointLightObj:GameObject = new GameObject("Point Light");
			var pointLight:Light = new Light();
			pointLight.intensity = 2;
			pointLight.range = 3;
			pointLightObj.addComponent(pointLight);
			var orbitMotion:OrbitMotion = new OrbitMotion();
			orbitMotion.target = sphereObj;
			pointLightObj.addComponent(orbitMotion);
			var flare:Billboard = new Billboard();
			flare.material = new Material(ShaderLib.createUnlitShader(), null, Texture.fromBitmapData(new flareBitmap().bitmapData));
			pointLightObj.addComponent(flare);
			pointLightObj.transform.translate(5, 0, 0);
			Scene.root.addChild(pointLightObj);
			
			var directionLightObj:GameObject = new GameObject("Direction Light");
			var dirLight:Light = new Light();
			dirLight.lightType = LightType.Directional;
			dirLight.color = Color.YELLOW;
			dirLight.intensity = 0.5;
			directionLightObj.addComponent(dirLight);
			directionLightObj.transform.translate(0, 20, 0);
			directionLightObj.transform.lookAtDirection(new Vector3D(0, 1, 0));
			Scene.root.addChild(directionLightObj);
			
			var spotLightObj:GameObject = new GameObject("Spot Light");
			var spotLight:Light = new Light();
			spotLight.lightType = LightType.Spot;
			spotLight.color = Color.RED;
			spotLight.intensity = 2;
			spotLightObj.addComponent(spotLight);
			spotLightObj.transform.translate(0, 0, 5);
			spotLightObj.transform.lookAt(new Vector3D(0, 0, 0));
			Scene.root.addChild(spotLightObj);
			
			//var ball1:GameObject = createModel("Ball1", Primitive.createSphereMesh(), null, ShaderLib.createDiffuseShader());
			//ball1.transform.translate(0, 0, -5);
			//var ball1Collider:Collider = new SphereCollider();
			//ball1Collider.material = PhysicMaterial.Bouncy;
			//ball1.addComponent(ball1Collider);
			//Scene.root.addChild(ball1);
			
			var ball2:GameObject = createModel("Ball2", Primitive.createSphereMesh(), null, ShaderLib.createDiffuseShader());
			ball2.transform.scale = new Vector3D(0.5, 0.5, 0.5);
			ball2.transform.translate(0, 5, -5);
			ball2.addComponent(new SphereCollider());
			var ball2Rigidbody:Rigidbody = new Rigidbody();
			ball2Rigidbody.addForce(new Vector3D(0, -9.8, 0), ForceMode.Force);
			ball2.addComponent(ball2Rigidbody);
			Scene.root.addChild(ball2);
			
			var box1:GameObject = createModel("Box1", Primitive.createCubeMesh(), null, ShaderLib.createDiffuseShader());
			box1.transform.translate(0, 0, -5);
			var box1Collider:BoxCollider = new BoxCollider();
			box1Collider.material = PhysicMaterial.Bouncy;
			box1.addComponent(box1Collider);
			Scene.root.addChild(box1);
		}
		
		private function createModel(name:String, mesh:Mesh, textureBitmap:Bitmap, shader:Shader):GameObject 
		{
			var model:GameObject = new GameObject(name);
			var renderer:MeshRenderer = new MeshRenderer(mesh);
			renderer.material = new Material(shader, null, textureBitmap ? Texture.fromBitmapData(textureBitmap.bitmapData) : null);
			model.addComponent(renderer);
			Scene.root.addChild(model);
			return model;
		}
		
		protected function setupContext():void 
		{
			_context3D.enableErrorChecking = true;
			_context3D.setDepthTest(true, Context3DCompareMode.LESS);
			_context3D.setCulling(Context3DTriangleFace.NONE);
		}
		
		public function start():void
		{
			Scene.root.start();
		}
		
		public function update():void 
		{
			if (_context3D == null)
				return;
			
			Time.tick();
			Input.update();

			_context3D.clear(RenderSettings.ambientLight.r, RenderSettings.ambientLight.g, RenderSettings.ambientLight.b, RenderSettings.ambientLight.a);
			//updateLighting();
			Scene.root.update();
			Camera.renderAll();
			_context3D.present();
		}
		
/*		
		private	var _lightTypes:Vector.<int> = new Vector.<int>(Light.MAX_LIGHT_NUMBER);
		private var _lightPositions:Vector.<Vector3D> = new Vector.<Vector3D>(Light.MAX_LIGHT_NUMBER);
		private var _lightDirections:Vector.<Vector3D> = new Vector.<Vector3D>(Light.MAX_LIGHT_NUMBER);
		private	var _lightColors:Vector.<Color> = new Vector.<Color>(Light.MAX_LIGHT_NUMBER);
		
		private function updateLighting():void 
		{
			Shader.setGlobalParameter("ambientLightColor", RenderSettings.ambientLight);
			
			for (var i:int = 0; i < Light.allLights.length; i++)
			{
				var light:Light = Light.allLights[i];
				if (light.enabled)
				{
					_lightTypes[i] = light.lightType.index;
					_lightPositions[i] = light.transform.getWorldPosition();
					_lightDirections[i] = light.transform.eulerAngles;
					_lightColors[i] = light.color;
				}
			}
			Shader.setGlobalParameter("lightTypes", _lightTypes);
			Shader.setGlobalParameter("lightPositions", _lightPositions);
			Shader.setGlobalParameter("lightDirections", _lightDirections);
			Shader.setGlobalParameter("lightColors", _lightColors);
			
			// TODO: these are shader's parameters
			Shader.setGlobalParameter("specularColor", Color.WHITE);
			Shader.setGlobalParameter("specularExponent", 16);
			
		}
*/		
	}

}