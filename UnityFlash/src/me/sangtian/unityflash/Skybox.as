package me.sangtian.unityflash 
{
	import com.adobe.utils.AGALMiniAssembler;
	import flash.display3D.Context3DCompareMode;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Program3D;
	import flash.geom.Matrix;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Skybox extends MeshRenderer 
	{
		[Embed (source="/../res/SkyboxV.pbasm", mimeType="application/octet-stream")]
		private static const SkyboxVertexProgram:Class;
		[Embed (source="/../res/SkyboxMV.pbasm", mimeType="application/octet-stream")]
		private static const SkyboxMaterialVertexProgram:Class;
		[Embed (source="/../res/SkyboxF.pbasm", mimeType="application/octet-stream")]
		private static const SkyboxFragmentProgram:Class;
		
		private static var _shader:Shader = new Shader(new SkyboxVertexProgram,
			new SkyboxMaterialVertexProgram, new SkyboxFragmentProgram);
		
		public function Skybox(cubemap:Cubemap) 
		{
			super(createSkyboxMesh());
			material = new Material(_shader);
			material.mainTexture = cubemap;
		}
		
		private function createSkyboxMesh():Mesh
		{
			var mesh:Mesh = new Mesh();
			mesh.vertices = Vector.<Vector3D>([
				new Vector3D(-1, 1, -1),
				new Vector3D(1, 1, -1),
				new Vector3D(1, 1, 1),
				new Vector3D(-1, 1, 1),
				new Vector3D(-1, -1, -1),
				new Vector3D(1, -1, -1),
				new Vector3D(1, -1, 1),
				new Vector3D(-1, -1, 1)
			]);
			mesh.vertexIndices = Vector.<uint>([
				0, 1, 2, 2, 3, 0,
				6, 5, 4, 4, 7, 6,
				2, 6, 7, 7, 3, 2,
				4, 5, 1, 1, 0, 4,
				4, 0, 3, 3, 7, 4,
				2, 1, 5, 5, 6, 2
			]);
			return mesh;
		}
		
		override protected function setParameters():void 
		{
			material.shader.setVector("viewPosition", transform.getWorldPosition());
			//trace("viewPosition", transform.getWorldPosition());
		}
		
		override public function render():void 
		{
			context3D.setDepthTest(false, Context3DCompareMode.LESS);
			super.render();
			
/*			
			var program:Program3D = context3D.createProgram();
			var vertexByteCode : ByteArray = new AGALMiniAssembler().assemble(Context3DProgramType.VERTEX, 
				"m44 vt7, va0, vc0		\n" +
				// fit within texture range
				"mul op, vt7, vc4\n" +
				"mov v0, va0\n"
			);
			var fragmentByteCode : ByteArray = new AGALMiniAssembler().assemble(Context3DProgramType.FRAGMENT, 
				"tex ft0, v0, fs0 <cube,linear,clamp,miplinear>	\n" +
				"mov oc, ft0"
			);
			program.upload(vertexByteCode, fragmentByteCode);
			context3D.setProgram(program);
			
			var mvp:Matrix3D = new Matrix3D();
			mvp.appendScale(1000, 1000, 1000);
			mvp.appendTranslation(camera.transform.localPosition.x, camera.transform.localPosition.y, camera.transform.localPosition.z);
			mvp.append(camera.viewMatrix);
			mvp.append(camera.projectionMatrix);
			context3D.setTextureAt(0, material.mainTexture.flashTexture);
			context3D.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX, 0, mvp, true);
			for (var index:int = 0; index < _maxVertexBufferSize; index++)
			{
				if (index < _vertexBuffers.length)
				{
					context3D.setVertexBufferAt(index, _vertexBuffers[index].vertexBuffer, 0, _vertexBuffers[index].registerInfo.format);
				}
				else
				{
					context3D.setVertexBufferAt(index, null);
				}
			}
			context3D.drawTriangles(_indexBuffer);
*/
			
			context3D.setDepthTest(true, Context3DCompareMode.LESS);
		}
	}

}