package me.sangtian.unityflash 
{
	import flash.geom.Point;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Mesh 
	{
		public var vertices:Vector.<Vector3D>;
		public var vertexIndices:Vector.<uint>;
		
		public var normals:Vector.<Vector3D>;
		public var textureCoords:Vector.<Point>;
		public var textureIndices:Vector.<uint>;
		public var normalIndices:Vector.<uint>;

		public function generateFaceNormals():void
		{
			normals = new Vector.<Vector3D>(vertices.length, true);
			for (var i:uint = 0; i < vertexIndices.length; i+=3)
			{
				var i0:uint = vertexIndices[i];
				var i1:uint = vertexIndices[i + 1];
				var i2:uint = vertexIndices[i + 2];
				var v0:Vector3D = vertices[i0];
				var v1:Vector3D = vertices[i1];
				var v2:Vector3D = vertices[i2];
				var fn:Vector3D = v1.subtract(v0).crossProduct(v2.subtract(v0));
				updateNormal(i0, fn);
				updateNormal(i1, fn);
				updateNormal(i2, fn);
			}
		}
		
		private function updateNormal(index:uint, faceNormal:Vector3D):void
		{
			if (normals[index] == null)
			{
				normals[index] = faceNormal;
			}
			else
			{
				normals[index].incrementBy(faceNormal);
				normals[index].normalize();
			}
		}
	}

}