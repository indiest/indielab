package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class RayRenderer extends MeshRenderer 
	{
		public var rayDirection:Vector3D = Vector3D.Z_AXIS;
		public var color:Color = Color.RED;
		
		public function RayRenderer() 
		{
			super(Primitive.createTriangleMesh(new Vector3D(0, -2, 0), new Vector3D(100, -2, 1000), new Vector3D(100, 2, 1000)));
			material = new Material(ShaderLib.createRayShader());
		}
		
		override protected function setParameters():void 
		{
			super.setParameters();
			material.shader.setVector("rayOrigin", transform.localPosition);
			material.shader.setVector("rayDirection", rayDirection);
			material.color = color;
		}
	}

}