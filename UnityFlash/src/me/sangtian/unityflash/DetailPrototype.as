package me.sangtian.unityflash 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class DetailPrototype 
	{
		// GameObject used by the DetailPrototype.
		public var prototype:GameObject;
		// Texture used by the DetailPrototype.
		public var prototypeTexture:Texture2D;
		
		// Render mode for the DetailPrototype.
		public var renderMode:DetailRenderMode;
	}

}