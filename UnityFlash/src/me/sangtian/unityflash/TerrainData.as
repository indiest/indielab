package me.sangtian.unityflash 
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author tiansang
	 */
	public class TerrainData 
	{
		private var _heightData:Vector.<Number>;
		private var _width:int;
		private var _height:int;
		private var _detailPrototypes:Vector.<DetailPrototype> = new Vector.<DetailPrototype>();
		
		public function get width():int
		{
			return _width;
		}
		
		public function get height():int
		{
			return _height;
		}
		
		public function get detailPrototypes():Vector.<DetailPrototype> 
		{
			return _detailPrototypes;
		}
		
		public function TerrainData(width:int, height:int) 
		{
			_width = width;
			_height = height;
			_heightData = new Vector.<Number>(width * height, true);
		}
		
		public function getHeight(x:int, y:int):Number
		{
			return _heightData[x + y * width];
		}
		
		public function setHeight(x:int, y:int, value:Number):void
		{
			_heightData[x + y * width] = value;
		}
		
		public function getHeights(x:int, y:int, w:int, h:int):Vector.<Number>
		{
			var heights:Vector.<Number> = new Vector.<Number>(w * h, true);
			for (var r:int = 0; r < h; r++)
			{
				for (var c:int = 0; c < w; c++)
				{
					heights[c + r * w] = getHeight(c + x, r + y);
				}
			}
			return heights;
		}
		
		public function setHeights(x:int, y:int, w:int, h:int, heights:Vector.<Number>):void
		{
			for (var r:int = 0; r < h; r++)
			{
				for (var c:int = 0; c < w; c++)
				{
					setHeight(c + x, r + y, heights[c + r * w]);
				}
			}
		}
		
		// Raw heightmap exported from Unity has resolution = 2^n + 1
		public static function fromRawData(ba:ByteArray):TerrainData
		{
			var resolution:int;
			var depth:int;
			if (ba.length % 2 == 0)
			{
				resolution = Math.sqrt(ba.length / 2)
				depth = 16;
			}
			else
			{
				resolution = Math.sqrt(ba.length);
				depth = 8;
			}
			var data:TerrainData = new TerrainData(resolution, resolution);
			ba.position = 0;
			var i:uint = 0;
			while (ba.bytesAvailable > 0)
			{
				data._heightData[i++] = (depth == 8 ? ba.readByte() : ba.readShort());
			}
			return data;
		}
		
		public static function fromBitmapData(bitmapData:BitmapData, zeroLevel:uint = 2147483648):TerrainData
		{
			var data:TerrainData = new TerrainData(bitmapData.width, bitmapData.height);
			for (var y:int = 0; y < bitmapData.height; y++)
			{
				for (var x:int = 0; x < bitmapData.width; x++)
				{
					data.setHeight(x, y, bitmapData.getPixel(x, y) / zeroLevel);
				}
			}
			return data;
		}
	}

}