package me.sangtian.unityflash 
{
	import flash.display.BitmapData;
	import flash.display3D.Context3DTextureFormat;
	//import flash.display3D.textures.Texture;
	import flash.display3D.textures.TextureBase;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class Texture 
	{
		protected var _flashTexture:TextureBase;
		protected var _width:int;
		protected var _height:int;
		// TODO: Implement filtering/mipmapping/repeat/dimension - see P43
		protected var _wrapMode:TextureWrapMode;
		protected var _filterMode:FilterMode;
		protected var _mipLevel:uint;
		
		public function get width():int 
		{
			return _width;
		}
		
		public function get height():int 
		{
			return _height;
		}
		
		internal function get flashTexture():TextureBase
		{
			return _flashTexture;
		}
		
		
		public static const DEFAULT:Texture = fromBitmapData(new BitmapData(1, 1, true, Color.GREY.ARGB));
		
		public static function fromBitmapData(bitmapData:BitmapData, mipLevel:int = 0):Texture
		{
			var tex:Texture = new Texture();
			bitmapData = checkBitmapSize(bitmapData);
			var flashTex:flash.display3D.textures.Texture = Game.context3D.createTexture(bitmapData.width, bitmapData.height,
				Context3DTextureFormat.BGRA, false);
			flashTex.uploadFromBitmapData(bitmapData, mipLevel);
			
			tex._flashTexture = flashTex;
			tex._width = bitmapData.width;
			tex._height = bitmapData.height;
			tex._mipLevel = mipLevel;
			return tex;
		}
		
		private static function checkBitmapSize(bitmapData:BitmapData):BitmapData 
		{
			var width:int = 1;
			while (width < bitmapData.width)
			{
				width *= 2;
			}
			var height:int = 1;
			while (height < bitmapData.height)
			{
				height *= 2;
			}
			if (width == bitmapData.width && height == bitmapData.height)
				return bitmapData;
				
			var fixedBitmapData:BitmapData = new BitmapData(width, height);
			fixedBitmapData.copyPixels(bitmapData, bitmapData.rect, new Point());
			return fixedBitmapData;
		}
	}

}