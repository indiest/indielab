package me.sangtian.unityflash 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class Scene 
	{
		private static var _root:GameObject = new GameObject("Root");
		
		public static var mainCamera:Camera;
		
		public static function get root():GameObject
		{
			return _root;
		}
		
		public function Scene() 
		{
			
		}
		
		public static function findGameObject(name:String):GameObject
		{
			return root.findChildByName(name);
		}
		
		public static function instantiate(prefab:GameObject):GameObject
		{
			return null;
		}
		
		public static function destroy(instance:GameObject):void
		{
			
		}
	}

}