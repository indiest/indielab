package me.sangtian.unityflash 
{
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class RenderTexture extends Texture 
	{
		private var _isCubemap:Boolean;
		private var _depth:Boolean;
		private var _antiAlias:int;
		private var _format:RenderTextureFormat;
		
		public function RenderTexture(width:int, height:int, isCubemap:Boolean, format:RenderTextureFormat) 
		{
			_width = width;
			_height = height;
			_isCubemap = isCubemap;
			_format = format;
			if (isCubemap)
			{
				ASSERT(width == height);
				_flashTexture = Game.context3D.createCubeTexture(width, format.value, true);
			}
			else
			{
				_flashTexture = Game.context3D.createTexture(width, height, format.value, true);
			}
		}
		
		public function get depth():Boolean 
		{
			return _depth;
		}
		
		public function set depth(value:Boolean):void 
		{
			_depth = value;
		}
		
		public function get isCubemap():Boolean 
		{
			return _isCubemap;
		}
		
		public function set isCubemap(value:Boolean):void 
		{
			_isCubemap = value;
		}
		
		public function get antiAlias():int 
		{
			return _antiAlias;
		}
		
		public function set antiAlias(value:int):void 
		{
			_antiAlias = value;
		}
		
		public function get format():RenderTextureFormat 
		{
			return _format;
		}
		
	}

}