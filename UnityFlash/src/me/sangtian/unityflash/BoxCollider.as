package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author tiansang
	 */
	public class BoxCollider extends Collider 
	{
		private var _center:Vector3D = new Vector3D();
		private var _size:Vector3D = new Vector3D(1, 1, 1);
		
		public function BoxCollider() 
		{
			
		}
		
		/**
		 * The center of the box, measured in the object's local space.
		 */
		public function get center():Vector3D 
		{
			return _center;
		}
		
		public function set center(value:Vector3D):void 
		{
			_center = value;
			updateBounds();
		}
		
		/**
		 * The size of the box, measured in the object's local space.
		 */
		public function get size():Vector3D 
		{
			return _size;
		}
		
		public function set size(value:Vector3D):void 
		{
			_size = value;
			updateBounds();
		}
		
		override protected function updateBounds():void 
		{
			var c:Vector3D = transform.getWorldPosition().add(center);
			var s:Vector3D = MathUtil.scaleVector(size, transform.scale);
			if (bounds == null)
				bounds = new Bounds(c, s);
			else
				bounds.setCenterSize(c, s);
		}
		
		override public function raycast(ray:Ray, distance:Number = -1, hitInfo:RaycastHit = null):Boolean 
		{
			return bounds.intersectRay(ray, distance);
		}
		
		override protected function checkCollision(collider:Collider):Collision 
		{
			if (bounds.interects(collider.bounds))
			{
				return new Collision(collider);
			}
			return null;
		}
	}

}