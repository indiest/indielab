package me.sangtian.unityflash 
{
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Ray 
	{
		public var origin:Vector3D;
		private var _direction:Vector3D;
		
		public function get direction():Vector3D 
		{
			return _direction;
		}
		
		public function set direction(value:Vector3D):void 
		{
			_direction = value.clone();
			_direction.normalize();
		}
		
		public function Ray(origin:Vector3D, direction:Vector3D) 
		{
			this.origin = origin;
			this.direction = direction;
		}
		
		public function getPoint(distance:Number):Vector3D
		{
			var v:Vector3D = direction.clone();
			v.scaleBy(distance);
			v.incrementBy(origin);
			return v;
		}
		
		public function isPointOn(p:Vector3D):Boolean
		{
			var dir:Vector3D = p.subtract(origin);
			dir.normalize();
			return dir.equals(direction);
		}
	}

}