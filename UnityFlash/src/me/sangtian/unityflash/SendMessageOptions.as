package me.sangtian.unityflash 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class SendMessageOptions extends Enum 
	{
		// A receiver is required for SendMessage. If no receiver is found, an error is printed to the console. (This is the default value.)
		public static const RequireReceiver:SendMessageOptions = new SendMessageOptions();
		// No receiver is required for SendMessage. 
		public static const DontRequireReceiver:SendMessageOptions = new SendMessageOptions();
	}

}