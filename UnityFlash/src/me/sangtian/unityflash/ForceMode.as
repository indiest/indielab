package me.sangtian.unityflash 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class ForceMode extends Enum 
	{
		// Add a continuous force to the rigidbody, using its mass.
		public static const Force:ForceMode = new ForceMode();
		// Add a continuous acceleration to the rigidbody, ignoring its mass.
		public static const Acceleration:ForceMode = new ForceMode();
		// Add an instant force impulse to the rigidbody, using its mass.
		public static const Impluse:ForceMode = new ForceMode();
		// Add an instant velocity change to the rigidbody, ignoring its mass.
		public static const VelocityChange:ForceMode = new ForceMode();
	}

}