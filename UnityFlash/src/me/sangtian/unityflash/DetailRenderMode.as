package me.sangtian.unityflash 
{
	import me.sangtian.common.Enum;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class DetailRenderMode extends Enum 
	{
		public static const GrassBillboard:DetailRenderMode = new DetailRenderMode();
		public static const VertexLit:DetailRenderMode = new DetailRenderMode();
		public static const Grass:DetailRenderMode = new DetailRenderMode();
	}

}