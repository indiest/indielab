package me.sangtian.unityflash 
{
	import com.adobe.pixelBender3D.AGALProgramPair;
	import com.adobe.pixelBender3D.ParameterRegisterInfo;
	import com.adobe.pixelBender3D.PBASMCompiler;
	import com.adobe.pixelBender3D.PBASMProgram;
	import com.adobe.pixelBender3D.RegisterMap;
	import com.adobe.pixelBender3D.utils.ProgramConstantsHelper;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Program3D;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import me.sangtian.common.logging.Log;
	/**
	 * ...
	 * @author tiansang
	 */
	public class Shader 
	{
		private var _shaderProgram:Program3D;
		private var _vertexRegisterMap:RegisterMap;
		private var _fragmentRegisterMap:RegisterMap;
		private var _parameterBufferHelper:ProgramConstantsHelper;
		
		internal function get vertexRegisterMap():RegisterMap 
		{
			return _vertexRegisterMap;
		}
		
		internal function get fragmentRegisterMap():RegisterMap 
		{
			return _fragmentRegisterMap;
		}
		
		public function Shader(vpSource:String, mvpSource:String, fpSource:String) 
		{
			var inputVertexProgram : PBASMProgram = new PBASMProgram( vpSource );

			var inputMaterialVertexProgram : PBASMProgram = new PBASMProgram( mvpSource );
			var inputFragmentProgram : PBASMProgram = new PBASMProgram( fpSource );

			var programs : AGALProgramPair = PBASMCompiler.compile( inputVertexProgram, inputMaterialVertexProgram, inputFragmentProgram );

			var agalVertexBinary : ByteArray = programs.vertexProgram.byteCode;
			var agalFragmentBinary : ByteArray = programs.fragmentProgram.byteCode;

			_vertexRegisterMap = programs.vertexProgram.registers;
			_fragmentRegisterMap = programs.fragmentProgram.registers;
			
			_shaderProgram = Game.context3D.createProgram();
			_shaderProgram.upload( agalVertexBinary, agalFragmentBinary );
			
			_parameterBufferHelper = new ProgramConstantsHelper(Game.context3D,
				_vertexRegisterMap, _fragmentRegisterMap);
		}
		
		public function setInts(propertyName:String, value:Vector.<int>):void
		{
			var programType:String = getProgramTypeOfParameter(propertyName);
			if (programType != null)
			{
				_parameterBufferHelper.setIntParameterByName(programType, propertyName, value);
			}
		}
		
		public function setNumbers(propertyName:String, value:Vector.<Number>):void
		{
			var programType:String = getProgramTypeOfParameter(propertyName);
			if (programType != null)
			{
				_parameterBufferHelper.setNumberParameterByName(programType, propertyName, value);
			}
		}
		
		public function setMatrix(propertyName:String, value:Matrix3D, transpose:Boolean = true):void
		{
			var programType:String = getProgramTypeOfParameter(propertyName);
			if (programType != null)
			{
				_parameterBufferHelper.setMatrixParameterByName(programType, propertyName, value, transpose);
			}
		}
		
		public function setTexture(propertyName:String, value:Texture):void
		{
			_parameterBufferHelper.setTextureByName(propertyName, value.flashTexture);
		}
		
		public function setVector(propertyName:String, value:Vector3D):void
		{
			setNumbers(propertyName, Vector.<Number>([value.x, value.y, value.z]));
		}
		
		public function setVectors(propertyName:String, value:Vector.<Vector3D>):void 
		{
			var numbers:Vector.<Number> = new Vector.<Number>();
			for (var i:int = 0; i < value.length; i++)
			{
				if (value[i] != null)
				{
					numbers.push(value[i].x, value[i].y, value[i].z);
				}
				else
				{
					numbers.push(0, 0, 0);
				}
			}
			setNumbers(propertyName, numbers);
		}
		
		public function setColors(propertyName:String, value:Vector.<Color>):void 
		{
			var numbers:Vector.<Number> = new Vector.<Number>();
			for (var i:int = 0; i < value.length; i++)
			{
				if (value[i] != null)
				{
					numbers.push(value[i].r, value[i].g, value[i].b, value[i].a);
				}
				else
				{
					numbers.push(0, 0, 0, 0);
				}
			}
			setNumbers(propertyName, numbers);
		}
		
		public function setColor(propertyName:String, value:Color):void
		{
			setNumbers(propertyName, Vector.<Number>([value.r, value.g, value.b, value.a]));
		}
		
		private static var _maxTextureIndex:uint;
		
		public function setTextureAt(index:uint, value:Texture):void
		{
			if (index < _fragmentRegisterMap.textureRegisters.length)
			{
				setTexture(_fragmentRegisterMap.textureRegisters[index].name, value);
				//Game.context3D.setTextureAt(_fragmentRegisterMap.textureRegisters[index].sampler, value.flashTexture);
				_maxTextureIndex = index;
			}
		}
		
		private static var _globalParameters:Dictionary = new Dictionary();
		
		public static function setGlobalParameter(name:String, value:*):void
		{
			_globalParameters[name] = value;
		}
		
		public function getProgramTypeOfParameter(name:String):String
		{
			// WTF?!
			//if (_vertexRegisterMap.getParameterRegisterByName(name) == null)
			
			var info:ParameterRegisterInfo;
			for each(info in _vertexRegisterMap.parameterRegisters)
			{
				if (info.name == name)
					return Context3DProgramType.VERTEX;
			}
			for each(info in _fragmentRegisterMap.parameterRegisters)
			{
				if (info.name == name)
					return Context3DProgramType.FRAGMENT;
			}
			return null;
		}
		
		public function update():void 
		{
			for (var name:String in _globalParameters)
			{
				if (getProgramTypeOfParameter(name) == null)
					continue;
					
				var value:* = _globalParameters[name];
				if (value is Number)
				{
					setNumbers(name, Vector.<Number>([value]));
				}
				else if (value is int)
				{
					setInts(name, Vector.<int>([value]));
				}
				else if (value is Color)
				{
					setColor(name, value);
				}
				else if (value is Vector3D)
				{
					setVector(name, value);
				}
				else if (value is Matrix3D)
				{
					setMatrix(name, value);
				}
				else if (value is Vector.<Number>)
				{
					setNumbers(name, value);
				}
				else if (value is Vector.<int>)
				{
					setInts(name, value);
				}
				else if (value is Vector.<Vector3D>)
				{
					setVectors(name, value);
				}
				else if (value is Vector.<Color>)
				{
					setColors(name, value);
				}
				else
				{
					Log.error("Unsupported global paramter type:", value);
				}
			}
			_parameterBufferHelper.update();
			Game.context3D.setProgram(_shaderProgram);
			// reset unused texture buffers
			for (var i:uint = _fragmentRegisterMap.textureRegisters.length; i <= _maxTextureIndex; i++)
			{
				Game.context3D.setTextureAt(i, null);
			}
		}
	}

}