package 
{
	import flash.display.Sprite;
	import flash.display.Stage3D;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.text.TextField;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.Time;
	import me.sangtian.unityflash.Game;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class Main extends Sprite 
	{
		private var game:Game;
		private var _statsText:TextField;
		
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			stage.frameRate = 60;
			game = new Game(stage);
			addEventListener(Event.ENTER_FRAME, update);
			_statsText = new TextField();
			_statsText.textColor = 0xffffff;
			addChild(_statsText);
		}
		
		private function update(e:Event):void 
		{
			game.update();
			_statsText.text = "FPS: " + int(Time.fps);
		}
		
	}
	
}