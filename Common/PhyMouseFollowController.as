package  
{
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyMouseFollowController extends PhyMouseController
	{
		override protected function updateTarget(target:PhyBody, e:MouseEvent):void 
		{
			target.body.GetWorldCenter().Set(e.localX / Global.RATIO, e.localX / Global.RATIO);
		}
	}

}