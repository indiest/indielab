package  
{
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyBody extends MovieClip
	{
		[Inspectable(name="unrotatable", type=Boolean, defaultValue=false)]
		public var fixedRotation:Boolean = false;
		[Inspectable(name="isBullet", type=Boolean, defaultValue=false)]
		public var isBullet:Boolean = false;
		[Inspectable(name="initVelocityX", type=Number, defaultValue=0)]
		public var initVelocityX:Number = 0;
		[Inspectable(name="initVelocityY", type=Number, defaultValue=0)]
		public var initVelocityY:Number = 0;
		[Inspectable(name="initAngularVelocity", type=Number, defaultValue=0)]
		public var initAngularVelocity:Number = 0;
		
		
		public var body:b2Body;
		public var world:PhyWorld;
		private var _destroying:Boolean = false;
		public var activated:Boolean = true;
		
		public function get destroying():Boolean 
		{
			return _destroying;
		}
		
		public function set destroying(value:Boolean):void 
		{
			_destroying = value;
		}
		
		public function PhyBody()
		{
			
		}
		
		public function createBodyDef():b2BodyDef
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.fixedRotation = fixedRotation;
			bodyDef.position.Set(x / Global.RATIO, y / Global.RATIO);
			bodyDef.angle = Util.anglesToRadians(rotation);
			bodyDef.isBullet = isBullet;
			return bodyDef;
		}

		public function step():void 
		{
			if (activated == false)
				return;
				
			stepUpdate();
			
			if (body == null)
			{
				trace("body is null!");
				return;
			}
			x = body.GetPosition().x * Global.RATIO;
			y = body.GetPosition().y * Global.RATIO;
			rotation = Util.radiansToAngles(body.GetAngle());
		}
		
		protected function stepUpdate():void 
		{
			
		}
		
		public function addedToWorld():void 
		{
			
		}
		
		public function removingFromWorld():void 
		{
			
		}
		
		override public function toString():String 
		{
			return super.toString() + " x=" + x + " y=" + y;
		}
	}
}