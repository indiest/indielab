package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.events.KeyboardEvent;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyKeyboardMotionController extends PhyKeyboardController
	{
		public static const MODE_POSITION:int = 0;
		public static const MODE_LINEAR_VELOCITY:int = 1;
		public static const MODE_FORCE:int = 2;
		public static const MODE_ANGULAR_VELOCITY = 3;
		
		[Inspectable(name="mode", type=int, defaultValue=2)]
		public var mode:int = MODE_FORCE;
		[Inspectable(name="scalarX", type=Number, defaultValue=5)]
		public var scalarX:Number = 5;
		[Inspectable(name="scalarY", type=Number, defaultValue=10)]
		public var scalarY:Number = 10;
		
		override protected function get defaultCommandKeyMapping():String 
		{
			return "UP=W,DOWN=S,LEFT=A,RIGHT=D";
		}
		
		override protected function doCommand(command:String, target:PhyBody, e:KeyboardEvent):void 
		{
			var v:b2Vec2 = new b2Vec2();
			switch(command)
			{
				case "UP":
					v.y = -scalarY;
					break;
				case "DOWN":
					v.y = scalarY;
					break;
				case "LEFT":
					v.x = -scalarX;
					break;
				case "RIGHT":
					v.x = scalarX;
					break;
				default:
					trace("unsupported command:", command);
					return;
			}
			if (mode == MODE_POSITION)
			{
				trace("position:", target.body.GetPosition().x, target.body.GetPosition().y);
				target.body.GetPosition().Add(v);
				trace("position:", target.body.GetPosition().x, target.body.GetPosition().y);
			}
			else if (mode == MODE_LINEAR_VELOCITY)
			{
				target.body.GetLinearVelocity().Add(v);
				//target.body.SetLinearVelocity(v);
			}
			else if (mode == MODE_FORCE)
			{
				target.body.ApplyForce(v, target.body.GetWorldCenter());
			}
			else if (mode == MODE_ANGULAR_VELOCITY)
			{
				target.body.SetAngularVelocity(v.x);
			}
		}
	}

}