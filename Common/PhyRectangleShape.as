package  
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Collision.Shapes.b2ShapeDef;
	import Box2D.Common.Math.b2Vec2;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyRectangleShape extends PhyShape
	{
		
		public function PhyRectangleShape() 
		{
			
		}
		
		override public function createShapeDef():b2ShapeDef 
		{
			var shapeDef:b2PolygonDef = new b2PolygonDef();
			shapeDef.density = density;
			shapeDef.friction = friction;
			shapeDef.restitution = restitution;
			shapeDef.vertexCount = 4;
			var center:b2Vec2 = new b2Vec2(x / Global.RATIO, y / Global.RATIO);
			var w:int = width;
			var h:int = height;
			if (rotation != 0)
			{
				//var s:String = "";
				//shapeDef.vertices.forEach(function(v:b2Vec2, index:int, arr:Array):void
				//{
					//s += "(" + v.x + ", " + v.y + ")";
				//});
				//trace(s);
				var r:Number = rotation;
				rotation = 0;
				w = width;
				h = height;
				rotation = r;
				//trace("rotation:", rotation);
			}
			shapeDef.SetAsOrientedBox(w / Global.RATIO / 2, h / Global.RATIO / 2,
					center, Util.anglesToRadians(rotation));
			return shapeDef;
		}
	}

}