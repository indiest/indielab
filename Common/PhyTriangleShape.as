package  
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Collision.Shapes.b2ShapeDef;
	import Box2D.Common.Math.b2Vec2;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyTriangleShape extends PhyShape
	{
		
		public function PhyTriangleShape() 
		{
			
		}
		
		override public function createShapeDef():b2ShapeDef 
		{
			var shapeDef:b2PolygonDef = new b2PolygonDef();
			shapeDef.density = density;
			shapeDef.friction = friction;
			shapeDef.restitution = restitution;
			shapeDef.vertexCount = 3;
			var center:b2Vec2 = new b2Vec2(x / Global.RATIO, y / Global.RATIO);
			var w:int = width;
			var h:int = height;
			if (rotation != 0)
			{
				var r:Number = rotation;
				rotation = 0;
				w = width;
				h = height;
				rotation = r;
				//trace("rotation:", rotation);
			}
			shapeDef.vertices[0].Set( -w / 3 / Global.RATIO, -h * 2 / 3 / Global.RATIO);
			shapeDef.vertices[1].Set( -w / 3 / Global.RATIO, h / 3 / Global.RATIO);
			shapeDef.vertices[2].Set( w * 2 / 3 / Global.RATIO, h / 3 / Global.RATIO);
			return shapeDef;
		}
	}

}