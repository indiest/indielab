package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyMouseMotionController extends PhyMouseController
	{
		[Inspectable(name="forceMode", type=Boolean, defaultValue=false)]
		public var forceMode:Boolean = false;
		[Inspectable(name="mouseDistanceRelative", type=Boolean, defaultValue=true)]
		public var mouseDistanceRelative:Boolean = true;
		[Inspectable(name="multiplier", type=Number, defaultValue=1.0)]
		public var multiplier:Number = 1.0;
		
		override protected function updateTarget(target:PhyBody, e:MouseEvent):void 
		{
			var pos:b2Vec2 = target.body.GetWorldCenter();
			var v:b2Vec2 = new b2Vec2((e.localX / Global.RATIO - pos.x), (e.localY / Global.RATIO - pos.y));
			if (!mouseDistanceRelative)
				v.Normalize();
			v.Multiply(multiplier);
			if (forceMode)
				target.body.ApplyForce(v, pos);
			else
				target.body.SetLinearVelocity(v);
		}
	}

}