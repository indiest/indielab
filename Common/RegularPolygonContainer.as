package  
{
	import Box2D.Dynamics.b2BodyDef;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class RegularPolygonContainer extends PhyBody 
	{
		[Inspectable(name="faceNum", type=int, defaultValue=4)]
		public var faceNum:int = 4;
		[Inspectable(name="radius", type=Number, defaultValue=100)]
		public var radius:Number = 100;
		[Inspectable(name="thickness", type=Number, defaultValue=1)]
		public var thickness:Number = 1;
		
		[Inspectable(name="density", type=Number, defaultValue=0.0)]
		public var density:Number = 0.0;
		[Inspectable(name="friction", type=Number, defaultValue=0.0)]
		public var friction:Number = 0.0;
		[Inspectable(name="restitution", type=Number, defaultValue=0.0)]
		public var restitution:Number = 0.0;
		[Inspectable(name="displayable", type=Boolean, defaultValue=false)]
		public var displayable:Boolean = false;
		
		protected function initFaces():void
		{
			var theta:Number = Math.PI * 2 / faceNum;
			var w:Number = 2 * radius * Math.tan(theta / 2);
			for (var i:int = 0; i < faceNum; i++)
			{
				var angle:Number = theta * i;
				var rect:PhyRectangleShape = new PhyRectangleShape();
				rect.width = w;
				rect.height = thickness;
				rect.rotation = Util.radiansToAngles(angle);
				rect.x = radius * Math.sin(angle);
				rect.y = -radius * Math.cos(angle);
				rect.density = density;
				rect.friction = friction;
				rect.restitution = restitution;
				rect.displayable = displayable;
				addChild(rect);
			}
		}
		
		override public function createBodyDef():b2BodyDef 
		{
			initFaces();
			return super.createBodyDef();
		}
	}

}