package  
{
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyBodyController extends PhyBodyComponent
	{
		
		public function registerEvents():void
		{
			
		}
		
		public function unregisterEvents():void
		{
			
		}
		
		override public function set enabled(value:Boolean):void 
		{
			if (value == _enabled)
				return;
			if (value)
				registerEvents();
			else
				unregisterEvents();
			super.enabled = value;
		}
	}

}