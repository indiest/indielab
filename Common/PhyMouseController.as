package  
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyMouseController extends PhyBodyController
	{
		//private static const ALL_EVENT_TYPES:String = new Array(
				//MouseEvent.CLICK, MouseEvent.DOUBLE_CLICK, MouseEvent.MOUSE_DOWN, MouseEvent.MOUSE_MOVE)
				//.join(",");
		//[Inspectable(name="eventType", type="List", enumeration=ALL_EVENT_TYPES)]
		[Inspectable(name="eventType", type=String, defaultValue="mouseMove")]
		public var eventType:String = MouseEvent.MOUSE_MOVE;
		[Inspectable(name="requireButtonDown", type=Boolean, defaultValue=true)]
		public var requireButtonDown:Boolean = true;
		
		override public function registerEvents():void 
		{
			//trace("registering events:", eventType, "body:", getBody());
			stage.addEventListener(eventType, handleMouseEvent);
		}
		
		override public function unregisterEvents():void 
		{
			//trace("unregistering events:", eventType, "body:", getBody());
			stage.removeEventListener(eventType, handleMouseEvent);
		}
		
		protected function handleMouseEvent(e:MouseEvent):void
		{
			if (requireButtonDown && (e.buttonDown == false))
				return;
			// 将鼠标位置从舞台坐标系转换为容器的坐标系
			var container:DisplayObject = getBody().world.container;
			e.localX = container.mouseX;
			e.localY = container.mouseY;
			updateTarget(getBody(), e);
		}
		
		protected function updateTarget(target:PhyBody, e:MouseEvent):void 
		{
			
		}
		
	}

}