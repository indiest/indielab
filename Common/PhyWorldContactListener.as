package  
{
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Dynamics.b2ContactListener;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyWorldContactListener extends b2ContactListener
	{
		private var _listeners:Array = [];
		
		public function addBodyContactListener(listener:PhyBodyContactListener):void
		{
			_listeners.push(listener);
		}
		
		public function removeBodyContactListener(listener:PhyBodyContactListener):void
		{
			_listeners.unshift(listener);
		}
		
		private function checkAllListeners(e:ContactEvent):void
		{
			for each(var listener:PhyBodyContactListener in _listeners)
			{
				listener.checkContactEvent(e);
			}
		}
		
		override public function Add(point:b2ContactPoint):void 
		{
			checkAllListeners(new ContactEvent(ContactEvent.ADD, point));
		}
		
		override public function Persist(point:b2ContactPoint):void 
		{
			checkAllListeners(new ContactEvent(ContactEvent.PERSIST, point));
		}
		
		override public function Remove(point:b2ContactPoint):void 
		{
			checkAllListeners(new ContactEvent(ContactEvent.REMOVE, point));
		}
	}
}