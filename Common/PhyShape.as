package  
{
	import Box2D.Collision.Shapes.b2ShapeDef;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.errors.IllegalOperationError;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyShape extends MovieClip
	{
		[Inspectable(name="density", type=Number, defaultValue=0.0)]
		public var density:Number = 0.0;
		[Inspectable(name="friction", type=Number, defaultValue=0.0)]
		public var friction:Number = 0.0;
		[Inspectable(name="restitution", type=Number, defaultValue=0.0)]
		public var restitution:Number = 0.0;
		[Inspectable(name="displayable", type=Boolean, defaultValue=false)]
		public var displayable:Boolean = false;
		
		public function PhyShape() 
		{
			
		}
		
		public function createShapeDef():b2ShapeDef
		{
			throw new IllegalOperationError("Not implemented!");
		}
	}

}