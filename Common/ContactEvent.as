package  
{
	import Box2D.Collision.b2ContactPoint;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ContactEvent extends Event
	{
		/// Called when a contact point is added. This includes the geometry
		/// and the forces.
		public static const ADD:String = "add";
		/// Called when a contact point persists. This includes the geometry
		/// and the forces.
		public static const PERSIST:String = "persist";
		/// Called when a contact point is removed. This includes the last
		/// computed geometry and forces.
		public static const REMOVE:String = "remove";
		
		private var _point:b2ContactPoint;
		public function get point():b2ContactPoint
		{
			return _point;
		}
		
		public function ContactEvent(type:String, point:b2ContactPoint) 
		{
			super(type);
			_point = point;
		}
		
	}

}