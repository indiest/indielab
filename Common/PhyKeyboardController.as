package  
{
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyKeyboardController extends PhyBodyController
	{
		[Inspectable(name="commandKeyMapping", type=String)]
		public var commandKeyMapping:String;
		
		protected function get defaultCommandKeyMapping():String 
		{
			return "";
		}

		// key: Keyboard keyCode, value: command as string.
		private var _mapping:Object;
		// key: command, value: isPressing as Boolean
		private var _pressingCommands:Object = { };
		
		override public function registerEvents():void 
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
		}
		
		override public function unregisterEvents():void 
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			stage.removeEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
		}
		
		private function handleKeyDown(e:KeyboardEvent):void 
		{
			if (_mapping == null)
			{
				initMapping();
			}
			var command:String = _mapping[e.keyCode];
			if (command)
			{
				_pressingCommands[command] = true;
			}
			
			for (var cmd in _pressingCommands)
			{
				//trace("press command:", cmd);
				doCommand(cmd, getBody(), e);
			}
		}
		
		private function handleKeyUp(e:KeyboardEvent):void
		{
			var command:String = _mapping[e.keyCode];
			if (command)
			{
				trace("release command:", command);
				delete _pressingCommands[command];
			}
		}
		
		private function initMapping():void 
		{
			_mapping = { };
			if (!commandKeyMapping)
			{
				commandKeyMapping = defaultCommandKeyMapping;
			}
			for each(var commandKey:String in commandKeyMapping.split(","))
			{
				if (commandKey.length == 0)
				{
					continue;
				}
				var arr:Array = commandKey.split("=");
				var command:String = arr[0] as String;
				var key:String = arr[1] as String;
				var keyCode:uint = Keyboard[key];
				if (!keyCode)
				{
					trace("undefined key:", key);
					continue;
				}
				_mapping[keyCode] = command;
			}
		}
		
		protected function doCommand(command:String, target:PhyBody, e:KeyboardEvent):void 
		{
			
		}
		
	}

}