package  
{
	import Box2D.Collision.Shapes.b2CircleDef;
	import Box2D.Collision.Shapes.b2ShapeDef;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyCircleShape extends PhyShape
	{
		
		public function PhyCircleShape() 
		{
			
		}
		
		override public function createShapeDef():b2ShapeDef 
		{
			var shapeDef:b2CircleDef = new b2CircleDef();
			shapeDef.density = density;
			shapeDef.friction = friction;
			shapeDef.restitution = restitution;
			shapeDef.localPosition.Set(x / Global.RATIO, y / Global.RATIO);
			shapeDef.radius = width / Global.RATIO / 2;
			return shapeDef;
		}
	}

}