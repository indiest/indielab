package  
{
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Dynamics.b2Body;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyBodyContactListener extends PhyBodyComponent
	{
		[Inspectable(name="nameIdentifier", type=String)]
		public var nameIdentifier:String;
		[Inspectable(name="classIdentifier", type=String)]
		public var classIdentifier:String;
		
		public function checkContactEvent(e:ContactEvent):void
		{
			var self:PhyBody = getBody();
			if (e.point.shape1.GetBody() == self.body)
			{
				checkTarget(e.point.shape2.GetBody(), e);
			}
			else if (e.point.shape2.GetBody() == self.body)
			{
				checkTarget(e.point.shape1.GetBody(), e);
			}
		}
		
		protected function checkTarget(body:b2Body, e:ContactEvent):void 
		{
			if (body.GetUserData() is PhyBody)
			{
				var target:PhyBody = body.GetUserData() as PhyBody;
				if (!Util.isStringNullOrEmpty(nameIdentifier))
				{
					if (target.name == nameIdentifier)
					{
						handleContactEvent(target, e);
					}
				}
				else if (!Util.isStringNullOrEmpty(classIdentifier))
				{
					if (getQualifiedClassName(target) == classIdentifier)
					{
						handleContactEvent(target, e);
					}
				}
				else
				{
					handleContactEvent(target, e);
				}
			}
		}
		
		protected function handleContactEvent(target:PhyBody, e:ContactEvent):void
		{
			
		}
	}

}