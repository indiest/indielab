﻿package  
{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Util
	{
		static public function anglesToRadians(angles:Number):Number
		{
			return angles * Math.PI / 180;
		}
		
		static public function radiansToAngles(radians:Number):Number 
		{
			return radians * 180 / Math.PI;
		}
		
		static public function isStringNullOrEmpty(str:String):Boolean
		{
			return str == null || str.length == 0;
		}
		
		static public function getDistance(p1:b2Vec2, p2:b2Vec2):Number
		{
			var dx:Number = p1.x - p2.x;
			var dy:Number = p1.y - p2.y;
			return Math.sqrt(dx * dx + dy * dy);
		}
		
		static public function getBodyDistance(b1:b2Body, b2:b2Body):Number
		{
			return getDistance(b1.GetPosition(), b2.GetPosition());
		}
		
		static public function sign(n:Number):int 
		{
			if (n > 0)
			{
				return 1;
			}
			else if (n < 0)
			{
				return -1;
			}
			return 0;
		}
		
		static public function getAngleInRadians(dx:Number, dy:Number):Number
		{
			return (dx == 0 ? (Math.PI / 2 * sign(dy)) : Math.atan(dy / dx));
		}
		
		static public function addVector(v1:b2Vec2, v2:b2Vec2):b2Vec2
		{
			return new b2Vec2(v1.x + v2.x, v1.y + v2.y);
		}
		
		static public function subVector(v1:b2Vec2, v2:b2Vec2):b2Vec2
		{
			return new b2Vec2(v1.x - v2.x, v1.y - v2.y);
		}
		static public function isZeroVector(v:b2Vec2):Boolean 
		{
			return (v.x != 0 && v.y != 0);
		}
		
		static public function vectorToString(v:b2Vec2):String 
		{
			return v.x + ", " + v.y;
		}
		
		static public function randomPositiveOrNagative():int 
		{
			var rnd:Number = Math.random();
			if (rnd > 0.5)
				return 1;
			else
				return -1;
		}
		
		static public function random(min:Number, max:Number):Number
		{
			return min + Math.random() * (max - min);
		}
		
		static public function getColor(r:uint, g:uint, b:uint):uint 
		{
			return r << 16 | g << 8 | b;
		}
		
		static public function addChildBefore(target:DisplayObject, child:DisplayObject, parent:DisplayObjectContainer):void
		{
			var index:int = parent.getChildIndex(target);
			if (index >= 0)
			{
				parent.addChildAt(child, index);
			}
		}
	}

}