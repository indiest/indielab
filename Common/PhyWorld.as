package  
{
	import Box2D.Collision.b2AABB;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2World;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.system.System;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyWorld extends MovieClip
	{
		[Inspectable(name="gravityX", type=Number, defaultValue=0.0)]
		public var gravityX:Number = 0.0;
		[Inspectable(name="gravityY", type=Number, defaultValue=0.0)]
		public var gravityY:Number = 0.0;
		[Inspectable(name="stepTime", type=Number, defaultValue=0.04)]
		public var stepTime:Number = 0.04;
		[Inspectable(name="doSleep", type=Boolean, defaultValue=true)]
		public var doSleep:Boolean = true;
		[Inspectable(name="useDebugDraw", type=Boolean, defaultValue=false)]
		public var useDebugDraw:Boolean = false;
		
		private var _world:b2World = null;
		private var _bodies:Array = [];
		private var _contactListener:PhyWorldContactListener;
		private var _activated:Boolean = true;
		private var _destroying:Boolean = false;
		private var _shapeLayer:ShapeLayer = new ShapeLayer();
		
		public function get bodies():Array
		{
			return _bodies;
		}
		
		public function get world():b2World
		{
			return _world;
		}
		
		public function get container():DisplayObjectContainer
		{
			return this;
		}
		
		public function get activated():Boolean {
			return _activated;
		}
		
		public function set activated(value:Boolean):void {
			_activated = value;
			if (value)
				trace("world activated.");
			else
				trace("world deactivated.");
		}
		
		public function get destroying():Boolean 
		{
			return _destroying;
		}
		
		public function set destroying(value:Boolean):void 
		{
			_destroying = value;
		}
		
		public function get shapeLayer():ShapeLayer
		{
			return _shapeLayer;
		}
		
		public function PhyWorld() 
		{
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
			container.addChild(_shapeLayer);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			if (activated == false)
				return;
				
			if (destroying)
			{
				destroy();
				return;
			}
			if (world == null)
			{
				initWorld();
			}
				
			world.Step(stepTime, 10);
			var deadBodies:Array = [];
			for each(var phyBody:PhyBody in _bodies)
			{
				if (phyBody.destroying)
				{
					deadBodies.push(phyBody); 
				}
				else
				{
					phyBody.step();
				}
			}
			for each(var deadBody:PhyBody in deadBodies)
			{
				destroyPhyBody(deadBody);
			}
		}
		
		protected function initWorld():void 
		{
			var worldSize:b2AABB = new b2AABB();
			worldSize.lowerBound.Set(-100, -100);
			worldSize.upperBound.Set(100, 100);
			_world = new b2World(worldSize, new b2Vec2(gravityX, gravityY), doSleep);
			_contactListener = new PhyWorldContactListener();
			_world.SetContactListener(_contactListener);
			
			if (useDebugDraw)
			{
				var draw:b2DebugDraw = new b2DebugDraw();
				var drawSprite:Sprite = new Sprite();
				Util.addChildBefore(_shapeLayer, drawSprite, container);
				draw.m_sprite = drawSprite;
				draw.m_drawScale = Global.RATIO;
				draw.m_drawFlags = b2DebugDraw.e_shapeBit;
				world.SetDebugDraw(draw);
			}
			
			for (var i:int = 0; i < container.numChildren; i++)
			{
				var obj:DisplayObject = container.getChildAt(i);
				//trace("container object:", obj);
				if (obj is PhyBody)
				{
					var phyBody:PhyBody = obj as PhyBody;
					addPhyBody(phyBody);
				}
			}

		}
		
		public function addPhyBody(phyBody:PhyBody):void 
		{
			if (!container.contains(phyBody))
			{
				Util.addChildBefore(_shapeLayer, phyBody, container);
			}
			phyBody.world = this;
			var body:b2Body = world.CreateBody(phyBody.createBodyDef());
			//trace("shape count:", phyBody.numChildren);
			var removeArr:Array = [];
			for (var i:int = 0; i < phyBody.numChildren; i++)
			{
				var obj:DisplayObject = phyBody.getChildAt(i);
				if (obj is PhyShape)
				{
					var phyShape:PhyShape = obj as PhyShape;
					body.CreateShape(phyShape.createShapeDef());
					if (phyShape.displayable == false)
					{
						//removeArr.push(phyShape);
						phyShape.visible = false;
					}
				}
				else if (obj is PhyBodyController)
				{
					var controller:PhyBodyController  = obj as PhyBodyController;
					if (controller.enabled)
						controller.registerEvents();
				}
				else if (obj is PhyBodyContactListener)
				{
					_contactListener.addBodyContactListener(obj as PhyBodyContactListener);
				}
			}
			body.SetMassFromShapes();
			//trace("mass:", body.GetMass());
			
			for each(var removeShape:PhyShape in removeArr)
			{
				//trace("remove un-displayable shape:", removeShape);
				phyBody.removeChild(removeShape);
			}
			
			body.GetLinearVelocity().Set(phyBody.initVelocityX, phyBody.initVelocityY);
			body.SetAngularVelocity(phyBody.initAngularVelocity);
			
			phyBody.body = body;
			body.SetUserData(phyBody);
			_bodies.push(phyBody);
			phyBody.addedToWorld();
			
			trace("added:", phyBody);
		}
		
		private function destroyPhyBody(phyBody:PhyBody):void
		{
			phyBody.removingFromWorld();
			for (var i:int = 0; i < phyBody.numChildren; i++)
			{
				var obj:DisplayObject = phyBody.getChildAt(i);
				if (obj is PhyBodyController)
				{
					(obj as PhyBodyController).unregisterEvents();
				}
				else if (obj is PhyBodyContactListener)
				{
					_contactListener.removeBodyContactListener(obj as PhyBodyContactListener);
				}
			}
			phyBody.body.SetUserData(null);
			_world.DestroyBody(phyBody.body);
			phyBody.body = null;
			phyBody.world = null;
			container.removeChild(phyBody);
			trace("destroyed:", _bodies.splice(_bodies.indexOf(phyBody), 1));
		}
		
		protected function destroy():void
		{
			while (bodies.length > 0)
			{
				destroyPhyBody(bodies[0]);
			}
			
			if (useDebugDraw)
			{
				container.removeChild(_world.m_debugDraw.m_sprite);
			}
			_world.SetContactListener(null);
			_world = null;
			
			removeEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
	}

}