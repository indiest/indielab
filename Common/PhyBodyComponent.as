package  
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyBodyComponent extends Sprite
	{
		protected var _enabled:Boolean = true;		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		[Inspectable(name="enabled", type=Boolean, defaultValue=true)]
		public function set enabled(value:Boolean):void 
		{
			_enabled = value;
		}
		
		public function getBody():PhyBody
		{
			return parent as PhyBody;
		}
		
		public function PhyComponent() 
		{
			this.visible = false;
			if (!(parent is PhyBody))
			{
				trace("parent is not PhyBody:", parent);
				return;
			}
		}
		
	}

}