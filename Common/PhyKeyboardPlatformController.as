package  
{
	import Box2D.Common.Math.b2Vec2;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PhyKeyboardPlatformController extends PhyKeyboardMotionController
	{
		
		override protected function get defaultCommandKeyMapping():String 
		{
			return "JUMP=W,LEFT=A,RIGHT=D";
		}

		override protected function doCommand(command:String, target:PhyBody, e:KeyboardEvent):void 
		{
			if (command == "JUMP")
			{
				// TODO can jump only on ground
				command = "UP";
			}
			super.doCommand(command, target, e);
		}
	}

}