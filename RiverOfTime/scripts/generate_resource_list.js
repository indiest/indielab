/**
 * This NodeJS script can generate resources.json by scanning specified directory.
 *
 * @author Nicolas
 */
 
var fs = require('fs');
var path = require('path');

var resourcesRoot = '../bin/resources';
var targetFile = '../bin/resources.json';
// assets and directories that are not included
var ignoreList = [
	'.svn'
];
// resource types to cache
var cacheTypes = {
	'images': true
}
var resourceList = [];

// recursively scan directories and put resources' path into the list
function generateResourceList(dir) {
	// filter unnecessary files
	for (var i = 0; i < ignoreList.length; i++) {
		if (dir.match(ignoreList[i]))
			return;
	}
	
	var files;
	try {
		files = fs.readdirSync(path.resolve(resourcesRoot, dir));
	} catch(err) {
		// not a directory
		var item = {
			path: dir.replace(/\\/g, '/'),
			id: path.basename(dir, path.extname(dir)),
			type: path.dirname(dir)
		};
		if (cacheTypes[item.type] != null)
			item.cache = cacheTypes[item.type];
		resourceList.push(item);
		return;
	}
	// dir is a directory
	for (var i = 0; i < files.length; i++) {
		generateResourceList(path.join(dir, files[i]));
	}
}

(function() {
	generateResourceList(".");
//	console.log(resourceList);
	try {
		// backup the class if it's existing
		fs.renameSync(targetFile, targetFile + '.bak');
		console.info('Backup to ' + targetFile + '.bak');
	} catch(err) {}
	fs.writeFileSync(targetFile, JSON.stringify(resourceList, null, '\t'));
	console.info('Resource list generated.');
})();