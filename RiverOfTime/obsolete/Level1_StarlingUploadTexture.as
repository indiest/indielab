package  
{
	import com.greensock.TweenLite;
	import com.junkbyte.console.Cc;
	import com.junkbyte.console.KeyBind;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import me.sangtian.common.console.BitmapInspector;
	import me.sangtian.common.Input;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.common.state.IState;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.space.Space;
	import nape.util.BitmapDebug;
	import nape.util.Debug;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author 
	 */
	public class Level1 extends Sprite implements IState 
	{
		private var _bg:Image;
		private var _riverBitmapData:BitmapData;
		private var _riverImage:Image;
		private var _collisionBitmapData:BitmapData;
		private var _diffBitmapData:BitmapData;
		private var _rockBitmapData:BitmapData;
		private var _rockTexture:Texture;
		private var _canoeBitmapData:BitmapData;
		private var _canoe:Image;
		private var _rockContainer:Sprite = new Sprite();
		private var _riverContainer:Sprite = new Sprite();
		
		private var _space:Space;
		private var _canoeBody:Body;
		private var _rockBodyPool:ObjectPool;
		private var _rockToBodyMapping:Dictionary = new Dictionary();
		
		private var _levelData:Vector.<LevelLineData> = new Vector.<LevelLineData>();
		private var _currentLine:int;
		private var _scrollSpeedY:uint = 4;
		private var _isTransition:Boolean;
		private var _canoeHitTestPoint:Point = new Point();
		private var _riverHitTestPoint:Point = new Point();
		
		public var scrollRect:Rectangle;
		
		public static const CANOE_MAX_SPEED_X:Number = 200;
		public static const CANOE_MAX_SPEED_Y:Number = 20;
		public static const CANOE_ACCELERATION_X:Number = 50;
		public static const ROCK_NUM:uint = 7;
		public static const CANOE_DEFAULT_Y:Number = 500;
		private var _debug:Debug;
		
		public function Level1() 
		{
			_bg = new Image(Texture.fromColor(Game.instance.stageWidth, Game.instance.stageHeight, 0xff00ccff));
			addChild(_bg);
			
			_space = new Space(Vec2.weak());
			_space.worldLinearDrag = 10;
			
			_riverBitmapData = new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight);
			_riverImage = new Image(_bg.texture);
			_riverContainer.addChild(_riverImage);
			_riverContainer.addChild(_riverImage);
			addChild(_riverContainer);
			
			_diffBitmapData = new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight, false);
			_collisionBitmapData = new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight, true, 0);
			
			_rockBitmapData = (new Assets.LEVELS_1_ROCK() as Bitmap).bitmapData;
			_rockTexture = Texture.fromBitmapData(_rockBitmapData);
			_rockBodyPool = new ObjectPool(ROCK_NUM, createRockBody);
			addChild(_rockContainer);
			
			_canoeBitmapData = (new Assets.LEVELS_1_CANOE() as Bitmap).bitmapData;
			_canoe = new Image(Texture.fromBitmapData(_canoeBitmapData));
			_canoe.pivotX = _canoe.width >> 1;
			_canoe.pivotY = _canoe.height >> 1;
			_canoeBody = PhysicsData.createBody("canoe");
			var canoeMaterial:Material = Material.wood();
			_canoeBody.setShapeMaterials(canoeMaterial);
			_canoeBody.allowRotation = false;
			_canoeBody.space = _space;
			
			//_debug = new BitmapDebug(Game.instance.stageWidth, Game.instance.stageHeight, Game.instance.stage.color, true);
			//Starling.current.nativeStage.addChild(_debug.display);
			
			Cc.bindKey(new KeyBind("p"), function():void
			{
				_isTransition = !_isTransition;
				if (_isTransition)
					Cc.visible = true;
				else
					BitmapInspector.hideAll();
			});
			
			scrollRect = new Rectangle(_bg.x, _bg.y, Game.instance.stageWidth, Game.instance.stageHeight);
		}
		
		private function createRockBody():Body
		{
			var rockBody:Body = PhysicsData.createBody("rock");
			rockBody.type = BodyType.KINEMATIC;
			var rock:Image = new Image(_rockTexture);
			rockBody.userData.display = rock;
			_rockToBodyMapping[rock] = rockBody;
			return rockBody;
		}
		
		/* INTERFACE me.sangtian.common.state.IState */
		
		public function get isTransientState():Boolean 
		{
			return false;
		}
		
		public function enter():void 
		{
			enterScene();
		}
		
		private function enterScene():void
		{
			_isTransition = true;
			_canoe.x = 180;
			_canoe.y = 700;
			addChild(_canoe);
			//Game.instance.fade(0x00ffffff, 2.0, enterCanoe);
			TweenLite.from(this, 1.0, { alpha: 0, onComplete: enterCanoe } );
			
			_currentLine = 0;
			generateLevelData();
			updateRiverImage(_riverImage, _riverBitmapData, _riverBitmapData.height);
			_currentLine = _riverImage.height;
		}
		
		private function generateLevelData():void
		{
			var riverWidthMin:Number = 120;
			var riverWidthMax:Number = 240;
			var bankWidthMin:Number = (Game.instance.stageWidth - riverWidthMax) >> 1;
			var bankWidthMax:Number = (Game.instance.stageWidth - riverWidthMin) >> 1;

			var bankRandomSpeed:Number = 0;
			var bankWidth:Number = bankWidthMin;
			var riverRandomSpeed:Number = 0;
			var riverWidth:Number = riverWidthMax;//Random.rangeNumber(riverWidthMin, riverWidthMax);
			
			_levelData.length = 0;
			for (var y:int = 0; y < 6400; y++)
			{
				if (y < 400)
				{
					_levelData.push(new LevelLineData(bankWidth, riverWidth));
					continue;
				}
				
				bankRandomSpeed += Random.rangeNumber( -3, 3);
				bankWidth += bankRandomSpeed * 0.01;
				if (bankWidth < bankWidthMin || bankWidth > bankWidthMax)
				{
					bankRandomSpeed = bankRandomSpeed * 0.1;//0;// 
					//riverWidth = Random.rangeNumber(widthMin, widthMax);
				}

				riverRandomSpeed += Random.rangeNumber( -2, 2);
				riverWidth += riverRandomSpeed * 0.01;
				if (riverWidth < riverWidthMin || riverWidth > riverWidthMax)
				{
					riverRandomSpeed = riverRandomSpeed * 0.1;//0;
				}
				
				var lineData:LevelLineData = new LevelLineData(bankWidth, riverWidth);
				if (y % 100 == 0 && riverWidth > (riverWidthMin + riverWidthMax) * 0.5)
					lineData.rockX = bankWidth + Random.rangeNumber(0, riverWidth);
				_levelData.push(lineData);
			}
		}
		
		private function updateRiverImage(image:Image, bd:BitmapData, lines:int):void 
		{
			var bankRect:Rectangle = new Rectangle(0, 0, 0, 1);
			bd.lock();
			for (var y:int = 0; y < lines; y++)
			{
				var lineData:LevelLineData = _levelData[_currentLine + y];
				bankRect.y = lines - y - 1;
				bankRect.x = 0;
				bankRect.width = lineData.bankWidth;
				bd.fillRect(bankRect, 0xff333333);
				
				bankRect.x = lineData.bankWidth;
				bankRect.width = lineData.riverWidth;
				bd.fillRect(bankRect, 0);
				
				bankRect.x = lineData.bankWidth + lineData.riverWidth;
				bankRect.width = bd.width - bankRect.x;
				bd.fillRect(bankRect, 0xff333333);
			}
			bd.unlock();
			image.texture = Texture.fromBitmapData(bd, false);
		}
		
		private function enterCanoe():void
		{
			TweenLite.to(_canoe, 3.0, { y: CANOE_DEFAULT_Y, onComplete: finishTransition } );
		}
		
		private function finishTransition():void 
		{
			_canoeBody.position.setxy(_canoe.x, _canoe.y);
			_isTransition = false;
		}
		
		public function update():void 
		{
			if (_isTransition)
				return;
			
			// TODO: Update scroll speed by different river width
			_riverBitmapData.scroll(0, _scrollSpeedY);
			updateRiverImage(_riverImage, _riverBitmapData, _scrollSpeedY);
			_currentLine += _scrollSpeedY;
			
			// Level complete
			if (_currentLine >= _levelData.length)
			{
				_isTransition = true;
				Game.instance.fade(0xffffffff, 2.0, nextLevel);
				return;
			}
			
			return;
			
			// TODO: Generate rock on the wide water by a random distance
			if (!isNaN(_levelData[_currentLine].rockX))
			{
				var rockBody:Body = _rockBodyPool.borrowObject();
				if (rockBody != null)
				{
					var rock:Image = rockBody.userData.display;
					rock.x = _levelData[_currentLine].rockX;
					rock.y = -rock.height;
					rockBody.position.setxy(rock.x, rock.y);
					rockBody.space = _space;
					_rockContainer.addChild(rock);
				}
			}
			
			_space.step(Time.deltaSecond);
			
			_canoeBody.velocity.x = MathUtil.clamp(
				_canoeBody.velocity.x + CANOE_ACCELERATION_X * Input.getAxis(Input.AXIS_HORIZONTAL),
				-CANOE_MAX_SPEED_X, CANOE_MAX_SPEED_X);
			// If the canoe is hit/rubbed behind the default line, speed it up
			_canoeBody.velocity.y = (_canoe.y > CANOE_DEFAULT_Y ? -CANOE_MAX_SPEED_Y : 0);
			_canoe.x = _canoeBody.position.x;
			_canoe.y = _canoeBody.position.y;
			if (_canoe.y > Game.instance.stageHeight)
			{
				_isTransition = true;
				Game.instance.fade(0xff000000, 1.0, restart);
				return;
			}
			
			//_debug.clear();
			_canoeHitTestPoint.setTo(_canoe.x - _canoe.pivotX, _canoe.y - _canoe.pivotY);
			_riverHitTestPoint.setTo(_riverImage0.x, _riverImage0.y);
			if (!hitTestRiver(_riverBitmapData0, _riverImage0))
			{
				_riverHitTestPoint.setTo(_riverImage1.x, _riverImage1.y);
				if (hitTestRiver(_riverBitmapData1, _riverImage1))
				{
					//_debug.draw(_space);
					//_debug.flush();
				}
			}
			/*
			if (_canoeBitmapData.hitTest(_canoeHitTestPoint, 1, _riverBitmapData0, _riverHitTestPoint0) ||
				_canoeBitmapData.hitTest(_canoeHitTestPoint, 1, _riverBitmapData1, _riverHitTestPoint1))
			{
				// Bounce in x direction
				_canoeBody.velocity.x = -_canoeBody.velocity.x * 0.5;
				// Rub in y direction
				_canoeBody.velocity.y = _canoeBody.velocity.y * 0.5;
			}
			*/
			
			for (var i:int = _rockContainer.numChildren - 1; i >= 0; i--)
			{
				rock = _rockContainer.getChildAt(i) as Image;
				rockBody = _rockToBodyMapping[rock];
				if (rockBody.position.y > _bg.y + Game.instance.stageHeight)
				{
					_rockContainer.removeChildAt(i);
					rockBody.space = null;
					_rockBodyPool.returnObject(rockBody);
				}
				else
				{
					rock.x = rockBody.position.x;
					rock.y = rockBody.position.y = rock.y + _scrollSpeedY;
				}
			}
			
			//scrollRect.y = _canoe.y - CANOE_DEFAULT_Y;
		}
		
		private function hitTestRiver(riverBitmapData:BitmapData, riverImage:Image):Boolean
		{
			if (_canoeBitmapData.hitTest(_canoeHitTestPoint, 1, riverBitmapData, _riverHitTestPoint))
			{
				_collisionBitmapData.copyPixels(riverBitmapData, riverImage.bounds, _riverHitTestPoint);
				var canoeBounds:Rectangle = _canoe.bounds;
				var canoeMatrix:Matrix = _canoe.transformationMatrix;
				_diffBitmapData.fillRect(_diffBitmapData.rect, 0);
				_diffBitmapData.copyPixels(_collisionBitmapData, canoeBounds, _canoeHitTestPoint);
				BitmapInspector.inspect(_diffBitmapData, "Copy collision");
				_diffBitmapData.draw(_canoeBitmapData, canoeMatrix, null, BlendMode.SUBTRACT);
				BitmapInspector.inspect(_diffBitmapData, "Draw substract");
				_diffBitmapData.draw(_collisionBitmapData, null, null, BlendMode.DIFFERENCE, canoeBounds, false);
				BitmapInspector.inspect(_diffBitmapData, "Draw difference");
				//_isTransition = true;
				var hitRect:Rectangle = _diffBitmapData.getColorBoundsRect(0xffffff, 0, false);
				//ASSERT(hitRect.isEmpty() == false);
				var overlapX:Number = hitRect.width * MathUtil.sign(_canoe.x - hitRect.x);
				//var overlapY:Number = hitRect.height* MathUtil.sign(_canoe.y - hitRect.y) * 0.1;
				_canoe.x += overlapX;
				//_canoe.y += overlapY;
				_canoeBody.position.x += overlapX;
				//_canoeBody.position.y += overlapY;
				//_canoeBody.applyImpulse(Vec2.weak(overlapX * 100));
				return true;
			}
			_canoeBody.debugDraw = false;
			return false;
		}
		
		private function restart():void 
		{
			for (var i:int = _rockContainer.numChildren - 1; i >= 0; i--)
			{
				var rock:Image = _rockContainer.getChildAt(i) as Image;
				var rockBody:Body = _rockToBodyMapping[rock];
				_rockContainer.removeChildAt(i);
				rockBody.space = null;
				_rockBodyPool.returnObject(rockBody);
			}
			enterScene();
		}
		
		/*
		override public function render(support:RenderSupport, parentAlpha:Number):void
		{
			support.finishQuadBatch();
	 
			Starling.context.setScissorRectangle(scrollRect);
	 
			super.render(support, parentAlpha);
			support.finishQuadBatch();
	 
			Starling.context.setScissorRectangle(null);
		}
		*/
		
		// TODO: destroy the state after completing the level
		
		public function exit():void 
		{
			
		}
		
		private function nextLevel():void
		{
			trace("Enter level 2");
		}
	}
}

class LevelLineData
{
	public var bankWidth:int;
	public var riverWidth:int;
	public var rockX:Number;
	
	public function LevelLineData(bankWidth:int, riverWidth:int, rockX:Number = NaN)
	{
		this.bankWidth = bankWidth;
		this.riverWidth = riverWidth;
		this.rockX = rockX;
	}
}