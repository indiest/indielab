// Copyright (c) 2013 Adobe Systems Inc

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package
{
	import flash.events.DataEvent;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
    import MyLib;
    import me.sangtian.MyLib.CModule;
    
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.text.TextField;
    import flash.utils.ByteArray;
    
    public class SwfEncryptor extends Sprite
    {       
        /** Constructor */
        public function SwfEncryptor()
        {
            CModule.startAsync(this);
            
            // setup the output text area
            output = new TextField();
            output.multiline = true;
            output.width = stage.stageWidth;
            output.height = stage.stageHeight;
            addChild(output);
            
            //var urlLoader:URLLoader = new URLLoader();
			//urlLoader.dataFormat = "binary";
			//urlLoader.addEventListener(Event.COMPLETE, loadComplete);
			//urlLoader.load(new URLRequest("RiverOfTime.swf"));
			
			var fr:FileReference = new FileReference();
			fr.addEventListener(Event.SELECT, selectFile);
			fr.browse();
        }
		
		private function selectFile(e:Event):void 
		{
			var fr:FileReference = e.currentTarget as FileReference;
			fr.removeEventListener(Event.SELECT, selectFile);
			fr.addEventListener(Event.COMPLETE, loadComplete);
			//fr.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, loadComplete);
			fr.load();
		}
		
		private function loadComplete(e:Event):void 
		{
			//var urlLoader:URLLoader = e.currentTarget as URLLoader;
			//urlLoader.removeEventListener(Event.COMPLETE, loadComplete);
			var fr:FileReference = e.currentTarget as FileReference;
			fr.removeEventListener(Event.COMPLETE, loadComplete);
			
			var data:ByteArray = new ByteArray();
			fr.data.readBytes(data, 0, fr.data.length);
			
			encryptData(data);

			saveFile(data);
		}
		
		private function encryptData(data:ByteArray):void 
		{
			var ptr:int = CModule.malloc(data.length);
			CModule.writeBytes(ptr, data.length, data);
			log("Checksum:", MyLib.checkSum(ptr, data.length));
			MyLib.encryptSwf(ptr, data.length, 0x013266ca);
			CModule.readBytes(ptr, data.length, data);
			log("Checksum:", MyLib.checkSum(ptr, data.length));
			CModule.free(ptr);
		}
		
		private function saveFile(data:ByteArray):void 
		{
			var fr:FileReference = new FileReference();
			fr.save(data, "Game.swf");
		}
        
        private var output:TextField;
        private function log(...params):void 
        {
            var text:String = params.join(" ")
			output.appendText(text + "\n");
            trace(text);
        }
    }
}
