package  
{
	import flash.display.Sprite;
	import me.sangtian.common.ProcedureProfiler;
	
	/**
	 * Test the performance of different ways of invoking a function.
	 * Result(100,000 times):
	 * 	Direct invoke: 43
	 * 	function() invoke: 33
	 * 	function.apply invoke: 90
	 * 	function.call invoke: 45
	 * 
	 * @author Nicolas Tian
	 */
	public class InvokeFunctionTest extends Sprite 
	{
		
		public function InvokeFunctionTest() 
		{
			const times:int = 100000;
			var i:int;
			
			ProcedureProfiler.Default.startTiming();
			for (i = 0; i < times; i++)
			{
				foo(i);
			}
			trace("Direct invoke:", ProcedureProfiler.Default.stopTiming());
			
			var func:Function = foo;
			ProcedureProfiler.Default.startTiming();
			for (i = 0; i < times; i++)
			{
				func(i);
			}
			trace("function() invoke:", ProcedureProfiler.Default.stopTiming());
			
			ProcedureProfiler.Default.startTiming();
			for (i = 0; i < times; i++)
			{
				func.apply(null, [i]);
			}
			trace("function.apply invoke:", ProcedureProfiler.Default.stopTiming());
			
			ProcedureProfiler.Default.startTiming();
			for (i = 0; i < times; i++)
			{
				func.call(null, i);
			}
			trace("function.call invoke:", ProcedureProfiler.Default.stopTiming());
		}
		
		public function foo(o:Object):void
		{
			
		}
	}

}