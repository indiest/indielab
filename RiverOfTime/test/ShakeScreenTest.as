package  
{
	import com.greensock.easing.Bounce;
	import com.greensock.easing.Expo;
	import com.greensock.easing.RoughEase;
	import com.greensock.plugins.ShakeEffectPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
    [SWF(width="800", height="600", frameRate="30", backgroundColor="0x000000")]
	public class ShakeScreenTest extends Sprite 
	{
		[Embed(source = "CMT2.PNG")]
		private static const CMT:Class;
		
		public function ShakeScreenTest() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			addChild(new CMT());
			//TweenLite.to(this, 1, {x: 20, y: 20, ease:RoughEase.create(2, 10, false, Bounce.easeInOut, "out")});
			//shakeItem(this);
			TweenPlugin.activate([ShakeEffectPlugin]);
			TweenLite.to(this, 1, { shake: { x:20, y: 20, numShakes:10 } } );
		}
		
		private function shakeItem(item:DisplayObject):void
		{
			TweenLite.to(item, 1, { repeat:2, y:item.y + (10 + Math.random() * 20), x:item.x + (1 + Math.random() * 2), delay: 0.1, ease:Expo.easeInOut } );
			TweenLite.to(item, 1, { y:item.y + (Math.random() * 0), x:item.x + (10 + Math.random() * 20), delay: 0.3, ease:Expo.easeInOut } );
		}
	}

}