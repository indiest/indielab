package  
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.net.URLRequest;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.util.DisplayObjectUtil;
	import net.hires.debug.Stats;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class WormholeAnimationTest extends Sprite 
	{
		[Embed("wormhole_360x640x108_transparent.png")]
		private static const SpriteSheetBitmap:Class;
		private var _container:Sprite = new Sprite();
		
		public function WormholeAnimationTest() 
		{
			stage.color = 0x000000;
			addChild(_container);
			addChild(new Stats());
			//var loader:Loader = new Loader();
			//_container.addChild(loader);
			//loader.load(new URLRequest("wormhole.swf"));
			var bitmap:Bitmap = new SpriteSheetBitmap();
			var ss:StarlingSpriteSheet = new StarlingSpriteSheet(bitmap.bitmapData,
				StarlingSpriteSheetData.create(360, 640, bitmap.width, bitmap.height));
			ss.playFixed();
			_container.addChild(ss);
		}
		
	}

}