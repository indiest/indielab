package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import me.sangtian.common.display.DualImageWrappingScroller;
	import me.sangtian.common.ProcedureProfiler;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class DualImageWrappingScrollerTest extends Sprite
	{
		private var _scroller:DualImageWrappingScroller;
		
		public function DualImageWrappingScrollerTest() 
		{
			var image1:Bitmap = new Assets.LEVELS_4_BG_SCROLL;
			var image2:Bitmap = new Assets.LEVELS_4_BG_SCROLL;
			image1.y = 0;
			image2.y = 700;
			_scroller = new DualImageWrappingScroller(image1, image2, new Rectangle(0, 0, stage.stageWidth, stage.stageHeight));
			addChild(image2);
			addChild(image1);
			//addChild(_scroller);
			addEventListener(Event.ENTER_FRAME, onUpdate);
			stage.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			_scroller.reset();
		}
		
		private var _profiler:ProcedureProfiler;
		private function onUpdate(e:Event):void 
		{
			if (_profiler == null)
			{
				_profiler = new ProcedureProfiler();
			}
			else
			{
			}
			_profiler.startTiming();
			_scroller.scrollY -= 7;
			_profiler.countTimes();
			trace(_profiler.getAverageMillisecond());
		}
		
	}

}