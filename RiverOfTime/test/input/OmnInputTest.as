package input 
{
	import flash.utils.describeType;
	import me.sangtian.common.input.InputAction;
	import me.sangtian.common.input.KeyboardController;
	import me.sangtian.common.input.MouseController;
	import me.sangtian.common.input.OmnInput;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class OmnInputTest extends TestBase 
	{
		[Embed(source = "config1.json", mimeType = "application/octet-stream")]
		private static const CONFIG:Class;
		
		public function OmnInputTest() 
		{
			OmnInput.registerController(new KeyboardController(stage));
			OmnInput.registerController(new MouseController(stage));
			OmnInput.loadConfig(new CONFIG());
		}
		
		override public function update(deltaSecond:Number):void 
		{
			OmnInput.update(deltaSecond);
		}
	}

}