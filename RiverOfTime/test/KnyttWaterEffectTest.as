package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import me.sangtian.common.display.DualImageWrappingScroller;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class KnyttWaterEffectTest extends Sprite 
	{
		[Embed(source = "Object20.png")]
		private static const WATERFALL:Class;
		
		private var _bitmap:Bitmap = new WATERFALL();
		
		private var _scrollers:Vector.<DualImageWrappingScroller> = new Vector.<DualImageWrappingScroller>();
		
		public function KnyttWaterEffectTest() 
		{
			for (var i:int = 0; i < 10; i++)
			{
				var bitmap1:Bitmap = new Bitmap(_bitmap.bitmapData);
				var bitmap2:Bitmap = new Bitmap(_bitmap.bitmapData);
				bitmap2.y = bitmap2.height;
				var scroller:DualImageWrappingScroller = new DualImageWrappingScroller(bitmap1, bitmap2);
				scroller.y = i * bitmap1.height;
				addChild(scroller);
				_scrollers.push(scroller);
			}
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		private function update(e:Event):void 
		{
			for each(var scroller:DualImageWrappingScroller in _scrollers)
			{
				scroller.scrollY += 1;
			}
		}
		
	}

}