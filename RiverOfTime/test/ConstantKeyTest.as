package  
{
	import flash.display.Sprite;
	import flash.ui.Keyboard;
	import me.sangtian.common.ProcedureProfiler;
	import me.sangtian.common.util.ObjectUtil;
	
	/**
	 * Benchmark for Class constant access and Object key access. Result:
		 * Class access: 53
		 * Object access: 45
	 * @author Nicolas Tian
	 */
	public class ConstantKeyTest extends Sprite 
	{
		
		public function ConstantKeyTest() 
		{
			var constants:Vector.<String> = ObjectUtil.getConstantNames(Keyboard);
			var constantsObj:Object = ObjectUtil.constantsToObject(Keyboard);
			
			var i:int, j:int;
			ProcedureProfiler.Default.startTiming();
			for (i = 0; i < 1000; i++)
			{
				for (j = 0; j < constants.length; j++)
				{
					Keyboard[constants[j]];
				}
			}
			trace("Class access:", ProcedureProfiler.Default.stopTiming());
			
			ProcedureProfiler.Default.startTiming();
			for (i = 0; i < 1000; i++)
			{
				for (j = 0; j < constants.length; j++)
				{
					constantsObj[constants[j]];
				}
			}
			trace("Object access:", ProcedureProfiler.Default.stopTiming());
		}
		
	}

}