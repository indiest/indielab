package  
{
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.Event;
	import me.sangtian.common.display.StarsEffect;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.Random;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	[SWF(width=800, height=600, color="#000000")]
	public class StarsEffectTest extends Sprite 
	{
		private var _starsEffect:StarsEffect = new StarsEffect(800, 600, 1000, 0xffffff);
		
		public function StarsEffectTest() 
		{
			_starsEffect.velocityY = 200;
			stage.color = 0;
			//_starsEffect.minRadius = _starsEffect.maxRadius = 100;
			addChild(_starsEffect);
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			Time.tick();
			_starsEffect.update(Time.deltaSecond);
		}
	}
}