package  
{
	import com.noteflight.standingwave3.elements.AudioDescriptor;
	import com.noteflight.standingwave3.elements.Sample;
	import com.noteflight.standingwave3.filters.ResamplingFilter;
	import com.noteflight.standingwave3.filters.StandardizeFilter;
	import com.noteflight.standingwave3.generators.SoundGenerator;
	import com.noteflight.standingwave3.output.AudioPlayer;
	import com.noteflight.standingwave3.sources.LoopSource;
	import com.noteflight.standingwave3.sources.SoundSource;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	import me.sangtian.common.util.MathUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class StandingWaveTest extends Sprite 
	{
		[Embed(source = "../bin/resources/music/Level1.mp3")]
		private static const SoundAsset:Class;
		private const MAGIC_DELAY:Number = 2257.0; // LAME 3.98.2 + flash.media.Sound Delay		
		private var _sound:Sound = new SoundAsset();
		private var _nextStartTime:Number;
		private var _channel:SoundChannel;
		private var _resampleSource:ResamplingFilter;
		private var _source:LoopSource;
		private var _player:AudioPlayer = new AudioPlayer();
		
		public function StandingWaveTest() 
		{
			//var mp3:Sound = new Sound();
			//mp3.load(new URLRequest("resources/music/test.mp3"));
			//mp3.play();
			//var soundSource:SoundSource = new SoundSource(sound);
			//var soundGenerator:SoundGenerator = new SoundGenerator(sound);
			
			/*
			_channel = _sound.play(0, int.MAX_VALUE);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			addEventListener(Event.ENTER_FRAME, onUpdate);
			*/
			
			
			var sample:Sample = new Sample(new AudioDescriptor(), _sound.length * 44.1);
			sample.extractSound(_sound, 0, _sound.length * 44.1);
			_source = new LoopSource(sample.descriptor, sample);
			_source.amplitude = 0.2;
			_source.firstFrame = MAGIC_DELAY;
			_source.startFrame = MAGIC_DELAY;
			_source.endFrame = sample.frameCount / 10;
			trace(_source.duration);
			//_resampleSource = new ResamplingFilter(_source, 0.99);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			_player.play(_source);
		}
		
		private function onUpdate(e:Event):void 
		{
			if (_source != null)
			{
				_source.frequencyShift += 0.005;
				if (_source.frequencyShift >= 1)
				{
					_player.stop();
					_channel = _sound.play(_nextStartTime);
				}
			}
		}
		
		private function onKeyDown(e:KeyboardEvent):void 
		{
			if (e.keyCode == Keyboard.SPACE)
			{
				var duration:Number = MathUtil.clamp(_sound.length - _channel.position, 0, 2);
				trace("Slow down duration:", duration);
				_nextStartTime = _channel.position + duration;
				var numFrames:Number = duration * 44.1 / 8;
				var sample:Sample = new Sample(new AudioDescriptor(), numFrames);
				sample.extractSound(_sound, _channel.position * 44.1 / 8, numFrames);
				_source = new LoopSource(sample.descriptor, sample);
				_source.frequencyShift = 0.4;
				_player.play(_source);
				_channel.stop();
			}
		}
		
		private function onMouseWheel(e:MouseEvent):void 
		{
			//var resampleSource:ResamplingFilter = _player.source as ResamplingFilter;
			//_resampleSource.factor += MathUtil.sign(e.delta) * 0.1;
			//trace(_resampleSource.factor);
			_source.frequencyShift += MathUtil.sign(e.delta) * 0.1;
			trace(_source.frequencyShift);
		}
		
	}

}