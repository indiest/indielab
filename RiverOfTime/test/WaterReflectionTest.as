package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DisplacementMapFilter;
	import flash.filters.DisplacementMapFilterMode;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import me.sangtian.common.display.MirrorEffect;
	import me.sangtian.common.Time;
	import me.sangtian.common.ui.UIUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	[SWF(width=900, height=480, color="#000000")]
	public class WaterReflectionTest extends Sprite 
	{
		[Embed(source = "moon_river1.png")]
		private static const Bitmap1:Class;
		[Embed(source = "moon_river2.png")]
		private static const Bitmap2:Class;
		[Embed(source = "../assets/levels/1/rock1.png")]
		private static const CanoeBitmap:Class;

		private var _canoe:Bitmap = new CanoeBitmap();
		private var _bitmap1:Bitmap = new Bitmap1();
		private var _bitmap2:Bitmap = new Bitmap2();
		private var _bitmapContainer:Sprite = new Sprite();
		
		private var _bitmap1Mirror:Bitmap;
		private var _filter:DisplacementMapFilter;
		private var _offset:Number = 0;
		
		private var _riverMirror:MirrorEffect = new MirrorEffect(_bitmap1);
		private var _canoeMirrorX:MirrorEffect = new MirrorEffect(_canoe, MirrorEffect.AXIS_X);
		private var _canoeMirrorY:MirrorEffect = new MirrorEffect(_canoe, MirrorEffect.AXIS_Y);
		
		public function WaterReflectionTest() 
		{
			_bitmapContainer.addChild(_bitmap1);

			/*
			_bitmap1Mirror = new Bitmap(new BitmapData(_bitmap1.width, _bitmap1.height, false));
			const drawMatrix:Matrix = new Matrix();
			drawMatrix.scale(1, -1);
			drawMatrix.translate(0, _bitmap1.height);
			_bitmap1Mirror.bitmapData.draw(_bitmap1.bitmapData, drawMatrix);
			_bitmap1Mirror.y = _bitmap1.height;
			_bitmapContainer.addChild(_bitmap1Mirror);
			*/

			_riverMirror.y = _bitmap1.height;
			_bitmapContainer.addChild(_riverMirror);
			
			_bitmap2.y = 420;//_bitmap1.y + _bitmap1.height;
			_bitmapContainer.addChild(_bitmap2);
			addChild(_bitmapContainer);
			
			/*
			var perlinMap:BitmapData = new BitmapData(_bitmap1Mirror.width, _bitmap1Mirror.height, false);
			perlinMap.perlinNoise(200, 20, 2, 5, true, true, 0, true);
			
			var displaceMap:BitmapData = new BitmapData(_bitmap1Mirror.width, _bitmap1Mirror.height * 2, false);
			displaceMap.copyPixels(perlinMap, perlinMap.rect, new Point(0, 0));
			displaceMap.copyPixels(perlinMap, perlinMap.rect, new Point(0, _bitmap1Mirror.height));
			
			_filter = new DisplacementMapFilter(displaceMap, null, BitmapDataChannel.RED, BitmapDataChannel.RED, 40, 5, DisplacementMapFilterMode.CLAMP);
			//_bitmap1Mirror.filters = [_filter];
			*/
			
			/*
			_canoe.x = 200;
			_canoe.y = 300;
			addChild(_canoe);
			_canoeMirrorX.x = _canoe.x;
			_canoeMirrorX.y = _canoe.y + _canoe.height;
			addChild(_canoeMirrorX);
			_canoeMirrorY.x = _canoe.x + _canoe.width;
			_canoeMirrorY.y = _canoe.y;
			addChild(_canoeMirrorY);
			*/
			
			const X:Number = 750;
			const Y:Number = 30;
			var i:int = 1;
			//UIUtil.addPropertySlider(this, X, i++*Y, _riverMirror, "perlinBaseX", 0, 1000, 10);
			//UIUtil.addPropertySlider(this, X, i++*Y, _riverMirror, "perlinBaseY", 0, 1000, 10);
			UIUtil.addPropertyComboBox(this, X, i++*Y, _riverMirror, "displacementAxis", MirrorEffect);
			UIUtil.addPropertyComboBox(this, X, i++*Y, _riverMirror, "displaceChannelX", BitmapDataChannel);
			UIUtil.addPropertyComboBox(this, X, i++*Y, _riverMirror, "displaceChannelY", BitmapDataChannel);
			UIUtil.addPropertySlider(this, X, i++*Y, _riverMirror, "displaceScaleX", 0, 100, 1);
			UIUtil.addPropertySlider(this, X, i++*Y, _riverMirror, "displaceScaleY", 0, 100, 1);
			UIUtil.addPropertySlider(this, X, i++*Y, _riverMirror, "speed", 1, 100, 1);
			UIUtil.addPropertySlider(this, X, i++*Y, _riverMirror, "updateInterval", 0, 1);
			
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			/*
			if (_offset > _bitmap1Mirror.height)
				_offset = 0;
			else
				_offset += 0.3;
				
			_filter.mapPoint = new Point(0, _offset - _bitmap1Mirror.height);
			_bitmap1Mirror.filters = [_filter];
			*/
			
			Time.tick();
			_riverMirror.update(Time.deltaSecond);
			_canoeMirrorX.update(Time.deltaSecond);
			_canoeMirrorY.update(Time.deltaSecond);
		}
		
	}

}