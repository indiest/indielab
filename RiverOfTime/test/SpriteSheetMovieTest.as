package  
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.getTimer;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.util.Random;
	import net.hires.debug.Stats;
	import starling.core.Starling;
	
	/**
	 * Test animation performance (20 sprites). Result:
	 *
	 * 	MovieClip + Vector:			58FPS, 3MB + 0.03MB/s
	 * 	MovieClip + cacheAsBitmap:	58FPS, 3.5MB + 0.02MB/s
	 * 	Sprite Sheet (copyPixels):	58FPS, 3.4MB + 0.002MB/s
	 * 	Sprite Sheet (scrollRect):	60FPS, 3.8MB + 0.01MB/s
	 * 	Sprite Sheet (Starling):	60FPS, 5MB + 0.04MB/s
	 * @author Nicolas Tian
	 */
	public class SpriteSheetMovieTest extends Sprite 
	{
		public static const NUM:int = 20;
		
		[Embed(source = "../assets/animations/explosion.png")]
		public static const ExplosionBitmap:Class;
		[Embed(source = "../assets/animations/explosion_data.xml", mimeType="application/octet-stream")]
		public static const ExplosionData:Class;
		
		public function SpriteSheetMovieTest() 
		{
			//testMovieClip(false);
			//testMovieClip(true);
			//testSpriteSheet();
			testStarlingMovieClip();
			addChild(new Stats());
		}
		
		private function testStarlingMovieClip():void 
		{
			new Starling(StarlingRoot, stage).start();
		}
		
		private var _sheets:Vector.<StarlingSpriteSheet> = new Vector.<StarlingSpriteSheet>();
		private function testSpriteSheet():void 
		{
			var image:Bitmap = new ExplosionBitmap();
			var data:String = new ExplosionData();
			for (var i:int = 0; i < NUM; i++)
			{
				var sheet:StarlingSpriteSheet = new StarlingSpriteSheet(image.bitmapData, StarlingSpriteSheetData.parse(data));
				sheet.x = Random.rangeInt(0, stage.stageWidth - 100);
				sheet.y = Random.rangeInt(0, stage.stageHeight - 100);
				_sheets.push(sheet);
				addChild(sheet);
			}
			addEventListener(Event.ENTER_FRAME, onUpdate);
		}
		
		private function onUpdate(e:Event):void 
		{
			for (var i:int = 0; i < NUM; i++)
			{
				var sheet:StarlingSpriteSheet = _sheets[i];
				sheet.currentFrame++;// = getTimer() / 1000;//
				if (sheet.currentFrame == sheet.totalFrames)
					sheet.currentFrame = 0;
			}
			
		}
		
		private function testMovieClip(cacheAsBitmap:Boolean):void
		{
			for (var i:int = 0; i < NUM; i++)
			{
				var mc:MovieClip = new Level2Explosion();
				mc.x = Random.rangeInt(0, stage.stageWidth - 100);
				mc.y = Random.rangeInt(0, stage.stageHeight - 100);
				mc.cacheAsBitmap = cacheAsBitmap;
				addChild(mc);
			}
		}
		
	}

}
import me.sangtian.common.util.Random;
import starling.core.Starling;
import starling.display.MovieClip;
//import starling.display.Sprite;
import starling.textures.Texture;
import starling.textures.TextureAtlas;


class StarlingRoot extends starling.display.Sprite
{
	public function StarlingRoot()
	{
		Starling.current.stage.stageWidth = Starling.current.nativeStage.fullScreenWidth;
		Starling.current.stage.stageHeight = Starling.current.nativeStage.fullScreenHeight;
		
		var textureAtlas:TextureAtlas = new TextureAtlas(
			Texture.fromBitmap(new SpriteSheetMovieTest.ExplosionBitmap), 
			new XML(new SpriteSheetMovieTest.ExplosionData));
		var textures:Vector.<Texture> = textureAtlas.getTextures();
		for (var i:int = 0; i < SpriteSheetMovieTest.NUM; i++)
		{
			var mc:MovieClip = new MovieClip(textures, 60);
			mc.x = Random.rangeInt(0, Starling.current.stage.stageWidth - 100);
			mc.y = Random.rangeInt(0, Starling.current.stage.stageHeight - 100);
			addChild(mc);
			Starling.juggler.add(mc);
		}
	}
}
