package  
{
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class MotionBlurTest extends Sprite 
	{
		[Embed("/../assets/levels/1/canoe.png")]
		public static const LEVELS_1_CANOE:Class;
		
		private var _gfxLayer:Sprite = new Sprite();
		private var _shadowEffect:ShadowEffect;
		private var _raft:PhysicalSprite = new PhysicalSprite(LEVELS_1_CANOE);
		
		public function MotionBlurTest() 
		{
			stage.color = 0x000000;
			_shadowEffect = new ShadowEffect(_gfxLayer, 20);
			DisplayObjectUtil.tint(_gfxLayer, 0.5, 0x888840);
			_gfxLayer.alpha = 0.5;
			addChild(_gfxLayer);
			addChild(_raft);
			_raft.setTo(180, 320);
			stage.addEventListener(MouseEvent.CLICK, handleClickStage);
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			Time.tick();
			_shadowEffect.update(Time.deltaSecond);
		}
		
		
		private function handleClickStage(e:MouseEvent):void 
		{
			TweenLite.to(_raft, 1.0, { x: e.stageX, y: e.stageY, onComplete: _shadowEffect.stopTracing } );
			_shadowEffect.startTracing(_raft, 10, 0.3, false);
		}
	}

}