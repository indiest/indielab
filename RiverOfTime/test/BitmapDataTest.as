package  
{
	import com.junkbyte.console.addons.displaymap.DisplayMapAddon;
	import com.junkbyte.console.Cc;
	import com.junkbyte.console.KeyBind;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import me.sangtian.common.console.BitmapInspector;
	import me.sangtian.common.ProcedureProfiler;
	
	/**
	 * ...
	 * @author 
	 */
	public class BitmapDataTest extends Sprite 
	{
		
		/**
		 * Properse: Compare the speed of two approaches of copying bitmaps: draw & copyPixels
		 * 
		 * Output result(100x): 31ms vs. 16ms
		 * 
		 * Conclusion: Flash has decent optimization for draw method. When copying content with multiple Bitmaps,
		 *    use draw rather than copyPixels for lower complexity.
		 */
		public function BitmapDataTest() 
		{
			//DisplayMapAddon.start(this);
			//DisplayMapAddon.addToMenu();
			
			var container:Sprite = new Sprite();
			var bitmapData1:BitmapData = new BitmapData(360, 640, true, 0xff00ff00);
			var bitmap1:Bitmap = new Bitmap(bitmapData1);
			bitmap1.y = -300;
			container.addChild(bitmap1);
			var bitmapData2:BitmapData = new BitmapData(360, 640, true, 0xff0000ff);
			var bitmap2:Bitmap = new Bitmap(bitmapData2);
			bitmap2.y = 440;
			container.addChild(bitmap2);
			
			var merge:Bitmap = new Bitmap(new BitmapData(360, 640, false, 0));
			ProcedureProfiler.Default.startTiming();
			for (var i:int = 0; i < 100; i++)
			{
				merge.bitmapData.draw(container, null, null, null, null);// merge.bitmapData.rect);
			}
			trace("Draw container:", ProcedureProfiler.Default.stopTiming());
			
			BitmapInspector.inspect(merge);
			merge.bitmapData.fillRect(merge.bitmapData.rect, 0);
			BitmapInspector.inspect(merge);
			
			ProcedureProfiler.Default.startTiming();
			for (var j:int = 0; j < 100; j++)
			{
				var bounds:Rectangle = bitmap1.getBounds(container);
				merge.bitmapData.copyPixels(bitmapData1, bounds, bounds.topLeft);
				bounds = bitmap2.getBounds(container);
				merge.bitmapData.copyPixels(bitmapData2, bounds, bounds.topLeft);
			}
			trace("Pixels copies:", ProcedureProfiler.Default.stopTiming());

			BitmapInspector.inspect(merge);
			addChild(merge);
		}
		
	}

}