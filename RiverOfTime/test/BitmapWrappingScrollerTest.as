package  
{
	import com.greensock.BlitMask;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	import me.sangtian.common.display.BitmapWrappingScroller;
	import me.sangtian.common.ProcedureProfiler;
	import net.hires.debug.Stats;
	
	/**
	 * Test functionality of BitmapWrappingScroller.
	 * Compare performance with BlitMask. Result:
	 *	Time(ms)	Scroller	Mask
	 * 	1015		1			1
	 * 	2012		0.048		1.244
	 * 	5004		0.009		0.466
	 *	10004 		0.004		0.25
	 * 	20006 		0.002		0.169
	 * 
	 * @author Nicolas Tian
	 */
	public class BitmapWrappingScrollerTest extends Sprite 
	{
		private var _scroller:BitmapWrappingScroller;
		private var _scrollerProfiler:ProcedureProfiler = new ProcedureProfiler();
		private var _mask:BlitMask;
		private var _maskTarget:Bitmap;
		private var _maskProfiler:ProcedureProfiler = new ProcedureProfiler();
		
		public function BitmapWrappingScrollerTest() 
		{
			var bitmap:Bitmap = new Assets.LEVELS_4_BG_SCROLL;
			addChild(bitmap);
			_maskTarget = new Bitmap(bitmap.bitmapData.clone());
			_maskTarget.x = 200;
			_mask = new BlitMask(_maskTarget, _maskTarget.x, _maskTarget.y, _maskTarget.width, 640, false, false, 0, true);
			_mask.x = 200;
			addChild(_mask);
			_scroller = new BitmapWrappingScroller(bitmap.bitmapData);
			addEventListener(Event.ENTER_FRAME, onUpdate);
			stage.addEventListener(MouseEvent.CLICK, onClick);
			addChild(new Stats());
		}
		
		private function onClick(e:MouseEvent):void 
		{
			_scroller.reset();
			_maskTarget.y = 0;
		}
		
		private function onUpdate(e:Event):void 
		{
			_scrollerProfiler.startTiming();
			//_scroller.scrollX += 7;
			_scroller.scrollY += 7;
			_scrollerProfiler.countTimes();
			
			_maskProfiler.startTiming();
			_maskTarget.y += 7;
			_mask.update(e, true);
			_maskProfiler.countTimes();
			
			trace(getTimer(), _scrollerProfiler.getAverageMillisecond(), _maskProfiler.getAverageMillisecond());
		}
		
	}

}