package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	import me.sangtian.common.util.MathUtil;
	import treefortress.sound.SoundManager;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class SoundManagerTest extends Sprite 
	{
		private var _sm:SoundManager = new SoundManager();
		private var _dynamicSound:Sound = new Sound();
		private var _dynamicSoundChannel:SoundChannel;
		private var _playbackSpeed:Number = 1.0;
		private var _phase:Number = 0;
		private var _numSamples:int;
		
		public function SoundManagerTest() 
		{
			_sm.loadSound("resources/music/test.mp3", "test");
			//_sm.play("river");
			_dynamicSound.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			play();
		}
		
		public static const SAMPLES_PER_CALLBACK:int = 4096; // Should be >= 2048 && < = 8192
		
		private function onSampleData(e:SampleDataEvent):void 
		{
			var sound:Sound = _sm.getSound("test").sound;
			_numSamples = int(sound.length * 44.1) / 8;
			//trace("samples:", _numSamples);
			
			//sampleBySpeed1(sound, e);
			sampleBySpeed2(sound, e);
		}
		
		private function sampleBySpeed2(sound:Sound, e:SampleDataEvent):void 
		{
			var samples:ByteArray = new ByteArray();
			sound.extract(samples, SAMPLES_PER_CALLBACK * _playbackSpeed, _phase);
			samples.position = 0;
			trace(int(samples.length / 8));
			var left:Number;
			var right:Number;
			while (samples.bytesAvailable > 1)
			{
				e.data.writeFloat(samples.readFloat());
				e.data.writeFloat(samples.readFloat());
				
				_phase += _playbackSpeed;
			}
		}
		
		private function sampleBySpeed1(sound:Sound, e:SampleDataEvent):void 
		{
			var l:Number;
			var r:Number;
			var p:int;

			var loadedSamples:ByteArray = new ByteArray();
			var startPosition:int = int(_phase);
			sound.extract(loadedSamples, SAMPLES_PER_CALLBACK * _playbackSpeed, startPosition);
			loadedSamples.position = 0;

			while (loadedSamples.bytesAvailable > 0) {

				p = int(_phase - startPosition) * 8;

				if (p < loadedSamples.length - 8 && e.data.length <= SAMPLES_PER_CALLBACK * 8) {
				   
				   loadedSamples.position = p;
				   
				   l = loadedSamples.readFloat(); 
				   r = loadedSamples.readFloat(); 

				   e.data.writeFloat(l); 
				   e.data.writeFloat(r);
					
				} else {
				   loadedSamples.position = loadedSamples.length;
				}

				_phase += _playbackSpeed;

				// loop
				if (_phase >= _numSamples) {
					trace("loop", sound.bytesLoaded, "/", sound.bytesTotal);
				   _phase -= _numSamples;
				   break;
				}
			}
			
		}
		
		private function onMouseWheel(e:MouseEvent):void 
		{
			_playbackSpeed += MathUtil.sign(e.delta) * 0.1;
			trace(_playbackSpeed);
			play();
		}
		
		private function play():void 
		{
			if (_dynamicSoundChannel == null)
			{
				_dynamicSoundChannel = _dynamicSound.play();
				_dynamicSoundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundFinished);
				_phase = 0;
			}
		}
      
		private function onSoundFinished(event:Event):void
		{
			_dynamicSoundChannel.removeEventListener(Event.SOUND_COMPLETE, onSoundFinished);
			_dynamicSoundChannel = _dynamicSound.play();
			_dynamicSoundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundFinished);
		}		
	}

}