package  
{
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class GradientTest extends Sprite 
	{
		
		public function GradientTest() 
		{
			//stage.color = 0xffffff;
			graphics.lineStyle(10, 0xffffff);
			graphics.drawRect(100, 200, 100, 100);
			
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(100, 20);
			graphics.lineGradientStyle("linear", [0xffffff, 0xffffff, 0xffffff], [1, 0.5, 0.1], [0, 128, 255], matrix);
			graphics.moveTo(100, 100);
			graphics.lineTo(200, 200);
			graphics.beginGradientFill("linear", [0xffffff, 0xffffff, 0xffffff], [1, 0.5, 0.1], [0, 128, 255], matrix);
			graphics.drawRect(200, 200, 100, 10);
			graphics.endFill();
			
			
            var myShape:Shape = new Shape();
            var gradientBoxMatrix:Matrix = new Matrix();
  
            gradientBoxMatrix.createGradientBox(200, 40, 0, 0, 0);  
            
            myShape.graphics.lineStyle(5);
  
            myShape.graphics.lineGradientStyle("linear", [0xFF0000,
            0x00FF00, 0x0000FF], [1, 1, 1], [0, 128, 255], gradientBoxMatrix);
            
            myShape.graphics.drawRect(0, 0, 200, 40);
            myShape.graphics.drawCircle(100, 120, 50);  
             
            this.addChild(myShape);	
			
			var circle:Shape = new Shape();
			circle.graphics.beginGradientFill(GradientType.RADIAL, [0xffffff, 0x000000], [1, 1], [0, 255]);
			circle.graphics.drawCircle(0, 0, 50);
			circle.graphics.endFill();
			circle.x = 100;
			circle.y = 300;
			addChild(circle);
		}
		
	}

}