package  
{
	import flash.display.Sprite;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Level4Laser extends Level4Bullet 
	{
		private var _laserAnimation:StarlingSpriteSheet;
		
		public function Level4Laser(shield:Sprite)
		{
			super(shield, Assets.LEVELS_4_BULLET_RED, PhysicalSprite.EMPTY_BODY_NAME);
			//// Set pivot point of laser block to (0, 0) for scaling purpose
			//DisplayObjectUtil.pivot(bullet.getChildAt(0), 0, 0);
			removeChild(this.bitmap);
			_laserAnimation = new StarlingSpriteSheet(
				AssetsCache.getBitmapData(Assets.ANIMATIONS_LASER),
				StarlingSpriteSheetData.parse(AssetsCache.getOrCacheAsset(Assets.ANIMATIONS_LASER_DATA))
			);
			_laserAnimation.frameRate = 10;
			addChild(_laserAnimation);
		}
		
		override public function update(deltaSecond:Number):void 
		{
			super.update(deltaSecond);
			_laserAnimation.updateFrame(deltaSecond);
		}
	}

}