package  
{
	import com.greensock.easing.Circ;
	import com.greensock.plugins.TransformAroundPointPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.Direction;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.Input;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.nape.NapeUtil;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.ColorUtil;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import nape.callbacks.CbType;
	import nape.phys.Material;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Raft extends PhysicalSprite 
	{
		public static const CB_TYPE:CbType = new CbType();
		public static const INIT_LIFE_REGEN:Number = 0.3;
		public static const INIT_MAX_SPEED:Number = 300;
		
		private var _hitTestShape:Shape;
		private var _animation:StarlingSpriteSheet;
		public function get animation():StarlingSpriteSheet 
		{
			return _animation;
		}
		
		public var accelerationX:Number = 100;
		public var accelerationY:Number = 100;
		public var maxSpeedX:Number = INIT_MAX_SPEED;
		public var maxSpeedY:Number = INIT_MAX_SPEED;
		private var _life:Number = 1;
		public function get life():Number 
		{
			return _life;
		}
		
		public var lifeRegen:Number = INIT_LIFE_REGEN;
		public var damageFactor:Number = 1;
		
		// Restart progress
		public var checkPoint:Number = 0;
		
		public function Raft() 
		{
			super(Assets.LEVELS_1_CANOE, "canoe");
			_animation = new StarlingSpriteSheet(
				AssetsCache.getBitmapData(Assets.ANIMATIONS_SOLITARY), 
				StarlingSpriteSheetData.parse(AssetsCache.getOrCacheAsset(Assets.ANIMATIONS_SOLITARY_DATA))
			);
			_animation.x = -27;
			_animation.y = -27;
			_animation.play();
			addChild(_animation);
			body.cbTypes.add(CB_TYPE);
			body.setShapeMaterials(Material.wood());
			body.allowRotation = false;
			
			_hitTestShape = new Shape();
			_hitTestShape.graphics.beginFill(0x000000, 0.7);
			// The hit area for bullets
			_hitTestShape.graphics.drawRect(-11, -27, 24, 48);
			_hitTestShape.graphics.endFill();
			_hitTestShape.visible = false;
			addChild(_hitTestShape);
		}
		
		override public function hitTestObject(obj:DisplayObject):Boolean 
		{
			if (collisionEnabled)
				return _hitTestShape.hitTestObject(obj);
			else
				return false;
		}
		
		public function update(deltaSecond:Number):void
		{
			CONFIG::debug
			{
				if (isNaN(body.velocity.x) || isNaN(body.velocity.y))
					throw new Error();
			}
			
			if (life <= 0)
				return;
				
			body.velocity.x = MathUtil.clamp(
				body.velocity.x + accelerationX * Game.instance.input.getAxisValue("X"),//Input.getAxis(Input.AXIS_HORIZONTAL),
				-maxSpeedX, maxSpeedX);
			body.velocity.y = MathUtil.clamp(
				body.velocity.y + accelerationY * Game.instance.input.getAxisValue("Y"),//Input.getAxis(Input.AXIS_VERTICAL),
				-maxSpeedY, maxSpeedY);
			
			x = body.position.x;
			y = body.position.y;
			
			updateLife(deltaSecond);
			
			var dir:uint = Game.instance.isOutOfScreen(this);
			if (dir & Direction.UP)
			{
				setTo(x, height >> 1);
			}
			if (dir & Direction.LEFT)
			{
				setTo(0, y);
			}
			else if (dir & Direction.RIGHT)
			{
				setTo(Game.instance.stageWidth, y);
			}
			else if (dir & Direction.DOWN)
			{
				Game.instance.currentLevel.gameOver(-1, checkPoint);
				return;
			}
		}
		
		private function updateLife(deltaSecond:Number):void 
		{
			if (life < 1)
			{
				_life += lifeRegen * deltaSecond;
				CONFIG::debug
				{
					//Log.debug("Raft life:", life);
				}
				if (life > 0.5)
				{
					DisplayObjectUtil.tint(Game.instance.currentLevel);
				}
				else if (life > 0)
				{
					DisplayObjectUtil.tint(Game.instance.currentLevel, life, ColorUtil.rgb(0xff * life, 0, 0));
				}
			}
		}
		
		public function damage(value:Number, cause:int):void
		{
			_life -= value * damageFactor;
			if (life <= 0)
			{
				Game.instance.currentLevel.gameOver(cause, checkPoint, 0xffff0000);
				Game.instance.zoom(2.0, this.x, this.y, 1.0);
			}
		}
		
		public function reset():void
		{
			_life = 1;
			body.velocity.x = body.velocity.y = 0;
			DisplayObjectUtil.tint(Game.instance.currentLevel);
		}
	}

}