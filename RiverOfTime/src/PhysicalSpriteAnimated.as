package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.util.DisplayObjectUtil;
	
	[Event(name="complete", type="flash.events.Event")] 
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class PhysicalSpriteAnimated extends PhysicalSprite 
	{
		private var _animation:StarlingSpriteSheet;
		private var _frameBitmap:Bitmap;
		
		public function get animation():StarlingSpriteSheet 
		{
			return _animation;
		}
		
		public function PhysicalSpriteAnimated(spritesheetClass:Class, data:StarlingSpriteSheetData, bodyName:String = null) 
		{
			super(spritesheetClass, bodyName);
			/*
			var data:* = AssetsCache.getOrCacheAsset(dataClass);
			if (data is String)
			{
				data = StarlingSpriteSheetData.parse(data);
			}
			*/
			_animation = new StarlingSpriteSheet(AssetsCache.getBitmapData(spritesheetClass), data);
			_animation.x = -_animation.maxWidth * 0.5;
			_animation.y = -_animation.maxHeight * 0.5;
			_animation.addEventListener(Event.COMPLETE, dispatchEvent);
			addChild(_animation);
		}
		
		/*
		override public function setTo(x:Number, y:Number):void 
		{
			super.setTo(x - _animation.maxWidth * 0.5, y - _animation.maxHeight * 0.5);
		}
		*/
		
		override protected function initBitmap():void 
		{
			// Discard static bitmap initialization
		}
		
		override public function get bitmap():Bitmap 
		{
			if (_frameBitmap == null)
			{
				_frameBitmap = new Bitmap(new BitmapData(_animation.maxWidth, _animation.maxHeight, true, 0));
			}
			DisplayObjectUtil.clearBitmap(_frameBitmap);
			_animation.getCurrentFrameBitmapData(_frameBitmap.bitmapData);
			return _frameBitmap;
		}
		
		public function play(startFrame:int = 0):void
		{
			_animation.play(startFrame);
		}
		
		public function stop():void
		{
			_animation.stop();
		}
		
		public function update(deltaSecond:Number):void
		{
			_animation.updateFrame(deltaSecond);
		}
	}

}