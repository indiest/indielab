package  
{
	import com.gamua.flox.Flox;
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.state.IState;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.Random;
	import me.sangtian.common.util.StringUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class MenuState extends Sprite implements IState 
	{
		private var _openingMovie:MovieClip;
		private var _openingScene1:Sprite;
		
		private var _menuMain:MenuMain;
		private var _menuLevel:MenuSelectLevel;
		
		private var _levelSelectMode:int;
		
		private var _time:Number;
		
		public function MenuState() 
		{
			// Lazy initialize menus
			//initMenus();
			
			addEventListener(MouseEvent.CLICK, handleClick);
		}
		
		private function initMenus():void 
		{
			if (_menuMain == null)
			{
				_menuMain = new MenuMain();
				_menuMain.buttonContinue.addEventListener(MouseEvent.CLICK, handleClickContinue);
				_menuMain.buttonNewGame.addEventListener(MouseEvent.CLICK, handleClickNewGame);
				_menuMain.buttonEndless.addEventListener(MouseEvent.CLICK, handleClickEndless);
				//CONFIG::debug
				{
					_menuMain.textVersion.text = "v" + Game.instance.appVersion;
				}
				
				_menuLevel = new MenuSelectLevel();
				_menuLevel.buttonBack.addEventListener(MouseEvent.CLICK, handleSelectLevelBack);
				for (var i:int = 1; i <= 4; i++)
				{
					var levelItem:MovieClip = _menuLevel["item" + i];
					levelItem.buttonMode = true;
					levelItem.addEventListener(MouseEvent.CLICK, handleSelectLevel);
				}
			}
		}
		
		private function handleClick(e:MouseEvent):void 
		{
			Flox.logEvent("MenuClick", { ItemName: e.target.name, Time: _time } );
			// Playing opening movie, click to skip
			if (Game.instance.player.level == 0)
			{
				onMovieFinish();
			}
		}
		
		private function handleClickContinue(e:MouseEvent):void 
		{
			enterLevel(Game.instance.player.level, BaseLevel.MODE_NORMAL);
		}
		
		private function handleClickNewGame(e:MouseEvent):void 
		{
			for (var i:int = 1; i <= 4; i++)
			{
				var levelItem:MovieClip = _menuLevel["item" + i];
				if (Game.instance.player.level >= i)
				{
					DisplayObjectUtil.tint(levelItem);
				}
				else
				{
					DisplayObjectUtil.tint(levelItem, 0.5);
				}
			}
			crossfadeMenu(_menuMain, _menuLevel);
			_levelSelectMode = BaseLevel.MODE_NORMAL;
		}
		
		private function handleClickEndless(e:MouseEvent):void 
		{
			for (var i:int = 1; i <= 4; i++)
			{
				var levelItem:MovieClip = _menuLevel["item" + i];
				if (Game.instance.player.level > i)
				{
					DisplayObjectUtil.tint(levelItem);
				}
				else
				{
					DisplayObjectUtil.tint(levelItem, 0.5);
				}
			}
			crossfadeMenu(_menuMain, _menuLevel);
			_levelSelectMode = BaseLevel.MODE_ENDLESS;
		}
		
		private function handleSelectLevelBack(e:MouseEvent):void 
		{
			_menuLevel.textPrompt.visible = false;
			addChild(_menuMain);
			crossfadeMenu(_menuLevel, _menuMain);
		}
		
		private function handleSelectLevel(e:MouseEvent):void 
		{
			var levelItem:MovieClip = e.currentTarget as MovieClip;
			var level:int = StringUtil.getPostfixNumber(levelItem.name);
			if (_levelSelectMode == BaseLevel.MODE_NORMAL)
			{
				if (level <= Game.instance.player.level)
				{
					enterLevel(level, _levelSelectMode);
				}
			}
			else if (_levelSelectMode == BaseLevel.MODE_ENDLESS)
			{
				if (level < Game.instance.player.level)
				{
					enterLevel(level, _levelSelectMode);
				}
				else
				{
					_menuLevel.textPrompt.y = levelItem.y;// - _menuLevel.textPrompt.height;
					_menuLevel.textPrompt.visible = true;
				}
			}
		}
		
		private function crossfadeMenu(from:MovieClip, to:MovieClip, duration:Number = 1.5):void 
		{
			from.alpha = 1;
			from.mouseEnabled = false;
			to.alpha = 0;
			to.mouseEnabled = false;
			addChild(to);
			TweenLite.to(from, duration, { alpha: 0, onComplete: DisplayObjectUtil.removeFromParent, onCompleteParams: [from] } );
			TweenLite.to(to, duration, { alpha : 1, onComplete: enableInteration, onCompleteParams: [to] } );
		}
		
		private function enableInteration(obj:Sprite):void 
		{
			obj.mouseEnabled = true;
		}
		
		/* INTERFACE me.sangtian.common.state.IState */
		
		public function get isTransientState():Boolean 
		{
			return false;
		}
		
		public function enter():void 
		{
			_time = 0;
			// When the opening movie ends, the background has to be white.
			Game.instance.nativeStage.color = 0;// 0xffffff;
			Game.instance.music.playLoop("Opening");
			// First time play
			if (Game.instance.player.level == 0)
			{
				//_openingMovie = new Opening();
				//_openingMovie = new Assets.ANIMATIONS_OPENING;
				//_openingMovie.x = _openingMovie.width >> 1;
				//_openingMovie.y = _openingMovie.height >> 1;
				//_openingMovie.name = "movieOpening";
				//DisplayObjectUtil.playMovieClip(_openingMovie, this, false, onMovieFinish);
				
				DisplayObjectUtil.playMovie("opening.swf", this, false, onMovieFinish);
				return;
			}
			
			if (_openingScene1 == null)
			{
				//_openingScene1 = new OpeningMovieScene1();
				_openingScene1 = new Assets.ANIMATIONS_OPENING_SCENE1;
				_openingScene1.name = "movieOpeningScene1";
				// Randomize birds flying animation
				var birdIndex:int = 0;
				while (true)
				{
					var birdAnimation:MovieClip = _openingScene1.getChildByName("b" + birdIndex) as MovieClip;
					if (birdAnimation == null)
						break;
					var birdMc:MovieClip = birdAnimation.getChildAt(0) as MovieClip;
					var frame:int = Random.rangeInt(1, birdMc.totalFrames);
					birdMc.gotoAndPlay(frame);
					birdIndex++;
				}
			}
			addChild(_openingScene1);
			
			initMenus();
			_menuMain.buttonContinue.visible = (Game.instance.player.level <= 4);
			_menuLevel.textPrompt.visible = false;
			_menuMain.alpha = 1;
			addChild(_menuMain);
			Game.instance.fade(0x00ffffff, 2.0);
		}
		
		private function onMovieFinish():void 
		{
			enterLevel(1, BaseLevel.MODE_NORMAL);
		}
		
		public function update():void 
		{
			_time += Time.deltaSecond;
		}
		
		public function exit():void 
		{
			DisplayObjectUtil.removeMovieClip(_openingMovie);
			//DisplayObjectUtil.removeMovieClip(_openingScene1);
			_openingMovie = null;
			DisplayObjectUtil.removeFromParent(_menuLevel);
			Game.instance.music.stopAll();
		}
		
		private function enterLevel(level:uint, mode:int):void
		{
			Game.instance.music.fadeAllTo(0, 1500, false);
			Game.instance.fade(0xffffffff, 2.0, Game.instance.loadLevel, [level, mode]);
		}
		
	}

}