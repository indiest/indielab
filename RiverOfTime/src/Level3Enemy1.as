package  
{
	import flash.geom.Point;
	import nape.geom.Vec2;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Level3Enemy1 extends Enemy 
	{
		private var _targetPos:Point;
		
		public function Level3Enemy1() 
		{
			super(null, Assets.LEVELS_3_ENEMY1);
		}
		
		override public function update(deltaSecond:Number, target:Vec2):void 
		{
			if (_targetPos == null && target != null)
				_targetPos = new Point(target.x, target.y);
			super.update(deltaSecond, target);
		}
		
		override public function move(deltaSecond:Number):void 
		{
			if (_targetPos == null)
				super.move(deltaSecond);
			else
				setTo(x + (_targetPos.x - x) * deltaSecond * 1.5, y + speedY * deltaSecond);
		}
		
		override public function removeFromParent():void 
		{
			super.removeFromParent();
			_targetPos = null;
		}
	}

}