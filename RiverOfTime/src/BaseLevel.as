package  
{
	import com.gamua.flox.Flox;
	import com.gamua.flox.Score;
	import com.gamua.flox.TimeScope;
	import com.greensock.core.Animation;
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.CooldownManager;
	import me.sangtian.common.Direction;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.Input;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.state.IState;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	import me.sangtian.common.util.StringUtil;
	import nape.geom.Vec2;
	import nape.space.Space;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.SwipeGesture;
	import org.gestouch.gestures.SwipeGestureDirection;
	
	/**
	 * ...
	 * @author 
	 */
	public class BaseLevel extends Sprite implements IState, IPhaseScriptHost
	{
		public static const MODE_NORMAL:int = 0;
		public static const MODE_ENDLESS:int = 1;
		
		public static const CD_ID_SLOW:int = 1;
		public static const CD_ID_LEAP:int = 2;
		public static const CD_ID_BLINK:int = 3;
		public static const CD_ID_INVISIBLE:int = 4;
		public static const CD_ID_SHIELD_LASER:int = 5;
		public static const CD_ID_SHIELD_PHOTON:int = 6;
		public static const CD_ID_SHIELD_TACHYON:int = 7;
		
		public static const CD_SLOW:Number = 5.0;
		public static const CD_LEAP:Number = 7.0;
		public static const CD_BLINK:Number = 8.0;
		public static const CD_INVISIBLE:Number = 10.0;
		public static const CD_SHIELD:Number = 1.0;
		
		protected var _space:Space;
		
		public function get space():Space 
		{
			return _space;
		}
		
		protected var _raft:Raft;
		
		private var _isTransition:Boolean;
		protected function get isTransition():Boolean
		{
			return _isTransition;
		}
		private var _progress:Number;
		
		public var level:int;
		public var deaths:int;
		public var surviveTime:Number;
		public var playTime:Number;
		public var difficulty:Number;
		public var autoDifficulty:Boolean = true;
		public var mode:int = MODE_NORMAL;
		// Endless Mode properties
		private static var _upgradeData:Array = JSON.parse(new Assets.DATA_UPGRADE) as Array;
		public function getUpgradeInfo(type:int, level:int):Object
		{
			return _upgradeData[type][level];
		}
		public var orbs:int = 0;
		public var upgradeLevels:Vector.<int> = Vector.<int>([0, 0, 0, 0]);
		//public var skillLevel:int = 0;
		//public var slowLevel:int = 0;
		//public var hpRegenLevel:int = 0;
		//public var speedLevel:int = 0;
		
		private var _orbButton:OrbButton;
		protected var _orbsLayer:Sprite = new Sprite();
		protected var _uiLayer:Sprite = new Sprite();
		protected var _clock:Clock;
		private var _pauseMenu:PauseMenu;
		private var _upgradePanel:UpgradePanel;
		private var _gameOverPanel:GameOverPanel;
		private var _textTip:TextField = new TextField();
		
		public function get progress():Number 
		{
			return _progress;
		}
		
		public function set progress(value:Number):void 
		{
			_progress = value;
			if (mode == MODE_NORMAL)
			{
				_clock.setProgress(value);
			}
			else
			{
				_clock.setProgress(0);
				_clock.setTime(surviveTime);
			}
		}
		
		public function get lastEnemyProgress():Number
		{
			if (mode == MODE_NORMAL)
				return 0.9;
			else
				return Number.MAX_VALUE;
		}
		
		private var _lastFrameProgress:Number;
		public function justPassedProgress(value:Number):Boolean
		{
			return (value >= _lastFrameProgress && value < progress);
		}
		
		public function get leftBoundary():Number
		{
			return 0;
		}
		
		public function get rightBoundary():Number
		{
			return Game.instance.stageWidth;
		}
		
		public function get scrollY():Number
		{
			return 0;
		}
		
		protected var _music:MusicPlayback = new MusicPlayback();
		public function get music():MusicPlayback 
		{
			return _music;
		}
		
		protected var _cdm:CooldownManager = new CooldownManager();

		public function BaseLevel(level:int) 
		{
			this.level = level;
			
			_space = new Space(Vec2.weak());
			_space.worldLinearDrag = 20;// 14;
			
			_raft = new Raft();
			_raft.body.space = _space;
			
			_cdm.add(CD_ID_SLOW, CD_SLOW);
			_cdm.add(CD_ID_LEAP, CD_LEAP);
			_cdm.add(CD_ID_BLINK, CD_BLINK);
			_cdm.add(CD_ID_INVISIBLE, CD_INVISIBLE);
			_cdm.add(CD_ID_SHIELD_LASER, CD_SHIELD);
			_cdm.add(CD_ID_SHIELD_PHOTON, CD_SHIELD);
			_cdm.add(CD_ID_SHIELD_TACHYON, CD_SHIELD);
			
			_clock = new Clock();
			_clock.x = Game.instance.stageWidth - 32;
			_clock.y = 32;
			_clock.addEventListener(MouseEvent.CLICK, handleClickClock);
			_uiLayer.addChild(_clock);
			
			_pauseMenu = new PauseMenu();
			_pauseMenu.buttonResume.addEventListener(MouseEvent.CLICK, resume);
			_pauseMenu.buttonReturn.addEventListener(MouseEvent.CLICK, back);
			_pauseMenu.buttonSoundOn.addEventListener(MouseEvent.CLICK, switchSound);
			_pauseMenu.buttonSoundOff.addEventListener(MouseEvent.CLICK, switchSound);
			
			_orbButton = new OrbButton();
			_orbButton.x = 32;
			_orbButton.y = 32;
			_orbButton.addEventListener(MouseEvent.CLICK, handleClickOrbButton);
			_uiLayer.addChild(_orbButton);
			
			_textTip.width = 160;//Game.instance.stageWidth;
			_textTip.height = 64;
			_textTip.x = 100;
			_textTip.y = -_textTip.height;//Game.instance.stageHeight;
			_textTip.embedFonts = true;
			var format:TextFormat = _textTip.defaultTextFormat;
			format.align = "center";
			//var font:Font = new FontBarkentina();
			format.font = "Barkentina 1";// "FontBarkentina";
			format.size = 18;
			_textTip.defaultTextFormat = format;
			_textTip.multiline = true;
			_textTip.wordWrap = true;
			_textTip.selectable = false;
			_textTip.visible = false;
			_uiLayer.addChild(_textTip);
		}
		
		protected function showTip(text:String, textColor:uint = 0xffffff, delay:Number = 0):void
		{
			_textTip.text = text;
			_textTip.textColor = textColor;
			_textTip.visible = true;
			TweenLite.to(_textTip, 1.0, { delay: delay, y: 32 } );
			TweenLite.to(_textTip, 1.0, { delay: delay + 3, y: - _textTip.height, visible: false } );
		}
		
		private function showGameOverPanel():void
		{
			if (_gameOverPanel == null)
			{
				_gameOverPanel = new GameOverPanel();
				_gameOverPanel.buttonLeaderboard.addEventListener(MouseEvent.CLICK, handleClickLeaderboard);
				_gameOverPanel.buttonUpgrade.addEventListener(MouseEvent.CLICK, showUpgradePanel);
				_gameOverPanel.buttonTryAgain.addEventListener(MouseEvent.CLICK, handleTryAgain);
			}
			
			DisplayObjectUtil.formatText(_gameOverPanel.textTime, surviveTime);
			var bestTime:Number = Game.instance.player.getBestTime(level);
			if (surviveTime > bestTime)
			{
				Game.instance.player.setBestTime(level, surviveTime);
				_gameOverPanel.textNewRecord.visible = true;
			}
			else
			{
				_gameOverPanel.textNewRecord.visible = false;
			}
			DisplayObjectUtil.formatText(_gameOverPanel.textOrbs, orbs);
			_gameOverPanel.textLoading.visible = true;
			_gameOverPanel.textRanking.visible = false;
			_gameOverPanel.buttonLeaderboard.visible = false;
			Flox.postScore("endlesslevel" + level, int(surviveTime * 10), Game.instance.player.name);
			Flox.loadScores("endlesslevel" + level, TimeScope.ALL_TIME, onLeaderboardLoaded, onLeaderboardError);
			
			Game.instance.addChild(_gameOverPanel);
		}
		
		private function onLeaderboardError(error:String):void 
		{
			CONFIG::debug
			{
				_gameOverPanel.textLoading.text = error;
			}
		}
		
		private var _ranking:int;
		private function onLeaderboardLoaded(scores:Array):void 
		{
			_gameOverPanel.textLoading.visible = false;
			_gameOverPanel.textRanking.visible = true;
			_gameOverPanel.buttonLeaderboard.visible = true;
			var myScore:int = int(surviveTime * 10);
			// TODO: Ranking by time period
			//var rankingToday:int;
			//var rankingWeek:int;
			//var rankingAllTime:int;
			_ranking = scores.length - 1;
			for (var i:int = 0; i < scores.length; i++)
			{
				var score:Score = scores[i];
				if (myScore > score.value)
				{
					_ranking = i;
					break;
				}
			}
			DisplayObjectUtil.formatText(_gameOverPanel.textRanking, _ranking + 1);
			Game.instance.endlessScores[level - 1] = Vector.<Score>(scores);
		}
		
		private function handleClickLeaderboard(e:MouseEvent = null):void 
		{
			Game.instance.showLeaderboard(level, _ranking);
		}
		
		private function handleTryAgain(e:MouseEvent):void 
		{
			var logProperties:Object = { Orbs: orbs };
			for (var i:int = 0; i < upgradeLevels.length; i++)
			{
				logProperties["Upgrade" + i] = upgradeLevels[i].toString();
			}
			Flox.logEvent("TryAgain", logProperties);
			DisplayObjectUtil.removeFromParent(_gameOverPanel);
			enterScene();
		}
		
		private function handleClickOrbButton(e:MouseEvent = null):void 
		{
			pause();
			DisplayObjectUtil.removeFromParent(_pauseMenu);
			showUpgradePanel();
		}
		
		private function showUpgradePanel(e:MouseEvent = null):void
		{
			var i:int;
			var item:UpgradeItem;
			if (_upgradePanel == null)
			{
				_upgradePanel = new UpgradePanel();
				var icon:DisplayObject = null;
				for (i = 0; i < upgradeLevels.length; i++)
				{
					item = _upgradePanel["item" + i];
					if (i == 0)
						icon = getSkillUpgradeIcon1();
					else if (i == 1)
						icon = AssetsCache.getNewBitmap(Assets.UI_ICON_SLOW);
					else if (i == 2)
						icon = AssetsCache.getNewBitmap(Assets.UI_ICON_SPEED);
					else if (i == 3)
						icon = AssetsCache.getNewBitmap(Assets.UI_ICON_HP);
					icon.width = item.selected.width;
					icon.height = item.selected.height;
					item.addChildAt(DisplayObjectUtil.pivot(icon), 0);
					item.addEventListener(MouseEvent.CLICK, handleSelectUpgradeItem);
				}
				_upgradePanel.buttonUpgrade.addEventListener(MouseEvent.CLICK, handleUpgrade);
				_upgradePanel.buttonClose.addEventListener(MouseEvent.CLICK, handleCloseUpgradePanel);
			}
			DisplayObjectUtil.formatText(_upgradePanel.textOrbs, orbs);
			for (i = 0; i < upgradeLevels.length; i++)
			{
				item = _upgradePanel["item" + i];
				item.selected.visible = false;
				item.textLevel.text = upgradeLevels[i].toString();
			}
			_upgradePanel.textHelp.visible = true;
			_upgradePanel.textItemInfo.visible = false;
			_upgradePanel.buttonUpgrade.visible = false;
			Game.instance.addChild(_upgradePanel);
		}
		
		private var _upgradeSelectedIndex:int;
		private function handleSelectUpgradeItem(e:MouseEvent):void 
		{
			var item:UpgradeItem = _upgradePanel["item" + _upgradeSelectedIndex];
			item.selected.visible = false;
			item = e.currentTarget as UpgradeItem;
			item.selected.visible = true;
			_upgradePanel.textHelp.visible = false;
			_upgradePanel.textItemInfo.visible = true;
			_upgradeSelectedIndex = StringUtil.getPostfixNumber(item.name);
			updateUpgradeItemInfo();
		}
		
		private function updateUpgradeItemInfo():void 
		{
			//const UPGRADE_NAMES:Vector.<String> = Vector.<String>(["Cooldown -", "Duration +", "Speed +", "Life Regen +"]);
			//const UPGRADE_UNITS:Vector.<String> = Vector.<String>(["sec", "sec", "%", "%"]);
			var currentLevel:int = upgradeLevels[_upgradeSelectedIndex];
			var upgradeInfo:Object = getUpgradeInfo(_upgradeSelectedIndex, currentLevel);
			DisplayObjectUtil.formatText(_upgradePanel.textItemInfo, upgradeInfo);
			_upgradePanel.buttonUpgrade.visible = (orbs >= upgradeInfo.orbs);
		}
		
		private function handleUpgrade(e:MouseEvent):void 
		{
			var currentLevel:int = upgradeLevels[_upgradeSelectedIndex];
			var upgradeInfo:Object = getUpgradeInfo(_upgradeSelectedIndex, currentLevel);
			orbs -= upgradeInfo.orbs;
			DisplayObjectUtil.formatText(_upgradePanel.textOrbs, orbs);
			performUpgrade(_upgradeSelectedIndex, currentLevel + 1);
			updateUpgradeItemInfo();
		}
		
		protected function performUpgrade(type:int, level:int):void
		{
			upgradeLevels[_upgradeSelectedIndex] = level;
			var upgradeInfo:Object = getUpgradeInfo(type, level);
			
			if (type == 0)
			{
				CONFIG::debug
				{
					Log.debug("Upgrade skill to level", level);
				}
				performUpgradeSkill(level, upgradeInfo.v0);
			}
			else if (type == 1)
			{
				var cooldown:Number = CD_SLOW * (1 - upgradeInfo.v0 / 100);
				CONFIG::debug
				{
					Log.debug("Upgrade Slow cooldown to", cooldown);
				}
				_cdm.setCooldown(CD_ID_SLOW, cooldown);
			}
			else if (type == 2)
			{
				var speed:Number = Raft.INIT_MAX_SPEED * (1 + upgradeInfo.v0 / 100);
				CONFIG::debug
				{
					Log.debug("Upgrade Speed to", speed);
				}
				_raft.maxSpeedX = _raft.maxSpeedY = speed;
			}
			else if (type == 3)
			{
				var lifeRegen:Number = Raft.INIT_LIFE_REGEN * (1 + upgradeInfo.v0 / 100);
				CONFIG::debug
				{
					Log.debug("Upgrade Life Regen to", lifeRegen);
				}
				_raft.lifeRegen = lifeRegen;
			}
		}
		
		protected function performUpgradeSkill(level:int, value:Number):void
		{
			throw new Error("Not implemented");
		}
		
		private function handleCloseUpgradePanel(e:MouseEvent):void 
		{
			DisplayObjectUtil.removeFromParent(_upgradePanel);
			resume();
		}
		
		private function handleClickClock(e:MouseEvent):void 
		{
			//if (!_isTransition)
				pause();
		}
		
		private var _paused:Boolean = false;
		
		public function pause(e:Event = null):void 
		{
			if (_paused)
				return;
			_paused = true;
			startTransition();
			// Pause all Tweens
			Animation._rootTimeline.pause();
			//Game.instance.music.pauseAll();
			_music.pause();
			Game.instance.sfx.pauseAll();
			
			if (_gameOverPanel == null || _gameOverPanel.parent == null)
			{
				DisplayObjectUtil.tint(DisplayObjectUtil.makeCover(this, Game.instance.stageRect), 0.5);
				_pauseMenu.buttonSoundOn.visible = Game.instance.soundOn;
				Game.instance.addChild(_pauseMenu);
			}
		}
		
		public function resume(e:Event = null):void
		{
			if (!_paused)
				return;
			_paused = false;
			DisplayObjectUtil.removeFromParent(_pauseMenu);
			DisplayObjectUtil.removeCover(this);
			finishTransition();
			Animation._rootTimeline.resume();
			//Game.instance.music.resumeAll();
			_music.resume();
			Game.instance.sfx.resumeAll();
		}
		
		protected function back(e:Event = null):void
		{
			resume();
			Game.instance.stateManager.loadState(MenuState);
		}
		
		protected function switchSound(e:Event = null):void
		{
			_pauseMenu.buttonSoundOn.visible = !_pauseMenu.buttonSoundOn.visible;
			Game.instance.soundOn = !Game.instance.soundOn;
			Flox.logEvent("SwitchSound", { On: Game.instance.soundOn, Source: "PauseMenu" } );
		}
		
		/* INTERFACE IPhaseScriptHost */
		
		public function compareProgress(nextProgress:Number):Number 
		{
			// In Endless Mode you never exceeds Last Enemy Progress
			if (mode == MODE_ENDLESS && progress >= lastEnemyProgress)
			{
				return -1;
			}
			return progress - nextProgress;
		}
		
		public function invokeMethod(methodName:String, args:Array):void
		{
			(this[methodName] as Function).apply(null, args);
		}
		
		/* INTERFACE me.sangtian.common.state.IState */
		
		public function get isTransientState():Boolean 
		{
			return false;
		}
		
		public function enter():void 
		{
			Flox.logEvent("EnterLevel", { Level: level.toString(), Mode: mode.toString() } );
			if (mode == MODE_NORMAL && level > Game.instance.player.level)
			{
				Game.instance.player.level = level;
				if (level > 1)
					showTip("Endless Mode Level " + (level - 1) + " Unlocked", 0xffffff, 5.0);
			}

			// Shaking will reveal the background, so set it to black 
			Game.instance.nativeStage.color = 0x000000;
			deaths = 0;
			playTime = 0;
			progress = 0;
			difficulty = 1.0;//100%
			
			enterScene();
			_music.mute = !Game.instance.soundOn;
			
			_orbButton.visible = (mode == MODE_ENDLESS);
			addChild(_orbsLayer);
			addChild(_uiLayer);
		}
		
		protected function enterScene():void
		{
			//_paused = false;
			resume();
			startTransition();
			
			surviveTime = 0;
			orbs = 0;

			_cdm.resetAll();
		}
		
		public function update():void 
		{
			if (_paused)
			{
				if (Game.instance.isKeyUp(Keyboard.P))
					resume();
				return;
			}
			
			_uiLayer.visible = !_isTransition;
			
			if (_isTransition)
				return;	
			
			// Play time is based on real world
			var realSecond:Number = Time.deltaSecond / Time.scale;
			playTime += realSecond;
			surviveTime += realSecond;
			
			// Space.step(0) will occur debug error (some vec2 will be NaN).
			if (Time.deltaSecond > 0)
			{
				_space.step(Time.deltaSecond);
			}
			
			updateProgress();
			if (mode == MODE_NORMAL && _progress >= 1)
			{
				finishLevel();
				return;
			}
			
			updateSkill();
			updateScene();
			updateOrbs();
			
			if (Game.instance.isKeyUp(Keyboard.P))
			{
				pause();
			}
			CONFIG::debug
			{
				if (Game.instance.isKeyUp(Keyboard.I))
				{
					// Switch between 1 and 0(invincible)
					_raft.damageFactor = Math.abs(_raft.damageFactor - 1);
					Log.debug("Switch Invincibility:", _raft.damageFactor == 0);
				}
				//if (Game.instance.isKeyUp(Keyboard.V))
				//{
					//_space.step(0);
				//}
			}
		}
		
		protected function updateProgress():void 
		{
			_lastFrameProgress = progress;
		}
		
		protected function updateScene():void 
		{
			
		}
		
		protected function generateOrb():void
		{	
			if (Random.randomBoolean(0.01))
			{
				var orb:StarlingSpriteSheet = Game.instance.orbs.add(
					Random.rangeNumber(leftBoundary, rightBoundary), 0, true, false);
				if (orb != null)
				{
					_orbsLayer.addChild(orb);
				}
			}
		}
		private var _orbButtonOrb:StarlingSpriteSheet;
		protected function updateOrbs():void 
		{
			if (mode == MODE_NORMAL)
				return;
			
			if (Game.instance.isKeyUp(Keyboard.ENTER))
			{
				handleClickOrbButton();
			}
			
			generateOrb();
			
			if (orbs > 0)
			{
				_orbButton.visible = true;
				_orbButton.textNum.text = orbs.toString();
				if (_orbButtonOrb == null)
				{
					_orbButtonOrb = Game.instance.orbs.add(0, 0, false, false);
					_orbButton.addChild(_orbButtonOrb);
				}
			}
			else
			{
				_orbButton.visible = false;
			}
			
			for (var i:int = _orbsLayer.numChildren - 1; i >= 0; i--)
			{
				var orb:StarlingSpriteSheet = _orbsLayer.getChildAt(i) as StarlingSpriteSheet;
				orb.y += scrollY;
				if (Game.instance.isOutOfScreen(orb, Direction.DOWN))
				{
					Game.instance.orbs.remove(orb);
				}
				else if (orb.hitTestObject(_raft))
				{
					_uiLayer.addChild(orb);
					TweenLite.to(orb, 1.5, { x: _orbButton.x, y: _orbButton.y,
						onComplete: Game.instance.orbs.remove, onCompleteParams: [orb] } );
					orbs++;
					Game.instance.sfx.playFx("orb" + Random.rangeInt(1, 7), 0.5);
				}
			}
		}
		
		protected function updateSkill():void
		{
			// Skill cooldowns are affected by time scale factor
			_cdm.update(Time.deltaSecond);// / Time.scale);
		}
		
		protected function onUseSkill(cdId:int):void
		{
			_cdm.startCooldown(cdId);
			Flox.logEvent("UseSkill", { Level: level.toString(), SkillID: cdId.toString() } );
		}
		
		public function startTransition():void 
		{
			_isTransition = true;
		}
		
		public function finishTransition():void 
		{
			_isTransition = false;
			_raft.body.position.setxy(_raft.x, _raft.y);
		}
		
		public function gameOver(cause:int = 0, restartProgress:Number = 0, fadeColor:uint = 0xff000000):void 
		{
			startTransition();
			
			deaths++;
			Flox.logEvent("GameOver", { Level: level.toString(), Mode: mode.toString(), Cause: cause.toString(), Deaths: deaths, Progress: this.progress, SurviveTime: surviveTime } );
			if (mode == MODE_NORMAL && autoDifficulty)
			{
				difficulty = MathUtil.clamp(difficulty - 0.05, 0.4, 1.0);
				CONFIG::debug
				{
					Log.debug("Difficulty changed to:", difficulty);
				}
			}
			
			Game.instance.fade(fadeColor, 1.0, restart, [restartProgress]);
			//Game.instance.music.fadeAllTo(0, 1000, false);
			_music.fadeOut(1.0);
			Game.instance.sfx.fadeAllTo(0, 1000, false);
		}
		
		protected function finishLevel():void 
		{
			startTransition();
			Flox.logEvent("FinishLevel", { Level: level.toString(), Deaths: deaths, PlayTime: playTime } );
			Game.instance.fade(0xffffffff, 2.0, showSkillUpgrade);
			//Game.instance.music.fadeAllTo(0, 1500, false);
			_music.fadeOut(1.5, 0, _music.stop);
			Game.instance.sfx.fadeAllTo(0, 1500, false);
		}
		
		protected function restart(progress:Number):void 
		{
			this.progress = progress;
			clear();
			if (mode == MODE_NORMAL)
			{
				enterScene();
			}
			else
			{
				//_paused = true;
				startTransition();
				showGameOverPanel();
			}
		}
		
		protected function clear():void 
		{
			Time.scale = 1;
			Game.instance.input.resetAll();
			//Input.reset();
			_raft.reset();
			DisplayObjectUtil.removeFromParent(_pauseMenu);
			DisplayObjectUtil.removeMovieClip(_tutorialSkillUpgrade);
			//Game.instance.music.stopAll();
			_music.stop();
			Game.instance.sfx.stopAll();
			
			Game.instance.orbs.reset();
			_orbButtonOrb = null;
			for (var i:int = 0; i < upgradeLevels.length; i++)
			{
				performUpgrade(i, 0);
			}
		}
		
		public function exit():void 
		{
			clear();
		}
		
		private static var _tutorialSkillUpgrade:TutorialSkillUpgrade;
		protected function showSkillUpgrade():void 
		{
			if (_tutorialSkillUpgrade == null)
			{
				_tutorialSkillUpgrade = new TutorialSkillUpgrade();
			}
			DisplayObjectUtil.formatText(_tutorialSkillUpgrade.textDesc,
				//getSkillUpgradeName1(),
				getSkillUpgradeName2());
			_tutorialSkillUpgrade.icon1Container.removeChildren();
			var icon1:DisplayObject = getSkillUpgradeIcon1();
			_tutorialSkillUpgrade.icon1Container.addChild(icon1);
			_tutorialSkillUpgrade.icon2Container.removeChildren();
			var icon2:DisplayObject = getSkillUpgradeIcon2();
			_tutorialSkillUpgrade.icon2Container.addChild(icon2);
			for (var i:int = 1; i < 4; i++)
			{
				var textQuote:TextField = _tutorialSkillUpgrade["textQuote" + i];
				textQuote.visible = (i == level);
			}
			DisplayObjectUtil.playMovieClip(_tutorialSkillUpgrade, this, false, null, onSkillUpgradeTutorialFrame);
			Game.instance.fade(0x00ffffff);
		}
		
		private function onSkillUpgradeTutorialFrame(mc:*):void 
		{
			if (_tutorialSkillUpgrade.currentFrameLabel == "playSound")
			{
				Game.instance.sfx.play("upgrade1");
			}
			else if (_tutorialSkillUpgrade.currentFrameLabel == "fadeOut")
			{
				Game.instance.fade(0xffffffff, 1.0, nextLevel);
			}
		}
		
		protected function getSkillUpgradeName1():String
		{
			throw new Error("Not implemented");
		}
		
		protected function getSkillUpgradeName2():String
		{
			throw new Error("Not implemented");
		}
		
		protected function getSkillUpgradeIcon1():DisplayObject
		{
			throw new Error("Not implemented");
		}
		
		protected function getSkillUpgradeIcon2():DisplayObject
		{
			throw new Error("Not implemented");
		}
		
		protected function nextLevel():void 
		{
			Game.instance.loadLevel(level + 1);
		}
		
	}

}