package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import me.sangtian.common.util.DisplayObjectUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Clock extends Sprite 
	{
		private var _mc:ClockButton = new ClockButton();
		
		public function Clock() 
		{
			_mc.buttonMode = true;
			_mc.textTime.autoSize = TextFieldAutoSize.CENTER;
			addChild(_mc);
		}
		
		public function setProgress(progress:Number):void
		{
			//_hand.visible = true;
			_mc.textTime.visible = false;
			_mc.hand.rotation = progress * 360;
		}
		
		public function setTime(seconds:Number):void
		{
			//_hand.visible = false;
			_mc.textTime.visible = true;
			_mc.textTime.text = seconds.toFixed(1);
		}
	}

}