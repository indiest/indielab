package  
{
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.ProcedureProfiler;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class PhaseScript 
	{
		private var _host:IPhaseScriptHost;
		private var _phases:Array = [];
		private var _currentPhaseIndex:int;
		private var _nextPhaseProgress:Number;
		
		public var logEnabled:Boolean = false;
		
		public function get currentPhase():Object
		{
			if (_currentPhaseIndex < 0 || _currentPhaseIndex >= _phases.length)
				return null;
			return _phases[_currentPhaseIndex];
		}
		
		public function PhaseScript(host:IPhaseScriptHost) 
		{
			_host = host;
		}
		
		public function addPhase(progress:Number, onEnter:Function = null, onUpdate:Function = null, onExit:Function = null):void
		{
			_phases.push( { progress: progress, onEnter: onEnter, onUpdate: onUpdate, onExit: onExit } );
		}
		
		public function parse(data:String):void
		{
			_phases = JSON.parse(data).phases;
			reset();
		}
		
		public function reset():void
		{
			//_phases.sortOn("progress", Array.NUMERIC);
			_currentPhaseIndex = -1;
			if (_phases.length > 0)
				_nextPhaseProgress = _phases[0].progress;
			else
				_nextPhaseProgress = Number.MAX_VALUE;
		}
		
		CONFIG::debug
		{
		private var _profiler:ProcedureProfiler = new ProcedureProfiler();
		}
		
		public function update():void
		{
			CONFIG::debug
			{
				_profiler.startTiming();
			}

			var nextPhase:Object = _phases[_currentPhaseIndex + 1];
			if (nextPhase != null && _host.compareProgress(nextPhase.progress) >= 0)
			{
				onExitPhase();
				_currentPhaseIndex++;
				CONFIG::debug
				{
					if (logEnabled)
						Log.debug("[Script] Enter phase", _currentPhaseIndex);
				}
				onEnterPhase();
				_nextPhaseProgress = nextPhase.progress;
			}
			
			onUpdatePhase();

			CONFIG::debug
			{
				_profiler.countTimes();
				var ms:Number = _profiler.getAverageMillisecond();
				if (ms > 1 && logEnabled)
					Log.debug("[Script] Update average time:", ms);
			}
		}
		
		private function invokeMethods(methods:Array):void 
		{
			for each(var methodObj:Array in methods)
			{
				var methodName:String = methodObj[0];
				CONFIG::debug
				{
					if (logEnabled)
						Log.debug("[Script] Invoke method:", methodObj);
				}
				//try
				//{
					_host.invokeMethod(methodName, methodObj.slice(1));
				//}
				//catch (err:Error)
				//{
					//if (Log.isErrorEnabled)
						//Log.error(err);
				//}
			}
		}
		
		protected function onUpdatePhase():void 
		{
			if (currentPhase == null)
				return;
			if (currentPhase.onUpdate is Function)
				currentPhase.onUpdate();
			else
				invokeMethods(currentPhase.onUpdate);
		}
		
		protected function onEnterPhase():void 
		{
			if (currentPhase == null)
				return;
			if (currentPhase.onEnter is Function)
				currentPhase.onEnter();
			else
				invokeMethods(currentPhase.onEnter);
		}
		
		protected function onExitPhase():void 
		{
			if (currentPhase == null)
				return;
			if (currentPhase.onExit is Function)
				currentPhase.onExit();
			else
				invokeMethods(currentPhase.onExit);
		}
		
	}

}