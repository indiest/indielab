package  
{
	import com.gamua.flox.Flox;
	import com.greensock.easing.Expo;
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.filters.BlurFilter;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.Direction;
	import me.sangtian.common.display.DualImageWrappingScroller;
	import me.sangtian.common.display.ShadowEffect;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.Input;
	import me.sangtian.common.nape.NapeUtil;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.common.ProcedureProfiler;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.Material;
	import treefortress.sound.SoundInstance;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Level3 extends BaseLevel
	{
		private static const CB_TYPE_ENEMY1:CbType = new CbType();
		
		private var _bg:Bitmap;
		private var _bgCloud1Scroller:DualImageWrappingScroller;
		private var _bgCloud2Scroller:DualImageWrappingScroller;
		
		private var _shadowContainer:Sprite = new Sprite();
		private var _shadowEffect:ShadowEffect;
		
		private var _bulletContainers:Vector.<PhysicalSpriteContainer> = new Vector.<PhysicalSpriteContainer>(4, true);
		private var _enemyContainers:Vector.<PhysicalSpriteContainer> = new Vector.<PhysicalSpriteContainer>(4, true);
		
		private var _enemyScript:PhaseScript;
		
		private var _explosions:GfxContainer;
		
		private var _iconInvisible:SkillIcon;
		private var _iconSlow:SkillIcon;
		
		private var _invisibleTiming:Number = 0;
		
		private static var _barrierFilter:InteractionFilter = new InteractionFilter(1);
		private static var _enemyFilter:InteractionFilter = new InteractionFilter(2);
		private static var _raftFilter:InteractionFilter = new InteractionFilter(1 | 2);
		private static var _invisibleFilter:InteractionFilter = new InteractionFilter(1, 1);
		
		override public function get scrollY():Number
		{
			return 70 * Time.deltaSecond;
		}

		public function Level3() 
		{
			super(3);
			
			_bg = new Assets.LEVELS_3_BG();
			addChild(_bg);
			_bgCloud1Scroller = new DualImageWrappingScroller(new Assets.LEVELS_3_BG_CLOUD1);
			addChild(_bgCloud1Scroller);
			_bgCloud2Scroller = new DualImageWrappingScroller(new Assets.LEVELS_3_BG_CLOUD2);
			addChild(_bgCloud2Scroller);
			
			_space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, Raft.CB_TYPE, CB_TYPE_ENEMY1, handleCollision));
			
			var barriers:Body = NapeUtil.makeBarriers(Game.instance.stageWidth, Game.instance.stageHeight,
				Direction.LEFT | Direction.RIGHT | Direction.UP);
			barriers.setShapeMaterials(Material.glass());
			barriers.space = _space;
			barriers.setShapeFilters(_barrierFilter);
			
			addChild(_shadowContainer);
			// Tint shadows to gold
			DisplayObjectUtil.tint(_shadowContainer, 0.5, 0x888840);
			_shadowContainer.alpha = 0.5;
			_shadowEffect = new ShadowEffect(_shadowContainer, 20);
			
			_raft.scale = 0.65;
			addChild(_raft);
			
			_bulletContainers[0] = new PhysicalSpriteContainer(_space, 36, Bullet.getProvider(Assets.LEVELS_3_BULLET0), updateBullet, onAddBullet);
			_bulletContainers[3] = new PhysicalSpriteContainer(_space, 12, Bullet.getProvider(Assets.LEVELS_3_BULLET3), updateBullet, onAddMissile);
			
			// Air blocker
			_enemyContainers[2] = new PhysicalSpriteContainer(_space, 2, createEnemy2, updateEnemy, onAddEnemy2, onRemoveEnemy2);
			addChild(_enemyContainers[2]);
			
			// Bullet shooter
			_enemyContainers[0] = new PhysicalSpriteContainer(_space, 4, createEnemy0, updateEnemy);
			addChild(_bulletContainers[0]);
			addChild(_enemyContainers[0]);
			
			// Suisider
			_enemyContainers[1] = new PhysicalSpriteContainer(_space, 10, createEnemy1, updateEnemy, onAddSuicider);
			addChild(_enemyContainers[1]);
			
			// Missle launcher
			_enemyContainers[3] = new PhysicalSpriteContainer(_space, 3, createEnemy3, updateEnemy3);
			addChild(_bulletContainers[3]);
			addChild(_enemyContainers[3]);
			
			_explosions = new GfxContainer(Assets.ANIMATIONS_EXPLOSION, Assets.ANIMATIONS_EXPLOSION_DATA, 10);
			addChild(_explosions);

			_enemyScript = new PhaseScript(this);
			_enemyScript.parse(new Assets.LEVELS_3_ENEMY_SCRIPT);
			
			_iconInvisible = new SkillIcon(AssetsCache.getNewBitmap(Assets.UI_ICON_INVISIBLE), CD_ID_INVISIBLE, _cdm, Game.ACTION_ID_SKILL_A);
			_iconInvisible.image.width = _iconInvisible.image.height = 48;
			_iconInvisible.x = 48;
			_iconInvisible.y = Game.instance.stageHeight - 48;
			_uiLayer.addChild(_iconInvisible);
			_iconSlow = new SkillIcon(AssetsCache.getNewBitmap(Assets.UI_ICON_SLOW), CD_ID_SLOW, _cdm, Game.ACTION_ID_SKILL_B);
			_iconSlow.x = Game.instance.stageWidth - 48;
			_iconSlow.y = Game.instance.stageHeight - 48;
			_uiLayer.addChild(_iconSlow);
		}
		
		private function createEnemy0():Enemy 
		{
			var enemy:Enemy = new Enemy(_bulletContainers[0], Assets.LEVELS_3_ENEMY0);
			enemy.speedX = 0;
			enemy.speedY = 150;
			enemy.attackInterval = 1.5;
			enemy.attackRange = 600;
			enemy.bulletDamage = 0.3;
			enemy.bulletSpeed = 400;
			enemy.biasRatio = 0;
			enemy.body.setShapeFilters(_enemyFilter);
			return enemy;
		}
		
		private function createEnemy1():Enemy 
		{
			var enemy:Enemy = new Level3Enemy1();// Enemy(null, Assets.LEVELS_3_ENEMY1);
			enemy.speedX = 0;
			enemy.speedY = 400;
			enemy.attackInterval = 0;
			enemy.attackRange = 0;
			enemy.bulletDamage = 0.9;// Hit damage
			enemy.bulletSpeed = 0;
			enemy.biasRatio = 0;
			enemy.body.cbTypes.add(CB_TYPE_ENEMY1);
			enemy.body.setShapeFilters(_enemyFilter);
			return enemy;
		}
		
		private function onAddSuicider(enemy:Enemy):void
		{
			enemy.body.userData.sfx = Game.instance.sfx.playFx("aircraft_whoosh");
		}
		
		private function createEnemy2():Enemy 
		{
			var enemy:Enemy = new Enemy(null, Assets.LEVELS_3_ENEMY2);
			enemy.speedX = 0;
			enemy.speedY = 200;
			enemy.attackInterval = 0;
			enemy.attackRange = 300;
			enemy.bulletDamage = 0;
			enemy.bulletSpeed = 0;
			enemy.biasRatio = 0;
			enemy.body.setShapeFilters(_enemyFilter);
			return enemy;
		}
		
		private function onAddEnemy2(enemy:Enemy):void
		{
			//Game.instance.sfx.playFx("flying_plane").fadeFrom(0, 0.6);
		}
		
		private function onRemoveEnemy2(enemy:Enemy):void
		{
			//Game.instance.sfx.fadeTo("flying_plane", 0);
		}
		
		private function createEnemy3():Enemy 
		{
			var enemy:Enemy = new Enemy(_bulletContainers[3], Assets.LEVELS_3_ENEMY3);
			enemy.speedX = 0;
			enemy.speedY = 150;
			enemy.attackInterval = 0.7;
			enemy.attackRange = 800;
			enemy.bulletDamage = 1.0;
			enemy.bulletSpeed = 300;
			enemy.biasRatio = 0;
			enemy.body.setShapeFilters(_enemyFilter);
			return enemy;
		}
		
		private function onAddBullet(bullet:PhysicalSprite):void 
		{
			Game.instance.sfx.playFx("bullet");
		}
		
		private function onAddMissile(missile:PhysicalSprite):void 
		{
			Game.instance.sfx.playFx("missile");
		}
		
		private function handleCollision(cb:InteractionCallback):void 
		{
			var enemy:Enemy = _enemyContainers[1].findFromBody(cb.int2.castBody) as Enemy;
			enemy.removeFromParent();
			
			// Stop flying sfx
			var s:SoundInstance = enemy.body.userData.sfx;
			s.stop();
			enemy.body.userData.sfx = null;
			
			_explosions.add(enemy.x, enemy.y);
			Game.instance.sfx.playFx("explosion", 0.3);
			
			_raft.damage(enemy.bulletDamage, 1);
			Game.instance.flash(0xffff0000, 0.2, 5);
		}
		
		private function updateBullet(bullet:Bullet):void 
		{
			bullet.update(Time.deltaSecond);
			if (!bullet.isInSpace)
				return;
			if (_raft.alpha == 1 && _raft.hitTestObject(bullet))
			{
				bullet.removeFromParent();
				_raft.damage(bullet.damage, 0);
				Game.instance.flash(0xffff0000, 0.2, 5);
			}
		}
		
		private function updateEnemy(enemy:Enemy):void 
		{
			enemy.update(Time.deltaSecond, _raft.alpha < 1 ? null : _raft.body.position);
		}
/*		
		private function updateEnemy1(enemy:Enemy):void 
		{
			// Chase and hit raft
			var t:Number = Math.abs(_raft.y - enemy.y) / enemy.speedY;
			enemy.speedX = MathUtil.clamp((_raft.x - enemy.x) / t, -100, 100);
			enemy.update(Time.deltaSecond, _raft.x, _raft.y);
		}
		
*/		private function updateEnemy3(enemy:Enemy):void 
		{
			// Alway fire down
			enemy.update(Time.deltaSecond, Vec2.weak(enemy.x, Game.instance.stageHeight + enemy.height));
		}
		
		override protected function updateSkill():void 
		{
			super.updateSkill();
			
			_iconInvisible.update();
			_iconSlow.update();
			
			// Swipe up or press SPACE to activate
			if (//(_iconInvisible.isPressed || _swipeOffsetY <= -1 || Input.getKeyDown(Keyboard.SPACE))
				Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_A)
				&& _cdm.isCooledDown(CD_ID_INVISIBLE))
			{
				//_raft.collisionEnabled = false;
				_raft.body.setShapeFilters(_invisibleFilter);
				TweenLite.to(_raft, 0.5, { alpha: 0.5 } );
				_invisibleTiming = 1.0;
				onUseSkill(CD_ID_INVISIBLE);
				Game.instance.sfx.play("invisible", 0.5);
			}
			
			if (Time.scale < 1)
			{
				Time.scale += 0.006;
			}
			else if (//(_iconSlow.isPressed || _swipeOffsetY >= 1 || Input.getKeyDown(Keyboard.SHIFT))
				Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_B)
				&& _cdm.isCooledDown(CD_ID_SLOW))
			{
				Time.scale = 0.2;
				onUseSkill(CD_ID_SLOW);
			}
			
		}
		
		private function finishInvisible():void 
		{
			_invisibleTiming = 0;
			//_raft.collisionEnabled = true;
			_raft.body.setShapeFilters(_raftFilter);
			_raft.alpha = 1;
		}
		
		override protected function updateScene():void 
		{
			_bgCloud1Scroller.scrollY += scrollY * 0.5;
			_bgCloud2Scroller.scrollY += scrollY;
			
			_raft.update(Time.deltaSecond);
			if (_invisibleTiming > 0)
			{
				_invisibleTiming -= Time.deltaSecond;
				if (_invisibleTiming <= 0)
				{
					Game.instance.flicker(_raft, 1.0, 0.2, finishInvisible);
				}
			}
			
			for each(var bulletContainer:PhysicalSpriteContainer in _bulletContainers)
			{
				if (bulletContainer != null)
					bulletContainer.update();
			}
			
			for each(var enemyContainer:PhysicalSpriteContainer in _enemyContainers)
			{
				if (enemyContainer != null)
					enemyContainer.update();
			}
			
			_explosions.update();
			
			_enemyScript.update();
			
			if (justPassedProgress(0.98))
			{
				startTransition();
				TweenLite.to(_raft, 2.0, { x: 180, y:400, onComplete: leaveRaft } );
			}
		}
		
		private function leaveRaft():void
		{
			/* Blur the screen except the raft
			removeChild(_raft);
			var world:Bitmap = DisplayObjectUtil.makeCover(this);
			world.bitmapData.applyFilter(world.bitmapData, world.bitmapData.rect,
				MathUtil.EMPTY_POINT, new BlurFilter());
			addChild(_raft);
			*/
			TweenLite.to(_raft, 1.0, { y: -100, ease: Expo.easeIn ,onComplete: finishLevel } );
			_shadowEffect.startTracing(_raft, 10, 0.3);
		}
		
		override public function update():void 
		{
			super.update();
			// Always update shadows and raft's animations
			_shadowEffect.update(Time.deltaSecond);
			_raft.animation.updateFrame(Time.deltaSecond);
		}
		
		// Hosted for script, must be public
		public function addEnemy(type:int, x:Number, y:Number):void
		{
			_enemyContainers[type].add(x, y);
		}
		
		public function addEnemyRandom(type:int, chance:Number, progressFactor:Number = 0):void
		{
			if (Random.randomBoolean(chance * difficulty * (1 + progress * progressFactor)))
			{
				var enemy:Enemy = _enemyContainers[type].add() as Enemy;
				if (enemy != null)
				{
					enemy.setTo(30 + Random.rangeInt(0, 6) * 60, -enemy.height >> 1);
				}
			}
		}
		
		
		override protected function updateProgress():void 
		{
			super.updateProgress();
			// 77 seconds
			progress += (0.013 * Time.deltaSecond);
		}
		
		override protected function enterScene():void 
		{
			super.enterScene();
			_enemyScript.reset();

			_raft.x = 180;
			_raft.y = 700;
			//progress = 0.95;
			finishInvisible();
			_iconInvisible.visible = _iconSlow.visible = false;
			Game.instance.fade(0x00ffffff, 1.0, enterRaft);
			
			if (mode == MODE_NORMAL)
			{
				//Game.instance.music.play("Level3");
				_music.play(Game.instance.music.getSound("Level3").sound);
			}
			else
			{
				//Game.instance.music.playLoop("wind").fadeFrom(0, 0.5, 3000);
				_music.playLoop(Game.instance.music.getSound("wind").sound, 0.5);
				_music.fadeIn(3.0);
			}
		}
		
		private function enterRaft():void 
		{
			TweenLite.to(_raft, 3.0, { y: 400, onComplete: finishTransition } );
		}
		
		override protected function clear():void 
		{
			super.clear();
			_shadowEffect.stopTracing(true);
			for each(var bulletContainer:PhysicalSpriteContainer in _bulletContainers)
			{
				if (bulletContainer != null)
					bulletContainer.clear();
			}
			
			for each(var enemyContainer:PhysicalSpriteContainer in _enemyContainers)
			{
				if (enemyContainer != null)
					enemyContainer.clear();
			}
			_bgCloud1Scroller.reset();
			_bgCloud2Scroller.reset();
			_explosions.clear();
	}
		
		override protected function getSkillUpgradeName1():String 
		{
			return "Invisibility";
		}
		
		override protected function getSkillUpgradeName2():String 
		{
			return "Force Shields";
		}
		
		override protected function getSkillUpgradeIcon1():DisplayObject 
		{
			var icon:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_INVISIBLE);
			icon.width = icon.height = 48;
			return icon;
		}
		
		override protected function getSkillUpgradeIcon2():DisplayObject 
		{
			var icons:Sprite = new Sprite();
			var icon0:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_SHIELD_0);
			icon0.width = icon0.height = 48;
			icons.addChild(DisplayObjectUtil.pivot(icon0, -2, 0));
			var icon1:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_SHIELD_1);
			icon1.width = icon1.height = 48;
			icons.addChild(DisplayObjectUtil.pivot(icon1, 0, 0));
			var icon2:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_SHIELD_2);
			icon2.width = icon2.height = 48;
			icons.addChild(DisplayObjectUtil.pivot(icon2, 2, 0));
			return icons;
		}
		
		override protected function performUpgradeSkill(level:int, value:Number):void
		{
			_cdm.setCooldown(CD_ID_INVISIBLE, CD_INVISIBLE * (1 - value / 100));
		}
	}

}