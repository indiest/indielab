package  
{
	import com.greensock.easing.Bounce;
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import me.sangtian.common.CooldownManager;
	import me.sangtian.common.Input;
	import me.sangtian.common.input.GestouchController;
	import me.sangtian.common.input.KeyboardController;
	import me.sangtian.common.input.KeyboardControllerKey;
	import me.sangtian.common.util.DisplayObjectUtil;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class SkillIcon extends Sprite 
	{
		private var _image:Bitmap;
		private var _tapGesture:TapGesture;
		private var _cdm:CooldownManager;
		private var _actionId:String;
		private var _id:int;
		
		public function SkillIcon(image:Bitmap, id:int, cdm:CooldownManager, actionId:String) 
		{
			_image = image;
			_image.width = _image.height = 48;
			addChild(DisplayObjectUtil.pivot(_image));
			this.buttonMode = true;
			_id = id;
			_cdm = cdm;
			_actionId = actionId;
			if (Game.instance.isMobile)
			{
				_tapGesture = new TapGesture(this);
				_tapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, handleMouseDown);
				//addEventListener(TouchEvent.TOUCH_BEGIN, handleMouseDown);
				//addEventListener(TouchEvent.TOUCH_END, handleMouseUp);
				//addEventListener(TouchEvent.TOUCH_OUT, handleMouseUp);
				//addEventListener(TouchEvent.TOUCH_ROLL_OUT, handleMouseUp);
			}
			else
			{
				addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			}
		}
		
		private var _pressed:Boolean = false;
		
		private function handleMouseDown(e:Event):void 
		{
			_pressed = true;
			Game.instance.input.getAction(_actionId).hold();
		}
		
		private function handleMouseUp(e:Event):void 
		{
			_pressed = false;
		}
		
		public function get isPressed():Boolean
		{
//			if (visible)
//			{
//				if (Game.instance.isMobile)
				{
					return _pressed;
				}
//				else
//				{
//					return Input.mouseButtonDown && hitTestPoint(Input.mousePos.x, Input.mousePos.y);
//				}
//			}
//			return false;
		}
		
		public function get image():Bitmap 
		{
			return _image;
		}
		
		public function update():void
		{
			var enabled:Boolean = Game.instance.input.getAction(_actionId).enabled &&
				_cdm.isCooledDown(_id);
			// Pop-up animation
			if (enabled && !visible)
			{
				TweenLite.from(this, 0.2, { scaleX: 0.2, scaleY: 0.2, ease: Bounce.easeInOut } );
			}
			if (visible && !enabled)
			{
				_pressed = false;
				Game.instance.input.getAction(_actionId).unhold();
			}
			visible = enabled;
		}
	}

}