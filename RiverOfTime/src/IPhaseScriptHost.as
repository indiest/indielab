package  
{
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public interface IPhaseScriptHost 
	{
		function compareProgress(nextProgress:Number):Number;
		
		function invokeMethod(methodName:String, args:Array):void;
	}
	
}