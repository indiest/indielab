package  
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Level4PhotonTurret extends Enemy 
	{
		
		public function Level4PhotonTurret(bulletContainer:PhysicalSpriteContainer) 
		{
			super(bulletContainer, Assets.LEVELS_4_ENEMY_YELLOW, PhysicalSprite.EMPTY_BODY_NAME);
			
		}
		
		override public function attack(targetX:Number, targetY:Number, bulletSpeed:Number, biasRatio:Number):void 
		{
			for (var speed:Number = bulletSpeed * 0.5; speed < bulletSpeed; speed += bulletSpeed * 0.25)
			{
				for (var bias:Number = 0; bias < biasRatio; bias += biasRatio * 0.334)
				{
					super.attack(targetX, targetY, speed, bias);
				}
			}
					Game.instance.sfx.playFx("photon");
		}
	}

}