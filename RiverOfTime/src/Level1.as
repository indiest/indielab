package  
{
	import com.gamua.flox.Flox;
	import com.greensock.easing.Bounce;
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TransformGestureEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.CooldownManager;
	import me.sangtian.common.Direction;
	import me.sangtian.common.display.DualImageWrappingScroller;
	import me.sangtian.common.display.PixelPerfectCollisionDetector;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.display.StarsEffect;
	import me.sangtian.common.Input;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.nape.NapeUtil;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.common.ProcedureProfiler;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.CollisionArbiter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Polygon;
	import nape.space.Space;
	import nape.util.Debug;
	
	/**
	 * ...
	 * @author 
	 */
	public class Level1 extends BaseLevel
	{
		public static const SCROLL_MIN_SPEED_Y:int = 4;
		public static const RIVER_WIDTH_MIN:Number = 150;// 180;
		public static const RIVER_WIDTH_MAX:Number = 200;// 240;
		public static const CANOE_DEFAULT_Y:Number = 500;
		public static const ROCK_NUM:uint = 12;
		public static const ROCK_SIZE_MAX:Number = 48;
		public static const WATERFALL_PROGRESS:Number = 0.95;
		
		private var _enemyScript:PhaseScript;

		private var _bgRiver:Bitmap;
		//private var _bgRiverEffect:Bitmap;
		private var _riverStarsEffect:StarsEffect;
		private var _bgBank:Bitmap;
		private var _bgBankScroller:DualImageWrappingScroller;
		private var _waveContainer:GfxContainer;
		private var _riverBitmapData:BitmapData;
		private var _riverImage:Bitmap;
		private var _raftCollisionDetector:PixelPerfectCollisionDetector;
		private var _rockContainer:PhysicalSpriteContainer;
		private var _waterSplashes:GfxContainer;
		private var _raftWave:StarlingSpriteSheet;
		private var _currentLine:int;
		private var _scrollY:uint = 0;
		override public function get scrollY():Number 
		{
			return _scrollY;
		}
		
		override public function get leftBoundary():Number 
		{
			return bankWidth;
		}
		
		override public function get rightBoundary():Number 
		{
			return bankWidth + riverWidth;
		}
		
		private var _leapTimer:Number;
		private var _iconLeap:SkillIcon;
		private var _iconSlow:SkillIcon
		
		public function Level1() 
		{
			super(1);
			
			_enemyScript = new PhaseScript(this);
			//_enemyScript.logEnabled = true;

			_bgRiver = new Bitmap(new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight, false));
			addChild(_bgRiver);
			
			
			_riverStarsEffect = new StarsEffect(Game.instance.stageWidth, Game.instance.stageHeight, 1000, 0xffffff, 0xffffff);
			_riverStarsEffect.maxRadius = 2;
			_riverStarsEffect.velocityY = -200;
			//addChild(_riverStarsEffect);
			
			/*
			*/
			var grid:Bitmap = new Bitmap(new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight, true, 0));
			var rect:Rectangle = new Rectangle(0, 0, 1, Game.instance.stageHeight);
			while (rect.x < Game.instance.stageWidth)
			{
				rect.x += 4;
				grid.bitmapData.fillRect(rect, 0x11ffffff);
			}
			addChild(grid);
			
			_waveContainer = new GfxContainer(Assets.ANIMATIONS_SPRAY, Assets.ANIMATIONS_SPRAY_DATA, 15, updateWave);
			_waveContainer.autoPlay = false;
			_waveContainer.playOnce = false;
			addChild(_waveContainer);
			
			_bgBank = new Assets.LEVELS_1_BG();//new Bitmap(new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight, false, 0x00ccff));
			_bgBankScroller = new DualImageWrappingScroller(_bgBank);
			addChild(_bgBankScroller);
			
			bankWidthMin = (_bgBank.width - RIVER_WIDTH_MAX) >> 1;
			bankWidthMax = (_bgBank.width - RIVER_WIDTH_MIN) >> 1;
			
			_riverBitmapData = new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight, true, 0);
			_riverImage = new Bitmap(_riverBitmapData);
			addChild(_riverImage);
			_bgBankScroller.cacheAsBitmap = true;
			_riverImage.cacheAsBitmap = true;
			_bgBankScroller.mask = _riverImage;
			
			//_space.worldLinearDrag = 20;
			_space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.ANY, Raft.CB_TYPE, CbType.ANY_BODY, handleCollision));
			
			// Add side barriers
			var barriers:Body = new Body(BodyType.STATIC);
			barriers.shapes.add(new Polygon(Polygon.rect( -20, 0, 20, Game.instance.stageHeight)));
			barriers.shapes.add(new Polygon(Polygon.rect(Game.instance.stageWidth, 0, 20, Game.instance.stageHeight)));
			barriers.space = _space;
			
			_rockContainer = new PhysicalSpriteContainer(_space, ROCK_NUM, createRock, updateRock);
			addChild(_rockContainer);
			
			_waterSplashes = new GfxContainer(Assets.ANIMATIONS_SPLASH,
				StarlingSpriteSheetData.create(128, 128, 1024, 512), 1, updateWaterSplash);
			addChild(_waterSplashes);

			_raftWave = new StarlingSpriteSheet(
				AssetsCache.getBitmapData(Assets.ANIMATIONS_RAFT_WAVE),
				StarlingSpriteSheetData.parse(AssetsCache.getOrCacheAsset(Assets.ANIMATIONS_RAFT_WAVE_DATA))
			);
			_raftWave.play();
			addChild(_raftWave);
			
			_raft.scale = _raftWave.scaleX = _raftWave.scaleY = 0.9;
			_raft.accelerationY = 0;
			//_raftBitmapData = _raft.bitmapData;
			_raft.animation.frameRate = 20;
			addChild(_raft);
			
			_raftCollisionDetector = new PixelPerfectCollisionDetector(_raft, Game.instance.stageWidth, Game.instance.stageHeight, _raft.bitmap.bitmapData);
			// The river beyond the Waterfall has 0x33 transparent cover.
			_raftCollisionDetector.targetAlphaThreshold = 0x40;
			
			_iconLeap = new SkillIcon(AssetsCache.getNewBitmap(Assets.UI_ICON_LEAP), CD_ID_LEAP, _cdm, Game.ACTION_ID_SKILL_A);
			_iconLeap.x = 48;
			_iconLeap.y = Game.instance.stageHeight - 48;
			_uiLayer.addChild(_iconLeap);
			_iconSlow = new SkillIcon(AssetsCache.getNewBitmap(Assets.UI_ICON_SLOW), CD_ID_SLOW, _cdm, Game.ACTION_ID_SKILL_B);
			_iconSlow.x = Game.instance.stageWidth - 48;
			_iconSlow.y = Game.instance.stageHeight - 48;
			_uiLayer.addChild(_iconSlow);
		}
		
		private function createRock():PhysicalSprite
		{
			var i:int = Random.rangeInt(1, 5);
			var rock:PhysicalSprite = new PhysicalSprite(Assets["LEVELS_1_ROCK" + i], "rock" + i);
			rock.body.type = BodyType.KINEMATIC;
			return rock;
		}
		
		override protected function enterScene():void 
		{
			super.enterScene();
			if (mode == MODE_NORMAL)
			{
				_enemyScript.parse(AssetsCache.getOrCacheAsset(Assets.LEVELS_1_NORMAL_SCRIPT));
			}
			else
			{
				_enemyScript.parse(AssetsCache.getOrCacheAsset(Assets.LEVELS_1_ENDLESS_SCRIPT));
			}
			_currentLine = 0;
			waterColor = 0xff2d6e63;
			bankRandomSpeed = 0;
			bankWidth = bankWidthMin;
			riverRandomSpeed = 0;
			riverWidth = RIVER_WIDTH_MAX;//Random.rangeNumber(riverWidthMin, riverWidthMax);
			rockX = 0;
			rockY = 0;
			//Log.debug("Before re-gen river");
			//ProcedureProfiler.Default.startTiming();
			updateRiverImage(_riverBitmapData, _riverBitmapData.height, false);
			// On Android the BitmapData.setPixel operations are asynchronous, we have to use some tricky way to refresh it.
			_riverBitmapData.scroll(0, 0);
			//Log.debug("After re-gen river", ProcedureProfiler.Default.stopTiming());
			_currentLine = _riverImage.height;
			
			_leapTimer = 0;
			_iconLeap.visible = _iconSlow.visible = false;
			
			_raft.x = 180;
			_raft.y = 700;
			//progress = 0.95;
			Game.instance.fade(0x00ffffff, 1.0, enterRaft);
			//TweenLite.from(this, 1.0, { alpha: 0, onComplete: enterRaft } );
			
			if (!Game.instance.player.tutorialRecord["1_0"])
			{
				Game.instance.input.getAxis("X").enabled = false;
			}
			if (!Game.instance.player.tutorialRecord["1_1"])
			{
				Game.instance.input.getAction(Game.ACTION_ID_SKILL_A).enabled = false;
			}
			if (!Game.instance.player.tutorialRecord["1_2"])
			{
				Game.instance.input.getAction(Game.ACTION_ID_SKILL_B).enabled = false;
			}
			
			if (mode == MODE_NORMAL)
			{
				//Game.instance.music.play("Level1");
				_music.play(Game.instance.music.getSound("Level1_2").sound);
			}
			else
			{
				//Game.instance.music.playLoop("stream").fadeFrom(0, 0.2, 3000);
				_music.playLoop(Game.instance.music.getSound("stream").sound, 0.1);
				_music.fadeIn(3.0);
			}
		}
		
		private function enterRaft():void
		{
			TweenLite.to(_raft, 3.0, { y: CANOE_DEFAULT_Y, onComplete: finishTransition } );
		}

		private var waterColor:uint;
		private var bankWidthMin:Number;
		private var bankWidthMax:Number;
		private var bankRandomSpeed:Number;
		private var bankWidth:Number;
		private var riverRandomSpeed:Number;
		private var riverWidth:Number;
		private var riverWidthUnderCanoe:Number;
		private var rockX:Number;
		private var rockY:Number;
		
		private function updateRiverImage(bd:BitmapData, lines:int, randomBank:Boolean):void 
		{
			//var widthFactor:Number = 1 / bankWidthMax;
			var rect:Rectangle = new Rectangle(0, 0, 0, 1);
			bd.lock();
			_bgRiver.bitmapData.lock();
			_bgRiver.bitmapData.scroll(0, lines);
			for (var i:int = 0; i < lines; i++)
			{
				//if (progress > 0.25 && progress < lastEnemyProgress)
				if (randomBank)
				{
					bankRandomSpeed += Random.rangeNumber( -3, 3);
					bankWidth += bankRandomSpeed * 0.02 * (1 + progress);
					if (bankWidth < bankWidthMin || bankWidth > bankWidthMax)
					{
						bankRandomSpeed = -bankRandomSpeed * 0.5;//0;// 
					}
					riverRandomSpeed += Random.rangeNumber( -2, 2);
					riverWidth += riverRandomSpeed * 0.02 * (1 + progress);
					if (riverWidth < RIVER_WIDTH_MIN || riverWidth > RIVER_WIDTH_MAX)
					{
						riverRandomSpeed = -riverRandomSpeed * 0.5;//0;
					}
				}
				
				//var y:int = _currentLine + i;
				
				/*
				var bdY:int = lines - i - 1;
				for (var x:int = 0; x < bd.width; x++)
				{
					var color:uint = progress > WATERFALL_PROGRESS ? 0x33ffffff : 0;
					if (x < bankWidth)
					{
						color = MathUtil.interpolateColor(0xFF0A0A0A, 0xFF2C682E, x * widthFactor);
					}
					else if (x >= bankWidth + riverWidth)
					{
						color = MathUtil.interpolateColor(0xFF0A0A0A, 0xFF2C682E, (_bg.width - x) * widthFactor);
					}
					bd.setPixel32(x, bdY, color);
				}
				*/
				
				rect.y = lines - i - 1;
				rect.x = 0;
				rect.width = _bgBank.width;
				/*
				bd.fillRect(rect, waterColor);
				rect.width = MathUtil.clamp(bankWidth, 0, _leftBankPixels.length - 1);
				bd.setVector(rect, _leftBankPixels[int(rect.width)]);
				rect.x = bankWidth + riverWidth;
				rect.width = MathUtil.clamp(_bg.width - bankWidth - riverWidth, 0, _rightBankPixels.length - 1);
				bd.setVector(rect, _rightBankPixels[int(rect.width)]);
				*/
				
				/*
				bd.fillRect(rect, 0);
				rect.x = bankWidth;
				rect.width = riverWidth;
				bd.fillRect(rect, waterColor);
				*/
				
				_bgRiver.bitmapData.fillRect(rect, waterColor);
				_bgRiver.bitmapData.setPixel32(bankWidth, rect.y, 0xff000000);
				_bgRiver.bitmapData.setPixel32(bankWidth + 1, rect.y, 0xff000000);
				_bgRiver.bitmapData.setPixel32(bankWidth + riverWidth - 2, rect.y, 0xff000000);
				_bgRiver.bitmapData.setPixel32(bankWidth + riverWidth - 1, rect.y, 0xff000000);

				bd.fillRect(rect, 0xffffffff);
				rect.x = bankWidth;
				rect.width = riverWidth;
				bd.fillRect(rect, 0);
				
			}
			_bgRiver.bitmapData.unlock();
			bd.unlock();
		}
		
		private function generateRock():void
		{		
			//if (rockX > 0)
			//{
				var rock:PhysicalSprite = _rockContainer.add();
				if (rock != null)
				{
					rock.setTo(rockX, -rock.height);
					rock.rotation = Random.angle;
					rock.body.rotation = MathUtil.ANGLE_RADIAN * rock.rotation;
					//_riverBitmapData.draw(rock, rock.transform.matrix);
				}
			//}
		}
		
		private function updateRock(rock:PhysicalSprite):void 
		{
			rock.y = rock.body.position.y = rock.y + scrollY;
			if (Game.instance.isOutOfScreen(rock, Direction.DOWN))
			{
				rock.removeFromParent();
			}
		}

		override protected function updateSkill():void 
		{
			super.updateSkill();
			
			_iconLeap.update();
			_iconSlow.update();
			
			if (Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_A) &&
				//(_swipeOffsetY <= -1 || _iconLeap.isPressed || Input.getKeyDown(Keyboard.SPACE)) &&
				_cdm.isCooledDown(CD_ID_LEAP))
			{
				if (_isTutoring)
				{
					if (_currentTutorial is Tutorial1_1)
					{
						finishTutorial();
						Game.instance.player.tutorialRecord["1_1"] = true;
					}
					else
					{
						return;
					}
				}
				
				leapCanoe();
				onUseSkill(CD_ID_LEAP);
			}
			if (_leapTimer > 0)
			{
				_leapTimer -= Time.deltaSecond;
				_raft.scaleX = _raft.scaleY = 1 + 0.3 * Math.sin(_leapTimer * Math.PI);
				if (_leapTimer <= 0)
				{
					resumeCanoe();
					//_raft.addChildAt(Game.instance.splashes.add(0, -32), 0);
					_waterSplashes.add(_raft.x, _raft.y - 32);
					Game.instance.sfx.playFx("water_splash");
				}
			}
			
			
			// Swipe down or press SHIFT to slow down timing
			if (Time.scale < 1)
			{
				Time.scale += 0.006;
				//_music.speed += 0.006;
			}
			else if (Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_B) &&
				//(_swipeOffsetY >= 1 || _iconSlow.isPressed || Input.getKeyDown(Keyboard.SHIFT)) &&
				_cdm.isCooledDown(CD_ID_SLOW))
			{
				if (_isTutoring)
				{
					if (_currentTutorial is Tutorial1_2)
					{
						finishTutorial();
						Game.instance.player.tutorialRecord["1_2"] = true;
					}
					else
					{
						return;
					}
				}
				
				Time.scale = 0.2;
				//_music.speed = 0.2;
				onUseSkill(CD_ID_SLOW);
			}
			
		}
		
		override protected function updateProgress():void 
		{
			if (_isTutoring)
				return;
			super.updateProgress();
			// 67 seconds
			progress += (0.015 * Time.deltaSecond);
			CONFIG::debug
			{
				//Log.debug("Progress:", progress);
				//_clock.setTime(surviveTime);
			}
			//progress = _currentLine / RIVER_LINE_LENGTH;
			_scrollY = Time.scale * (SCROLL_MIN_SPEED_Y
				// Progress
				+ 4 * progress
				// FIXME: River narrowness under the canoe
				+ 2 * (1 - riverWidth / RIVER_WIDTH_MAX)
			);
		}
		
		override public function update():void 
		{
			CONFIG::debug
			{
				if (Game.instance.isKeyUp(Keyboard.T))
				{
					if (_bgBankScroller.mask == null)
						_bgBankScroller.mask = _riverImage;
					else
						_bgBankScroller.mask = null;
				}
			}
			
			super.update();
			
			// Always update waterfall and raft's animations
			_waveContainer.update();
			_raft.animation.frameRate = scrollY * 5;
			_raft.animation.updateFrame(Time.deltaSecond);
			_raftWave.updateFrame(Time.deltaSecond);
			_raftWave.x = _raft.x -_raftWave.maxWidth * 0.5 + 2;
			_raftWave.y =  _raft.y -_raftWave.maxHeight * 0.5 - 4;
			_riverStarsEffect.update(Time.deltaSecond);
		}
		
		override protected function updateScene():void 
		{
			/*
			if (progress >= 0.15 && _tutorial2 == null &&
				!Game.instance.player.tutorialRecord["1_2"])
			{
				_tutorial2 = new Tutorial1_2();
				_tutorial2.textMobile.visible = _tutorial2.gesture.visible = Game.instance.isMobile;
				_tutorial2.textDesktop.visible = !Game.instance.isMobile;
				startTutorial(_tutorial2);
			}
			*/
			if (_isTutoring)
			{
				if (_currentTutorial is Tutorial1_0 &&
					(Game.instance.input.getAxisValue("X") != 0 || Game.instance.input.getAxisValue("Y") != 0))
				{
					finishTutorial();
					Game.instance.player.tutorialRecord["1_0"] = true;
				}
				return;
			}
			
			_bgBankScroller.scrollY += scrollY;
			_riverBitmapData.scroll(0, scrollY);
			//updateRiverImage(_riverBitmapData, scrollY, true);
			_currentLine += scrollY;

			_enemyScript.update();
			
			/*
			// Randomly generate rock
			if (progress > 0.3 && progress < lastEnemyProgress &&
				Random.randomBoolean(0.1 * difficulty * (1 + progress) * riverWidth / RIVER_WIDTH_MAX))
			{
				var rx:Number = bankWidth + Random.rangeNumber(0, riverWidth);
				if (MathUtil.compareDistance(rx - rockX, _currentLine - rockY, ROCK_SIZE_MAX * 2) > 0)
				{
					rockX = rx;
					rockY = _currentLine;
					generateRock();
				}
			}
			*/
			
			_waterSplashes.update();
			
			_raft.update(Time.deltaSecond);

			// If the canoe is hit/rubbed behind the default line, speed it up
			if (_raft.y > CANOE_DEFAULT_Y)
				_raft.body.velocity.y -= 10;
			else
				_raft.body.velocity.y = 0;
				
			updateHitTest();
			_rockContainer.update();
			
			//scrollRect.y = _raft.y - CANOE_DEFAULT_Y;
			
			if (mode == MODE_NORMAL)
			{
				if (justPassedProgress(WATERFALL_PROGRESS))
				{
					_raftWave.visible = false;
					// Add waves animation
					var waveX:Number = bankWidth;
					while (waveX < bankWidth + riverWidth)
					{
						var wave:PhysicalSpriteAnimated = _waveContainer.add(waveX, 0) as PhysicalSpriteAnimated;
						// Start at random frame
						wave.play(Random.rangeInt(1, wave.animation.totalFrames));
						waveX += 20;// wave.animation.maxWidth;
					}
				}
				
			}
		}
		
		private function updateWave(wave:PhysicalSpriteAnimated):void
		{
			if (wave.y < CANOE_DEFAULT_Y)
			{
				wave.y += scrollY;
			}
			else
			{
				if (!isTransition)
				{
					startTransition();
					TweenLite.to(_raft, 2.0, {y: CANOE_DEFAULT_Y - 100, scaleX: 0.1, scaleY: 0.1, onComplete: finishLevel } );
				}
			}
		}
		
		private function updateWaterSplash(splash:PhysicalSpriteAnimated):void 
		{
			splash.setTo(_raft.x, _raft.y - 32);
		}
		
		private function updateHitTest():void 
		{
			var hitRect:Rectangle = _raftCollisionDetector.hitTestBitmap(_riverBitmapData);
			if (hitRect != null)
			{
				var overlapX:Number = _raft.x < Game.instance.stageWidth * 0.5 ? hitRect.width : -hitRect.width;
				//trace("Overlap X:", overlapX);
				//var overlapY:Number = hitRect.height* MathUtil.sign(_raft.y - hitRect.y) * 0.1;
				_raft.x += overlapX;
				//_raft.y += overlapY;
				_raft.body.position.x += overlapX;
				
				// Slow canoe down
				//_raft.body.velocity.x = 0;
				_raft.body.velocity.y = 2 * hitRect.height;
				//Game.instance.shake(this, hitRect.width, hitRect.width, 0.1, 1);
			}
		}
		
		private function handleCollision(cb:InteractionCallback):void 
		{
			if (_leapTimer > 0)
			{
				if (_leapTimer < 0.2)
				{
					Game.instance.shake(this, 20, 20, 0.3, 3);
					Game.instance.sfx.play("wood_pile", 2.0);
					gameOver(1, 0, 0xffff0000);
				}

			}
			else
			{
				/*
				if (_cdm.isCooledDown(CD_ID_LEAP) && _tutorial1 == null &&
					!Game.instance.player.tutorialRecord["1_1"])
				{
					_tutorial1 = new Tutorial1_1();
					_tutorial1.textMobile.visible = _tutorial1.gesture.visible = Game.instance.isMobile;
					_tutorial1.textDesktop.visible = !Game.instance.isMobile;
					startTutorial(_tutorial1);
				}
				else
				*/
				{
					// Slow down raft in normal direction, to reduce further collision with the same rock.
					var arbiter:CollisionArbiter = cb.arbiters.at(0) as CollisionArbiter;
					_raft.body.velocity.x += arbiter.normal.x * _raft.body.velocity.x;
					_raft.body.velocity.y += arbiter.normal.y * _raft.body.velocity.y;
					
					//Game.instance.shake(this, 5, 5, 0.1, 1);
					Game.instance.sfx.play("hit_rock", 0.3);
				}
			}
		}
		
		private function resumeCanoe():void 
		{
			_raft.scaleX = _raft.scaleY = 1;
			_raft.collisionEnabled = true;
			_raftWave.visible = true;
		}
		
		private function leapCanoe():void 
		{
			if (_leapTimer > 0)
				return;
			_leapTimer = 1.0;
			_raft.collisionEnabled = false;
			_raftWave.visible = false;
		}
		
		override protected function clear():void 
		{
			super.clear();
			Game.instance.input.getAxis("X").enabled = true;
			Game.instance.input.getAction(Game.ACTION_ID_SKILL_A).enabled = true;
			Game.instance.input.getAction(Game.ACTION_ID_SKILL_B).enabled = true;
			//DisplayObjectUtil.clearBitmap(_riverImage);
			_bgBankScroller.reset();
			_rockContainer.clear();
			_waterSplashes.clear();
			_waveContainer.clear();
			resumeCanoe();
		}
		
		public function updateRiver(randomRank:Boolean = true):void
		{
			updateRiverImage(_riverBitmapData, scrollY, randomRank);
		}
		
		public function setWaterColor(color:String):void
		{
			waterColor = parseInt(color, 16);
		}
		
		public function addRock(x:Number = 0, y:Number = 0):PhysicalSprite
		{
			var rock:PhysicalSprite = _rockContainer.add(x, y);
			if (rock != null)
			{
				rock.rotation = Random.angle;
				rock.body.rotation = MathUtil.ANGLE_RADIAN * rock.rotation;
				if (x == 0)
					x = bankWidth + Random.rangeNumber(0, riverWidth);
				if (y == 0)
					y = Random.rangeNumber( -ROCK_SIZE_MAX, -ROCK_SIZE_MAX >> 1);
				rock.setTo(x, y);
			}
			return rock;
		}
		
		public function addRockRandomly(chance:Number = 0.1, minDist:Number = 128, progressFactor:Number = 1, riverWidthFactor:Number = 1):void
		{
			if (Random.randomBoolean(chance * difficulty
				* (1 + progressFactor * progress)
				* (1 + riverWidthFactor * riverWidth / RIVER_WIDTH_MAX)))
			{
				var rx:Number = bankWidth + Random.rangeNumber(0, riverWidth);
				var ry:Number = Random.rangeNumber( -ROCK_SIZE_MAX, -ROCK_SIZE_MAX >> 1);
				if (MathUtil.compareDistance(rx - rockX, ry - rockY, minDist) > 0)
				{
					rockX = rx;
					rockY = ry;
					addRock(rx, ry);
				}
			}
			
		}
		
		public function switchInput(enabled:Boolean):void
		{
			//Game.instance.input.enabled = enabled;
		}
		
		public function showTutorial(id:String):void
		{
			// Already finished the tutorial
			if (Game.instance.player.tutorialRecord[id])
				return;
				
			var tutorialClass:Class = _tutorials[id];
			if (tutorialClass == null)
			{
				Log.warn("Missing tutorial:", id);
				return;
			}
			
			var tutorial:MovieClip = new tutorialClass();
			if (tutorial is Tutorial1_0)
			{
				var tutorial0:Tutorial1_0 = tutorial as Tutorial1_0;
				tutorial0.textMobile.visible = tutorial0.gesture.visible = Game.instance.isMobile;
				tutorial0.textDesktop.visible = !Game.instance.isMobile;
				Game.instance.input.getAxis("X").enabled = true;
			}
			else if (tutorial is Tutorial1_1)
			{
				var tutorial1:Tutorial1_1 = tutorial as Tutorial1_1;
				tutorial1.textMobile.visible = tutorial1.gesture.visible = Game.instance.isMobile;
				tutorial1.textDesktop.visible = !Game.instance.isMobile;
				Game.instance.input.getAction(Game.ACTION_ID_SKILL_A).enabled = true;
			}
			else if (tutorial is Tutorial1_2)
			{
				var tutorial2:Tutorial1_2 = tutorial as Tutorial1_2;
				tutorial2.textMobile.visible = tutorial2.gesture.visible = Game.instance.isMobile;
				tutorial2.textDesktop.visible = !Game.instance.isMobile;
				Game.instance.input.getAction(Game.ACTION_ID_SKILL_B).enabled = true;
			}
			
			startTutorial(tutorial);
		}
		
		private var _isTutoring:Boolean = false;
		private var _currentTutorial:MovieClip = null;
		//private var _tutorial1:Tutorial1_1;
		//private var _tutorial2:Tutorial1_2;
		private var _tutorials:Object = 
		{
			"1_0": Tutorial1_0,
			"1_1": Tutorial1_1,
			"1_2": Tutorial1_2
		};

		private function startTutorial(mc:MovieClip):void
		{
			_isTutoring = true;
			addChild(mc);
			_currentTutorial = mc;
			//Game.instance.music.pauseAll();
			_music.pause();
			Game.instance.sfx.pauseAll();
		}
		
		private function finishTutorial():void
		{
			removeChild(_currentTutorial);
			_currentTutorial.stop();
			_currentTutorial = null;
			_isTutoring = false;
			//Game.instance.music.resumeAll();
			_music.resume();
			Game.instance.sfx.resumeAll();
		}
		
		override protected function getSkillUpgradeName1():String 
		{
			return "Leap of Faith";
		}
		
		override protected function getSkillUpgradeName2():String 
		{
			return "Blink";
		}
		
		override protected function getSkillUpgradeIcon1():DisplayObject 
		{
			var icon:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_LEAP);
			icon.width = icon.height = 48;
			return icon;
		}
		
		override protected function getSkillUpgradeIcon2():DisplayObject 
		{
			var icon:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_BLINK);
			icon.width = icon.height = 48;
			return icon;
		}
		
		override protected function performUpgradeSkill(level:int, value:Number):void
		{
			_cdm.setCooldown(CD_ID_LEAP, CD_LEAP * (1 - value / 100));
		}
	}
}