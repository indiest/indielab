package  
{
	import me.sangtian.common.state.ResourceLoadingState;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class LevelLoadingState extends ResourceLoadingState 
	{
		private static var _levelClass:Class;
		public static function init(listPath:String, levelClass:Class):void
		{
			resourceListPath = listPath;
			_levelClass = levelClass;
		}
		
		public function LevelLoadingState() 
		{
			super();
		}
		
		override public function enter():void 
		{
			ASSERT(_levelClass != null);
			// TODO: clear cache
			super.enter();
		}
		
		override protected function getUrl(item:Object):String 
		{
			return "resources/" + item.path;
		}
		
		override protected function loadResourceItem(item:Object):void 
		{
			if (item.type == "sfx")
			{
				Game.instance.sfx.loadSound(getUrl(item), getId(item));
			}
			else if (item.type == "music" || item.type == "ambience")
			{
				Game.instance.music.loadSound(getUrl(item), getId(item));
			}
			else
			{
				super.loadResourceItem(item);
			}
		}
		
		override protected function loadNextState():void 
		{
			Game.instance.stateManager.loadState(_levelClass);
		}
	}

}