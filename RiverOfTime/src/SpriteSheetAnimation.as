package  
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.common.util.DisplayObjectUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class SpriteSheetAnimation 
	{
		private var _objectPool:ObjectPool;
		private var _bitmap:Bitmap;
		private var _data:StarlingSpriteSheetData;
		
		public function SpriteSheetAnimation(bitmap:Bitmap, data:*, maxSize:uint) 
		{
			_bitmap = bitmap;
			if (data is StarlingSpriteSheetData)
			{
				_data = data;
			}
			else if (data is String || data is ByteArray)
			{
				_data = StarlingSpriteSheetData.parse(data);
			}
			else
			{
				throw new Error("Invalid data");
			}
			_objectPool = new ObjectPool(maxSize, createSpritesheet);
		}
		
		private function createSpritesheet():StarlingSpriteSheet
		{
			var ss:StarlingSpriteSheet = new StarlingSpriteSheet(_bitmap.bitmapData, _data);
			return ss;
		}
		
		private function handleComplete(e:Event):void 
		{
			var ss:StarlingSpriteSheet = e.currentTarget as StarlingSpriteSheet;
			ss.removeEventListener(Event.COMPLETE, handleComplete);
			remove(ss);
		}
		
		public function add(x:Number = 0, y:Number = 0, autoPlay:Boolean = true, autoRemove:Boolean = true):StarlingSpriteSheet
		{
			var ss:StarlingSpriteSheet = _objectPool.borrowObject();
			if (ss != null)
			{
				ss.x = x - ss.maxWidth * 0.5;
				ss.y = y - ss.maxHeight * 0.5;
				if (autoRemove)
				{
					ss.addEventListener(Event.COMPLETE, handleComplete);
				}
				if (autoPlay)
				{
					// FIXME: Step by Time.deltaSecond
					ss.playFixed();
				}
			}
			return ss;
		}
		
		public function remove(ss:StarlingSpriteSheet):void
		{
			DisplayObjectUtil.removeFromParent(ss);
			ss.stop();
			_objectPool.returnObject(ss);
		}
		
		public function reset():void
		{
			_objectPool.returnAll(resetSpritesheet);
		}
		
		private function resetSpritesheet(ss:StarlingSpriteSheet):void 
		{
			DisplayObjectUtil.removeFromParent(ss);
			ss.stop();
		}
	}

}