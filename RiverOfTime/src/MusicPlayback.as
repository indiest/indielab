package  
{
	import com.greensock.TweenLite;
	import com.noteflight.standingwave3.elements.AudioDescriptor;
	import com.noteflight.standingwave3.elements.Sample;
	import com.noteflight.standingwave3.output.AudioPlayer;
	import com.noteflight.standingwave3.sources.LoopSource;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;
	import me.sangtian.common.util.TweenUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class MusicPlayback 
	{
		private const SAMPLE_RATE:Number = 44.1;
		private const MAGIC_DELAY:Number = 2257.0 * 4; // LAME 3.98.2 + flash.media.Sound Delay		
		
		private var _audioPlayer:AudioPlayer = new AudioPlayer();
		private var _sample:Sample = new Sample(new AudioDescriptor(), 0);
		private var _loopSource:LoopSource = new LoopSource(_sample.descriptor, _sample);
		private var _volume:Number = 1.0;
		private var _pauseVolume:Number;
		private var _mute:Boolean = false;
		
		public function get volume():Number
		{
			return _volume;
		}
		
		public function set volume(value:Number):void
		{
			_volume = value;
			if (!mute)
			{
				setChannelVolumn(value);
			}
		}
		
		private function setChannelVolumn(value:Number):void
		{
			if (_audioPlayer.channel != null)
			{
				var st:SoundTransform = _audioPlayer.channel.soundTransform;
				st.volume = value;
				_audioPlayer.channel.soundTransform = st;
			}
		}
		
		public function get speed():Number
		{
			return _loopSource.frequencyShift;
		}
		
		public function set speed(value:Number):void
		{
			_loopSource.frequencyShift = value;
		}
		
		public function get mute():Boolean 
		{
			return _mute;
		}
		
		public function set mute(value:Boolean):void 
		{
			_mute = value;
			if (value)
				setChannelVolumn(0);
			else
				setChannelVolumn(volume);
		}
		
		public function MusicPlayback() 
		{
		}
		
		private var _sampleCache:Dictionary = new Dictionary(true);
		
		public function play(sound:Sound, volume:Number = 1.0, loop:Boolean = false):void
		{
			var sample:Sample = _sampleCache[sound];
			if (sample == null)
			{
				sample = new Sample(new AudioDescriptor(), sound.length * SAMPLE_RATE);
				sample.extractSound(sound, 0, sound.length * SAMPLE_RATE);
				_sampleCache[sound] = sample;
			}
			sample.resetPosition();
			_loopSource.generator = sample;
/*
			_loopSource = new LoopSource(sample.descriptor, sample);
			_loopSource.firstFrame = MAGIC_DELAY;
			_loopSource.startFrame = MAGIC_DELAY;
			_loopSource.endFrame = sample.frameCount;
			
			_sample.realloc(sound.length * SAMPLE_RATE);
			_sample.extractSound(sound, 0, sound.length * SAMPLE_RATE);
*/			
			if (loop)
			{
				_loopSource.firstFrame = MAGIC_DELAY;
				_loopSource.startFrame = MAGIC_DELAY;
				_loopSource.endFrame = sample.frameCount;
			}
			else
			{
				_loopSource.firstFrame = 0;
				_loopSource.startFrame = 0;
				_loopSource.endFrame = 0;
			}
			_loopSource.resetPosition();
			_audioPlayer.play(_loopSource);
			this.volume = volume;
		}
		
		public function playLoop(sound:Sound, volume:Number = 1.0):void
		{
			play(sound, volume, true);
		}
		
		public function stop():void
		{
			TweenUtil.killTweensOf(this);
			_audioPlayer.stop();
		}
		
		public function pause(fadeDuration:Number = 0.5):void
		{
			if (fadeDuration > 0)
			{
				_pauseVolume = volume;
				fadeOut(fadeDuration, 0, _audioPlayer.pause);
			}
			else
			{
				_audioPlayer.pause();
			}
		}
		
		public function resume(fadeDuration:Number = 0.5):void
		{
			if (fadeDuration > 0)
			{
				volume = _pauseVolume;
				fadeIn(fadeDuration);
			}
			_audioPlayer.resume();
			this.mute = mute;
		}
		
		public function fadeIn(duration:Number, startVolume:Number = 0, onFinish:Function = null):void
		{
			TweenLite.from(this, duration, { volume: startVolume, onComplete: onFinish } );
		}
		
		public function fadeOut(duration:Number, endVolume:Number = 0, onFinish:Function = null):void
		{
			TweenLite.to(this, duration, { volume: endVolume, onComplete: onFinish } );
		}
	}

}