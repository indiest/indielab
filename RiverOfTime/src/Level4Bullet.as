package  
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import me.sangtian.common.Time;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Level4Bullet extends Bullet 
	{
		public var shield:Sprite;
		public var firePos:Point;
		public var fireTime:int;
		
		public function Level4Bullet(shield:Sprite, assetClass:Class, bodyName:String = null)  
		{
			super(assetClass, bodyName);
			ASSERT(shield != null);
			this.shield = shield;
		}
		
		override public function setTo(x:Number, y:Number):void 
		{
			super.setTo(x, y);
			if (firePos == null)
			{
				firePos = new Point(x, y);
				fireTime = Time.lastTickTime;
			}
		}
		
		override public function removeFromParent():void 
		{
			super.removeFromParent();
			firePos = null;
		}
	}

}