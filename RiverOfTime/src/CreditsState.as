package  
{
	import com.greensock.easing.Linear;
	import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import me.sangtian.common.state.IState;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.TweenUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class CreditsState extends Sprite implements IState 
	{
		private static const SCROLL_SPEED:Number = 40;
		
		private var _credits:Credits = new Credits();
		private var _textInitY:Number;
		private var _endingMovie:MovieClip;
		
		public function CreditsState() 
		{
			_textInitY = _credits.textCredits.y;
		}
		
		/* INTERFACE me.sangtian.common.state.IState */
		
		public function get isTransientState():Boolean 
		{
			return true;
		}
		
		public function enter():void 
		{
			Game.instance.nativeStage.color = 0;//0xffffff;
			DisplayObjectUtil.playMovie("ending.swf", this, false, null, handleMovieLoaded, handleMovieExitFrame);
			Game.instance.music.play("Ending");
		}
		
		private function handleMovieLoaded(mc:MovieClip):void 
		{
			_endingMovie = mc;
		}
		
		private function handleMovieExitFrame(mc:MovieClip):void 
		{
			if (mc.totalFrames == 1 && mc.numChildren == 1)
				mc = mc.getChildAt(0) as MovieClip;
			/*
			if (mc.currentFrame == 30)
			{
				Game.instance.nativeStage.color = 0x000000;
				Game.instance.shake(this, -7, -7, 1.5, 10, onShakeComplete);
			}
			*/
			if (mc.currentFrame >= 800 && _credits.parent == null)
			{
				_credits.textCredits.y = _textInitY;
				TweenLite.to(_credits.textCredits, _credits.textCredits.height / SCROLL_SPEED, {
					y: _textInitY -_credits.textCredits.height,
					ease: Linear.ease,
					onComplete: onCreditsFinish
				} );
				addEventListener(MouseEvent.CLICK, handleClick);
				addChild(_credits);
			}
		}
		
		private function onShakeComplete():void 
		{
			Game.instance.nativeStage.color = 0xffffff;
		}
		
		private function handleClick(e:MouseEvent):void 
		{
			onCreditsFinish();
		}
		
		private function onCreditsFinish():void
		{
			Game.instance.fade(0xffffffff, 1.0, backToMenu);
		}
		
		private function backToMenu():void
		{
			Game.instance.stateManager.loadState(MenuState);			
		}
		
		public function update():void 
		{
			
		}
		
		public function exit():void 
		{
			DisplayObjectUtil.removeMovieClip(_endingMovie);
			_endingMovie = null;
			DisplayObjectUtil.removeFromParent(_credits);
			TweenUtil.killTweensOf(_credits.textCredits);
			Game.instance.music.stopAll();
		}
		
	}

}