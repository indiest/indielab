package  
{
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import me.sangtian.common.Direction;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Bullet extends PhysicalSprite 
	{
		public var speed:Number;
		public var damage:Number;
		public var dist:Number;
		public var distMax:Number;
		
		private var _pointHelper:Point = new Point();
		public function get headPoint():Point
		{
			_pointHelper.setTo(x + width * MathUtil.cosAngle(rotation) * 0.5,
				y + height * MathUtil.sinAngle(rotation) * 0.5);
			return _pointHelper;
		}
		public function get tailPoint():Point
		{
			_pointHelper.setTo(x - width * MathUtil.cosAngle(rotation) * 0.5,
				y - height * MathUtil.sinAngle(rotation) * 0.5);
			return _pointHelper;
		}
		
		public function Bullet(assetClass:Class, bodyName:String = null) 
		{
			super(assetClass, bodyName);
			body.isBullet = true;
		}
		
		public function update(deltaSecond:Number):void 
		{
			var d:Number = deltaSecond * speed;
			var dx:Number = d * MathUtil.cosAngle(rotation);
			var dy:Number = d * MathUtil.sinAngle(rotation);
			setTo(x + dx, y + dy);
			dist += d;
			if (dist >= distMax || Game.instance.isOutOfScreen(this, Direction.fromVector(dx, dy)))//!Game.instance.isOnScreen(this))
				removeFromParent();
		}
		
		public static function getProvider(asset:Class):Function 
		{
			return ObjectPool.getProvider(Bullet, [asset, PhysicalSprite.EMPTY_BODY_NAME]);
		}
		
	}

}