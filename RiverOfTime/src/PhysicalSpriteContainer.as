package  
{
	import flash.display.Sprite;
	import me.sangtian.common.ObjectPool;
	import nape.phys.Body;
	import nape.space.Space;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class PhysicalSpriteContainer extends Sprite 
	{
		private var _pool:ObjectPool;
		private var _space:Space;
		public var onUpdate:Function;
		public var onAdd:Function;
		public var onRemove:Function;
		
		public function get maxCount():uint
		{
			return _pool.maxSize;
		}
		
		public function PhysicalSpriteContainer(space:Space, maxCount:uint, provider:*, onUpdate:Function = null, onAdd:Function = null, onRemove:Function = null) 
		{
			_space = space;
			_pool = new ObjectPool(maxCount, provider);
			this.onUpdate = onUpdate;
			this.onAdd = onAdd;
			this.onRemove = onRemove;
		}
		
		public function add(x:Number = 0, y:Number = 0):PhysicalSprite
		{
			var sprite:PhysicalSprite = _pool.borrowObject();
			if (sprite != null)
			{
				sprite.body.space = _space;
				sprite.setTo(x, y);
				if (onAdd != null)
					onAdd(sprite);
				addChild(sprite);
			}
			return sprite;
		}
		
		public function remove(sprite:PhysicalSprite):void
		{
			sprite.body.space = null;
			removeChild(sprite);
			ASSERT(_pool.returnObject(sprite));
		}
		
		public function clear():void
		{
			for (var i:int = numChildren - 1; i >= 0; i--)
			{
				var sprite:PhysicalSprite = getChildAt(i) as PhysicalSprite;
				sprite.body.space = null;
				removeChildAt(i);
				_pool.returnObject(sprite);
				if (onRemove != null)
					onRemove(sprite);
			}
			//ASSERT(_pool.returnAll());
		}
		
		public function update():void
		{
			if (onUpdate != null)
			{
				for (var i:int = numChildren - 1; i >= 0; i--)
				{
					onUpdate(getChildAt(i));
				}
			}
		}
		
		public function findFromBody(body:Body):PhysicalSprite
		{
			for (var i:int = 0; i < numChildren; i++)
			{
				var sprite:PhysicalSprite = getChildAt(i) as PhysicalSprite;
				if (sprite.body == body)
					return sprite;
			}
			return null;
		}
	}

}