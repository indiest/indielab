package  
{
	import com.gamua.flox.Flox;
	import com.gamua.flox.Player;
	import com.gamua.flox.Score;
	import com.gamua.flox.TimeScope;
	import com.greensock.TweenLite;
	import com.junkbyte.console.Cc;
	import com.sociodox.theminer.TheMiner;
	import flash.display.DisplayObject;
	import flash.display.StageScaleMode;
	import flash.events.FocusEvent;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.input.ControllerKey;
	import me.sangtian.common.input.GestouchController;
	import me.sangtian.common.input.GestouchControllerKey;
	import me.sangtian.common.input.InputAction;
	import me.sangtian.common.input.InputAxis;
	import me.sangtian.common.input.InputManager;
	import me.sangtian.common.input.KeyboardController;
	import me.sangtian.common.input.KeyboardControllerKey;
	import me.sangtian.common.input.MouseController;
	import me.sangtian.common.input.MouseControllerKey;
	import me.sangtian.common.logging.FloxOutput;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.logging.LogOutput;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.StringUtil;
CONFIG::air
{
	import flash.desktop.NativeApplication;
}
	import flash.events.MouseEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.ui.Keyboard;
	import me.sangtian.common.game.BaseGame;
	import me.sangtian.common.Input;
	import me.sangtian.common.Time;
	import nape.util.BitmapDebug;
	import nape.util.Debug;
	import net.hires.debug.Stats;
	import treefortress.sound.SoundManager;
	
	/**
	 * ...
	 * @author 
	 */
	public class Game extends BaseGame
	{
		public static const ACTION_ID_SKILL_A:String = "SkillA";
		public static const ACTION_ID_SKILL_B:String = "SkillB";
		
		private static var _instance:Game;
		public static function get instance():Game
		{
			return _instance;
		}
		
		private var _player:MyPlayer;
		public function get player():MyPlayer 
		{
			return _player;
		}
		
		override public function set soundOn(value:Boolean):void
		{
			super.soundOn = value;
			if (currentLevel != null)
				currentLevel.music.mute = !value;
			player.soundOn = value;
		}
		
		public function get currentLevel():BaseLevel
		{
			return stateManager.currentState as BaseLevel;
		}
/*		
		private var _splashes:SpriteSheetAnimation = new SpriteSheetAnimation(
			new Assets.ANIMATIONS_SPLASH, 
			StarlingSpriteSheetData.create(128, 128, 1024, 512),
			1);
		public function get splashes():SpriteSheetAnimation 
		{
			return _splashes;
		}
		
		private var _explosions:SpriteSheetAnimation = new SpriteSheetAnimation(
			new Assets.ANIMATIONS_EXPLOSION, new Assets.ANIMATIONS_EXPLOSION_DATA, 10);
		public function get explosions():SpriteSheetAnimation 
		{
			return _explosions;
		}
*/		
		private var _orbs:SpriteSheetAnimation = new SpriteSheetAnimation(
			new Assets.ANIMATIONS_ORB, new Assets.ANIMATIONS_ORB_DATA, 10);
		public function get orbs():SpriteSheetAnimation
		{
			return _orbs;
		}
		
		public var endlessScores:Vector.<Vector.<Score>> = new Vector.<Vector.<Score>>(4, true);
		
		public function Game() 
		{
			super(360, 640);
			_instance = this;
		}
		
		override protected function init():void 
		{
			CONFIG::debug
			{
				Flox.reportAnalytics = false;
			}
			CONFIG::release
			{
				Log.outputs = new <LogOutput>[new FloxOutput()];
				Flox.reportAnalytics = true;
			}
			Flox.playerClass = MyPlayer;
			Flox.init("Hu6lQ5IqGpt4PSti", "FGUa7OGWJ50kWVsg", appVersion);
			loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, 
				function(event:UncaughtErrorEvent):void
				{
					Flox.logError(event.error, "Uncaught Error: " + event.error.message);
				}
			);
			_player = Player.current as MyPlayer;
			if (_player.appVersion != appVersion)
			{
				_player.reset();
				_player.appVersion = appVersion;
			}
			CONFIG::debug
			{
				//_player.reset();
				//_player.level = 5;
				_player.name = null;
				_player.saveQueued();
			}
			soundOn = _player.soundOn;
			
			input.addController(new MouseController(nativeStage));
			CONFIG::mobile
			{
				var gc:GestouchController = new GestouchController(nativeStage);
				input.addController(gc);
			}
			input.addAction(new InputAction("Left", Vector.<ControllerKey>([
					KeyboardControllerKey.fromName("LEFT"),
					KeyboardControllerKey.fromName("A"),
					MouseControllerKey.DRAG_LEFT
					])
				))
				.addAction(new InputAction("Right", Vector.<ControllerKey>([
					KeyboardControllerKey.fromName("RIGHT"),
					KeyboardControllerKey.fromName("D"),
					MouseControllerKey.DRAG_RIGHT
					])
				))
				.addAction(new InputAction("Up", Vector.<ControllerKey>([
					KeyboardControllerKey.fromName("UP"),
					KeyboardControllerKey.fromName("W"),
					MouseControllerKey.DRAG_UP
					])
				))
				.addAction(new InputAction("Down", Vector.<ControllerKey>([
					KeyboardControllerKey.fromName("DOWN"),
					KeyboardControllerKey.fromName("S"),
					MouseControllerKey.DRAG_DOWN
					])
				))
				.addAxis(new InputAxis(InputAxis.AXIS_ID_X, 
					input.getAction("Left").keys,
					input.getAction("Right").keys
				))
				.addAxis(new InputAxis(InputAxis.AXIS_ID_Y, 
					input.getAction("Up").keys,
					input.getAction("Down").keys
				))
				.addAction(new InputAction(ACTION_ID_SKILL_A, Vector.<ControllerKey>([
					CONFIG::mobile
						gc.addKey(gc.SWIPE_UP),
						KeyboardControllerKey.fromName("SPACE")
					])
				))
				.addAction(new InputAction(ACTION_ID_SKILL_B, Vector.<ControllerKey>([
					CONFIG::mobile
						gc.addKey(gc.SWIPE_DOWN),
						KeyboardControllerKey.fromName("SHIFT")
					])
				))
				.addAction(new InputAction(ACTION_ID_SKILL_A + 0, Vector.<ControllerKey>([
						KeyboardControllerKey.fromKeyCode(Keyboard.NUMBER_1)
					])
				))
				.addAction(new InputAction(ACTION_ID_SKILL_A + 1, Vector.<ControllerKey>([
						KeyboardControllerKey.fromKeyCode(Keyboard.NUMBER_2)
					])
				))
				.addAction(new InputAction(ACTION_ID_SKILL_A + 2, Vector.<ControllerKey>([
						KeyboardControllerKey.fromKeyCode(Keyboard.NUMBER_3)
					])
				));
					
			
			//CONFIG::mobile
			//{
				//Input.mouseControlEnabled = true;
			//}
			
			Font.registerFont(FontBarkentina);

			LevelLoadingState.init("resources.json", MenuState);
			stateManager.loadState(LevelLoadingState);
			
			
			CONFIG::debug
			{
				_napeDebug = new BitmapDebug(stageWidth, stageHeight, nativeStage.color, true);
				addChild(_napeDebug.display);
				
				addChild(new TheMiner());
				
				//Cc.startOnStage(stage, "`");
				//Cc.config.commandLineAllowed = true;
				//Cc.commandLine = true;
			}
		}
		
		private static const LEVELS:Array = [Level1, Level2, Level3, Level4];
		
		public function loadLevel(level:uint, mode:int = 0):void 
		{
			var levelClass:Class = LEVELS[level - 1];
			var levelState:BaseLevel = stateManager.getState(levelClass) as BaseLevel || new levelClass();
			levelState.mode = mode;
			stateManager.loadState(levelState);
		}
		
		public function showCredits():void
		{
			stateManager.loadState(CreditsState);
		}
		
		private var _leaderboardPanel:LeaderboardPanel;
		private var _leaderboardLevel:int;
		private var _startRanking:int;
		private var _playerRanking:int;
		private var _scoreItemNum:int;
		public function showLeaderboard(level:int, playerRanking:int):void
		{
			Flox.logEvent("ShowLeaderboard", { Level: level.toString(), PlayerRanking: playerRanking } );
			_leaderboardLevel = level;
			/*
			if (endlessScores[level - 1] == null)
			{
				Flox.loadScores("endlesslevel" + level, TimeScope.ALL_TIME, onLeaderboardLoaded, onLeaderboardError);
			}
			*/
			if (_leaderboardPanel == null)
			{
				_leaderboardPanel = new LeaderboardPanel();
				_leaderboardPanel.buttonBack.addEventListener(MouseEvent.CLICK, handleClickBack);
				_scoreItemNum = DisplayObjectUtil.getItemMaxNum(_leaderboardPanel, "scoreItem");
				_leaderboardPanel.buttonFirst.addEventListener(MouseEvent.CLICK, handleClickFirst);
				_leaderboardPanel.buttonPrev.addEventListener(MouseEvent.CLICK, handleClickPrev);
				_leaderboardPanel.buttonNext.addEventListener(MouseEvent.CLICK, handleClickNext);
				_leaderboardPanel.buttonLast.addEventListener(MouseEvent.CLICK, handleClickLast);
				_leaderboardPanel.buttonChangeName.addEventListener(MouseEvent.CLICK, handleChangeName);
			}
			
			DisplayObjectUtil.formatText(_leaderboardPanel.textTitle, level);
			_playerRanking = playerRanking;
			var startRanking:int = int(playerRanking / _scoreItemNum) * _scoreItemNum;
			updateLeaderboardItems(startRanking);
			
			addChild(_leaderboardPanel);
		}
		
		private function handleChangeName(e:MouseEvent):void 
		{
			var item:LeaderboardScoreItem = _leaderboardPanel["scoreItem" + (_playerRanking - _startRanking)];			
			if (item == null)
				return;
				
			if (item.textName.type == TextFieldType.DYNAMIC)
			{
				item.textName.type = TextFieldType.INPUT;
				//item.textName.maxChars = 12;
				item.textName.addEventListener(FocusEvent.FOCUS_OUT, handleChangeNameCancel);
				item.textName.setSelection(0, item.textName.length);
				nativeStage.focus = item.textName;
				nativeStage.requestSoftKeyboard();
			}
			else
			{
				item.textName.type = TextFieldType.DYNAMIC;
				item.textName.removeEventListener(FocusEvent.FOCUS_OUT, handleChangeNameCancel);
				player.name = item.textName.text;
				player.saveQueued();
				_leaderboardPanel.buttonChangeName.visible = false;
				nativeStage.focus = null;
			}
		}
		
		private function handleChangeNameCancel(e:FocusEvent):void 
		{
			if (e.relatedObject == _leaderboardPanel.buttonChangeName)
				return;
				
			var item:LeaderboardScoreItem = _leaderboardPanel["scoreItem" + (_playerRanking - _startRanking)];			
			if (item == null)
				return;
			
			var score:Score = endlessScores[_leaderboardLevel - 1][_playerRanking];
			item.textName.type = TextFieldType.DYNAMIC;
			item.textName.text = score.playerName;
			item.textName.removeEventListener(FocusEvent.FOCUS_OUT, handleChangeNameCancel);
			nativeStage.focus = null;
			updateLeaderboardItems(_startRanking);
		}
		
		private function handleClickFirst(e:MouseEvent):void 
		{
			updateLeaderboardItems(0);
		}
		
		private function handleClickPrev(e:MouseEvent):void 
		{
			updateLeaderboardItems(_startRanking - _scoreItemNum);
		}
		
		private function handleClickNext(e:MouseEvent):void 
		{
			updateLeaderboardItems(_startRanking + _scoreItemNum);
		}
		
		private function handleClickLast(e:MouseEvent):void 
		{
			var scores:Vector.<Score> = endlessScores[_leaderboardLevel - 1];
			updateLeaderboardItems(int(scores.length / _scoreItemNum) * _scoreItemNum);
		}
		
		private function updateLeaderboardItems(startRanking:int = 0):void 
		{
			var scores:Vector.<Score> = endlessScores[_leaderboardLevel - 1];
			if (startRanking < 0 || startRanking >= scores.length)
				return;
			_startRanking = startRanking;
			_leaderboardPanel.buttonChangeName.visible = false;
			for (var i:int = 0; i < _scoreItemNum; i++)
			{
				var item:LeaderboardScoreItem = _leaderboardPanel["scoreItem" + i];
				var ranking:int = startRanking + i;
				if (ranking < scores.length)
				{
					item.visible = true;
					var score:Score = scores[ranking];
					item.textRank.text = int(ranking + 1).toString();
					item.textName.text = StringUtil.getFixedString(score.playerName, 15, "...", "", "");
					item.textTime.text = Number(score.value / 10).toFixed(1);
					/*
					DisplayObjectUtil.formatText(item.text, 
						ranking + 1,//StringUtil.getFixedString(ranking + 1, 5),
						StringUtil.getFixedString(score.playerName, 15), 
						score.value / 10);
					*/
					if (ranking == _playerRanking)
					{
						item.alpha = 0;
						TweenLite.to(item, 1.5, { alpha: 1 } );
						if (score.playerName == MyPlayer.DEFAULT_NAME)
						{
							_leaderboardPanel.buttonChangeName.visible = true;
							_leaderboardPanel.buttonChangeName.y = item.y;
						}
						CONFIG::debug
						{
							_leaderboardPanel.buttonChangeName.visible = true;
							_leaderboardPanel.buttonChangeName.y = item.y;
						}
					}
				}
				else
				{
					item.visible = false;
				}
			}
		}
		
		private function handleClickBack(e:MouseEvent):void 
		{
			DisplayObjectUtil.removeFromParent(_leaderboardPanel);
		}
		
		CONFIG::debug
		{
			private var _stats:Stats;
			private var _napeDebug:Debug;
			private var _napeDebugOn:Boolean = false;
		}
		
		override protected function update():void 
		{
			if (isKeyUp(Keyboard.M))
			{
				soundOn = !soundOn;
				Flox.logEvent("SwitchSound", { On: Game.instance.soundOn, Source: "ShortcutKey" } );
			}
			
			CONFIG::debug
			{
				if (isKeyUp(Keyboard.F))
				{
					if (_stats == null)
					{
						_stats = new Stats();
					}
					if (_stats.parent == null)
					{
						addChild(_stats);
					}
					else
					{
						removeChild(_stats);
					}
				}
				
				if (_napeDebugOn)
				{
					_napeDebug.clear();
					_napeDebug.draw(currentLevel.space);
					_napeDebug.flush();
				}
				if (isKeyUp(Keyboard.N))
				{
					_napeDebugOn = !_napeDebugOn;
					_napeDebug.clear();
					_napeDebug.flush();
				}
			
				if (isKeyUp(Keyboard.F1))
				{
					stateManager.loadState(Level1);
				}
				else if (isKeyUp(Keyboard.F2))
				{
					stateManager.loadState(Level2);
				}
				else if (isKeyUp(Keyboard.F3))
				{
					stateManager.loadState(Level3);
				}
				else if (isKeyUp(Keyboard.F4))
				{
					stateManager.loadState(Level4);
				}
				
				if (currentLevel != null)
				{
					for (var i:int = 1; i < 10; i++)
					{
						// NUMPAD_1~9 -> 10%~90%
						if (Game.instance.isKeyUp(Keyboard.NUMPAD_0 + i))
						{
							currentLevel.progress = i / 10;
						}
					}
				}
				
				if (isKeyUp(Keyboard.E))
				{
					showCredits();
				}
				if (isKeyUp(Keyboard.R))
				{
					player.reset();
				}
			}
		}
		
		override protected function onDeactivated():void 
		{
			Flox.logEvent("FPS", { Current: Time.fps, Average: Time.averageFps } );
			//player.saveQueued();
			//Flox.shutdown();
			CONFIG::release
			{
				if (currentLevel != null)
				{
					currentLevel.pause();
				}
				else
				{
					sfx.pauseAll();
					music.pauseAll();
				}
			}
		}
		
		override protected function onActivated():void 
		{
			CONFIG::release
			{
				if (currentLevel != null)
				{
					//currentLevel.resume();
				}
				else
				{
					sfx.resumeAll();
					music.resumeAll();
				}
			}
		}
		
		override protected function onPressBack():void 
		{
			if (currentLevel != null)
			{
				currentLevel.pause();
			}
			else
			{
				CONFIG::air
				{
					NativeApplication.nativeApplication.exit();
				}
			}
		}
	}

}