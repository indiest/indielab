package  
{
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.ui.Keyboard;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.Direction;
	import me.sangtian.common.display.DualImageWrappingScroller;
	import me.sangtian.common.display.MirrorEffect;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.Input;
	import me.sangtian.common.nape.NapeUtil;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.ColorUtil;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	import me.sangtian.gengraphy.CloudsGenerator;
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	
	/**
	 * ...
	 * @author 
	 */
	public class Level2 extends BaseLevel
	{
		public static const CD_ID_GENERATE_ELITE:int = 201;
		public static const CD_ID_GENERATE_GATE:int = 202;
		public static const SCROLL_SPEED:Number = 200;
		public static const BANK_WIDTH:Number = 32;
		
		private static const CB_TYPE_MINE:CbType = new CbType();
		private static const CB_TYPE_ARROW:CbType = new CbType();
		
		private var _bg:Bitmap;
		private var _bgMirrorEffect:MirrorEffect;
		private var _raftWave:StarlingSpriteSheet;
		private var _trees:Bitmap;
		private var _treeScroller:DualImageWrappingScroller;
		private var _mineContainer:PhysicalSpriteContainer;
		private var _gateContainer:PhysicalSpriteContainer;
		private var _arrowContainer:PhysicalSpriteContainer;
		private var _eliteArrowContainer:PhysicalSpriteContainer;
		private var _projectileContainer:PhysicalSpriteContainer;
		private var _enemyBoatContainer:PhysicalSpriteContainer;
		private var _enemyEliteContainer:PhysicalSpriteContainer;
		private var _enemyArtilleryContainer:PhysicalSpriteContainer;
		
		private var _gfxLayer:Sprite = new Sprite();
		private var _explosions:GfxContainer;
		private var _clouds:PhysicalSpriteContainer;
		
		private var _iconBlink:SkillIcon;
		private var _iconSlow:SkillIcon;
		
		override public function get scrollY():Number
		{
			return SCROLL_SPEED * Time.deltaSecond;
		}
		
		override public function get leftBoundary():Number 
		{
			return BANK_WIDTH;
		}
		
		override public function get rightBoundary():Number 
		{
			return Game.instance.stageWidth - BANK_WIDTH;
		}
		
		public function Level2() 
		{
			super(2);
			
			_bg = new Assets.LEVELS_2_BG();
			//addChild(_bg);
			_bgMirrorEffect = new MirrorEffect(_bg);
			//_bgMirrorEffect.displaceScaleX = _bgMirrorEffect.displaceScaleX = 20;
			_bgMirrorEffect.speed = 40;
			_bgMirrorEffect.updateInterval = 0.1;
			addChild(_bgMirrorEffect);
						
			//_space.worldLinearDrag = 10;
			_space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_TYPE_MINE, Raft.CB_TYPE, handleMineCollision));
			//_space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_TYPE_ARROW, CbType.ANY_BODY, handleArrowCollision));
			
			var barriers:Body = NapeUtil.makeBarriers(Game.instance.stageWidth, Game.instance.stageHeight,
				Direction.LEFT | Direction.RIGHT | Direction.UP, -BANK_WIDTH);
			barriers.setShapeMaterials(Material.sand());
			barriers.space = _space;

			_raftWave = new StarlingSpriteSheet(
				AssetsCache.getBitmapData(Assets.ANIMATIONS_RAFT_WAVE),
				StarlingSpriteSheetData.parse(AssetsCache.getOrCacheAsset(Assets.ANIMATIONS_RAFT_WAVE_DATA))
			);
			_raftWave.play();
			addChild(_raftWave);
			
			_raft.scale = _raftWave.scaleX = _raftWave.scaleY = 0.7;
			//_raft.body.mass = 100;
			_raft.animation.frameRate = 30;
			addChild(_raft);
			
			_gateContainer = new PhysicalSpriteContainer(_space, 2, createGate, updateGate);
			addChild(_gateContainer);
			
			_trees = new Assets.LEVELS_2_TREES();
			_treeScroller = new DualImageWrappingScroller(_trees);
			addChild(_treeScroller);
			
			addChild(_gfxLayer);
			_explosions = new GfxContainer(Assets.ANIMATIONS_EXPLOSION, Assets.ANIMATIONS_EXPLOSION_DATA, 10);
			addChild(_explosions);
			
			_mineContainer = new PhysicalSpriteContainer(_space, 10, createMine, updateMine, null, onRemoveMine);
			addChild(_mineContainer);
			
			_arrowContainer = new PhysicalSpriteContainer(_space, 20, createArrow, updateArrow);// , onAddArrow);
			addChild(_arrowContainer);
			
			_eliteArrowContainer = new PhysicalSpriteContainer(_space, 2, createEliteArrow, updateArrow);// , onAddArrow);
			addChild(_eliteArrowContainer);
			
			_projectileContainer = new PhysicalSpriteContainer(_space, 10, createProjectile, updateProjectile);
			addChild(_projectileContainer);
			
			_enemyBoatContainer = new PhysicalSpriteContainer(_space, 10, createEnemyTower, updateEnemy);
			addChild(_enemyBoatContainer);
			
			_enemyEliteContainer = new PhysicalSpriteContainer(_space, 2, createEnemyElite, updateEnemy);
			addChild(_enemyEliteContainer);
			
			_enemyArtilleryContainer = new PhysicalSpriteContainer(_space, 10, createEnemyArtillery, updateEnemy);
			addChild(_enemyArtilleryContainer);
			
			_clouds = new PhysicalSpriteContainer(null, 100, createCloud);
			addChild(_clouds);
			
			_cdm.add(CD_ID_GENERATE_ELITE, 3);
			_cdm.add(CD_ID_GENERATE_GATE, 7, 30);
			_iconBlink = new SkillIcon(AssetsCache.getNewBitmap(Assets.UI_ICON_BLINK), CD_ID_BLINK, _cdm, Game.ACTION_ID_SKILL_A);
			_iconBlink.x = 48;
			_iconBlink.y = Game.instance.stageHeight - 48;
			_uiLayer.addChild(_iconBlink);
			_iconSlow = new SkillIcon(AssetsCache.getNewBitmap(Assets.UI_ICON_SLOW), CD_ID_SLOW, _cdm, Game.ACTION_ID_SKILL_B);
			_iconSlow.x = Game.instance.stageWidth - 48;
			_iconSlow.y = Game.instance.stageHeight - 48;
			_uiLayer.addChild(_iconSlow);
		}
		
		private function createCloud():PhysicalSprite
		{
			return new PhysicalSprite(Assets.LEVELS_2_CLOUD, PhysicalSprite.EMPTY_BODY_NAME);
		}
		
		private function createGate():PhysicalSprite 
		{
			var gate:PhysicalSprite = new PhysicalSprite(Assets.LEVELS_2_GATE);
			gate.body.type = BodyType.KINEMATIC;
			gate.body.setShapeMaterials(Material.glass());
			return gate;
		}
		
		private function createArrow():Bullet 
		{
			var arrow:Bullet = new Bullet(Assets.LEVELS_2_ARROW, PhysicalSprite.EMPTY_BODY_NAME);
			//arrow.body.cbTypes.add(CB_TYPE_ARROW);
			return arrow;
		}
		
		private function createEliteArrow():Bullet 
		{
			var arrow:Bullet = new Bullet(Assets.LEVELS_2_ARROW_RED, PhysicalSprite.EMPTY_BODY_NAME);
			//arrow.body.cbTypes.add(CB_TYPE_ARROW);
			//DisplayObjectUtil.tint(arrow, 0, 0xff0000);
			return arrow;
		}
		
		private function onAddArrow(arrow:Bullet):void
		{
			arrow.y += scrollY;
		}
		
		private function createProjectile():Bullet 
		{
			var projectile:Bullet = new Bullet(Assets.LEVELS_2_PROJECTILE, PhysicalSprite.EMPTY_BODY_NAME);
			return projectile;
		}
		
		private function createEnemyTower():Enemy 
		{
			var enemy:Enemy = new Enemy(_arrowContainer, Assets.LEVELS_2_ENEMY, PhysicalSprite.EMPTY_BODY_NAME);//"enemy");
			enemy.speedY = SCROLL_SPEED;
			enemy.attackRange = 600;
			enemy.bulletDamage = 0.3;
			enemy.bulletSpeed = 400;
			enemy.attackInterval = 2.0;
			enemy.biasRatio = 0.0;
			enemy.shootSound = "arrow";
			return enemy;
		}
		
		private function createEnemyElite():Enemy 
		{
			var elite:Enemy = new Enemy(_eliteArrowContainer, Assets.LEVELS_2_BOSS, PhysicalSprite.EMPTY_BODY_NAME);//"boss");
			elite.speedY = SCROLL_SPEED;
			elite.attackRange = 1000;
			elite.bulletSpeed = 1000;
			elite.bulletDamage = 0.7;
			elite.attackCooldown = 3.0;
			elite.biasRatio = 0;
			elite.shootSound = "arrow_fast";
			return elite;
		}
		
		private function createEnemyArtillery():Enemy 
		{
			var artillery:Enemy = new Enemy(_projectileContainer, Assets.LEVELS_2_ARTILLERY, PhysicalSprite.EMPTY_BODY_NAME);//"artillery");
			artillery.speedY = SCROLL_SPEED;
			artillery.attackRange = 400;
			artillery.bulletDamage = 0.6;
			artillery.bulletSpeed = 300;
			artillery.attackCooldown = 3.0;
			artillery.biasRatio = 0.05;
			artillery.shootSound = "projectile";
			return artillery;
		}
		
		private function createMine():PhysicalSprite 
		{
			var mine:PhysicalSprite = new PhysicalSprite(Assets.LEVELS_2_MINE, "mine");
			mine.body.type = BodyType.DYNAMIC;
			mine.body.mass = 100;
			mine.body.cbTypes.add(CB_TYPE_MINE);
			mine.body.userData.name = "mine";
			return mine;
		}
		
		override protected function enterScene():void
		{
			super.enterScene();
			_raft.x = 180;
			_raft.y = 700;
			_lastMineX = _lastMineY = 0;
			_iconBlink.visible = _iconSlow.visible = false;
			Game.instance.fade(0x00ffffff, 1.0, enterRaft);
			
			if (mode == MODE_NORMAL)
			{
				//Game.instance.music.play("Level2");
				_music.play(Game.instance.music.getSound("Level2").sound);
			}
			else
			{
				//Game.instance.music.playLoop("river").fadeFrom(0, 0.3, 3000);
				_music.playLoop(Game.instance.music.getSound("river").sound, 0.3);			
				_music.fadeIn(3.0);
			}
		}
		
		private function enterRaft():void 
		{
			TweenLite.to(_raft, 3.0, { y: 400, onComplete: finishTransition } );
		}
		
		override protected function updateProgress():void 
		{
			super.updateProgress();
			// 60 seconds
			progress += (0.017 * Time.deltaSecond);
		}
		
		private function updateEnemy(enemy:Enemy):void 
		{
			enemy.update(Time.deltaSecond, _raft.body.position);
		}
		
		private var _arrowTrack:Bitmap = new Assets.LEVELS_2_ARROW_TRACK;
		
		private function updateArrow(arrow:Bullet):void 
		{
			//if (arrow.speed > 800)
			{
				//_gfxLayer.addChild(_arrowTrack);
				//_arrowTrack.x = arrow.tailPoint.x;
				//_arrowTrack.y = arrow.tailPoint.y;
				//_arrowTrack.rotation = arrow.rotation;
				
				var dx:Number = 0.1 * arrow.speed * MathUtil.cosAngle(arrow.rotation);
				var dy:Number = 0.1 * arrow.speed * MathUtil.sinAngle(arrow.rotation);
				//var drawMatrix:Matrix = arrow.transform.matrix.clone();
				//drawMatrix.translate(dx, dy);
				//_gfxLayer.graphics.lineBitmapStyle(arrow.bitmapData, drawMatrix);
				_gfxLayer.graphics.lineStyle(3, 0xffffff);
				_gfxLayer.graphics.lineGradientStyle("linear", [0xffffff, 0xffffff], [1, 0], [0, 255]);
				_gfxLayer.graphics.moveTo(arrow.tailPoint.x, arrow.tailPoint.y);
				_gfxLayer.graphics.lineTo(arrow.x - dx, arrow.y - dy);
			}
			
			arrow.update(Time.deltaSecond);
			if (!arrow.isInSpace)
				return;
			// Arrow can trigger mine explosion
			for (var i:int = 0; i < _mineContainer.numChildren; i++)
			{
				var mine:PhysicalSprite = _mineContainer.getChildAt(i) as PhysicalSprite;
				if (mine.hitTestObject(arrow))
				{
					arrow.removeFromParent();
					onHitMine(mine.body);
					return;
				}
			}
			if (_raft.hitTestObject(arrow))
			{
				//Game.instance.shake(this, 10, 10, 0.1, 2);
				//_raft.body.applyImpulse(Vec2.weak(0, arrow.speed));
				_raft.damage(arrow.damage, 0);
				if (_raft.life > 0)
				{
					arrow.removeFromParent();
				}
				Game.instance.flash(0xffff0000, 0.2, 5);
			}
		}
		
		private function updateProjectile(projectile:Bullet):void 
		{
			if (projectile.dist == 0)
				projectile.distMax = MathUtil.dist(projectile.x, projectile.y, _raft.x, _raft.y);
			projectile.update(Time.deltaSecond);
			projectile.scale = 0.5 + Math.sin(projectile.dist / projectile.distMax * Math.PI);
			if (projectile.dist >= projectile.distMax)// * 0.9)
			{
				explode(projectile.x, projectile.y, 50, projectile.damage);
			}
		}
		
		private function explode(x:Number, y:Number, radius:Number, maxDamage:Number):void
		{
			var dist:Number = MathUtil.dist(_raft.x, _raft.y, x, y);
			var ratio:Number = MathUtil.clamp(1 - dist / radius, 0, 1);
			_explosions.add(x, y);
			Game.instance.sfx.playFx("explosion", 0.2 + ratio * 0.2);
			if (dist < radius)
			{
				_raft.damage(maxDamage * ratio, 1);
				Game.instance.shake(this, 10, 10, 0.2, 3);
				Game.instance.flash(0xffff0000, 0.2, 5);
			}
			
			// Explosion can blast mine 'instantly'
			for (var i:int = 0; i < _mineContainer.numChildren; i++)
			{
				var mine:PhysicalSprite = _mineContainer.getChildAt(i) as PhysicalSprite;
				if (MathUtil.compareDistance(x - mine.x, y - mine.y, radius) < 0)
				{
					if (Number(mine.body.userData.timing) <= 0)
					{
						mine.body.userData.timing = 0.1;
					}
					break;
				}
			}
		}
		
		private function handleMineCollision(cb:InteractionCallback):void 
		{
			onHitMine(cb.int1.castBody);
		}
		
		private function onHitMine(mineBody:Body):void
		{
			if (!mineBody.userData.timing || mineBody.userData.timing <= 0)
			{
				mineBody.userData.timing = 1;
				Game.instance.sfx.playFx("hit_mine");
			}
		}
		
		private function onRemoveMine(mine:PhysicalSprite):void
		{
			mine.body.userData.timing = 0;
		}
		
		private function updateMine(mine:PhysicalSprite):void
		{
			_lastMineY += scrollY;
			//mine.body.velocity.y = SCROLL_SPEED;
			mine.x = mine.body.position.x;
			mine.y = mine.body.position.y = mine.y + scrollY;
			//mine.rotation = mine.body.rotation;
			if (Game.instance.isOutOfScreen(mine, Direction.DOWN))
			{
				mine.removeFromParent();
			}
			else if (mine.body.userData.timing > 0)
			{
				DisplayObjectUtil.tint(mine, 0.5, ColorUtil.rgb(
					0xff * (1 - mine.body.userData.timing), 0, 0));
				if ((mine.body.userData.timing -= Time.deltaSecond) <= 0)
				{
					mine.removeFromParent();
					explode(mine.x, mine.y, mine.width * 2, 1.0);
				}
			}
		}
		
		private function updateGate(gate:PhysicalSprite):void 
		{
			var xDir:Number = MathUtil.sign(Game.instance.stageWidth * 0.5 - gate.x);
			gate.setTo(gate.x + 100 * Time.deltaSecond * xDir,
						gate.y + scrollY);
			Game.instance.sfx.play("stone_moving", 0.7, 0, 0, false, false);
			//trace(gate.x, gate.y);
			if (Game.instance.isOutOfScreen(gate, Direction.DOWN))
			{
				gate.removeFromParent();
				Game.instance.sfx.fadeTo("stone_moving", 0);
			}
		}
		
		override public function update():void 
		{
			super.update();

			// Always update water reflection effect and raft's animations
			_bgMirrorEffect.update(Time.deltaSecond);
			_raft.animation.updateFrame(Time.deltaSecond);
			_raftWave.updateFrame(Time.deltaTime);
			_raftWave.x = _raft.x -_raftWave.maxWidth * 0.5 + 1;
			_raftWave.y =  _raft.y -_raftWave.maxHeight * 0.5 - 3;
		}
		
		override protected function updateScene():void 
		{
			_gfxLayer.graphics.clear();
			_explosions.update();

			_treeScroller.scrollY += scrollY;
			
			_raft.update(Time.deltaSecond);
			
			_arrowContainer.update();
			_eliteArrowContainer.update();
			_projectileContainer.update();
			
			_enemyBoatContainer.update();
			if (progress > 0.1 && progress < lastEnemyProgress && Random.randomBoolean(0.01 * difficulty))
			{
				var enemy:Enemy = _enemyBoatContainer.add() as Enemy;
				if (enemy != null)
				{
					enemy.initPosition(BANK_WIDTH);
				}
			}
			
			_enemyEliteContainer.update();
			if (progress > 0.2 && progress < lastEnemyProgress &&
				Random.randomBoolean(0.01 * difficulty * (1 + progress)) &&
				_cdm.isCooledDown(CD_ID_GENERATE_ELITE))
			{
				var elite:Enemy = _enemyEliteContainer.add() as Enemy;
				if (elite != null)
				{
					elite.initPosition(BANK_WIDTH);
					onUseSkill(CD_ID_GENERATE_ELITE);
				}
			}
			
			_enemyArtilleryContainer.update();
			if (progress > 0.4 && progress < lastEnemyProgress &&
				Random.randomBoolean(0.006 * difficulty * (1 + progress)))
			{
				var artillery:Enemy = _enemyArtilleryContainer.add() as Enemy;
				if (artillery != null)
				{
					artillery.initPosition(BANK_WIDTH);
				}
			}
			
			_mineContainer.update();
			if (progress > 0.0 && progress < lastEnemyProgress && Random.randomBoolean(0.01 * difficulty * (1 + progress)))
			{
				var mineX:Number = Game.instance.stageWidth * 0.5 + Random.rangeInt( -120, 120);
				var mineY:Number = -100;
				if (MathUtil.compareDistance(mineX - _lastMineX, mineY - _lastMineY, 120) > 0)
				{
					_lastMineX = mineX;
					_lastMineY = mineY;
					var mine:PhysicalSprite = _mineContainer.add(mineX, mineY);
					if (mine != null)
					{
						DisplayObjectUtil.tint(mine);
						mine.scale = Random.rangeNumber(0.7, 1);
					}
				}
			}
			
			_gateContainer.update();
			if (progress < lastEnemyProgress &&// Random.randomBoolean(0.01 * difficulty) &&
				_cdm.isCooledDown(CD_ID_GENERATE_GATE))
			{
				var gate:PhysicalSprite = _gateContainer.add();
				if (gate != null)
				{
					gate.setTo(Random.randomBoolean() ? -gate.width * 0.5 : Game.instance.stageWidth + gate.width * 0.5, -gate.height * 0.5);
					onUseSkill(CD_ID_GENERATE_GATE);
				}
			}
			
			if (mode == MODE_NORMAL && justPassedProgress(0.95))
			{
				for (var i:int = 0; i < _clouds.maxCount; i++)
				{
					var cloud:PhysicalSprite = _clouds.add(
						Random.rangeNumber(0, Game.instance.stageWidth),
						-10 * i//Random.rangeNumber( -200, -100);
					);
					cloud.alpha = i < 30 ? i / 30 : 1;
					cloud.scale = Random.rangeNumber(1, 2);
					//cloud.cacheAsBitmap = true;
					TweenLite.to(cloud, 3.0 + i / 10, {/* x: Game.instance.stageWidth * 0.5,*/ y: Game.instance.stageHeight } );
				}
			}
			/*
			if (progress > 0.95)
			{
				addChild(_clouds);
				_cloudsGenerator.offset.offset(0, scrollY);
				_cloudsGenerator.generateToBitmapData(_clouds.bitmapData);
			}
		private var _clouds:Bitmap = new Bitmap(new BitmapData(Game.instance.stageWidth, Game.instance.stageHeight, true, 0));
		private var _cloudsGenerator:CloudsGenerator = new CloudsGenerator();
			*/
		}
		
		private var _lastMineX:Number;
		private var _lastMineY:Number;
		
		override protected function updateSkill():void 
		{
			super.updateSkill();
			
			_iconBlink.update();
			_iconSlow.update();
			
			// Swipe up or press SPACE to blink
			if (Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_A)
				//(_iconBlink.isPressed || _swipeOffsetY <= -1 || Input.getKeyDown(Keyboard.SPACE))
				&& _cdm.isCooledDown(CD_ID_BLINK))
			{
				_raft.setTo(_raft.x, _raft.y - 200);
				onUseSkill(CD_ID_BLINK);
				Game.instance.sfx.play("blink", 0.5);
			}
			/*
			if (_leapTimer > 0)
			{
				_leapTimer -= Time.deltaSecond;
				if (_leapTimer <= 0)
				{
					resumeCanoe();
				}
			}
			*/
			// Swipe down or press SHIFT to slow down timing
			if (Time.scale < 1)
			{
				Time.scale += 0.006;
				//_music.speed += 0.006;
			}
			else if (Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_B)
				//(_iconSlow.isPressed || _swipeOffsetY >= 1 || Input.getKeyDown(Keyboard.SHIFT))
				&& _cdm.isCooledDown(CD_ID_SLOW))
			{
				Time.scale = 0.2;
				//_music.speed = 0.2;
				onUseSkill(CD_ID_SLOW);
			}
			
		}
		
		override protected function clear():void 
		{
			super.clear();
			_enemyBoatContainer.clear();
			_enemyEliteContainer.clear();
			_enemyArtilleryContainer.clear();
			_arrowContainer.clear();
			_eliteArrowContainer.clear();
			_projectileContainer.clear();
			_mineContainer.clear();
			_gateContainer.clear();
			_gfxLayer.graphics.clear();
			_gfxLayer.removeChildren();
			_explosions.clear();
			_clouds.clear();
		}
		
		override protected function getSkillUpgradeName1():String 
		{
			return "Blink";
		}
		
		override protected function getSkillUpgradeName2():String 
		{
			return "Invisibility";
		}
		
		override protected function getSkillUpgradeIcon1():DisplayObject 
		{
			var icon:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_BLINK);
			icon.width = icon.height = 48;
			return icon;
		}
		
		override protected function getSkillUpgradeIcon2():DisplayObject 
		{
			var icon:Bitmap = AssetsCache.getNewBitmap(Assets.UI_ICON_INVISIBLE);
			icon.width = icon.height = 48;
			return icon;
		}
		
		override protected function performUpgradeSkill(level:int, value:Number):void
		{
			_cdm.setCooldown(CD_ID_BLINK, CD_BLINK * (1 - value / 100));
		}
	}

}