package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.nape.NapeUtil;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	import nape.phys.Body;
	import nape.phys.BodyType;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class PhysicalSprite extends Sprite 
	{
		public static const EMPTY_BODY_NAME:String = "empty";
		
		private static var _generatedBodyCache:Dictionary = new Dictionary();

		private var _assetClass:Class;
		
		public function get assetClass():Class 
		{
			return _assetClass;
		}
		
		private var _bodyName:String;
		public function get bodyName():String 
		{
			return _bodyName;
		}

		private var _bitmap:Bitmap;
		public function get bitmap():Bitmap 
		{
			return _bitmap;
		}
		
		private var _body:Body;
		public function get body():Body 
		{
			return _body;
		}
		
		public function get isInSpace():Boolean
		{
			return _body.space != null;
		}
		
		private var _collisionEnabled:Boolean = true;
		
		public function get collisionEnabled():Boolean 
		{
			return _collisionEnabled;
		}
		
		public function set collisionEnabled(value:Boolean):void 
		{
			_collisionEnabled = value;
			NapeUtil.setBodySensorEnabled(body, !value);
		}
		
		private var _scale:Number = 1;
		public function get scale():Number 
		{
			return _scale;
		}
		public function set scale(value:Number):void 
		{
			body.scaleShapes(1 / _scale, 1 / _scale);
			_scale = value;
			scaleX = scaleY = value;
			body.scaleShapes(value, value);
		}
		
		public function PhysicalSprite(assetClass:Class, bodyName:String = null) 
		{
			_assetClass = assetClass;
			_bodyName = bodyName;
			initBitmap();
			initBody();
		}
		
		protected function initBitmap():void 
		{
			_bitmap = AssetsCache.getNewBitmap(assetClass);
			addChild(DisplayObjectUtil.pivot(_bitmap));
		}
		
		protected function initBody():void
		{
			if (bodyName == null)
			{
				var cachedBody:Body = _generatedBodyCache[assetClass];
				if (cachedBody == null)
				{
					cachedBody = NapeUtil.createBodyFromBitmapData(_bitmap.bitmapData);
					_generatedBodyCache[assetClass] = cachedBody;
				}
				_body = cachedBody.copy();
			}
			else if (bodyName == EMPTY_BODY_NAME)
			{
				_body = new Body(BodyType.KINEMATIC);
			}
			else
			{
				_body = PhysicsData.createBody(bodyName);
			}
		}
		
		public function setTo(x:Number, y:Number):void
		{
			this.x = body.position.x = x;
			this.y = body.position.y = y;
		}
		
		public function removeFromParent():void
		{
			if (parent is PhysicalSpriteContainer)
			{
				(parent as PhysicalSpriteContainer).remove(this);
			}
			else
			{
				DisplayObjectUtil.removeFromParent(this);
				body.space = null;
			}
		}
		
		public function getDistanceFrom(target:PhysicalSprite):Number
		{
			return MathUtil.dist(x, y, target.x, target.y);
		}
		
		/*
		private var _motionBlur:Boolean = false;
		public function get motionBlur():Boolean 
		{
			return _motionBlur;
		}
		public function set motionBlur(value:Boolean):void 
		{
			if (_motionBlur == value)
				return;
			_motionBlur = value;
			if (value)
			{
				_blurBitmap.bitmapData = new BitmapData(bitmap.width + 30, bitmap.height + 30, true, 0);
				_blurBitmap.x = bitmap.x;
				_blurBitmap.y = bitmap.y;
				addChildAt(_blurBitmap, 0);
				//addChild(_blurBitmap);
			}
			else
			{
				DisplayObjectUtil.removeFromParent(_blurBitmap);
			}
		}
		private var _blurBitmap:Bitmap = new Bitmap();
		private var _blurFilter:BlurFilter = new BlurFilter(0, 0);
		private var _blurPointHelper:Point = new Point();
		
		public function updateMotionBlur(dx:Number, dy:Number):void
		{
			if (!motionBlur)
				return;
				
			_blurFilter.blurX = dx;
			_blurFilter.blurY = dy;
			//trace(_blurFilter.blurX, _blurFilter.blurY, _blurFilter.quality);
			_blurBitmap.bitmapData.applyFilter(bitmap.bitmapData,
				bitmap.bitmapData.rect, _blurPointHelper, _blurFilter);
			_blurBitmap.x = bitmap.x - dx;
			_blurBitmap.y = bitmap.y - dy;
		}
		*/
	}

}