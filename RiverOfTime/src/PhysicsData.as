package {

import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Shape;
import nape.shape.Polygon;
import nape.shape.Circle;
import nape.geom.Vec2;
import nape.dynamics.InteractionFilter;
import nape.phys.Material;
import nape.phys.FluidProperties;
import nape.callbacks.CbType;
import nape.geom.AABB;

import flash.display.DisplayObject;
import flash.geom.Rectangle;
import flash.utils.Dictionary;

public class PhysicsData {

	public static function createBody(name:String):Body {
		var xret:BodyPair = lookup(name);
		return xret.body.copy();
	}

	public static function registerMaterial(name:String,material:Material):void {
		if(materials==null) materials = new Dictionary();
		materials[name] = material;	
	}
	public static function registerFilter(name:String,filter:InteractionFilter):void {
		if(filters==null) filters = new Dictionary();
		filters[name] = filter;
	}
	public static function registerFluidProperties(name:String,properties:FluidProperties):void {
		if(fprops==null) fprops = new Dictionary();
		fprops[name] = properties;
	}
	public static function registerCbType(name:String,cbType:CbType):void {
		if(types==null) types = new Dictionary();
		types[name] = cbType;
	}

	//----------------------------------------------------------------------	

	private static var bodies   :Dictionary;
	private static var materials:Dictionary;
	private static var filters  :Dictionary;
	private static var fprops   :Dictionary;
	private static var types    :Dictionary;
	private static function material(name:String):Material {
		if(name=="default") return new Material();
		else {
			if(materials==null || materials[name] === undefined)
				throw "Error: Material with name '"+name+"' has not been registered";
			return materials[name] as Material;
		}
	}
	private static function filter(name:String):InteractionFilter {
		if(name=="default") return new InteractionFilter();
		else {
			if(filters==null || filters[name] === undefined)
				throw "Error: InteractionFilter with name '"+name+"' has not been registered";
			return filters[name] as InteractionFilter;
		}
	}
	private static function fprop(name:String):FluidProperties {
		if(name=="default") return new FluidProperties();
		else {
			if(fprops==null || fprops[name] === undefined)
				throw "Error: FluidProperties with name '"+name+"' has not been registered";
			return fprops[name] as FluidProperties;
		}
	}
	private static function cbtype(name:String):CbType {
		if(name=="null") return null;
		else {
			if(types==null || types[name] === undefined)
				throw "Error: CbType with name '"+name+"' has not been registered";
			return types[name] as CbType;
		}	
	}

	private static function lookup(name:String):BodyPair {
		if(bodies==null) init();
		if(bodies[name] === undefined) throw "Error: Body with name '"+name+"' does not exist";
		return bodies[name] as BodyPair;
	}

	//----------------------------------------------------------------------	

	private static function init():void {
		bodies = new Dictionary();

		var body:Body;
		var mat:Material;
		var filt:InteractionFilter;
		var prop:FluidProperties;
		var bodyCbType:CbType;
		var cbType:CbType;
		var s:Shape;
		var anchor:Vec2;

		
			body = new Body();
			bodyCbType = cbtype("null");
			if (bodyCbType != null) body.cbTypes.add(bodyCbType);

			
				mat = material("default");
				filt = filter("default");
				prop = fprop("default");
				cbType = cbtype("null");

				
					
						s = new Polygon(
							[   Vec2.weak(41,96)   ,  Vec2.weak(41,0)   ,  Vec2.weak(3,0)   ,  Vec2.weak(3,96)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
				
			

			anchor = (true) ? body.localCOM.copy() : Vec2.get(21.5,48);
			body.translateShapes(Vec2.weak(-anchor.x,-anchor.y));
			body.position.setxy(0,0);

			bodies["canoe"] = new BodyPair(body,anchor);
		
			body = new Body();
			bodyCbType = cbtype("null");
			if (bodyCbType != null) body.cbTypes.add(bodyCbType);

			
				mat = material("default");
				filt = filter("default");
				prop = fprop("default");
				cbType = cbtype("null");

				
					
						s = new Polygon(
							[   Vec2.weak(22,0)   ,  Vec2.weak(14,0)   ,  Vec2.weak(0,16)   ,  Vec2.weak(1,27)   ,  Vec2.weak(6,32.8000001907349)   ,  Vec2.weak(27,9)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(22,48)   ,  Vec2.weak(27,42)   ,  Vec2.weak(6,32.8000001907349)   ,  Vec2.weak(14,48)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(27,42)   ,  Vec2.weak(42,33)   ,  Vec2.weak(44,20)   ,  Vec2.weak(38,12)   ,  Vec2.weak(27,9)   ,  Vec2.weak(6,32.8000001907349)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
				
			

			anchor = (true) ? body.localCOM.copy() : Vec2.get(22,24);
			body.translateShapes(Vec2.weak(-anchor.x,-anchor.y));
			body.position.setxy(0,0);

			bodies["rock1"] = new BodyPair(body,anchor);
		
			body = new Body();
			bodyCbType = cbtype("null");
			if (bodyCbType != null) body.cbTypes.add(bodyCbType);

			
				mat = material("default");
				filt = filter("default");
				prop = fprop("default");
				cbType = cbtype("null");

				
					
						s = new Polygon(
							[   Vec2.weak(35,39)   ,  Vec2.weak(38,24)   ,  Vec2.weak(38,10)   ,  Vec2.weak(13,8)   ,  Vec2.weak(3,10)   ,  Vec2.weak(3,16)   ,  Vec2.weak(15,34)   ,  Vec2.weak(23,41)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(16,0)   ,  Vec2.weak(13,8)   ,  Vec2.weak(38,10)   ,  Vec2.weak(22,0)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(9,38)   ,  Vec2.weak(15,34)   ,  Vec2.weak(3,16)   ,  Vec2.weak(0,21)   ,  Vec2.weak(1,35)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
				
			

			anchor = (true) ? body.localCOM.copy() : Vec2.get(19,20.5);
			body.translateShapes(Vec2.weak(-anchor.x,-anchor.y));
			body.position.setxy(0,0);

			bodies["rock2"] = new BodyPair(body,anchor);
		
			body = new Body();
			bodyCbType = cbtype("null");
			if (bodyCbType != null) body.cbTypes.add(bodyCbType);

			
				mat = material("default");
				filt = filter("default");
				prop = fprop("default");
				cbType = cbtype("null");

				
					
						s = new Polygon(
							[   Vec2.weak(15,34)   ,  Vec2.weak(23,34)   ,  Vec2.weak(26,30)   ,  Vec2.weak(31,6)   ,  Vec2.weak(26,2)   ,  Vec2.weak(9,0)   ,  Vec2.weak(9,27)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(0,20)   ,  Vec2.weak(9,27)   ,  Vec2.weak(9,0)   ,  Vec2.weak(1,3)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(32,29)   ,  Vec2.weak(31,6)   ,  Vec2.weak(26,30)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
				
			

			anchor = (true) ? body.localCOM.copy() : Vec2.get(16,17.5);
			body.translateShapes(Vec2.weak(-anchor.x,-anchor.y));
			body.position.setxy(0,0);

			bodies["rock3"] = new BodyPair(body,anchor);
		
			body = new Body();
			bodyCbType = cbtype("null");
			if (bodyCbType != null) body.cbTypes.add(bodyCbType);

			
				mat = material("default");
				filt = filter("default");
				prop = fprop("default");
				cbType = cbtype("null");

				
					
						s = new Polygon(
							[   Vec2.weak(19,0)   ,  Vec2.weak(7,1)   ,  Vec2.weak(0,6)   ,  Vec2.weak(0,29)   ,  Vec2.weak(4,33)   ,  Vec2.weak(17,32)   ,  Vec2.weak(20.6000003814697,6.60000038146973)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(28,13)   ,  Vec2.weak(20.6000003814697,6.60000038146973)   ,  Vec2.weak(17,32)   ,  Vec2.weak(28,23)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
				
			

			anchor = (true) ? body.localCOM.copy() : Vec2.get(14,17);
			body.translateShapes(Vec2.weak(-anchor.x,-anchor.y));
			body.position.setxy(0,0);

			bodies["rock4"] = new BodyPair(body,anchor);
		
			body = new Body();
			bodyCbType = cbtype("null");
			if (bodyCbType != null) body.cbTypes.add(bodyCbType);

			
				mat = material("default");
				filt = filter("default");
				prop = fprop("default");
				cbType = cbtype("null");

				
					
						s = new Polygon(
							[   Vec2.weak(28,72)   ,  Vec2.weak(31,68)   ,  Vec2.weak(27,67)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(3,54)   ,  Vec2.weak(10,53)   ,  Vec2.weak(7,48)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(0,39)   ,  Vec2.weak(6,43)   ,  Vec2.weak(5,36)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(43,2)   ,  Vec2.weak(39,7)   ,  Vec2.weak(44,8)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(67,20)   ,  Vec2.weak(60,20)   ,  Vec2.weak(64,28)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(21,4)   ,  Vec2.weak(22,10)   ,  Vec2.weak(27,8)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(55,65)   ,  Vec2.weak(56,59)   ,  Vec2.weak(49,64)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(10,66)   ,  Vec2.weak(17,62)   ,  Vec2.weak(12,59)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(4,19)   ,  Vec2.weak(7,28)   ,  Vec2.weak(11,19)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(5,61)   ,  Vec2.weak(12,59)   ,  Vec2.weak(10,53)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(0,45)   ,  Vec2.weak(7,48)   ,  Vec2.weak(6,43)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(60,11)   ,  Vec2.weak(53,12)   ,  Vec2.weak(60,20)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(49,70)   ,  Vec2.weak(49,64)   ,  Vec2.weak(41,67)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(1,28)   ,  Vec2.weak(5,36)   ,  Vec2.weak(7,28)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(71,32)   ,  Vec2.weak(65,31)   ,  Vec2.weak(66,35)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(55,5)   ,  Vec2.weak(49,9)   ,  Vec2.weak(53,12)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(61,60)   ,  Vec2.weak(61,53)   ,  Vec2.weak(56,59)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(12,10)   ,  Vec2.weak(11,19)   ,  Vec2.weak(17,13)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(39,73)   ,  Vec2.weak(41,67)   ,  Vec2.weak(31,68)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(33,1)   ,  Vec2.weak(31,7)   ,  Vec2.weak(35,7)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(19,69)   ,  Vec2.weak(22,66)   ,  Vec2.weak(17,62)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(66,52)   ,  Vec2.weak(65,45)   ,  Vec2.weak(61,53)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(70,45)   ,  Vec2.weak(66,35)   ,  Vec2.weak(65,45)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(12,59)   ,  Vec2.weak(17,62)   ,  Vec2.weak(27,67)   ,  Vec2.weak(64,28)   ,  Vec2.weak(35,7)   ,  Vec2.weak(22,10)   ,  Vec2.weak(17,13)   ,  Vec2.weak(10,53)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(49,9)   ,  Vec2.weak(39,7)   ,  Vec2.weak(35,7)   ,  Vec2.weak(64,28)   ,  Vec2.weak(60,20)   ,  Vec2.weak(53,12)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(22,66)   ,  Vec2.weak(27,67)   ,  Vec2.weak(17,62)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(5,36)   ,  Vec2.weak(6,43)   ,  Vec2.weak(7,48)   ,  Vec2.weak(10,53)   ,  Vec2.weak(17,13)   ,  Vec2.weak(11,19)   ,  Vec2.weak(7,28)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(65,45)   ,  Vec2.weak(66,35)   ,  Vec2.weak(65,31)   ,  Vec2.weak(64,28)   ,  Vec2.weak(31,68)   ,  Vec2.weak(41,67)   ,  Vec2.weak(56,59)   ,  Vec2.weak(61,53)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(49,64)   ,  Vec2.weak(56,59)   ,  Vec2.weak(41,67)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(31,7)   ,  Vec2.weak(27,8)   ,  Vec2.weak(22,10)   ,  Vec2.weak(35,7)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
						s = new Polygon(
							[   Vec2.weak(27,67)   ,  Vec2.weak(31,68)   ,  Vec2.weak(64,28)   ],
							mat,
							filt
						);
						s.body = body;
						s.fluidEnabled = false;
						s.fluidProperties = prop;
						if (cbType != null)	s.cbTypes.add(cbType);
					
				
			

			anchor = (true) ? body.localCOM.copy() : Vec2.get(35.50000002,37.00000036);
			body.translateShapes(Vec2.weak(-anchor.x,-anchor.y));
			body.position.setxy(0,0);

			bodies["mine"] = new BodyPair(body,anchor);
		
	}
}
}

import nape.phys.Body;
import nape.geom.Vec2;

class BodyPair {
	public var body:Body;
	public var anchor:Vec2;
	public function BodyPair(body:Body,anchor:Vec2):void {
		this.body = body;
		this.anchor = anchor;
	}
}
