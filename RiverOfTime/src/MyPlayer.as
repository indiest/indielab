package  
{
	import com.gamua.flox.Player;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class MyPlayer extends Player
	{
		public static const DEFAULT_NAME:String = "guest";
		
		public var appVersion:String;
		public var level:int;
		public var tutorialRecord:Object;
		public var bestTimeInEndlessLevels:Vector.<Number>;
		// Settings
		public var soundOn:Boolean = true;
		// Achievements
		
		private var _name:String;
		public function get name():String
		{
			if (_name == null)
				_name = DEFAULT_NAME;//"Player" + id;
			return _name;
		}
		public function set name(value:String):void
		{
			_name = value;
		}
		
		public function MyPlayer() 
		{
			reset();
		}
		
		public function reset():void 
		{
			level = 0;
			tutorialRecord = { };
			bestTimeInEndlessLevels = Vector.<Number>([0, 0, 0, 0]);
		}
		
		public function getBestTime(level:int):Number
		{
			return bestTimeInEndlessLevels[level - 1];
		}
		
		public function setBestTime(level:int, time:Number):void
		{
			bestTimeInEndlessLevels[level - 1] = time;
		}
	}

}