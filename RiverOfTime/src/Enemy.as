package  
{
	import flash.geom.Point;
	import me.sangtian.common.Direction;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	import nape.geom.Vec2;
	import nape.phys.BodyType;
	import nape.phys.Material;
	
	/**
	 * ...
	 * @author 
	 */
	public class Enemy extends PhysicalSprite 
	{
		public var bulletContainer:PhysicalSpriteContainer;
		
		public var speedX:Number = 0;
		public var speedY:Number = 100;
		public var attackInterval:Number = 2.0;
		public var attackRange:Number = 800;
		public var bulletDamage:Number = 0.3;
		public var bulletSpeed:Number = 400;
		public var biasRatio:Number = 0.1;
		public var shootPosition:Point = null;
		public var shootOffsetX:Number = 0;
		public var shootOffsetY:Number = 0;
		public var shootSound:String;
		
		public var attackCooldown:Number = 0;
		
		public function Enemy(bulletContainer:PhysicalSpriteContainer = null, asset:Class = null, bodyName:String = null) 
		{
			super(asset, bodyName);
			this.bulletContainer = bulletContainer;
			body.allowRotation = false;
			//body.setShapeMaterials(Material.wood());
			//body.mass = 0.1;
			//body.type = BodyType.KINEMATIC;//BodyType.DYNAMIC;//
		}
		
		public function initPosition(bankWidth:Number, startX:Number = 0):void 
		{
			setTo(Random.rangeNumber(startX, bankWidth), -height >> 1);
			if (Random.randomBoolean())
			{
				setTo(Game.instance.stageWidth - x, y);
				scale = -1;
			}
			else
			{
				scale = 1;
			}
		}
		
		public function update(deltaSecond:Number, target:Vec2):void
		{
			move(deltaSecond);
			
			if (Game.instance.isOutOfScreen(this, Direction.DOWN))
			{
				removeFromParent();
				return;
			}
			
			if (bulletContainer != null)
			{
				// Enemy's attack rate is affected by time scale.
				attackCooldown -= deltaSecond;
				if (target != null)
				{
					if (MathUtil.compareDistance(target.x - x, target.y - y, attackRange) < 0)
					{
						if (attackCooldown <= 0)
						{
							attackCooldown = attackInterval;
							attack(target.x, target.y, bulletSpeed, biasRatio);
						}
					}
				}
			}
		}
		
		public function move(deltaSecond:Number):void
		{
			//body.velocity.x = speedX;
			//body.velocity.y = speedY;
			// Only set bodies' positions can't cause collision 
			setTo(x + speedX * deltaSecond, y + speedY * deltaSecond);
			//if (speedY > 0)
			//trace(body.position);
		}
		
		public function attack(targetX:Number, targetY:Number, bulletSpeed:Number, biasRatio:Number):void
		{
			var bullet:Bullet;
			if (shootPosition == null)
			{
				bullet = bulletContainer.add(x + scaleX * shootOffsetX, y + scaleY * shootOffsetY) as Bullet;
			}
			else
			{
				bullet = bulletContainer.add(shootPosition.x, shootPosition.y) as Bullet;
			}
			if (bullet == null)
				return;
			bullet.speed = bulletSpeed;
			bullet.rotation = MathUtil.angle(targetX - bullet.x, targetY - bullet.y) +
				Random.angle * biasRatio;
			bullet.damage = bulletDamage;
			bullet.dist = 0;
			bullet.distMax = attackRange;
			if (shootSound != null)
				Game.instance.sfx.play(shootSound);
		}
	}

}