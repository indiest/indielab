package  
{
	import com.gamua.flox.Flox;
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.Direction;
	import me.sangtian.common.display.DualImageWrappingScroller;
	import me.sangtian.common.display.PixelPerfectCollisionDetector;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.Input;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.nape.NapeUtil;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	import me.sangtian.common.util.Random;
	import nape.phys.Body;
	import nape.phys.Material;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Level4 extends BaseLevel
	{
		public static const SCROLL_SPEED:Number = 100;
		public static const BANK_WIDTH:Number = 50;
		public static const SHIELD_RADIUS:Number = 32;
		public static const BOSS_PROGRESS:Number = 0.7;
		public static const END_PROGRESS:Number = 0.975;
		public static const CD_ID_GENERATE_PLASMA_TURRET:int = 401;
		
		private var _bg:Bitmap;
		private var _bgScrollLT:Bitmap;
		private var _bgScrollLB:Bitmap;
		private var _bgScrollRT:Bitmap;
		private var _bgScrollRB:Bitmap;
		private var _bgScrollerL:DualImageWrappingScroller;
		private var _bgScrollerR:DualImageWrappingScroller;
		private var _raftCollisionDetector:PixelPerfectCollisionDetector;
		//private var _photonBulletContainer:PhysicalSpriteContainer;
		//private var _laserBulletContainer:PhysicalSpriteContainer;
		//private var _tachyonBulletContainer:PhysicalSpriteContainer;
		//private var _plasmaBulletContainer:PhysicalSpriteContainer;
		// 0 = Laser, 1 = Photon, 2 = Tachyon, 3 = Plasma
		private var _bulletContainers:Vector.<PhysicalSpriteContainer> = new Vector.<PhysicalSpriteContainer>(4, true);
		//private var _photonTurretContainer:PhysicalSpriteContainer;
		//private var _laserTurretContainer:PhysicalSpriteContainer;
		//private var _tachyonTurretContainer:PhysicalSpriteContainer;
		//private var _plasmaTurretContainer:PhysicalSpriteContainer;
		private var _turretContainers:Vector.<PhysicalSpriteContainer> = new Vector.<PhysicalSpriteContainer>(4, true);
		private var _bossLayer:Sprite = new Sprite();
		private var _boss:Level4Boss;
		private var _bossVelocity:Point = new Point();
		private var _script:PhaseScript;
		
		private var _shieldNull:Sprite = new Sprite();
		//private var _shieldLaser:Sprite = new Sprite();
		//private var _shieldPhoton:Sprite = new Sprite();
		//private var _shieldTachyon:Sprite = new Sprite();
		private var _shields:Vector.<Sprite> = new Vector.<Sprite>(3, true);
		private var _currentShield:Sprite = _shieldNull;
		private var _shieldContainer:Sprite = new Sprite();
		
		private var _gfxLayer:Sprite = new Sprite();
		
		//private var _iconShieldLaser:SkillIcon;
		//private var _iconShieldPhoton:SkillIcon;
		//private var _iconShieldTachyon:SkillIcon;
		private var _iconShields:Vector.<SkillIcon> = new Vector.<SkillIcon>(3, true);
		private var _iconSlow:SkillIcon;
		
		override public function get scrollY():Number
		{
			if (progress < BOSS_PROGRESS || mode == MODE_ENDLESS)
				return SCROLL_SPEED * Time.deltaSecond;
			else
				return 0;
		}
		
		override public function get leftBoundary():Number 
		{
			return BANK_WIDTH;
		}
		
		override public function get rightBoundary():Number 
		{
			return Game.instance.stageWidth - BANK_WIDTH;
		}
		
		override public function get lastEnemyProgress():Number 
		{
			if (mode == MODE_NORMAL)
				return 0.6;
			else
				return Number.MAX_VALUE;
		}
		
		public function Level4() 
		{
			super(4);
			
			_bg = new Assets.LEVELS_4_BG();
			addChild(_bg);
			
			var bgScroll:Bitmap = new Assets.LEVELS_4_BG_SCROLL();
			_bgScrollLT = new Bitmap(bgScroll.bitmapData);
			_bgScrollLT.x = -5;
			_bgScrollLT.y = -1400;
			_bgScrollLB = new Bitmap(bgScroll.bitmapData);
			_bgScrollLB.x = -5;
			_bgScrollLB.y = -700;
			_bgScrollerL = new DualImageWrappingScroller(_bgScrollLT, _bgScrollLB, Game.instance.stageRect);
			// LT is on top of LB
			_bgScrollerL.addChild(_bgScrollLT);
			addChild(_bgScrollerL);

			_bgScrollRT = new Bitmap(bgScroll.bitmapData);
			_bgScrollRT.scaleX = -1;
			_bgScrollRT.x = Game.instance.stageWidth + 5;
			_bgScrollRT.y = -1600;
			_bgScrollRB = new Bitmap(bgScroll.bitmapData);
			_bgScrollRB.scaleX = -1;
			_bgScrollRB.x = Game.instance.stageWidth + 5;
			_bgScrollRB.y = -900;
			_bgScrollerR = new DualImageWrappingScroller(_bgScrollRT, _bgScrollRB, Game.instance.stageRect);
			// RT is on top of RB
			_bgScrollerR.addChild(_bgScrollRT);
			addChild(_bgScrollerR);
						
			//_space.worldLinearDrag = 10;
			
			var barriers:Body = NapeUtil.makeBarriers(Game.instance.stageWidth, Game.instance.stageHeight,
				Direction.LEFT | Direction.RIGHT | Direction.UP, -BANK_WIDTH);
			barriers.setShapeMaterials(Material.steel());
			barriers.space = _space;
			
			_raft.scale = 0.5;
			addChild(_raft);
			
			_raftCollisionDetector = new PixelPerfectCollisionDetector(_raft, Game.instance.stageWidth, Game.instance.stageHeight, DisplayObjectUtil.drawToBitmapData(_raft));
			
			addChild(_bossLayer);
			_boss = new Level4Boss();
			_boss.cacheAsBitmap = true;
			
			_script = new PhaseScript(this);
			_script.parse(new Assets.LEVELS_4_BOSS_SCRIPT);
			
			//_shieldLaser.graphics.lineStyle(2, 0xff0000);
			//_shieldLaser.graphics.drawCircle(0, 0, SHIELD_RADIUS);
			//_shieldPhoton.graphics.lineStyle(2, 0xffff00);
			//_shieldPhoton.graphics.drawCircle(0, 0, SHIELD_RADIUS);
			//_shieldTachyon.graphics.lineStyle(2, 0x00ff00);
			//_shieldTachyon.graphics.drawCircle(0, 0, SHIELD_RADIUS);
			var i:int;
			for (i = 0; i < 3; i++)
			{
				_shields[i] = new Sprite();
				_shields[i].addChild(DisplayObjectUtil.pivot(new Assets["LEVELS_4_SHIELD_" + i]));
			}
			
			_bulletContainers[0] = new PhysicalSpriteContainer(_space, 1, createLaserBullet, updateLaserBullet);
			_bulletContainers[1] = new PhysicalSpriteContainer(_space, 36, createPhotonBullet, updateBullet);
			_bulletContainers[2] = new PhysicalSpriteContainer(_space, 2, createTachyonBullet, updateBullet);
			_bulletContainers[3] = new PhysicalSpriteContainer(_space, 10, createPlasmaBullet, updatePlasmaBullet);
			
			_turretContainers[0] = new PhysicalSpriteContainer(_space, 1, createLaserTurret, updateEnemy, null, resetTurret);
			_turretContainers[1] = new PhysicalSpriteContainer(_space, 2, createPhotonTurret, updateEnemy, null, resetTurret);
			_turretContainers[2] = new PhysicalSpriteContainer(_space, 2, createTachyonTurret, updateEnemy, null, resetTurret);
			_turretContainers[3] = new PhysicalSpriteContainer(_space, 5, createPlasmaTurret, updateEnemy, null, resetTurret);
			
			addChild(_turretContainers[0]);
			addChild(_turretContainers[1]);
			addChild(_turretContainers[2]);
			addChild(_turretContainers[3]);
			
			addChild(_bulletContainers[0]);
			addChild(_bulletContainers[1]);
			addChild(_bulletContainers[2]);
			addChild(_bulletContainers[3]);
			
			addChild(_shieldContainer);
			
			addChild(_gfxLayer);

			_cdm.add(CD_ID_GENERATE_PLASMA_TURRET, 10.0);
			
			for (i = 0; i < 3; i++)
			{
				var cdId:int = CD_ID_SHIELD_LASER + i;
				_iconShields[i] = new SkillIcon(AssetsCache.getNewBitmap(Assets["UI_ICON_SHIELD_" + i]), cdId, _cdm, Game.ACTION_ID_SKILL_A + i);
				_iconShields[i].x = 48;
				_iconShields[i].y = Game.instance.stageHeight - 48 - 64 * i;
				_uiLayer.addChild(_iconShields[i]);
			}
			_iconSlow = new SkillIcon(AssetsCache.getNewBitmap(Assets.UI_ICON_SLOW), CD_ID_SLOW, _cdm, Game.ACTION_ID_SKILL_B);
			_iconSlow.x = Game.instance.stageWidth - 48;
			_iconSlow.y = Game.instance.stageHeight - 48;
			_uiLayer.addChild(_iconSlow);
		}
		
		private function createLaserBullet():Level4Bullet 
		{
			var bullet:Level4Laser = new Level4Laser(_shields[0]);
			return bullet;
		}
		
		private function createPhotonBullet():Level4Bullet
		{
			return new Level4Bullet(_shields[1], Assets.LEVELS_4_BULLET_YELLOW, PhysicalSprite.EMPTY_BODY_NAME);
		}
		
		private function createTachyonBullet():Level4Bullet
		{
			return new Level4Bullet(_shields[2], Assets.LEVELS_4_BULLET_GREEN, PhysicalSprite.EMPTY_BODY_NAME);
		}

		private function createPlasmaBullet():Level4Bullet 
		{
			var bullet:Level4Bullet = new Level4Bullet(_shieldNull, Assets.LEVELS_4_BULLET_BLUE, PhysicalSprite.EMPTY_BODY_NAME);
			//bullet.bitmap.blendMode = BlendMode.ADD;
			return bullet;
		}
		
		private function createLaserTurret():Enemy 
		{
			var turret:Enemy = new Enemy(_bulletContainers[0], Assets.LEVELS_4_ENEMY_RED, PhysicalSprite.EMPTY_BODY_NAME);
			turret.attackInterval = 5;
			turret.attackRange = 600;
			turret.bulletDamage = 0.6;//DPS
			turret.bulletSpeed = 0;
			turret.biasRatio = 0;
			turret.shootOffsetX = 18;
			return turret;
		}
		
		private function createPhotonTurret():Level4PhotonTurret
		{
			var turret:Level4PhotonTurret = new Level4PhotonTurret(_bulletContainers[1]);
			turret.attackInterval = 2;
			turret.attackRange = 600;
			turret.bulletDamage = 0.3;
			turret.bulletSpeed = 400;
			turret.biasRatio = 0.1;
			turret.shootOffsetX = 22;// 20;
			return turret;
		}
		
		private function createTachyonTurret():Enemy
		{
			var turret:Enemy = new Enemy(_bulletContainers[2], Assets.LEVELS_4_ENEMY_GREEN, PhysicalSprite.EMPTY_BODY_NAME);
			turret.attackInterval = 3;
			turret.attackRange = 800;
			turret.bulletDamage = 1.0;
			turret.bulletSpeed = 500;
			turret.biasRatio = 0;
			turret.shootOffsetX = 20;// 24;
			turret.shootSound = "tachyon";
			return turret;
		}
		
		private function createPlasmaTurret():Enemy
		{
			var turret:Enemy = new Enemy(_bulletContainers[3], Assets.LEVELS_4_ENEMY_BLUE, PhysicalSprite.EMPTY_BODY_NAME);
			turret.attackInterval = 5;
			turret.attackRange = 500;
			turret.bulletDamage = 1.0;
			turret.bulletSpeed = 120;
			turret.biasRatio = 0;
			turret.shootOffsetX = 7;// 16;
			return turret;
		}
		
		private function updateEnemy(enemy:Enemy):void 
		{
			enemy.update(Time.deltaSecond, _raft.body.position);
		}
		
		private function updateLaserBullet(bullet:Level4Bullet):void
		{
			bullet.firePos.y += (scrollY + _bossVelocity.y * Time.deltaSecond);
			Game.instance.sfx.play("laser", 1, 0, 0, false, false, true);
			//trace(bullet.firePos);
			if (bullet.firePos.y > Game.instance.stageHeight ||
				 Time.lastTickTime - bullet.fireTime >= 1500)
			{
				Game.instance.sfx.fadeTo("laser", 0, 500);
				bullet.removeFromParent();
				return;
			}
			var dx:Number = _raft.x - bullet.firePos.x;
			var dy:Number = _raft.y - bullet.firePos.y;
			var dist:Number = Math.sqrt(dx * dx + dy * dy);
			if (dist < bullet.distMax)
			{
				bullet.x = _raft.x;
				bullet.y = _raft.y;
				if (_currentShield == _shields[0])
				{
					dist -= SHIELD_RADIUS;
					bullet.x += SHIELD_RADIUS * MathUtil.cosAngle(bullet.rotation);
					bullet.y += SHIELD_RADIUS * MathUtil.sinAngle(bullet.rotation);
				}
				else
				{
					_raft.damage(bullet.damage * Time.deltaSecond, 0);
					Game.instance.flash(0xffff0000, 0.2, 3);
				}
				bullet.rotation = 0;
				bullet.width = dist;
				bullet.height = 2;
				bullet.rotation = 180 + MathUtil.angle(dx, dy);
			}
			else
			{
				Game.instance.sfx.fadeTo("laser", 0, 500);
				bullet.removeFromParent();
			}
		}
		
		private function updatePlasmaBullet(bullet:Level4Bullet):void
		{
			bullet.rotation = MathUtil.angle(_raft.x - bullet.x, _raft.y - bullet.y);
			bullet.scale = 1 - bullet.dist / bullet.distMax;
			//trace("plasma bullet", bullet.x, bullet.y, bullet.width, bullet.height);
			Game.instance.sfx.play("plasma", bullet.scale, 0, 0, false, false, true);
			bullet.update(Time.deltaSecond);
			if (!bullet.isInSpace)
			{
				Game.instance.sfx.stopSound("plasma");
				return;
			}
			
			if (_currentShield != _shieldNull && MathUtil.compareDistance(bullet.x - _raft.x, bullet.y - _raft.y, SHIELD_RADIUS) < 0)
			{
				bullet.removeFromParent();
				Game.instance.sfx.stopSound("plasma");
				resetShield();
				Game.instance.sfx.play("thunder");
			}
			if (_raft.hitTestObject(bullet))
			{
				bullet.removeFromParent();
				_raft.damage(bullet.damage * bullet.scaleX, 3);
				Game.instance.flash(0xffff0000, 0.2, 5);
				Game.instance.sfx.stopSound("plasma");
			}
		}
		
		private function updateBullet(bullet:Level4Bullet):void 
		{
			bullet.update(Time.deltaSecond);
			if (!bullet.isInSpace)
				return;
			if (bullet.shield == _currentShield)
			{
				if (MathUtil.compareDistance(bullet.x - _raft.x, bullet.y - _raft.y, SHIELD_RADIUS + bullet.bitmap.bitmapData.width * 0.5) < 0)
				{
					bullet.removeFromParent();
				}
			}
			else if (bullet.shield == _shields[2])
			{
				// Use pixel-perfect collision detection for Tachyon bullet
				if (_raftCollisionDetector.hitTestObject(bullet))
				{
					bullet.removeFromParent();
					_raft.damage(bullet.damage, 1);
					Game.instance.flash(0xffff0000, 0.2, 5);
				}
			}
			else if (_raft.hitTestObject(bullet))
			{
				bullet.removeFromParent();
				_raft.damage(bullet.damage, 1);
				Game.instance.flash(0xffff0000, 0.2, 5);
			}
		}
		
		override public function update():void 
		{
			super.update();
			// Always update raft's animations
			_raft.animation.updateFrame(Time.deltaSecond);
		}
		
		override protected function updateScene():void 
		{
			_bgScrollerL.scrollY += scrollY;
			_bgScrollerR.scrollY += scrollY;
			
			_raft.update(Time.deltaSecond);
			
			_currentShield.x = _raft.x;
			_currentShield.y = _raft.y;
			
			for each(var bulletContainer:PhysicalSpriteContainer in _bulletContainers)
			{
				bulletContainer.update();
			}
			//_laserBulletContainer.update();
			//_photonBulletContainer.update();
			//_tachyonBulletContainer.update();
			//_plasmaBulletContainer.update();
			
			for each(var turretContainer:PhysicalSpriteContainer in _turretContainers)
			{
				turretContainer.update();
			}
			
			//_laserTurretContainer.update();
			if (progress > 0.03 && progress < lastEnemyProgress &&
				Random.randomBoolean(0.006 * difficulty))
			{
				var laserTurret:Enemy = _turretContainers[0].add() as Enemy;
				if (laserTurret != null)
					laserTurret.initPosition(laserTurret.width >> 1, laserTurret.width >> 1);//BANK_WIDTH);
			}
			
			//_photonTurretContainer.update();
			if (progress > 0.2 && progress < lastEnemyProgress &&
				Random.randomBoolean(0.006 * difficulty * (1 + progress)))
			{
				var photonTurret:Enemy = _turretContainers[1].add() as Level4PhotonTurret;
				if (photonTurret != null)
					photonTurret.initPosition(photonTurret.width >> 1, photonTurret.width >> 1);//BANK_WIDTH);
			}
			
			//_tachyonTurretContainer.update();
			if (progress > 0.3 && progress < lastEnemyProgress &&
				Random.randomBoolean(0.006 * difficulty * (1 + progress)))
			{
				var tachyonTurret:Enemy = _turretContainers[2].add() as Enemy;
				if (tachyonTurret != null)
					tachyonTurret.initPosition(tachyonTurret.width >> 1, tachyonTurret.width >> 1);//BANK_WIDTH);
			}
			
			//_plasmaTurretContainer.update();
			if (progress > 0.1 && progress < lastEnemyProgress &&
				Random.randomBoolean(0.01 * difficulty) &&
				_cdm.isCooledDown(CD_ID_GENERATE_PLASMA_TURRET))
			{
				var plasmaTurret:Enemy = _turretContainers[3].add() as Enemy;
				if (plasmaTurret != null)
				{
					plasmaTurret.initPosition(plasmaTurret.width >> 1, plasmaTurret.width >> 1);//BANK_WIDTH);
					onUseSkill(CD_ID_GENERATE_PLASMA_TURRET);
				}
			}
			
			if (mode == MODE_NORMAL)
			{
				if (justPassedProgress(lastEnemyProgress))
				{
					_music.fadeOut(5.0);
				}
				
				if (progress >= BOSS_PROGRESS)
				{
					// Enter Boss
					if (justPassedProgress(BOSS_PROGRESS))
					{
						startTransition();
						_raft.checkPoint = BOSS_PROGRESS;
						_boss.x = Game.instance.stageWidth >> 1;
						_boss.y = -_boss.height;
						_script.reset();
						_bossLayer.addChild(_boss);
						TweenLite.to(_boss, 3.0, { y: 0, onComplete: finishTransition } );
						
						Game.instance.sfx.play("boss_enter");
						_music.playLoop(Game.instance.music.getSound("boss").sound);
						_music.fadeIn(5.0);
					}
				
					_script.update();
					_boss.x += _bossVelocity.x * Time.deltaSecond;
					_boss.y += _bossVelocity.y * Time.deltaSecond;
					updateBossTurrets();
				}
				if (justPassedProgress(END_PROGRESS))
				{
					// Blackhole cut scene
					startTransition();
					DisplayObjectUtil.tint(this);
					Game.instance.sfx.stopAll();
					var tweenVars:Object = { x: 180, y: 320 };
					for (var i:int = 0; i < _bulletContainers[3].numChildren; i++)
					{
						TweenLite.to(_bulletContainers[3].getChildAt(i), 2.0, tweenVars);
					}
					Game.instance.shake(this, 20, 20, 2.0, 100, enterBlackhole);
					_music.stop();
					Game.instance.sfx.play("alarm").fadeTo(0, 4000);
				}
			}
		}
		
		private var _blackhole:Sprite;
		
		private function enterBlackhole():void 
		{
			//_iconSlow.visible = false;
			//for each(var icon:DisplayObject in _iconShields)
				//icon.visible = false;
			
			Game.instance.sfx.play("blackhole");
			
			/*
			// TODO: use bitmap
			var blackhole:Shape = new Shape();
			blackhole.graphics.beginFill(0);
			blackhole.graphics.drawCircle(0, 0, 16);
			blackhole.graphics.endFill();
			blackhole.x = Game.instance.stageWidth >> 1;
			blackhole.y = 180;
			_gfxLayer.addChild(blackhole);
			TweenLite.to(blackhole, 4.0, { scaleX: 40, scaleY: 40, onComplete: nextLevel } );
			*/
			
			/*
			if (_blackhole == null)
			{
				_blackhole = new Sprite();
				_blackhole.addChild(DisplayObjectUtil.pivot(new Assets.LEVELS_4_BLACKHOLE()));
				_blackhole.x = 180;
				_blackhole.y = 320;
			}
			Game.instance.addChild(_blackhole);
			_blackhole.width = _blackhole.height = 16;
			TweenLite.to(_blackhole, 2.0, { scaleX: 1, scaleY: 1, onComplete: nextLevel } );
			TweenLite.to(this, 2.0, { alpha: 0 } );
			*/
			
			DisplayObjectUtil.playMovie("blackhole.swf", Game.instance, false, onBlackholeEnd, null, onBlackholeUpdate);
			_bulletContainers[3].clear();
		}
		
		private function onBlackholeUpdate(mc:MovieClip):void 
		{
			if (mc.totalFrames == 1 && mc.numChildren == 1)
				mc = mc.getChildAt(0) as MovieClip;
			this.alpha = 1 - mc.currentFrame / mc.totalFrames;
			mc.alpha = mc.currentFrame < 100 ? 1 : this.alpha;
		}
		
		private function onBlackholeEnd():void 
		{
			nextLevel();
		}
		
		/*
		private function enterWormhole():void 
		{
			DisplayObjectUtil.playMovie("wormhole.swf", this, false, nextLevel);
		}
		*/
		
		private function updateBossTurrets():void 
		{
			for (var i:int = 0; i < _boss.numChildren; i++)
			{
				var mc:MovieClip = _boss.getChildAt(i) as MovieClip;
				if (mc != null && mc.name.indexOf("w") == 0)
				{
					var turret:Enemy = mc["turret"];
					if (turret != null)
					{
						turret.speedX = _bossVelocity.x;
						turret.speedY = _bossVelocity.y;
					}
				}
			}
		}
		
		public function setBossVelocity(vx:Number, vy:Number):void
		{
			_bossVelocity.setTo(vx, vy);
		}
		
		public function addTurret(turretObj:Object):void
		{
			//if (_boss.hasOwnProperty(turretObj.name))
			{
				var slot:MovieClip = _boss[turretObj.name];
				var slotMatrix:Matrix = slot.transform.matrix.clone();
				slotMatrix.concat(_boss.transform.matrix);
				var turret:Enemy = _turretContainers[turretObj.type].add() as Enemy;
				turret.transform.matrix = slotMatrix;
				turret.speedY = 0;
				// Fire instantly
				turret.attackCooldown = 0;
				turret.visible = false;
				ObjectUtil.applyProperties(turret, turretObj.properties);
				//slot.visible = false;
				slot["turret"] = turret;
			}
		}
		
		public function removeTurrets(slotNames:Array):void
		{
			for each(var slotName:String in slotNames)
			{
				//if (_boss.hasOwnProperty(slotName))
				{
					var slot:DisplayObject = _boss[slotName];
					//slot.visible = true;
					var turret:Enemy = slot["turret"];
					if (turret != null)
					{
						turret.removeFromParent();
						delete slot["turret"];
					}
				}
			}
		}
		
		private function resetTurret(turret:Enemy):void
		{
			turret.visible = true;
			turret.speedY = 100;
			turret.transform.matrix.identity();
		}
		
		override protected function updateSkill():void 
		{
			super.updateSkill();
			
			_iconSlow.update();
			
			for (var i:int = 0; i < 3; i++)
			{
				_iconShields[i].update();
				var cdId:int = CD_ID_SHIELD_LASER + i;
				if (//(_iconShields[i].isPressed || Input.getKeyDown(Keyboard.NUMBER_1 + i))
					Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_A + i)
					&& _cdm.isCooledDown(cdId))
				{
					DisplayObjectUtil.removeFromParent(_currentShield);
					_currentShield = _shields[i];
					_shieldContainer.addChild(_currentShield);
					_currentShield.alpha = 0;
					TweenLite.to(_currentShield, 0.5, { alpha: 1 } );
					onUseSkill(cdId);
					Game.instance.sfx.play("shield", 0.5);
					break;
				}
			}
			
			// Swipe down or press SHIFT to slow down timing
			if (Time.scale < 1)
			{
				Time.scale += 0.006;
			}
			else if (//(_iconSlow.isPressed || _swipeOffsetY >= 1 || Input.getKeyDown(Keyboard.SHIFT))
				Game.instance.input.isActionActive(Game.ACTION_ID_SKILL_B)
				&& _cdm.isCooledDown(CD_ID_SLOW))
			{
				Time.scale = 0.2;
				onUseSkill(CD_ID_SLOW);
			}
		}
		
		override protected function updateProgress():void 
		{
			super.updateProgress();
			// 100 seconds
			progress += (0.01 * Time.deltaSecond);
		}
		
		override protected function enterScene():void 
		{
			super.enterScene();
			_raft.x = 180;
			_raft.y = 700;
			_iconSlow.visible = false;
			for (var i:int = 0; i < _iconShields.length; i++)
			{
				_iconShields[i].visible = false;
			}
			//_bgScroll.y = -_bgScroll.height;
			Game.instance.fade(0x00ffffff, 1.0, enterRaft);
			
			if (mode == MODE_NORMAL)
			{
				//Game.instance.music.play("Level4");
				_music.play(Game.instance.music.getSound("Level4").sound);
			}
			else
			{
				//Game.instance.music.playLoop("space").fadeFrom(0, 0.3, 3000);
				_music.playLoop(Game.instance.music.getSound("space").sound, 0.3);			
				_music.fadeIn(3.0);
			}
		}
		
		private function enterRaft():void 
		{
			TweenLite.to(_bgScrollerL, 3.0, { scrollY: 600 } );
			TweenLite.to(_bgScrollerR, 3.0, { scrollY: 600 } );
			TweenLite.to(_raft, 3.0, { y: 400, onComplete: finishTransition } );
			//progress = 0.9;// BOSS_PROGRESS;
		}
		
		private function resetShield():void 
		{
			DisplayObjectUtil.removeFromParent(_currentShield);
			_currentShield = _shieldNull;
		}
		
		override protected function clear():void 
		{
			super.clear();
			//_laserBulletContainer.clear();
			//_photonBulletContainer.clear();
			//_tachyonBulletContainer.clear();
			//_plasmaBulletContainer.clear();
			for each(var bulletContainer:PhysicalSpriteContainer in _bulletContainers)
			{
				bulletContainer.clear();
			}
			//_laserTurretContainer.clear();
			//_photonTurretContainer.clear();
			//_tachyonTurretContainer.clear();
			//_plasmaTurretContainer.clear();
			for each(var turretContainer:PhysicalSpriteContainer in _turretContainers)
			{
				turretContainer.clear();
			}
			resetShield();
			DisplayObjectUtil.removeFromParent(_boss);
			_bossVelocity.setTo(0, 0);
			_gfxLayer.graphics.clear();
			_gfxLayer.removeChildren();
			
			_bgScrollerL.reset();
			// LT is on top of LB
			_bgScrollerL.addChild(_bgScrollLT);
			_bgScrollerR.reset();
			// RT is on top of RB
			_bgScrollerR.addChild(_bgScrollRT);
			
			DisplayObjectUtil.removeFromParent(_blackhole);
			this.alpha = 1;
		}
		
		override protected function showSkillUpgrade():void 
		{
			// No skill upgrade tutorial
			nextLevel();
		}
		
		override protected function nextLevel():void 
		{
			Game.instance.player.level = 5;
			Game.instance.fade(0x00ffffff, 1.0, Game.instance.showCredits);
			//Game.instance.showCredits();
		}
		
		override protected function getSkillUpgradeIcon1():DisplayObject 
		{
			return AssetsCache.getNewBitmap(Assets.UI_ICON_SHIELD);
		}
		
		override protected function performUpgradeSkill(level:int, value:Number):void
		{
			_cdm.setCooldown(CD_ID_SHIELD_LASER, CD_SHIELD * (1 - value / 100));
			_cdm.setCooldown(CD_ID_SHIELD_PHOTON, CD_SHIELD * (1 - value / 100));
			_cdm.setCooldown(CD_ID_SHIELD_TACHYON, CD_SHIELD * (1 - value / 100));
		}
	}

}