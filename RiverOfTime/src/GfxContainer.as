package  
{
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.display.StarlingSpriteSheet;
	import me.sangtian.common.display.StarlingSpriteSheetData;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.DisplayObjectUtil;
	/**
	 * Contains graphics FXs which generally play once and have no physical body.
	 * 
	 * @author Nicolas Tian
	 */
	public class GfxContainer extends PhysicalSpriteContainer 
	{
		private var _spritesheetClass:Class;
		private var _data:StarlingSpriteSheetData;
		
		public var autoPlay:Boolean = true;
		public var playOnce:Boolean = true;
		
		public function GfxContainer(spritesheetClass:Class, data:*, maxCount:uint, onUpdate:Function = null, onAdd:Function = null, onRemove:Function = null) 
		{
			_spritesheetClass = spritesheetClass;
			if (data is StarlingSpriteSheetData)
			{
				_data = data;
			}
			else if (data is String || data is ByteArray)
			{
				_data = StarlingSpriteSheetData.parse(data);
			}
			else if (data is Class)
			{
				_data = StarlingSpriteSheetData.parse(AssetsCache.getOrCacheAsset(data));
			}
			else
			{
				//throw new Error("Invalid data");
			}
			super(null, maxCount, createSprite, onUpdate, onAdd, onRemove);
		}
		
		private function createSprite():PhysicalSpriteAnimated
		{
			return new PhysicalSpriteAnimated(_spritesheetClass, _data, PhysicalSprite.EMPTY_BODY_NAME);
		}
		
		override public function add(x:Number = 0, y:Number = 0):PhysicalSprite 
		{
			var sprite:PhysicalSpriteAnimated = super.add(x, y) as PhysicalSpriteAnimated;
			if (sprite != null)
			{
				if (autoPlay)
				{
					sprite.play();
				}
				if (playOnce)
				{
					sprite.addEventListener(Event.COMPLETE, handleComplete);
				}
			}
			return sprite;
		}
		
		private function handleComplete(e:Event):void 
		{
			var sprite:PhysicalSpriteAnimated = e.currentTarget as PhysicalSpriteAnimated;
			sprite.removeEventListener(Event.COMPLETE, handleComplete);
			remove(sprite);
		}
		
		override public function remove(sprite:PhysicalSprite):void 
		{
			(sprite as PhysicalSpriteAnimated).stop();
			super.remove(sprite);
		}
		
		override public function update():void 
		{
			for (var i:int = numChildren - 1; i >= 0; i--)
			{
				var sprite:PhysicalSpriteAnimated = getChildAt(i) as PhysicalSpriteAnimated;
				sprite.update(Time.deltaSecond);
			}
			super.update();
		}
	}

}