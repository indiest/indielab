package  
{
	import flash.display.Sprite;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class MapObject extends Sprite 
	{
		private var _worldPosition:Point = new Point();
		
		public function get worldPosition():Point 
		{
			_worldPosition.x = x;
			_worldPosition.y = y;
			return _worldPosition;
		}
		
		public function set worldPosition(p:Point):void
		{
			_worldPosition = p;
			this.x = p.x;
			this.y = p.y;
		}
		
		public function get screenPosition():Point
		{
			return state.worldmapLayer.localToGlobal(worldPosition);
		}
		
		protected function get state():MapState
		{
			return Game.current.stateManager.currentState as MapState;
		}
		
		public function MapObject() 
		{
			
		}
		
		public function update():void
		{
			
		}
		
		public function getTooltip():String
		{
			return null;
		}
		
		public function onClicked():void 
		{
			
		}
		
	}

}