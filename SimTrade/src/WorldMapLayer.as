package  
{
	import flash.display.Bitmap;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	/**
	 * ...
	 * @author tiansang
	 */
	public class WorldMapLayer extends MapLayer 
	{
		private static var MERCATOR_X:Number = 537;
		private static var MERCATOR_Y:Number = 341;

		private var _image:Bitmap;
		private var _showSeaways:Boolean = false;;
		
		public function WorldMapLayer() 
		{
			_image = new Assets.IMAGES_SIMTRADEMAP();
			MERCATOR_X = _image.width / 2;
			MERCATOR_Y = _image.height / 2;
			addChild(_image);
			
		}
		
		override protected function addMouseEventListeners():void 
		{
			Game.stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
		}
		
		private function handleKeyDown(e:KeyboardEvent):void 
		{
			if (e.keyCode == Keyboard.S)
			{
				_showSeaways = !_showSeaways;
				if (_showSeaways)
				{
					//_image.visible = false;
					graphics.clear();
					graphics.lineStyle(5, 0xffffff);
					for each(var seawayData:Object in Data.seaways)
					{
						for (var i:uint = 0; i < seawayData.path.length; i++)
						{
							if (i == 0)
								graphics.moveTo(seawayData.path[i].x, seawayData.path[i].y);
							else
								graphics.lineTo(seawayData.path[i].x, seawayData.path[i].y);
						}
					}
				}
			}
		}
		
		public function getLongituteFromX(x:Number):Number
		{
			return (x - MERCATOR_X) / _image.width * 360;
		}
		
		public function getLatituteFromY(y:Number):Number
		{
			var lat:Number = Math.exp((MERCATOR_Y - y) / _image.height * Math.PI * 4);
			return (Math.asin((lat - 1) / (lat + 1)) * 180) / Math.PI;
			//return (MERCATOR_Y - y) / MERCATOR_Y * 180 / Math.PI;
		}
		
		override public function update():void 
		{
			super.update();
		}
	}

}