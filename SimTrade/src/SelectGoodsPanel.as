package  
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class SelectGoodsPanel extends Sprite 
	{
		private var _ui:PanelSelectUI = new PanelSelectUI();
		
		public function SelectGoodsPanel() 
		{
			_ui.btn_close.addEventListener(MouseEvent.CLICK, handleClose);
			_ui.cs_btn.addEventListener(MouseEvent.CLICK, handleOk);
			addChild(_ui);
		}
		
		private function handleOk(e:MouseEvent):void 
		{
			close();
		}
		
		private function handleClose(e:MouseEvent):void 
		{
			close();
		}
		
		public function close():void
		{
			if (parent)
				parent.removeChild(this);
		}
	}

}