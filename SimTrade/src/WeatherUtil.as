package  
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class WeatherUtil 
	{
		public static const WEATHER_SUN:int = 1;
		public static const WEATHER_CLOUD:int = 2;
		public static const WEATHER_RAIN:int = 3;
		public static const WEATHER_STORM:int = 4;
		
		public static function randomWeather():int
		{
			return Math.random() * 4 + 1;
		}
	}

}