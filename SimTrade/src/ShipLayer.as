package  
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	/**
	 * ...
	 * @author 
	 */
	public class ShipLayer extends MapLayer 
	{
		private var _lineStyleBitmapData:BitmapData = new BitmapData(5, 5);
		private var _weatherTip:WeatherTip = new WeatherTip();
		
		public function ShipLayer() 
		{
			_lineStyleBitmapData.fillRect(_lineStyleBitmapData.rect, 0xffff7700);
		}
		
		override public function addMapObject(obj:MapObject):void 
		{
			super.addMapObject(obj);
			var ship:Ship = obj as Ship;
			ship.randomWeather();
		}
		
		override public function update():void 
		{
			super.update();
			graphics.clear();
			for each(var ship:Ship in objects)
			{
				if (ship.path.length > 0)
				{
					graphics.moveTo(ship.x, ship.y);
					graphics.lineStyle(5, 0xff7700);
					//graphics.lineBitmapStyle(_lineStyleBitmapData);
					for each(var p:Point in ship.path)
					{
						graphics.lineTo(p.x, p.y);
					}
				}
			}
		}
		
		override protected function onMouseHover(obj:MapObject):void 
		{
			showWeather(obj as Ship);
		}
		
		public function showWeather(ship:Ship):void
		{
			_weatherTip.cs_tip.cs_icon.gotoAndStop(ship.currentWeather);
			var p:Point = worldToScreen(ship.worldPosition);
			_weatherTip.x = p.x;
			_weatherTip.y = p.y - _weatherTip.height * 0.5;
			state.tooltipLayer.showTooltip(_weatherTip);
		}
	}

}