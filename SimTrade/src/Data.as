package  
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class Data 
	{
		public static var cities:Array;
		public static var friends:Array;
		public static var seaways:Array;
		
		public static function loadCities(data:String):void
		{
			cities = JSON.parse(data).cities;
		}
		
		public static function loadFriends(data:String):void
		{
			friends = JSON.parse(data).friends;
		}
		
		public static function loadSeaways(data:String):void
		{
			seaways = JSON.parse(data).seaways;
		}
		
		public static function loadAll():void
		{
			loadCities(new Assets.DATA_CITY);
			loadFriends(new Assets.DATA_FRIEND);
			loadSeaways(new Assets.DATA_SEAWAY);
		}
	}

}