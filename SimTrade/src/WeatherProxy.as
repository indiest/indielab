package  
{
	import common.ObjectPool;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author tiansang
	 */
	public class WeatherProxy 
	{
		private static var _loaderPool:ObjectPool = new ObjectPool(10, Loader);
		private var _iconLoader:Loader;
		private var _icon:DisplayObject;
		private var _weatherData:Object;
		
		public function get weatherData():Object 
		{
			return _weatherData;
		}
		
		public function WeatherProxy() 
		{
			_iconLoader = _loaderPool.borrowObject();
			_iconLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleIconLoaded);
		}
		
		private function handleIconLoaded(e:Event):void 
		{
			_iconLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, handleIconLoaded);
			_icon = _iconLoader.content;
			if (_iconLoader.parent)
			{
				_iconLoader.parent.addChildAt(_icon, _iconLoader.parent.getChildIndex(_iconLoader));
				_iconLoader.parent.removeChild(_iconLoader);
			}
			_loaderPool.returnObject(_iconLoader);
		}
		
		public function onLoadComplete(e:Event):void
		{
			var urlLoader:URLLoader = e.currentTarget as URLLoader;
			urlLoader.removeEventListener(Event.COMPLETE, onLoadComplete);
			var response:Object = JSON.parse(urlLoader.data as String);
			var weathers:Array = response.data.weather;
			var date:Date = new Date();
			for each(var w:Object in weathers)
			{
				for each(var h:Object in w.hourly)
				{
					if (h.time == String(date.getHours() * 100))
					{
						_weatherData = h;
						_iconLoader.load(new URLRequest(h.weatherIconUrl[0].value));
					}
				}
			}
		}
		
		public function get weatherIcon():DisplayObject
		{
			return _icon || _iconLoader;
		}
	}

}