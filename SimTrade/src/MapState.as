package  
{
	import com.greensock.easing.Back;
	import com.greensock.easing.Linear;
	import com.greensock.TweenNano;
	import common.logging.Log;
	import common.state.BaseState;
	import common.util.DisplayObjectUtil;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class MapState extends BaseState 
	{
		
		public static const MAX_ZOOM_LEVEL:uint = 5;
		
		/*
		 * 1. dragging
		 * 2. get longitute/latitute
		 * 3. zooming(optional)
		 * 4. looping(optional)
		 */
		public var worldmapLayer:WorldMapLayer = new WorldMapLayer();
		/*
		 * 1. displaying cities
		 */
		public var cityLayer:CityLayer = new CityLayer();
		/*
		 * 1. displaying ships
		 * 2. ship path finding
		 */
		public var shipLayer:ShipLayer = new ShipLayer();
		/*
		 * 1. displaying icons of:
			 * city(friend)
			 * ship
			 * weather
		 * 
		 */ 
		public var iconLayer:MapLayer = new MapLayer();
		/*
		 * 1. Map coordinates & LL
		 * 
		 */
		public var tooltipLayer:TooltipLayer = new TooltipLayer();
		
		private var _mapLayersContainer:Sprite = new Sprite();
		
		private var _selectGoodsPanelUI:PanelGoodsUI = new PanelGoodsUI();
		private var _selectSecretPanelUI:PanelSelectUI = new PanelSelectUI();
		
		private var _dragging:Boolean;
		private var _lastX:Number;
		private var _lastY:Number;
		
		private var _zoomLevel:uint;
		private var _ship:Ship;
		
		public function get zoomLevel():uint 
		{
			return _zoomLevel;
		}
		
		public function set zoomLevel(value:uint):void 
		{
			if (value < 1)
				value = 1;
			if (value > MAX_ZOOM_LEVEL)
				value = MAX_ZOOM_LEVEL;
			_zoomLevel = value;
			//_mapLayersContainer.scaleX = _mapLayersContainer.scaleY = value / MAX_ZOOM_LEVEL;
			TweenNano.to(_mapLayersContainer, 0.5, { scaleX: value / MAX_ZOOM_LEVEL, scaleY: value / MAX_ZOOM_LEVEL, ease: Linear.easeNone } );
		}
		
		public function MapState() 
		{
			_mapLayersContainer.addChild(worldmapLayer);
			_mapLayersContainer.addChild(cityLayer);
			_mapLayersContainer.addChild(shipLayer);
			_mapLayersContainer.addChild(iconLayer);
			addChild(_mapLayersContainer);
			addChild(tooltipLayer);
			
			_mapLayersContainer.addEventListener(MouseEvent.MOUSE_MOVE, handleLayersMouseMove);
			_mapLayersContainer.addEventListener(MouseEvent.MOUSE_WHEEL, handleLayersMouseHeel);
			worldmapLayer.addEventListener(MouseEvent.MOUSE_DOWN, handleWorldMapMouseDown);
			worldmapLayer.addEventListener(MouseEvent.MOUSE_UP, handleWorldMapMouseUp);
			worldmapLayer.addEventListener(MouseEvent.CLICK, handleWorldMapClick);
			
			_selectGoodsPanelUI.x = Game.current.stageWidth * 0.5;
			_selectGoodsPanelUI.y = Game.current.stageHeight * 0.5;
			_selectGoodsPanelUI.btn_close.addEventListener(MouseEvent.CLICK, handleCloseSelectGoodsPanel);
			_selectGoodsPanelUI.cs_button_cancel.addEventListener(MouseEvent.CLICK, handleCloseSelectGoodsPanel);
			_selectGoodsPanelUI.cs_btn_ok.addEventListener(MouseEvent.CLICK, handleSelectGoods);
			
			_selectSecretPanelUI.x = Game.current.stageWidth * 0.5;
			_selectSecretPanelUI.y = Game.current.stageHeight * 0.5;
			_selectSecretPanelUI.btn_close.addEventListener(MouseEvent.CLICK, handleCloseSelectSecretPanel);
			_selectSecretPanelUI.cs_btn_skip.addEventListener(MouseEvent.CLICK, handleSelectSecret);
			_selectSecretPanelUI.cs_btn_ok.addEventListener(MouseEvent.CLICK, handleSelectSecret);
			
		}
		
		private function handleSelectGoods(e:MouseEvent):void 
		{
			handleCloseSelectGoodsPanel();
			showPanel(_selectSecretPanelUI);
		}
		
		private function showPanel(panel:DisplayObject):void 
		{
			addChild(panel);
			TweenNano.from(panel, 0.5, { scaleX: 0.1, scaleY: 0.1, ease: Back.easeOut } );
		}
		
		private function handleCloseSelectGoodsPanel(e:MouseEvent = null):void 
		{
			DisplayObjectUtil.removeFromParent(_selectGoodsPanelUI);
		}
		
		private function handleSelectSecret(e:MouseEvent):void 
		{
			handleCloseSelectSecretPanel();
			_ship.sailTo(cityLayer.selectedCity);
			cityLayer.selectedCity.alpha = 1;
			cityLayer.selectedCity = null;
		}
		
		private function handleCloseSelectSecretPanel(e:MouseEvent = null):void 
		{
			DisplayObjectUtil.removeFromParent(_selectSecretPanelUI);
		}
		
		override public function enter():void 
		{
			super.enter();
			
			_ship = new Ship(cityLayer.getCityFromName("Tianjin"));
			//_ship.startPath(Data.seaways[1].path);
			///_ship.sailTo(cityLayer.getCityFromName(
			//	//"Hongkong"));
			//	"Marseille"));
			shipLayer.addMapObject(_ship);
			
			zoomLevel = 2;
		}
		
		override public function exit():void 
		{
			super.exit();
			shipLayer.removeMapObject(_ship);
			_ship = null;
		}
		
		private function handleWorldMapClick(e:MouseEvent):void 
		{
			trace("{\"x\":" + e.localX + ",\"y\":" + e.localY + "},");
		}
		
		private function handleLayersMouseHeel(e:MouseEvent):void 
		{
			zoomLevel += (e.delta > 0 ? 1: -1);
			//_mapLayersContainer.x = e.stageX - e.localX;
			//_mapLayersContainer.y = e.stageY - e.localY;
		}
		
		private function handleLayersMouseMove(e:MouseEvent):void 
		{
			var mapPoint:Point = worldmapLayer.globalToLocal(new Point(e.stageX, e.stageY));
			var lon:Number = worldmapLayer.getLongituteFromX(mapPoint.x);
			var lat:Number = worldmapLayer.getLatituteFromY(mapPoint.y);
			tooltipLayer.setLocInfo("(" + mapPoint.x + "," + mapPoint.y + ")\n" + lon + "\n" + lat);
			
			/*
			if (e.buttonDown)
			{
				if (_dragging)
				{
					var dx:Number = e.stageX - _lastX;
					var dy:Number = e.stageY - _lastY;
					_worldmapLayer.x += dx;
					_worldmapLayer.y += dy;
					if (_worldmapLayer.x < Game.current.stageWidth - _worldmapLayer.width)
						_worldmapLayer.x = Game.current.stageWidth - _worldmapLayer.width;
					else if (_worldmapLayer.x > 0)
						_worldmapLayer.x = 0;
					if (_worldmapLayer.y < Game.current.stageHeight - _worldmapLayer.height)
						_worldmapLayer.y = Game.current.stageHeight - _worldmapLayer.height;
					else if (_worldmapLayer.y > 0)
						_worldmapLayer.y = 0;
				}
				_lastX = e.stageX;
				_lastY = e.stageY;
				_dragging = true;
			}
			*/
		}
		
		private function handleWorldMapMouseUp(e:MouseEvent = null):void 
		{
			_mapLayersContainer.stopDrag();
			refreshAllLayers();
			
			/*
			if (_dragging)
			{
				_dragging = false;
				refreshAllLayers();
			}
			*/
		}
		
		private function handleWorldMapMouseDown(e:MouseEvent = null):void 
		{
			var boundX:Number = Game.current.stageWidth - worldmapLayer.width * _mapLayersContainer.scaleX;
			var boundY:Number = Game.current.stageHeight - worldmapLayer.height * _mapLayersContainer.scaleY;
			_mapLayersContainer.startDrag(false, new Rectangle(
				boundX, boundY, Math.abs(boundX), Math.abs(boundY)));
		}
		
		private function refreshAllLayers():void 
		{
			for (var i:uint = 0; i < _mapLayersContainer.numChildren; i++)
				(_mapLayersContainer.getChildAt(i) as MapLayer).refresh(worldmapLayer.x, worldmapLayer.y);
		}
		
		override public function update():void 
		{
			super.update();
			for (var i:uint = 0; i < _mapLayersContainer.numChildren; i++)
				(_mapLayersContainer.getChildAt(i) as MapLayer).update();
		}
		
		public function selectCity(city:City):void 
		{
			if (_ship.destCity != null)
				return;
			cityLayer.selectedCity = city;
			showPanel(_selectGoodsPanelUI);
		}
	}

}