package  
{
	import common.state.BaseState;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.StatusEvent;
	import flash.net.LocalConnection;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class LoadingState extends BaseState 
	{
		private var _logo:DisplayObject;
		private var _loader:Loader;
		private var _loaded:Boolean;
		
		public function LoadingState() 
		{
			_logo = new Assets.IMAGES_SIMTRADELOGO;
			addChild(_logo);
			
			_loader = new Loader();
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleLoadComplete);
			
			addEventListener(MouseEvent.CLICK, handleMouseClick);
		}
		
		private function handleLoadComplete(e:Event):void 
		{
			MinigameState.minigameLoader = _loader;
			_loaded = true;
		}
		
		private function handleMouseClick(e:MouseEvent):void 
		{
			if (_loaded)
				Game.current.stateManager.loadState(MapState);
		}
		
		override public function enter():void 
		{
			super.enter();
			_loaded = false;
			_logo.x = (Game.current.stageWidth - _logo.width) * 0.5;
			_loader.load(new URLRequest("minigame.swf"));
		}
		
	}

}