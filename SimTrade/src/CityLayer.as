package  
{
	import common.Time;
	import flash.geom.Point;
	/**
	 * ...
	 * @author tiansang
	 */
	public class CityLayer extends MapLayer 
	{
		public var selectedCity:City;
		
		public function CityLayer() 
		{
			for each(var cityData:Object in Data.cities)
			{
				addMapObject(new City(cityData));
			}
		}
		
		public function getCityFromName(name:String):City 
		{
			for each(var city:City in objects)
			{
				if (city.name == name)
					return city;
			}
			return null;
		}
		
		override public function update():void 
		{
			super.update();
			/*
			if (selectedCity)
			{
				selectedCity.alpha -= Time.deltaSecond;
				if (selectedCity.alpha == 0)
					selectedCity.alpha = 1;
			}
			*/
		}
	}

}