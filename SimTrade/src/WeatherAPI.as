package  
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	/**
	 * ...
	 * @author tiansang
	 */
	public class WeatherAPI 
	{
		private var _key:String;
		
		public function WeatherAPI(key:String = null) 
		{
			_key = key || "4cdc7ec87a100101121010";
		}
		
		public function getWeather(longitude:Number, latitude:Number):WeatherProxy
		{
			var proxy:WeatherProxy = new WeatherProxy();
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, proxy.onLoadComplete);
			//http://free.worldweatheronline.com/feed/marine.ashx?q=0.00,0.00&format=json&key=4cdc7ec87a100101121010
			var urlRequest:URLRequest = new URLRequest("http://free.worldweatheronline.com/feed/marine.ashx");
			var requestData:URLVariables = new URLVariables();
			requestData.q = longitude + "," + latitude;
			requestData.format = "json";
			requestData.key = _key;
			urlRequest.data = requestData;
			urlLoader.load(urlRequest);
			return proxy;
		}
	}

}