package 
{
	import flash.display.FrameLabel;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	public class SimCityButton
	{
		public static const FRAME_UP : String = "up";
		public static const FRAME_DOWN : String = "down";
		public static const FRAME_OVER : String = "over";
		public static const FRAME_DISABLE : String = "disable";
		public static const FRAME_SELECTED : String = "selected";
		public static const FRAME_SELECTED_OVER : String = "selectedOver";
		public static const FRAME_SELECTED_DOWN : String = "selectedDown";
		
		protected var _button : MovieClip;
		
		protected var _enable : Boolean;
		protected var _selected : Boolean;
		
		public function SimCityButton(button : MovieClip, mouseChildren:Boolean = false)
		{
			_button = button;
			_button.buttonMode = true;
			_button.enabled = true;
			_button.mouseChildren = mouseChildren;
			
			_enable = true;
			
			removeListeners();
			initListeners();
			
			changeLanguageContent();
		}
		
		protected function initListeners():void
		{
			_button.addEventListener(MouseEvent.CLICK, clickHandler);
			
			_button.addEventListener(MouseEvent.MOUSE_UP, buttonUpListener);
			_button.addEventListener(MouseEvent.MOUSE_DOWN, buttonDownListener);
			_button.addEventListener(MouseEvent.ROLL_OVER, buttonOverListener);
			_button.addEventListener(MouseEvent.ROLL_OUT, buttonOutListener);
			_button.addEventListener(MouseEvent.MOUSE_MOVE, buttonMoveListener);
		}
		
		protected function removeListeners():void
		{
			_button.removeEventListener(MouseEvent.CLICK, clickHandler);
			
			_button.removeEventListener(MouseEvent.MOUSE_UP, buttonUpListener);
			_button.removeEventListener(MouseEvent.MOUSE_DOWN, buttonDownListener);
			_button.removeEventListener(MouseEvent.ROLL_OVER, buttonOverListener);
			_button.removeEventListener(MouseEvent.ROLL_OUT, buttonOutListener);
			_button.removeEventListener(MouseEvent.MOUSE_MOVE, buttonMoveListener);
		}
		
		
		public function changeLanguageContent():void
		{
			
		}
		
		public function show(isShow:Boolean = true):void
		{
			_button.visible = isShow;
		}
		
		public function hide():void
		{
			_button.visible = false;
		}
		
		protected function clickHandler(e : MouseEvent) : void
		{
		}
		
		protected function createTip():void
		{
			//if sub call has tip, should override in here.
		}
		
		protected function removeTip():void
		{
			
		}
		
		public function initTipContent(tip:MovieClip):void
		{
			//if sub call has tip, should override in here.
		}
		
		public function get enable():Boolean
		{
			return _enable;
		}
		
		public function set enable(value:Boolean):void
		{
			if(value)
			{
				if(_selected)
				{
					_button.gotoAndStop(FRAME_SELECTED);
				}
				else
				{
					_button.gotoAndStop(FRAME_UP);
				}
			}
			else
			{
				_button.gotoAndStop(FRAME_DISABLE);
			}
			_enable = value;
			_button.enabled = value;
		}
		
		public function get selected():Boolean
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void
		{
			if(_enable)
			{
				if(value)
				{
					_button.gotoAndStop(FRAME_SELECTED);
				}
				else
				{
					_button.gotoAndStop(FRAME_UP);
				}
			}
			
			_selected = value;
		}
		
		public function get button():MovieClip
		{
			return _button;
		}
		
		protected function buttonUpListener(e:MouseEvent):void
		{
			if(_enable)
			{
				if(_selected)
				{
					if(haveFrameLable(_button, FRAME_SELECTED))
					{
						_button.gotoAndStop(FRAME_SELECTED);
					}
				}
				else
				{
					_button.gotoAndStop(FRAME_UP);
				}
			}
		}
		
		protected function buttonDownListener(e : MouseEvent):void
		{
			if(_enable)
			{
				if(_selected)
				{
					if(haveFrameLable(_button, FRAME_SELECTED_DOWN))
					{
						_button.gotoAndStop(FRAME_SELECTED_DOWN);
					}
				}
				else
				{
					_button.gotoAndStop(FRAME_DOWN);
				}
			}
		}
		
		protected function buttonOverListener(e : MouseEvent):void
		{
			if(_enable)
			{
				if(_selected)
				{
					if(haveFrameLable(_button, FRAME_SELECTED_OVER))
					{
						_button.gotoAndStop(FRAME_SELECTED_OVER);
					}
				}
				else
				{
					_button.gotoAndStop(FRAME_OVER);
				}
			}
			
			createTip();
		}
		
		protected function buttonOutListener(e : MouseEvent):void
		{
			if(_enable)
			{
				if(_selected)
				{
					if(haveFrameLable(_button, FRAME_SELECTED))
					{
						_button.gotoAndStop(FRAME_SELECTED);
					}
				}
				else
				{
					_button.gotoAndStop(FRAME_UP);
				}
			}
			removeTip();
		}
		
		protected function buttonMoveListener(e : MouseEvent):void
		{
			
		}
		
		public function getGlobalRect() : Rectangle
		{
			return button.getRect(button.stage);
		}
		
		public function isShow():Boolean
		{
			return _button.visible;
		}
		
		public static function haveFrameLable(mc:MovieClip,frameName:String):Boolean
		{
			var array:Array = mc.currentLabels;
			for(var i:int = 0 ; i < array.length ; i++)
			{
				var frameLabel :FrameLabel =  array[i];
				if(frameLabel.name == frameName)
					return true;
			}
			return false;
		}
		
		public function destory():void
		{
			removeListeners();
			_button = null;
			
		}
	}
}