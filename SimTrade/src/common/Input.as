package common 
{
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	/**
	 * Insprired from Unity's Input system.
	 * 
	 * @author tiansang
	 */
	public class Input 
	{	
		public static const AXIS_HORIZONTAL:String = "Horizontal";
		public static const AXIS_VERTICAL:String = "Vertical";
		
		private static var _axes:Dictionary = new Dictionary();
		private static var _negativeKeyMap:Dictionary = new Dictionary();
		private static var _positiveKeyMap:Dictionary = new Dictionary();
		
		private static var _pressingKeys:Dictionary = new Dictionary();
		
		public static function addAxis(name:String, negativeButtons:Array, positiveButtons:Array, gravity:Number = 3, sensitivity:Number = 3):void
		{
			var inputAxis:InputAxis = new InputAxis(name, negativeButtons, positiveButtons, gravity, sensitivity);
			_axes[name] = inputAxis;
			var button:*;
			for each(button in negativeButtons)
			{
				_negativeKeyMap[button] = inputAxis;
			}
			for each(button in positiveButtons)
			{
				_positiveKeyMap[button] = inputAxis;
			}
		}
		
		public static function getAxis(name:String):Number
		{
			var inputAxis:InputAxis = _axes[name];
			if (inputAxis == null)
				return 0;
			return inputAxis.value;
		}
		
		public static function getKeyDown(keyCode:uint):Boolean
		{
			return (_pressingKeys[keyCode] == 1);
		}
		
		public static function getKeyUp(keyCode:uint):Boolean
		{
			return (_pressingKeys[keyCode] < 1);
		}
		
		public static function initialize(stage:Stage):void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			
			addAxis(AXIS_HORIZONTAL, [Keyboard.A, Keyboard.LEFT], [Keyboard.D, Keyboard.RIGHT]);
			addAxis(AXIS_VERTICAL, [Keyboard.W, Keyboard.UP], [Keyboard.S, Keyboard.DOWN]);
		}
		
		static private function handleKeyUp(e:KeyboardEvent):void 
		{
			_pressingKeys[e.keyCode] = 0;
		}
		
		static private function handleKeyDown(e:KeyboardEvent):void 
		{
			_pressingKeys[e.keyCode] = 1;
		}
		
		public static function update():void
		{
			a: for (var name:String in _axes)
			{
				var inputAxis:InputAxis = _axes[name];
				for (var keyCode:* in _pressingKeys)
				{
					if (inputAxis.negativeButtons.indexOf(keyCode) >= 0)
					{
						inputAxis.value -= inputAxis.sensitivity * Time.deltaSecond;
						continue a;
					}
					else if (inputAxis.positiveButtons.indexOf(keyCode) >= 0)
					{
						inputAxis.value += inputAxis.sensitivity * Time.deltaSecond;
						continue a;
					}
				}
				if (inputAxis.value != 0)
				{
					inputAxis.value -= (inputAxis.value > 0 ? 1 : -1) * inputAxis.gravity * Time.deltaSecond;
					if (Math.abs(inputAxis.value) < 0.1)
						inputAxis.value = 0;
				}
				//trace(inputAxis.name, inputAxis.value);
			}
			
			for (keyCode in _pressingKeys)
			{
				if (_pressingKeys[keyCode] == 0)
					_pressingKeys[keyCode] = -1;
				else if(_pressingKeys[keyCode] == -1)
					delete _pressingKeys[keyCode];
			}
		}
	}

}

class InputAxis
{
	public var name:String;
	public var negativeButtons:Array;
	public var positiveButtons:Array;
	public var gravity:Number;
	public var sensitivity:Number;
	private var _value:Number;
	
	public function get value():Number 
	{
		return _value;
	}
	
	public function set value(value:Number):void 
	{
		if (value < -1)
			value = -1;
		else if (value > 1)
			value = 1;
		_value = value;
	}
	
	public function InputAxis(name:String, negativeButtons:Array, positiveButtons:Array, gravity:Number, sensitivity:Number)
	{
		this.name = name;
		this.negativeButtons = negativeButtons;
		this.positiveButtons = positiveButtons;
		this.gravity = gravity;
		this.sensitivity = sensitivity;
		this.value = 0;
	}
}