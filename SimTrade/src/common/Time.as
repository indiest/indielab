package common 
{
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author 
	 */
	public class Time 
	{
		private static var _lastTick:uint = 0;
		private static var _deltaTime:uint = 0;
		
		public static function tick():void
		{
			var tick:int = getTimer();
			if (_lastTick > 0)
			{
				_deltaTime = tick - _lastTick;
			}
			_lastTick = tick;
		}
		
		public static function get lastTickTime():uint
		{
			return _lastTick;
		}
		
		public static function get deltaTime():uint
		{
			return _deltaTime;
		}
		
		public static function get deltaSecond():Number
		{
			return deltaTime / 1000;
		}
	}

}