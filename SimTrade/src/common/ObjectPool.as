package common 
{
	import common.logging.Log;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class ObjectPool 
	{
		private var _clazz:Class;
		private var _objects:Vector.<Poolable>;
		private var _borrowedCount:uint = 0;
		
		public function get maxSize():uint 
		{
			return _objects.length;
		}
		
		public function get borrowedCount():uint 
		{
			return _borrowedCount;
		}
		
		/**
		 * 
		 * @param	maxSize
		 * @param	clazz
		 * @param	initializer	Function<object:Object, index:uint>
		 */
		public function ObjectPool(maxSize:uint, clazz:Class, initializer:Function = null) 
		{
			_clazz = clazz;
			_objects = new Vector.<Poolable>(maxSize, true);
			for (var i:uint = 0; i < maxSize; i++)
			{
				_objects[i] = new Poolable(new clazz);
				if (initializer != null)
					initializer(_objects[i].object, i);
			}
		}
		
		public function borrowObject():*
		{
			if (borrowedCount >= maxSize)
				return null;
			for each(var p:Poolable in _objects)
			{
				if (p.borrowable)
				{
					p.borrowable = false;
					_borrowedCount++;
					return p.object;
				}
			}
			Log.warn("ObjectPool is full, cannot borrow object.");
			return null;
		}
		
		public function returnObject(object:Object):Boolean
		{
			if (!(object is _clazz))
			{
				//throw new ArgumentError("object is not a instance of " + _clazz);
				Log.warn("The returned object is not a instance of " + _clazz);
				return false;
			}
			for each(var p:Poolable in _objects)
			{
				if (p.object === object)
				{
					p.borrowable = true;
					_borrowedCount--;
					return true;
				}
			}
			return false;
		}
		
		public function returnAll():void 
		{
			for each(var p:Poolable in _objects)
			{
				if (p.borrowable == false)
				{
					p.borrowable = true;
					_borrowedCount--;
				}
			}
		}
		
	}
}

class Poolable
{
	public var borrowable:Boolean = true;
	public var object:Object;
	
	public function Poolable(object:Object, borrowable:Boolean = true)
	{
		this.object = object;
		this.borrowable = borrowable;
	}
}
