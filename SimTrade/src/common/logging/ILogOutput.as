package common.logging 
{
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public interface ILogOutput 
	{
		function write(text:String):void;
	}
	
}