package common.logging 
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class TraceOutput implements ILogOutput 
	{
		
		public function TraceOutput() 
		{
			
		}
		
		public function write(text:String):void
		{
			trace(text);
		}
		
	}

}