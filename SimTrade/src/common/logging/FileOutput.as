package common.logging 
{
	import com.rockyouasia.common.util.FileUtil;
	import com.rockyouasia.common.util.StringUtil;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class FileOutput implements ILogOutput 
	{
		private var _fs:FileStream = new FileStream();
		
		public function FileOutput(path:String = null) 
		{
			if (path == null)
			{
				path = "app-storage:/log/" + StringUtil.getLogFormatTimeString(new Date()) + ".log";
			}
			var file:File = new File(path);
			_fs.open(file, FileMode.APPEND);
		}
		
		public function write(text:String):void
		{
			_fs.writeUTFBytes(text + "\r\n");
		}
	}

}