package common.logging 
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Log 
	{
		private static const LEVEL_PREFIXES:Vector.<String> = new <String>["[DEBUG] ", "[INFO ] ", "[WARN ] ", "[ERROR] "];
		public static const LEVEL_DEBUG:uint = 0;
		public static const LEVEL_INFO:uint = 1;
		public static const LEVEL_WARN:uint = 2;
		public static const LEVEL_ERROR:uint = 3;
		
		public static var outputs:Vector.<ILogOutput> = new <ILogOutput>[new TraceOutput];
		public static var thresholdLevel:uint = 0;
		
		public function Log()
		{
			
		}
		
		public static function write(text:String, level:uint):void
		{
			if (level < thresholdLevel)
				return;
			text = LEVEL_PREFIXES[level] + text;
			for each(var output:ILogOutput in outputs)
				output.write(text);
		}
		
		public static function get isDebugEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_DEBUG;
		}
		
		public static function get isInfoEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_INFO;
		}
		
		public static function get isWarnEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_WARN;
		}
		
		public static function get isErrorEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_ERROR;
		}
		
		public static function debug(...text):void
		{
			if (isDebugEnabled)
				write(text.join(" "), LEVEL_DEBUG);
		}
		
		public static function info(...text):void
		{
			if (isInfoEnabled)
				write(text.join(" "), LEVEL_INFO);
		}
		
		public static function warn(...text):void
		{
			if (isWarnEnabled)
				write(text.join(" "), LEVEL_WARN);
		}
		
		public static function error(...text):void
		{
			if (isErrorEnabled)
				write(text.join(" "), LEVEL_ERROR);
		}
	}

}
