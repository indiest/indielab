package common.state 
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author 
	 */
	public class BaseState extends Sprite
	{
		
		public function get isTransientState():Boolean 
		{
			return false;
		}
		
		public function enter():void 
		{
			
		}
		
		public function update():void 
		{
			
		}
		
		public function exit():void 
		{
			
		}
		
	}

}