package common.state 
{
	import flash.display.DisplayObjectContainer;
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author 
	 */
	public class StateManager 
	{
		private var _previousState:BaseState;
		private var _currentState:BaseState;
		private var _states:Object = { };
		private var _stateLayer:DisplayObjectContainer;
		
		public function StateManager(stateLayer:DisplayObjectContainer) 
		{
			_stateLayer = stateLayer;
		}
		
		public function get currentState():BaseState 
		{
			return _currentState;
		}
		
		// @param state	A BaseState instance or a BaseState class object.
		public function loadState(state:Object):BaseState
		{
			if (_currentState != null)
			{
				_currentState.exit();
				// don't save transient state as previous, because it may blocks when go back to it.
				if (!_currentState.isTransientState)
					_previousState = _currentState;
				_stateLayer.removeChild(_currentState);
			}
			var name:String = getQualifiedClassName(state);
			_currentState = _states[name];
			if (_currentState == null)
			{
				_currentState = state is Class ? new state() as BaseState : state as BaseState;
				if (_currentState == null)
					throw new ArgumentError("BaseState is not a valid BaseState instance or BaseState class object");
				_states[name] = _currentState;
			}
			_stateLayer.addChild(_currentState);
			_currentState.enter();
			return _currentState;
		}
		
		public function getState(stateClass:Class):BaseState
		{
			return _states[getQualifiedClassName(stateClass)];
		}
		
		public function backState():BaseState
		{
			if (_previousState == null)
				return null;
			return loadState(_previousState);
		}
		
		public function update():void
		{
			if (_currentState != null)
				_currentState.update();
		}
	}

}