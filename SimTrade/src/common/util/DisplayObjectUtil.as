package common.util 
{
	import common.logging.Log;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class DisplayObjectUtil 
	{
		public static const GREYSCALE_FILTERS:Array = [
			new ColorMatrixFilter([
				0.33, 0.33, 0.33, 0, 0,
				0.33, 0.33, 0.33, 0, 0,
				0.33, 0.33, 0.33, 0, 0,
				0, 0, 0, 1, 0])
		];
		
		public static function tint(obj:DisplayObject, multiplier:Number = 1, color:uint = 0):void
		{
			var ct:ColorTransform = obj.transform.colorTransform;
			ct.color = color;
			ct.redMultiplier = ct.greenMultiplier = ct.blueMultiplier = multiplier;
			obj.transform.colorTransform = ct;
		}
		
		public static function center(obj:DisplayObject, container:DisplayObjectContainer = null):void
		{
			centerX(obj, container);
			centerY(obj, container);
		}
		
		public static function centerX(obj:DisplayObject, container:DisplayObjectContainer = null):void
		{
			obj.x = ((container ? container.width : obj.stage.stageWidth) - obj.width) / 2;
		}
		
		public static function centerY(obj:DisplayObject, container:DisplayObjectContainer = null):void
		{
			obj.y = ((container ? container.height : obj.stage.stageHeight) - obj.height) / 2;
		}		
		
		public static function fixSize(obj:DisplayObject, width:Number, height:Number):void 
		{
			var widthRatio:Number = width / obj.width;
			var heightRatio:Number = height / obj.height;
			var scale:Number = Math.min(widthRatio, heightRatio, 1);
			obj.scaleX = obj.scaleY = scale;
		}
		
		public static function removeFromParent(child:DisplayObject):void 
		{
			if (child.parent != null)
				child.parent.removeChild(child);
		}
		
		public static function clearContainer(container:DisplayObjectContainer):void 
		{
			while (container.numChildren > 0)
				container.removeChildAt(0);
		}
		
		/**
		 * Iterate all sub nodes of the specified display object to the leaf nodes.
		 * @param	obj root container
		 * @param	callback [param1:DisplayObject(child object)]. The root node will be called at first time.
		 */
		public static function iterateAllChildren(obj:DisplayObject, callback:Function = null):void
		{
			if (callback != null)
			{
				if (callback.length == 0)
					callback();
				else if (callback.length == 1)
					callback(obj);
			}
			if (obj is DisplayObjectContainer)
			{
				var container:DisplayObjectContainer = obj as DisplayObjectContainer;
				for (var i:uint = 0; i < container.numChildren; i++)
				{
					iterateAllChildren(container.getChildAt(i), callback);
				}
			}
		}
		
		private static function handleStopMovieClip(obj:DisplayObject):void 
		{
			if (obj is MovieClip)
				MovieClip(obj).stop();
			else if (obj is SimpleButton)
			{
				var button:SimpleButton = obj as SimpleButton;
				stopAllMovieClips(button.upState);
				stopAllMovieClips(button.downState);
				stopAllMovieClips(button.overState);
			}
		}
		
		/**
		 * Stops all MovieClips in sub nodes of the specified display object, including all the states of SimpleButton.
		 */
		public static function stopAllMovieClips(obj:DisplayObject):void
		{
			iterateAllChildren(obj, handleStopMovieClip);
		}
		
		
		
		private static var _coverCache:Dictionary = new Dictionary(true);
		
		public static function makeCover(container:DisplayObjectContainer):Bitmap
		{
			var bitmap:Bitmap = _coverCache[container];
			if (!bitmap)
			{
				var width:int = container.width;
				var height:int = container.height;
				/*
				if (container.stage)
				{
					if (width > container.stage.stageWidth)
						width = container.stage.stageWidth;
					if (height > container.stage.stageHeight)
						height = container.stage.stageHeight;
				}
				*/
				if (Log.isDebugEnabled)
					Log.debug("Creating bitmap cover in size:", width, height);
				bitmap = new Bitmap(new BitmapData(width, height, true, 0));
				_coverCache[container] = bitmap;
			}
			var time:int = getTimer();
			var matrix:Matrix = new Matrix();
			var bounds:Rectangle = container.getBounds(container);
			matrix.translate(-bounds.x, -bounds.y);
			bitmap.x = bounds.x;
			bitmap.y = bounds.y;
			bitmap.bitmapData.draw(container, matrix);
			if (Log.isDebugEnabled)
				Log.debug("Rendered cover bitmap in ms:", getTimer() - time);
			container.addChild(bitmap);
			for (var i:uint = 0; i < container.numChildren - 1; i++)
				container.getChildAt(i).visible = false;
			return bitmap;
		}
		
		public static function removeCover(container:DisplayObjectContainer, dispose:Boolean = false):void
		{
			var bitmap:Bitmap = _coverCache[container];
			if (!bitmap)
				return;
			for (var i:uint = 0; i < container.numChildren - 1; i++)
				container.getChildAt(i).visible = true;
			removeFromParent(bitmap);
			if (dispose)
			{
				delete _coverCache[container];
				bitmap.bitmapData.dispose();
			}
		}
		
		
		
		/* load/play SWF utility section */
		private static var _movieCache:Object = { };
		private static var _movieMask:Sprite;
		
		private static function initMask():void
		{
			_movieMask = new Sprite();
			_movieMask.graphics.beginFill(0, 0);
			_movieMask.graphics.drawRect(0, 0, 1, 1);
			_movieMask.graphics.endFill();
			_movieMask.mouseChildren = false;
		}
		
		public static function playMovie(url:String, container:DisplayObjectContainer, cache:Boolean = false, movieEndCallback:Function = null, movieLoadedCallback:Function = null):void 
		{
			if (_movieCache[url])
			{
				playMovieClip(_movieCache[url], container, true, movieEndCallback);
			}
			else
			{
				var loader:MovieClipLoader = new MovieClipLoader();
				loader.movieContainer = container;
				loader.cacheKey = cache ? url : null;
				loader.movieEndCallback = movieEndCallback;
				loader.movieLoadedCallback = movieLoadedCallback;
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleMovieLoaded);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleMovieLoadError);
				loader.load(new URLRequest(url));
			}
		}
		
		static private function handleMovieLoadError(e:IOErrorEvent):void 
		{
			var loaderInfo:LoaderInfo = e.currentTarget as LoaderInfo;
			loaderInfo.removeEventListener(Event.COMPLETE, handleMovieLoaded);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleMovieLoadError);
			Log.error("Unable to load and play MovieClip:", loaderInfo.url);
			var loader:MovieClipLoader = loaderInfo.loader as MovieClipLoader;
			loader.cacheKey = null;
			if (loader.movieEndCallback != null)
			{
				loader.movieEndCallback();
				loader.movieEndCallback = null;
			}
		}
		
		static private function handleMovieLoaded(e:Event):void 
		{
			var loaderInfo:LoaderInfo = e.currentTarget as LoaderInfo;
			loaderInfo.removeEventListener(Event.COMPLETE, handleMovieLoaded);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleMovieLoadError);
			var loader:MovieClipLoader = loaderInfo.loader as MovieClipLoader;
			var mc:MovieClip = loaderInfo.content as MovieClip;
			if (loader.cacheKey)
				_movieCache[loader.cacheKey] = mc;
			if (loader.movieLoadedCallback != null)
				loader.movieLoadedCallback(mc);
			playMovieClip(mc, loader.movieContainer, true, loader.movieEndCallback);
			loader.unload();
			loader.movieContainer = null;
			loader.cacheKey = null;
			loader.movieEndCallback = null;
		}
		
		public static function playOnce(mc:MovieClip):void
		{
			mc.addEventListener(Event.EXIT_FRAME, stopLastFrame);
			mc.gotoAndPlay(1);
		}
		
		static private function stopLastFrame(e:Event):void 
		{
			var mc:MovieClip = e.currentTarget as MovieClip;
			if (mc.currentFrame < mc.totalFrames)
				return;	
			
			mc.removeEventListener(Event.EXIT_FRAME, stopLastFrame);	
			mc.gotoAndStop(mc.totalFrames);
		}		
		
		
		public static function playMovieClip(mc:MovieClip, container:DisplayObjectContainer, cover:Boolean = true, movieEndCallback:Function = null):void
		{
			/*
			if (_movieMask == null)
				initMask();
			_movieMask.width = container.width;
			_movieMask.height = container.height;
			container.addChild(_movieMask);
			*/
			if (cover)
				makeCover(container);
			
			mc.movieEndCallback = movieEndCallback;
			mc.addEventListener(Event.EXIT_FRAME, handleMovieFrame);
			mc.gotoAndPlay(1);
			if (!container.contains(mc))
				container.addChild(mc);
		}
		
		static private function handleMovieFrame(e:Event):void 
		{
			var mc:MovieClip = e.currentTarget as MovieClip;
			if (mc.currentFrame < mc.totalFrames)
				return;
				
			mc.removeEventListener(Event.EXIT_FRAME, handleMovieFrame);
			if (mc.movieEndCallback != null)
			{
				mc.movieEndCallback();
				mc.movieEndCallback = null;
			}
			
			removeCover(mc.parent);
			//removeFromParent(_movieMask);
			
			removeFromParent(mc);
			mc.gotoAndStop(1);
		}		
		
		private static const MATRIX_ROF_HIT_TEST:Matrix = new Matrix();
		private static const POINT_FOR_HIT_TEST:Point = new Point(0,0);
		/**
		 * 检测某个点是否在一个显示对象的非透明区域(是否点中)
		 * @param	object 要检测的显示对象
		 * @param	stageX 要检测的点x(全局坐标)
		 * @param   stageY 要检测的点y(全局坐标)
		 * @return true Or false
		 */
		public static function realHitTest(object:DisplayObject, stageX:Number, stageY:Number):Boolean 
		{  
			if (object == null)
				return false; 
			var bmapData:BitmapData = new BitmapData(1, 1, true, 0x00000000);	
			var localPt:Point = object.globalToLocal(new Point(stageX, stageY));
			MATRIX_ROF_HIT_TEST.tx = -localPt.x;  
			MATRIX_ROF_HIT_TEST.ty = -localPt.y;  
			bmapData.draw(object, MATRIX_ROF_HIT_TEST);   
			var returnVal:Boolean = bmapData.hitTest(POINT_FOR_HIT_TEST, 128, POINT_FOR_HIT_TEST);   
			bmapData.dispose();   
			return returnVal;  
		} 
	}

}

import flash.display.DisplayObjectContainer;
import flash.display.Loader;

class MovieClipLoader extends Loader
{
	public var movieContainer:DisplayObjectContainer;
	public var cacheKey:String;
	public var movieEndCallback:Function;
	public var movieLoadedCallback:Function;
}
		/* end of load/play SWF utility section */
