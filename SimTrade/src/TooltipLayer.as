package  
{
	import com.greensock.TweenNano;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.text.TextField;
	/**
	 * ...
	 * @author tiansang
	 */
	public class TooltipLayer extends MapLayer 
	{
		private var _locInfoText:TextField;
		private var _weatherIconHolder:Sprite;
		
		private var _commonTooltip:Sprite;
		private var _commonTooltipText:TextField;
		private var _currentTooltip:DisplayObject;
		
		private var _numberText:TextField;
		
		public function TooltipLayer() 
		{
			_locInfoText = new TextField();
			_locInfoText.x = 10;
			_locInfoText.y = 10;
			_locInfoText.background = true;
			_locInfoText.backgroundColor = 0xffffff;
			_locInfoText.opaqueBackground = 0.7;
			//addChild(_locInfoText);
			
			_weatherIconHolder = new Sprite();
			_weatherIconHolder.x = 10;
			_weatherIconHolder.y = 60;
			addChild(_weatherIconHolder);
			
			_commonTooltip = new Sprite();
			_commonTooltipText = new TextField();
			_commonTooltipText.width = 160;
			_commonTooltipText.height = 160;
			_commonTooltipText.multiline = true;
			_commonTooltipText.wordWrap = true;
			_commonTooltip.addChild(_commonTooltipText);
			_commonTooltip.graphics.beginFill(0xffffff, 0.8);
			_commonTooltip.graphics.drawRect(0, 0, _commonTooltip.width, _commonTooltip.height);
			_commonTooltip.graphics.endFill();
			
			_numberText = new TextField();
			_numberText.alpha = 0;
			addChild(_numberText);
		}
		
		public function setLocInfo(text:String):void
		{
			_locInfoText.text = text;
		}
		
		public function setWeatherIcon(icon:DisplayObject):void
		{
			_weatherIconHolder.removeChildren();
			_weatherIconHolder.addChild(icon);
		}
		
		override public function update():void 
		{
			super.update();
			
		}
		
		public function showTooltip(tooltip:DisplayObject):void
		{
			_currentTooltip = tooltip;
			addChild(tooltip);
		}
		
		public function hideTooltip():void
		{
			if (_currentTooltip && _currentTooltip.parent)
			{
				removeChild(_currentTooltip);
			}
		}
		
		public function showCommonTooltip(x:Number, y:Number, htmlText:String):void 
		{
			_commonTooltip.x = x - _commonTooltip.width / 2;
			_commonTooltip.y = y + 50;
			_commonTooltipText.htmlText = htmlText;
			showTooltip(_commonTooltip);
		}
		
		public function showNumber(x:Number, y:Number, value:Number):void 
		{
			_numberText.text = value.toString();
			_numberText.textColor = value < 0 ? 0xff0000 : 0x00ff00;
			_numberText.x = x;
			_numberText.y = y;
			_numberText.alpha = 1;
			TweenNano.to(_numberText, 1, {y: y - 100, alpha: 0} );
		}
	}

}