package  
{
	import common.state.BaseState;
	import flash.display.AVM1Movie;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.StatusEvent;
	import flash.net.LocalConnection;
	import flash.system.ApplicationDomain;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * @author 
	 */
	public class MinigameState extends BaseState 
	{
		public static const FRAME_INSTRUCTIONS:String = "Instructions";
		public static const FRAME_GAME_OVER_LOSE:String = "Game Over Lose";
		public static const FRAME_GAME_OVER_WIN:String = "Game Over Win";
		
		public static var minigameLoader:Loader;
		private var _minigameMc:AVM1Movie;
		private var _lc:LocalConnection = new LocalConnection();
		
		public function MinigameState() 
		{
			_minigameMc = minigameLoader.content as AVM1Movie;
			//_minigameMc.addEventListener(Event.ENTER_FRAME, handleEnterFrame);
			_lc.client = this;
			_lc.allowDomain("*");
			_lc.addEventListener(StatusEvent.STATUS, handleStatus);
			_lc.connect("minigame");
			
			//minigameLoader.x = (Game.current.stageWidth - minigameLoader.width) * 0.5;
			//minigameLoader.y = (Game.current.stageHeight - minigameLoader.height) * 0.5;
			addChild(minigameLoader);
			
		}
		
		private function handleStatus(e:StatusEvent):void 
		{
			trace(e);
		}
		
		public function onGameOver(score:int):void
		{
			trace(score);
		}
		
		override public function enter():void 
		{
			super.enter();
			Game.stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
		}
		
		private function handleKeyDown(e:KeyboardEvent):void 
		{
			if (e.keyCode == Keyboard.ESCAPE)
				Game.current.stateManager.loadState(MapState);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			trace(_minigameMc['currentFrameLabel']);
			//if (minigameMc.currentFrameLabel == FRAME_GAME_OVER_LOSE)
			//{
				//
			//}
		}
		
		override public function exit():void 
		{
			super.exit();
			//_lc.close();
			Game.stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
		}
	}

}