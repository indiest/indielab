package  
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class City extends MapObject 
	{
		public var id:int;
		public var friend:Object;
		
		public function City(data:Object) 
		{
			id = data.id;
			for each(var friend:Object in Data.friends)
			{
				if (friend.cityId == id)
				{
					this.friend = friend;
					break;
				}
			}
			name = data.name;
			x = data.x;
			y = data.y;
			graphics.beginFill(0xff0000);
			//graphics.lineStyle(3, 0xff0000);
			graphics.drawCircle(0, 0, 10);
			graphics.endFill();
		}
		
		override public function getTooltip():String 
		{
			var htmlText:String = "<p>City: " + name + "</p>";
			if (friend)
			{
				htmlText += "<p><img src='" + friend.imageUrl + "' width='100' height='100'/></p><p>Friend: " + friend.name + "</p>";
			}
			else
			{
				htmlText += "<p>No friend yet. Click to add friend.</p>";
			}
			return htmlText;
		}
		
		override public function onClicked():void 
		{
			super.onClicked();
			state.selectCity(this);
		}
	}

}