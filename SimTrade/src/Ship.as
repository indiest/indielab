package  
{
	import common.logging.Log;
	import common.Time;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.utils.setInterval;
	
	/**
	 * ...
	 * @author 
	 */
	public class Ship extends MapObject 
	{
		private var _path:Vector.<Point> = new Vector.<Point>();
		private var _weather:WeatherProxy;
		private var _showNumberTimer:Number;
		private var _shipMc:ship = new ship();
		private var _minigameTimer:Number;
		
		public var speed:Number = 100;
		public var homeCity:City;
		public var currentCity:City;
		public var destCity:City;
		public var currentWeather:int;
		
		public function get path():Vector.<Point> 
		{
			return _path;
		}
		
		public function Ship(homeCity:City) 
		{
			this.homeCity = homeCity;
			this.currentCity = homeCity;
			x = homeCity.x;
			y = homeCity.y;
			//graphics.beginFill(0x777777);
			//graphics.drawRect(0, 0, 40, 20);
			addChild(_shipMc);
			_shipMc.gotoAndStop(3);
		}
		
		public function sailTo(destCity:City):void
		{
			if (currentCity == null)
			{
				Log.error("Ship must start sail from a city!");
				return;
			}
			for each(var seawayData:Object in Data.seaways)
			{
				if (seawayData.fromCityId == currentCity.id && seawayData.toCityId == destCity.id)
				{
					currentCity = null;
					this.destCity = destCity;
					startPath(seawayData.path);
					return;
				}
			}
			Log.error("Can't find seaway from", currentCity.name, "to", destCity.name);
		}
		
		private function startPath(points:Array, startAtFirstPoint:Boolean = false):void
		{
			if (points.length == 0)
				return;
			if (startAtFirstPoint)
			{
				x = points[0].x;
				y = points[0].y;
			}
			_path.length = 0;
			for each(var p:* in points)
				_path.push(new Point(p.x, p.y));
		}
		
		override public function update():void
		{
			if (_showNumberTimer > 0)
			{
				_showNumberTimer -= Time.deltaSecond;
				if (_showNumberTimer <= 0)
				{
					state.tooltipLayer.hideTooltip();
					state.tooltipLayer.showNumber(screenPosition.x, screenPosition.y, -100);
				}
			}
			
			if (_minigameTimer > 0)
			{
				_minigameTimer -= Time.deltaSecond;
				if (_minigameTimer <= 0)
				{
					Game.current.stateManager.loadState(MinigameState);
				}
			}

			if (_path.length == 0)
				return;
			var nextPoint:Point = _path[0];
			var nx:Number = nextPoint.x - x;
			var ny:Number = nextPoint.y - y;
			var nd:Number = Math.sqrt(nx * nx + ny * ny);
			var dist:Number = Time.deltaSecond * speed;
			if (dist >= nd)
			{
				x = nextPoint.x;
				y = nextPoint.y;
				_path.shift();
				if (_path.length == 0)
				{
					// reached destination
					currentCity = destCity;
					destCity = null;
					_minigameTimer = 1;
				}
				
				randomWeather();
				if (currentWeather == WeatherUtil.WEATHER_STORM)
				{
					state.shipLayer.showWeather(this);
					_showNumberTimer = 2;
				}
				
				//_weather = Game.current.weatherAPI.getWeather(
					//state.worldmapLayer.getLongituteFromX(x),
					//state.worldmapLayer.getLatituteFromY(y));
				//state.tooltipLayer.setWeatherIcon(_weather.weatherIcon);
			}
			else
			{
				x += dist * nx / nd;
				y += dist * ny / nd;
				var angle:Number = Math.acos(nx / nd) * (ny > 0 ? -1 : 1) - Math.PI / 2;
				var frame:int = 4 * angle / Math.PI + 5;
				if (frame < 1)
					frame += _shipMc.totalFrames;
				else if (frame > _shipMc.totalFrames)
					frame -= _shipMc.totalFrames;
				_shipMc.gotoAndStop(frame);
			}
		}
		
		public function randomWeather():void 
		{
			currentWeather = WeatherUtil.randomWeather();	
		}
		
/*		
		override public function getTooltip():String 
		{
			var htmlText:String = "<p>Home city: " + homeCity.name + "</p>";
			if (destCity)
			{
				htmlText += "<p>Dest city: " + destCity.name + "</p><p>Local weather:</p>";// 
				if (_weather && _weather.weatherData)
					htmlText += "<p><img src='" + _weather.weatherData.weatherIconUrl[0].value + "'/></p>";
				else
					htmlText += "Loading...";
			}
			return htmlText;
		}
*/
		override public function onClicked():void 
		{
			super.onClicked();
		}
	}

}