package  
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author tiansang
	 */
	public class MapLayer extends Sprite 
	{
		protected var _objects:Vector.<MapObject> = new Vector.<MapObject>();
		
		public function get objects():Vector.<MapObject> 
		{
			return _objects;
		}
		
		public function MapLayer() 
		{
			addMouseEventListeners();
		}
		
		protected function addMouseEventListeners():void 
		{
			addEventListener(MouseEvent.CLICK, handleMouseClick);
			addEventListener(MouseEvent.MOUSE_OVER, handleMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, handleMouseOut);
		}
		
		private function handleMouseClick(e:MouseEvent):void 
		{
			if (e.target is MapObject)
				onMouseClick(e.target as MapObject);
		}
		
		private function handleMouseOut(e:MouseEvent):void 
		{
			if (e.target is MapObject)
				onMouseLeave(e.target as MapObject);
		}
		
		private function handleMouseOver(e:MouseEvent):void 
		{
			if (e.target is MapObject)
				onMouseHover(e.target as MapObject);
		}
		
		public function addMapObject(obj:MapObject):void
		{
			objects.push(obj);
			addChild(obj);
		}
		
		public function removeMapObject(obj:MapObject):void
		{
			var index:int = objects.indexOf(obj);
			if (index >= 0)
			{
				objects.splice(index, 1);
				removeChild(obj);
			}
		}
		
		protected function get state():MapState
		{
			return Game.current.stateManager.currentState as MapState;
		}
		
		public function worldToScreen(point:Point):Point
		{
			return state.worldmapLayer.localToGlobal(point);
		}
		
		public function refresh(left:Number, top:Number):void
		{
			
		}
		
		public function update():void
		{
			for each(var obj:MapObject in objects)
				obj.update();
		}
		
		protected function onMouseClick(obj:MapObject):void
		{
			obj.onClicked();
		}
		
		protected function onMouseHover(obj:MapObject):void
		{
			var tooltip:String = obj.getTooltip();
			if (tooltip)
			{
				var p:Point = worldToScreen(obj.worldPosition);
				state.tooltipLayer.showCommonTooltip(p.x, p.y, tooltip);
			}
		}
		
		protected function onMouseLeave(obj:MapObject):void
		{
			state.tooltipLayer.hideTooltip();
		}
	}

}