package 
{
	import common.Input;
	import common.state.StateManager;
	import common.Time;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author tiansang
	 */
	[SWF(width="800", height="600")]
	public class Game extends Sprite 
	{
		
		public static var current:Game;
		public static var stage:Stage;
		
		public var stageWidth:int = 800;
		public var stageHeight:int = 600;

		private var _stateLayer:Sprite;
		private var _stateManager:StateManager;
		private var _weatherAPI:WeatherAPI = new WeatherAPI();
		
		public function get stateManager():StateManager 
		{
			return _stateManager;
		}
		
		public function get weatherAPI():WeatherAPI 
		{
			return _weatherAPI;
		}
		
		public function Game() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			Game.current = this;
			Game.stage = this.stage;
			
			stageWidth = stage.stageWidth;
			stageHeight = stage.stageHeight;
			
			Data.loadAll();
			
			_stateLayer = new Sprite();
			addChild(_stateLayer);
			
			_stateManager = new StateManager(_stateLayer);
			_stateManager.loadState(LoadingState);
			
			Input.initialize(stage);
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			Time.tick();
			Input.update();
			_stateManager.update();
		}
		
	}
	
}