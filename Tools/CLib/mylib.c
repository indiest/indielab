// Copyright (c) 2013 Adobe Systems Inc

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "mylib.h"

int checkSum(const unsigned char* buffer, int bufferSize){
	int i;
	int stupidSum = 0;
	
	for (i = 0; i < bufferSize; i++){
		stupidSum += *buffer;
		buffer++;
	}
	return stupidSum;
}

void encryptSwf(unsigned char* buffer, int len, int seed) {
	int blocks = len / 4;
	int* intBuffer = (int*)buffer;
	int i;
	seed = seed ^ 0x013266ca;
	for (i = 0; i < blocks; i++) {
		*(intBuffer + i) = intBuffer[i] ^ seed;
	}
}