#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mylib.h"

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		printf("Usage: encswf seed input.swf output.swf\n");
		return -1; 
	}
	
	int seed = atoi(argv[1]);
	printf("Encrypt with seed %d\n", seed);

	FILE *fp = fopen(argv[2], "r");
	if (fp == NULL)
	{
		printf("Can't open input file\n");
		return -1;
	}
	
 	// obtain file size:
	fseek (fp, 0, SEEK_END);
	long lSize = ftell (fp);
	rewind (fp);

	// allocate memory to contain the whole file:
	unsigned char* buffer = (unsigned char*) malloc (sizeof(unsigned char)*lSize);
	if (buffer == NULL)
	{
		printf ("Memory error");
		return -2;
	}

	// copy the file into the buffer:
	size_t result = fread (buffer, 1, lSize, fp);
	if (result != lSize)
	{	
		printf ("Reading error");
		return -3;
	}
	fclose (fp);
	
	printf("Checksum before encrypt: %d\n", checkSum(buffer, (int)lSize));
	encryptSwf(buffer, (int)lSize, seed);
	printf("Checksum after  encrypt: %d\n", checkSum(buffer, (int)lSize));
	
	fp = fopen(argv[3], "wb");
	if (fp == NULL)
	{
		printf("Can't open output file\n");
		return -1;
	}
	
	fwrite(buffer, 1, lSize, fp);
	fclose(fp);
	
	// terminate
	free (buffer);	
	
	return 0;
}
