package  
{
	import com.gamua.flox.Flox;
	import com.gamua.flox.Player;
	import com.gamua.flox.Query;
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.text.TextField;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.logging.Logger;
	import me.sangtian.common.logging.TextFieldOutput;
	import me.sangtian.common.util.FileUtil;
	import me.sangtian.common.util.StringUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class EntityDumper extends Sprite 
	{
		private static const logger:Logger = Logger.create(EntityDumper);
		
		public var gameID:String = "xBJBZoGfJm9KCbvV";
		public var gameKey:String = "9elmk5hRiXyBGquI";
		public var gameVersion:String = "0.7.2";
		public var outputDir:String = "app-storage:/DungeonUp";
		
		private var outputPath:File;
		private var mailList:Array = [];
		
		public function EntityDumper() 
		{
			var tf:TextField = new TextField();
			tf.width = stage.stageWidth;
			tf.height = stage.stageHeight;
			tf.textColor = 0xffffff;
			tf.multiline = true;
			tf.wordWrap = true;
			addChild(tf);
			Log.outputs.push(new TextFieldOutput(tf));
			
			outputPath = File.applicationDirectory.resolvePath(outputDir);
			if (outputPath.exists == false)
				outputPath.createDirectory();
			
			Flox.init(gameID, gameKey, gameVersion);
			Player.loginWithKey("Mga5GoCHmYswlTGa", onLoginComplete, onError);
		}
		
		private function onLoginComplete(p:Player):void 
		{
			// Legacy players
			new Query(Player).find(onLoadPlayerComplete, onError);
			
		}
		
		private function onLoadPlayerComplete(players:Array):void 
		{
			for each(var player:Player in players)
			{
				//mailList.push(player.authId);
			}
			
			var query:Query = new Query(MyPlayer);
			query.limit = int.MAX_VALUE;
			query.find(onLoadMyPlayersComplete, onError);
		}
		
		private function onLoadMyPlayersComplete(players:Array):void 
		{
			var playerDir:File = outputPath.resolvePath("Players");
			if (playerDir.exists == false)
			{
				playerDir.createDirectory();
			}
			
			for each(var player:MyPlayer in players)
			{
				//var playerDir:File = outputPath.resolvePath(player.id);
				//if (playerDir.exists == false)
				//{
					//playerDir.createDirectory();
					//logger.info("Created player dir:", player.id);
				//}
					
				FileUtil.writeTextFile(
					playerDir.resolvePath(player.id + "_" + player.email + ".json"),
					JSON.stringify( {
						id: player.id,
						email: player.email,
						createdAt: player.createdAt,
						updatedAt: player.updatedAt,
						appVersion: player.appVersion,
						isSpender: player.isSpender,
						language: player.language,
						stones: player.stones
					}, null, "\t")
				);
				
				if (player.email)
					mailList.push(player.email);
			}
			
			FileUtil.writeTextFile(outputPath.resolvePath("mail_list.csv"), mailList.join("\r\n"));
			//var query:Query = new Query(PlayStatsEntity);
			//query.find(onEntityComplete, onError);
		}
		
		private function onEntityComplete(entities:Array):void
		{
			logger.info("Entities found:", entities.length);
			var entityDir:File = outputPath.resolvePath("PlayerStats");
			for each(var entity:PlayStatsEntity in entities)
			{
				//var entityDir:File = outputPath.resolvePath(entity.ownerId);
				//if (!entityDir.exists)
				//{
					//entityDir.createDirectory();
					//logger.info("Created player dir:", entity.ownerId);
				//}
				var csvText:String = 
					"Mode:," + entity.mode + 
					",Difficulty:," + entity.difficulty + 
					",Duration:," + entity.duration +
					",Deaths:," + entity.deaths + 
					"\rLevel,Life,Attack,Defence,Coin,YellowKey,BlueKey";
				for each(var stats:Array in entity.levelStats)
				{
					csvText += "\r";
					csvText += stats.join(",");
				}
				FileUtil.writeTextFile(entityDir.resolvePath(
					entity.ownerId + "_" + StringUtil.getLogFormatDateString(entity.createdAt) + "_" + int(entity.duration) + ".csv"), csvText);
				logger.info("Wrote entity:", entity.id);
			}
		}
		
		private function onError(err:String):void
		{
			logger.error(err);
		}
		
	}

}