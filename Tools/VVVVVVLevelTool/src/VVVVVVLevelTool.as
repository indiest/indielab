package
{
	import com.adobe.serialization.json.JSON;
	
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileReference;
	import flash.system.ApplicationDomain;
	import flash.system.System;
	
	public class VVVVVVLevelTool extends Sprite
	{
		public function VVVVVVLevelTool()
		{
			var levelDir:File = File.documentsDirectory.resolvePath("VVVVVV");
			var fs:FileStream = new FileStream();
			var levels:Array = [];
			var appPath:String = File.applicationDirectory.nativePath;
			for each (var file:File in levelDir.getDirectoryListing())
			{
				if (file.extension == "vvvvvv")
				{
					trace("processing", file.name);
					
					fs.open(file, FileMode.READ);
					var content:String = fs.readUTFBytes(fs.bytesAvailable);
					var xml:XML = new XML(content);
					var meta:* = xml.Data.MetaData;
					levels.push(
					{
						File: file.name,
						Title: String(meta.Title),
						Creator: String(meta.Creator),
						Desc: String(meta.Desc1) + "\n" + String(meta.Desc2) + "\n" + String(meta.Desc3)
					});
					fs.close();
					
					fs.open(new File(appPath + "/levels/" + file.name), FileMode.WRITE);
					fs.writeUTFBytes(content);
					fs.close();
				}
			}
			
			fs.open(new File(appPath + "/levels.json"), FileMode.WRITE);
			fs.writeUTFBytes(com.adobe.serialization.json.JSON.encode(levels));
			fs.close();
		}
	}
}