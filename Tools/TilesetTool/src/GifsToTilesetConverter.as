package  
{
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PNGEncoderOptions;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.InvokeEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import me.sangtian.common.logging.Logger;
	import me.sangtian.common.util.FileUtil;
	import me.sangtian.common.util.StringUtil;
	import org.bytearray.gif.decoder.GIFDecoder;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class GifsToTilesetConverter extends Sprite 
	{
		private static const logger:Logger = Logger.create(GifsToTilesetConverter);
		
		public var input:String = "monsters";
		public var output:String = "monsters.png";
		public var tileWidth:uint = 64;
		public var tileHeight:uint = 16;
		public var tileWidthPx:uint = 24;
		public var tileHeightPx:uint = 24;
		public var frameOffsetX:int = 0;
		public var frameOffsetY:int = 1;
		
		private static const ARGUMENT_MAPPINGS:Object =
		{
			"-i": "input",
			"-o": "output",
			"-tw": "tileWidth",
			"-th": "tileHeight",
			"-twp": "tileWidthPx",
			"-thp": "tileHeightPx",
			"-fox": "frameOffsetX",
			"-foy": "frameOffsetY"
		};
		
		public function GifsToTilesetConverter() 
		{
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, handleInvoke);
		}
		
		private function handleInvoke(e:InvokeEvent):void 
		{
			for each (var arg:String in e.arguments)
			{
				if (arg.indexOf("=") > 0)
				{
					var arr:Array = arg.split("=");
					var argName:String = arr[0];
					if (ARGUMENT_MAPPINGS[argName] != null)
					{
						argName = ARGUMENT_MAPPINGS[argName];
					}
					if (this.hasOwnProperty(argName))
					{
						this[argName] = arr[1];
						logger.info("Set property:", argName, "=", arr[1]);
						continue;
					}
				}
				logger.warn("Invalid argument:", arg);
			}
			
			convert();
			
		}
		
		private function convert():void
		{
			var tileset:BitmapData = new BitmapData(tileWidth * tileWidthPx, tileHeight * tileHeightPx, true, 0);
			stage.stageWidth = tileset.width;
			stage.stageHeight = tileset.height;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			var gifDir:File = new File(File.applicationDirectory.nativePath).resolvePath(input);
			
			var fs:FileStream = new FileStream();
			var decoder:GIFDecoder = new GIFDecoder();
			var tileIndex:uint = 0;
			for each(var file:File in gifDir.getDirectoryListing())
			{
				if (file.exists && !file.isDirectory && file.extension.toLowerCase() == "gif")
				{
					logger.info("Reading", file.nativePath);
					//tileIndex++;
					tileIndex = StringUtil.getNumber(file.name);
					
					var bytes:ByteArray = new ByteArray();
					fs.open(file, FileMode.READ);
					fs.readBytes(bytes);
					fs.close();
					bytes.position = 0;
					decoder.read(bytes);
					logger.info("GIF Decoded, frames:", decoder.getFrameCount(), "size:", decoder.getFrameSize());
					
					copyToTileset(decoder, tileset, tileIndex % tileWidth, tileIndex / tileWidth);
				}
			}
			
			var targetFile:File = new File(File.applicationDirectory.nativePath).resolvePath(output);
			fs.open(targetFile, FileMode.WRITE);
			fs.writeBytes(tileset.encode(tileset.rect, new PNGEncoderOptions()));
			fs.close();
			logger.info("PNG written to", targetFile.nativePath);
			
			addChild(new Bitmap(tileset));
		}
		
		private var _pointHelper:Point = new Point();
		private var _rectHelper:Rectangle = new Rectangle();
		private function copyToTileset(decoder:GIFDecoder, tileset:BitmapData, startTileX:uint, startTileY:uint):void 
		{
			for (var i:int = 0; i < decoder.getFrameCount(); i++)
			{
				var source:BitmapData = decoder.getFrame(i).bitmapData;
				var frameTileWidth:uint = source.width / tileWidthPx;
				var frameTileHeight:uint = source.height / tileHeightPx;
				for (var tileY:int = 0; tileY < frameTileHeight; tileY++)
				{
					for (var tileX:int = 0; tileX < frameTileWidth; tileX++)
					{
						_pointHelper.setTo((startTileX + tileX + tileY * frameTileWidth + frameOffsetX * i) * tileWidthPx, (startTileY + frameOffsetY * i) * tileHeightPx);
						_rectHelper.setTo(tileX * tileWidthPx, tileY * tileHeightPx, tileWidthPx, tileHeightPx);
						tileset.copyPixels(source, _rectHelper, _pointHelper);
					}
				}
			}
		}
	}

}