package  
{
	import com.bit101.components.ColorChooser;
	import com.bit101.components.PushButton;
	import flash.display.Sprite;
	import me.sangtian.common.ui.UIUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class ControlBox extends Sprite 
	{
		private var _gridlines:Gridlines;
		private var _lineColorChooser:ColorChooser;
		
		public function ControlBox(gridlines:Gridlines) 
		{
			_gridlines = gridlines;
			
			UIUtil.addPropertyCheckbox(this, 0, 0, _gridlines, "visible");
			UIUtil.addPropertySlider(this, 0, 20, _gridlines, "x", 0, 15, 1);
			UIUtil.addPropertySlider(this, 0, 40, _gridlines, "y", 0, 16, 1);
			UIUtil.addPropertyColorChooser(this, 0, 60, _gridlines, "lineColor");
			UIUtil.addPropertySlider(this, 0, 80, _gridlines, "alpha", 0, 1);
			UIUtil.addPropertySlider(this, 0, 100, _gridlines, "lineThickness", 1, 8, 1);
			UIUtil.addPropertySlider(this, 0, 120, _gridlines, "gridWidth", 0, 16, 1);
			UIUtil.addPropertySlider(this, 0, 140, _gridlines, "gridHeight", 0, 16, 1);
			UIUtil.addPropertySlider(this, 0, 160, _gridlines, "sizeX", 1, 2 >> 12, 16);
			UIUtil.addPropertySlider(this, 0, 180, _gridlines, "sizeY", 1, 2 >> 12, 16);
		}
	}

}