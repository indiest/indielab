package  
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Gridlines extends Sprite 
	{
		public var lineColor:uint = 0x000000;
		public var lineThickness:Number = 1;
		public var gridWidth:Number = 4;
		public var gridHeight:Number = 4;
		public var sizeX:Number;
		public var sizeY:Number;
		
		public function Gridlines(sizeX:Number, sizeY:Number) 
		{
			this.sizeX = sizeX;
			this.sizeY = sizeY;
		}
		
		public function draw():void
		{
			graphics.clear();
			graphics.lineStyle(lineThickness, lineColor);
			if (gridWidth > 0)
			{
				for (var x:Number = 0; x < sizeX; x += gridWidth)
				{
					graphics.moveTo(x, 0);
					graphics.lineTo(x, sizeY);
				}
			}
			if (gridHeight > 0)
			{
				for (var y:Number = 0; y < sizeY; y += gridHeight)
				{
					graphics.moveTo(0, y);
					graphics.lineTo(sizeX, y);
				}
			}
		}
	}

}