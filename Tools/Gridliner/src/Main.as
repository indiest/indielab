package 
{
	import com.bit101.components.ColorChooser;
	import com.bit101.components.PushButton;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.JPEGEncoderOptions;
	import flash.display.Loader;
	import flash.display.PNGEncoderOptions;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import me.sangtian.common.ui.UIUtil;
	import me.sangtian.common.util.DisplayObjectUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Main extends Sprite 
	{
		public static const GRIDLINES_COUNT:int = 3;
		
		private var _fileRefLoad:FileReference = new FileReference();
		private var _fileRefSave:FileReference = new FileReference();
		private var _imageLoader:Loader = new Loader();
		private var _imageContainer:Sprite = new Sprite();
		private var _gridsContainer:Sprite = new Sprite();
	
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			_fileRefLoad.addEventListener(Event.SELECT, handleSelectFile);
			_fileRefLoad.addEventListener(Event.COMPLETE, handleFileLoaded);
			_fileRefLoad.addEventListener(Event.CANCEL, handleFileCancel);
			_fileRefLoad.addEventListener(IOErrorEvent.IO_ERROR, handleIoError);
			_fileRefLoad.addEventListener(Event.OPEN, handleOpenFile);
			
			//_fileRefSave.addEventListener(Event.SELECT, handleSelectFile);
			//_fileRefSave.addEventListener(Event.COMPLETE, handleFileLoaded);
			_fileRefSave.addEventListener(Event.CANCEL, handleFileCancel);
			_fileRefSave.addEventListener(IOErrorEvent.IO_ERROR, handleIoError);
			_fileRefSave.addEventListener(Event.OPEN, handleOpenFile);
			
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleImageLoaded);
			_imageContainer.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDownImage);
			_imageContainer.addEventListener(MouseEvent.MOUSE_UP, handleMouseUpImage);
			_imageContainer.addEventListener(MouseEvent.ROLL_OUT, handleMouseUpImage);
			_imageContainer.useHandCursor = true;
			addChild(_imageContainer);

			_gridsContainer.mouseEnabled = false;
			_gridsContainer.mouseChildren = false;
			addChild(_gridsContainer);
			
			var buttonLoad:PushButton = new PushButton(this, 0, 0, "Load...", handleClickLoad);
			var buttonSave:PushButton = new PushButton(this, 100, 0, "Save...", handleClickSave);
			for (var i:int = 0; i < GRIDLINES_COUNT; i++)
			{
				var gridlines:Gridlines = new Gridlines(1024, 1024);
				gridlines.visible = (i == 0);
				gridlines.alpha = 0.1;
				gridlines.x = gridlines.y = -i;
				_gridsContainer.addChild(gridlines);
				var controlBox:ControlBox = new ControlBox(gridlines);
				controlBox.y = 32 + i * (controlBox.height + 32);
				addChild(controlBox);
			}
			
			//stage.addEventListener(MouseEvent.MOUSE_UP, updateGrids);
			addEventListener(Event.ENTER_FRAME, updateGrids);
		}
		
		private function handleClickSave(e:Event):void 
		{
			var bd:BitmapData = DisplayObjectUtil.drawToBitmapData(_gridsContainer);
			_fileRefSave.save(bd.encode(bd.rect, new PNGEncoderOptions()), "grid.png");
		}
		
		private function handleMouseUpImage(e:MouseEvent):void 
		{
			_imageContainer.stopDrag();
		}
		
		private function handleMouseDownImage(e:MouseEvent):void 
		{
			_imageContainer.startDrag();
		}
		
		private function handleImageLoaded(e:Event):void 
		{
			_imageContainer.removeChildren();
			_imageContainer.addChild(_imageLoader.content);
			DisplayObjectUtil.center(_imageContainer, this);
		}
		
		private function updateGrids(e:Event):void 
		{
			for (var i:int = 0; i < _gridsContainer.numChildren; i++)
			{
				var gridlines:Gridlines = _gridsContainer.getChildAt(i) as Gridlines;
				if (gridlines.visible)
					gridlines.draw();
			}
		}
		
		private function handleClickLoad(e:Event):void 
		{
			_fileRefLoad.browse();// [new FileFilter("Image Files", "jpg;png")]);
		}
		
		private function handleSelectFile(e:Event):void 
		{
			var fileRef:FileReference = e.currentTarget as FileReference;
			//fileRef.removeEventListener(Event.SELECT, handleSelectFile);
			if (fileRef == _fileRefLoad)
			{
				fileRef.load();
			}
		}
		
		private function handleFileCancel(e:Event):void 
		{
			trace("File Cancel!");
		}
		
		private function handleOpenFile(e:Event):void 
		{
			trace("File Open:", (e.currentTarget as FileReference).name);
		}
		
		private function handleIoError(e:IOErrorEvent):void 
		{
			trace(e);
		}
		
		private function handleFileLoaded(e:Event):void 
		{
			//_fileRef.removeEventListener(Event.COMPLETE, handleFileLoaded);
			_imageLoader.loadBytes(_fileRefLoad.data);
		}
		
	}
	
}