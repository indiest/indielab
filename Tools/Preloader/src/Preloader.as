package  
{
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.SecurityDomain;
	import flash.text.TextField;
	import me.sangtian.common.AssetsCache;
	
	/**
	 * Common used Preloader for web releases.
	 * It depends on a preload.json in the same path. Format:
		 * [
		 * 		{"path": "PATH_OF_SWF", "cache": (optional)true/false, "main": (optional)true/false}
		 * ]
	 * 
	 * @author Nicolas Tian
	 */
	public class Preloader extends Sprite 
	{
		private static var appId:String;
		private static var appVersion:String;

		private var _urlLoader:URLLoader = new URLLoader();
		private var _preloadList:Array;
		private var _loadingIndex:int = 0;
		private var _loader:Loader = new Loader();
		private var _proportion:Number;
		private var _percent:Number = 0;
		private var _textProgress:TextField = new TextField();
		private var _mainSWF:DisplayObject;
		
		private function get loadingItem():Object
		{
			return _preloadList[_loadingIndex];
		}
		
		public function Preloader() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			appId = ExternalInterface.objectID || loaderInfo.parameters["id"];
			appVersion = loaderInfo.parameters["version"];
			
			_loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, handleProgress);
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleLoadComplete);
			//_loader.contentLoaderInfo.addEventListener(Event.UNLOAD, handleUnloaded);
			//_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleLoadError);
			
			_textProgress.selectable = false;
			_textProgress.multiline = true;
			_textProgress.textColor = 0xffffff - stage.color;
			addChild(_textProgress);
			
			//loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, handleUncaughtError);
			_urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			_urlLoader.addEventListener(Event.COMPLETE, handleListLoaded);
			_urlLoader.load(new URLRequest("preload.json"));
		}

		/*
		private function handleLoadError(e:IOErrorEvent):void 
		{
			trace(e);
		}
		
		private function handleUncaughtError(e:UncaughtErrorEvent):void 
		{
			e.preventDefault();
			trace(e.error);
		}
		*/
		
		private function handleListLoaded(e:Event):void 
		{
			var text:String = _urlLoader.data;
			_preloadList = JSON.parse(text) as Array;
			_loadingIndex = 0;
			_percent = 0;
			loadNext();
		}
		
		private function loadNext():void 
		{
			if (loadingItem == null)
			{
				stage.addChild(_mainSWF);
				stage.removeChild(this);
			}
			else
			{
				_proportion = loadingItem.propotion || (100 / _preloadList.length);
				const loaderContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
				loaderContext.allowCodeImport = true;
				_loader.load(new URLRequest(loadingItem.path), loaderContext);
			}
		}
		
		private function handleProgress(e:ProgressEvent):void 
		{
			var percent:Number = _percent + (e.bytesLoaded / e.bytesTotal) * _proportion;
			_textProgress.text = percent.toFixed() + "%";
		}
		
		private function handleLoadComplete(e:Event):void 
		{
			if (loadingItem.cache)
			{
				//if (_loader.content is MovieClip)
				//{
					//(_loader.content as MovieClip).stop();
				//}
				AssetsCache.cache(loadingItem.id || loadingItem.path, _loader.content);
			}
			if (loadingItem.main)
			{
				_mainSWF = _loader.content;
			}
			
			// Async operation
			_loader.unload();
			
//		}
		
//		private function handleUnloaded(e:Event):void
//		{
			_percent += _proportion;
			_loadingIndex++;
			loadNext();
		}
	}

}