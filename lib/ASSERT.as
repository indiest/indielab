package
{
	public function ASSERT(condition:Boolean, message:String = "ASSERT"):void
	{
		CONFIG::debug
		{
			if (condition == false)
			{
				var error:Error = new Error(message);
				throw error;
			}
		}
	}
}