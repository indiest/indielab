package me.sangtian.common 
{
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.logging.Logger;
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class ObjectPool 
	{
		private static const logger:Logger = Logger.create(ObjectPool);
		
		private var _provider:Function;
		private var _providerArgs:Array;
		private var _clazz:Class;
		private var _objects:Vector.<Poolable>;
		private var _borrowedCount:uint = 0;
		private var _autoExpand:Boolean = false;
		
		public function get maxSize():uint 
		{
			return _objects.length;
		}
		
		public function get borrowedCount():uint 
		{
			return _borrowedCount;
		}
		
		private function createObject():Poolable
		{
			var obj:Object = (_provider as Function).apply(null, _providerArgs);
			if (_clazz == null)
			{
				_clazz = ObjectUtil.getClass(obj);
			}
			return new Poolable(obj);
		}
		
		/**
		 * 
		 * @param	maxSize The pool will dynamicaly expand if maxSize is 0, otherwise the size is fixed.
		 * @param	provider A class object, or a function which returns an object.
		 * @param	providerArgs The args for the class constructor, or the function.
		 */
		public function ObjectPool(maxSize:uint, provider:*, providerArgs:Array = null) 
		{
			if (provider is Class)
			{
				_clazz = provider as Class;
				_provider = getProvider(_clazz, providerArgs);
			}
			else if (provider is Function)
			{
				_provider = provider;
			}
			else
			{
				throw new ArgumentError("Invalid provider");
			}
			_providerArgs = providerArgs;

			if (maxSize == 0)
			{
				_autoExpand = true;
				_objects = new Vector.<Poolable>();
			}
			else
			{
				_objects = new Vector.<Poolable>(maxSize, true);
				for (var i:uint = 0; i < maxSize; i++)
				{
					_objects[i] = createObject();
				}
			}
		}
		
		public function borrowObject():*
		{
			if (borrowedCount >= maxSize)
			{
				if (_autoExpand)
				{
					_objects.push(createObject());
				}
				else
				{
					return null;
				}
			}
			for each(var p:Poolable in _objects)
			{
				if (p.borrowable)
				{
					p.borrowable = false;
					_borrowedCount++;
					return p.object;
				}
			}
			logger.warn("ObjectPool is empty, cannot borrow object.");
			return null;
		}
		
		public function returnObject(object:Object):Boolean
		{
			if (!(object is _clazz))
			{
				//throw new ArgumentError("object is not a instance of " + _clazz);
				logger.warn("The returned object is not a instance of " + _clazz);
				return false;
			}
			for each(var p:Poolable in _objects)
			{
				if (p.object === object)
				{
					if (p.borrowable == true)
					{
						logger.warn("The returning object is not borrowed");
						continue;
					}
					p.borrowable = true;
					_borrowedCount--;
					return true;
				}
			}
			return false;
		}
		
		public function returnAll(callback:Function = null):Boolean 
		{
			for each(var p:Poolable in _objects)
			{
				if (p.borrowable == false)
				{
					if (callback != null)
						callback(p.object);
					p.borrowable = true;
					_borrowedCount--;
				}
			}
			return _borrowedCount == 0;
		}
	
		public static function getProvider(type:Class, args:Array = null):Function
		{
			return function():*
			{
				if (args == null)
					return new type();
				switch (args.length)
				{
					case 0:
						return new type();
						break;
					case 1:
						return new type(args[0]);
						break;
					case 2:
						return new type(args[0], args[1]);
						break;
					case 3:
						return new type(args[0], args[1], args[2]);
						break;
					case 4:
						return new type(args[0], args[1], args[2], args[3]);
						break;
					case 5:
						return new type(args[0], args[1], args[2], args[3], args[4]);
						break;
					case 6:
						return new type(args[0], args[1], args[2], args[3], args[4], args[5]);
						break;
					case 7:
						return new type(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
						break;
					case 8:
						return new type(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
						break;
					case 9:
						return new type(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
						break;
					case 10:
						return new type(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);
						break;
					default:
						return null;
				}
				return null;
			};
		}
	}
	
}

class Poolable
{
	public var borrowable:Boolean = true;
	public var object:Object;
	
	public function Poolable(object:Object, borrowable:Boolean = true)
	{
		this.object = object;
		this.borrowable = borrowable;
	}
}
