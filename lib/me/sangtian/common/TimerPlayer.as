package me.sangtian.common 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class TimerPlayer// extends Sprite 
	{
		private var _timer:Timer;
		private var _mc:MovieClip;
		private var _completeCallback:Function;
		
		public function TimerPlayer(framerate:uint = 30) 
		{
			_timer = new Timer(1000 / framerate);
			_timer.addEventListener(TimerEvent.TIMER, handleTimer);
		}
		
		private function handleTimer(e:TimerEvent):void 
		{
			trace("[TimerPlayer.handleTimer] mc.currentFrame:", _mc.currentFrame);
			if (_mc.currentFrame == _mc.totalFrames)
			{
				if (_completeCallback != null)
					_completeCallback(_mc);
				stop();
				return;
			}
			_mc.gotoAndStop(_mc.currentFrame + 1);
		}
		
		public function play(mc:MovieClip, completeCallback:Function = null):void
		{
			_mc = mc;
			_completeCallback = completeCallback;
			_mc.gotoAndStop(1);
			//addChild(_mc);
			_timer.repeatCount = mc.totalFrames;
			_timer.start();
		}
		
		public function stop():void
		{
			_timer.stop();
			//removeChild(_mc);
			_mc = null;
		}
	}

}