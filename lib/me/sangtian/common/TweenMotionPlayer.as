package me.sangtian.common 
{
	import com.greensock.easing.Elastic;
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class TweenMotionPlayer 
	{
		private var _motion:XML;
		private var _target:DisplayObject;
		private var _fps:uint;
		private var _currentFrame:uint;
		
		public function TweenMotionPlayer(motion:XML, target:DisplayObject, overrideInitProperty:Boolean = false) 
		{
			_motion = motion;
			_target = _target;
			
			var source:* = motion.source[0].Source;
			_fps = source.frameRate;
			if (overrideInitProperty)
			{
				target.x = source.@x;
				target.y = source.@y;
				target.scaleX = source.@scaleX;
				target.scaleY = source.@scaleY;
				target.rotation = source.@rotation;
			}
		}

		public function play(startFrame:uint = 0):void
		{
			var keyFrames:* = _motion.KeyFrame.(@index <= startFrame);
			var keyFrame:* = keyFrames[keyFrame.length() - 1];
			
			var frames:uint = keyFrame.@index - startFrame;
			_currentFrame = startFrame;
			frameIndex = keyFrame.@index;
			TweenLite.to(target, frames / fps, {
				x: keyFrame.@x
			});
				
		}
		
		public function stop():void
		{
			
		}
	}

}