package me.sangtian.common 
{
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TransformGestureEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.Dictionary;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * Insprired from Unity's Input system.
	 * 
	 * @author tiansang
	 */
	public class Input 
	{	
		public static const AXIS_HORIZONTAL:String = "Horizontal";
		public static const AXIS_VERTICAL:String = "Vertical";
		
		CONFIG::debug
		{
			private static var _keyCodeNameMapping:Object = ObjectUtil.constantsToObject(Keyboard, true);
		}
		
		private static var _stage:Stage;
		
		private static var _axes:Dictionary = new Dictionary();
		private static var _negativeKeyMap:Dictionary = new Dictionary();
		private static var _positiveKeyMap:Dictionary = new Dictionary();
		
		private static var _pressingKeys:Dictionary = new Dictionary();
		private static var _lastMousePos:Point;
		public static function get mousePos():Point
		{
			return _lastMousePos;
		}
		private static var _mouseDelta:Point = new Point();
		public static function get mouseDelta():Point 
		{
			return _mouseDelta;
		}
		
		private static var _mouseControlEnabled:Boolean = false;
		public static function get mouseControlEnabled():Boolean 
		{
			return _mouseControlEnabled;
		}
		public static function set mouseControlEnabled(value:Boolean):void 
		{
			if (value != _mouseControlEnabled)
			{
				if (value)
				{
					_stage.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseEvent);
					_stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseEvent);
				}
				else
				{
					_stage.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseEvent);
					_stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseEvent);
				}
			}
			_mouseControlEnabled = value;
		}
		
		private static var _mouseButtonDown:Boolean = false;
		public static function get mouseButtonDown():Boolean 
		{
			return _mouseButtonDown;
		}
		
		public static function addAxis(name:String, negativeButtons:Array, positiveButtons:Array, gravity:Number = 3, sensitivity:Number = 3):void
		{
			var inputAxis:InputAxis = new InputAxis(name, negativeButtons, positiveButtons, gravity, sensitivity);
			_axes[name] = inputAxis;
			var button:*;
			for each(button in negativeButtons)
			{
				_negativeKeyMap[button] = inputAxis;
			}
			for each(button in positiveButtons)
			{
				_positiveKeyMap[button] = inputAxis;
			}
		}
		
		public static function getAxis(name:String):Number
		{
			var inputAxis:InputAxis = _axes[name];
			if (inputAxis == null)
				return 0;
			return inputAxis.value;
		}
		
		public static function setAxis(name:String, value:Number):void
		{
			var inputAxis:InputAxis = _axes[name];
			if (inputAxis == null)
				return;
			inputAxis.value = value;
		}
		
		public static function getKeyDown(keyCode:uint):Boolean
		{
			return (_pressingKeys[keyCode] == 1);
		}
		
		public static function getKeyUp(keyCode:uint):Boolean
		{
			return (_pressingKeys[keyCode] < 1);
		}
		
		public static function initialize(stage:Stage):void
		{
			_stage = stage;
			_stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			_stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			
			addDefaultDirectionKeys();
		}
		
		private static function addDefaultDirectionKeys():void
		{
			addAxis(AXIS_HORIZONTAL, [Keyboard.A, Keyboard.LEFT], [Keyboard.D, Keyboard.RIGHT]);
			addAxis(AXIS_VERTICAL, [Keyboard.W, Keyboard.UP], [Keyboard.S, Keyboard.DOWN]);
		}
		
		public static function enableSwipeControl():void
		{
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			_stage.addEventListener(TransformGestureEvent.GESTURE_SWIPE, handleSwipe);
		}
		
		public static function disableSwipeControl():void
		{
			Multitouch.inputMode = MultitouchInputMode.NONE;
			_stage.removeEventListener(TransformGestureEvent.GESTURE_SWIPE, handleSwipe);
		}
		
		private static function handleSwipe(e:TransformGestureEvent):void 
		{
			setAxis(AXIS_HORIZONTAL, getAxis(AXIS_HORIZONTAL) + e.offsetX);// / _stage.stageWidth);
			setAxis(AXIS_VERTICAL, getAxis(AXIS_VERTICAL) + e.offsetY);// / _stage.stageHeight);
		}
		
		private static function handleMouseEvent(e:MouseEvent):void 
		{
			_mouseButtonDown = e.buttonDown;
		}
		
		static private function handleKeyUp(e:KeyboardEvent):void 
		{
			setKeyStatus(e.keyCode, false);
		}
		
		static private function handleKeyDown(e:KeyboardEvent):void 
		{
			CONFIG::debug
			{
				//Log.debug("[Input] Key down:", _keyCodeNameMapping[e.keyCode]);
			}
			setKeyStatus(e.keyCode, true);
		}
		
		private static function setKeyStatus(keyCode:uint, down:Boolean):void
		{
			_pressingKeys[keyCode] = down ? 1 : 0;
		}
		
		public static function update():void
		{
			// Input is not affected by scale factor
			var deltaSecond:Number = Time.deltaSecond / Time.scale;
			a: for (var name:String in _axes)
			{
				var inputAxis:InputAxis = _axes[name];
				for (var keyCode:* in _pressingKeys)
				{
					if (inputAxis.negativeButtons.indexOf(keyCode) >= 0)
					{
						inputAxis.value -= inputAxis.sensitivity * deltaSecond;
						continue a;
					}
					else if (inputAxis.positiveButtons.indexOf(keyCode) >= 0)
					{
						inputAxis.value += inputAxis.sensitivity * deltaSecond;
						continue a;
					}
				}
				if (inputAxis.value != 0)
				{
					inputAxis.value -= (inputAxis.value > 0 ? 1 : -1) * inputAxis.gravity * deltaSecond;
					if (Math.abs(inputAxis.value) < 0.1)
						inputAxis.value = 0;
				}
				//trace(inputAxis.name, inputAxis.value);
			}
			
			for (keyCode in _pressingKeys)
			{
				if (_pressingKeys[keyCode] == 0)
					_pressingKeys[keyCode] = -1;
				else if(_pressingKeys[keyCode] == -1)
					delete _pressingKeys[keyCode];
			}
			
			if (_lastMousePos == null)
			{
				_lastMousePos = new Point();
			}
			else
			{
				_mouseDelta.x = _stage.mouseX - _lastMousePos.x;
				_mouseDelta.y = _stage.mouseY - _lastMousePos.y;
			}
			_lastMousePos.x = _stage.mouseX;
			_lastMousePos.y = _stage.mouseY;
			
			if (mouseControlEnabled && mouseButtonDown)
			{
				inputAxis = _axes[AXIS_HORIZONTAL];
				inputAxis.value += _mouseDelta.x * deltaSecond;
				inputAxis = _axes[AXIS_VERTICAL];
				inputAxis.value += _mouseDelta.y * deltaSecond;
				//setKeyStatus(Keyboard.LEFT, _mouseDelta.x < 0);
				//setKeyStatus(Keyboard.RIGHT, _mouseDelta.x > 0);
				//setKeyStatus(Keyboard.UP, _mouseDelta.y < 0);
				//setKeyStatus(Keyboard.DOWN, _mouseDelta.y > 0);
			}
		}
		
		public static function reset():void
		{
			for (var name:String in _axes)
			{
				setAxis(name, 0);
			}
			for (var keyCode:* in _pressingKeys)
			{
				delete _pressingKeys[keyCode];
			}
		}
		
		public static function destroy():void
		{
			_stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			_stage.removeEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
			mouseControlEnabled = false;
			disableSwipeControl();
			
			_axes = new Dictionary();
			_negativeKeyMap = new Dictionary();
			_positiveKeyMap = new Dictionary();
			_pressingKeys = new Dictionary();
			_mouseDelta.x = _mouseDelta.y = 0;
			
			_stage = null;
		}
	}

}
import me.sangtian.common.logging.Log;
import me.sangtian.common.util.MathUtil;

class InputAxis
{
	public var name:String;
	public var negativeButtons:Array;
	public var positiveButtons:Array;
	public var gravity:Number;
	public var sensitivity:Number;
	private var _value:Number;
	
	public function get value():Number 
	{
		return _value;
	}
	
	public function set value(value:Number):void 
	{
		_value = MathUtil.clamp(value, -1, 1);
		CONFIG::debug
		{
			//Log.debug("[Input] Set Axis:", name, _value);
		}
	}
	
	public function InputAxis(name:String, negativeButtons:Array, positiveButtons:Array, gravity:Number, sensitivity:Number)
	{
		this.name = name;
		this.negativeButtons = negativeButtons;
		this.positiveButtons = positiveButtons;
		this.gravity = gravity;
		this.sensitivity = sensitivity;
		this.value = 0;
	}
}