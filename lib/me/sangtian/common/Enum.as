package me.sangtian.common 
{
	import flash.utils.describeType;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author tiansang
	 */
	public dynamic class Enum 
	{
		// Map<className, Array<instance>>
		private static var _typeMap:Dictionary = new Dictionary();		
		// Map<instance, constName>
		private static var _nameMap:Dictionary;
		
		private var _index:int;
		private var _className:String;
		private var _bitwise:Boolean;
		
		public function get index():int 
		{
			return _index;
		}
		
		public function Enum(index:int = -1, bitwise:Boolean = false) 
		{
			if (_className == null)
			{
				_className = getQualifiedClassName(this);
				//_clazz = getDefinitionByName(className);
				//trace(describeType(this));
			}
			var instances:Array = _typeMap[_className];
			if (instances == null)
			{
				instances = [];
				_typeMap[_className] = instances;
			}
			_index = index > 0 ? index : instances.length;
			instances[_index] = this;
			_bitwise = bitwise;
		}
		
		// DO NOT call this method if it's bitwise enum
		public static function fromIndex(classOrName:*, index:int):Enum
		{
			var className:String = classOrName is String ? classOrName : getQualifiedClassName(classOrName);
			var instances:Array = _typeMap[className];
			if (instances == null)
				return null;
			return instances[index];
		}
		
		public function checkBitwise(target:Enum):Boolean
		{
			if (typeof(this) != typeof(target))
				return false;
			if (!this._bitwise)
				return false;
			return (this.index & target.index) != 0;
		}
		
		private static function initNameMap():void 
		{
			_nameMap = new Dictionary();
			for (var className:String in _typeMap)
			{
				var type:Object = getDefinitionByName(className);
				var xml:XML = describeType(type);
				for each(var constant:* in xml.constant)
				{
					_nameMap[type[constant.@name]] = constant.@name;
				}
			}
		}
		
		public function toString():String
		{
			if (_nameMap == null)
				initNameMap();
			return _className + "." + _nameMap[this];
		}
	}

}