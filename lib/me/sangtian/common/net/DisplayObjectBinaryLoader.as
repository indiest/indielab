package me.sangtian.common.net
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.BinaryDataLoader;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.system.LoaderContext;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class DisplayObjectBinaryLoader extends BinaryDataLoader 
	{
		protected var _imageLoader:Loader;
		protected var _loaderContext:LoaderContext;
		protected var _imageLoadQueue:Vector.<QueueItem> = new Vector.<QueueItem>();
		protected var _onDecoded:Function;
		protected var _onAllLoaded:Function;
		protected var _decodedResources:Object = { };
		
		public function get decodedResources():Object 
		{
			return _decodedResources;
		}
		
		/**
		 * 
		 * @param	urlOrRequest
		 * @param	vars
		 * <ul>
		 * 	</li><strong>onDecoded:Function</strong> - Dispached when a file is decoded and cached. Parameters: filename:String, rawData:ByteArray, decodedData:*. Ignore if decodeResource is set to false.
		 * 	</li><strong>onAllLoaded:Function</strong> - Dispached when all items are loaded.
		 * </ul>
		 */
		public function DisplayObjectBinaryLoader(urlOrRequest:*, vars:Object = null) 
		{
			super(urlOrRequest, vars);
			if (vars)
			{
				_onDecoded = vars.onDecoded || null;
				_onAllLoaded = vars.onAllLoaded || null;
			}
			_imageLoader = new Loader();
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadImages);
			_loaderContext = new LoaderContext();
			// FIXME
			_loaderContext.allowCodeImport = true;			
		}
		
		public function addItemToDecode(name:String, content:ByteArray):void
		{
			_imageLoadQueue.push(new QueueItem(name, content));
		}
		
		protected function onDecoded(filename:String, rawData:ByteArray, decodedData:*):void 
		{
			if (_onDecoded != null)
				_onDecoded(filename, rawData, decodedData);
		}
		
		protected function loadImages(e:Event = null):void 
		{
			if (e == null)
			{
				var item:QueueItem = _imageLoadQueue.pop();
				if (item == null)
				{
					onAllLoaded();
					return;
				}
				_imageLoader.name = item.name;
				_imageLoader.loadBytes(item.content, _loaderContext);
			}
			else
			{
				if (_imageLoader.content is Bitmap)
					_decodedResources[_imageLoader.name] = (_imageLoader.content as Bitmap).bitmapData;
				else
					_decodedResources[_imageLoader.name] = _imageLoader.content;
				onDecoded(_imageLoader.name, _imageLoader.contentLoaderInfo.bytes, _decodedResources[_imageLoader.name]);
				loadImages();
			}
		}
		
		protected function onAllLoaded():void
		{
			_imageLoader.unload();
			super._completeHandler();
//			if (_onAllLoaded != null)
//				_onAllLoaded(content);
		}
		
		override protected function _completeHandler(event:Event = null):void 
		{
			addItemToDecode(url, content as ByteArray);
			loadImages();
		}
	}

}
import flash.utils.ByteArray;

class QueueItem
{
	public var name:String;
	public var content:ByteArray;
	
	public function QueueItem(name:String, content:ByteArray)
	{
		this.name = name;
		this.content = content;
	}
}