package me.sangtian.common.net 
{
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class URLRequestWithCallback //extends URLRequest
	{
		public var request:URLRequest;
		public var callback:Function;
		
		public function URLRequestWithCallback(url:String = null, callback:Function = null)
		{
			request = new URLRequest(url);
			this.callback = callback;
		}
	}

}