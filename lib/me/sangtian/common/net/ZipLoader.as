package me.sangtian.common.net 
{
	import com.greensock.loading.BinaryDataLoader;
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.system.LoaderContext;
	import flash.system.System;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class ZipLoader extends DisplayObjectBinaryLoader 
	{	
		private var _zip:FZip;
		private var _decodeResource:Boolean;
		
		/**
		 * 
		 * @param	urlOrRequest
		 * @param	vars
		 * <ul>
		 * 	<li><strong>decodeResource:Boolean</strong> - If true, files in following format will be decoded and cached: png, jpg, txt, json, csv, xml. Default value is false.
		 * 	</li><strong>onDecoded:Function</strong> - Dispached when a file is decoded and cached. Parameters: filename:String, rawData:ByteArray, decodedData:*. Ignore if decodeResource is set to false.
		 * 	</li><strong>onAllLoaded:Function</strong> - Dispached when all items are loaded.
		 * </ul>
		 */
		public function ZipLoader(urlOrRequest:*, vars:Object = null) 
		{
			super(urlOrRequest, vars);
			if (vars)
			{
				_decodeResource = vars.decodeResource || false;
			}
			_zip = new FZip();
			_zip.addEventListener(Event.COMPLETE, _zipCompleteHandler);
		}
		
		private function _zipCompleteHandler(e:Event):void 
		{
			var file:FZipFile;
			var i:uint;
			if (_decodeResource)
			{
				for (i = 0; i < _zip.getFileCount(); i++)
				{
					file = _zip.getFileAt(i);
					var ext:String = file.filename.substr(file.filename.length - 4, 4);
					if (ext == ".png" || ext == ".jpg" || ext == ".swf")
					{
						if (file.content.length > 0)
							addItemToDecode(file.filename, file.content);
					}
					else if (ext == ".txt" || ext == "json" || ext == ".xml" || ext == ".csv")
					{
						_decodedResources[file.filename] = file.getContentAsString();
						onDecoded(file.filename, file.content, _decodedResources[file.filename]);
					}
					else
					{
						_decodedResources[file.filename] = file.content;
					}
				}
				loadImages();
			}
			else
			{
				for (i; i < _zip.getFileCount(); i++)
				{
					file = _zip.getFileAt(i);
					_decodedResources[file.filename] = file.content;
				}
				onAllLoaded();
			}
		}
		
		override protected function onAllLoaded():void 
		{
			unloadAll();
			super.onAllLoaded();
		}
		
		private function unloadAll():void
		{
			while (_zip.getFileCount() > 0)
			{
				_zip.removeFileAt(0);
			}
			System.gc();
		}
		
		override protected function _completeHandler(event:Event = null):void 
		{
			_zip.loadBytes(_content as ByteArray);
			//super._completeHandler(event);
		}
		
		
	}

}