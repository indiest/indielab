package me.sangtian.common.net 
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class HTTPServiceEvent extends Event 
	{
		public static const RESPONSE:String = "response";
		public static const IO_ERROR:String = IOErrorEvent.IO_ERROR;
		public static const SECURITY_ERROR:String = SecurityErrorEvent.SECURITY_ERROR;
		public static const JSON_ERROR:String = "jsonError";
		
		public var request:Object;
		public var response:Object;
		public var error:String;
		
		public function HTTPServiceEvent(type:String, request:Object, response:Object = null, error:String = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{
			super(type, bubbles, cancelable);
			this.request = request;
			this.response = response;
			this.error = error;
		}
		
		public static function getErrorType(errorEvent:ErrorEvent):String
		{
			if (errorEvent is IOErrorEvent)
				return IO_ERROR;
			else if (errorEvent is SecurityErrorEvent)
				return SECURITY_ERROR;
			else
				return ErrorEvent.ERROR;
		}
	}

}