package me.sangtian.common.net 
{
	import flash.net.URLLoader;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class URLRequestLoader extends URLLoader 
	{
		public var request:URLRequestWithCallback;
		
		public function loadRequestWithCallback(request:URLRequestWithCallback):void 
		{
			this.request = request;
			super.load(request.request);
		}
	}

}