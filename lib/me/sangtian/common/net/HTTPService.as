package me.sangtian.common.net 
{
	import flash.system.Security;
	import me.sangtian.common.logging.Log;
	import flash.display.Shape;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import me.sangtian.common.logging.Logger;
	import me.sangtian.common.ObjectPool;
	
	[Event(name="response", type="me.sangtian.common.HTTPServiceEvent")] 
	[Event(name="error", type="me.sangtian.common.HTTPServiceEvent")] 
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class HTTPService extends EventDispatcher 
	{
		private static const logger:Logger = Logger.create(HTTPService);

		private var _loaderPool:ObjectPool;
		private var _requestQueue:Vector.<URLRequestWithCallback> = new Vector.<URLRequestWithCallback>();
		private var _url:String;
		private var _method:String = "POST";
		
		/** @private A reference to the Shape that we use to drive all our ENTER_FRAME events. **/
		private var _shape:Shape = new Shape();
		
		public function get url():String 
		{
			return _url;
		}
		
		public function set url(value:String):void 
		{
			_url = value;
		}
		
		public function get method():String 
		{
			return _method;
		}
		
		public function set method(value:String):void 
		{
			_method = value;
		}
		
		public function HTTPService(maxLoaderCount:uint = 1, url:String = null) 
		{
			_loaderPool = new ObjectPool(maxLoaderCount, initLoader);
			this.url = url;
			_shape.addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		private function initLoader():URLRequestLoader
		{
			var loader:URLRequestLoader = new URLRequestLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, handleLoadComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, handleError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleError);
			return loader;
		}
		
		private function handleError(e:ErrorEvent):void 
		{
			logger.warn("HTTP Service Error:", e.toString());
			var loader:URLRequestLoader = e.currentTarget as URLRequestLoader;
			var requestVars:URLVariables = loader.request.request.data as URLVariables;
			var callback:Function = loader.request.callback;
			loader.request = null;
			_loaderPool.returnObject(loader);
			var event:HTTPServiceEvent = new HTTPServiceEvent(HTTPServiceEvent.getErrorType(e), requestVars, null, e.toString());
			dispatchEvent(event);
			if (callback != null)
				callback(event);
		}
		
		private function handleLoadComplete(e:Event):void 
		{
			var loader:URLRequestLoader = e.currentTarget as URLRequestLoader;
			var requestVars:URLVariables = loader.request.request.data as URLVariables;
			var callback:Function = loader.request.callback;
			loader.request = null;
			var json:String = loader.data;
			if (logger.isDebugEnabled)
				logger.debug(json);
			loader.data = null;
			loader.close();
			_loaderPool.returnObject(loader);
			var response:Object;
			var event:HTTPServiceEvent;
			try
			{
				response = JSON.parse(json);
				event = new HTTPServiceEvent(HTTPServiceEvent.RESPONSE, requestVars, response);
			}
			catch (err:Error)
			{
				event = new HTTPServiceEvent(HTTPServiceEvent.JSON_ERROR, requestVars, null, err.toString());
			}
			dispatchEvent(event);
			if (callback != null)
				callback(event);
		}
		
		private function update(e:Event = null):void 
		{
			while (_requestQueue.length > 0)
			{
				var loader:URLRequestLoader = _loaderPool.borrowObject();
				if (loader == null)
					break;
				var request:URLRequestWithCallback = _requestQueue.pop();
				if (logger.isDebugEnabled)
				{
					logger.debug("Loading request", _url, ", Method:", request.request.method, ", Data:", request.request.data);
				}
				loader.loadRequestWithCallback(request);
			}
		}
		
		public function invoke(data:Object, callback:Function = null):void
		{
			var request:URLRequestWithCallback = new URLRequestWithCallback(url, callback);
			request.request.method = method;
			if (data is String)
			{
				request.request.data = data;
			}
			else
			{
				var vars:URLVariables = new URLVariables();
				for (var key:String in data)
				{
					vars[key] = data[key];
				}
				request.request.data = vars;
			}
			_requestQueue.unshift(request);
		}
		
		public function clearQueue():void
		{
			_requestQueue.length = 0;
			_loaderPool.returnAll();
		}
	}

}