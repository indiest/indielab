package me.sangtian.common 
{
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author 
	 */
	public class ProcedureProfiler 
	{
		private var _startTime:int;
		private var _totalTime:int;
		private var _times:uint;
		
		public function ProcedureProfiler()
		{
			
		}
		
		public function startTiming():int
		{
			_startTime = getTimer();
			return _startTime;
		}
		
		public function stopTiming():int
		{
			return getTimer() - _startTime;
		}
		
		public function pauseTiming():int
		{
			throw new Error("Unimplemented");
		}
		
		public function resumeTiming():int
		{
			throw new Error("Unimplemented");
		}
		
		public function countTimes(times:uint = 1):int
		{
			_times += times;
			var duration:int = stopTiming();
			_totalTime += duration;
			return duration;
		}
		
		public function getAverageMillisecond():Number
		{
			return _totalTime / _times;
		}
		
		public function resetTimes():void
		{
			_totalTime = 0;
			_times = 0;
		}
		
		public static const Default:ProcedureProfiler = new ProcedureProfiler();
	}

}