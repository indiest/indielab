package me.sangtian.common 
{
	import com.milkmangames.nativeextensions.android.AndroidIAB;
	import com.milkmangames.nativeextensions.android.events.AndroidBillingErrorEvent;
	import com.milkmangames.nativeextensions.android.events.AndroidBillingEvent;
	import flash.events.Event;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class IAPHelper 
	{
		private var _publicKey:String;
		private var _iab:AndroidIAB;
		private var _itemId:String;
		private var _onSuccess:Function;
		private var _onFail:Function;
		
		public function IAPHelper(publicKey:String) 
		{
			_publicKey = publicKey;
		}
		
		/**
		 * 
		 * @param	itemId
		 * @param	onSuccess 	function(itemId:String):void
		 * @param	onFail		function(itemId:String, msg:String):void
		 */
		public function purchase(itemId:String, onSuccess:Function = null, onFail:Function = null):void
		{
			_itemId = itemId;
			_onSuccess = onSuccess;
			_onFail = onFail;
			
			if (_iab == null)
				init();
			else
				doPurchase();
		}
		
		private function doPurchase():void 
		{
			_iab.purchaseItem(_itemId);
		}
		
		private function init():void 
		{
			_iab = AndroidIAB.create();
			_iab.addEventListener(AndroidBillingEvent.SERVICE_READY, onServiceReady);
			_iab.addEventListener(AndroidBillingEvent.SERVICE_NOT_SUPPORTED, handleEvent);
			_iab.addEventListener(AndroidBillingEvent.PURCHASE_SUCCEEDED, handleEvent);
			_iab.addEventListener(AndroidBillingEvent.PURCHASE_REFUNDED, handleEvent);
			_iab.addEventListener(AndroidBillingEvent.PURCHASE_CANCELLED, handleEvent);
			_iab.addEventListener(AndroidBillingEvent.PURCHASE_USER_CANCELLED, handleEvent);
			_iab.addEventListener(AndroidBillingErrorEvent.PURCHASE_FAILED, handleEvent);
			_iab.addEventListener(AndroidBillingEvent.TRANSACTIONS_RESTORED,handleEvent);
			_iab.addEventListener(AndroidBillingErrorEvent.TRANSACTION_RESTORE_FAILED, handleEvent);
			_iab.startBillingService(_publicKey);
		}
		
		private function handleEvent(e:Event):void 
		{
			switch (e.type)
			{
				case AndroidBillingEvent.PURCHASE_SUCCEEDED:
					if (_onSuccess != null)
						_onSuccess((e as AndroidBillingEvent).itemId);
					_itemId = null;
					break;
				case AndroidBillingEvent.SERVICE_NOT_SUPPORTED:
				case AndroidBillingEvent.PURCHASE_CANCELLED:
				case AndroidBillingErrorEvent.PURCHASE_FAILED:
					if (_onFail != null)
						_onFail(e["itemId"], e["text"]);
					_itemId = null;
					break;
				default:
					break;
			}
		}
		
		private function onServiceReady(e:AndroidBillingEvent):void 
		{
			if (_itemId != null)
				doPurchase();
		}
	}

}