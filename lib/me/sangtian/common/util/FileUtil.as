package me.sangtian.common.util 
{
	import com.adobe.crypto.MD5;
	import flash.display.BitmapData;
	import flash.display.PNGEncoderOptions;
	import me.sangtian.common.logging.Log;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	import me.sangtian.common.logging.Logger;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class FileUtil 
	{
		private static const logger:Logger = Logger.create(FileUtil);

		public static function getFilenameWithoutExtension(path:String):String
		{
			var arr:Array = path.split("/");
			var filename:String = arr[arr.length - 1];
			var index:int = filename.lastIndexOf(".");
			if (index == -1)
				return filename;
			else
				return filename.substring(0, index);
		}
		
		public static function getFilenameExtension(path:String):String
		{
			var arr:Array = path.split("/");
			var filename:String = arr[arr.length - 1];
			var index:int = filename.lastIndexOf(".");
			if (index == -1)
				return "";
			else
				return filename.substr(index);
		}
		
		public static function readFile(file:File):ByteArray
		{
			if (!file.exists)
				return null;
			var fs:FileStream = new FileStream();
			var bytes:ByteArray = new ByteArray();
			try
			{
				fs.open(file, FileMode.READ);
				fs.readBytes(bytes);
			}
			catch (err:Error)
			{
				CONFIG::debug
				{
					throw err;
				}
				CONFIG::release
				{
					logger.error(err);
				}
			}
			finally
			{
				fs.close();
			}
			return bytes;
		}
		
		public static function readTextFile(file:File):String
		{
			var bytes:ByteArray = readFile(file);
			if (bytes == null)
				return null;
			return bytes.readUTFBytes(bytes.bytesAvailable);
		}
				
		public static function readFileFromAppStorage(relativePath:String):ByteArray
		{
			var file:File = File.applicationStorageDirectory.resolvePath(relativePath);
			return readFile(file);
		}
		
		public static function readTextFileFromAppStorage(relativePath:String):String
		{
			var file:File = File.applicationStorageDirectory.resolvePath(relativePath);
			return readTextFile(file);
		}
		
		public static function readTextFileFromAppDir(relativePath:String):String
		{
			var file:File = File.applicationDirectory.resolvePath(relativePath);
			return readTextFile(file);
		}
		
		public static function writeTextFile(file:File, text:String):void
		{
			var bytes:ByteArray = new ByteArray();
			bytes.writeUTFBytes(text);
			writeFile(file, bytes);
		}
		
		public static function writeImageFile(file:File, bitmapData:BitmapData):void
		{
			var bytes:ByteArray = new ByteArray();
			bitmapData.encode(bitmapData.rect, new PNGEncoderOptions(), bytes);
			writeFile(file, bytes);
		}
		
		public static function writeFile(file:File, bytes:ByteArray):void
		{
			var fs:FileStream = new FileStream();
			logger.info("Writing to", file.nativePath);
			try
			{
				fs.open(file, FileMode.WRITE);
				fs.writeBytes(bytes);
			}
			catch (err:Error)
			{
				CONFIG::debug
				{
					throw err;
				}
				CONFIG::release
				{
					logger.error(err);
				}
			}
			finally
			{
				fs.close();
			}			
		}
		
		public static function writeFileToAppStorage(relativePath:String, bytes:ByteArray):void
		{
			var file:File = File.applicationStorageDirectory.resolvePath(relativePath);
			return writeFile(file, bytes);
		}
		
		public static function checksum(file:File):String 
		{
			var bytes:ByteArray = readFile(file);
			if (bytes == null)
				return null;
			return MD5.hashBinary(bytes);
		}
	}

}