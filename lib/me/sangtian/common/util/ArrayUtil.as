package me.sangtian.common.util 
{
	/**
	 * ...
	 * @author Nicolas
	 */
	public class ArrayUtil 
	{
		
		public static function shuffle(arr:Object, times:uint):Object
		{
			var i:uint = 0;
			var index1:uint;
			var index2:uint;
			var obj:Object;
			while (i < times)
			{
				i++;
				index1 = Math.random() * arr.length;
				index2 = Math.random() * arr.length;
				swap(arr, index1, index2);
			}
			return arr;
		}
		
		public static function swap(arr:Object, index1:uint, index2:uint):Object
		{
			var obj:* = arr[index1];
			arr[index1] = arr[index2];
			arr[index2] = obj;
			return arr;
		}
		
		public static function attributes(arr:Object, attributeName:String):Array 
		{
			var attrs:Array = [];
			for each(var obj:Object in arr)
			{
				attrs.push(obj[attributeName]);
			}
			return attrs;
		}
		
		public static function toDictionary(arr:Object, attributeName:String):Object
		{
			var dict:Object = { };
			for each(var obj:Object in arr)
			{
				dict[obj[attributeName]] = obj;
			}
			return dict;
		}
		
		public static function makeArray(item:*, count:uint):Array
		{
			var arr:Array = [];
			for (var i:uint = 0; i < count; i++)
				arr.push(item);
			return arr;
		}
		
		public static function resetValue(arr:Array, value:*):void
		{
			var len:uint = arr.length;
			for (var i:uint = 0; i < len; i++)
			{
				arr[i] = value;
			}
		}
		
		public static function fromIterable(iterable:*):Array
		{
			var arr:Array = [];
			for each(var item:* in iterable)
				arr.push(item);
			return arr;
		}
		
		public static function takeOne(arr:Object):*
		{
			var index:int = Random.rangeInt(0, arr.length);
			return arr.splice(index, 1)[0]
		}
		
		public static function getComparerByProperty(propName:String):Function
		{
			return function(obj1:Object, obj2:Object):int
			{
				return obj1[propName] - obj2[propName];
			};
		}
		
		public static function parseIntArray(str:String, delim=","):Array 
		{
			var strArr:Array = str.split(delim);
			var intArr:Array = [];
			for each(var s:String in strArr)
			{
				intArr.push(parseInt(s));
			}
			return intArr;
		}
		
		public static function resize2D(arr:*, oldW:uint, oldH:uint, newW:uint, newH:uint, defaultValue:* = null):Array 
		{
			var oldL:uint = arr.length;
			var r:Array = [];
			for (var y:uint = 0; y < newH; y++)
			{
				for (var x:uint = 0; x < newW; x++)
				{
					var newIndex:uint = x + y * newW;
					if (x >= oldW || y >= oldH)
					{
						r[newIndex] = defaultValue;
					}
					else
					{
						var oldIndex:uint = x + y * oldW;
						r[newIndex] = arr[oldIndex];
					}
				}
			}
			return r;
		}
		
		public static function isVector(obj:Object):Boolean 
		{
			return obj is Vector.<*>
				|| obj is Vector.<int>
				|| obj is Vector.<uint>
				|| obj is Vector.<Number>;
		}
	}

}