package me.sangtian.common.util 
{
	/**
	 * ...
	 * @author 
	 */
	public class ColorUtil 
	{
		[Inline]
		public static function getRed(color:uint):uint
		{
			return (color >> 16) & 0xff;
		}
		
		[Inline]
		public static function getGreen(color:uint):uint
		{
			return (color >> 8) & 0xff;
		}
		
		[Inline]
		public static function getBlue(color:uint):uint
		{
			return color & 0xff;
		}
		
		[Inline]
		public static function getAlpha(color:uint):uint
		{
			return (color >> 24) & 0xff;
		}
		
		[Inline]
        public static function rgb(red:uint, green:uint, blue:uint):uint
        {
            return (red << 16) | (green << 8) | blue;
        }
		
		[Inline]
        public static function rgba(red:uint, green:uint, blue:uint, alpha:uint):uint
        {
            return (alpha << 24) | (red << 16) | (green << 8) | blue;
        }
		
		[Inline]
		public static function setAlpha(color:uint, alpha:Number = 1.0):uint
		{
			return (uint(alpha * 255) << 24) | (color & 0xffffff);
		}
		
		[Inline]
		public static function fromString(text:String):uint 
		{
			return parseInt(text.replace("#", ""), 16);
		}
	}

}