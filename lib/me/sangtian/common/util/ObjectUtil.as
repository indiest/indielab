package me.sangtian.common.util 
{
	import flash.utils.ByteArray;
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author Nicolas
	 */
	public class ObjectUtil 
	{
		public static function clone(obj:*):*
		{
			var ba:ByteArray = new ByteArray();
			ba.writeObject(obj);
			ba.position = 0;
			return ba.readObject();
		}
		
		public static function merge(source:Object, properties:Object, target:Object = null):Object
		{
			target = target || { };
			var key:String;
			if (source != target)
			{
				for (key in source)
				{
					target[key] = source[key];
				}
			}
			for (key in properties)
			{
				target[key] = properties[key];
			}
			return target;
		}
		
		public static function applyProperties(target:Object, properties:Object, checkOwnProperty:Boolean = true):*
		{
			for (var propName:String in properties)
			{
				if (!checkOwnProperty || target.hasOwnProperty(propName))
					target[propName] = properties[propName];
			}
			return target;
		}
		
		public static function applyAttributes(target:Object, xmlElement:XML, checkOwnProperty:Boolean = true):void
		{
			for each(var attr:XML in xmlElement.attributes())
			{
				var attrName:String = attr.name();
				if (!checkOwnProperty || target.hasOwnProperty(attrName))
					target[attrName] = attr;
			}
		}
		
		public static function getPackageName(obj:*):String
		{
			var fullClassName:String = getQualifiedClassName(obj);
			return fullClassName.substr(0, fullClassName.indexOf('::'));
		}
		
		static public function getOwnPropertyNames(obj:Object):Vector.<String>
		{
			var propNames:Array = [];
			var typeInfo:XML = describeType(obj);
			//trace(typeInfo);
			for each(var v:* in typeInfo.variable)
			{
				//propNames.push(v.@name);
				propNames[v.metadata.arg.@value] = String(v.@name);
			}
			var propertyNames:Vector.<String> = new Vector.<String>();
			for (var i:int = 0; i < propNames.length; i++)
			{
				if (propNames[i])
				{
					propertyNames.push(propNames[i]);
				}
			}
			return propertyNames;
		}
		
		static public function isExtendedFrom(clazz:Class, baseClazz:Class):Boolean 
		{
			var typeInfo:XML = describeType(clazz);
			//trace(typeInfo);
			var baseClazzName:String = getQualifiedClassName(baseClazz);
			for each(var c:* in typeInfo.factory.extendsClass)
			{
				if (c.@type == baseClazzName)
					return true;
			}
			return false;
		}
		
		public static function getConstantNames(type:Class):Vector.<String>
		{
			var typeInfo:XML = describeType(type);
			var constantNames:Vector.<String> = new Vector.<String>();
			for each(var constant:* in typeInfo.constant)
			{
				constantNames.push(constant.@name);
			}
			return constantNames;
		}
		
		public static function constantsToObject(type:Class, reversed:Boolean = false):Object
		{
			var keys:Vector.<String> = getConstantNames(type);
			var obj:Object = { };
			for each(var key:String in keys)
			{
				if (reversed)
					obj[type[key]] = key;
				else
					obj[key] = type[key];
			}
			return obj;
		}
		
		public static function constantsToArray(type:Class, keyName:String = "name", valueName:String = "value"):Array
		{
			var keys:Vector.<String> = getConstantNames(type);
			var arr:Array = [];
			for each(var key:String in keys)
			{
				var obj:Object = { };
				obj[keyName] = key;
				obj[valueName] = type[key];
				arr.push(obj);
			}
			return arr;
		}
		
		static public function getClass(obj:Object):Class 
		{
			return getDefinitionByName(getQualifiedClassName(obj)) as Class;
		}
		
		/**
		*   Create a Function that, when called, instantiates a class
		*   @author Jackson Dunstan
		*   @param c Class to instantiate
		*   @return A function that, when called, instantiates a class with the
		*           arguments passed to said Function or null if the given class
		*           is null.
		*/
		public static function makeConstructorFunction(c:Class): Function
		{
			if (c == null)
			{
				return null;
			}
		 
			/**
			*   The function to call to instantiate the class
			*   @param args Arguments to pass to the constructor. There may be up to
			*               20 arguments.
			*   @return The instantiated instance of the class or null if an instance
			*          couldn't be instantiated. This happens if the given class or
			*          arguments are null, there are more than 20 arguments, or the
			*          constructor of the class throws an exception.
			*/
			return function(...args:Array): Object
			{
				switch (args.length)
				{
					case 0:
						return new c();
						break;
					case 1:
						return new c(args[0]);
						break;
					case 2:
						return new c(args[0], args[1]);
						break;
					case 3:
						return new c(args[0], args[1], args[2]);
						break;
					case 4:
						return new c(args[0], args[1], args[2], args[3]);
						break;
					case 5:
						return new c(args[0], args[1], args[2], args[3], args[4]);
						break;
					case 6:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5]);
						break;
					case 7:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
						break;
					case 8:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
						break;
					case 9:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
						break;
					case 10:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);
						break;
					case 11:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10]);
						break;
					case 12:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11]);
						break;
					case 13:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12]);
						break;
					case 14:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13]);
						break;
					case 15:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14]);
						break;
					case 16:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15]);
						break;
					case 17:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16]);
						break;
					case 18:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17]);
						break;
					case 19:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18]);
						break;
					case 20:
						return new c(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19]);
						break;
					default:
						return null;
				}
				return null;
			}
		}
	}
}