package me.sangtian.common.util 
{
	import com.greensock.TweenLite;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class TweenUtil 
	{
		
		public function TweenUtil() 
		{
			
		}
		
		public static function killTweensOf(target:Object):void
		{
			// Force all tweens to the end to REALLY get them kill-ready.
			for each(var tl:TweenLite in TweenLite.getTweensOf(target))
			{
				tl.seek(tl.duration());
			}
			TweenLite.killTweensOf(target);
		}
	}

}