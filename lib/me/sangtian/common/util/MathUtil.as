package me.sangtian.common.util 
{
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author Nicolas
	 */
	public class MathUtil 
	{
		public static const EMPTY_POINT:Point = new Point();
		public static const HALF_PI:Number = Math.PI * 0.5;
		public static const TWO_PI:Number = Math.PI * 2;
		public static const ANGLE_RADIAN:Number = Math.PI / 180;
		public static const RADIAN_ANGLE:Number = 180 / Math.PI;
		public static const ONE_OF_255:Number = 1 / 255;
		
		[Inline]
		public static function angleToRadian(a:Number):Number
		{
			return a * ANGLE_RADIAN;
		}
		
		[Inline]
		public static function radianToAngle(r:Number):Number
		{
			return (r % TWO_PI) * RADIAN_ANGLE;
		}
		
		[Inline]
		public static function cosAngle(a:Number):Number
		{
			return Math.cos(a * ANGLE_RADIAN);
		}
		
		[Inline]
		public static function sinAngle(a:Number):Number
		{
			return Math.sin(a * ANGLE_RADIAN);
		}

		[Inline]
		public static function hexToNumber(hex:uint):Number
		{
			return hex * ONE_OF_255;
		}
		
		[Inline]
		public static function sign(num:Number):int
		{
			if (num > 0)
				return 1;
			else if (num < 0)
				return -1;
			else
				return 0;
		}
		
		[Inline]
		public static function angle(dx:Number, dy:Number):Number
		{
			return RADIAN_ANGLE * Math.atan2(dy, dx);
		}
		
		[Inline]
		public static function dist(x1:Number, y1:Number, x2:Number, y2:Number):Number
		{
			var dx:Number = x1 - x2;
			var dy:Number = y1 - y2;
			return Math.sqrt(dx * dx + dy * dy);
		}
		
		[Inline]
		public static function compareDistance(dx:Number, dy:Number, dist:Number):int
		{
			return dx * dx + dy * dy - dist * dist;
		}
		
		[Inline]
		public static function dragNumber(num:Number, drag:Number):Number
		{
			if (num > 0)
			{
				num -= drag;
				if (num < 0)
					num = 0;
			}
			else if (num < 0)
			{
				num += drag;
				if (num > 0)
					num = 0;
			}
			return num;
		}
		
		[Inline]
		public static function fastInterpolateVector3D(v1:Vector3D, v2:Vector3D, f:Number, out:Vector3D):void
		{
			out.x = v2.x + (v1.x - v2.x) * f;
			out.y = v2.y + (v1.y - v2.y) * f;
			out.z = v2.z + (v1.z - v2.z) * f;
		}
		
		[Inline]
		public static function interpolateVector3D(v1:Vector3D, v2:Vector3D, f:Number):Vector3D
		{
			var dv:Vector3D = v1.subtract(v2);
			dv.scaleBy(f);
			dv.incrementBy(v2);
			return dv;
		}
		
		public static function matrixToString(matrix:Matrix3D):String 
		{
			var rawData:Vector.<Number> = matrix.rawData;
			var str:String = "";
			for (var y:int = 0; y < 4; y++)
			{
				str += "|";
				for (var x:int = 0; x < 4; x++)
				{
					str += rawData[x + y * 4].toFixed(2);
					str += "\t";
				}
				str += "|\n"
			}
			return str;
		}
		
		[Inline]
		public static function geometricMean(v:Vector3D):Number 
		{
			return Math.pow(v.x * v.y * v.z, 1 / 3);
		}
		
		[Inline]
		public static function scalePoint(p:Point, scale:Number):Point
		{
			return new Point(p.x * scale, p.y * scale);
		}
		
		[Inline]
		public static function scaleVector(v:Vector3D, scale:Vector3D):Vector3D
		{
			return new Vector3D(v.x * scale.x, v.y * scale.y, v.z * scale.z);
		}
		
		[Inline]
		public static function clamp(num:Number, min:Number, max:Number):Number
		{
			if (num < min)
				num = min;
			else if (num > max)
				num = max;
			return num;
		}
		
		[Inline]
		public static function wrap(num:Number, min:Number, max:Number):Number
		{
			if (num < min)
				num = num + max - min;
			else if (num > max)
				num = num + min - max;
			return num;
		}
		
		[Inline]
		public static function wrapInt(num:int, min:int, max:int):int
		{
			if (num < min)
				num = max;
			else if (num > max)
				num = min;
			return num;
		}
		
		[Inline]
		public static function interpolate(min:Number, max:Number, f:Number):Number
		{
			return min + (max - min) * f;
		}
		
		[Inline]
		public static function interpolateColor(c1:uint, c2:uint, f:Number):uint
		{
			var r:uint = interpolate(ColorUtil.getRed(c1), ColorUtil.getRed(c2), f);
			var g:uint = interpolate(ColorUtil.getGreen(c1), ColorUtil.getGreen(c2), f);
			var b:uint = interpolate(ColorUtil.getBlue(c1), ColorUtil.getBlue(c2), f);
			var a:uint = interpolate(ColorUtil.getAlpha(c1), ColorUtil.getAlpha(c2), f);
			return ColorUtil.rgba(r, g, b, a);
		}

	}

}