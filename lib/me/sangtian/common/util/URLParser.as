package me.sangtian.common.util
{

	public class URLParser
	{
		public var host:String;
		public var port:String;
		public var protocol:String;
		public var path:String;
		public var parameters:Object;	

		public function parse( url:String ):void
		{
			var reg:RegExp = /(?P<protocol>[a-zA-Z]+) : \/\/  (?P<host>[^:\/]*) (:(?P<port>\d+))?  ((?P<path>[^?]*))? ((?P<parameters>.*))? /x;
			var results:Object = reg.exec(url);

			protocol = results.protocol;
			host = results.host;
			port = results.port;
			path = results.path;
			var paramsStr:String = results.parameters;
			parameters = {};
			if (paramsStr.length > 0)
			{
				if(paramsStr.charAt(0) == "?")
				{
					paramsStr = paramsStr.substring(1);
				}
				var params:Array = paramsStr.split("&");
				for each(var paramStr:String in params)
				{
					var param:Array = paramStr.split("=");
					parameters[param[0]] = param[1];
				}
			}
		}
	}
}
