package me.sangtian.common.util 
{
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.logging.Log;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	import me.sangtian.common.logging.Logger;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class DisplayObjectUtil 
	{
		private static const logger:Logger = Logger.create(DisplayObjectUtil);
		
		public static const GREYSCALE_FILTERS:Array = [
			new ColorMatrixFilter([
				0.33, 0.33, 0.33, 0, 0,
				0.33, 0.33, 0.33, 0, 0,
				0.33, 0.33, 0.33, 0, 0,
				0, 0, 0, 1, 0])
		];
		
		public static function getTintColorMatrix(color:uint):Array
		{
			return [
				0, 0, 0, 0, (color >> 16) & 0xff,
				0, 0, 0, 0, (color >> 8 ) & 0xff,
				0, 0, 0, 0,  color        & 0xff,
				0, 0, 0, 1, 0
			];
		}
		
		public static function tint(obj:DisplayObject, multiplier:Number = 1, color:uint = 0):void
		{
			var ct:ColorTransform = obj.transform.colorTransform;
			ct.color = color;
			ct.redMultiplier = ct.greenMultiplier = ct.blueMultiplier = multiplier;
			obj.transform.colorTransform = ct;
		}
		
		public static function setBrightness(obj:DisplayObject, percentage:Number = 0):void
		{
			var offset:uint = percentage > 0 ? 0xff * percentage : 0;
			tint(obj, 1 - Math.abs(percentage), ColorUtil.rgb(offset, offset, offset));
		}
		
		public static function setContrast(obj:DisplayObject, percentage:Number = 0):void
		{
			var offset:uint = 0x88 * (1 - percentage);
			tint(obj, percentage, ColorUtil.rgb(offset, offset, offset));
		}
		
		public static function replace(obj1:DisplayObject, obj2:DisplayObject):void 
		{
			var container:DisplayObjectContainer = obj1.parent;
			var index:int = container.getChildIndex(obj1);
			container.addChildAt(obj2, index);
			obj2.transform.matrix = obj1.transform.matrix;
			obj2.name = obj1.name;
			container.removeChild(obj1);
		}
		
		public static function pivot(obj:DisplayObject, xScale:Number = 0.5, yScale:Number = 0.5):DisplayObject
		{
			obj.x = -obj.width * xScale;
			obj.y = -obj.height * yScale;
			return obj;
		}
		
		public static function center(obj:DisplayObject, container:DisplayObjectContainer = null):void
		{
			centerX(obj, container);
			centerY(obj, container);
		}
		
		public static function centerX(obj:DisplayObject, container:DisplayObjectContainer = null):void
		{
			obj.x = ((container ? container.width : obj.stage.stageWidth) - obj.width) * 0.5;
		}
		
		public static function centerY(obj:DisplayObject, container:DisplayObjectContainer = null):void
		{
			obj.y = ((container ? container.height : obj.stage.stageHeight) - obj.height) * 0.5;
		}		
		
		public static function fixSize(obj:DisplayObject, width:Number, height:Number):void 
		{
			var widthRatio:Number = width / obj.width;
			var heightRatio:Number = height / obj.height;
			var scale:Number = Math.min(widthRatio, heightRatio, 1);
			obj.scaleX = obj.scaleY = scale;
		}
		
		public static function removeFromParent(child:DisplayObject):void 
		{
			if (child != null && child.parent != null)
				child.parent.removeChild(child);
		}
		
		public static function clearContainer(container:DisplayObjectContainer):void 
		{
			while (container.numChildren > 0)
				container.removeChildAt(0);
		}
		
		public static function clearBitmap(bitmap:Bitmap, color:uint = 0):void
		{
			bitmap.bitmapData.fillRect(bitmap.bitmapData.rect, color);
		}
		
		/**
		 * Iterate all sub nodes of the specified display object to the leaf nodes.
		 * @param	obj root container
		 * @param	callback [param1:DisplayObject(child object)]. The root node will be called at first time.
		 */
		public static function iterateAllChildren(obj:DisplayObject, callback:Function = null):void
		{
			if (callback != null)
			{
				if (callback.length == 0)
					callback();
				else if (callback.length == 1)
					callback(obj);
			}
			if (obj is DisplayObjectContainer)
			{
				var container:DisplayObjectContainer = obj as DisplayObjectContainer;
				for (var i:uint = 0; i < container.numChildren; i++)
				{
					iterateAllChildren(container.getChildAt(i), callback);
				}
			}
		}
		
		private static function handleStopMovieClip(obj:DisplayObject):void 
		{
			if (obj is MovieClip)
				MovieClip(obj).stop();
			else if (obj is SimpleButton)
			{
				var button:SimpleButton = obj as SimpleButton;
				stopAllMovieClips(button.upState);
				stopAllMovieClips(button.downState);
				stopAllMovieClips(button.overState);
			}
		}
		
		/**
		 * Stops all MovieClips in sub nodes of the specified display object, including all the states of SimpleButton.
		 */
		public static function stopAllMovieClips(obj:DisplayObject):void
		{
			iterateAllChildren(obj, handleStopMovieClip);
		}
		
		
		
		
		private static var _coverCache:Dictionary = new Dictionary(true);
		
		public static function makeCover(container:DisplayObjectContainer, bounds:Rectangle = null):Bitmap
		{
			bounds = bounds || container.getBounds(container);
			if (bounds.width == 0 || bounds.height == 0)
			{
				logger.warn("Attempt to create zero-size bitmap cover.");
				return null;
			}
			var bitmap:Bitmap = _coverCache[container];
			if (bitmap == null || bitmap.width < bounds.width || bitmap.height < bounds.height)
			{
				bitmap = new Bitmap(new BitmapData(bounds.width, bounds.height, true, 0));
				_coverCache[container] = bitmap;
			}
			else
			{
				clearBitmap(bitmap);
			}
			var time:int = getTimer();
			var matrix:Matrix = new Matrix();
			matrix.translate(-bounds.x, -bounds.y);
			bitmap.x = bounds.x;
			bitmap.y = bounds.y;
			bitmap.bitmapData.draw(container, matrix);
			//bitmap.bitmapData = drawToBitmapData(container);
			logger.debug("Rendered cover bitmap in ms:", getTimer() - time, "bounds:", bounds);
			container.addChild(bitmap);
			for (var i:uint = 0; i < container.numChildren - 1; i++)
				container.getChildAt(i).visible = false;
			return bitmap;
		}
		
		public static function removeCover(container:DisplayObjectContainer, dispose:Boolean = false):Bitmap
		{
			var bitmap:Bitmap = _coverCache[container];
			if (!bitmap)
				return null;
			for (var i:uint = 0; i < container.numChildren; i++)
				container.getChildAt(i).visible = true;
			removeFromParent(bitmap);
			if (dispose)
			{
				delete _coverCache[container];
				bitmap.bitmapData.dispose();
			}
			return bitmap;
		}
		
		
		
		/* load/play SWF utility section */
		// Use AssetsCache
		//private static var _movieCache:Object = { };
		private static var _movieMask:Sprite;
		
		private static function initMask():void
		{
			_movieMask = new Sprite();
			_movieMask.graphics.beginFill(0, 0);
			_movieMask.graphics.drawRect(0, 0, 1, 1);
			_movieMask.graphics.endFill();
			_movieMask.mouseChildren = false;
		}
		
		public static function playMovie(url:String, container:DisplayObjectContainer, cache:Boolean = false, movieEndCallback:Function = null, movieLoadedCallback:Function = null, movieExitFrameCallback:Function = null):void 
		{
			if (AssetsCache.getAsset(url))
			{
				playMovieClip(AssetsCache.getAsset(url), container, movieEndCallback, movieExitFrameCallback);
			}
			else
			{
				var loader:MovieClipLoader = new MovieClipLoader();
				loader.movieContainer = container;
				loader.cacheKey = cache ? url : null;
				loader.movieEndCallback = movieEndCallback;
				loader.movieLoadedCallback = movieLoadedCallback;
				loader.movieExitFrameCallback = movieExitFrameCallback;
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleMovieLoaded);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleMovieLoadError);
				loader.load(new URLRequest(url));
			}
		}
		
		static private function handleMovieLoadError(e:IOErrorEvent):void 
		{
			var loaderInfo:LoaderInfo = e.currentTarget as LoaderInfo;
			loaderInfo.removeEventListener(Event.COMPLETE, handleMovieLoaded);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleMovieLoadError);
			logger.error("Unable to load and play MovieClip:", loaderInfo.url);
			var loader:MovieClipLoader = loaderInfo.loader as MovieClipLoader;
			loader.cacheKey = null;
			if (loader.movieEndCallback != null)
			{
				loader.movieEndCallback();
				loader.movieEndCallback = null;
			}
		}
		
		static private function handleMovieLoaded(e:Event):void 
		{
			var loaderInfo:LoaderInfo = e.currentTarget as LoaderInfo;
			loaderInfo.removeEventListener(Event.COMPLETE, handleMovieLoaded);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleMovieLoadError);
			var loader:MovieClipLoader = loaderInfo.loader as MovieClipLoader;
			var mc:MovieClip = loaderInfo.content as MovieClip;
			if (loader.cacheKey)
				AssetsCache.cache(loader.cacheKey, mc);
			if (loader.movieLoadedCallback != null)
				loader.movieLoadedCallback(mc);
			playMovieClip(mc, loader.movieContainer, loader.movieEndCallback, loader.movieExitFrameCallback);
			loader.unload();
			loader.movieContainer = null;
			loader.cacheKey = null;
			loader.movieEndCallback = null;
		}
		
		public static function playOnce(mc:MovieClip):void
		{
			mc.addEventListener(Event.EXIT_FRAME, stopLastFrame);
			mc.gotoAndPlay(1);
		}
		
		static private function stopLastFrame(e:Event):void 
		{
			var mc:MovieClip = e.currentTarget as MovieClip;
			if (mc.currentFrame < mc.totalFrames)
				return;	
			
			mc.removeEventListener(Event.EXIT_FRAME, stopLastFrame);	
			mc.gotoAndStop(mc.totalFrames);
		}		
		
		
		public static function playMovieClip(mc:MovieClip, container:DisplayObjectContainer, /*cover:Boolean = true,*/ movieEndCallback:Function = null, movieExitFrameCallback:Function = null):void
		{
			//if (cover)
				//makeCover(container);
				
			if (!container.contains(mc))
				container.addChild(mc);
			
			mc.movieEndCallback = movieEndCallback;
			mc.movieExitFrameCallback = movieExitFrameCallback;
			mc.addEventListener(Event.EXIT_FRAME, handleMovieFrame);
			mc.gotoAndPlay(1);
			
			if (mc.totalFrames == 1 && mc.numChildren == 1)
			{
				var childMc:MovieClip = mc.getChildAt(0) as MovieClip;
				if (childMc != null)
				{
					childMc.gotoAndPlay(1);
				}
			}
		}
		
		static private function handleMovieFrame(e:Event):void 
		{
			var mc:MovieClip = e.currentTarget as MovieClip;
			CONFIG::debug
			{
				//logger.debug("Playing MovieClip", mc.name, "frame:", mc.currentFrame, "/", mc.totalFrames, "children:", mc.numChildren);
			}
			if (mc.movieExitFrameCallback != null)
			{
				mc.movieExitFrameCallback(mc);
			}
			
			// If the MovieClip is no more on the stage, remove it.
			if (mc.stage == null)
			{
				removeMovieClip(mc);
				return;
			}
			
			// If the MovieClip has only 1 frame and 1 child, use the child MC's frame num.
			if (mc.totalFrames == 1 && mc.numChildren == 1)
			{
				var childMc:MovieClip = mc.getChildAt(0) as MovieClip;
				if (childMc != null)
				{
					if (childMc.currentFrame < childMc.totalFrames)
						return;
				}
			}
			/*
			*/
			if (mc.currentFrame < mc.totalFrames)
				return;
				
			if (mc.movieEndCallback != null)
			{
				mc.movieEndCallback();
			}
			removeMovieClip(mc);
		}		
		
		public static function removeMovieClip(mc:MovieClip):void
		{
			if (mc == null)
				return;
			mc.removeEventListener(Event.EXIT_FRAME, handleMovieFrame);
			delete mc["movieEndCallback"];
			delete mc["movieExitFrameCallback"];
			//removeCover(mc.parent);
			removeFromParent(mc);
			mc.gotoAndStop(1);
			if (mc.totalFrames == 1 && mc.numChildren == 1)
			{
				var childMc:MovieClip = mc.getChildAt(0) as MovieClip;
				if (childMc != null)
				{
					childMc.gotoAndStop(1);
				}
			}
		}
		
		private static var _hitTestBitmapData:BitmapData = new BitmapData(1, 1, true, 0x00000000);
		private static var _hitTestMatrix:Matrix = new Matrix();
		private static var _hitTestPoint:Point = new Point(0,0);
		public static function hitTest(object:DisplayObject, localX:Number, localY:Number, alphaThreshold:uint = 128):Boolean 
		{  
			_hitTestMatrix.tx = -localX;  
			_hitTestMatrix.ty = -localY;  
			_hitTestBitmapData.draw(object, _hitTestMatrix);   
			return _hitTestBitmapData.hitTest(_hitTestPoint, alphaThreshold, _hitTestPoint);
		} 
		
		public static function drawToBitmapData(obj:DisplayObject, matrix:Matrix = null):BitmapData
		{
			var bounds:Rectangle = obj.getBounds(obj);
			if (matrix == null)
			{
				matrix = new Matrix();
				matrix.tx = -bounds.x;
				matrix.ty = -bounds.y;
			}
			var bd:BitmapData = new BitmapData(bounds.width + 0.5, bounds.height + 0.5, true, 0);
			bd.draw(obj, matrix);
			return bd;
		}
		
		public static function drawRotations(obj:DisplayObject, rotations:uint = 4, result:Array = null):Array
		{
			result ||= [];
			var matrix:Matrix = new Matrix();
			for (var i:int = 0; i < rotations; i++)
			{
				obj.rotation = i / rotations * 360;
				var bounds:Rectangle = obj.getBounds(obj);
				matrix = obj.transform.matrix.clone();
				matrix.translate( -bounds.x, -bounds.y);
				var bitmapData:BitmapData = drawToBitmapData(obj, matrix);
				
				//matrix.identity();
				//matrix.rotate(i / rotations * Math.PI * 2);
				//var bitmapData:BitmapData = result[i] || new BitmapData(bounds.width + 0.5, bounds.height + 0.5, true, 0);
				//bitmapData.draw(obj, matrix);
				result[i] = bitmapData;
			}
			return result;
		}
		
		private static var _originalTexts:Dictionary = new Dictionary(true);
		/**
		 * Format TextField.htmlText using printf function.
		 * 
		 * @param	textField TextField with htmlText preformat (usually set in Flash CS). The original htmlText will be cached so every time calling printf it will be retrieved.
		 * @param	...rest
		 * @see printf
		 */
		static public function formatText(textField:TextField, ...rest):void 
		{
			var originalText:String = _originalTexts[textField];
			if (originalText == null)
			{
				originalText = _originalTexts[textField] = textField.htmlText;
			}
			rest.unshift(originalText);
			textField.htmlText = printf.apply(null, rest);
		}
		
		public static function autoSizeText(tf:TextField, text:String):void 
		{
			var width:Number = tf.width;
			var format:TextFormat = tf.getTextFormat();
			tf.text = text;
			while (tf.textWidth > width)
			{
				format.size = int(format.size) - 1;
				tf.setTextFormat(format);
			}
		}
		
		public static function getItemMaxNum(obj:Object, prefix:String = "item", startNum:int = 0):int
		{
			var num:int = startNum;
			while (obj.hasOwnProperty(prefix + num))
			{
				num++;
			}
			return num;
		}
		
		public static function showItems(obj:Object, num:uint, prefix:String = "item"):void
		{
			var maxNum:uint = getItemMaxNum(obj, prefix);
			for (var i:uint = 0; i < maxNum; i++)
			{
				obj[prefix + i].visible = (i <= num);
			}
		}

		private static var _mouseButtonHoldListeners:Dictionary = new Dictionary(true);
		
		public static function addMouseButtonHoldListener(obj:InteractiveObject, listener:Function):void
		{
			_mouseButtonHoldListeners[obj] = { listener: listener, event: null };
			obj.addEventListener(Event.ENTER_FRAME, handleEnterFrame);
			obj.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			obj.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			obj.addEventListener(MouseEvent.MOUSE_OUT, handleMouseUp);
			obj.addEventListener(MouseEvent.ROLL_OUT, handleMouseUp);
		}
		
		public static function removeMouseButtonHoldListener(obj:InteractiveObject, listener:Function):void
		{
			obj.removeEventListener(Event.ENTER_FRAME, handleEnterFrame);
			obj.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			obj.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			obj.removeEventListener(MouseEvent.MOUSE_OUT, handleMouseUp);
			obj.removeEventListener(MouseEvent.ROLL_OUT, handleMouseUp);
			delete _mouseButtonHoldListeners[obj];
		}
		
		static private function handleEnterFrame(e:Event):void 
		{
			var obj:InteractiveObject = e.currentTarget as InteractiveObject;
			var ctx:Object = _mouseButtonHoldListeners[obj];
			if (ctx.event)
				ctx.listener(ctx.event);
		}
		
		static private function handleMouseDown(e:MouseEvent):void 
		{
			var obj:InteractiveObject = e.currentTarget as InteractiveObject;
			var ctx:Object = _mouseButtonHoldListeners[obj];
			ctx.event = e;
		}
		
		static private function handleMouseUp(e:MouseEvent):void 
		{
			var obj:InteractiveObject = e.currentTarget as InteractiveObject;
			var ctx:Object = _mouseButtonHoldListeners[obj];
			ctx.event = null;
		}
	}

}

import flash.display.DisplayObjectContainer;
import flash.display.Loader;

class MovieClipLoader extends Loader
{
	public var movieContainer:DisplayObjectContainer;
	public var cacheKey:String;
	public var movieEndCallback:Function;
	public var movieLoadedCallback:Function;
	public var movieExitFrameCallback:Function;
}
		/* end of load/play SWF utility section */
