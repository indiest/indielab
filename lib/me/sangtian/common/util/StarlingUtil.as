package me.sangtian.common.util 
{
	import flash.display.SimpleButton;
	import flash.geom.Rectangle;
	import flash.text.TextFieldAutoSize;
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author 
	 */
	public class StarlingUtil 
	{
		
		public static function generateFromFlashUI(ui:flash.display.MovieClip, uiController:Object = null):Sprite
		{
			var starlingUI:Sprite = new Sprite();
			for (var i:int = 0; i < ui.numChildren; i++)
			{
				var child:flash.display.DisplayObject = ui.getChildAt(i);
				var starlingChild:DisplayObject;
				if (child is SimpleButton)
				{
					var sb:SimpleButton = child as SimpleButton;
					var button:Button = new Button(drawToTexture(sb.upState));
					if (sb.downState != null && sb.downState != sb.upState)
						button.downState = drawToTexture(sb.downState);
					starlingChild = button;
				}
				else if (child is flash.text.TextField)
				{
					var tf:flash.text.TextField = child as flash.text.TextField;
					var stf:TextField = new TextField(tf.width, tf.height, tf.text);
					stf.autoScale = (tf.autoSize != TextFieldAutoSize.NONE);
					stf.bold = tf.defaultTextFormat.bold;
					stf.border = tf.border;
					stf.color = tf.textColor;
					stf.fontName = tf.defaultTextFormat.font;
					stf.fontSize = Number(tf.defaultTextFormat.size);
					stf.hAlign = tf.defaultTextFormat.align;
					stf.italic = tf.defaultTextFormat.italic;
					stf.kerning = tf.defaultTextFormat.kerning;
					stf.nativeFilters = tf.filters;
					stf.underline = tf.defaultTextFormat.underline;
					starlingChild = stf;
				}
				else if (child is flash.display.Shape)
				{
					starlingChild = new Image(drawToTexture(child));
				}
				else if (child is flash.display.MovieClip)
				{
					if (child.name.indexOf("instance") == 0)
					{
						var image:Image = new Image(drawToTexture(child));
						var bounds:Rectangle = child.getBounds(child);
						image.x = bounds.x;
						image.y = bounds.y;
						var sprite:Sprite = new Sprite();
						sprite.addChild(image);
						starlingChild = sprite;
					}
					else
					{
						starlingChild = generateFromFlashUI(child as flash.display.MovieClip);
					}
				}
				else
				{
					throw new Error("Unsupport type:" + typeof(child));
				}
				
				if (starlingChild != null)
				{
					starlingChild.name = child.name;
					starlingChild.transformationMatrix = child.transform.matrix;
					starlingChild.alpha = child.alpha;
					starlingChild.blendMode = child.blendMode;
					starlingChild.visible = child.visible;
					starlingUI.addChild(starlingChild);
				}
				if (uiController != null && uiController.hasOwnProperty(starlingChild.name))
				{
					uiController[starlingChild.name] = starlingChild;
				}
			}
			return starlingUI;
		}
		
		public static function drawToTexture(obj:flash.display.DisplayObject):Texture
		{
			return Texture.fromBitmapData(DisplayObjectUtil.drawToBitmapData(obj), false);
		}
	}

}