package me.sangtian.common.util 
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class StringUtil 
	{
		private static const FILENAME_REGEX:RegExp = new RegExp(
			/^(.*\/)(.*)\.(.*)$/);
		
		public static function parseFileName(str:String):Array
		{
			return str.toLowerCase().match(FILENAME_REGEX);
		}
		
		public static function strEndWith(str:String, compare:String):Boolean
		{
			return str.lastIndexOf(compare) == (str.length - compare.length);
		}
		
		public static function formatTime(ms:uint):String
		{
			var s:uint = ms / 1000;
			var hours:uint = s / 3600;
			var minutes:uint = (s - hours * 3600) / 60;
			var seconds:uint = s - hours * 3600 - minutes * 60;
			var str:String = hours > 0 ? hours + ":" : "";
			return str + getFixedString(String(minutes), 2, "", "0", "") + ":" + getFixedString(String(seconds), 2, "", "0", "");
		}
		
		public static function getLogFormatDateString(date:Date):String
		{
			return date.fullYear +
				getFixedString(String(date.month + 1), 2, null, "0", "") +
				getFixedString(date.date.toString(), 2, null, "0", "");
		}
		
		public static function getLogFormatTimeString(date:Date):String
		{
			return getLogFormatDateString(date) +
				getFixedString(date.hours.toString(), 2, null, "0", "") +
				getFixedString(date.minutes.toString(), 2, null, "0", "") +
				getFixedString(date.seconds.toString(), 2, null, "0", "") +
				getFixedString(date.milliseconds.toString(), 4, null, "0", "");
		}
		
		public static function getFixedString(str:String, length:uint, ellipsis:String = "...", leftPadding:String = "", rightPadding:String = " "):String
		{
			if (str.length > length)
			{
				return str.substr(0, length - ellipsis.length) + ellipsis;
			}
			else if (leftPadding.length > 0 || rightPadding.length > 0)
			{
				while (str.length < length)
					str = leftPadding + str + rightPadding;
			}
			return str;
		}
		
		public static function getNumber(str:String, start:int = 0, end:int = 2147483647):int
		{
			//var result:String = "";
			var step:int = MathUtil.sign(end - start);
			for (var i:uint = start; i != end; i += step)
			{
				var c:Number = str.charCodeAt(i);
				if (c >= 0x30 && c <= 0x39)//0~9
					//result = str.charAt(i) + result;
					continue;
				else
					break;
			}
			return int(str.substring(start, i));
			//return int(result);
		}
		
		public static function getPostfixNumber(str:String):int 
		{
			var result:String = "";
			for (var i:uint = str.length - 1; i >= 0; i--)
			{
				var c:Number = str.charCodeAt(i);
				if (c >= 0x30 && c <= 0x39)//0~9
					result = str.charAt(i) + result;
				else
					break;
			}
			return int(result);
		}
		
		public static function replaceChar(str:String, index:uint, char:String):String 
		{
			return str.substr(0, index) + char + str.substr(index + 1);
		}
		
		public static function getRandomNumberArray(min:Number, max:Number, count:uint, radix:uint = 10, sep:* = null):String 
		{
			var arr:Array = [];
			for (var i:uint = 0; i < count; i++)
				arr.push((Math.random() * (max - min) + min).toString(radix));
			return arr.join(sep);
		}
		
		public static function isNullOrEmpty(str:String):Boolean 
		{
			return str == null || str.length == 0;
		}
		
		public static function trim(str:String):String 
		{
			return str.replace(/^\s+|\s+$/g, '');
		}
		
		public static function toBoolean(str:String):Boolean
		{
			return str == "true" ? true : false;
		}
	}

}