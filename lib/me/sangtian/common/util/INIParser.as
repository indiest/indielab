package me.sangtian.common.util 
{
	public class INIParser 
	{
		private static const GLOBAL_SECTION:String = 'global';
		private static const SECTION_START:String = '[';
		private static const SECTION_END:String = ']';
		private static const NESTED_SECTION:String = '.';
		
		private static const COMMENT:String = ';';
		private static const COMMENT_2:String = '#';
		private static const EMPTY:String = '';

		private static const NEW_LINE:RegExp = /\r?\n/;
		private static const KEY_SEPARATOR:RegExp = / ?= ?/;
		
		private static const DECIMAL:RegExp = /^-?\d+(\.\d+)?$/;
		private static const HEXADECIMAL:RegExp = /^0x[0-9a-f]+$/i;
		
		private static const ESCAPED_NEW_LINE:RegExp = /\\n/g;
		
		public static function parse(code:String):Object
		{
			var data:Object = {};
			var holder:Object = data;

			for each (var line:String in code.split(NEW_LINE))
			{
				switch (line.charAt(0))
				{
					case EMPTY:
					case COMMENT:
					case COMMENT_2: break;
					case SECTION_START:
						var section:String = line.slice(1, line.indexOf(SECTION_END));
						holder = data;

						if (section !== GLOBAL_SECTION)
							for each (var part:String in section.split(NESTED_SECTION))
								holder = holder[part] || (holder[part] = {});
					break;
					default:
						var i:int = line.search(KEY_SEPARATOR);
						holder[line.slice(0, i)] = parseToken(line.slice(i+1));
					break;
				}
			}
			return data;
		}
		
		private static function parseToken(token:String):*
		{
			token = StringUtil.trim(token);
			
			switch (token)
			{
				case 'true': return true;
				case 'false': return false;
				case 'undefined':
				case 'null': return null;
			}
			
			if (DECIMAL.test(token))
				return parseFloat(token);

			if (HEXADECIMAL.test(token))
				return parseInt(token,16);
			
			return token.replace(ESCAPED_NEW_LINE, '\n');
		}
	}

}