package me.sangtian.common.logging 
{
	import avmplus.getQualifiedClassName;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Logger 
	{
		private var _name:String;
		public var thresholdLevel:uint;
		public var format:String;
		
		public function get name():String 
		{
			return _name;
		}
		
		public function get isDebugEnabled():Boolean
		{
			return thresholdLevel <= Log.LEVEL_DEBUG;
		}
		
		public function get isInfoEnabled():Boolean
		{
			return thresholdLevel <= Log.LEVEL_INFO;
		}
		
		public function get isWarnEnabled():Boolean
		{
			return thresholdLevel <= Log.LEVEL_WARN;
		}
		
		public function get isErrorEnabled():Boolean
		{
			return thresholdLevel <= Log.LEVEL_ERROR;
		}

		public function Logger(name:String) 
		{
			_name = name;
			Log.initLogger(this);
		}
		
		public function write(text:String, level:uint):void
		{
			if (level >= thresholdLevel)
				Log.write(text, level, this);
		}
		
		public function debug(...text):void
		{
			CONFIG::debug
			{
				if (isDebugEnabled)
					write(text.join(" "), Log.LEVEL_DEBUG);
			}
		}
		
		public function info(...text):void
		{
			if (isInfoEnabled)
				write(text.join(" "), Log.LEVEL_INFO);
		}
		
		public function warn(...text):void
		{
			if (isWarnEnabled)
				write(text.join(" "), Log.LEVEL_WARN);
		}
		
		public function error(...text):void
		{
			if (isErrorEnabled)
				write(text.join(" "), Log.LEVEL_ERROR);
		}
		
		public static function create(clazz:Class):Logger 
		{
			return new Logger(getQualifiedClassName(clazz));
		}
		
	}

}