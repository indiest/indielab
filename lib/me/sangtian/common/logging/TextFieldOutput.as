package me.sangtian.common.logging 
{
	import flash.text.TextField;
	/**
	 * ...
	 * @author 
	 */
	public class TextFieldOutput extends LogOutput 
	{
		public static var levelColors:Array = [
			"#cccccc",
			"#ffffff",
			"#ffff00",
			"#ff0000"
		];
		
		private var _tf:TextField;
		
		public function TextFieldOutput(tf:TextField) 
		{
			_tf = tf;
		}
		
		override public function write(text:String, level:uint):void 
		{
			_tf.htmlText += "<p><font color='" + levelColors[level] + "'>" + text + "</font></p>";
		}
	}

}