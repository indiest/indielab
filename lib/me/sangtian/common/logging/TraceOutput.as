package me.sangtian.common.logging 
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class TraceOutput extends LogOutput
	{
		
		public function TraceOutput() 
		{
			
		}
		
		override public function write(text:String, level:uint):void
		{
			trace(text);
		}
		
	}

}