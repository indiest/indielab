package me.sangtian.common.logging 
{
	import me.sangtian.common.util.FileUtil;
	import me.sangtian.common.util.StringUtil;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class FileOutput extends LogOutput
	{
		private var _fs:FileStream = new FileStream();
		
		public function FileOutput(path:String = null) 
		{
			if (path == null)
			{
				path = "app-storage:/log/" + StringUtil.getLogFormatTimeString(new Date()) + ".log";
			}
			var file:File = new File(path);
			_fs.open(file, FileMode.APPEND);
			_fs.writeUTFBytes("=====" + new Date().toLocaleString() + "=====\r\n");
		}
		
		override public function write(text:String, level:uint):void
		{
			_fs.writeUTFBytes(text + "\r\n");
		}
	}

}