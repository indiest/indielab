package me.sangtian.common.logging 
{
	import com.gamua.flox.Flox;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class FloxOutput extends LogOutput
	{
		
		public function FloxOutput() 
		{
			
		}
		
		override public function write(text:String, level:uint):void
		{
			if (level == Log.LEVEL_INFO)
				Flox.logInfo(text);
			else if (level == Log.LEVEL_WARN)
				Flox.logWarning(text);
			else if (level == Log.LEVEL_ERROR)
				Flox.logError(text);
		}
		
	}

}