package me.sangtian.common.logging 
{
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * Facade class for logging serivce.
	 * Calling debug/info/warn/error of this class can log to trace output, but is not recommended.
	 * 
	 * @author Nicolas Tian
	 */
	public class Log 
	{
		public static const LEVEL_PREFIXES:Vector.<String> = new <String>["DEBUG", "INFO ", "WARN ", "ERROR"];

		public static const LEVEL_DEBUG:uint = 0;
		public static const LEVEL_INFO:uint = 1;
		public static const LEVEL_WARN:uint = 2;
		public static const LEVEL_ERROR:uint = 3;
		
		public static var outputs:Vector.<LogOutput> = new <LogOutput>[new TraceOutput];
		public static var thresholdLevel:uint = 0;
		
		private static var _registeredOutputs:Dictionary = new Dictionary();
		private static var _config:Object;
		
		public function Log()
		{
		}
		
		public static function registerOuputClass(clazz:Class, alias:String = null):void
		{
			var name:String = alias || getQualifiedClassName(clazz);
			_registeredOutputs[name] = clazz;
		}
		
		public static function loadConfig(cfgObj:Object):void
		{
			_config = cfgObj;
			thresholdLevel = cfgObj.thresholdLevel;
			outputs.length = 0;
			for each(var outputObj:Object in cfgObj.outputs)
			{
				var outputClass:Class = _registeredOutputs[outputObj.type];
				if (outputClass == null)
				{
					trace("[LOG] Unregistered output type:", outputObj.type);
					continue;
				}
				var output:LogOutput;
				try
				{
					//var outputClass:Class = getDefinitionByName(outputObj.type) as Class;
					output = ObjectUtil.makeConstructorFunction(outputClass).apply(null, outputObj.args);
					ObjectUtil.applyProperties(output, outputObj);
				}
				catch (err:Error)
				{
					CONFIG::debug
					{
						throw err;
					}
					CONFIG::release
					{
						error("Failed to create Log Output:", err);
						continue;
					}
				}
				outputs.push(output);
			}
		}
		
		public static function initLogger(logger:Logger):void
		{
			if (_config == null)
				return;
			for each(var loggerObj:Object in _config.loggers)
			{
				var matchName:String = loggerObj._name;
				if (matchName == logger.name ||
					(loggerObj.matchAll && logger.name.search(matchName) >= 0))
				{
					ObjectUtil.applyProperties(logger, loggerObj);
				}
			}
		}
		
		private static function formatText(format:String, text:String, level:uint, logger:Logger = null):String
		{
			if (format == null)
				return text;
			var params:Object = {
				text: text,
				level: LEVEL_PREFIXES[level],
				time: new Date(),
				logger: logger ? logger.name : "Default"
			};
			return printf(format, params);
		}
		
		public static function write(text:String, level:uint, logger:Logger = null):void
		{
			if (level < thresholdLevel)
			{
				return;
			}
			
			for each(var output:LogOutput in outputs)
			{
				if (level >= output.thresholdLevel)
				{
					text = formatText((logger ? logger.format : null) || output.format, text, level, logger);
					output.write(text, level);
				}
			}
		}
		
		public static function get isDebugEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_DEBUG;
		}
		
		public static function get isInfoEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_INFO;
		}
		
		public static function get isWarnEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_WARN;
		}
		
		public static function get isErrorEnabled():Boolean
		{
			return thresholdLevel <= LEVEL_ERROR;
		}
		
		public static function debug(...text):void
		{
			CONFIG::debug
			{
				if (isDebugEnabled)
					write(text.join(" "), LEVEL_DEBUG);
			}
		}
		
		public static function info(...text):void
		{
			if (isInfoEnabled)
				write(text.join(" "), LEVEL_INFO);
		}
		
		public static function warn(...text):void
		{
			if (isWarnEnabled)
				write(text.join(" "), LEVEL_WARN);
		}
		
		public static function error(...text):void
		{
			if (isErrorEnabled)
				write(text.join(" "), LEVEL_ERROR);
		}
	}

}
