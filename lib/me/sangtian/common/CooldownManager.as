package me.sangtian.common 
{
	import flash.utils.Dictionary;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class CooldownManager 
	{
		private var _items:Dictionary = new Dictionary();
		
		public function CooldownManager() 
		{
			
		}
		
		public function add(id:int, cooldown:Number, initRemainingTime:Number = 0):void
		{
			var item:CooldownItem = new CooldownItem();
			item.cooldown = cooldown;
			item.remainingTime = item.initRemainingTime = initRemainingTime;
			_items[id] = item;
		}
		
		public function setCooldown(id:int, cooldown:Number):void
		{
			var item:CooldownItem = _items[id];
			item.cooldown = cooldown;
		}
		
		public function setRemainingTime(id:int, remainingTime:Number = 0):void
		{
			var item:CooldownItem = _items[id];
			item.remainingTime = remainingTime;
		}
		
		public function startCooldown(id:int):void 
		{
			var item:CooldownItem = _items[id];
			item.remainingTime = item.cooldown;
		}
		
		public function getRemainingTime(id:int):Number
		{
			var item:CooldownItem = _items[id];
			return item.remainingTime;
		}
		
		public function isCooledDown(id:int):Boolean
		{
			return getRemainingTime(id) == 0;
		}
		
		public function update(deltaSecond:Number):void
		{
			for (var id:* in _items)
			{
				var item:CooldownItem = _items[id];
				if (item.remainingTime > 0)
				{
					item.remainingTime = Math.max(0, item.remainingTime - deltaSecond);
				}
			}
		}
		
		public function resetAll(isCooledDown:Boolean = true):void 
		{
			for (var id:* in _items)
			{
				var item:CooldownItem = _items[id];
				item.remainingTime = isCooledDown ? item.initRemainingTime : item.cooldown;
			}
		}
	}

}

class CooldownItem
{
	public var cooldown:Number;
	public var remainingTime:Number;
	public var initRemainingTime:Number;
}