package me.sangtian.common
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.utils.describeType;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class AssetsCache 
	{
		public static const DEFAULT_SLOT:int = 0;
		
		public function AssetsCache() 
		{
			
		}

		private static var _slots:Dictionary = new Dictionary();
		{
			_slots[DEFAULT_SLOT] = new Dictionary();
		}
		
		public static function cache(key:*, asset:*, slotId:* = DEFAULT_SLOT):void
		{
			var slot:Dictionary = _slots[slotId];
			ASSERT(slot != null, "Slot should be created before cache");
			slot[key] = asset;
		}
		
		public static function cacheAll(assetsClass:Class, slotId:* = DEFAULT_SLOT):void
		{
			var slot:Dictionary = _slots[slotId];
			ASSERT(slot != null, "Slot should be created before cache");
			var typeInfo:XML = describeType(assetsClass);
			for each(var constant:* in typeInfo.constant)
			{
				var assetClass:* = assetsClass[constant.@name];
				if (assetClass is Class)
				{
					slot[assetClass] = new assetClass();
				}
			}
		}
		
		public static function createSlot(slotId:*):void
		{
			_slots[slotId] = new Dictionary();
		}
		
		public static function clearSlot(slotId:* = DEFAULT_SLOT):void
		{
			var slot:Dictionary = _slots[slotId];
			for (var key:* in slot)
			{
				delete slot[key];
			}
		}
		
		public static function getAsset(key:*, slotId:* = DEFAULT_SLOT):*
		{
			var slot:Dictionary = _slots[slotId];
			if (slot == null)
				return null;
			return slot[key];
		}
		
		public static function getOrCacheAsset(key:Class, slotId:* = DEFAULT_SLOT):*
		{
			var asset:* = getAsset(key, slotId);
			if (asset == null)
			{
				asset = new key();
				cache(key, asset, slotId);
			}
			return asset;
		}
		
		public static function getBitmapData(key:Class, slotId:* = DEFAULT_SLOT):BitmapData
		{
			var asset:* = getOrCacheAsset(key, slotId);
			if (asset is BitmapData)
				return asset;
			else if (asset is Bitmap)
				return (asset as Bitmap).bitmapData;
			else 
				return null;
		}
		
		public static function getNewBitmap(key:Class, slotId:* = DEFAULT_SLOT):Bitmap
		{
			return new Bitmap(getBitmapData(key, slotId));
		}
	}

}