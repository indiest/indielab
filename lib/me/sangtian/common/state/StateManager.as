package me.sangtian.common.state 
{
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author 
	 */
	public class StateManager 
	{
		private var _previousState:IState;
		private var _currentState:IState;
		private var _states:Object = { };
		private var _stateContainer:Object;
		
		public function StateManager(stateContainer:Object) 
		{
			_stateContainer = stateContainer;
		}
		
		public function get currentState():IState 
		{
			return _currentState;
		}
		
		// @param state	An IState instance or an IState class object.
		public function loadState(state:Object):IState
		{
			if (_currentState != null)
			{
				_currentState.exit();
				// don't save transient state as previous, because it may blocks when go back to it.
				if (!_currentState.isTransientState)
					_previousState = _currentState;
				_stateContainer.removeChild(_currentState);
			}
			var name:String = getQualifiedClassName(state);
			_currentState = _states[name];
			if (_currentState == null)
			{
				_currentState = addState(state, name);
			}
			_stateContainer.addChild(_currentState);
			_currentState.enter();
			return _currentState;
		}
		
		public function addState(state:Object, name:String = null):IState
		{
			var result:IState = state is Class ? new state() as IState : state as IState;
			if (result == null)
				throw new ArgumentError("state is not a valid IState instance or IState class object");
			if (name == null)
				name = getQualifiedClassName(state);
			_states[name] = result;
			return result;
		}
		
		public function getState(stateClass:Class):IState
		{
			return _states[getQualifiedClassName(stateClass)];
		}
		
		public function backState():IState
		{
			if (_previousState == null)
				return null;
			return loadState(_previousState);
		}
		
		public function update():void
		{
			if (_currentState != null)
				_currentState.update();
		}
	}

}