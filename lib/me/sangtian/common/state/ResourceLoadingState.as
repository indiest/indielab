package me.sangtian.common.state
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.BinaryDataLoader;
	import com.greensock.loading.core.LoaderItem;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import me.sangtian.common.AssetsCache;
	import me.sangtian.common.state.IState;
	
	/**
	 * Base class for resource loading state.
	 * Usage:
	 * 	Specify resource list file before load this state.
	 * 	Resource list file is a JSON file consisted of item objects. Each item is a JSON object which should have properties:
	 *		path:String - the relative path of the resource file. Override getUrl method to manipulate the download URL.
	 * 		[cache:Boolean] - should the loaded resource be cached. Default is true.
	 * 		[slotId:int] - which slotId should it use in AssetsCache. Default is 0.
	 * 		[id:String] - the key for caching and retrieving. If not specified, path will be used as id.
	 * 
	 * @author Nicolas Tian
	 */
	public class ResourceLoadingState extends Sprite implements IState 
	{
		//private static var _cache:Dictionary = new Dictionary();
		//public static function getCached(id:String):*
		//{
			//return _cache[id];
		//}
		
		protected static var _fileTypeLoaders:Object =
		{
			"jpg,png": ImageLoader,
			"dat,bin": BinaryDataLoader,
			"txt,json": DataLoader
		};
		
		public static function registerFileType(extensions:String, loaderClass:Class):void
		{
			_fileTypeLoaders[extensions] = loaderClass;
		}
		
		public static var resourceListPath:String;
		
		protected var _loaderQueue:LoaderMax = new LoaderMax( { onError: onLoaderError } );
		
		public function ResourceLoadingState() 
		{
			initLoaders();
		}
		
		protected function initLoaders():void 
		{
			var loaderClasses:Array = [];
			for (var fileType:String in _fileTypeLoaders)
			{
				var loaderClass:Class = _fileTypeLoaders[fileType];
				LoaderMax.registerFileType(fileType, loaderClass);
				loaderClasses.push(loaderClass);
			}
			LoaderMax.activate(loaderClasses);
		}
		
		/* INTERFACE me.sangtian.common.state.IState */
		
		public function get isTransientState():Boolean 
		{
			return true;
		}
		
		public function enter():void 
		{
			ASSERT(resourceListPath != null);
			_loaderQueue.append(new DataLoader(resourceListPath, {format: "text", onComplete: onResourceListLoadComplete } ));
			_loaderQueue.load();
		}
		
		protected function onLoaderError(e:LoaderEvent):void
		{
			
		}
		
		private function onResourceListLoadComplete(e:LoaderEvent):void 
		{
			var loader:LoaderItem = e.target as LoaderItem;
			var list:Object = JSON.parse(loader.content);
			for each(var item:Object in list)
			{
				loadResourceItem(item);
			}
			_loaderQueue.addEventListener(LoaderEvent.COMPLETE, onAllItemsLoadComplete);
			_loaderQueue.load();
		}
		
		protected function loadResourceItem(item:Object):void 
		{
			if (AssetsCache.getAsset(getId(item), item.slotId) == null)
			{
				_loaderQueue.append(LoaderMax.parse(getUrl(item), { item: item, onComplete: onItemLoadComplete } ));
			}
		}
		
		protected function getUrl(item:Object):String 
		{
			return item.path;
		}
		
		protected function getId(item:Object):String
		{
			return item.id || item.path;
		}
		
		private function onItemLoadComplete(e:LoaderEvent):void 
		{
			var loader:LoaderItem = e.target as LoaderItem;
			processResourceItem(loader.vars.item, loader.content);
		}
		
		protected function processResourceItem(item:Object, content:*):void 
		{
			if (item.cache != false)
			{
				AssetsCache.cache(getId(item), content, item.slotId);
			}
		}
		
		private function onAllItemsLoadComplete(e:LoaderEvent):void 
		{
			loadNextState();
		}
		
		protected function loadNextState():void 
		{
		}
		
		public function update():void 
		{
			
		}
		
		public function exit():void 
		{
		}
		
	}

}