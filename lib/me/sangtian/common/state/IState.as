package me.sangtian.common.state 
{
	/**
	 * ...
	 * @author 
	 */
	public interface IState
	{
		
		function get isTransientState():Boolean;
		
		function enter():void;
		
		function update():void;
		
		function exit():void;
		
	}

}