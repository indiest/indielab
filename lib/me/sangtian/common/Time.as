package me.sangtian.common 
{
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Time 
	{
		private static var _lastTick:int = 0;
		private static var _deltaTime:int = 0;
		private static var _totalTime:int = 0;
		private static var _totalFrames:int = 0;
		private static var _fps:uint = 0;
		private static var _frameCounter:uint = 0;
		private static var _timeInSecond:uint = 0;
		/**
		 * Set time scale factor smaller/bigger to achieve slower/faster playing effect.
		 */
		public static var scale:Number = 1.0;
		
		[Inline]
		public static function tick():void
		{
			var tick:int = getTimer();
			// Real world time
			var deltaTime:int = tick - _lastTick;
			if (_lastTick > 0)
			{
				_totalTime += deltaTime;
				_deltaTime = scale * deltaTime;
			}
			_lastTick = tick;
			
			_frameCounter++;
			_totalFrames++;
			_timeInSecond += deltaTime;
			if (_timeInSecond >= 1000)
			{
				_timeInSecond -= 1000;
				_fps = _frameCounter;
				_frameCounter = 0;
			}
		}
		
		[Inline]
		public static function get lastTickTime():int
		{
			return _lastTick;
		}
		
		/**
		 * Get time period in millisecond since last tick. It's affected by scale factor.
		 */
		[Inline]
		public static function get deltaTime():int
		{
			return _deltaTime;
		}
		
		/**
		 * Get time period in second since last tick. It's affected by scale factor.
		 */
		[Inline]
		public static function get deltaSecond():Number 
		{
			return _deltaTime * 0.001;
		}
		
		/**
		 * Get current framerate which is calculated based on the ticking. It's not affected by scale factor.
		 */
		[Inline]
		public static function get fps():uint
		{
			return _fps;
		}
		
		/**
		 * Get total milliseconds since first ticking. It's not affected by scale factor.
		 */
		[Inline]
		public static function get totalTime():int 
		{
			return _totalTime;
		}
		
		/**
		 * Get average FPS since first ticking. It's not affected by scale factor.
		 */
		[Inline]
		public static function get averageFps():Number
		{
			return _totalFrames * 1000 / _totalTime;
		}
	}

}