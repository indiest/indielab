package me.sangtian.common.input 
{
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class KeyboardController extends Controller 
	{
		
		public static var inactiveWhenInputtingText:Boolean = false;
		
		private var _stage:Stage;
		
		public function KeyboardController(stage:Stage) 
		{
			//KeyboardControllerKey.cacheKeyboardKeys();
			super("Keyboard");
			_stage = stage;
			stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);
		}
		
		private function handleKeyDown(e:KeyboardEvent):void 
		{
			if (!enabled)
				return;
			if (inactiveWhenInputtingText)
			{
				if (_stage.focus is TextField && (_stage.focus as TextField).type == TextFieldType.INPUT)
					return;
			}
			var key:KeyboardControllerKey = KeyboardControllerKey.fromKeyCode(e.keyCode);
			onKeyDown(key);
		}
		
		private function handleKeyUp(e:KeyboardEvent):void 
		{
			if (!enabled)
				return;
			if (inactiveWhenInputtingText)
			{
				if (_stage.focus is TextField && (_stage.focus as TextField).type == TextFieldType.INPUT)
					return;
			}
			var key:KeyboardControllerKey = KeyboardControllerKey.fromKeyCode(e.keyCode);
			onKeyUp(key);
		}
	}

}