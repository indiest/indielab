package me.sangtian.common.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	[Event(name="moving", type="me.sangtian.common.ScrollEvent")] 
	public class ScrollList extends ScrollContainer 
	{
		public var xFactor:Number;
		public var yFactor:Number;
		public var mouseWheelFactor:Number = 10;
		public var clickCheckPixels:Number = 10;
		
		protected var _mousePressed:Boolean;
		protected var _mouseDownX:Number;
		protected var _mouseDownY:Number;
		protected var _scrollDownX:Number;
		protected var _scrollDownY:Number;
		protected var _clicked:Boolean;
		
		public function get clicked():Boolean 
		{
			return _clicked;
		}
		
		public function ScrollList(width:uint, height:uint, direction:uint = DIRECTION_VERTICAL, xFactor:Number = 0.0, yFactor:Number = 1.0)
		{
			super(width, height, direction);
			this.xFactor = xFactor;
			this.yFactor = yFactor;
			
			addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			addEventListener(MouseEvent.MOUSE_MOVE, handleMouseMove);
			addEventListener(MouseEvent.ROLL_OUT, handleMouseOut);
			//addEventListener(MouseEvent.MOUSE_OUT, handleMouseOut);
			addEventListener(MouseEvent.MOUSE_WHEEL, handleMouseWheel);
		}
		
		private function updateScroll():void 
		{
			checkScrollBoundary();
			scrollRect = _scrollRect;
			dispatchEvent(new ScrollEvent(ScrollEvent.MOVING, percentageX, percentageY));
		}
		
		private function handleMouseWheel(e:MouseEvent):void 
		{
			if (e.delta != 0)
			{
				_scrollRect.x -= e.delta * xFactor * mouseWheelFactor;
				_scrollRect.y -= e.delta * yFactor * mouseWheelFactor;
				updateScroll();
			}
		}
		
		private function handleMouseMove(e:MouseEvent):void 
		{
			if (_mousePressed)
			{
				_scrollRect.x = _scrollDownX + (_mouseDownX - e.stageX) * xFactor;
				_scrollRect.y = _scrollDownY + (_mouseDownY - e.stageY) * yFactor;
				updateScroll();
			}
		}
		
		protected function releaseMouse(mouseX:Number, mouseY:Number):void 
		{
			_mousePressed = false;
		}
		
		protected function handleMouseOut(e:MouseEvent):void 
		{
			if (!_mousePressed)
				return;
			//trace("mouse out");
			_clicked = false;
			releaseMouse(e.stageX, e.stageY);
			_mousePressed = false;
		}
		
		protected function handleMouseUp(e:MouseEvent):void 
		{
			if (!_mousePressed)
				return;
			//trace("mouse up");
			_clicked = (Math.abs(_mouseDownX - e.stageX) <= clickCheckPixels && Math.abs(_mouseDownY - e.stageY) <= clickCheckPixels);
			releaseMouse(e.stageX, e.stageY);
		}
		
		protected function handleMouseDown(e:MouseEvent):void 
		{
			//trace("mouse down");
			_mousePressed = true;
			_clicked = false;
			_mouseDownX = e.stageX;
			_mouseDownY = e.stageY;
			_scrollDownX = _scrollRect.x;
			_scrollDownY = _scrollRect.y;
		}
		
	}

}