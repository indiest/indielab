package me.sangtian.common.ui 
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.ColorChooser;
	import com.bit101.components.ComboBox;
	import com.bit101.components.Label;
	import com.bit101.components.List;
	import com.bit101.components.Slider;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class UIUtil 
	{
		
		public function UIUtil() 
		{
			
		}
		
		public static function addPropertyCheckbox(parent:DisplayObjectContainer, x:Number, y:Number, obj:Object, propName:String):CheckBox
		{
			var checkbox:CheckBox = new CheckBox(parent, x, y, propName,
				function(e:*):void
				{
					obj[propName] = checkbox.selected;
				}
			);
			checkbox.selected = obj[propName];
			return checkbox;
		}
		
		public static function addPropertySlider(parent:DisplayObjectContainer, x:Number, y:Number, obj:Object, propName:String, min:Number, max:Number, tick:Number = 0.01):Slider
		{
			var labelPropName:Label = new Label(null, 0, 0, propName);
			var slider:Slider = new Slider(Slider.HORIZONTAL, parent, x + labelPropName.width + 20, y,
				function(e:*):void
				{
					obj[propName] = slider.value;
					labelValue.text = slider.value.toFixed(2);
				}
			);
			var labelValue:Label = new Label(null, slider.width, 0);
			slider.addChild(labelValue);
			labelPropName.x = -labelPropName.width - 20;
			slider.addChild(labelPropName);
			slider.value = obj[propName];
			slider.minimum = min;
			slider.maximum = max;
			slider.tick = tick;
			return slider;
		}
		
		public static function addPropertyColorChooser(parent:DisplayObjectContainer, x:Number, y:Number, obj:Object, propName:String):ColorChooser
		{
			var colorChooser:ColorChooser = new ColorChooser(parent, x, y, obj[propName],
				function(e:*):void
				{
					obj[propName] = colorChooser.value;
				}
			);
			colorChooser.usePopup = true;
			return colorChooser;
		}
		
		public static function addPropertyComboBox(parent:DisplayObjectContainer, x:Number, y:Number, obj:Object, propName:String, enumClass:Class):ComboBox
		{
			var cb:ComboBox = new ComboBox(parent, x, y, propName, ObjectUtil.constantsToArray(enumClass, "label", "value"));
			cb.addEventListener(Event.SELECT, function(e:*):void
			{
				obj[propName] = cb.selectedItem["value"];
			});
			return cb;
		}
	}

}