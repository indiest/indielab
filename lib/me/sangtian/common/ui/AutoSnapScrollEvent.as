package me.sangtian.common.ui 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class AutoSnapScrollEvent extends Event 
	{
		public static const SNAP:String = "snap";
		
		private var _snapToIndex:uint;
		
		public function AutoSnapScrollEvent(type:String, snapToIndex:uint, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_snapToIndex = snapToIndex;
		}
		
		public function get snapToIndex():uint 
		{
			return _snapToIndex;
		}
		
	}

}