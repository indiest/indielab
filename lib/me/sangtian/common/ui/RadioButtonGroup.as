package me.sangtian.common.ui 
{
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class RadioButtonGroup 
	{
		private var _buttons:Array = [];
		private var _selectedIndex:int = -1;
		public var onSelectionChanged:Function;
		
		public function get selectedIndex():int 
		{
			return _selectedIndex;
		}
		
		public function set selectedIndex(value:int):void 
		{
			_selectedIndex = value;
			selectButton(value < 0 ? null : _buttons[value]);
		}
		
		public function get selectedButton():InteractiveObject
		{
			return _buttons[selectedIndex];
		}
		
		public function get numButtons():uint
		{
			return _buttons.length;
		}
				
		// onSelectionChanged params = button:InteractiveObject, selected:Boolean
		public function RadioButtonGroup(onSelectionChanged:Function = null) 
		{
			this.onSelectionChanged = onSelectionChanged;
		}

		public function addRadioButton(button:InteractiveObject, index:int = -1):void
		{
			button.addEventListener(MouseEvent.CLICK, handleClickButton);
			if (index > 0)
				_buttons[index] = button;
			else
				_buttons.push(button);
		}
		
		public function getRadioButton(index:uint):InteractiveObject 
		{
			return _buttons[index];
		}
		
		public function removeRadioButton(button:InteractiveObject):void 
		{
			var index:int = _buttons.indexOf(button);
			if (index < 0)
				return;
			button.removeEventListener(MouseEvent.CLICK, handleClickButton);
			_buttons.splice(index, 1);
			if (selectedIndex == index)
			{
				selectedIndex = Math.min(index, numButtons - 1);
			}
		}
		
		private function handleClickButton(e:MouseEvent):void 
		{
			selectButton(e.currentTarget as InteractiveObject);
		}
		
		public function selectButton(button:InteractiveObject):void
		{
			for (var i:uint = 0; i < _buttons.length; i++)
			{
				var selected:Boolean = (_buttons[i] === button);
				if (selected)
					_selectedIndex = i;
				if (onSelectionChanged != null)
					onSelectionChanged(_buttons[i], selected);
			}
		}
		
		public function clear():void
		{
			for each (var button:InteractiveObject in _buttons)
				button.removeEventListener(MouseEvent.CLICK, handleClickButton);
			_buttons.length = 0;
			_selectedIndex = -1;
			onSelectionChanged = null;
		}
		
		public function realign():void 
		{
			if (_buttons.length == 0)
				return;
			getRadioButton(0).x = 0;
			for (var i:int = 1; i < _buttons.length; i++)
			{
				var lastButton:InteractiveObject = _buttons[i - 1];
				var button:InteractiveObject = _buttons[i];
				button.x = lastButton.x + lastButton.width;
			}
		}
	}

}