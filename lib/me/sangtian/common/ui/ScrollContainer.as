package me.sangtian.common.ui 
{
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import me.sangtian.common.logging.Log;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class ScrollContainer extends Sprite 
	{
		public static const DIRECTION_HORIZONTAL:uint = 1;
		public static const DIRECTION_VERTICAL:uint = 2;

		protected var _shape:Shape;
		protected var _scrollRect:Rectangle;
		protected var _itemContainer:Sprite;
		protected var _direction:uint;
		protected var _scrollIndex:uint = 0;
		
		public function ScrollContainer(width:uint, height:uint, direction:uint = DIRECTION_HORIZONTAL) 
		{
			// Add a placeholder shape to make sure mouse down events always occur in the rectangle area
			_shape = new Shape();
			_shape.graphics.beginFill(0, 0);
			_shape.graphics.drawRect(0, 0, width, height);
			_shape.graphics.endFill();
			addChildAt(_shape, 0);

			_scrollRect = new Rectangle(0, 0, width, height);
			_direction = direction;
			_itemContainer = new Sprite();
			super.addChild(_itemContainer);
			scrollRect = _scrollRect;
		}
		
		override public function set scrollRect(value:flash.geom.Rectangle):void 
		{
			super.scrollRect = value;
			_shape.x = value.x;
			_shape.y = value.y;
		}
		
		public function get scrollX():Number
		{
			return _scrollRect.x;
		}
		
		public function get scrollY():Number
		{
			return _scrollRect.y;
		}
		
		public function set scrollX(value:Number):void
		{
			_scrollRect.x = value;
			scrollRect = _scrollRect;
		}
		
		public function set scrollY(value:Number):void
		{
			_scrollRect.y = value;
			scrollRect = _scrollRect;
		}
		
		public function get numItems():uint
		{
			return _itemContainer.numChildren;
		}
		
		public function get direction():uint 
		{
			return _direction;
		}
		
		public function set direction(value:uint):void 
		{
			ASSERT(value == DIRECTION_VERTICAL || value == DIRECTION_HORIZONTAL);
			_direction = value;
		}
		
		public function get itemsWidth():uint
		{
			return _itemContainer.width;
		}
		
		public function get itemsHeight():uint
		{
			return _itemContainer.height;
		}
		
		public function get percentageX():Number
		{
			return _scrollRect.x / _itemContainer.width;
		}
		
		public function get percentageY():Number
		{
			return _scrollRect.y / _itemContainer.height;
		}
		
		public function get scrollIndex():uint 
		{
			return _scrollIndex;
		}
		
		public function set scrollIndex(value:uint):void 
		{
			if (value >= numItems)
				return;
			_scrollIndex = value;
			scrollToItem(_scrollIndex);
		}
		
		public function scrollToItem(index:uint, duration:Number = 0.5):void
		{
			var item:DisplayObject = getItem(index);
			var vars:Object;
			if (direction == DIRECTION_HORIZONTAL)
				vars = { scrollX: item.x };
			else if (direction == DIRECTION_VERTICAL)
				vars = { scrollY: item.y };
			TweenLite.to(this, duration, vars);
		}
		
		public function addItem(item:DisplayObject, autoAlign:Boolean = true, margin:uint = 0):DisplayObject
		{
			CONFIG::debug
			{
				//Log.debug("ScrollContainer.addItem, numItems:", numItems);
			}
			if (autoAlign)
			{
				if (direction == DIRECTION_HORIZONTAL)
					item.x = numItems > 0 ? _itemContainer.width + margin : 0;
				else
					item.y = numItems > 0 ? _itemContainer.height + margin : 0;
			}
			return _itemContainer.addChild(item);
		}
		
		public function addItemAt(item:DisplayObject, index:int):void
		{
			_itemContainer.addChildAt(item, index);
		}
		
		public function removeItem(index:uint):DisplayObject
		{
			return _itemContainer.removeChildAt(index);
		}
		
		public function getItem(index:uint):DisplayObject
		{
			return _itemContainer.getChildAt(index);
		}
		
		public function getItemIndex(item:DisplayObject):int
		{
			if (item.parent != _itemContainer)
				return -1;
			return _itemContainer.getChildIndex(item);
		}
		
		public function removeAllItems():void
		{
			while (numItems > 0)
				removeItem(0);
			resetScroll();
		}
		
		protected function checkScrollBoundary():void 
		{
			if (_scrollRect.x > _itemContainer.width - _scrollRect.width)
				_scrollRect.x = _itemContainer.width - _scrollRect.width;
			if (_scrollRect.x < 0)
				_scrollRect.x = 0;
			if (_scrollRect.y > _itemContainer.height - _scrollRect.height)
				_scrollRect.y = _itemContainer.height - _scrollRect.height;
			if (_scrollRect.y < 0)
				_scrollRect.y = 0;
		
		}
		
		public function resetScroll():void
		{
			_scrollRect.x = 0;
			_scrollRect.y = 0;
			scrollRect = _scrollRect;
		}
	}

}