package me.sangtian.common.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	import me.sangtian.common.util.DisplayObjectUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class MenuItem extends Sprite 
	{
		public var checkbox:DisplayObject;
		public var label:TextField;
		
		public function get selected():Boolean
		{
			return checkbox.visible;
		}
		public function set selected(value:Boolean):void
		{
			checkbox.visible = value;
		}
		
		public function get labelText():String
		{
			return label.text;
		}
		public function set labelText(value:String):void
		{
			label.text = value;
		}
		
		private var _enabled:Boolean = true;
		public function get enabled():Boolean
		{
			return _enabled;
		}
		public function set enabled(value:Boolean):void
		{
			_enabled = value;
			mouseEnabled = value;
			DisplayObjectUtil.tint(this, value ? 1 : 0.5);
		}
		
		public var id:String;
		public var key:String;
		
		public function MenuItem() 
		{
			mouseChildren = false;
			addEventListener(MouseEvent.CLICK, handleClick);
		}
		
		private function handleClick(e:MouseEvent):void 
		{
			if (key == null || key.length == 0)
				return;
				
			var ke:KeyboardEvent = new KeyboardEvent(KeyboardEvent.KEY_DOWN);
			var keys:Array = key.split("+");
			for each(var keyName:String in keys)
			{
				if (keyName == "Ctrl")
				{
					ke.controlKey = true;
				}
				else if (keyName == "Alt")
				{
					ke.altKey = true;
				}
				else if (keyName == "Shift")
				{
					ke.shiftKey = true;
				}
				else
				{
					ke.keyCode = Keyboard[keyName.toUpperCase()];
				}
			}
			dispatchEvent(ke);
		}
		
	}

}