package me.sangtian.common.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class SwitchableButton extends Sprite 
	{
		private var _states:Object = { };
		private var _state:String;
		
		public function get state():String 
		{
			return _state;
		}
		
		public function set state(value:String):void 
		{
			if (_state == value)
				return;
			if (stateButton)
				//removeChild(stateButton);
				stateButton.visible = false;
			_state = value;
			if (stateButton)
				//addChild(stateButton);
				stateButton.visible = true;
		}
		
		public function get stateButton():DisplayObject
		{
			return _states[_state];
		}
		
		public function SwitchableButton() 
		{
			
		}
		
		public function addState(state:String, button:DisplayObject, visible:Boolean = false):void
		{
			_states[state] = button;
			addChild(button);
			button.visible = visible;
		}
		
	}

}