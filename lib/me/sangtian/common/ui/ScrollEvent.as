package me.sangtian.common.ui 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Nicolas
	 */
	public class ScrollEvent extends Event 
	{
		public static const MOVING:String = "moving";
		
		private var _percentageX:Number;
		private var _percentageY:Number;
		
		public function ScrollEvent(type:String, percentageX:Number, percentageY:Number, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_percentageX = percentageX;
			_percentageY = percentageY;
		}
		
		public function get percentageX():Number 
		{
			return _percentageX;
		}
		
		public function get percentageY():Number 
		{
			return _percentageY;
		}
		
	}

}