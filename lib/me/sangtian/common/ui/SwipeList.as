package me.sangtian.common.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.TransformGestureEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	[Event(name="moving", type="me.sangtian.common.ScrollEvent")] 
	public class SwipeList extends ScrollContainer 
	{
		public static const SWIPE_BY_PIXEL:uint = 1;
		public static const SWIPE_BY_ITEM:uint = 2;
		
		//private var _direction:uint;
		//public function get direction():uint 
		//{
			//return _direction;
		//}
		//public function set direction(value:uint):void 
		//{
			//_direction = value;
		//}
		
		private var _swipeType:uint;		
		public function get swipeType():uint 
		{
			return _swipeType;
		}		
		public function set swipeType(value:uint):void 
		{
			_swipeType = value;
		}
			
		private var _scrollSpeed:Number;
		public function get scrollSpeed():Number 
		{
			return _scrollSpeed;
		}
		public function set scrollSpeed(value:Number):void 
		{
			_scrollSpeed = value;
		}
		
		private var _swipeItemSize:Number;	
		public function get swipeItemSize():Number 
		{
			return _swipeItemSize;
		}		
		public function set swipeItemSize(value:Number):void 
		{
			_swipeItemSize = value;
		}
		
		public function get swiping():Boolean
		{
			return _offset.length > 1;
		}
		
		//// press arrow key to simulate swip action.
		//// NOTICE: please set focus on the list to make sure it works.
		//private var _simulateSwipe:Boolean = false;
		//public function get simulateSwipe():Boolean 
		//{
			//return _simulateSwipe;
		//}
		
		public function simulateSwipe(stage:Stage, on:Boolean):void 
		{
			//_simulateSwipe = value;
			if (on)
			{
				stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
				stage.focus = this;
			}
			else
			{
				stage.removeEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
				stage.focus = null;
			}
		}
		
		private function handleKeyDown(e:KeyboardEvent):void 
		{
			var evt:TransformGestureEvent = new TransformGestureEvent(TransformGestureEvent.GESTURE_SWIPE);
			if (e.keyCode == Keyboard.LEFT)
			{
				evt.offsetX = -1;
			}
			else if (e.keyCode == Keyboard.RIGHT)
			{
				evt.offsetX = 1;
			}
			else if (e.keyCode == Keyboard.UP)
			{
				evt.offsetY = -1;
			}
			else if (e.keyCode == Keyboard.DOWN)
			{
				evt.offsetY = 1;
			}
			handleSwipe(evt);
		}
		
		public function SwipeList(width:uint, height:uint, direction:uint = DIRECTION_VERTICAL, swipeType:uint = SWIPE_BY_PIXEL, scrollSpeed:Number = 20) 
		{
			super(width, height);
			this.direction = direction;
			this.swipeType = swipeType;
			this.scrollSpeed = scrollSpeed;
			this.swipeItemSize = width;
			
			addEventListener(TransformGestureEvent.GESTURE_SWIPE, handleSwipe);
			addEventListener(Event.ENTER_FRAME, update);
		}
			
		private var _offset:Point = new Point();
		private var _offsetDir:Point = new Point();
		
		private function handleSwipe(e:TransformGestureEvent):void 
		{
			if (direction == DIRECTION_HORIZONTAL)
			{
				_offsetDir.x = e.offsetX;
				_offsetDir.y = 0;
				_offset.x = -e.offsetX;
				_offset.y = 0;
			}
			else
			{
				_offsetDir.x = 0;
				_offsetDir.y = e.offsetX;
				_offset.x = 0;
				_offset.y = -e.offsetY;
			}
			
			if (swipeType == SWIPE_BY_PIXEL)
				_offset.normalize(scrollSpeed);
			else	
				_offset.normalize(Math.sqrt(swipeItemSize * 2 + 0.25));
		}
		
		public function update(evt:Event = null):void
		{
			if (_offset.length <= 1)
				return;
			_scrollRect.x += _offset.x
			_scrollRect.y += _offset.y;
			_offset.offset(_offsetDir.x, _offsetDir.y);
			checkScrollBoundary();
			scrollRect = _scrollRect;		
			//trace(scrollRect, _offset);
			dispatchEvent(new ScrollEvent(ScrollEvent.MOVING, percentageX, percentageY));
		}
	}

}