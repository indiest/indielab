package me.sangtian.common.ui 
{
	import me.sangtian.common.util.MathUtil;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Nicolas
	 */
	[Event(name="snap", type="me.sangtian.common.AutoSnapScrollEvent")] 
	public class AutoSnapScrollList extends ScrollList 
	{
		public var distanceToSnapInPercentage:Number = 0.1;
		public var swapTimeThreshold:uint = 1000;
		public var autoSnapToLeft:Boolean;

		protected var _mouseDownTime:uint;
		protected var _speedX:Number = 0;
		protected var _speedY:Number = 0;
		protected var _snapToX:Number;
		protected var _snapToIndex:int = 0;
		public function get currentIndex():int 
		{
			return _snapToIndex;
		}
		
		public function AutoSnapScrollList(width:uint, height:uint, direction:uint = DIRECTION_HORIZONTAL, xFactor:Number = 1.0, yFactor:Number = 0, autoSnapToLeft:Boolean = true)
		{
			super(width, height, direction, xFactor, yFactor);
			this.autoSnapToLeft = autoSnapToLeft;
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		override protected function releaseMouse(mouseX:Number, mouseY:Number):void 
		{
			super.releaseMouse(mouseX, mouseY);
			
			if (autoSnapToLeft && !_clicked && numItems > 0)
			{
				var holdTime:int = Time.lastTickTime - _mouseDownTime;
				var snapToIndex:int = _snapToIndex;
				// held for a short time?
				if (holdTime < swapTimeThreshold)
				{
					if (direction == DIRECTION_HORIZONTAL)
						snapToIndex += MathUtil.sign(_mouseDownX - mouseX);
					else
						snapToIndex += MathUtil.sign(_mouseDownY - mouseY);
					if (snapToIndex < 0)
						snapToIndex = 0;
					else if (snapToIndex >= numItems)
						snapToIndex = numItems - 1;
				}
				else
				{
					for (var i:uint = 0; i < numItems; i++)
					{
						var item:DisplayObject = getItem(i);
						if (direction == DIRECTION_HORIZONTAL)
						{
							if (Math.abs(item.x - _scrollRect.x) <= item.width * distanceToSnapInPercentage)
							{
								snapToIndex = i;
								_speedX = (item.x - _scrollRect.x) / 200;
								break;
							}
						}
						else
						{
							// TODO: implement another direction
						}
					}
				}
				
				//trace("snap to index:", snapToIndex);
				// if no item matches, snap to previous item
				snapTo(snapToIndex);
			}
		}
		
		public function snapTo(index:uint):void
		{
			_speedX = (getItem(index).x - _scrollRect.x) / 200;
			if (index != _snapToIndex)
			{
				_snapToIndex = index;
				dispatchEvent(new AutoSnapScrollEvent(AutoSnapScrollEvent.SNAP, index));
			}
		}
		
		private function update(e:Event = null):void 
		{
			if (numItems == 0)
				return;
			if (_speedX != 0)
			{
				var dx:Number = _speedX * Time.deltaTime;
				var snapToX:Number = getItem(_snapToIndex).x;
				if (Math.abs(snapToX - _scrollRect.x) <= Math.abs(dx))
				{
					_speedX = 0;
					_scrollRect.x = snapToX;
				}
				else
				{
					_scrollRect.x += dx;
				}
			}
			if (_speedY != 0)
			{
				var dy:Number = _speedY * Time.deltaTime;
				var snapToY:Number = getItem(_snapToIndex).y;
				if (Math.abs(snapToY - _scrollRect.y) <= Math.abs(dy))
				{
					_speedY = 0;
					_scrollRect.y = snapToY;
				}
				else
				{
					_scrollRect.y += dy;
				}
			}
			//checkScrollBoundary();
			scrollRect = _scrollRect;
		}
		
		override protected function handleMouseDown(e:MouseEvent):void 
		{
			super.handleMouseDown(e);
			
			_mouseDownTime = Time.lastTickTime;
			_speedX = 0;
			_speedY = 0;
		}
	}

}