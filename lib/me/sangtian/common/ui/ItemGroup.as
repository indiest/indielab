package me.sangtian.common.ui 
{
	/**
	 * ...
	 * @author 
	 */
	public class ItemGroup 
	{
		private var _items:Vector.<Object>;
		
		public function get count():uint
		{
			return _items.length;
		}
		
		public function ItemGroup(uiController:Object, itemPrefix:String, count:uint = 0) 
		{
			if (count == 0)
			{
				while (uiController.hasOwnProperty(itemPrefix + count))
				{
					count++;
				}
			}
			_items = new Vector.<Object>(count, true);
			for (var i:int = 0; i < count; i++)
			{
				_items[i] = uiController[itemPrefix + i];
			}
		}
		
		public function getItem(index:uint):Object
		{
			return _items[index];
		}
		
		public function getIndex(item:Object):int
		{
			for (var i:int = 0; i < count; i++)
			{
				if (_items[i] === item)
					return i;
			}
			return -1;
		}
		
		public function addEventListener(type:String, listener:Function):void
		{
			for (var i:int = 0; i < count; i++)
			{
				_items[i].addEventListener(type, listener);
			}
		}
		
	}

}