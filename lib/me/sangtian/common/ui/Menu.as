package me.sangtian.common.ui 
{
	import flash.display.Sprite;
	import me.sangtian.common.logging.Logger;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Menu extends Sprite 
	{
		private static const logger:Logger = Logger.create(Menu);
		
		private var _rootItemsContainer:Sprite = new Sprite();
		private var _subItemsContainer:Sprite = new Sprite();
		
		private var _items:Object = { };
		
		public function Menu() 
		{
			addChild(_rootItemsContainer);
		}
		
		private var _menuItemType:Class;
		public function registerMenuItemType(type:Class):void
		{
			_menuItemType = type;
		}
		
		public function addItem(id:String, path:String = "/", key:String = null):void
		{
			var subpath:Array = path.split("/");
			if (subpath.length < 2)
			{
				logger.warn("Invalid path:", path, "id:", id);
				return;
			}
			
			var item:MenuItem = new _menuItemType();
			item.id = id;
			item.key = key;
			
			var items:Object = _items;
			for (var i:uint = 0; i < subpath.length; i++)
			{
				var subItems:Object = items[subpath[i]];
				if (subItems == null)
				{
					subItems = { };
					_items[subpath[i]] = subItems;
				}
				items = subItems;
			}
			items[id] = item;
		}
		
		private function showItems(items:Object, container:Sprite):void
		{
			for each(var item:MenuItem in items)
			{
				item.x = 0;
				item.y = 0;
				container.addChild(item);
			}
		}
		
		public function load(data:Object):void
		{
			loadItems(data.items, "/");
			var rootItems:Array = _items[""];
			showItems(rootItems, _rootItemsContainer);
		}
		
		private function loadItems(itemsData:Array, path:String):void
		{
			for each(var itemData:Object in itemsData)
			{
				addItem(itemData.id, path, itemData.key);
				if (itemData.items)
				{
					loadItems(itemData.items, path + itemData.id + "/");
				}
			}
		}
	}

}