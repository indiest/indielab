package me.sangtian.common.ui 
{
	import me.sangtian.common.logging.Log;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class TabControl 
	{
		private var _container:DisplayObjectContainer;
		private var _tabButtons:RadioButtonGroup;
		private var _pages:Object = { };
		private var _currentPageName:String;
		public var onSelectionChanged:Function;
		
		public function get currentPageName():String 
		{
			return _currentPageName;
		}
		
		public function set currentPageName(value:String):void 
		{
			if (_currentPageName == value)
				return;
			if (currentPageName != null)
			{
				//_container.removeChild(currentPage);
				currentPage.visible = false;
				
			}
			_currentPageName = value;
			//_container.addChild(currentPage);
			currentPage.visible = true;
			_tabButtons.selectButton(_container.getChildByName(value) as InteractiveObject);
		}
		
		public function get currentPage():DisplayObject
		{
			return getPage(_currentPageName);
		}
		
		public function TabControl(container:DisplayObjectContainer, onSelectionChanged:Function = null) 
		{
			_container = container;
			_tabButtons = new RadioButtonGroup(onSelectionChanged);
		}
		
		public function getPage(pageName:String):DisplayObject
		{
			return _pages[pageName];
		}
		
		public function addTabPage(buttonName:String, page:DisplayObject, visiable:Boolean = false):void
		{
			var button:InteractiveObject = _container.getChildByName(buttonName) as InteractiveObject;
			if (button == null)
			{
				//Log.error("Can't find button by name:", buttonName);
				return;
			}
			button.addEventListener(MouseEvent.CLICK, handleButtonClick);
			_tabButtons.addRadioButton(button, _tabButtons.numButtons);
			_pages[buttonName] = page;
			
			page.visible = visiable;
			_container.addChild(page);
		}
		
		private function handleButtonClick(e:MouseEvent):void 
		{
			currentPageName = e.currentTarget.name;
		}
	}

}