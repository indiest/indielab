package me.sangtian.common.ui 
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class ProgressBar
	{
		private var _container:Object;
		private var _bar:Object;
		private var _percentage:Number = 1;
		
		public function get percentage():Number 
		{
			return _percentage;
		}
		
		public function set percentage(value:Number):void 
		{
			_percentage = value;
			_bar.scaleX = value;
		}
		
		public function ProgressBar(bar:Object, container:Object = null) 
		{
			_bar = bar;
			_container = container || bar.parent;
		}
	}

}