package me.sangtian.common.console 
{
	import com.junkbyte.console.Cc;
	import com.junkbyte.console.KeyBind;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.ObjectPool;
	/**
	 * ...
	 * @author 
	 */
	public class BitmapInspector extends Sprite
	{
		//private static var _instance:BitmapInspector = new BitmapInspector();
		private static var _instancePool:ObjectPool = new ObjectPool(3, BitmapInspector);
		private var _bitmap:Bitmap = new Bitmap();
		private var _buttonClose:Sprite = new Sprite();
		private var _textCaption:TextField = new TextField();
		
		public function BitmapInspector() 
		{
			//if (_instance == null)
			{
				//Cc.instance.addChild(this);
				_bitmap.pixelSnapping = PixelSnapping.NEVER;
				_bitmap.smoothing = false;
				addChild(_bitmap);
				_buttonClose.graphics.lineStyle(3, 0xcccccc);
				_buttonClose.graphics.moveTo( -8, -8);
				_buttonClose.graphics.lineTo(8, 8);
				_buttonClose.graphics.moveTo( 8, -8);
				_buttonClose.graphics.lineTo( -8, 8);
				_buttonClose.buttonMode = true;
				_buttonClose.useHandCursor = true;
				addChild(_buttonClose);
				_buttonClose.addEventListener(MouseEvent.CLICK, handleClose);
				_textCaption.textColor = 0xcccccc;
				_textCaption.mouseEnabled = false;
				addChild(_textCaption);
				addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
				addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
				addEventListener(MouseEvent.MOUSE_WHEEL, handleMouseWheel);
				addEventListener(MouseEvent.DOUBLE_CLICK, handleDoubleClick);
			}
		}
		
		private function handleClose(e:MouseEvent):void 
		{
			hide();
		}
		
		private function handleDoubleClick(e:MouseEvent):void 
		{
			scaleX = scaleY = 1;
		}
		
		private function handleMouseWheel(e:MouseEvent):void 
		{
			var scale:Number = scaleX + e.delta / 30;
			scaleX = scaleY = scale;
		}
		
		private function handleMouseDown(e:MouseEvent):void 
		{
			if (e.buttonDown)
				startDrag();
		}
		
		private function handleMouseUp(e:MouseEvent):void 
		{
			stopDrag();
		}
		
		private function show(bitmapData:BitmapData, caption:String = null):void 
		{
			_bitmap.bitmapData = bitmapData.clone();
			//_bitmap.x = -_bitmap.width >> 1;
			//_bitmap.y = -_bitmap.height >> 1;
			_buttonClose.x = _bitmap.width;
			_textCaption.width = _bitmap.width;
			_textCaption.text = caption;
			graphics.clear();
			graphics.lineStyle(1, 0xffffff);
			graphics.drawRect(_bitmap.x - 1, _bitmap.y - 1, _bitmap.x + _bitmap.width + 2, _bitmap.y + _bitmap.height + 2);
			//visible = true;
			Cc.instance.addChild(this);
		}
		
		private function hide():void
		{
			//visible = false;
			Cc.instance.removeChild(this);
			stopDrag();
			_bitmap.bitmapData.dispose();
			_instancePool.returnObject(this);
		}
		
		public static function inspect(obj:*, caption:String = null):void
		{
			Cc.instance.refs.focus(obj);
			keyCallback(caption);
		}
		
		public static function hideAll():void 
		{
			_instancePool.returnAll(hideOne);
		}
		
		static private function hideOne(instance:BitmapInspector):void 
		{
			instance.hide();
		}
		
		public static function bindShowKey(key:KeyBind):void
		{
			Cc.bindKey(key, keyCallback);
		}
		
		public static function bindHideAllKey(key:KeyBind):void
		{
			Cc.bindKey(key, hideAll);
		}
		
		private static function keyCallback(caption:String = null):void
		{
			var current:* = Cc.instance.refs.current;
			var bitmapData:BitmapData;
			if (current is Bitmap)
			{
				bitmapData = (current as Bitmap).bitmapData;
			}
			else if (current is BitmapData)
			{
				bitmapData = current;
			}
			else
			{
				return;
			}
			var instance:BitmapInspector = _instancePool.borrowObject();
			if (instance == null)
				return;
			instance.show(bitmapData, caption);
		}
	}

}