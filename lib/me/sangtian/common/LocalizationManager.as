package me.sangtian.common 
{
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.logging.Logger;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.ObjectUtil;
	import flash.display.DisplayObject;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author Nicolas
	 */
	public class LocalizationManager 
	{
		private static const logger:Logger = Logger.create(LocalizationManager);

		public static const DEFAULT_LANGUAGE:String = "en";
	
		//private static var _strings:Object;
		//private static var _arrays:Object;
		//private static var _uiBindings:Object;
		private static var _locales:Object = { };
		private static var _language:String = DEFAULT_LANGUAGE;
		static public function get language():String 
		{
			return _language;
		}
		
		static public function set language(value:String):void 
		{
			_language = value;
		}
		
		public static function loadLocale(data:Object):void 
		{
			//_strings = lang["strings"];
			//_arrays = lang["arrays"];
			//_uiBindings = lang["uiBindings"];
			var locale:Object = _locales[data.language] || {};
			locale.strings = ObjectUtil.merge(locale.strings, data.strings, locale.strings);
			locale.arrays = ObjectUtil.merge(locale.arrays, data.arrays, locale.arrays);
			locale.uiBindings = ObjectUtil.merge(locale.uiBindings, data.uiBindings, locale.uiBindings);
			_locales[data.language] = locale;
		}
		
		public static function getRawString(key:String):String
		{
			var locale:Object = _locales[language];
			if (locale == null)
			{
				logger.warn("Missing locale:", language);
				locale = _locales[DEFAULT_LANGUAGE];
			}
			return locale.strings[key];
		}
		
		public static function getString(key:String, params:Array = null):String
		{
			var text:String = getRawString(key);
			if (text == null)
			{
				//Log.warn("Missing localization string:", key);
				text = key;
			}
			if (params && params.length > 0)
			{
				params.unshift(text);
				return printf.apply(null, params);
			}
			return text;
		}
		
		public static function getArray(key:String):Array
		{
			var locale:Object = _locales[language];
			if (locale == null)
			{
				logger.warn("Missing locale:", language);
				locale = _locales[DEFAULT_LANGUAGE];
			}
			var arr:Array = locale.arrays[key];
			if (arr == null || arr.length == 0)
			{
				logger.warn("Missing localization array:", key);
				return null;
			}
			return arr;
		}
		
		public static function getArrayItem(key:String, index:uint):*
		{
			var arr:Array = getArray(key);
			return arr == null ? null : arr[index];
		}

		private static function getUIBindingKey(obj:DisplayObject):String
		{
			if (obj.parent)
				return getQualifiedClassName(obj.parent) + "." + obj.name;
			else
				return obj.name;
		}
		
		public static function bindTextField(tf:TextField, properties:Object = null):void
		{
			var locale:Object = _locales[language];
			if (locale == null)
			{
				logger.warn("Missing locale:", language);
				locale = _locales[DEFAULT_LANGUAGE];
			}
			if (locale.uiBindings == null)
			{
				logger.info("No UI Binding defined in locale:", language);
				return;
			}
			properties = properties || locale.uiBindings[getUIBindingKey(tf)] || locale.uiBindings["_global"];
			tf.mouseEnabled = false;
			if (properties is String)
			{
				tf.text = properties as String;
				return;
			}
			
			for (var propName:String in properties)
			{
				if (propName == "textFormat")
				{
					var format:TextFormat = tf.defaultTextFormat;
					ObjectUtil.applyProperties(format, properties[propName]);
					tf.setTextFormat(format);
					tf.defaultTextFormat = format;
				}
				else
				{
					tf[propName] = properties[propName];
					if (logger.isDebugEnabled)
						logger.debug("Binding property:", tf.name + "." + propName + "=", properties[propName]);
				}
			}
		}
		
		private static function bindTextLocalization(obj:DisplayObject):void
		{
			if (obj is TextField)
				bindTextField(obj as TextField);
		}
		
		public static function bindAllTextFields(obj:DisplayObject):void 
		{
			DisplayObjectUtil.iterateAllChildren(obj, bindTextLocalization);
		}
	}

}