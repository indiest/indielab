package me.sangtian.common.game 
{
	import com.greensock.easing.*;
	import com.greensock.plugins.ShakeEffectPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.ui.Keyboard;
	import flash.utils.getDefinitionByName;
	import me.sangtian.common.input.InputManager;
	import me.sangtian.common.input.KeyboardController;
	import me.sangtian.common.input.KeyboardControllerKey;
	import me.sangtian.common.logging.Logger;
	import treefortress.sound.SoundManager;
CONFIG::air
{
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
}
	import flash.display.BitmapData;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.external.ExternalInterface;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.utils.Dictionary;
	import me.sangtian.common.Direction;
	import me.sangtian.common.logging.Log;
	import me.sangtian.common.state.StateManager;
	import me.sangtian.common.Time;
	import me.sangtian.common.util.ColorUtil;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
CONFIG::flash2d
{
	import flash.display.DisplayObject;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
}
CONFIG::starling
{
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
}
	
	/**
	 * Add these constants to the compiler:
		CONFIG::flash2d,true/false
		CONFIG::starling,!CONFIG::flash2d
	 *	
	 * @author Nicolas Tian
	 */
	public class BaseGame extends Sprite 
	{
		protected static const logger:Logger = new Logger("Game");
		
		private var _stageWidth:int;
		private var _stageHeight:int;
		private var _appId:String;
		protected var _appVersion:String;
		
		public var isMobile:Boolean;
		
		private var _keyboard:KeyboardController;
		public function get keyboard():KeyboardController
		{
			return _keyboard;
		}
		public function isKeyDown(keyCode:uint):Boolean
		{
			return _keyboard.isKeyDown(KeyboardControllerKey.fromKeyCode(keyCode));
		}
		public function isKeyUp(keyCode:uint):Boolean
		{
			return _keyboard.isKeyUp(KeyboardControllerKey.fromKeyCode(keyCode));
		}
		
		private var _input:InputManager = new InputManager();
		public function get input():InputManager 
		{
			return _input;
		}
		
		private var _stateLayer:Sprite;
		private var _stateManager:StateManager;
		public function get stateManager():StateManager 
		{
			return _stateManager;
		}
		
		public function get nativeStage():flash.display.Stage
		{
			CONFIG::flash2d
			{
				return stage;
			}
			CONFIG::starling
			{
				return Starling.current.nativeStage;
			}
		}
		
		public function get stageWidth():int
		{
			return _stageWidth;
		}
		
		public function get stageHeight():int
		{
			return _stageHeight;
		}
		
		private var _stageRect:Rectangle = new Rectangle();
		public function get stageRect():Rectangle
		{
			_stageRect.x = stage.x;
			_stageRect.y = stage.y;
			_stageRect.width = stageWidth;
			_stageRect.height = stageHeight;
			return _stageRect;
		}
		
		public function get focusingInput():Boolean
		{
			return nativeStage != null && nativeStage.focus is TextField &&
				(nativeStage.focus as TextField).type == TextFieldType.INPUT;
		}
		
		public function get appId():String 
		{
			return _appId;
		}
		
		public function get appVersion():String 
		{
			return _appVersion;
		}
		
		private var _sfx:SoundManager = new SoundManager();
		public function get sfx():SoundManager 
		{
			return _sfx;
		}
		
		private var _music:SoundManager = new SoundManager();
		public function get music():SoundManager 
		{
			return _music;
		}
		
		public function get soundOn():Boolean
		{
			return !_sfx.mute;
		}
		
		public function set soundOn(value:Boolean):void
		{
			_sfx.mute = !value;
		}
		
		public function BaseGame(stageWidth:int = 0, stageHeight:int = 0) 
		{
			_stageWidth = stageWidth;
			_stageHeight = stageHeight;
			
			CONFIG::web
			{
				try
				{
					var preloader:Object = getDefinitionByName("Preloader");
					_appId = preloader.appId;
					_appVersion = preloader.appVersion;
				}
				catch (err:Error) { }
				if (_appId == null)
				{
					_appId = ExternalInterface.objectID || loaderInfo.parameters["id"];
				}
				if (_appVersion == null)
				{
					_appVersion = loaderInfo.parameters["version"] || "0.0.0";
				}
			}
			CONFIG::air
			{
				var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
				var airNs:Namespace = appXml.namespaceDeclarations()[0];
				_appVersion = appXml.airNs::versionNumber;
				_appId = NativeApplication.nativeApplication.applicationID;
			}
			
			//var osPrefix:String = Capabilities.os.substr(0, 3).toUpperCase();
			var manufacturer:String = Capabilities.manufacturer.toUpperCase();
			isMobile = (manufacturer.indexOf("ANDROID") >= 0 || manufacturer.indexOf("IOS") >= 0);

			addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
		}
		
		protected function onPressBack():void
		{
		}
		
		protected function onPressHome():void
		{
		}
		
		protected function onPressEscape():void
		{
		}
		
		private function handleAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
			
			CONFIG::starling
			{
				if (nativeStage.displayState == StageDisplayState.NORMAL)
				{
					nativeStage.scaleMode = StageScaleMode.NO_SCALE;
					nativeStage.align = StageAlign.TOP_LEFT;
				}
				else
				{
					var xRatio:Number = nativeStage.fullScreenWidth / nativeStage.stageWidth;
					var yRatio:Number = nativeStage.fullScreenHeight/ nativeStage.stageHeight;
					logger.debug("Stage ratio(x, y):", xRatio, yRatio);
					var ratio:Number = Math.min(xRatio, yRatio);
					if (ratio != 1)
					{
						Starling.current.stage.stageWidth = nativeStage.stageWidth * ratio;
						Starling.current.stage.stageHeight= nativeStage.stageHeight* ratio;
					}
				}
			}

			if (_stageWidth == 0 || _stageHeight == 0)
			{
				CONFIG::flash2d
				{
					_stageWidth = stage.stageWidth;
					_stageHeight = stage.stageHeight;
				}
				CONFIG::starling
				{
					_stageWidth = Starling.current.stage.stageWidth;
					_stageHeight = Starling.current.stage.stageHeight;
				}
			}

			//Input.initialize(nativeStage);
			//Starling.current.simulateMultitouch = true;
			
			_stateLayer = new Sprite();
			_stateLayer.scrollRect = new Rectangle(0, 0, stageWidth, stageHeight);
			addChild(_stateLayer);
			_stateManager = new StateManager(_stateLayer);
			
			_keyboard = new KeyboardController(nativeStage);
			_input.addController(_keyboard);
			init();
			
			CONFIG::mobile
			{
				NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			}
			
			CONFIG::web
			{
				nativeStage.addEventListener(Event.DEACTIVATE, handleDeactivate);
				nativeStage.addEventListener(Event.ACTIVATE, handleActivate);
			}
			CONFIG::air
			{
				NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, handleDeactivate);
				NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, handleActivate);
				NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			}
			nativeStage.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
			addEventListener(Event.ENTER_FRAME, handleEnterFrame);
		}
		
		public var disableNonInputFocus:Boolean = true;
		protected function onStageMouseUp(e:MouseEvent):void 
		{
			if (!disableNonInputFocus)
				return;
			// De-focus any button or sprite after clicking it, otherwise Keyboard won't function properly
			if (!(e.target is TextField) || (e.target as TextField).type != TextFieldType.INPUT)
				nativeStage.focus = null;
		}
		
		private function handleActivate(e:Event):void 
		{
			onActivated();
		}
		
		private function handleDeactivate(e:Event):void 
		{
			CONFIG::mobile
			{
				if (_lastKeyCode != Keyboard.BACK)
				{
					// HOME key pressed
					onPressHome();
				}
			}
			onDeactivated();
		}
		
		protected var _lastKeyCode:uint
		private function handleKeyDown(e:KeyboardEvent):void 
		{
			_lastKeyCode = e.keyCode;
			if (e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				e.stopImmediatePropagation();
				onPressBack();
			}
			else if (e.keyCode == Keyboard.ESCAPE)
			{
				CONFIG::air
				{
					e.preventDefault();
					e.stopImmediatePropagation();
				}
				onPressEscape();
			}
		}
		
		protected function onActivated():void 
		{
			
		}
		
		protected function onDeactivated():void 
		{
		}
		
		protected function init():void
		{
			
		}
		
		public function isOnScreen(obj:DisplayObject):Boolean
		{
			return obj.getBounds(stage).intersects(stageRect);
		}
		
		public function isOutOfScreen(obj:DisplayObject, dir:uint = /*Direction.ALL*/15, offset:Number = 0):uint
		{
			var bounds:Rectangle = obj.getBounds(nativeStage);
			var result:uint = Direction.NONE;
			if ((dir & Direction.UP) != 0 && bounds.bottom < -offset)
				result |= Direction.UP;
			if ((dir & Direction.DOWN) != 0 && bounds.top > stageHeight + offset)
				result |= Direction.DOWN;
			if ((dir & Direction.LEFT) != 0 && bounds.right < -offset)
				result |= Direction.LEFT;
			if ((dir & Direction.RIGHT) != 0 && bounds.left > stageWidth + offset)
				result |= Direction.RIGHT;
			return result;
		}
		
		protected function initShakePlugin():void 
		{
			if (TweenLite._plugins[ShakeEffectPlugin.NAME] == null)
				TweenPlugin.activate([ShakeEffectPlugin]);
		}
		
		// Key as DisplayObject, value as TweenLite
		private var _shakeTweens:Dictionary = new Dictionary(true);
		// Use a timeline object to handle overlapped shaking
		// Key as DisplayObject, value as TimelineLite
		private var _shakeTimelines:Dictionary = new Dictionary(true);
		public function shake(target:DisplayObject, dx:Number, dy:Number, duration:Number, times:int, completeCallback:Function = null):void
		{
			//initShakePlugin();
			var vars:Object = {
				x: target.x - dx,
				y: target.y - dy,
				//shake: { x: dx, y: dy, numShakes: times },
				ease: RoughEase.ease.config({strength:1, points: times, template:Expo.easeInOut, randomize:false}),//Bounce.easeInOut,
				onComplete: handleShakeComplete,
				onCompleteParams: [target, target.x, target.y, completeCallback]
			};
			
			TweenLite.from(target, duration, vars);
			/* Try to re-use tween objects but failed: can't just re-set tween.vars
			var shakeTimeline:TimelineLite = _shakeTimelines[target];
			var shakeTween:TweenLite;
			if (shakeTimeline == null)
			{
				shakeTimeline = new TimelineLite();
				shakeTween = TweenLite.from(target, duration, vars);
				shakeTimeline.append(shakeTween);
				_shakeTweens[target] = shakeTween;
				_shakeTimelines[target] = shakeTimeline;
			}
			else
			{
				shakeTween = _shakeTweens[target];
				if (shakeTween._active)
					return;
				shakeTween.target = target;
				shakeTween.vars = vars;
				shakeTween.duration(duration);
			}
			shakeTimeline.restart();
			*/
		}
		
		private function handleShakeComplete(target:DisplayObject, originalX:Number, originalY:Number, completeCallback:Function):void 
		{
			target.x = originalX;
			target.y = originalY;
			if (completeCallback != null)
				completeCallback();
		}
		
		private function initCover(cover:DisplayObject, color:uint):DisplayObject
		{
			CONFIG::flash2d
			{
				if (cover == null)
					cover = new Bitmap(new BitmapData(stageWidth, stageHeight, true, 0xffffffff));
				DisplayObjectUtil.tint(cover, 0, color);
			}
			CONFIG::starling
			{
				if (cover == null)
					cover = new Image(Texture.fromColor(stageWidth, stageHeight, 0xffffffff));
				(cover as Image).color = color;
			}
			return cover;
		}
		
		private var _fadeCover:DisplayObject;
		public function fade(color:uint = 0xff000000, duration:Number = 1.0, completeCallback:Function = null, completeCallbackArgs:Array = null):void
		{
			_fadeCover = initCover(_fadeCover, color);
			var a:uint = ColorUtil.getAlpha(color);
			_fadeCover.alpha = (a == 0 ? 1 : 0);
			addChild(_fadeCover);
			
			TweenLite.killTweensOf(_fadeCover);
			TweenLite.to(_fadeCover, duration, {
				alpha: MathUtil.hexToNumber(a),
				onComplete: onFadeComplete, onCompleteParams: [completeCallback, completeCallbackArgs]
			} );
		}
		
		private function onFadeComplete(completeCallback:Function = null, completeCallbackArgs:Array = null):void
		{
			DisplayObjectUtil.removeFromParent(_fadeCover);
			if (completeCallback != null)
				completeCallback.apply(null, completeCallbackArgs);
		}
		
		private var _flashCover:DisplayObject;
		//private var _flashDuration:Number;
		public function flash(color:uint, duration:Number, times:uint, completeCallback:Function = null):void
		{
			//initShakePlugin();
			_flashCover = initCover(_flashCover, color);
			_flashCover.alpha = 0;
			addChild(_flashCover);
			//_flashDuration = duration;
			TweenLite.to(_flashCover, duration / times, {
				overwrite: 1,//Immediately overwrite
				//shake: {alpha: 1, numShakes: times },
				alpha: 1,
				repeat: times,
				onComplete: onFlashComplete, onCompleteParams: [completeCallback]
			} );
		}
		
		private function onFlashComplete(completeCallback:Function = null):void
		{
			DisplayObjectUtil.removeFromParent(_flashCover);
			if (completeCallback != null)
				completeCallback();
		}
		
		
		private var _flickingObjs:Dictionary = new Dictionary(true);
		public function flicker(target:DisplayObject, duration:Number, interval:Number = 0.2, completeCallback:Function = null):void
		{
			_flickingObjs[target] = {timing: interval, interval: interval, times: int(duration / interval), completeCallback: completeCallback };
		}
		private function updateFlickers():void
		{
			for (var key:* in _flickingObjs)
			{
				var target:DisplayObject = key as DisplayObject;
				var obj:Object = _flickingObjs[target];
				obj.timing -= Time.deltaSecond;
				if (obj.timing <= 0)
				{
					target.visible = !target.visible;
					obj.timing += obj.interval;
					obj.times--;
					if (obj.times == 0)
					{
						delete _flickingObjs[target];
						target.visible = true;
						if (obj.completeCallback)
							obj.completeCallback();
					}
				}
			}
		}
		
		public function zoom(scale:Number = 1, centerX:Number = 0, centerY:Number = 0, duration:Number = 0, completeCallback:Function = null):void 
		{
			TweenLite.to(this, duration, { x: -centerX, y: -centerY, scaleX: scale, scaleY: scale,
				ease: Circ.easeOut,
				onComplete: onZoomComplete,
				onCompleteParams: [completeCallback]
			});
		}
		private function onZoomComplete(completeCallback:Function = null):void
		{
			scaleX = scaleY = 1;
			x = y = 0;
			if (completeCallback != null)
				completeCallback();
		}
		
		public function focusInput(tf:TextField, selectAll:Boolean = true):void
		{
			nativeStage.focus = tf;
			if (selectAll)
				tf.setSelection(0, tf.length);
		}
		
		private function handleEnterFrame(e:Event):void 
		{
			Time.tick();
			//Input.update();
			input.update(Time.deltaSecond);			
			updateFlickers();
			_stateManager.update();
			update();
		}
		
		protected function update():void 
		{
			
		}
	}

}