package me.sangtian.common.game
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.System;
	import flash.utils.ByteArray;
	import me.sangtian.common.ProcedureProfiler;
	import me.sangtian.MyLib.CModule;
	
	/**
	 * ...
	 * @author 
	 */
	public class Shell extends Sprite 
	{
		private var _loader:Loader = new Loader();
		private var _data:ByteArray;
		private var _seed:uint;
		public var addLoader:Boolean = true;
		
		public function Shell(data:ByteArray, seed:uint):void 
		{
			_data = data;
			_seed = seed;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
 			processData(_data, _seed);
			//addChild(_loader);
			
			var context:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
			context.allowCodeImport = true;
			context.parameters = { };
			for (var key:String in loaderInfo.parameters)
			{
				//_loader.contentLoaderInfo.parameters[key] = loaderInfo.parameters[key];
				context.parameters[key] = loaderInfo.parameters[key];
			}
			//_loader.contentLoaderInfo.parameters["root"] = this.root;
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleLoadComplete);
			_loader.loadBytes(_data, context);
			if (addLoader)
				addChild(_loader);
		}
		
		private function handleLoadComplete(e:Event):void 
		{
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, handleLoadComplete);
			if (!addLoader)
			{
				addChild(_loader.content);
				_loader.unload();
				_loader = null;
			}
			_data.clear();
			_data = null;
			System.gc();
		}
		
		public static function processData(data:ByteArray, seed:uint):void 
		{
           //CModule.startAsync(this);
		   //trace(data.endian);
		   //data.endian = "littleEndian";
			var ptr:int = CModule.malloc(data.length);
			CModule.writeBytes(ptr, data.length, data);
			CONFIG::debug
			{
				trace("Checksum before encrypt:", MyLib.checkSum(ptr, data.length));
				ProcedureProfiler.Default.startTiming();
			}
			//data.position = 0;
			MyLib.encryptSwf(ptr, data.length, seed);
			CONFIG::debug
			{
				trace("Encrypt time:", ProcedureProfiler.Default.stopTiming());
				trace("Checksum after  encrypt:", MyLib.checkSum(ptr, data.length));
			}
			data.position = 0;
			CModule.readBytes(ptr, data.length, data);
			data.position = 0;
			CModule.free(ptr);
		}
		
	}
	
}