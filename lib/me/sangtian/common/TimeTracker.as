package me.sangtian.common 
{
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author tiansang
	 */
	public class TimeTracker 
	{
		private var _records:Dictionary = new Dictionary();
		
		public function TimeTracker() 
		{
			
		}
		
		public function startRecording(name:String):void
		{
			var record:Record = _records[name];
			if (record == null)
			{
				record = new Record();
				_records[name] = record;
			}
			record.startTime = getTimer();
		}
		
		public function endRecording(name:String, times:uint = 1):int
		{
			var record:Record = _records[name];
			var elapsed:int = getTimer() - record.startTime;
			record.times += times;
			record.totalTime += elapsed;
			return elapsed;
		}
		
		public function reset(name:String):void
		{
			var record:Record = _records[name];
			if (record == null)
				return;
			record.times = 0;
			record.totalTime = 0;
		}
		
		public function resetAll():void
		{
			for (var name:String in _records)
			{
				reset(name);
			}
		}
		
		public function toString():String
		{
			var str:String = "";
			for (var name:String in _records)
			{
				var record:Record = _records[name];
				str += name + ":" + record.times + "(" + record.totalTime + "ms)\n";
			}
			return str;
		}
	}

}

class Record
{
	public var times:uint;
	public var startTime:int;
	public var totalTime:int;
}