package me.sangtian.common.display 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.Sprite;
	import me.sangtian.common.util.DisplayObjectUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class DarknessContainer extends Sprite 
	{
		private var _darkness:Bitmap = new Bitmap();
		private var _darknessColor:uint;
		private var _lights:Vector.<Light> = new Vector.<Light>();
		
		public var showLights:Boolean = true;
		
		public function get isDarknessOn():Boolean
		{
			return _darkness.parent == this;
		}
		
		public function get numLights():uint
		{
			return _lights.length;
		}
		
		public function DarknessContainer() 
		{
			
		}
		
		public function showDarkness(width:uint, height:uint, color:uint = 0xff000000):void
		{
			_darknessColor = color;
			_darkness.bitmapData = new BitmapData(width, height, true, color);
			_darkness.blendMode = BlendMode.MULTIPLY;
			addChild(_darkness);
		}
		
		public function removeDarkness():void
		{
			DisplayObjectUtil.removeFromParent(_darkness);
		}
		
		public function addLight(source:DisplayObject, offsetX:int = 0, offsetY:int = 0, followObject:DisplayObject = null):void
		{
			var light:Light = new Light();
			light.source = source;
			light.offsetX = offsetX;
			light.offsetY = offsetY;
			light.source.x = offsetX;
			light.source.y = offsetY;
			light.followObject = followObject;
			_lights.push(light);
		}
		
		public function createPointLightSource(color:uint, radius:Number, brightness:Number = 1.0, attenuance:Number = 1.0):DisplayObject
		{
			var shape:Shape = new Shape();
			shape.graphics.beginGradientFill(GradientType.RADIAL, [color, 0x000000], [brightness, 0], [0, 255 * attenuance]);
			shape.graphics.drawCircle(0, 0, radius);
			shape.graphics.endFill();
			
			/* CPU 28~35%
			shape.cacheAsBitmap = true;
			return shape;
			*/
			
			/* CPU 22~30%
			*/
			var bitmap:Bitmap = new Bitmap(DisplayObjectUtil.drawToBitmapData(shape), "never");
			//return bitmap;
			bitmap.x = -shape.width >> 1;
			bitmap.y = -shape.height >> 1;
			var sprite:Sprite = new Sprite();
			sprite.addChild(bitmap);
			return sprite;
		}
		
		public function removeLight(source:DisplayObject):void
		{
			for (var i:int = 0; i < _lights.length; i++)
			{
				if (_lights[i].source == source)
				{
					_lights.splice(i, 1);
					break;
				}
			}
		}
		
		public function removeAllLights():void
		{
			_lights.length = 0;
		}
		
		public function update():void
		{
			if (!isDarknessOn)
				return;
				
			setChildIndex(_darkness, numChildren - 1);
			_darkness.bitmapData.fillRect(_darkness.bitmapData.rect, _darknessColor);
			
			if (!showLights)
				return;
				
			for each(var light:Light in _lights)
			{
				if (light.followObject)
				{
					light.source.x = light.followObject.x + light.offsetX;
					light.source.y = light.followObject.y + light.offsetY;
				}
				_darkness.bitmapData.draw(light.source, light.source.transform.matrix, null, BlendMode.SCREEN);
				//addChild(light.source);
			}
		}
	}

}
import flash.display.DisplayObject;

class Light
{
	public var source:DisplayObject;
	public var offsetX:int;
	public var offsetY:int;
	public var followObject:DisplayObject;
}