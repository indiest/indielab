package me.sangtian.common.display
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.filters.BlurFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import me.sangtian.common.ObjectPool;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class ShadowEffect 
	{
		private var _container:DisplayObjectContainer;
		private var _shadowPool:ObjectPool;
		private var _shadows:Vector.<Shadow> = new Vector.<Shadow>();
		//public var shadowOffsetX:Number = 0;
		//public var shadowOffsetY:Number = 0;
		
		public function ShadowEffect(container:DisplayObjectContainer, maxCount:uint = 0) 
		{
			_container = container;
			_shadowPool = new ObjectPool(maxCount, Shadow);
		}
		
		public function addShadow(target:DisplayObject, dx:Number, dy:Number, lifetime:Number, blur:Boolean = false):Shadow
		{
			//var bd:BitmapData = DisplayObjectUtil.drawToBitmapData(target);
			var bounds:Rectangle = target.getBounds(target);
			var bd:BitmapData = new BitmapData(
				//bounds.width + Math.abs(dx), bounds.height + Math.abs(dy), true, 0);
				bounds.width + 0.5, bounds.height + 0.5, true, 0);
			const drawBitmapMatrix:Matrix = new Matrix();
			drawBitmapMatrix.tx = -(bounds.x);// - dx);
			drawBitmapMatrix.ty = -(bounds.y);// - dy);
			bd.draw(target, drawBitmapMatrix);
			
			if (blur)
			{
				const destPoint:Point = new Point();
				const blurFilter:BlurFilter = new BlurFilter();
				blurFilter.blurX = dx;
				blurFilter.blurY = dy;
				bd.applyFilter(bd, bd.rect, destPoint, blurFilter);
			}
			var shadow:Shadow = _shadowPool.borrowObject();
			if (shadow != null)
			{
				shadow.lifetime = shadow.timing = lifetime;
				shadow.bitmapData = bd;
				shadow.x = target.x + bounds.x - dx;
				shadow.y = target.y + bounds.y - dy;
				_container.addChild(shadow);
				_shadows.push(shadow);
			}
			return shadow;
		}
		
		private function removeShadow(shadow:Bitmap):void
		{
			_container.removeChild(shadow);
			shadow.alpha = 1;
		}
		
		public function clearShadows():void
		{
			_shadowPool.returnAll(removeShadow);
			_shadows.length = 0;
		}
		
		public function update(deltaSecond:Number):void
		{
			for (var i:int = _shadows.length - 1; i >= 0; i--)
			{
				var shadow:Shadow = _shadows[i]
				shadow.alpha = shadow.timing / shadow.lifetime;
				shadow.timing -= deltaSecond;
				if (shadow.timing <= 0)
				{
					removeShadow(shadow);
					_shadows.splice(i, 1);
					_shadowPool.returnObject(shadow);
				}
			}
			
			if (_target != null)
				updateTracing();
		}
		
		private var _target:DisplayObject;
		private var _targetLastX:Number;
		private var _targetLastY:Number;
		private var _minDist:Number;
		private var _lifetime:Number;
		private var _blur:Boolean;
		public function get isTracing():Boolean
		{
			return _target != null;
		}
		
		public function startTracing(target:DisplayObject, minDist:Number, lifetime:Number, blur:Boolean = false):void
		{
			_target = target;
			_targetLastX = target.x;
			_targetLastY = target.y;
			_minDist = minDist;
			_lifetime = lifetime;
			_blur = blur;
		}
		
		public function stopTracing(clearAll:Boolean = false):void
		{
			_target = null;
			if (clearAll)
			{
				clearShadows();
			}
		}
		
		private function updateTracing():void
		{
			var dx:Number = _target.x - _targetLastX;
			var dy:Number = _target.y - _targetLastY;
			if (MathUtil.compareDistance(dx, dy, _minDist) > 0)
			{
				addShadow(_target, dx, dy, _lifetime, _blur);
				_targetLastX = _target.x;
				_targetLastY = _target.y;
			}
			
		}
	}

}
import flash.display.Bitmap;
import flash.display.BitmapData;

class Shadow extends Bitmap
{
	public var lifetime:Number;
	public var timing:Number;
	public var startAlpha:Number;
	public function Shadow()
	{
		super();
	}
}