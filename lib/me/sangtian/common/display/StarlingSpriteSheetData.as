package me.sangtian.common.display 
{
	import me.sangtian.common.util.ObjectUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class StarlingSpriteSheetData 
	{
		private var _frames:Vector.<StarlingSpriteSheetDataFrame> = new Vector.<StarlingSpriteSheetDataFrame>();
		private var _maxWidth:int;
		private var _maxHeight:int;
		
		public function get frames():Vector.<StarlingSpriteSheetDataFrame> 
		{
			return _frames;
		}
		
		public function get maxWidth():int 
		{
			return _maxWidth;
		}
		
		public function get maxHeight():int 
		{
			return _maxHeight;
		}
		
		public function StarlingSpriteSheetData() 
		{
			
		}
		
		public static function parse(text:String):StarlingSpriteSheetData
		{
			var data:StarlingSpriteSheetData = new StarlingSpriteSheetData();
			var xml:XML = new XML(text);
			for each(var element:* in xml.SubTexture)
			{
				var frame:StarlingSpriteSheetDataFrame = new StarlingSpriteSheetDataFrame();
				ObjectUtil.applyAttributes(frame, element);
				data.frames.push(frame);
				if (frame.frameWidth > data._maxWidth)
					data._maxWidth = frame.frameWidth;
				if (frame.frameHeight > data._maxHeight)
					data._maxHeight = frame.frameHeight;
			}
			return data;
		}
		
		public static function create(frameWidth:int, frameHeight:int, imageWidth:int, imageHeight:int):StarlingSpriteSheetData
		{
			var data:StarlingSpriteSheetData = new StarlingSpriteSheetData();
			var cols:int = Math.ceil(imageWidth / frameWidth);
			var rows:int = Math.ceil(imageHeight / frameHeight);
			for (var y:int = 0; y < rows; y++)
			{
				for (var x:int = 0; x < cols; x++)
				{
					var frameData:StarlingSpriteSheetDataFrame = new StarlingSpriteSheetDataFrame();
					frameData.x = x * frameWidth;
					frameData.y = y * frameHeight;
					frameData.width = frameData.frameWidth = frameWidth;
					frameData.height = frameData.frameHeight = frameHeight;
					data.frames.push(frameData);
				}
			}
			data._maxWidth = frameWidth;
			data._maxHeight = frameHeight;
			return data;
		}
	}

}