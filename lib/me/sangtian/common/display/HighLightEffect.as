package me.sangtian.common.display 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.MathUtil;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class HighLightEffect extends Sprite 
	{
		public var maskHeight:Number = 2;
		public var moveArea:Rectangle;
		public var moveSpeedX:Number = 0;
		public var moveSpeedY:Number = -32;
		
		private var _highlightedBitmap:Bitmap;
		private var _mask:Shape;
		
		public function get highlightedBitmap():Bitmap 
		{
			return _highlightedBitmap;
		}
		
		public function HighLightEffect(target:DisplayObject) 
		{
			_highlightedBitmap = new Bitmap(DisplayObjectUtil.drawToBitmapData(target));
			DisplayObjectUtil.tint(_highlightedBitmap, 1, 0x888888);
			_highlightedBitmap.cacheAsBitmap = true;
			addChild(_highlightedBitmap);
			
			_mask = new Shape();
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(_highlightedBitmap.width, maskHeight, Math.PI * 0.5);
			_mask.graphics.beginGradientFill("linear", [0, 0, 0], [0, 1, 0], [0, 128, 255], matrix);
			_mask.graphics.drawRect(0, 0, _highlightedBitmap.width, maskHeight);
			_mask.graphics.endFill();
			_mask.cacheAsBitmap = true;
			addChild(_mask);
			
			moveArea ||= _highlightedBitmap.getBounds(this);
			_highlightedBitmap.mask = _mask;
		}
		
		public function update(deltaSecond:Number):void
		{
			_mask.x = MathUtil.wrap(_mask.x + moveSpeedX * deltaSecond, moveArea.x - _mask.width, moveArea.x + moveArea.width);
			_mask.y = MathUtil.wrap(_mask.y + moveSpeedY * deltaSecond, moveArea.y - _mask.height, moveArea.y + moveArea.height);
			/*
			_mask.x += moveSpeedX * deltaSecond;
			_mask.y += moveSpeedY * deltaSecond;
			if (_mask.x > moveArea.x + moveArea.width)
				_mask.x = moveArea.x - _mask.width;
			else if (mask.x < moveArea.x - _mask.width)
				_mask.x = moveArea.x + moveArea.width;
			*/
		}
		
		public function reset():void
		{
			_mask.x = _mask.y = 0;
		}
	}

}