package me.sangtian.common.display 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.filters.DisplacementMapFilter;
	import flash.filters.DisplacementMapFilterMode;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class MirrorEffect extends Sprite 
	{
		// No reflection
		public static const AXIS_NONE:uint = 0;
		// Reflect up side down
		public static const AXIS_X:uint = 1;
		// Reflect left side right
		public static const AXIS_Y:uint = 2;
		
		private var _reflectionAxis:uint;
		private var _displacementAxis:uint;
		private var _bitmapDataMirrored:BitmapData;
		private var _bitmap:Bitmap;
		private var _perlinMap:BitmapData;
		private var _displacementMap:BitmapData;
		private var _filter:DisplacementMapFilter;
		private var _offset:Number = 0;
		private var _regenMap:Boolean = false;
		private var _updateTimer:Number = 0;
		
		public var updateInterval:Number = 0;
		public var speed:Number = 20;

		/* Generally these values are fixed as mirroring should always use 200,20.
		private var _perlinBaseX:Number = 200;
		public function get perlinBaseX():Number 
		{
			return _perlinBaseX;
		}
		public function set perlinBaseX(value:Number):void 
		{
			_perlinBaseX = value;
			_regenMap = true;
		}
		
		private var _perlinBaseY:Number = 20;
		public function get perlinBaseY():Number 
		{
			return _perlinBaseY;
		}
		public function set perlinBaseY(value:Number):void 
		{
			_perlinBaseY = value;
			_regenMap = true;
		}
		*/
		
		public function get displacementAxis():uint 
		{
			return _displacementAxis;
		}
		
		public function set displacementAxis(value:uint):void 
		{
			_displacementAxis = value;
			_regenMap = true;
		}
		
		//private var _displaceChannelX:uint = BitmapDataChannel.RED;
		public function get displaceChannelX():uint 
		{
			return _filter.componentX;
		}
		public function set displaceChannelX(value:uint):void 
		{
			_filter.componentX = value;
		}
		
		//private var _displaceChannelY:uint = BitmapDataChannel.RED;
		public function get displaceChannelY():uint 
		{
			return _filter.componentY;
		}
		public function set displaceChannelY(value:uint):void 
		{
			_filter.componentY = value;
		}
		
		//private var _displaceScaleX:Number = 40;
		public function get displaceScaleX():Number 
		{
			return _filter.scaleX;
		}
		public function set displaceScaleX(value:Number):void 
		{
			_filter.scaleX = value;
		}
		
		//private var _displaceScaleY:Number = 5;
		public function get displaceScaleY():Number 
		{
			return _filter.scaleY;
		}
		public function set displaceScaleY(value:Number):void 
		{
			_filter.scaleY = value;
		}
		
		public function MirrorEffect(target:DisplayObject, reflectionAxis:uint = AXIS_X, displacementAxis:uint = AXIS_X) 
		{
			_reflectionAxis = reflectionAxis;
			_displacementAxis = displacementAxis;
			var bounds:Rectangle = target.getBounds(target);
			_bitmapDataMirrored = new BitmapData(bounds.width + 0.5, bounds.height + 0.5, true, 0);
			_bitmap = new Bitmap(new BitmapData(_bitmapDataMirrored.width, _bitmapDataMirrored.height, true, 0));
			addChild(_bitmap);

			_perlinMap = new BitmapData(_bitmapDataMirrored.width, _bitmapDataMirrored.height, false);
			generateMap();
			
			_filter = new DisplacementMapFilter(_displacementMap, null, 
				BitmapDataChannel.RED, BitmapDataChannel.RED, 
				10, 10, 
				DisplacementMapFilterMode.CLAMP);
			
			setTarget(target);
		}
		
		public function setTarget(target:DisplayObject):void
		{
			var bounds:Rectangle = target.getBounds(target);
			var drawMatrix:Matrix;
			if (_reflectionAxis == AXIS_X)
			{
				drawMatrix = new Matrix(1, 0, 0, -1, -bounds.x, -bounds.y + target.height);
			}
			else if (_reflectionAxis == AXIS_Y)
			{
				drawMatrix = new Matrix( -1, 0, 0, 1, -bounds.x + target.width, -bounds.y);
			}
			else
			{
				drawMatrix = new Matrix();
			}
			_bitmapDataMirrored.fillRect(_bitmapDataMirrored.rect, 0);
			_bitmapDataMirrored.draw(target, drawMatrix);
			
			_bitmap.bitmapData.fillRect(_bitmap.bitmapData.rect, 0);
			offset(0);
		}
		
		private static const EMPTY_POINT:Point = new Point();
		private var _drawPoint:Point = new Point();
		private var _mapPoint:Point = new Point();
		
		private function generateMap():void
		{
			_perlinMap.fillRect(_perlinMap.rect, 0);
			_perlinMap.perlinNoise(200, 20, 2, 5, true, true, 0, true);

			if (displacementAxis == AXIS_X)
			{
				_displacementMap = new BitmapData(_bitmapDataMirrored.width, _bitmapDataMirrored.height * 2, false);
			}
			else if (displacementAxis == AXIS_Y)
			{
				_displacementMap = new BitmapData(_bitmapDataMirrored.width * 2, _bitmapDataMirrored.height, false);
			}
			else
			{
				_displacementMap = new BitmapData(_bitmapDataMirrored.width, _bitmapDataMirrored.height, false);
			}
			
			_displacementMap.copyPixels(_perlinMap, _perlinMap.rect, EMPTY_POINT);
			if (_displacementAxis == AXIS_X)
			{
				_drawPoint.setTo(0, _perlinMap.height);
			}
			else if (_displacementAxis == AXIS_Y)
			{
				_drawPoint.setTo(_perlinMap.width, 0);
			}
			else
			{
				return;
			}
			_displacementMap.copyPixels(_perlinMap, _perlinMap.rect, _drawPoint);
		}
		
		private function draw():void 
		{
			_bitmap.bitmapData.applyFilter(_bitmapDataMirrored, _bitmapDataMirrored.rect, EMPTY_POINT, _filter);
			//var rect:Rectangle = new Rectangle(0, 0, _bitmap.width, _bitmap.height);
			//rect.x = _mapPoint.x;
			//rect.y = _mapPoint.y + _bitmap.height;
			//_bitmap.bitmapData.copyPixels(_displacementMap, rect, EMPTY_POINT);
		}
		
		public function update(deltaSecond:Number):void
		{
			/*
			*/
			if (_updateTimer < updateInterval)
			{
				_updateTimer += deltaSecond;
				if (_updateTimer >= updateInterval)
				{
					_updateTimer -= updateInterval;
				}
				else
				{
					return;
				}
			}
			
			if (_regenMap)
			{
				generateMap();
				_regenMap = false;
			}
			
			offset(speed * deltaSecond);
		}
		
		public function offset(value:Number):void
		{
			if (_displacementAxis == AXIS_X)
			{
				_offset = MathUtil.wrap(_offset + value, 0, _bitmap.height);
				_mapPoint.setTo(0, _offset - _bitmap.height);
			}
			else if (_displacementAxis == AXIS_Y)
			{
				_offset = MathUtil.wrap(_offset + value, 0, _bitmap.width);
				_mapPoint.setTo(_offset - _bitmap.width, 0);
			}
			else
			{
				return;
			}
			_filter.mapPoint = _mapPoint;
			draw();
		}
		
		public function dispose():void
		{
			_bitmapDataMirrored.dispose();
			_displacementMap.dispose();
			_perlinMap.dispose();
		}
	}

}