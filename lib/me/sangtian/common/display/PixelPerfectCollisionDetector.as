package me.sangtian.common.display
{
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import me.sangtian.common.console.BitmapInspector;
	import me.sangtian.common.util.DisplayObjectUtil;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class PixelPerfectCollisionDetector 
	{
		private static const DEFAULT_POS:Point = new Point();
		
		private var _obj:DisplayObject;
		private var _objBitmapData:BitmapData;
		private var _diffBitmapData:BitmapData;
		
		public var objAlphaThreshold:uint = 1;
		public var targetAlphaThreshold:uint = 1;
		
		/**
		 * Constructor.
		 * 
		 * @param	obj The DisplayObject used for detecting collision from other objects.
		 * @param	canvasWidth The width of the whole displaying area. It's for creating BitmapData buffer which is used for drawing objects onto. Generally it's equal to the stage width.
		 * @param	canvasHeight The height of the whole displaying area. It's for creating BitmapData buffer which is used for drawing objects onto. Generally it's equal to the stage height.
		 * @param	objBitmapData The BitmapData of the DisplayObject. If the DisplayObject never scales or rotates, preset it's BitmapData can speed up the detection.
		 */
		public function PixelPerfectCollisionDetector(obj:DisplayObject, canvasWidth:int, canvasHeight:int, objBitmapData:BitmapData = null) 
		{
			_obj = obj;
			_objBitmapData = objBitmapData;
			_diffBitmapData = new BitmapData(canvasWidth, canvasHeight, false);
		}
		
		public function hitTestObject(target:DisplayObject):Rectangle
		{
			if (_obj.hitTestObject(target))
			{
				var bitmapData:BitmapData = DisplayObjectUtil.drawToBitmapData(target);
				//BitmapInspector.inspect(bitmapData, "Target");
				return hitTestBitmap(bitmapData, target.getBounds(target.parent).topLeft);
			}
			return null;
		}
		
		public function hitTestBitmap(bitmapData:BitmapData, pos:Point = null):Rectangle
		{
			var objBitmapData:BitmapData = _objBitmapData || DisplayObjectUtil.drawToBitmapData(_obj);
			var objBounds:Rectangle = _obj.getBounds(_obj.parent);
			var objHitTestPoint:Point = objBounds.topLeft;
			var targetPos:Point = pos || DEFAULT_POS;
			if (objBitmapData.hitTest(objHitTestPoint, objAlphaThreshold, bitmapData, targetPos, targetAlphaThreshold))
			{
				_diffBitmapData.fillRect(_diffBitmapData.rect, 0);
				_diffBitmapData.copyPixels(bitmapData, objBounds, objHitTestPoint);
				//BitmapInspector.inspect(_diffBitmapData, "Copy collision");
				_diffBitmapData.draw(_obj, _obj.transform.matrix, null, BlendMode.SUBTRACT);
				//BitmapInspector.inspect(_diffBitmapData, "Draw substract");
				_diffBitmapData.draw(bitmapData, null, null, BlendMode.DIFFERENCE, objBounds, false);
				//BitmapInspector.inspect(_diffBitmapData, "Draw difference");
				return _diffBitmapData.getColorBoundsRect(0xffffff, 0, false);
			}
			return null;
		}
	}

}