package me.sangtian.common.display 
{
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.Sprite;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.Random;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class StarsEffect extends Sprite 
	{
		private static const DRAW_RADIUS:Number = 100;
		private static const RADIUS_SCALE:Number = 1 / DRAW_RADIUS;
		
		private var _bdStar:BitmapData;
		private var _stars:Vector.<Star>;
		
		public function get numStars():uint
		{
			return _stars.length;
		}
		
		public function set numStars(value:uint):void
		{
			_stars.length = value;
		}
		
		public var canvasWidth:uint;
		public var canvasHeight:uint;
		
		public var minRadius:Number = 1;
		public var maxRadius:Number = 3;
		
		public var velocityX:Number = 0;
		public var velocityY:Number = 0;
		
		public function StarsEffect(canvasWidth:uint, canvasHeight:uint, numStars:uint, starInnerColor:uint = 0xffffff, starOuterColor:uint = 0x000000) 
		{
			_stars = new Vector.<Star>(numStars, true);
			
			var circle:Shape = new Shape();
			circle.graphics.beginGradientFill(GradientType.RADIAL, [starInnerColor, starOuterColor], [1, 1], [0, 255]);
			circle.graphics.drawCircle(0, 0, DRAW_RADIUS);
			circle.graphics.endFill();
			_bdStar = DisplayObjectUtil.drawToBitmapData(circle);
			
			for (var i:uint = 0; i < numStars; i++)
			{
				var star:Star = new Star(_bdStar);
				_stars[i] = star;
			}
			
			this.canvasWidth = canvasWidth;
			this.canvasHeight = canvasHeight;
			
			this.cacheAsBitmap = true;
		}
		
		public function update(deltaSecond:Number):void
		{
			for each(var star:Star in _stars)
			{
				if (star.parent == null)
				{
					star.scaleX = star.scaleY = Random.rangeNumber(minRadius * RADIUS_SCALE, maxRadius * RADIUS_SCALE);
					star.x = Random.rangeNumber(0, canvasWidth);
					star.y = Random.rangeNumber(0, canvasHeight);
					star.alpha = 0;
					star.alphaSign = 1;
					addChild(star);
					// One star a time
					break;
				}
				else
				{
					star.alpha += star.alphaSign * deltaSecond * Random.value;
					if (star.alpha > 1)
					{
						star.alphaSign = -1;
					}
					if (star.alpha < 0)
					{
						removeChild(star);
					}
					star.x += velocityX * deltaSecond;
					star.y += velocityY * deltaSecond;
				}
			}
		}
	}

}
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.PixelSnapping;
import flash.display.Shape;

class Star extends Bitmap
{
	public var alphaSign:int;
	
	public function Star(bd:BitmapData)
	{
		super(bd, PixelSnapping.NEVER, false);
	}
}