package me.sangtian.common.display 
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class AlignmentEngine 
	{
		public static const ALIGNMENT_TYPE_NEGATIVE:int = -1;
		public static const ALIGNMENT_TYPE_CENTER:int = 0;
		public static const ALIGNMENT_TYPE_POSITIVE:int = 1;
		
		private var _container:DisplayObjectContainer;
		private var _canvasWidth:Number;
		private var _canvasHeight:Number;
		
		public function AlignmentEngine(container:DisplayObjectContainer, canvasWidth:Number, canvasHeight:Number) 
		{
			_container = container;
			_canvasWidth = canvasWidth;
			_canvasHeight = canvasHeight;
		}
		
		public function alignX(child:DisplayObject, type:int, margin:Number = 0):void
		{
			if (type == ALIGNMENT_TYPE_NEGATIVE)
				child.x = 0;
			else if (type == ALIGNMENT_TYPE_CENTER)
				child.x = _canvasWidth >> 1;
			else
				child.x = _canvasWidth;
			child.x -= type * margin;
		}
		
		public function alignY(child:DisplayObject, type:int, margin:Number = 0):void
		{
			if (type == ALIGNMENT_TYPE_NEGATIVE)
				child.y = 0;
			else if (type == ALIGNMENT_TYPE_CENTER)
				child.y = _canvasHeight >> 1;
			else
				child.y = _canvasHeight;
			child.y -= type * margin;
		}
		
		public function align(child:DisplayObject, typeX:int, typeY:uint, marginX:Number = 0, marginY:Number = 0):void
		{
			alignX(child, typeX, marginX);
			alignY(child, typeY, marginY);
		}
		
		public function center(child:DisplayObject):void
		{
			align(child, ALIGNMENT_TYPE_CENTER, ALIGNMENT_TYPE_CENTER);
		}
		
		private var _autoAlignmentX:int;
		private var _autoAlignmentY:int;
		private var _autoMarginX:Number;
		private var _autoMarginY:Number;
		public function enableAutoAlignment(typeX:int, typeY:int, marginX:Number = 0, marginY:Number = 0):void
		{
			_autoAlignmentX = typeX;
			_autoAlignmentY = typeY;
			_autoMarginX = marginX;
			_autoMarginY = marginY;
			_container.addEventListener(Event.ADDED, handleChildAdded);
		}
		
		public function disableAutoAlignment():void
		{
			_container.removeEventListener(Event.ADDED, handleChildAdded);
		}
		
		private function handleChildAdded(e:Event):void 
		{
			var child:DisplayObject = e.target as DisplayObject;
			if (child.parent != _container)
				return;
			align(child, _autoAlignmentX, _autoAlignmentY, _autoMarginX, _autoMarginY);
		}
		
	}

}