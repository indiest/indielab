package me.sangtian.common.display 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import me.sangtian.common.util.DisplayObjectUtil;
	import me.sangtian.common.util.ObjectUtil;
	
	[Event(name="complete", type="flash.events.Event")] 
	/**
	 * Implement Starling sprite sheet rendering in Flash 2D
	 * 
	 * @author Nicolas Tian
	 */
	public class StarlingSpriteSheet extends Sprite//Bitmap
	{
		private var _data:StarlingSpriteSheetData;
		private var _image:Bitmap;
		private var _currentFrame:int = -1;
		
		public var playOnce:Boolean;
		
		public function get currentFrame():int 
		{
			return _currentFrame;
		}
		
		public function set currentFrame(value:int):void 
		{
			if (value < 0 || value >= totalFrames)
				return;
			_currentFrame = value;
			showFrame(value);
		}
		
		public function get totalFrames():int
		{
			return _data.frames.length;
		}
		
		public function get maxWidth():Number 
		{
			return _data.maxWidth * scaleX;
		}
		
		public function get maxHeight():Number 
		{
			return _data.maxHeight * scaleY;
		}
		
		public function get isPlaying():Boolean 
		{
			return _isPlaying;
		}
		
		public function get frameRate():Number 
		{
			return _frameRate;
		}
		
		public function set frameRate(value:Number):void 
		{
			_frameRate = value;
		}
		
		override public function get width():Number 
		{
			return _image.scrollRect.width;
		}
		
		override public function get height():Number 
		{
			return _image.scrollRect.height;
		}

		public function StarlingSpriteSheet(bd:BitmapData, data:StarlingSpriteSheetData)
		{
			_image = new Bitmap(bd);
			_data = data;
			addChild(_image);
			currentFrame = 0;
		}
		
		private function showFrame(index:int):void
		{
			var frame:StarlingSpriteSheetDataFrame = _data.frames[index];
			_image.x = -frame.frameX;
			_image.y = -frame.frameY;
			var scrollRect:Rectangle = _image.scrollRect || new Rectangle();
			scrollRect.setTo(frame.x, frame.y, frame.width, frame.height);
			_image.scrollRect = scrollRect;
		}
		
		private var _rectHelper:Rectangle = new Rectangle();
		private var _pointHelper:Point = new Point();
		public function getCurrentFrameBitmapData(target:BitmapData = null):BitmapData
		{
			var frame:StarlingSpriteSheetDataFrame = _data.frames[currentFrame];
			target = target || new BitmapData(frame.width, frame.height);
			_rectHelper.setTo(frame.x, frame.y, frame.width, frame.height);
			_pointHelper.setTo(-frame.frameX, -frame.frameY);
			target.copyPixels(_image.bitmapData, _rectHelper, _pointHelper);
			return target;
		}
		
		public function getFrameWidth(frame:int):int
		{
			return _data.frames[frame].width;
		}
		
		public function getFrameHeight(frame:int):int
		{
			return _data.frames[frame].height;
		}
		
		private var _isPlaying:Boolean = false;
		public function playFixed(startFrame:int = 0):void
		{
			if (!hasEventListener(Event.ENTER_FRAME))
			{
				addEventListener(Event.ENTER_FRAME, stepFrame);
			}
			play(startFrame);
		}
		
		public function play(startFrame:int = 0):void
		{
			currentFrame = startFrame;
			_isPlaying = true;
		}
		
		private var _frameRate:Number;
		private var _frameTimer:Number = 0;
		public function updateFrame(deltaSecond:Number):void
		{
			if (!isPlaying)
				return;
			var fps:Number = frameRate || stage.frameRate;
			var frameSecond:Number = 1 / fps;
			_frameTimer += deltaSecond;
			if (_frameTimer >= frameSecond)
			{
				_frameTimer -= frameSecond;
				stepFrame();
			}
		}
		
		private function stepFrame(e:Event = null):void 
		{
			if (currentFrame == totalFrames - 1)
			{
				currentFrame = 0;
				dispatchEvent(new Event(Event.COMPLETE));
				if (playOnce)
				{
					stop();
					DisplayObjectUtil.removeFromParent(this);
				}
			}
			else
			{
				currentFrame++;
			}
		}
		
		public function stop():void
		{
			_isPlaying = false;
			currentFrame = 0;
			removeEventListener(Event.ENTER_FRAME, stepFrame);
		}
	}
}

