package me.sangtian.common.display
{
	import flash.display.Bitmap;
	import flash.display.PixelSnapping;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class DualImageWrappingScroller extends WrappingScroller
	{
		private var _image1:Bitmap;
		private var _image2:Bitmap;
		private var _scrollRect:Rectangle;
		private var _overlap:Boolean;
		private var _overlapX:Number;
		private var _overlapY:Number;
		private var _origin1:Point;
		private var _origin2:Point;
		
		public function DualImageWrappingScroller(image1:Bitmap, image2:Bitmap = null, scrollRect:Rectangle = null, overlap:Boolean = true) 
		{
			//ASSERT(image1.parent == image2.parent);
			_image1 = image1;
			_image2 = image2;
			if (_image2 == null)
			{
				_image2 = new Bitmap(image1.bitmapData);
				_image2.y = _image1.y + _image1.height;
			}
			_image1.pixelSnapping = PixelSnapping.NEVER;
			_image2.pixelSnapping = PixelSnapping.NEVER;
			_scrollRect = scrollRect || _image1.bitmapData.rect;
			_overlap = overlap;
			if (overlap)
			{
				_overlapX = _image1.x + _image1.width - _image2.x;
				_overlapY = _image1.y + _image1.height - _image2.y;
			}
			else
			{
				_overlapX = 0;
				_overlapY = 0;
			}
			_origin1 = new Point(_image1.x, _image1.y);
			_origin2 = new Point(_image2.x, _image2.y);
			
			addChild(_image1);
			addChild(_image2);
			this.scrollRect = _scrollRect;
		}
		
		override protected function scrollH(dx:Number):void 
		{
			_image1.x += dx;
			_image2.x += dx;
			scrollImageH(_image1, _image2, dx);
			scrollImageH(_image2, _image1, dx);
		}
		
		override protected function scrollV(dy:Number):void 
		{
			//trace(_image1.y, _image2.y, dy);
			_image1.y += dy;
			_image2.y += dy;
			scrollImageV(_image1, _image2, dy);
			scrollImageV(_image2, _image1, dy);
			//trace(_image1.y - _image2.y);
		}
		
		private function scrollImageH(target:Bitmap, rest:Bitmap, dx:Number):void
		{
			if (dx > 0 && target.x > _scrollRect.right)
			{
				target.x = rest.x + _overlapX - target.width;
				if (_overlap)
					target.parent.swapChildren(target, rest);
			}
			else if (dx < 0 && target.x + target.width < _scrollRect.left)
			{
				target.x = rest.x + rest.width - _overlapX;
				if (_overlap)
					target.parent.swapChildren(target, rest);
			}
		}
		
		private function scrollImageV(target:Bitmap, rest:Bitmap, dy:Number):void
		{
			if (dy > 0 && target.y > _scrollRect.bottom)
			{
				target.y = rest.y + _overlapY - target.height;
				if (_overlap)
					target.parent.swapChildren(target, rest);
			}
			else if (dy < 0 && target.y + target.height < _scrollRect.top)
			{
				target.y = rest.y + rest.height - _overlapY;
				if (_overlap)
					target.parent.swapChildren(target, rest);
			}
		}
		
		override public function reset():void 
		{
			super.reset();
			_image1.x = _origin1.x;
			_image1.y = _origin1.y;
			_image2.x = _origin2.x;
			_image2.y = _origin2.y;
		}
		
		public function get image1():Bitmap 
		{
			return _image1;
		}
		
		public function get image2():Bitmap 
		{
			return _image2;
		}
	}

}