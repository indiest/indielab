package me.sangtian.common.display 
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * 
	 * @author Nicolas Tian
	 */
	public class BitmapWrappingScroller extends WrappingScroller
	{
		private var _bd:BitmapData;
		private var _bdCopy:BitmapData;
		
		private var _rectHelper:Rectangle = new Rectangle();
		private var _pointHelper:Point = new Point();
				
		public function BitmapWrappingScroller(bd:BitmapData) 
		{
			_bd = bd;
			_bdCopy = new BitmapData(_bd.width, _bd.height);
		}
		
		override protected function scrollH(dx:Number):void 
		{
			if (dx == 0)
				return;
			ASSERT(int(dx) == dx, "Only integral number is allowed.");
			_rectHelper.setTo(dx > 0 ? _bd.width - dx : 0, 0, Math.abs(dx), _bd.height);
			_pointHelper.setTo(dx > 0 ? 0 : _bd.width + dx, 0);
			_bdCopy.copyPixels(_bd, _rectHelper, _pointHelper);
			_bd.scroll(dx, 0);
			_rectHelper.x = _pointHelper.x;
			_bd.copyPixels(_bdCopy, _rectHelper, _pointHelper);
		}
		
		override protected function scrollV(dy:Number):void 
		{
			if (dy == 0)
				return;
			ASSERT(int(dy) == dy, "Only integral number is allowed.");
			_rectHelper.setTo(0, dy > 0 ? _bd.height - dy : 0, _bd.width, Math.abs(dy));
			_pointHelper.setTo(0, dy > 0 ? 0 : _bd.height + dy);
			_bdCopy.copyPixels(_bd, _rectHelper, _pointHelper);
			_bd.scroll(0, dy);
			_rectHelper.y = _pointHelper.y;
			_bd.copyPixels(_bdCopy, _rectHelper, _pointHelper);
		}
		
		override public function reset():void
		{
			scrollH( -scrollX % _bd.width);
			scrollV( -scrollY % _bd.height);
			super.reset();
		}
		
		public function dispose():void
		{
			_bdCopy.dispose();
		}
	}

}