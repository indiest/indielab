package me.sangtian.common.display 
{
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class WrappingScroller extends Sprite
	{
		private var _scrollX:Number = 0;
		private var _scrollY:Number = 0;
		
		public function get scrollX():Number 
		{
			return _scrollX;
		}
		
		public function set scrollX(value:Number):void 
		{
			scrollH(value - _scrollX);
			_scrollX = value;
		}
		
		public function get scrollY():Number 
		{
			return _scrollY;
		}
		
		public function set scrollY(value:Number):void 
		{
			scrollV(value - _scrollY);
			_scrollY = value;
		}
		
		public function WrappingScroller() 
		{
			
		}
		
		protected function scrollH(dx:Number):void
		{
			
		}
		
		protected function scrollV(dy:Number):void
		{
			
		}
		
		public function reset():void
		{
			_scrollX = 0;
			_scrollY = 0;
		}
	}

}