package me.sangtian.common.nape 
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import me.sangtian.common.Direction;
	import nape.geom.AABB;
	import nape.geom.GeomPoly;
	import nape.geom.GeomPolyList;
	import nape.geom.IsoFunction;
	import nape.geom.MarchingSquares;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.shape.Polygon;
	import nape.shape.Shape;
	import nape.space.Space;
	/**
	 * ...
	 * @author 
	 */
	public class NapeUtil 
	{
		
		public static function makeBarriers(spaceWidth:Number, spaceHeight:Number, directions:uint = 15, offset:Number = 0, size:Number = 100):Body
		{
			var barriers:Body = new Body(BodyType.STATIC);
			if ((directions & Direction.UP) != 0)
				barriers.shapes.add(new Polygon(Polygon.rect( 0, -offset - size, spaceWidth, size)));
			if ((directions & Direction.DOWN) != 0)
				barriers.shapes.add(new Polygon(Polygon.rect( 0, offset + spaceHeight, spaceWidth, size)));
			if ((directions & Direction.LEFT) != 0)
				barriers.shapes.add(new Polygon(Polygon.rect( -offset - size, 0, size, spaceHeight)));
			if ((directions & Direction.RIGHT) != 0)
				barriers.shapes.add(new Polygon(Polygon.rect(offset + spaceWidth, 0, size, spaceHeight)));
			return barriers;
		}
		
		private static function setShapeSensorEnabled(shape:Shape):void
		{
			shape.sensorEnabled = true;
		}
		private static function setShapeSensorDisabled(shape:Shape):void
		{
			shape.sensorEnabled = false;
		}
		public static function setBodySensorEnabled(body:Body, enabled:Boolean):void
		{
			body.shapes.foreach(enabled ? setShapeSensorEnabled : setShapeSensorDisabled);
		}
		
		public static function createBodyFromIso(iso:IsoFunction, bounds:AABB, granularity:Vec2 = null, quality:int = 2, simplification:Number = 1.5):Body {
			var body:Body = new Body();
			if (granularity==null) granularity = Vec2.weak(8, 8);
			var polys:GeomPolyList = MarchingSquares.run(iso, bounds, granularity, quality);
			for (var i:int = 0; i < polys.length; i++) {
				var p:GeomPoly = polys.at(i);

				var qolys:GeomPolyList = p.simplify(simplification).convexDecomposition(true);
				for (var j:int = 0; j < qolys.length; j++) {
					var q:GeomPoly = qolys.at(j);

					body.shapes.add(new Polygon(q));

					// Recycle GeomPoly and its vertices
					q.dispose();
				}
				// Recycle list nodes
				qolys.clear();

				// Recycle GeomPoly and its vertices
				p.dispose();
			}
			// Recycle list nodes
			polys.clear();

			// Align body with its centre of mass.
			// Keeping track of our required graphic offset.
			var pivot:Vec2 = body.localCOM.mul(-1);
			body.translateShapes(pivot);
			body.userData.graphicOffset = pivot;
			return body;
		}
		
		public static function createBodyFromDisplayObject(obj:DisplayObject, granularity:Vec2 = null, quality:int = 2, simplification:Number = 1.5):Body
		{
			var iso:DisplayObjectIso = new DisplayObjectIso(obj);
			return createBodyFromIso(iso, iso.bounds, granularity, quality, simplification);
		}

		public static function createBodyFromBitmapData(bd:BitmapData, alphaThreshold:uint = 0x80, granularity:Vec2 = null, quality:int = 2, simplification:Number = 1.5):Body
		{
			var iso:BitmapDataIso = new BitmapDataIso(bd, alphaThreshold);
			return createBodyFromIso(iso, iso.bounds, granularity, quality, simplification);
		}
	}

}