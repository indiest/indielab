package me.sangtian.gengraphy 
{
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.geom.Matrix;
	import me.sangtian.common.util.MathUtil;
	import me.sangtian.common.util.Random;
	/**
	 * ...
	 * @author 
	 */
	public class MountainGenerator 
	{
		public var maxHeight:uint = 400;
		public var minHeight:uint = 100;
		public var topColor:uint = 0xff3f3f3f;
		public var bottomColor:uint = 0xff000000;
		public var slope0:Number = 3.0;
		public var slope1:Number = 1.0;
		public var treeDensity:uint = 0;
		public var treeSlopeThreshold:Number = 10;
		private var _treeGenerator:AnimatedTreeGenerator = new AnimatedTreeGenerator();
		private var _treeMatrix:Matrix = new Matrix();
		
		public function get treeGenerator():AnimatedTreeGenerator 
		{
			return _treeGenerator;
		}
		
		public function MountainGenerator() 
		{
			
		}
		
		public function generateToBitmapData(bd:BitmapData):void
		{
			//var shape:Shape = new Shape();
			//shape.graphics.lineStyle(1);
			//shape.graphics.lineGradientStyle(GradientType.LINEAR, [bottomColor, topColor], [1, 1], [0, 127]);
			var height:int = Random.rangeInt(minHeight, maxHeight);
			var velocity:Number = 0;
			bd.lock();
			for (var x:uint = 0; x < bd.width; x++)
			{
				velocity += Random.rangeNumber(-slope1, slope1);
				var delta:int = velocity * 0.1 + Random.rangeInt( -slope0, slope0);
				if (height + delta > maxHeight || height + delta < minHeight)
					velocity = -velocity;
				height = MathUtil.clamp(height + delta, minHeight, maxHeight);
				//trace(h);
				//shape.graphics.moveTo(x, bd.height);
				//shape.graphics.lineTo(x, h);
				for (var y:uint = bd.height - height; y < bd.height; y++)
				{
					var f:Number = (bd.height - y) / height;
					bd.setPixel32(x, y, MathUtil.interpolateColor(bottomColor, topColor, f));
				}
				
				if (treeDensity > 0 &&
					x % treeDensity == 0 &&
					Math.abs(velocity) < treeSlopeThreshold)
				{
					_treeGenerator.generate();
					_treeGenerator.finish();
					_treeMatrix.tx = x;
					_treeMatrix.ty = bd.height - height;
					bd.draw(_treeGenerator, _treeMatrix, _treeGenerator.transform.colorTransform);
				}
			}
			//bd.draw(shape);
			bd.unlock();
		}
	}

}