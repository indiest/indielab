package me.sangtian.gengraphy
{
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.filters.BlurFilter;
    import flash.geom.Point;
    import flash.display.Shape;
    import flash.events.MouseEvent;
	import me.sangtian.common.util.Random;

/*
 *
 * AS3 Tree
 * by Duarte Peixinho
 * [email]duarte.peixinho@gmail.com[/email]
 * page: [url]http://duartepeixinho.com[/url]
 *
 */
    public class AnimatedTreeGenerator extends Sprite
    {
		public var heightMin:uint = 600;
		public var heightMax:uint = 700;
		public var thickness:uint = 10;
		
        private var branche1:Branches;
        private var branche2:Branches;
        private var branche3:Branches;

        public function AnimatedTreeGenerator():void
        {
			generate();
        }
		
		public function generate():void
		{
			if (branche1 != null)
				removeChild(branche1);
            branche1 = new Branches(Random.rangeInt(heightMin, heightMax), thickness, 5, 10, -95, 20);
            //branche1.x = (stage.stageWidth / 2);
            //branche1.y = stage.stageHeight;
            //branche1.filters = [new BlurFilter(2,2)];
            addChild(branche1);
			
			if (branche2 != null)
				removeChild(branche2);
            branche2 = new Branches(Random.rangeInt(heightMin, heightMax), thickness, 5, 10, -90, 20);
            //branche2.x = (stage.stageWidth / 2);
            //branche2.y = stage.stageHeight;
            //branche2.filters = [new BlurFilter(2,2)];
            addChild(branche2);
			
			if (branche3 != null)
				removeChild(branche3);
            branche3 = new Branches(Random.rangeInt(heightMin, heightMax), thickness, 5, 10, -85, 20);
            //branche3.x = (stage.stageWidth / 2);
            //branche3.y = stage.stageHeight;
            //branche3.filters = [new BlurFilter(2,2)];
            addChild(branche3);
		}

        public function update():void
        {
            branche1.processFrame();
            branche2.processFrame();
            branche3.processFrame();
        }
		
		public function finish():void
		{
			while (!branche1.finished)
				branche1.processFrame();
			
			while (!branche2.finished)
				branche2.processFrame();
			
			while (!branche3.finished)
				branche3.processFrame();
		}
    }
}

import flash.display.Sprite;

class Branches extends Sprite
{


    private var angle:Number;
    private var extension:Number;
    private var thickness:int;
    private var thicknessInit:int;

    private var angleInterval:Number;
    private var lineSize:Number;
    private var posX:Number = 0;
    private var posY:Number = 0;
    private var extensionComplete:Number = 0;

    //branches
    private var branches:Array = new Array();
    private var branchesNumber:Number = 0;
    private var actualBranches:Number = 0;
    private var i:int = 1;

    //lineSize calculation
    private var lineDone:Boolean = true;
    private var angleCalc:Number;
    private var posXinit:Number = 0;
    private var posYinit:Number = 0;
	
	public function get finished():Boolean
	{
		return extension <= extensionComplete;
	}

    public function Branches(extension:Number, thickness:int, branchesNumber:Number = 5, lineSize:Number = 5, angle:Number = 90, angleInterval:Number = 30 )
    {

        this.angle = angle;
        this.extension = extension;
        this.thickness = thickness;
        this.thicknessInit = thickness;
        this.angleInterval = angleInterval;
        this.lineSize = lineSize;
        this.branchesNumber = branchesNumber;
    }

    public function processFrame():void
    {
        //draw here
        if (!finished)
        {

            if (lineDone)
            {
                posXinit = posX;
                posYinit = posY;
                angleCalc = Math.random() * ((angle + angleInterval) - (angle-angleInterval)) + (angle-angleInterval);
                lineDone = false;

            }
            graphics.lineStyle(thickness);
            posX += Math.cos(angleCalc * Math.PI / 180) * lineSize;
            posY+=Math.sin(angleCalc*Math.PI/180)*lineSize;
            extensionComplete+=Math.round(Math.sqrt(Math.pow(posX-posXinit,2)+Math.pow(posY-posYinit,2)));
            thickness = thicknessInit - (thicknessInit * extensionComplete / extension);
            graphics.lineTo(posX, posY);

            if (Math.sqrt(Math.pow(lineSize,2))<Math.sqrt(Math.pow(posX-posXinit,2)+Math.pow(posY-posYinit,2)))
            {
                lineDone=true;
            }


            //branches
            if (actualBranches<branchesNumber)
            {
                //tweak numbers here to create the branches
                if (extensionComplete>=extension/branchesNumber*i)
                {
                    branches[i - 1] = new Branches(extension/2, thickness, Math.random()*10+5, lineSize/2, Math.random()*(angle-90), 30);
                    branches[i-1].x=posX;
                    branches[i-1].y=posY;
                    addChild(branches[i - 1]);
                    actualBranches++;
                    i++;
                }
            }
            for (var k:int = 0; k < branches.length; k++)
            {
                branches[k].processFrame();
            }
        }
    }
}