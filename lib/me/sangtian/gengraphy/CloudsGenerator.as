package me.sangtian.gengraphy
{
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.geom.Point;
	/**
	 * ...
	 * @author 
	 */
	public class CloudsGenerator 
	{
		public var cloudMaxWidth:Number = 256;
		public var cloudMaxHeight:Number = 64;
		public var numOctaves:uint = 3;
		public var seed:int = int(Math.random() * 1000);
		public var grayScale:Boolean = true;
		public var offset:Point = new Point();
		
		public function CloudsGenerator() 
		{
			
		}
		
		public function generateToBitmapData(bd:BitmapData):void
		{
            bd.perlinNoise(
                cloudMaxWidth, // baseX:Number
                cloudMaxHeight, // baseY:Number
                numOctaves, // numOctaves:uint
                seed, // randomSeed:int
                false, // stitch:Boolean
                true, // fractalNoise:Boolean. False for ocean waves.
                BitmapDataChannel.ALPHA, // channelOptions:uint
                grayScale, //  grayScale:Boolean. False for dark clouds.
                [offset, offset] // offsets:Array
            );
		}
	}

}