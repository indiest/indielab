﻿/**
 * VERSION: 12.1
 * DATE: 2012-06-19
 * AS3
 * UPDATES AND DOCS AT: http://www.greensock.com
 **/
package com.greensock.plugins {
	import com.greensock.TweenLite;
/**
 * [AS3/AS2 only] Set the text property of an object (espeically for a TextField) by adding number to the value.
 * 
 * 
 * <p><b>USAGE:</b></p>
 * <listing version="3.0">
import com.greensock.TweenLite; <br />
import com.greensock.plugins.TweenPlugin; <br />
import com.greensock.plugins.VisiblePlugin; <br />
TweenPlugin.activate([TextPlugin]); //activation is permanent in the SWF, so this line only needs to be run once.<br /><br />

TweenLite.to(textField, 1, {num: 123}); <br /><br />
</listing>
 * 
 * 
 * @author Nicolas Tian
 */
	public class TextNumberPlugin extends TweenPlugin {
		/** @private **/
		public static const API:Number = 2; //If the API/Framework for plugins changes in the future, this number helps determine compatibility
		
		/** @private **/
		protected var _target:Object;
		/** @private **/
		protected var _tween:TweenLite;
		/** @private **/
		protected var _targetNum:Number;
		/** @private **/
		protected var _initVal:Number;
		/** @private **/
		//protected var _progress:int;
		protected var _fractionDigits:uint = 0;
		
		/** @private **/
		public function TextNumberPlugin() {
			super("num");
		}
		
		/** @private **/
		override public function _onInitTween(target:Object, value:*, tween:TweenLite):Boolean {
			_target = target;
			_tween = tween;
			//_progress = (_tween.vars.runBackwards) ? 0 : 1;
			_initVal = parseFloat(_target.text) || 0;
			_targetNum = value;
			return true;
		}
		
		/** @private **/
		override public function setRatio(v:Number):void {
			super.setRatio(v);
			_target.text = (_initVal + (_targetNum - _initVal) * v).toFixed(_fractionDigits);
		}

	}
}