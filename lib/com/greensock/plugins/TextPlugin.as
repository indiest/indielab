﻿/**
 * VERSION: 12.1
 * DATE: 2012-06-19
 * AS3
 * UPDATES AND DOCS AT: http://www.greensock.com
 **/
package com.greensock.plugins {
	import com.greensock.TweenLite;
/**
 * [AS3/AS2 only] Set the text property of an object (espeically for a TextField) by adding each characters.
 * 
 * 
 * <p><b>USAGE:</b></p>
 * <listing version="3.0">
import com.greensock.TweenLite; <br />
import com.greensock.plugins.TweenPlugin; <br />
import com.greensock.plugins.VisiblePlugin; <br />
TweenPlugin.activate([TextPlugin]); //activation is permanent in the SWF, so this line only needs to be run once.<br /><br />

TweenLite.to(textField, 1, {text: "Blah blah"}); <br /><br />
</listing>
 * 
 * 
 * @author Nicolas Tian
 */
	public class TextPlugin extends TweenPlugin {
		/** @private **/
		public static const API:Number = 2; //If the API/Framework for plugins changes in the future, this number helps determine compatibility
		
		/** @private **/
		protected var _target:Object;
		/** @private **/
		protected var _tween:TweenLite;
		/** @private **/
		protected var _text:String;
		/** @private **/
		protected var _initVal:String;
		
		/** @private **/
		public function TextPlugin() {
			super("text");
		}
		
		/** @private **/
		override public function _onInitTween(target:Object, value:*, tween:TweenLite):Boolean {
			_target = target;
			_tween = tween;
			_initVal = _target.text;
			_text = value;
			return true;
		}
		
		/** @private **/
		override public function setRatio(v:Number):void {
			super.setRatio(v);
			_target.text = _text.substr(0, v * _text.length);
		}

	}
}