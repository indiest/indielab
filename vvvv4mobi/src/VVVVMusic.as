package
{
	import com.jac.ogg.OggManager;
	import com.jac.ogg.events.OggManagerEvent;
	
	import flash.Lib;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	import org.flixel.FlxG;
	import org.flixel.FlxSound;
	import org.xiph.fogg.Buffer;
	import org.xiph.fogg.Page;
	import org.xiph.fvorbis.FuncFloor;
	import org.xiph.fvorbis.FuncMapping;
	import org.xiph.fvorbis.FuncResidue;
	import org.xiph.fvorbis.FuncTime;

	[Event(name=LOAD_PROGRESS, type="flash.events.ProgressEvent")]
	[Event(name=LOAD_COMPLETE, type="flash.events.Event")]
	[Event(name=SONG_FINISHED, type="flash.events.Event")]
	public class VVVVMusic extends EventDispatcher
	{
		public static const LOAD_PROGRESS:String = "loadProgress";
		public static const LOAD_COMPLETE:String = "loadComplete";
		public static const SONG_FINISHED:String = "songFinished";
		
		private var _loader:URLLoader = new URLLoader();
		private var _infos:Vector.<MusicInfo> = new Vector.<MusicInfo>();
//		private var _oggMgr:OggManager;
		private var _vvvBytes:ByteArray;
		private var _oggBytes:ByteArray = new ByteArray();
		private var _oggSound:OGGSound;
//		private var _sound:Sound = new Sound();
//		private var _sound:FlxSound = new FlxSound();
		private var _soundChannel:SoundChannel;
		private var _decodeIndex:uint = 0;
		private var _loop:Boolean;
		
//		public function get sound():FlxSound
//		{
//			return _sound;
//		}
		
		public function load(url:String="vvvvvvmusic.vvv"):void
		{
//			_sound.addEventListener(SampleDataEvent.SAMPLE_DATA, handleSampleData);
			
			_loader.addEventListener(ProgressEvent.PROGRESS, handleProgress);
			_loader.addEventListener(Event.COMPLETE, handleLoadComplete);
			_loader.addEventListener(IOErrorEvent.IO_ERROR, handleLoadError);
			_loader.dataFormat = URLLoaderDataFormat.BINARY;
			_loader.load(new URLRequest(url));
		}
		
		private function handleLoadError(evt:IOErrorEvent):void
		{
			trace("Music file not found!");
			dispatchEvent(new Event(LOAD_COMPLETE));
		}
		
//		private function handleSampleData(evt:SampleDataEvent):void
//		{
//			var decodedBytes:ByteArray = new ByteArray();
//			_oggMgr.getSampleData(decodedBytes);
//			if (decodedBytes.length < OggManager.DEFAULT_NUM_SAMPLES * 8)
//			{
//				if (_loop)
//				{
//					_oggMgr.initDecoder(_oggBytes);
//					_oggMgr.getSampleData(decodedBytes);
//				}
//				else
//				{
//					return;
//				}
//			}
//			
//			decodedBytes.position = 0;
//			while (decodedBytes.bytesAvailable)
//			{
//				//Left Channel
//				evt.data.writeFloat(decodedBytes.readFloat());
//				//Right Channel
//				evt.data.writeFloat(decodedBytes.readFloat());
//			}
//			dispatchEvent(new Event(SONG_FINISHED));
//		}
		
		private function handleProgress(e:ProgressEvent):void 
		{
			dispatchEvent(new ProgressEvent(LOAD_PROGRESS, false, false, e.bytesLoaded, e.bytesTotal));
		}
		
		private function handleLoadComplete(evt:Event):void
		{
			_loader.removeEventListener(ProgressEvent.PROGRESS, handleProgress);
			_loader.removeEventListener(Event.COMPLETE, handleLoadComplete);
			_vvvBytes = _loader.data as ByteArray;
			_vvvBytes.endian = Endian.LITTLE_ENDIAN;
			var offset:uint = 60*128;
			var info:MusicInfo;
			while (_vvvBytes.position < offset)
			{
				info = new MusicInfo();
				info.name = _vvvBytes.readMultiByte(48, "UTF-8");
				_vvvBytes.readInt();
				info.size = _vvvBytes.readInt();
				info.offset = offset;
				if (_vvvBytes.readInt() == 0)
					break;
				_infos.push(info);
				
				offset += info.size;
			}
			
			dispatchEvent(new Event(LOAD_COMPLETE));
		}
		
		public function play(num:uint, loop:Boolean = true):void
		{
//			if (_soundChannel != null)
//			{
//				_soundChannel.stop();
//				// TODO: Fade out
//			}
			
			if (_oggSound != null)
				_oggSound.stop();
			if (num >= 0 && num < _infos.length)
			{
				_decodeIndex = num;
				_loop = loop;
			}
			else
			{
				return;
			}
			var info:MusicInfo = _infos[_decodeIndex];
			_vvvBytes.position = info.offset;
			_oggBytes.clear();
			_vvvBytes.readBytes(_oggBytes, 0, info.size);
			if (_oggSound == null)
			{
//				Lib.current = this;
				Buffer._s_init();
				Page.__static_init__();
				FuncFloor._s_init();
				FuncMapping._s_init();
				FuncResidue._s_init();
				FuncTime._s_init();
				_oggSound = new OGGSound();
			}
			_oggSound.loadBytes(_oggBytes);
			_oggSound.play();
			_oggSound.setVolume(0.5);
			// TODO: Fade in
		}
		
//		private function saveOggBytesToFile():void
//		{
//			var file:File = File.documentsDirectory.resolvePath("piercing the sky.ogg");
//			var stream:FileStream = new FileStream();
//			stream.open(file, FileMode.WRITE); 
//			stream.writeBytes(_oggBytes);
//			stream.close();
//		}
		
	}
}
