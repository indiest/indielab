package ui 
{
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class FlxTextInput extends FlxSprite
	{
		private var _textField:TextField;
		public function get textField():TextField
		{
			return _textField;
		}
		public var bgColor:uint;
		
		public function FlxTextInput(width:uint, height:uint, bgColor:uint = 0xffffffff, textColor:uint = 0xff000000) 
		{
			_textField = new TextField();
			_textField.type = TextFieldType.INPUT;
			_textField.background = true;
			_textField.backgroundColor = bgColor;
			_textField.width = width;
			_textField.height = height;
			var format:TextFormat = new TextFormat("system", 8, textColor);
			_textField.defaultTextFormat = format;
			_textField.setTextFormat(format);
			this.bgColor = bgColor;
			makeGraphic(width, height, bgColor);
		}
		
		public function focus():void
		{
			FlxG.stage.focus = _textField;
		}
		
		public function get isFocusing():Boolean
		{
			return FlxG.stage.focus == _textField;
		}
		
		override public function draw():void 
		{
			framePixels.fillRect(framePixels.rect, bgColor);
			framePixels.draw(_textField);
			super.draw();
		}
		
		override public function update():void 
		{
			super.update();
			
			if (FlxG.mouse.pressed())
			{
				if (overlapsPoint(FlxG.mouse))
				{
					focus();
				}
			}
		}
	}

}