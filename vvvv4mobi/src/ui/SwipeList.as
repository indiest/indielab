package ui
{
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.Graphics;
	import org.flixel.FlxRect;
	
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	
	public class SwipeList extends FlxBitmapFont
	{
		public var scrollHeight:uint;
		public var seperateColor:uint = 0xff666666;
		private var _items:Array;
		private var _selectedIndex:int = -1;

		private var _scrollY:int;
		private var _pressing:Boolean = false;
		private var _lastY:Number;
		private var _pressIndex:int;
		
		private var _selectCallback:Function;
		private var _selectCallbackArgs:Array;
		
		public function SwipeList(scrollHeight:uint)
		{
			super(Assets.FONT, 8, 8, FlxBitmapFont.TEXT_SET1, 0, 0, 0, 0, 16);
			this.scrollHeight = scrollHeight;
		}
		
		public function get items():Array
		{
			return _items;
		}
		
		public function set items(value:Array):void
		{
			_items = value;
			text = value.join("\n");
			for (var i:int = 0; i < value.length; i++)
			{
				var y:uint = indentY + (i * (characterHeight + customSpacingY)) - customSpacingY / 2;
				drawLine(0, y, pixels.width, y, seperateColor);
			}
			//drawRect(new FlxRect(0, 0, pixels.width, pixels.height), 0xffffffff);
		}
		
		private function getMousePointIndex():uint
		{
			return (FlxG.mouse.screenY - y - indentY + _scrollY) / (characterHeight + customSpacingY);
		}
		
		public override function update():void
		{
			if (_items == null || _items.length == 0)
				return;
			if (FlxG.mouse.pressed())
			{
				if (overlapsPoint(FlxG.mouse))
				{
					if (!_pressing)
					{
						_pressing = true;
						_lastY = FlxG.mouse.screenY;
						_pressIndex = getMousePointIndex();
						//trace("pressIndex", _pressIndex);
					}
					else
					{
						var dy:int = FlxG.mouse.screenY - _lastY;
						if (dy != 0)
						{
							_scrollY -= dy;
							if (_scrollY > pixels.height - scrollHeight)
								_scrollY = pixels.height - scrollHeight;
							if (_scrollY < 0)
								_scrollY = 0;
							_pressIndex = selectedIndex;
							dirty = true;
						}
						_lastY = FlxG.mouse.screenY;
					}
				}
				else
				{
					_pressing = false;
				}
			}
			else
			{
				_pressing = false;
			}
			
			if (FlxG.mouse.justReleased())
			{
				var index:uint = getMousePointIndex();
				//trace("release index", index);
				if (index == _pressIndex)
				{
					selectedIndex = index;
				}
			}
		}
		
		public function get selectedIndex():int
		{
			return _selectedIndex;
		}
		
		public function set selectedIndex(value:int):void
		{
			if (_selectedIndex == value)
				return;
			_selectedIndex = value;
			dirty = true;

			if (_selectCallback != null)
				_selectCallback.apply(null, _selectCallbackArgs);
		}
		
		override protected function drawFramePixels():void 
		{
			_flashRect.y = _scrollY;
			_flashRect.height = scrollHeight;
			clearFramePixels();
			super.drawFramePixels();
			if (selectedIndex >= 0)
			{
				var gfx:Graphics = FlxG.flashGfx;
				gfx.clear();
				gfx.beginFill(0xaaaaaa);
				gfx.drawRect(0, -_scrollY + indentY + selectedIndex * (characterHeight + customSpacingY) - customSpacingY / 2, pixels.width, characterHeight + customSpacingY);
				gfx.endFill();
				framePixels.draw(FlxG.flashGfxSprite, null, null, BlendMode.INVERT);
			}
			//trace("scrollY", _scrollY);
			//framePixels.scroll(0, _scrollY);
		}
		
		public function setSelectCallback(callback:Function, ...args):void
		{
			_selectCallback = callback;
			_selectCallbackArgs = args;
		}
	}
}