package ui 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import test.InputBoxTestState;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class InputBox extends FlxGroup
	{
		private var _input:FlxTextInput = new FlxTextInput(100, 20);
		private var _bg:FlxSprite = new FlxSprite();
		private var _txtMsg:FlxBitmapFont = Util.createVVVVText();
		private var _btnOk:FlxButtonPlus = new FlxButtonPlus(110, 130, finishInput, null, "OK", 40, 20);
		private var _btnCancel:FlxButtonPlus = new FlxButtonPlus(160, 130, finishInput, [true], "Cancel", 50, 20);
		private var _finishCallback:Function;
		
		public function InputBox() 
		{
			//_bg.makeGraphic(120, 80, 0xff000000);
			//Util.centerX(_bg);
			//Util.centerY(_bg);
			_bg.makeGraphic(VVVVGame.SCREEN_WIDTH, VVVVGame.SCREEN_HEIGHT, 0xff000000);
			add(_bg);

			_txtMsg.x = 110;
			_txtMsg.y = 80;
			add(_txtMsg);

			Util.centerX(_input);
			_input.y = 100;
			_input.textField.multiline = false;
			//_input.textField.needsSoftKeyboard = VVVVGame.mobile;
			add(_input);
			
			_btnOk.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
			_btnOk.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
			add(_btnOk);
			_btnCancel.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
			_btnCancel.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
			add(_btnCancel);
		}
		
		public function open(msg:String = null, maxChars:uint = 0, defaultText:String = "", finishCallback:Function = null):void
		{
			_txtMsg.text = msg;
			Util.centerX(_txtMsg);
			_input.textField.maxChars = maxChars;
			_input.textField.text = defaultText;
			_input.textField.setSelection(0, _input.textField.length);
			_input.focus();
			
			_finishCallback = finishCallback;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (FlxG.keys.justPressed("ENTER"))
			{
				if (_input.isFocusing)
				{
					finishInput();
				}
			}
		}
		
		private function finishInput(canceled:Boolean = false):void 
		{
			if (_finishCallback != null)
			{
				_finishCallback(canceled ? null : _input.textField.text);
			}
			hide();
		}
		
		private static var _instance:InputBox = new InputBox();
		
		public static function show(msg:String = null, maxChars:uint = 0, defaultText:String = "", finishCallback:Function = null):void
		{
			_instance.open(msg, maxChars, defaultText, finishCallback);
			FlxG.state.add(_instance);
		}
		
		public static function hide():void
		{
			FlxG.state.remove(_instance);
		}
	}

}