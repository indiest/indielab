package ui
{
	import org.flixel.FlxBasic;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	
	public class SelectBox extends FlxGroup
	{
		private var _bg:FlxSprite = new FlxSprite();
		private var _titleText:FlxBitmapFont = Util.createVVVVText();
		private var _items:Array = []
		private var _selectCallback:Function;
		
		public function SelectBox()
		{
			super();
			_bg.makeGraphic(VVVVGame.SCREEN_WIDTH, VVVVGame.SCREEN_HEIGHT, 0xff000000);
		}
		
		public function open(selections:Array, title:String = null, selectCallback:Function = null):void
		{
			add(_bg);
			_titleText.text = title;
			_titleText.scale.make(2, 2);
			_titleText.x = (VVVVGame.SCREEN_WIDTH - _titleText.pixels.width) / 2;
			_titleText.y = (VVVVGame.SCREEN_HEIGHT - _titleText.pixels.height - selections.length * 30) / 2;			
			add(_titleText);
			
			var y:uint = _titleText.y + _titleText.pixels.height * 4;
			for (var i:uint = 0; i < selections.length; i++)
			{
				var selection:Object = selections[i];
				//var _item:TilemapText = new TilemapText();
				var _item:FlxButtonPlus = new FlxButtonPlus(0, y, onSelectItem, [i], "", 100, 20);
				_item.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
				_item.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
				for (var key:* in selection)
				{
					_item[key] = selection[key];
				}
				_item.x = (VVVVGame.SCREEN_WIDTH - _item.width) / 2;
				//_item.y = y;
				y += 30;
				_items.push(_item);
				add(_item);
			}
			
			_selectCallback = selectCallback;
		}
		
		private function onSelectItem(index:uint):void
		{
			if (_selectCallback != null)
			{
				if (!_selectCallback(index, _items[index]))
					return;
			}
			hide();
			clear();
		}
		
		override public function clear():void 
		{
			super.clear();
			for each (var item:FlxBasic in _items)
			{
				item.destroy();
			}
			_items.length = 0;
		}
		
		private static var _instance:SelectBox = new SelectBox();
		private static var _showing:Boolean = false;
		public static function isShowing():Boolean
		{
			return _showing;
		}
		
		public static function show(selections:Array, title:String = null, selectCallback:Function = null):void
		{
			_instance.open(selections, title, selectCallback);
			FlxG.state.add(_instance);
			_showing = true;
		}
		
		public static function hide():void
		{
			FlxG.state.remove(_instance);
			_showing = false;
		}
	}
}