package ui
{
	import flash.display.Graphics;
	import org.flixel.FlxPoint;
	import org.flixel.FlxU;
	
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	
	public class MessageBox extends FlxGroup
	{
		private var _messageText:FlxBitmapFont;
		private var _finishCallback:Function;
		public var color:uint = 0xaaaaaa;
		
		public function MessageBox()
		{
			super();
			
			_messageText = Util.createVVVVText();
			_messageText.customSpacingY = 8;
			_messageText.color = color;
			add(_messageText);
		}
		
		public function showText(text:String, finishCallback:Function = null, color:* = null, pos:FlxPoint = null):void
		{
			this.color = color || 0xaeaeae;
			_messageText.text = text;
			_messageText.color = this.color;
			if (pos == null)
			{
				Util.centerX(_messageText);
				Util.centerY(_messageText);
			}
			else
			{
				_messageText.x = Math.max(10, pos.x - _messageText.pixels.width / 2);
				_messageText.y = Math.max(10, pos.y - _messageText.pixels.height);
			}
			_finishCallback = finishCallback;
		}
		
		public override function draw():void
		{
			if (_messageText.text == null || _messageText.text.length == 0)
				return;
			var gfx:Graphics = FlxG.flashGfx;
			gfx.clear();
			gfx.beginFill(FlxU.makeColor(27, 27, 42));
			gfx.drawRect(_messageText.x - 8, _messageText.y - 8, _messageText.width + 16, _messageText.height + 16);
			gfx.endFill();
			gfx.lineStyle(2, color);
			gfx.drawRect(_messageText.x - 7, _messageText.y - 7, _messageText.width + 14, _messageText.height + 14);
			gfx.lineStyle(1, color);
			gfx.drawRect(_messageText.x - 4, _messageText.y - 4, _messageText.width + 8, _messageText.height + 8);
			if (cameras == null)
				cameras = FlxG.cameras;
			for each(var camera:FlxCamera in cameras)
			{
				camera.buffer.draw(FlxG.flashGfxSprite);
			}
			
			super.draw();
		}
		
		public override function update():void
		{
			super.update();
			if (FlxG.keys.justPressed("SPACE") || FlxG.mouse.justPressed())
			{
				FlxG.state.remove(this);
				var state:PlayState = FlxG.state as PlayState;
				if (state != null)
				{
					state.setTopText("");
					state.captain.paused = false;
				}
				if (_finishCallback != null)
				{
					_finishCallback();
				}
			}
		}
		
		private static var _instance:MessageBox = new MessageBox();
		
		public static function show(text:String, finishCallback:Function = null, color:* = null, pos:FlxPoint = null):void
		{
			_instance.showText(text, finishCallback, color, pos);
			var state:PlayState = FlxG.state as PlayState;
			if (state != null)
			{
				state.setTopText("Press ACTION or tab screen to continue");
				state.captain.paused = true;
			}
			FlxG.state.add(_instance);
		}
	}
}