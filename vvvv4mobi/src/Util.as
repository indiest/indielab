package  
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxU;
	
	import org.flixel.plugin.photonstorm.FlxBitmapFont;

	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Util
	{
		public static function createVVVVText(text:String = null):FlxBitmapFont
		{
			var font:FlxBitmapFont = new FlxBitmapFont(Assets.FONT, 8, 8, FlxBitmapFont.TEXT_SET1, 0, 0, 0, 0, 16);
			font.color = FlxU.makeColor(212, 212, 251);//0xb8b8ed;
			if (text)
				font.text = text;
			return font;
		}
		
		public static function centerX(obj:FlxObject):void
		{
			obj.x = (VVVVGame.SCREEN_WIDTH - obj.width) / 2;
		}
		
		public static function centerY(obj:FlxObject):void
		{
			obj.y = (VVVVGame.SCREEN_HEIGHT - obj.height) / 2;
		}
		
		
		
		public static function formatTime(ms:uint):String
		{
			var s:uint = ms / 1000;
			var hours:uint = s / 3600;
			var minutes:uint = (s % 3600) / 60;
			var seconds:uint = s % 216000;
			return hours + ":" + getFixedString(String(minutes), 2, "", "0", "") + ":" + getFixedString(String(seconds), 2, "", "0", "");
		}
		
		public static function getFixedString(str:String, length:uint, ellipsis:String = "...", leftPadding:String = "", rightPadding:String = " "):String
		{
			if (str.length > length)
			{
				return str.substr(0, length - ellipsis.length) + ellipsis;
			}
			else
			{
				while (str.length < length)
					str = leftPadding + str + rightPadding;
			}
			return str;
		}
		
		/**
		 * Make a HTTP request in a simple call.
		 * 
		 * @param	url			If the request is GET with queries, the URL should be encoded by yourself.
		 * @param	postData	Data to post to the URL. Should match the dataFormat if assigned.
		 * @param	dataFormat	The URLLoaderDataFormat constant indicates both the request and response data format. Ignore is postData is null.
		 * @param	callback	function(data:*, error:String). The first parameter will be null if failed, as the second will null if succeeded.
		 */
		public static function httpRequest(url:String, postData:Object = null, dataFormat:String = "text", callback:Function = null):void
		{
			var req:URLRequest = new URLRequest(url);
			if (postData != null)
			{
				req.method = URLRequestMethod.POST;
				if (dataFormat == URLLoaderDataFormat.TEXT)
				{
					req.data = String(postData);
					req.contentType = "text/plain";
				}
				else if (dataFormat == URLLoaderDataFormat.VARIABLES)
				{
					req.data = new URLVariables();
					if (postData is String)
						req.data.decode(postData as String);
					else
					{
						for (var key:String in postData)
						{
							req.data[key] = postData[key];
						}
					}
				}
				else if (dataFormat == URLLoaderDataFormat.BINARY)
				{
					req.data = postData;
					req.contentType = "application/binary";
				}
			}
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = dataFormat;
			var handleHttpRequestLoaded:Function = function(evt:Event):void
			{
				urlLoader.removeEventListener(Event.COMPLETE, handleHttpRequestLoaded);
				urlLoader.removeEventListener(Event.COMPLETE, handleHttpRequestError);
				if (callback != null)
					callback(urlLoader.data, null);
			};
			var handleHttpRequestError:Function = function(evt:IOErrorEvent):void
			{
				urlLoader.removeEventListener(Event.COMPLETE, handleHttpRequestLoaded);
				urlLoader.removeEventListener(Event.COMPLETE, handleHttpRequestError);
				if (callback != null)
					callback(null, evt.text);
			};
			urlLoader.addEventListener(Event.COMPLETE, handleHttpRequestLoaded);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, handleHttpRequestError);
			urlLoader.load(req);
		}
		
		public static function lookupInObject(target:*, obj:Object):String
		{
			for (var key:String in obj)
			{
				var value:* = obj[key];
				if (value === target)
					return key;
				else if (value is Array)
				{
					if ((value as Array).indexOf(target) >= 0)
						return key;
				}
			}
			return null;
		}
		
		public static function makeIntArray(start:int, end:int):Array
		{
			var arr:Array = [];
			while (start <= end)
			{
				arr.push(start);
				start++;
			}
			return arr;
		}
		
		public static function makeTileData(value:uint, length:uint):String
		{
			var str:String = String(value);
			while ((length--) > 0)
				str += ("," + value);
			return str;
		}
		
		public static function deepClone(object:*):*
		{
			var bytes:ByteArray = new ByteArray();
			bytes.writeObject(object);
			bytes.position = 0;
			return bytes.readObject();
		}
	}

}