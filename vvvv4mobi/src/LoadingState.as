package
{
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class LoadingState extends FlxState
	{
		public static var music:VVVVMusic = new VVVVMusic();
		public static var archiveDirectory:File;
		
		private var _progress:FlxBitmapFont = Util.createVVVVText();
		
		public override function create():void
		{
			var docDir:File = File.documentsDirectory;
			var vvvDir:File = docDir.resolvePath("VVVVVV");
			if (!vvvDir.exists)
			{
				docDir.addEventListener(Event.SELECT, handleDirSelect);
				docDir.browseForDirectory("VVVVVV Archive Directory");
			}
			else
			{
				archiveDirectory = vvvDir;
				loadMusic();
			}

			_progress.x = 170;
			_progress.y = 200;
			_progress.text = "LOADING...";
			add(_progress);
		}
		
		private function handleDirSelect(evt:Event):void
		{
			File.documentsDirectory.removeEventListener(Event.SELECT, handleDirSelect);
			archiveDirectory = (evt.target as File);
			loadMusic();
		}
		
		private function loadMusic():void
		{
			music.addEventListener(VVVVMusic.LOAD_PROGRESS, handleLoadProgress);
			music.addEventListener(VVVVMusic.LOAD_COMPLETE, handleLoadComplete);
			music.load(archiveDirectory.resolvePath("vvvvvvmusic.vvv").url);
		}
		
		private function handleLoadProgress(e:ProgressEvent):void 
		{
			var percentage:uint = e.bytesLoaded * 100 / e.bytesTotal;
			_progress.text = "LOADING... " + percentage + "%";
		}
		
		private function handleLoadComplete(evt:Event):void
		{
			music.removeEventListener(VVVVMusic.LOAD_PROGRESS, handleLoadProgress);
			music.removeEventListener(VVVVMusic.LOAD_COMPLETE, handleLoadComplete);
			
			FlxG.switchState(new MenuState());
		}
	}
}