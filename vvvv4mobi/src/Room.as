package  
{
	import entity.WarpLine;
	
	import org.flixel.FlxRect;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Room
	{
		public static const WARP_DIR_NONE:uint = 0;
		public static const WARP_DIR_HORIZONTAL:uint = 1;
		public static const WARP_DIR_VERTICAL:uint = 2;
		public static const WARP_DIR_ALL:uint = WARP_DIR_HORIZONTAL | WARP_DIR_VERTICAL;
		
		public var index:uint;
		public var name:String;
		public var data:Array = [];//new Vector.<uint>();
		public var entities:Vector.<BaseEntity> = new Vector.<BaseEntity>();
		public var platformBound:FlxRect = new FlxRect();
		public var enemyBound:FlxRect = new FlxRect();
		public var enemyType:uint;
		public var tileSet:uint;
		public var tileColor:uint;
		public var warpDir:uint;
		public var warpBounds:Array = [[], [], [], []];
		
		public function setWarpDir(dir:uint):void 
		{
			warpDir = dir;
			if ((dir & WARP_DIR_HORIZONTAL) > 0)
			{
				warpBounds[WarpLine.DIR_LEFT] = warpBounds[WarpLine.DIR_RIGHT] = 
					Util.makeIntArray(0, VVVVGame.SCREEN_TILE_ROWS);
			}
			if ((dir & WARP_DIR_VERTICAL) > 0)
			{
				warpBounds[WarpLine.DIR_TOP] = warpBounds[WarpLine.DIR_BOTTOM] = 
					Util.makeIntArray(0, VVVVGame.SCREEN_TILE_COLS);
			}
		}
		
		public function isWarping(dir:uint, i:int):Boolean 
		{
			return warpBounds[dir].indexOf(i) >= 0;
		}
	}

}