package  
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Assets
	{
		[Embed(source="../assets/sprites.png")]
		public static const SPRITES:Class;
		
		// 0 = Space Station Tileset (stars move left)
		[Embed(source="../assets/tiles.png")]
		public static const TILES:Class;
		
		// 1 = Outside Tileset (stars move left)
		// 2 = Lab Tileset (rectangles move right and down)
		// 3 = Warp Zone Tileset (stars move up)
		// 4 = Ship Tileset (stars move left)
		[Embed(source="../assets/tiles2.png")]
		public static const TILES2:Class;

		[Embed(source="../assets/font.png")]
		public static const FONT:Class;

		[Embed(source="../assets/entcolours.png")]
		public static const ENTITY_COLORS:Class;
		
		[Embed(source="../assets/sounds/jump.mp3")]
		public static const SOUND_JUMP:Class;
		
		[Embed(source="../assets/sounds/jump2.mp3")]
		public static const SOUND_JUMP2:Class;
		
		[Embed(source="../assets/sounds/hurt.mp3")]
		public static const SOUND_HURT:Class;
		
		[Embed(source="../assets/sounds/vanish.mp3")]
		public static const SOUND_VANISH:Class;
		
		[Embed(source="../assets/sounds/teleport.mp3")]
		public static const SOUND_TELEPORT:Class;
		
		[Embed(source="../assets/sounds/terminal.mp3")]
		public static const SOUND_TERMINAL:Class;
		
		[Embed(source="../assets/sounds/save.mp3")]
		public static const SOUND_SAVE:Class;
		
		[Embed(source="../assets/sounds/souleyeminijingle.mp3")]
		public static const SOUND_SOULEYEMINIJINGLE:Class;
		
		[Embed(source="../assets/sounds/rescue.mp3")]
		public static const SOUND_RESCUE:Class;
		
		[Embed(source="../assets/sounds/preteleport.mp3")]
		public static const SOUND_PRETELEPORT:Class;
		
		[Embed(source="../assets/sounds/blip.mp3")]
		public static const SOUND_BLIP:Class;
		
		[Embed(source="../assets/sounds/blip2.mp3")]
		public static const SOUND_BLIP2:Class;
		
		[Embed(source="../assets/sounds/crew1.mp3")]
		public static const SOUND_CREW1:Class;
	}

}