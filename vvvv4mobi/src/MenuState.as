package
{
	import com.adobe.serialization.json.JSON;
	import org.flixel.FlxU;
	
	import flash.events.Event;
	import flash.net.FileFilter;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	
	import ui.SwipeList;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class MenuState extends FlxState
	{
		public var levels:Array;
		private var _lstLevel:SwipeList = new SwipeList(VVVVGame.SCREEN_HEIGHT);
		private var _txtTitle:FlxBitmapFont = Util.createVVVVText("VVVVVV Remake");
		private var _txtCreator:FlxBitmapFont = Util.createVVVVText("by IndieST");
		private var _txtDesc:FlxBitmapFont = Util.createVVVVText("<- Slide up/down to scroll list");
		private var _btnContinue:FlxButtonPlus;
		private var _btnStart:FlxButtonPlus;
		private var _btnHighscore:FlxButtonPlus;
		private var _lstHighscore:SwipeList = new SwipeList(120);
		
		public override function create():void
		{			
//			showFileList(LoadingState.archiveDirectory);
			
			// Play "Presenting VVVVVV"
			VVVVGame.playMusic(5);
//			add(LoadingState.music.sound);
			
			FlxG.camera.x = 0;
			
			if (VVVVGame.menuState == null)
			{
				VVVVGame.menuState = this;
				Util.httpRequest("levels.json", null, URLLoaderDataFormat.TEXT, handleLoadComplete);
			}
		}
		
		override public function destroy():void 
		{
			// Blocks the super class's destroy logic to make MenuState reusable.
			//super.destroy();
		}
		
		private function handleLoadComplete(data:String, error:String):void
		{
			levels = com.adobe.serialization.json.JSON.decode(data);
			
			var head:FlxBitmapFont = Util.createVVVVText("Local levels:");
			head.y = 4;
			add(head);			
			_lstLevel.y = head.y + 8;
			_lstLevel.setFixedWidth(124);
			_lstLevel.indentX = 4;
			_lstLevel.indentY = 7;
			_lstLevel.customSpacingY = 14;
			_lstLevel.color = 0xb8b8ed;
			_lstLevel.setSelectCallback(onLevelSelected);
			add(_lstLevel);
			
			_txtTitle.x = 200;
			_txtTitle.y = 16;
			_txtTitle.scale.make(2, 2);
			//_txtTitle.setFixedWidth(240, FlxBitmapFont.ALIGN_CENTER);
			add(_txtTitle);
			_txtCreator.x = 130;
			_txtCreator.y = 40;
			_txtCreator.setFixedWidth(270, FlxBitmapFont.ALIGN_CENTER);
			add(_txtCreator);
			_txtDesc.x = 130;
			_txtDesc.y = 60;
			_txtDesc.setFixedWidth(270, FlxBitmapFont.ALIGN_CENTER);
			add(_txtDesc);
			_btnContinue = new FlxButtonPlus(130, 200, playLevel, null, "CONTINUE", 80, 20);
			_btnContinue.visible = false;
			_btnContinue.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
			_btnContinue.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
			add(_btnContinue);
			_btnStart = new FlxButtonPlus(220, 200, playLevel, [true], "NEW GAME", 80, 20);
			_btnStart.visible = false;
			_btnStart.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
			_btnStart.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
			add(_btnStart);
			_btnHighscore = new FlxButtonPlus(310, 200, loadHighscore, null, "HIGH SCORE", 80, 20);
			_btnHighscore.visible = false;
			_btnHighscore.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
			_btnHighscore.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
			add(_btnHighscore);
			_lstHighscore.x = 130;
			_lstHighscore.y = 70;
			_lstHighscore.indentY = 4;
			_lstHighscore.customSpacingY = 4;
			_lstHighscore.color = 0xb8b8ed;
			_lstHighscore.visible = false;
			add(_lstHighscore);

//			var y:int = head.y + 16;
			var items:Array = [];
			for each (var level:Object in levels)
			{
				items.push(Util.getFixedString(level.Title, 15));
				//var levelBtn:FlxButtonPlus = new FlxButtonPlus(16, y, playLevel,
					//["levels/" + level.File, level.Title], level.Title, 100, 16);
				//levelBtn.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
				//levelBtn.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
				//y += 20;
				//add(levelBtn);
			}
			_lstLevel.items = items;
		}
		
		public function getSelectedLevel():Object
		{
			return levels[_lstLevel.selectedIndex];;
		}
		
		private function onLevelSelected():void
		{
			var level:Object = getSelectedLevel();
			_txtTitle.text = level.Title;
			_txtCreator.text = "by " + level.Creator;
			_txtDesc.text = level.Desc;
			try
			{
				_btnContinue.visible = (SharedObject.getLocal(level.File).size > 0);
			}
			catch (e:Error)
			{
				trace(e);
			}
			_btnStart.visible = true;
			_btnHighscore.visible = true;
			_lstHighscore.visible = false;
		}
		
//		private function showFileList(vvvDir:File):void
//		{
//			var files:Array = vvvDir.getDirectoryListing();
//			var head:FlxBitmapFont = Util.createVVVVText("Local levels:");
//			head.y = 4;
//			var y:int = head.y + 16;
//			add(head);
//			
//			for each (var file:File in files)
//			{
//				if (file.extension == "vvvvvv")
//				{
//					var levelName:String = file.name.replace(".vvvvvv", "");
//					var levelBtn:FlxButtonPlus = new FlxButtonPlus(16, y, playLevel,
//						[file.url, levelName], levelName, 100, 16);
//					levelBtn.updateInactiveButtonColors([0xff000000, 0xffb8b8ed]);
//					levelBtn.updateActiveButtonColors([0xffb8b8ed, 0xff000000]);
//					y += 20;
//					add(levelBtn);
//				}
//			}
//		}

		private function playLevel(restart:Boolean = false):void
		{
			var level:Object = levels[_lstLevel.selectedIndex];
			var playState:PlayState = new PlayState("levels/" + level.File,
				level.File, restart);
			FlxG.switchState(playState);
		}
		
		public function loadHighscore():void
		{
			_txtDesc.visible = true;
			_txtDesc.text = "Loading high scores...";
			var level:Object = getSelectedLevel();
			Util.httpRequest(VVVVGame.HIGHSCORE_URL + "/" + level.File, null, URLLoaderDataFormat.TEXT, handleHighscoreLoaded);
		}
		
		internal function handleHighscoreLoaded(data:String, error:String):void 
		{
			if (error != null)
			{
				_txtDesc.text = error;
				return;
			}
			var highscores:Array;
			try
			{
				highscores = com.adobe.serialization.json.JSON.decode(data) as Array;
			}
			catch (err:Error)
			{
				_txtDesc.text = data;
				return;
			}
			showHighscore(highscores);
		}
		
		public function showHighscore(data:Array):void
		{
			data.sortOn(["playTime", "deathcounts"], Array.NUMERIC);
			_txtDesc.text = "PLAYER     TIME   DEATHS TRINKETS\n\n";
			var items:Array = [];
			for each (var highscore:Object in data)
			{
				//text += printf("%s %s %d %d/%d\n", highscore.playerName, FlxU.formatTime(highscore.playTime / 1000), highscore.deathcounts, highscore.trinkets, highscore.trinketsTotal);
				items.push(Util.getFixedString(highscore.playerName, 11) + 
						Util.getFixedString(Util.formatTime(highscore.playTime), 9) + 
						Util.getFixedString(highscore.deathcounts, 6) + 
						Util.getFixedString(highscore.trinkets + "/" + highscore.trinketsTotal, 5));
			}
			_lstHighscore.items = items;
			_lstHighscore.visible = true;
		}
	}
}