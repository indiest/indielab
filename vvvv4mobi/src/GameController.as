package
{
	import org.flixel.FlxGame;

	public interface GameController
	{
		function plugIn():void;
		
		function pullOut():void;
	}
}