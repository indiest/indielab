package
{
	[Deprecated]
	public class QuickSave
	{
		public var savex:int;
		public var savey:int;
		public var saverx:int;
		public var savery:int;
		public var savegc:int;
		public var savedir:int;
		public var trinkets:uint;
		public var collect:Vector.<uint>;
		public var crewmates:uint;
		public var crewstats:Vector.<uint>;
		
		public function saveTo(obj:Object):void
		{
			for (var key:* in this)
			{
				obj[key] = this[key];
			}
		}
		
		public function loadFrom(obj:Object):void
		{
			for (var key:* in obj)
			{
				this[key] = obj[key];
			}
		}
	}
}