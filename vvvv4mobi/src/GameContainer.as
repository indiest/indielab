package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.AccelerometerEvent;
	import flash.events.MouseEvent;
	import flash.sensors.Accelerometer;
	import flash.system.Capabilities;
	import flash.system.TouchscreenType;
	import flash.ui.Keyboard;
	
	import net.hires.debug.Stats;
	
	import org.flixel.FlxG;
	
	public class GameContainer extends Sprite
	{
		public function GameContainer()
		{			
			var screenWidth:uint = Math.max(Capabilities.screenResolutionX, Capabilities.screenResolutionY);
			var screenHeight:uint = Math.min(Capabilities.screenResolutionX, Capabilities.screenResolutionY);
			// Add virtual keys to screen if it's touchable
			if (Capabilities.touchscreenType == TouchscreenType.FINGER)
			{
				FlxG.mobile = false;
				
				stage.align = StageAlign.TOP;
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.setOrientation(StageOrientation.ROTATED_LEFT);
				
				
				var leftBtn:Sprite = new Sprite();
				leftBtn.graphics.beginFill(0x00ff00);
				leftBtn.graphics.drawRect(0, 0, (screenWidth - 640)/2, screenHeight);
				leftBtn.graphics.endFill();
				addChild(leftBtn);
				leftBtn.addEventListener(MouseEvent.MOUSE_DOWN, function(evt:MouseEvent):void
				{
					FlxG.keys.A = true;
				});				
				leftBtn.addEventListener(MouseEvent.MOUSE_UP, function(evt:MouseEvent):void
				{
					FlxG.keys.A = false;
				});
				
				var rightBtn:Sprite = new Sprite();
				rightBtn.x = (screenWidth + 640)/2;
				rightBtn.graphics.beginFill(0x0000ff);
				rightBtn.graphics.drawRect(0, 0, (screenWidth - 640)/2, screenHeight);
				rightBtn.graphics.endFill();
				addChild(rightBtn);
				rightBtn.addEventListener(MouseEvent.MOUSE_DOWN, function(evt:MouseEvent):void
				{
					FlxG.keys.D = true;
				});
				rightBtn.addEventListener(MouseEvent.MOUSE_UP, function(evt:MouseEvent):void
				{
					FlxG.keys.D = false;
				});
			}
			
			if (Accelerometer.isSupported)
			{
				var acc:Accelerometer = new Accelerometer();
				var accY:Number = 0;
				acc.addEventListener(AccelerometerEvent.UPDATE, function(evt:AccelerometerEvent):void
				{
					if (Math.abs(evt.accelerationY - accY) > 0.2)
						FlxG.keys.keyDown(Keyboard.W);
					else
						FlxG.keys.keyUp(Keyboard.W);
					accY = evt.accelerationY;
				});
			}
			
			var game:VVVVGame = new VVVVGame();
			game.x = (screenWidth - 640)/2;
			addChild(game);
			
			addChild(new Stats());

		}
	}
}