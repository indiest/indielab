package  
{
	import entity.*;
	import org.flixel.FlxObject;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class EntityType
	{
		public static const ENEMIES:uint = 1;
		public static const MOVING:uint = 2;
		public static const CONVEYORS:uint = 2;
		public static const DISAPPEAR:uint = 3;
		public static const TRINKETS:uint = 9;
		public static const CHECKPOINT:uint = 10;
		public static const GRAV_LINE:uint = 11;
		public static const WARP_TOKEN:uint = 13;
		public static const CREWMATE:uint = 15;
		public static const START_POINT:uint = 16;
		public static const ROOMTEXT:uint = 17;
		//public static const TERMINAL:uint = 18;
		//public static const SCRIPT_BOX:uint = 19;
		public static const WARP_LINES:uint = 50;
		
		public static const ENTITY_CLASSES:Object = 
		{
			1: Enemy,
			2: Moving,
			3: Disappear,
			9: Trinket,
			10: Checkpoint,
			11: GravLine,
			13: WarpToken,
			15: Crewmate,
			16: StartPoint,
			17: Roomtext,
			18: Terminal,
			19: ScriptBox,
			50: WarpLine
		};
		
		public static const PRICK_TILE_VALUES:Array = [6, 7, 8, 9, 49, 50];
		
		public static const SPIKE_TILE_COLLISIONS:Object =
		{
			"LEFT"/*0x1101*/: [50, 52, 54, 56, 58, 60, 62],//LEFT
			"RIGHT"/*0x1110*/: [49, 51, 53, 55, 57, 59, 61],//RIGHT
			"UP"/*0x0111*/: [6, 8, 63, 65, 67, 69, 71, 73],//UP
			"DOWN"/*0x1011*/: [7, 9, 64, 66, 68, 70, 72, 74] //DOWN
		};
		
		public static const WALL_TILE_VALUES:Array = [];
	}

}