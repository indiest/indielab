package
{
	import flash.utils.ByteArray;

	public class MusicInfo
	{
		public var name:String;
		public var offset:uint;
		public var size:uint;
	}
}