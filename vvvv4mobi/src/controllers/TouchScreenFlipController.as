package controllers
{
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	import org.flixel.FlxPoint;
	
	public class TouchScreenFlipController implements GameController
	{
//		private var _point:FlxPoint = new FlxPoint();
		public function plugIn():void
		{
			FlxG.stage.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			FlxG.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
		
		private function handleMouseDown(evt:MouseEvent):void
		{
//			_point.make(evt.stageX, evt.stageY);
//			if (FlxG.worldBounds.contains(_point))
//			{
				FlxG.keys.keyDown(Keyboard.UP);
//			}
		}
		
		private function handleMouseUp(evt:MouseEvent):void
		{
//			_point.make(evt.stageX, evt.stageY);
//			if (FlxG.worldBounds.contains(_point))
//			{
			FlxG.keys.keyUp(Keyboard.UP);
//			}
		}
		
		public function pullOut():void
		{
			FlxG.stage.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			FlxG.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
	}
}