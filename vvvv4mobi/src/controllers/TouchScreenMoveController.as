package controllers
{
	import flash.events.MouseEvent;
	
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	
	public class TouchScreenMoveController implements GameController
	{
		
		public function plugIn(game:FlxGame):void
		{
			FlxG.stage.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			FlxG.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
		
		private function handleMouseDown(evt:MouseEvent):void
		{
			if (evt.stageX < FlxG.stage.stageWidth / 2)
				FlxG.keys.LEFT = true;
			else
				FlxG.keys.RIGHT = true;
		}
		
		private function handleMouseUp(evt:MouseEvent):void
		{
			if (evt.stageX < FlxG.stage.stageWidth / 2)
				FlxG.keys.LEFT = false;
			else
				FlxG.keys.RIGHT = false;
		}
		
		public function pullOut(game:FlxGame):void
		{
			FlxG.stage.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			FlxG.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		}
	}
}