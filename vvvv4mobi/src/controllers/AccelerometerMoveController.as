package controllers
{
	import flash.events.AccelerometerEvent;
	import flash.ui.Keyboard;
	
	import org.flixel.FlxG;

	public class AccelerometerMoveController extends AccelerometerController
	{
		public var flipSeneitivity:Number;
		
		public function AccelerometerMoveController(sensitivity:Number = 0.25, flipSeneitivity:Number = 0.5)
		{
			super(sensitivity);
			this.flipSeneitivity = flipSeneitivity;
		}
		
		protected override function handleAccUpdate(evt:AccelerometerEvent):void
		{
			FlxG.keys.RIGHT = (evt.accelerationX <= -sensitivity);
			FlxG.keys.LEFT = (evt.accelerationX >= sensitivity);
			var state:PlayState = FlxG.state as PlayState;
			if (state != null)
			{
				var captain:Captain = state.captain;
//				FlxG.keys.UP = (state.captain.acceleration.y > 0 && evt.accelerationY <= sensitivity)
//					|| (state.captain.acceleration.y < 0 && evt.accelerationY > sensitivity);

				if (FlxG.keys.UP)
					FlxG.keys.keyUp(Keyboard.UP);
				if ((state.captain.wasTouching & 0x1100) > 0)
				{
					if (state.captain.acceleration.y > 0)
					{
						if (/*lastY > flipSeneitivity && */evt.accelerationY < sensitivity)
						{
							FlxG.keys.keyDown(Keyboard.UP);
						}
					}
					else
					{
						if (/*lastY < flipSeneitivity && */evt.accelerationY > sensitivity)
						{
							FlxG.keys.keyDown(Keyboard.UP);
						}
					}
				}
				
				super.handleAccUpdate(evt);
			}
			
		}
	}
}