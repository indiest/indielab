package controllers
{
	import flash.events.AccelerometerEvent;
	import flash.ui.Keyboard;
	
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	
	public class AccelerometerFlipController extends AccelerometerController
	{
		public function AccelerometerFlipController(sensitivity:Number = 0.3)
		{
			super(sensitivity);
		}
		
		protected override function handleAccUpdate(evt:AccelerometerEvent):void
		{
			if (Math.abs(evt.accelerationZ - lastZ) > sensitivity)
				FlxG.keys.keyDown(Keyboard.UP);
			else
				FlxG.keys.keyUp(Keyboard.UP);
			
			super.handleAccUpdate(evt);
		}
	}
}