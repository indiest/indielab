package controllers
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	
	public class VirtualKeyController implements GameController
	{
		public var keys:Vector.<VirtualKey> = new Vector.<VirtualKey>();
		public var keyButtons:Vector.<Sprite> = new Vector.<Sprite>();
		
		public function VirtualKeyController(virtualKeys:Array)
		{
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			for each (var key:VirtualKey in virtualKeys)
			{
				var keyBtn:Sprite = new Sprite();
				keyBtn.x = key.x;
				keyBtn.y = key.y;
				var s:Shape = new Shape();
				s.graphics.beginFill(key.color, key.alpha);
				s.graphics.drawCircle(0, 0, key.size);
				s.graphics.endFill();
				keyBtn.addChild(s);
				var t:TextField = new TextField();
				t.type = TextFieldType.DYNAMIC;
				var tf:TextFormat = t.defaultTextFormat;
				tf.size = key.size / key.label.length;
				tf.color = key.labelColor;
				tf.bold = true;
				t.defaultTextFormat = tf;
				t.text = key.label;
				t.selectable = false;
				t.width = t.textWidth + 4;
				t.height = t.textHeight + 4;
				t.x = -t.width/2;
				t.y = -t.height/2;
				keyBtn.addChild(t);
				keyBtn.cacheAsBitmap = true;
				keyBtn.mouseChildren = false;
//				keyBtn.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
//				keyBtn.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
//				keyBtn.addEventListener(MouseEvent.MOUSE_OUT, handleMouseUp);
				keyBtn.addEventListener(TouchEvent.TOUCH_BEGIN, handleMouseDown);
				keyBtn.addEventListener(TouchEvent.TOUCH_END, handleMouseUp);
//				keyBtn.addEventListener(TouchEvent.TOUCH_ROLL_OUT, handleMouseUp);
				keys.push(key);
				keyButtons.push(keyBtn);
			}
		}
		
		private function handleMouseDown(evt:Event):void
		{
			var keyBtn:Sprite = evt.target as Sprite;
			var index:int = keyButtons.indexOf(keyBtn);
			var key:VirtualKey = keys[index];
			FlxG.keys.keyDown(key.keyCode);
			if (key.keyDownCallback != null)
				key.keyDownCallback();
		}
		
		private function handleMouseUp(evt:Event):void
		{
			var keyBtn:Sprite = evt.target as Sprite;
			var index:int = keyButtons.indexOf(keyBtn);
			var key:VirtualKey = keys[index];
			FlxG.keys.keyUp(key.keyCode);
			if (key.keyUpCallback != null)
				key.keyUpCallback();
		}
		
		public function plugIn():void
		{
			for each (var keyBtn:Sprite in keyButtons)
				FlxG.stage.addChild(keyBtn);
		}
		
		public function pullOut():void
		{
			for each (var keyBtn:Sprite in keyButtons)
			{
				if (keyBtn.parent != null)
					keyBtn.parent.removeChild(keyBtn);
			}
		}
	}
}