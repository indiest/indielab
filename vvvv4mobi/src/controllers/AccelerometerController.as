package controllers
{
	import flash.events.AccelerometerEvent;
	import flash.sensors.Accelerometer;
	import flash.ui.Keyboard;
	
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	
	public class AccelerometerController implements GameController
	{
		protected var _acc:Accelerometer;
		public var sensitivity:Number;
		public var lastX:Number;
		public var lastY:Number;
		public var lastZ:Number;
		
		public function AccelerometerController(sensitivity:Number)
		{
			if (Accelerometer.isSupported)
			{
				_acc = new Accelerometer();
			}
			this.sensitivity = sensitivity;
		}
		
		public function plugIn(game:FlxGame):void
		{
			if (_acc != null)
				_acc.addEventListener(AccelerometerEvent.UPDATE, handleAccUpdate);
		}
		
		protected function handleAccUpdate(evt:AccelerometerEvent):void
		{
			lastX = evt.accelerationX;
			lastY = evt.accelerationY;
			lastZ = evt.accelerationZ;
		}
		
		public function pullOut():void
		{
			if (_acc != null)
				_acc.removeEventListener(AccelerometerEvent.UPDATE, handleAccUpdate);
		}
	}
}