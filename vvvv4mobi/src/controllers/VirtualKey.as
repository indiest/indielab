package controllers
{
	public class VirtualKey
	{
		public static const DEFAULT_SIZE:uint = 40;
		
		public var keyCode:uint;
		public var x:Number;
		public var y:Number;
		public var size:Number;
		public var color:uint;
		public var alpha:Number;
		public var label:String;
		public var labelColor:uint;
		public var keyDownCallback:Function;
		public var keyUpCallback:Function;
		
		public function VirtualKey(keyCode:uint, x:Number, y:Number, label:String, labelColor:uint = 0xffffff,
				size:uint = DEFAULT_SIZE, color:uint = 0xaaaaaa, alpha:Number = 0.5, keyDownCallback:Function = null, keyUpCallback:Function = null)
		{
			this.keyCode = keyCode;
			this.x = x;
			this.y = y;
			this.label = label;
			this.labelColor = labelColor;
			this.size = size;
			this.color = color;
			this.alpha = alpha;
			this.keyDownCallback = keyDownCallback;
			this.keyUpCallback = keyUpCallback;
		}
	}
}