package  
{
	import org.flixel.FlxBasic;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	import org.flixel.FlxTilemap;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public interface BaseEntity //extends FlxBasic
	{
		//public function BaseEntity(x:Number, y:Number);
		
		function init(data:*, room:Room):void;
		
		function resetAll():void;
		
		//function checkOverlaps(obj:FlxObject):Boolean;
		
		//function onOverlaps(obj:FlxObject):void;
	}

}