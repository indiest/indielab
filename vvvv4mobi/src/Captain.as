package  
{
	import entity.Crewmate;
	import entity.Enemy;
	import entity.WarpLine;
	
	import flash.geom.ColorTransform;
	import flash.utils.getTimer;
	
	import org.flixel.FlxBasic;
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	import ui.MessageBox;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Captain extends FlxSprite
	{		
		private var _flipping:Boolean = false;
		public var paused:Boolean = false;
		public var happy:Boolean = true;

		public function Captain(x:Number = 0, y:Number = 0) 
		{
			super(x, y);
			
			loadGraphic(Assets.SPRITES, true, false, 32, 32);
			addAnimation("stand_right", [0]);
			addAnimation("walk_right", [1, 2], 10);
			addAnimation("stand_left", [3]);
			addAnimation("walk_left", [4, 5], 10);
			addAnimation("flip_stand_right", [6]);
			addAnimation("flip_walk_right", [7, 8]);
			addAnimation("flip_stand_left", [9]);
			addAnimation("flip_walk_left", [10, 11]);
			addAnimation("hurt_right", [12]);
			addAnimation("hurt_left", [13]);
			addAnimation("flip_hurt_right", [14]);
			addAnimation("flip_hurt_left", [15]);
			
			maxVelocity.x = 180;//240;
			maxVelocity.y = 300;//320;
			acceleration.y = 2400;
			drag.x = maxVelocity.x * 15;
			
			resetColor();
			this.width = 10;
			this.height = 21;
			this.offset.make(7, 2);
		}
		
		private function resetColor():void
		{
			var rgba:Array = FlxU.getRGBA(Crewmate.COLORS[0]);
			_colorTransform = new ColorTransform(0, 0, 0, 1.0, rgba[0], rgba[1], rgba[2]);
			dirty = true;
		}

		public function isOnSurface():Boolean
		{
			return (acceleration.y > 0 && isTouching(FlxObject.DOWN)) || acceleration.y < 0 && isTouching(FlxObject.UP);
		}
		
		public function get fixedX():Number
		{
			return x - offset.x;
		}
		
		public function get fixedY():Number
		{
			return y - offset.y;
		}

		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (moves)
			{
//				color = int(getTimer() / FlxG.flashFramerate) % 0x40 + 0xffffbf;
//				_colorTransform.greenOffset = 0x20;

				acceleration.x = 0;
					
				if (!paused)
				{
					if (FlxG.keys.LEFT || FlxG.keys.A)
					{
						facing = FlxObject.LEFT;
						acceleration.x = -this.maxVelocity.x * 15;
					}
					if (FlxG.keys.RIGHT || FlxG.keys.D)
					{
						facing = FlxObject.RIGHT;
						acceleration.x = this.maxVelocity.x * 15;
					}
					
					if (!_flipping && (FlxG.keys.UP || FlxG.keys.W) && isOnSurface())//velocity.y == 0)
					{
						_flipping = true;
						acceleration.y = -acceleration.y;
						FlxG.play(acceleration.y > 0 ? Assets.SOUND_JUMP : Assets.SOUND_JUMP2,
							1.0, false, false);
					}
					if (FlxG.keys.justReleased("UP") || FlxG.keys.justReleased("W"))
						_flipping = false;
					
					if (FlxG.keys.justPressed("ENTER"))
					{
						state.showMenu();
					}
					
					if (FlxG.keys.justPressed("ESCAPE"))
					{
						state.confirmExit();
					}
				}
			}
			
			var animName:String;
			if (!moves || !happy)
			{
				animName = "hurt";
			}
			else if (velocity.x == 0 && velocity.y == 0)
			{
				animName = "stand";
			}
			else
			{
				animName = "walk";
			}
			if (facing == FlxObject.LEFT)
			{
				animName += "_left";
			}
			else
			{
				animName += "_right";
			}
			if (acceleration.y < 0)
			{
				animName = "flip_" + animName;
			}
			
			play(animName);
			
			var roomX:int = state.roomX;//x / VVVVGame.SCREEN_WIDTH;
			var roomY:int = state.roomY;//y / VVVVGame.SCREEN_HEIGHT;
			
			if (x + width/2 < 0)
			{
				last.x = x = VVVVGame.SCREEN_WIDTH - width / 2;
				if (!state.currentRoom.isWarping(WarpLine.DIR_LEFT, y / VVVVGame.TILE_HEIGHT))
					roomX--;
			}
			else if (x > VVVVGame.SCREEN_WIDTH - width/2)
			{
				last.x = x = -width/2;
				if (!state.currentRoom.isWarping(WarpLine.DIR_RIGHT, y / VVVVGame.TILE_HEIGHT))
					roomX++;
			}
			if (y + height/2 < 0)
			{
				last.y = y = VVVVGame.SCREEN_HEIGHT - height/2;
				//velocity.y = 0;
				if (!state.currentRoom.isWarping(WarpLine.DIR_TOP, x / VVVVGame.TILE_WIDTH))
					roomY--;
			}
			else if (y > VVVVGame.SCREEN_HEIGHT - height/2)
			{
				last.y = y = -height/2;
				//velocity.y = 0;
				if (!state.currentRoom.isWarping(WarpLine.DIR_BOTTOM, x / VVVVGame.TILE_WIDTH))
					roomY++;
			}
			
			if (roomX < 0)
				roomX = state.mapWidth - 1;
			else if (roomX > state.mapWidth - 1)
				roomX = 0;
			if (roomY < 0)
				roomY = state.mapHeight - 1;
			else if (roomY > state.mapHeight - 1)
				roomY = 0;
			if (roomX != state.roomX || roomY != state.roomY)
			{
				state.roomX = roomX;
				state.roomY = roomY;
				state.loadRoom();
			}
		}
		
		public function transport(targetX:uint, targetY:uint):void
		{
			var state:PlayState = FlxG.state as PlayState;
			state.roomX = targetX / VVVVGame.SCREEN_TILE_COLS;
			state.roomY = targetY / VVVVGame.SCREEN_TILE_ROWS;
			
			state.captain.x = targetX * VVVVGame.TILE_WIDTH % VVVVGame.SCREEN_WIDTH;
			state.captain.y = targetY * VVVVGame.TILE_HEIGHT % VVVVGame.SCREEN_HEIGHT;
		}
		
		public function die():void
		{
			if (!flickering)
			{
				// FIXME: The whole world stops except the flickering
				moves = false;

				var state:PlayState = FlxG.state as PlayState;
				state.deathcounts++;
				
				flicker(1.0, revive);
				// Tint captain to RED
				_colorTransform.color = 0xffffff;
				_colorTransform.redOffset = 0xff;
				_colorTransform.greenOffset = 0x00;
				_colorTransform.blueOffset = 0x00;
				dirty = true;
				FlxG.play(Assets.SOUND_HURT, 1.0, false, false);
			}
		}

		override public function revive():void 
		{
			touching = NONE;
			wasTouching = NONE;
			acceleration.y = FlxU.abs(acceleration.y);
			moves = true;
			resetColor();

			var state:PlayState = FlxG.state as PlayState;
			var saveData:Object = state.quickSave;
			if (saveData == null)
			{
				state.restart();
				state.loadRoom();
			}
			else
			{
				x = saveData.savex;
				y = saveData.savey;
				last.x = x;
				last.y = y;
				acceleration.y *= (saveData.savegc == 0 ? 1: -1);
				facing = (saveData.savedir == 0 ? FlxObject.LEFT : FlxObject.RIGHT);
				state.roomX = saveData.saverx;
				state.roomY = saveData.savery;
				state.loadRoom();
			}
		}
	}

}