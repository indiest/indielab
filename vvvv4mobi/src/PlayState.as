package
{
	import bg.HVWarpBg;
	import bg.HWarpBg;
	import bg.LabBg;
	import bg.SpaceBg;
	import bg.VWarpBg;
	
	import com.adobe.serialization.json.JSON;
	
	import entity.Checkpoint;
	
	import flash.display.BitmapData;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	
	import org.flixel.FlxBasic;
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSave;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import org.flixel.FlxTilemap;
	import org.flixel.FlxTimer;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import org.flixel.system.FlxTile;
	import org.flixel.system.FlxWindow;
	
	import script.ScriptObject;
	
	import ui.InputBox;
	import ui.MessageBox;
	import ui.SelectBox;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PlayState extends FlxState
	{	
		private var _bg:FlxBasic = null;
		private var _spaceBg:SpaceBg = new SpaceBg();
		private var _warpBgs:Array = [null, new HWarpBg(), new VWarpBg(), new HVWarpBg()];
		private var _labBg:LabBg = new LabBg();
		private var _roomNameText:FlxBitmapFont = Util.createVVVVText();
		private var _topText:FlxBitmapFont = Util.createVVVVText();
		private var _tilemap:FlxTilemap = new FlxTilemap();
		private var _entityGroup:FlxGroup = new FlxGroup();
		private var _levelUrl:String;
		private var _levelData:XML;
		private var _tileData:Vector.<Vector.<Room>> = new Vector.<Vector.<Room>>();
		private var _scripts:Object = { };
		private var _scriptContext:Object = { };
		private var _captain:Captain = new Captain();
		public var mapWidth:uint;
		public var mapHeight:uint;
		public var roomX:uint = 0;
		public var roomY:uint = 0;
		public var quickSave:Object;
		public var activatedCheckpoint:Checkpoint;
		public var save:FlxSave = new FlxSave();
		public var startPoint:FlxPoint;
		public var startFacing:uint;
		public var trinketsTotal:uint = 0;
		public var trinkets:uint = 0;
		public var trinketsCollect:Vector.<uint> = new Vector.<uint>();
		public var crewmatesTotal:uint = 0;
		public var crewmates:uint = 0;
		public var crewmatesStats:Vector.<uint> = new Vector.<uint>();
		public var deathcounts:uint = 0;
		public var playTime:int = getTimer();
		
		public function switchBg(bg:FlxBasic):void
		{
			if (_bg == bg)
				return;
			remove(_bg);
			_bg = bg;
			add(_bg);
		}
		
		public function PlayState(levelUrl:String, saveName:String, restart:Boolean = false)
		{
			_levelUrl = levelUrl;
			save.bind(saveName);
			if (!restart && save.data != null && save.size > 0)
			{
				quickSave = Util.deepClone(save.data);
				trinkets = quickSave.trinkets;
				trinketsCollect = quickSave.collect;
				crewmates = quickSave.crewmates;
				crewmatesStats = quickSave.crewstats;
				deathcounts = quickSave.deathcounts;
			}
		}
		
		public function get captain():Captain
		{
			return _captain;
		}
		
		public function get tilemap():FlxTilemap
		{
			return _tilemap;
		}
		
		public function get currentRoom():Room
		{
			return  _tileData[roomY][roomX] as Room;
		}
		
		override public function create():void 
		{
			//trace("PlayState#create");
			VVVVGame.center();
			VVVVGame.plugInControllers();
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, handleLevelLoaded);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, handlerLoadFailed);
			urlLoader.load(new URLRequest(
				_levelUrl));
				//"levels/linewrap.vvvvvv"));
				//"C:/Users/boboibobo/Documents/VVVVVV/test.vvvvvv"));
				//"D:/Documents/VVVVVV/123.vvvvvv"));
		}
		
		private function handlerLoadFailed(e:IOErrorEvent):void 
		{
			throw new IOError(e.toString());
		}
		
		private function handleLevelLoaded(e:Event):void
		{
			var urlLoader:URLLoader = e.target as URLLoader;
			urlLoader.removeEventListener(Event.COMPLETE, handleLevelLoaded);
			_levelData = new XML(urlLoader.data);
			//trace(levelData.Data.contents);
			var contents:String = _levelData.Data.contents;
			var arr:Array = contents.split(",");
			mapWidth = uint(_levelData.Data.mapwidth);
			mapHeight = uint(_levelData.Data.mapheight);
			var cols:uint = mapWidth * VVVVGame.SCREEN_TILE_COLS;//20*40
			var rows:uint = mapHeight * VVVVGame.SCREEN_TILE_ROWS;//20*30
			
			var musicNum:uint = uint(_levelData.Data.levmusic);
			if (musicNum > 0)
				VVVVGame.playMusic(musicNum);
			
			var i:int = -1;
			var room:Room;
			for (var y:uint = 0; y < mapHeight; y++)
			{
				_tileData[y] = new Vector.<Room>();
				for (var x:uint = 0; x < VVVVGame.MAX_MAP_WIDTH; x++)
				{
					i++;
					if (x >= mapWidth)
						continue;
					var edLevel:* = _levelData.Data.levelMetaData.edLevelClass[i];
					room = new Room();
					room.index = i;
					room.name = edLevel.text();
					room.tileSet = uint(edLevel.@tileset);
					room.tileColor = uint(edLevel.@tilecol);
					var x1:int = int(edLevel.@platx1);
					var y1:int = int(edLevel.@platy1);
					var x2:int = int(edLevel.@platx2);
					var y2:int = int(edLevel.@platy2);
					room.platformBound.make(x1, y1, x2 - x1, y2 - y1);
					x1 = int(edLevel.@enemyx1);
					y1 = int(edLevel.@enemyy1);
					x2 = int(edLevel.@enemyx2);
					y2 = int(edLevel.@enemyy2);
					room.enemyBound.make(x1, y1, x2 - x1, y2 - y1);
					room.enemyType = uint(edLevel.@enemytype);
					room.setWarpDir(uint(edLevel.@warpdir));
					
					_tileData[y][x] = room;
				}
			}
			
			var data:String = "";
			var item:String;
			for (var index:uint = 0; index < arr.length - 1; index++)
			{

				var ry:uint = index / (mapWidth * VVVVGame.SCREEN_TILE_COLS * VVVVGame.SCREEN_TILE_ROWS);
				var rx:uint = (index / VVVVGame.SCREEN_TILE_COLS) % mapWidth;
				_tileData[ry][rx].data.push(uint(arr[index]));
				
//				data += (arr[index] + ",");
//				if (((index + 1) % cols) == 0)
//				{
//					data += "\n";
//				}
			}
//			trace(_tileData[roomY][roomX].data);
//			_tilemap.loadMap(data, Assets.TILES, VVVVGame.TILE_WIDTH, VVVVGame.TILE_HEIGHT, FlxTilemap.OFF, 0, 1, 40*30);
//			initTilemap();			
			
			for each(var edentity:* in _levelData.Data.edEntities.edentity)
			{
				//trace(edentity.@x, edentity.@y);
				var entityClass:Class = EntityType.ENTITY_CLASSES[edentity.@t];
				if (entityClass == null)
					continue;
				var entityX:int = int(edentity.@x);
				var entityY:int = int(edentity.@y);
				var ent:BaseEntity = new entityClass(
					(entityX % VVVVGame.SCREEN_TILE_COLS) * VVVVGame.TILE_WIDTH,
					(entityY % VVVVGame.SCREEN_TILE_ROWS) * VVVVGame.TILE_HEIGHT) as BaseEntity;
				room = _tileData[int(entityY / VVVVGame.SCREEN_TILE_ROWS)][int(entityX / VVVVGame.SCREEN_TILE_COLS)];
				ent.init(edentity, room);
				room.entities.push(ent);
			}
			
			parseScript();
			
			_captain.revive();
			//loadRoom();
			//add(_bg);
			add(_tilemap);
			add(_entityGroup);
			
			_roomNameText.y = VVVVGame.SCREEN_HEIGHT - VVVVGame.TILE_HEIGHT;
			add(_roomNameText);
			_topText.visible = false;
			add(_topText);
		}
		
		public function setTopText(text:String):void
		{
			_topText.visible = true;
			_topText.text = text;
			Util.centerX(_topText);
		}

		private function initTilemap():void
		{
			_tilemap.loadMap(FlxTilemap.arrayToCSV(currentRoom.data, VVVVGame.SCREEN_TILE_COLS),
				currentRoom.tileSet == 0 ? Assets.TILES : Assets.TILES2,
				VVVVGame.TILE_WIDTH, VVVVGame.TILE_HEIGHT, FlxTilemap.OFF, 0, 1, 40 * 30);
			
			_tilemap.setTileProperties(40 * 2, 0x1111, null, null, 40 * 15);
			
			setSpikeTiles();
		}
		
		public function loadRoom():void
		{
			//FlxG.camera.focusOn(new FlxPoint((roomX + 1) * VVVVGame.SCREEN_WIDTH, (roomY + 1) * VVVVGame.SCREEN_HEIGHT));
			if (currentRoom.warpDir == Room.WARP_DIR_NONE)
			{
				switch(currentRoom.tileSet)
				{
					case 0:
					case 1:
					case 4:
						_spaceBg.setDir(SpaceBg.DIR_MOVE_LEFT);
						switchBg(_spaceBg);
						break;
					case 2:
						_labBg.setColor(currentRoom.tileColor);
						switchBg(_labBg);
						break;
					case 3:
						_spaceBg.setDir(SpaceBg.DIR_MOVE_UP);
						switchBg(_spaceBg);
						break;
					default:
						break;
				}
			}
			else
			{
				switchBg(_warpBgs[currentRoom.warpDir]);
			}
			
			
			_roomNameText.text = currentRoom.name;
			_roomNameText.x = (VVVVGame.SCREEN_WIDTH - _roomNameText.width) / 2;
			
			initTilemap();

			_entityGroup.clear();
			for each (var ent:BaseEntity in currentRoom.entities)
			{
				ent.resetAll();
				_entityGroup.add(FlxBasic(ent));
			}
			_entityGroup.add(_captain);
		}
		
		private function setSpikeTiles():void
		{
			//EntityType.PRICK_TILE_VALUES.forEach(function(item:uint, index:uint, a:Array):void
			//{
			//_tilemap.setTileProperties(item, 0x1111, onCollidePrick, Captain);
			//});
			//
			//if (currentRoom.tileSet > 0)
			//_tilemap.setTileProperties(50, 0x1111, onCollidePrick, Captain, 30);
			
			for (var collision:* in EntityType.SPIKE_TILE_COLLISIONS)
			{
				for each(var tile:uint in EntityType.SPIKE_TILE_COLLISIONS[collision])
				{
					_tilemap.setTileProperties(tile, /*uint(collision)*/FlxObject.NONE, onCollideSpike, Captain);
				}
			}
		}
		
		private var _captainHitTestPoint:Point = new Point();
		private var _spikeHitTestPoint:Point = new Point();
		private var _spikes:FlxSprite;
		
		private function onCollideSpike(tile:FlxTile, obj:FlxObject):void
		{
			var dir:String = Util.lookupInObject(tile.index, EntityType.SPIKE_TILE_COLLISIONS);
			if (_spikes == null)
			{
				_spikes = new FlxSprite();
				_spikes.loadGraphic(Assets.TILES, true, false, VVVVGame.TILE_WIDTH, VVVVGame.TILE_HEIGHT);
				_spikes.addAnimation("UP", [8]);
				_spikes.addAnimation("DOWN", [9]);
				_spikes.addAnimation("RIGHT", [49]);
				_spikes.addAnimation("LEFT", [50]);
			}
			_spikes.play(dir);
			_spikes.drawFrame();
			
			_captainHitTestPoint.x = _captain.fixedX;
			_captainHitTestPoint.y = _captain.fixedY;
			_spikeHitTestPoint.x = tile.mapX;
			_spikeHitTestPoint.y = tile.mapY;
			//trace("spike:", _spikeHitTestPoint, "captain:", _captainHitTestPoint);
			if (_captain.framePixels.hitTest(_captainHitTestPoint, 1, 
					_spikes.framePixels, _spikeHitTestPoint, 1))
			{
				_captain.die();
			}
		}
		
		override public function update():void 
		{
			FlxG.collide(_entityGroup, _tilemap);
			//FlxG.overlap(_entityGroup, _tilemap, null, checkOverlaps);
			
			super.update();
		}
		
		private function checkOverlaps(obj1:FlxObject, obj2:FlxObject):Boolean
		{
			if (obj1 is FlxTile && obj2 is Captain)
			{
				onCollideSpike(obj1 as FlxTile, obj2);
				return false;
			}
			return FlxObject.separate(obj1, obj2);
		}
		
		public function restart():void
		{
			//trace("PlayState#restart");
			if (startPoint == null)
			{
				FlxG.resetState();
			}
			else
			{
				_captain.transport(startPoint.x, startPoint.y);
				_captain.facing = startFacing;
			}
		}
		
		public function showMenu():void
		{
			var musicStat:String = VVVVGame.musicAvailable ? "ON" : "OFF";
			var sfxStat:String = FlxG.mute ? "OFF" : "ON";
			SelectBox.show([ { text:"SAVE GAME" }, { text:"MUSIC: " + musicStat }, { text:"SOUND FX: " + sfxStat }, { text:"QUIT TO MENU" }, { text:"BACK TO GAME" } ],
				"VVVVVV", onSelectMenuItem);
		
		}
		
		public function backToMenuState():void
		{
			FlxG.switchState(VVVVGame.menuState);
		}
		
		private function onSelectMenuItem(index:uint, item:FlxButtonPlus):Boolean
		{
			switch(index)
			{
				case 0:
					saveGame();
					MessageBox.show("Game Saved!");
					break;
				case 1:
					VVVVGame.switchMusic();
					break;
				case 2:
					FlxG.mute = !FlxG.mute;
					FlxG.volume = FlxG.volume;
					break;
				case 3:
					backToMenuState();
					break;
				default:
					break;
			}
			return true;
		}
		
		public function confirmExit():void
		{
			if (SelectBox.isShowing())
			{
				_captain.paused = false;
				SelectBox.hide();
			}
			else
			{
				_captain.paused = true;
				SelectBox.show([{text:"YES, QUIT"}, {text:"NO"}], "QUIT TO MENU?", onConfirmExit);
			}
		}
		
		private function onConfirmExit(index:uint, item:*):Boolean
		{
			_captain.paused = false;
			if (index == 0)
			{
				backToMenuState();
			}
			return true;
		}
		
		public override function destroy():void
		{
			super.destroy();
			VVVVGame.pullOutControllers();
			FlxG.camera.x = 0;
		}
		
		public function quickSaveGame(cp:Checkpoint):void 
		{
			activatedCheckpoint = cp;
			if (quickSave == null)
			{
				quickSave = new Object();
				quickSave.saveToken = uint(Math.random() * uint.MAX_VALUE);
				quickSave.playTime = 0;
			}
			quickSave.savex = cp.x;
			quickSave.savey = cp.dir == 0 ? cp.y : cp.y - 8;
			quickSave.saverx = roomX;
			quickSave.savery = roomY;
			quickSave.savegc = captain.acceleration.y > 0 ? 0 : 1;
			quickSave.savedir = captain.facing == FlxObject.LEFT ? 0 : 1;
			quickSave.trinkets = trinkets;
			quickSave.collect = trinketsCollect;
			quickSave.crewmates = crewmates;
			quickSave.crewstats = crewmatesStats;
		}
		
		public function saveGame():void
		{
			quickSave.deathcounts = deathcounts;
			quickSave.playTime += (getTimer() - playTime);
			playTime = getTimer();
			save.data = quickSave;
			//trace(JSON.encode(quickSave));
		}
		
		public function levelComplete():void 
		{
			InputBox.show("Please submit your level score:", 10, "Your Name", onSubmitScore);
		}
		
		private function onSubmitScore(playerName:String):void
		{
			if (playerName != null)
			{
				var time:uint = getTimer() - playTime;
				var highscore:String = com.adobe.serialization.json.JSON.encode( {
					levelName: save.name,
					playerName: playerName,
					submitTime: uint(new Date().time),
					playTime: quickSave ? (quickSave.playTime + time) : time,
					deathcounts: deathcounts,
					trinkets: trinkets,
					trinketsTotal: trinketsTotal,
					saveToken: quickSave ? quickSave.saveToken : 0
				});
				Util.httpRequest(VVVVGame.HIGHSCORE_URL, highscore, URLLoaderDataFormat.TEXT,
					VVVVGame.menuState.handleHighscoreLoaded);			
			}
			backToMenuState();
		}
		
		private function parseScript():void 
		{
			var text:String = _levelData.Data.script.text();
			var reg:RegExp = /\w+[:][|]/g;
			var m0:Object = reg.exec(text);
			if (m0 == null)
				return;
			while (m0 != null)
			{
				var m:Object = reg.exec(text);
				var id:String = m0[0];
				var s:ScriptObject = new ScriptObject(id);
				_scripts[s.id] = s;
				if (m != null)
				{
					s.parse(text.substring(m0.index + id.length, m.index));
				}
				else
				{
					s.parse(text.substring(m0.index + id.length, text.length - 1));
				}
				m0 = m;
			}
		}
				
		public function executeScript(scriptName:String):void 
		{
			var s:ScriptObject = _scripts[scriptName + ":|"];
			if (s != null)
				s.execute(_scriptContext);
			else
				FlxG.log("Can't find script by name: " + scriptName);
		}
	}

}