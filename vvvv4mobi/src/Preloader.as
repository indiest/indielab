package
{
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import org.flixel.system.FlxPreloader;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Preloader extends FlxPreloader
	{
		public static var music:VVVVMusic = new VVVVMusic();
		
		protected override function create():void
		{
			super.create();
			music.addEventListener(VVVVMusic.LOAD_PROGRESS, handleLoadProgress);
			music.addEventListener(VVVVMusic.LOAD_COMPLETE, handleLoadComplete);
			music.load();
		}
		
		private function handleLoadProgress(e:ProgressEvent):void 
		{
			update(e.bytesLoaded / e.bytesTotal);
		}
		
		private function handleLoadComplete(evt:Event):void
		{
			music.removeEventListener(VVVVMusic.LOAD_PROGRESS, handleLoadProgress);
			music.removeEventListener(VVVVMusic.LOAD_COMPLETE, handleLoadComplete);
			music.play(0, false);
		}
	}

}