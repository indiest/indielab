package
{	
	import controllers.*;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.system.Capabilities;
	import flash.system.TouchscreenType;
	import flash.ui.Keyboard;
	
	import net.hires.debug.Stats;
	
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	import org.flixel.FlxSound;
	
	[SWF(width="800", height="480", backgroundColor="#000000")]
	//[Frame(factoryClass="Preloader")]
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class VVVVGame extends FlxGame
	{
		public static const SCREEN_WIDTH:uint = 320;
		public static const SCREEN_HEIGHT:uint = 240;
		public static var ZOOM:Number = 2;
		
		public static const TILE_WIDTH:uint = 8;
		public static const TILE_HEIGHT:uint = 8;
		public static const SCREEN_TILE_COLS:uint = SCREEN_WIDTH / TILE_WIDTH;
		public static const SCREEN_TILE_ROWS:uint = SCREEN_HEIGHT / TILE_HEIGHT;
		public static const MAX_MAP_WIDTH:uint = 20;
		
		public static const HIGHSCORE_URL:String = 
			//"http://127.0.0.1:1337";
			//"http://50.18.157.102:1337";
			"http://sangtian.me:1337";
			
		public static var menuState:MenuState;
		public static var mobile:Boolean;
		public static var screenWidth:uint;
		public static var screenHeight:uint;
		
		private static var _controllers:Vector.<GameController> = new Vector.<GameController>();
		
		public function VVVVGame()
		{
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			//FlxG.visualDebug = true;
			stage.addChild(new Stats());
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			
			var desktop:Boolean = (Capabilities.os.indexOf("Windows") >= 0);
			screenWidth = desktop ? stage.stageWidth : Capabilities.screenResolutionX;
			screenHeight = desktop ? stage.stageHeight : Capabilities.screenResolutionY;
			if (screenWidth < screenHeight)
			{
				var w:uint = screenWidth;
				screenWidth = screenHeight;
				screenHeight = w;
			}

			// Add virtual keys to screen if it's touchable
			if (Capabilities.touchscreenType == TouchscreenType.FINGER)
			{
				mobile = true;
//				FlxG.mobile = true;
//				stage.addEventListener(Event.DEACTIVATE, onFocusLost);
//				stage.addEventListener(Event.ACTIVATE, onFocus);
				// Zoom to full screen
				ZOOM = Math.min(screenWidth / SCREEN_WIDTH, screenHeight / SCREEN_HEIGHT);
				trace("ZOOM:", ZOOM);
				//stage.stageWidth = SCREEN_WIDTH * ZOOM;
				//stage.stageHeight = SCREEN_HEIGHT * ZOOM;

//				new TouchScreenMoveController().plugIn(this);
				
				_controllers.push(new VirtualKeyController([
					new VirtualKey(Keyboard.LEFT, VirtualKey.DEFAULT_SIZE, screenHeight - 50, "L"),
					new VirtualKey(Keyboard.RIGHT, VirtualKey.DEFAULT_SIZE * 3, screenHeight - 50, "R"),
					new VirtualKey(Keyboard.UP, screenWidth - VirtualKey.DEFAULT_SIZE, screenHeight - 50, "F"),
					new VirtualKey(Keyboard.ENTER, screenWidth - VirtualKey.DEFAULT_SIZE, VirtualKey.DEFAULT_SIZE, "M")
				]));
			}
			else
			{
				mobile = false;
			}
			
			super(stage.stageWidth / ZOOM, stage.stageHeight / ZOOM, MenuState, ZOOM, 60, 60, true);
			
			//_controllers.push(new TouchScreenFlipController());
			//_controllers.push(new AccelerometerMoveController());
			//plugInControllers();
		}
		
		public static function center():void
		{
			if (mobile)
				FlxG.camera.x = (screenWidth - SCREEN_WIDTH * ZOOM) / 2;
		}
		
		public static function plugInControllers():void
		{
			for each (var controller:GameController in _controllers)
			{
				controller.plugIn();
			}
		}
		
		public static function pullOutControllers():void
		{
			for each (var controller:GameController in _controllers)
			{
				controller.pullOut();
			}
		}
		
//		protected override function onFocus(FlashEvent:Event=null):void
//		{
//			super.onFocus(FlashEvent);
//			plugInControllers();
//		}
		
//		protected override function onFocusLost(FlashEvent:Event=null):void
//		{
//			super.onFocusLost(FlashEvent);
//			pullOutControllers();
//		}
		
		private function handleKeyDown(evt:KeyboardEvent):void
		{
			if (evt.keyCode == Keyboard["BACK"])
			{
				var state:PlayState = FlxG.state as PlayState;
				if (state != null)
				{
					evt.preventDefault();
					state.confirmExit();
				}
			}
		}

		private static var music:FlxSound;
		
		public static function get musicAvailable():Boolean
		{
			return (music != null && music.active);
		}
		
		public static function playMusic(num:uint, loop:Boolean = true):void
		{
			stopMusic();
			if (num < 0)
			{
				return;
			}
			music = FlxG.stream("music/" + num + ".vvvv", 1.0, loop);
			music.fadeIn(7);
		}
		
		public static function switchMusic():void
		{
			if (music != null)
			{
				if (music.active)
					music.pause();
				else
					music.resume();
			}
		}
		
		public static function pauseMusic():void
		{
			if (music != null)
				music.pause();
		}
		
		public static function resumeMusic():void
		{
			if (music != null)
				music.resume();
		}
		
		public static function stopMusic():void
		{
			if (music != null)
				music.stop();
		}
	}

}