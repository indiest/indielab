package test 
{
	import flash.geom.Point;
	import org.flixel.FlxRect;
	
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class BitmapDataHitTestState extends FlxState
	{
		private var captain:FlxSprite;
		private var spike:FlxSprite;
		private var _hit:Boolean;
		private var _spikeDir:uint = 0;
		
		override public function create():void 
		{
			//FlxG.bgColor = 0xffffff;

			spike = new FlxSprite();
			//spike.x = 40;
			//spike.y = 40;
			spike.loadGraphic(Assets.TILES, true, false, 8, 8);
			spike.addAnimation("0", [8]);
			spike.addAnimation("1", [9]);
			spike.addAnimation("2", [49]);
			spike.addAnimation("3", [50]);
			spike.play(String(_spikeDir));
			//add(spike);
			spike.drawFrame();
			
			captain = new FlxSprite();
			captain.loadGraphic(Assets.SPRITES, true, false, 32, 32);
			captain.width = 10;
			captain.height = 21;
			captain.offset.make(7, 2);
			captain.addAnimation("normal", [0]);
			captain.play("normal");
			captain.drawRect(new FlxRect(0, 0, captain.frameWidth - 1, captain.frameHeight - 1), 0x80ffffff);
			captain.x = captain.y = 40;
			add(captain);
		}
		
		override public function update():void 
		{
			spike.x = FlxG.mouse.x;
			spike.y = FlxG.mouse.y;
			var hit:Boolean = captain.framePixels.hitTest(new Point(captain.x - captain.offset.x, captain.y - captain.offset.y), 0xff, 
				spike.framePixels, new Point(spike.x, spike.y), 0xff);
			if (hit != _hit)
			{
				_hit = hit;
				trace("Hit test result changed to:", _hit);
			}
			
			if (FlxG.mouse.justReleased())
			{
				_spikeDir++;
				if (_spikeDir > 3)
					_spikeDir = 0;
				spike.play(String(_spikeDir));
			}
		}
	}

}