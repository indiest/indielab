package test 
{
	import org.flixel.FlxState;
	import org.flixel.FlxTilemap;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class TilemapTestState extends FlxState
	{
		[Embed(source="../../assets/tiles.png")]
		private var TILES:Class;
		
		override public function create():void 
		{
			var tilemap:FlxTilemap = new FlxTilemap();
			tilemap.loadMap("0,1,2,3,4,5,6,7,8", TILES, 8, 8, FlxTilemap.OFF, 0, 0, 0);
			add(tilemap);
		}
	}

}