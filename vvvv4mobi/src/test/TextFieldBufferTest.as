package test 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class TextFieldBufferTest extends Sprite
	{
		
		public function TextFieldBufferTest() 
		{
			var _tf:TextField = new TextField();
			_tf.type = TextFieldType.INPUT;
			_tf.textColor = 0xffffff;
			_tf.background = true;
			_tf.backgroundColor = 0xaaaaaa;
			_tf.multiline = false;
			
			var bmp:Bitmap = new Bitmap(new BitmapData(400, 100));
			addChild(bmp);

			addEventListener(Event.ENTER_FRAME, function(evt:*):void
			{
				bmp.bitmapData.draw(_tf);
			});
			
			stage.focus = _tf;
		}
		
	}

}