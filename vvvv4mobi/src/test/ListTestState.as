package test
{
	import org.flixel.FlxState;
	
	import ui.SwipeList;
	
	public class ListTestState extends FlxState
	{
		public override function create():void
		{
			var list:SwipeList = new SwipeList(100);
			//list.align = "center";
			//list.x = 10;
			list.y = 10;
			list.setFixedWidth(100);
			//list.indentX = 4;
			//list.indentY = 4;
			list.customSpacingY = 8;
			list.items = ["1", "22", "333", "4444", "55555", "666666", "Hello World, Blahblahblah", "The End"];
			list.setSelectCallback(function():void
			{
				trace(list.items[list.selectedIndex]);
			});
			add(list);
		}
	}
}