package test 
{
	import org.flixel.FlxState;
	import ui.FlxTextInput;
	import ui.InputBox;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class InputBoxTestState extends FlxState
	{
		override public function create():void 
		{
			InputBox.show("Please submit your level score:", 10, "Your Name", function(result:String):void
			{
				trace("Input name:", result);
			});
			
		}
		
	}

}