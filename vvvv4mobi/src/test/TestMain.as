package test
{
	import flash.display.Sprite;
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class TestMain extends Sprite
	{
		public function TestMain() 
		{
			var game:FlxGame = new FlxGame(640, 480, InputBoxTestState, 2);
			addChild(game);
			
			FlxG.mouse.show();
			FlxG.debug = true;
		}
		
	}

}