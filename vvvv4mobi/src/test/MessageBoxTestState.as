package test 
{
	import org.flixel.FlxState;
	import ui.MessageBox;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class MessageBoxTestState extends FlxState 
	{
		
		override public function create():void 
		{
			MessageBox.show("Congratulations!\n A trinket had been found!");
		}
	}

}