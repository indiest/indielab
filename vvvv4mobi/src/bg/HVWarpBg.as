package bg 
{
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	
	import org.flixel.FlxBasic;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class HVWarpBg extends FlxBasic 
	{
		private static const Z:uint = 16;
		private var _z:uint = 0;
		public var color:uint = FlxU.makeColor(16, 16, 34);
		
		override public function draw():void 
		{
			var gfx:Graphics = FlxG.flashGfx;
			gfx.clear();
			gfx.lineStyle(Z, color, 1.0, false, LineScaleMode.NORMAL, CapsStyle.NONE, JointStyle.MITER);
			var half:uint;
			for (var i:uint = 0; i < 6; i++)
			{
				half = (i * Z)* 2 + _z;
				gfx.drawRect(VVVVGame.SCREEN_WIDTH / 2 - half, VVVVGame.SCREEN_HEIGHT / 2 - half, half * 2, half * 2);
			}
			FlxG.camera.buffer.draw(FlxG.flashGfxSprite);
			
			_z++;
			if (_z >= Z * 2)
				_z = 0;
		}
		
	}

}