package bg
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class VWarpBg extends FlxSprite
	{
		private var _lineColor:uint = FlxU.makeColor(16, 16, 34);
		
		public function VWarpBg() 
		{
			makeGraphic(VVVVGame.SCREEN_WIDTH, VVVVGame.SCREEN_HEIGHT * 2, 0xff000000);
			for (var x:int = 0; x < 20; x++)
			{
				for (var y:int = 0; y < 32; y++)
				{
					drawLine(x * 16, y * 18, x * 16 + 8, y * 18 + 8, _lineColor, 8);
					drawLine(x * 16 + 8, y * 18 + 8, x * 16, y * 18 + 16, _lineColor, 8);
				}
			}
		}
		
		override public function draw():void
		{
			_flashRect.height = VVVVGame.SCREEN_HEIGHT;
			_flashRect.y += 2;
			if (_flashRect.y >= VVVVGame.SCREEN_HEIGHT)
				_flashRect.y = 0;
			super.draw();
		}
		
		//override public function update():void 
		//{
			//y -= 2;
			//if (y <= -VVVVGame.SCREEN_HEIGHT)
				//y = 0;
			//super.update();
		//}
	}

}