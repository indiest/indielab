package bg
{
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class SpaceBg extends FlxGroup
	{
		public static const DIR_MOVE_LEFT:uint = 1;
		public static const DIR_MOVE_UP:uint = 2;
		private var _dir:uint;
		
		public function SpaceBg()
		{
			for (var i:uint = 0; i < 20; i++)
			{
				var star:FlxSprite = new FlxSprite(Math.random() * VVVVGame.SCREEN_WIDTH,
					Math.random() * VVVVGame.SCREEN_HEIGHT);
				star.makeGraphic(2, 2, (uint(Math.random() * 0x40) << 24) + 0x20ffffff);
				add(star);
			}
		}
		
		public function setDir(dir:uint):void
		{
			if (_dir == dir)
				return;
			_dir = dir;
			for each (var star:FlxSprite in members)
			{
				if (dir == DIR_MOVE_LEFT)
					star.velocity.make(-(100 + Math.random() * 200), 0);
				else if (dir == DIR_MOVE_UP)
					star.velocity.make(0, -(100 + Math.random() * 200));
			}
		}
		
		override public function update():void 
		{
			// TODO: different tileset or warp direction
			for each (var star:FlxSprite in members)
			{
				if (star.x < 0)
				{
					star.x = VVVVGame.SCREEN_WIDTH;
					star.y = Math.random() * VVVVGame.SCREEN_HEIGHT;
				}
			}
			
			super.update();
		}
	}

}