package bg
{
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class HWarpBg extends FlxSprite
	{
		private var _lineColor:uint = FlxU.makeColor(16, 16, 34);

		public function HWarpBg() 
		{
			makeGraphic(VVVVGame.SCREEN_WIDTH * 2, VVVVGame.SCREEN_HEIGHT, 0xff000000);
			for (var y:int = 0; y < 16; y++)
			{
				for (var x:int = 0; x < 40; x++)
				{
					drawLine(x * 16, y * 18, x * 16 + 8, y * 18 + 8, _lineColor, 8);
					drawLine(x * 16 + 8, y * 18 + 8, x * 16 + 16, y * 18, _lineColor, 8);
				}
			}
		}
		
		override public function draw():void
		{
			_flashRect.width = VVVVGame.SCREEN_WIDTH;
			_flashRect.x += 2;
			if (_flashRect.x >= VVVVGame.SCREEN_WIDTH)
				_flashRect.x = 0;
			super.draw();
		}
	}

}