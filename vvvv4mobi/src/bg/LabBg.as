package bg
{
	import adobe.utils.CustomActions;
	
	import flash.display.Shape;
	import flash.geom.Rectangle;
	
	import org.flixel.FlxGroup;
	import org.flixel.FlxRect;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class LabBg extends FlxGroup
	{
		private static const COLORS:Object =
		{
			0: FlxU.makeColor(0, 165, 206),
			1: FlxU.makeColor(206, 5, 0),
			2: FlxU.makeColor(206, 0, 160),
			3: FlxU.makeColor(27, 40, 141),
			4: FlxU.makeColor(194, 206, 0),
			5: FlxU.makeColor(0, 206, 39)
		};
		
		private var _tileColor:uint;
		
		public function LabBg() 
		{
			var rect:FlxSprite;
			for (var i:uint = 0; i < 8; i++)
			{
				rect = new FlxSprite(Math.random() * VVVVGame.SCREEN_WIDTH,
					Math.random() * VVVVGame.SCREEN_HEIGHT);
				rect.makeGraphic(32, 12, 0);
				var shape:Shape = new Shape();
				rect.velocity.x = 240;
				add(rect);
				
				rect = new FlxSprite(Math.random() * VVVVGame.SCREEN_WIDTH,
					Math.random() * VVVVGame.SCREEN_HEIGHT);
				rect.makeGraphic(12, 32, 0);
				rect.velocity.y = 240;
				add(rect);
			}
			//setColor(0x10ff0000);
		}
		
		public function setColor(tileColor:uint):void
		{
			if (_tileColor == tileColor)
				return;
			_tileColor = tileColor;
			for each (var rect:FlxSprite in members)
			{
				rect.drawRect(new FlxRect(0, 0, rect.width-1, rect.height-1), COLORS[tileColor]);
			}
		}
		
		override public function update():void 
		{
			for each (var rect:FlxSprite in members)
			{
				if (rect.x > VVVVGame.SCREEN_WIDTH)
				{
					rect.x = -rect.width;
					rect.y = Math.random() * VVVVGame.SCREEN_HEIGHT;
				}
				if (rect.y > VVVVGame.SCREEN_HEIGHT)
				{
					rect.y = -rect.height;
					rect.x = Math.random() * VVVVGame.SCREEN_WIDTH;
				}
			}
			
			super.update();
		}
		
	}

}