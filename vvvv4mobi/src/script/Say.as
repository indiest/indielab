package script 
{
	import org.flixel.FlxG;
	import ui.MessageBox;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Say extends ScriptCommand
	{
		public var text:String;

		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint
		{
			var end:uint = lineIndex + (strParams.length > 0 ? uint(strParams[0]) : 1);
			var arr:Array = [];
			while (lineIndex < end)
			{
				lineIndex++;
				arr.push(lines[lineIndex]);
			}
			text = arr.join("\n");
			return end;
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void
		{
			FlxG.play(Assets.SOUND_BLIP2);
			MessageBox.show(text, future.proceed);
		}
	}
}