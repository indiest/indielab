package script 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public interface ScriptExecution
	{		
		function proceed():void;
		
		function end():void;
	}

}