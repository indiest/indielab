package script 
{
	import org.flixel.FlxG;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Flash extends ScriptCommand
	{
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			FlxG.flash();
			FlxG.shake(0.007, 1.0, future.proceed);
		}
		
	}

}