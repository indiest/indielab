package script 
{
	import org.flixel.FlxG;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Happy extends ScriptCommand
	{
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			var state:PlayState = FlxG.state as PlayState;
			state.captain.happy = true;
			future.proceed();
		}
	}

}