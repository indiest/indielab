package script
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ScriptCommand
	{		
		public var name:String;
		
		public function ScriptCommand() 
		{
			
		}
		
		public function parse(strParams:Array, lines:Array, lineIndex:uint):uint
		{
			return lineIndex;
		}
		
		public function execute(ctx:Object, future:ScriptExecution):void
		{
			
		}
	}

}