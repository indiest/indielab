package script 
{
	import entity.*;
	import org.flixel.FlxBasic;
	import org.flixel.FlxG;
	import script.ScriptExecution;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Destroy extends ScriptCommand
	{
		public var entityClass:Class;
		
		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint 
		{
			switch (strParams[0])
			{
				case "gravitylines":
					entityClass = GravLine;
					break;
				case "warptokens":
					entityClass = WarpToken;
					break;
				case "platforms":
					entityClass = Moving;
					break;
				default:
					FlxG.log("Invalid destroy type: " + strParams[0]);
					break;
			}
			return super.parse(strParams, lines, lineIndex);
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			var state:PlayState = FlxG.state as PlayState;
			for each(var e:FlxBasic in state.currentRoom.entities)
			{
				if (e is entityClass)
						e.kill();
			}
			state.loadRoom();
			future.proceed();
		}
	}

}