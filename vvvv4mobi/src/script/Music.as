package script 
{
	import script.ScriptExecution;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Music extends ScriptCommand
	{
		public var musicNum:int;
		
		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint 
		{
			musicNum = int(strParams[0]);
			return super.parse(strParams, lines, lineIndex);
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			VVVVGame.playMusic(musicNum);
			future.proceed();
		}
	}

}