package script 
{
	import script.ScriptExecution;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Flag extends ScriptCommand
	{
		public var flagName:String;
		public var flag:Boolean;
		
		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint 
		{
			flagName = strParams[0];
			flag = (strParams[1] == "on");
			return super.parse(strParams, lines, lineIndex);
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			ctx[flagName] = flag;
			future.proceed();
		}
	}

}