package script 
{
	import script.ScriptExecution;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class End extends ScriptCommand
	{
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			future.end();
		}
		
	}

}