package script 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ScriptParser
	{
		private static const FINISHED:int = 1;
		private static const FAILED:int = -1;
		private static const UNFINISHED:int = 0;
		
		private var _index:uint;
		private var _text:String;
		private var _func:Function = parseId;
		
		public function ScriptParser() 
		{
			
		}
		
		public function parse(text:String):Array
		{
			_text = text;
			_index = 0;
			var arr:Array = [];
			var s:ScriptObject = new ScriptObject();
			a: while (_index < _text.length)
			{
				switch (_func(s))
				{
					case FAILED:
						trace("Parse error at index:", _index);
						break a;
					case FINISHED:
						arr.push(s);
						s = new ScriptObject();
						break;
				}
			}
			return arr;
		}
		
		private function parseId(s:ScriptObject):Boolean
		{
			var c:String = _text.charAt(_index);
			_index++;
			if (c != ":")
				s.id += c;
			else
				_func = parseCommand;
			return UNFINISHED;
		}
		
		private var _cmdName:String;
		
		private function parseLines(s:ScriptObject):Boolean
		{
			_cmdName = "";
			_func = parseCommand;
		}
		
		private function parseCommand(s:ScriptObject):Boolean
		{
			var c:String = _text.charAt(_index);
			_index++;
			if (c != "|")
			{
				_cmdName += c;
				var cmd:ScriptCommand = ScriptCommand.MAP[_cmdName];
				if (cmd)
				{
					s.commands.push(cmd);
					_func = parseParams;
				}
			}
			else
			{
				_func = parseLines;
			}
		}
		
		private function parseParams(s:ScriptObject):Boolean 
		{
			var cmd:ScriptCommand = s.commands[s.commands.length - 1];
			var c:String = _text.charAt(_index);
			_index++;
		}
	}

}