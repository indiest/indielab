package script 
{
	import org.flixel.FlxG;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class IfFlag extends ScriptCommand
	{
		public var flagName:String;
		public var scriptName:String;
		
		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint 
		{
			flagName = strParams[0];
			scriptName = strParams[1];
			return super.parse(strParams, lines, lineIndex);
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			if (ctx[flagName])
			{
				future.end();
				var state:PlayState = FlxG.state as PlayState;
				state.executeScript(scriptName);
			}
			else
			{
				future.proceed();
			}
		}
	}

}