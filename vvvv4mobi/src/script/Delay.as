package script 
{
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import org.flixel.FlxG;
	import org.flixel.FlxTimer;
	import script.ScriptExecution;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Delay extends ScriptCommand
	{
		public var frames:uint;
		
		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint 
		{
			frames = uint(strParams[0]);
			return super.parse(strParams, lines, lineIndex);
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			var captain:Captain = (FlxG.state as PlayState).captain;
			captain.paused = true;
			var id:uint = setInterval(function():void
			{
				clearInterval(id);
				captain.paused = false;
				future.proceed();
			}, 1000 * frames / FlxG.framerate);
		}
	}
}