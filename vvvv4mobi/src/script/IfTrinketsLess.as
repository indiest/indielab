package script 
{
	import org.flixel.FlxG;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class IfTrinketsLess extends ScriptCommand
	{
		public var num:uint;
		public var scriptName:String;
		
		override public function parse(strParams:Array, lines:Array, lineIndex:uint):uint 
		{
			num = uint(strParams[0]);
			scriptName = strParams[1];
			return super.parse(strParams, lines, lineIndex);
		}
		
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (state.trinkets < num)
			{
				future.end();
				state.executeScript(scriptName);
			}
			else
			{
				future.proceed();
			}
		}
	}

}