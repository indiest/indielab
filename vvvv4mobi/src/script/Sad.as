package script 
{
	import org.flixel.FlxG;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Sad extends ScriptCommand
	{
		override public function execute(ctx:Object, future:ScriptExecution):void 
		{
			var state:PlayState = FlxG.state as PlayState;
			state.captain.happy = false;
			FlxG.play(Assets.SOUND_HURT);
			future.proceed();
		}
	}

}