package script 
{
	import org.flixel.FlxG;
	import org.flixel.FlxPoint;
	import org.flixel.FlxU;
	import ui.MessageBox;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Reply extends Say
	{
		
		override public function execute(ctx:Object, future:ScriptExecution):void
		{
			FlxG.play(Assets.SOUND_CREW1);
			var state:PlayState = FlxG.state as PlayState;
			MessageBox.show(text, future.proceed, FlxU.makeColor(164, 164, 255),
				new FlxPoint(state.captain.fixedX, state.captain.fixedY - 8));
		}
	}

}