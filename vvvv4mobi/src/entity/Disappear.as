package entity 
{
	import com.greensock.TweenNano;
	import com.greensock.easing.Linear;
	
	import flash.utils.getTimer;
	
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	import org.flixel.FlxTileblock;
	import org.flixel.FlxTilemap;

	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Disappear extends FlxSprite implements BaseEntity 
	{
		private static const TILE_DATA:Array = [2, 3, 4, 5];
		private static const WIDTH_IN_TILES:uint = 4;
		
		private var _collided:Boolean = false;
		
		public function Disappear(x:Number, y:Number) 
		{
			super();
			
			this.x = x;
			this.y = y;
			this.solid = true;
			this.immovable = true;
			this.moves = false;
			loadGraphic(Assets.TILES, true, false, VVVVGame.TILE_WIDTH * WIDTH_IN_TILES, VVVVGame.TILE_HEIGHT);
			frameWidth = VVVVGame.TILE_WIDTH;
			addAnimation("normal", [2]);
			addAnimation("collapse", TILE_DATA, 8);
			addAnimationCallback(onAnimationCallback);
			play("normal");
		}
		
		private function onAnimationCallback(animName:String, frameNumber:uint, frameIndex:uint):void
		{
//			trace("animation callback:" + animName);
			if (animName == "collapse" && finished)
				kill();
		}
		
		public function init(data:*, room:Room):void 
		{
			
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (FlxG.collide(this, state.captain))
			{
				if (!_collided && !state.captain.isTouching(FlxObject.LEFT | FlxObject.RIGHT))
				{
					_collided = true;
					play("collapse");
					FlxG.play(Assets.SOUND_VANISH);
				}
			}
			
			super.update();
		}
		
		protected override function drawFramePixels():void
		{
			for (var i:int = 0; i < WIDTH_IN_TILES; i++)
			{
				_flashPoint.x = i * VVVVGame.TILE_WIDTH;
				_flashPoint.y = 0;
				framePixels.copyPixels(_pixels,_flashRect,_flashPoint);
			}
		}
		
		public function resetAll():void 
		{
			_collided = false;
			this.revive();
			play("normal");
		}
	}

}