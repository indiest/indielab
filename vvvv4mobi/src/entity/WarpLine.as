package entity 
{
	import org.flixel.FlxBasic;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class WarpLine extends FlxBasic implements BaseEntity
	{
		public static const DIR_LEFT:uint = 0;
		public static const DIR_RIGHT:uint = 1;
		public static const DIR_TOP:uint = 2;
		public static const DIR_BOTTOM:uint = 3;

		public var dir:uint;

		public function WarpLine(x:Number, y:Number) 
		{
			super();
		}
		
		public function init(data:*, room:Room):void 
		{
			dir = uint(data.@p1);
			
			var length:int = int(data.@p3) / VVVVGame.TILE_WIDTH;
			var start:int = int(data.@p2);
			while (length > 0)
			{
				length--;
				room.warpBounds[dir].push(start + length);
			}
		}
		
		public function resetAll():void 
		{
			
		}
	}

}