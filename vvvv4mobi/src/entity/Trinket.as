package entity
{
	import flash.geom.ColorTransform;
	import flash.utils.getTimer;
	
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import ui.MessageBox;

	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Trinket extends FlxSprite implements BaseEntity
	{
		public function Trinket(x:Number, y:Number)
		{
			super(x, y);
			loadGraphic(Assets.SPRITES, true, false, 32, 32);
			width = 16;
			height = 16;
			solid = false;
			_colorTransform = new ColorTransform(0, 0, 0, 1.0, 0xff, 0xff, 0xff);
			addAnimation("default", [22]);
			play("default");
		}
		
		private var _trinketIndex:uint;
		public function init(data:*, room:Room):void
		{
			var state:PlayState = FlxG.state as PlayState;
			if (!isCollected(state))
			{
				_trinketIndex = state.trinketsTotal++;
				state.trinketsCollect[_trinketIndex] = 0;
			}
		}
		
		private function isCollected(state:PlayState):Boolean
		{
			return (state.trinketsCollect.length > _trinketIndex &&
				state.trinketsCollect[_trinketIndex] == 1);
		}
		
		private var _tintTimer:int;
		override public function update():void
		{
			_tintTimer -= FlxG.elapsed;
			if (_tintTimer <= 0)
			{
				color = Math.random() * 0x333333 + 0xcccccc;
				_tintTimer = 10;
			}
			
			var state:PlayState = FlxG.state as PlayState;
			if (overlaps(state.captain))
			{
				state.trinkets++;
				state.trinketsCollect[_trinketIndex] = 1;
				kill();
				MessageBox.show("        Congratulations!\n" +
								"You have found a shiny trinket!\n" +
								"          " + state.trinkets + " out of " + state.trinketsTotal,
								VVVVGame.resumeMusic);
				VVVVGame.pauseMusic();
				FlxG.play(Assets.SOUND_SOULEYEMINIJINGLE);
			}
		}
		
		public function resetAll():void
		{
			var state:PlayState = FlxG.state as PlayState;
			if (isCollected(state))
				kill();
		}
	}
}