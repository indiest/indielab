package entity
{
	import flash.display.BlendMode;
	import flash.geom.ColorTransform;
	import flash.utils.getTimer;
	
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSave;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Checkpoint extends FlxSprite implements BaseEntity
	{
		public var activated:Boolean = false;
		public var dir:uint;
		
		public function Checkpoint(x:Number, y:Number)
		{
			super(x, y);
			loadGraphic(Assets.SPRITES, true, false, 32, 32);
			width = 16;
			height = 16;
			solid = false;
			resetColor();
		}
		
		private function resetColor():void
		{
			_colorTransform = new ColorTransform(0, 0, 0, 0.7, 0xaa, 0xaa, 0xaa);
			dirty = true;
		}
		
		public function init(data:*, room:Room):void 
		{
			dir = uint(data.@p1);
			addAnimation("default", [dir == 0 ? 20 : 21]);
			play("default");
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (!activated)
			{
				if (overlaps(state.captain))
				{
					activated = true;
					state.quickSaveGame(this);
					
					_colorTransform = new ColorTransform(0, 0, 0, 0.8, 0xff, 0xff, 0xff);
					dirty = true;
					FlxG.play(Assets.SOUND_SAVE);
				}
			}
			
			super.update();
		}
		
		public function resetAll():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			activated = (state.activatedCheckpoint == this);
			if (!activated)
				resetColor();
		}
		
	}

}