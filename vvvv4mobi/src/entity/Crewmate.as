package entity
{
	import flash.geom.ColorTransform;
	import flash.utils.getTimer;
	
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxU;
	import ui.MessageBox;

	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Crewmate extends FlxSprite implements BaseEntity
	{
		public function Crewmate(x:Number, y:Number)
		{
			super(x, y);
			loadGraphic(Assets.SPRITES, true, false, 32, 32);
			width = 10;
			height = 21;
			offset.make(7, 2);
			acceleration.y = 2400;
		}
		
		public static const COLORS:Object = 
		{
			0: FlxU.makeColor(129, 182, 184),
			1: FlxU.makeColor(191, 111, 201),
			2: FlxU.makeColor(192, 201, 111),
			3: FlxU.makeColor(251, 61, 61),
			4: FlxU.makeColor(105, 211, 111),
			5: FlxU.makeColor(75, 75, 233)
		};
		
		private var _crewmateIndex:uint;
		public function init(data:*, room:Room):void
		{
			var state:PlayState = FlxG.state as PlayState;
			_crewmateIndex = state.crewmatesTotal++;
			state.crewmatesStats[_crewmateIndex] = 0;
			
			var rgba:Array = FlxU.getRGBA(COLORS[uint(data.@p1)]);
			_colorTransform = new ColorTransform(0, 0, 0, 1.0, rgba[0], rgba[1], rgba[2]);
			addAnimation("right", [12]);
			addAnimation("left", [13]);
			play("right");
		}
		
		override public function update():void
		{
			var state:PlayState = FlxG.state as PlayState;
			if (state.captain.x > x)
				play("right");
			else
				play("left");
			
			if (overlaps(state.captain))
			{
				state.crewmates++;
				state.crewmatesStats[_crewmateIndex] = 1;
				kill();
				var text:String = "        Congratulations!\n" +
								  "You have found a lost crewmate!\n";
				var callback:Function = null;
				if (state.crewmates == state.crewmatesTotal)
				{
					text +=       "     All crewmates rescued!"
					callback = onAllRescued;
				}
				else
				{
					text += ("           " + (state.crewmatesTotal - state.crewmates) + " remains");
					callback = VVVVGame.resumeMusic;
				}
				MessageBox.show(text, callback);
				VVVVGame.pauseMusic();
				FlxG.play(Assets.SOUND_RESCUE);
			}
		}
		
		private function onAllRescued():void
		{
			FlxG.flash();
			FlxG.shake(0.007, 2.0, onShakeComplete);
			FlxG.play(Assets.SOUND_PRETELEPORT);
		}
		
		private function onShakeComplete():void
		{
			var state:PlayState = FlxG.state as PlayState;
			state.levelComplete();
		}
		
		public function resetAll():void
		{
			var state:PlayState = FlxG.state as PlayState;
			if (state.crewmatesStats[_crewmateIndex] == 1)
				kill();
		}
	}
}