package entity 
{
	import org.flixel.FlxBasic;
	import org.flixel.FlxG;
	import org.flixel.FlxRect;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class ScriptBox extends FlxSprite implements BaseEntity
	{
		public var scriptName:String;
		//private var _rect:FlxRect;
		
		public function ScriptBox(x:Number, y:Number) 
		{
			//_rect = new FlxRect(x, y);
			super(x, y);
		}
		
		public function init(data:*, room:Room):void
		{
			scriptName = data.text();
			//_rect.width = uint(data.@p1) * VVVVGame.TILE_WIDTH;
			//_rect.height = uint(data.@p2) * VVVVGame.TILE_HEIGHT;
			makeGraphic(uint(data.@p1) * VVVVGame.TILE_WIDTH, uint(data.@p2) * VVVVGame.TILE_HEIGHT, 0);
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (overlaps(state.captain))
			{
				kill();
				state.executeScript(scriptName);
			}
		}
		
		public function resetAll():void 
		{
			
		}
	}

}