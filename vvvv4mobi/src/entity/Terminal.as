package entity 
{
	import flash.geom.ColorTransform;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Terminal extends FlxSprite implements BaseEntity
	{
		public var activated:Boolean = false;
		public var dir:uint;
		public var scriptName:String;
		
		public function Terminal(x:Number, y:Number) 
		{
			super(x, y);
			loadGraphic(Assets.SPRITES, true, false, 32, 32);
			width = 16;
			height = 16;
			solid = false;
			resetColor();
		}
		
		private function resetColor():void
		{
			_colorTransform = new ColorTransform(0, 0, 0, 0.7, 0xaa, 0xaa, 0xaa);
			dirty = true;
		}
		
		public function init(data:*, room:Room):void 
		{
			scriptName = data.text();
			dir = uint(data.@p1);
			addAnimation("default", [dir == 0 ? 17 : 18]);
			if (dir == 0)
				offset.y = -8;
			play("default");
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (overlaps(state.captain))
			{
				if (!activated)
				{
					activated = true;
					state.setTopText("Press ENTER or tab terminal to activate");
					FlxG.play(Assets.SOUND_TERMINAL);
					_colorTransform = new ColorTransform(0, 0, 0, 0.8, 0xff, 0xff, 0xff);
					dirty = true;
				}
				
				if (state.captain.paused)
					return;
				if (FlxG.keys.justPressed("ENTER") || (FlxG.mouse.justPressed() && overlapsPoint(FlxG.mouse)))
				{
					state.captain.paused = true;
					state.executeScript(scriptName);
				}
			}
			else
			{
				if (activated)
				{
					activated = false;
					state.setTopText("");
				}
			}
		}
		
		public function resetAll():void 
		{
			resetColor();
			activated = false;
		}
	}

}