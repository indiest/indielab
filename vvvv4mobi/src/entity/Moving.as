package entity 
{
	import flash.utils.getTimer;
	
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxRect;
	import org.flixel.FlxSprite;
	import org.flixel.FlxTilemap;

	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Moving extends FlxSprite implements BaseEntity 
	{
		public static const CONVEYOR_RIGHT:uint = 5;
		public static const CONVEYOR_LEFT:uint = 6;
		public static const CONVEYOR_RIGHT_2X:uint = 7;
		public static const CONVEYOR_LEFT_2X:uint = 8;
		
		private static const CONVEYOR_LEFT_TILES:Array = [20, 21, 22, 23];
		private static const CONVEYOR_RIGHT_TILES:Array = [63, 62, 61, 60];
		private static const CONVEYOR_FRAME_RATE:uint = 12;
		
		private var _widthInTiles:uint = 4;
		
		public var direction:uint;
		public var bound:FlxRect;
		public var speed:Number = 112;
		private var _initX:Number;
		private var _initY:Number;
		
		public function Moving(x:Number, y:Number) 
		{
			super();
			
			this.x = _initX = x;
			this.y = _initY = y;
			this.solid = true;
			this.immovable = true;
			this.moves = false;
		}
		
		public function init(data:*, room:Room):void 
		{
			direction = uint(data.@p1);
			resetSpeed();
			var frames:Array;
			var frameRate:uint = 0;
			if (direction == CONVEYOR_RIGHT)
			{
				frames = CONVEYOR_RIGHT_TILES;
				frameRate = CONVEYOR_FRAME_RATE;
			}
			else if (direction == CONVEYOR_LEFT)
			{
				frames = CONVEYOR_LEFT_TILES;
				frameRate = CONVEYOR_FRAME_RATE;
			}
			else if (direction == CONVEYOR_RIGHT_2X)
			{
				frames = CONVEYOR_RIGHT_TILES;
				frameRate = CONVEYOR_FRAME_RATE;
				_widthInTiles = 8;
			}
			else if (direction == CONVEYOR_LEFT_2X)
			{
				frames = CONVEYOR_LEFT_TILES;
				frameRate = CONVEYOR_FRAME_RATE;
				_widthInTiles = 8;
			}
			else
			{
				frames = [1];
				this.moves = true;
			}
			
			loadGraphic(Assets.TILES, true, false, VVVVGame.TILE_WIDTH * _widthInTiles, VVVVGame.TILE_HEIGHT);
			frameWidth = VVVVGame.TILE_WIDTH;
			addAnimation("normal", frames, frameRate);
			play("normal");
			
			bound = room.platformBound;
		}
		
		private function resetSpeed():void
		{
			switch (direction)
			{
				case Enemy.DIR_DOWN:
					velocity.y = speed;
					break;
				case Enemy.DIR_UP:
					velocity.y = -speed;
					break;
				case Enemy.DIR_LEFT:
					velocity.x = -speed;
					break;
				case Enemy.DIR_RIGHT:
					velocity.x = speed;
					break;
				case CONVEYOR_LEFT:
				case CONVEYOR_LEFT_2X:
					speed = -Math.abs(speed);
					break;
				default:
					// no speed?
					break;
			}
		}
		
		public function isConveyor():Boolean
		{
			return direction >= CONVEYOR_RIGHT;
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			var captain:Captain = state.captain;

			if (FlxG.collide(this, captain))
			{
				if (captain.isOnSurface())
				{
					if (isConveyor())
					{
						captain.x += speed * FlxG.elapsed;
					}
					else
					{
						captain.x += velocity.x * FlxG.elapsed;
						captain.y += velocity.y * FlxG.elapsed;
					}
				}
				
				//trace("colliding captain!");
				if (FlxG.collide(captain, state.tilemap))
				{
					//trace("crushing captain!");
					this.velocity.make(0, 0);
				}
			}
			else if (overlaps(captain))
			{
				//trace("overlapping captain!");
				this.velocity.make(0, 0);
			}
			
			if (!isConveyor())
			{
				// TODO: Move the entity into the bound
				if (this.x < bound.x || this.x + this.width > bound.right ||
					this.y < bound.y || this.y + this.height > bound.bottom || 
					state.tilemap.overlaps(this))
				{
					this.velocity.make(0, 0);
				}
				if (velocity.x == 0 && velocity.y == 0)
				{
					speed = -speed;
					resetSpeed();
				}
			}
			
			super.update();
		}
		
		
		protected override function drawFramePixels():void
		{
			for (var i:int = 0; i < _widthInTiles; i++)
			{
				_flashPoint.x = i * VVVVGame.TILE_WIDTH;
				_flashPoint.y = 0;
				framePixels.copyPixels(_pixels,_flashRect,_flashPoint);
			}
		}
		
		public function resetAll():void 
		{
			this.x = _initX;
			this.y = _initY;
			// TODO: reset direction?
		}
	}

}