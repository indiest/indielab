package entity 
{
	import org.flixel.plugin.photonstorm.FlxBitmapFont;

	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Roomtext extends FlxBitmapFont implements BaseEntity
	{
		public function Roomtext(x:Number, y:Number) 
		{
			super(Assets.FONT, 8, 8, FlxBitmapFont.TEXT_SET1, 0, 0, 0, 0, 16);
			color = 0xb8b8ed;
			this.x = x
			this.y = y;
		}
		
		public function init(data:*, room:Room):void 
		{
			text = data.text();
		}
		
		public function resetAll():void 
		{
			
		}
		
	}

}