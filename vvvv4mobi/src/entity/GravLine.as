package entity 
{
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class GravLine extends FlxSprite implements BaseEntity 
	{
		public static const HORIZONTAL:uint = 0;
		public static const VERTICAL:uint = 1;
		
		private var _collided:Boolean = false;
		
		public function GravLine(x:Number, y:Number) 
		{
			super(x, y);
			solid = false;
		}
		
		public function init(data:*, room:Room):void 
		{
			var dir:uint = uint(data.@p1);
			var len:uint = Math.abs(int(data.@p3));
			if (dir == HORIZONTAL)
			{
				this.makeGraphic(len, VVVVGame.TILE_HEIGHT, 0x00ffffff);
				this.drawLine(0, 4, this.width, 4, 0xaaaaaa);
				this.x = int(data.@p2) * VVVVGame.TILE_WIDTH;
			}
			else
			{
				this.makeGraphic(VVVVGame.TILE_WIDTH, len, 0x00ffffff);
				this.drawLine(4, 0, 4, this.height, 0xaaaaaa);
				this.y = int(data.@p2) * VVVVGame.TILE_HEIGHT;
			}
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (overlaps(state.captain))
			{
				if (!_collided)
				{
					_collided = true;
					this.alpha = 0.5;
					state.captain.acceleration.y = -state.captain.acceleration.y;
					state.captain.velocity.y *= 0.5;
					FlxG.play(Assets.SOUND_BLIP);
				}
			}
			else
			{
				_collided = false;
				this.alpha = 1;
			}
			
			super.update();
		}
		
		public function resetAll():void 
		{
			
		}
	}

}