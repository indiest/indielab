package entity 
{
	import org.flixel.FlxBasic;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class StartPoint extends FlxBasic implements BaseEntity
	{
		
		public function StartPoint(x:Number, y:Number) 
		{
			super();
		}
		
		public function init(data:*, room:Room):void 
		{
			var state:PlayState = FlxG.state as PlayState;
			state.startPoint = new FlxPoint(int(data.@x), int(data.@y));
			state.startFacing = (uint(data.@p1) == 0 ? FlxObject.RIGHT : FlxObject.LEFT);
		}
		
		public function resetAll():void 
		{
			
		}
	}

}