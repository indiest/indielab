package entity 
{
	import org.flixel.FlxBasic;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxRect;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Enemy extends FlxSprite implements BaseEntity
	{
		private static const ENEMY_FRAMES:Object = 
		{
			0: [78, 79, 80, 81],
			1: [88, 89, 90, 91],
			2: [36, 37, 38, 39],
			3: [164, 165, 166, 167],
			4: [56, 57, 58, 59],
			5: [48, 49],
			6: [176, 177, 178, 179],
			7: [168, 169, 170, 171],
			8: [112, 113],
			9: [114, 115]
		};
		
		public static const DIR_DOWN:uint = 0;
		public static const DIR_UP:uint = 1;
		public static const DIR_LEFT:uint = 2;
		public static const DIR_RIGHT:uint = 3;
		
		public var speed:Number = 105;
		public var direction:uint;
		public var bound:FlxRect;
		private var _initX:Number;
		private var _initY:Number;
		
		public function Enemy(x:Number, y:Number)
		{
			super(x, y);
			_initX = x;
			_initY = y;
			this.loadGraphic(Assets.SPRITES, true, false, 32, 32);
			this.width = 16;
			this.height = 16;
			//solid = false;
		}
		
		public function init(data:*, room:Room):void
		{
			this.addAnimation("default", ENEMY_FRAMES[room.enemyType], 4);
			this.play("default");
			direction = uint(data.@p1);
			resetSpeed();
			bound = room.enemyBound;
		}
		
		private function resetSpeed():void
		{
			switch (direction)
			{
				case DIR_DOWN:
					this.velocity.y = speed;
					break;
				case DIR_UP:
					this.velocity.y = -speed;
					break;
				case DIR_LEFT:
					this.velocity.x = -speed;
					break;
				case DIR_RIGHT:
					this.velocity.x = speed;
					break;
				default:
					// no speed?
					break;
			}
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (overlaps(state.captain))
				state.captain.die();
			for each(var ent:FlxBasic in state.currentRoom.entities)
			{
				if (ent is Moving || ent is Disappear)
				{
					if (ent.exists && overlaps(ent))
					{
						this.velocity.make(0, 0);
						break;
					}
				}
			}
				
			// TODO: Move the entity into the bound
			if (this.x <= bound.x || this.x + this.width >= bound.right ||
				this.y <= bound.y || this.y + this.height >= bound.bottom)
				this.velocity.make(0, 0);
			
			if (this.velocity.x == 0 && this.velocity.y == 0)
			{
				speed = -speed;
				resetSpeed();
			}
			super.update();
		}
		
		public function resetAll():void 
		{
			this.x = _initX;
			this.y = _initY;
		}
	}

}