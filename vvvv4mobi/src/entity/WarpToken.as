package entity 
{
	import flash.geom.ColorTransform;
	
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;

	/**
	 * ...
	 * @author Sang Tian
	 */
	public class WarpToken extends FlxSprite implements BaseEntity
	{
		public var targetX:uint;
		public var targetY:uint;
		
		public function WarpToken(x:Number, y:Number) 
		{
			super(x, y);
			
			this.loadGraphic(Assets.SPRITES, true, false, 32, 32);
			this.addAnimation("default", [18, 19], 3);
			this.play("default");
			this.width = 16;
			this.height = 16;
			_colorTransform = new ColorTransform(0, 0, 0, 0.8, 0xff, 0xff, 0xff);
		}
		
		public function init(data:*, room:Room):void 
		{
			targetX = uint(data.@p1);
			targetY = uint(data.@p2);
		}
		
		override public function update():void 
		{
			var state:PlayState = FlxG.state as PlayState;
			if (overlaps(state.captain))
			{
				state.captain.transport(targetX, targetY);
				state.loadRoom();
				//FlxG.collide(state.captain, state.tilemap);
				state.captain.acceleration.y = Math.abs(state.captain.acceleration.y);
				state.captain.velocity.make(0, 0);
				
				FlxG.flash();
				FlxG.shake(0.007, 1.0);
				FlxG.play(Assets.SOUND_TELEPORT);
			}
			super.update();
		}
		
		public function resetAll():void 
		{
			
		}

	}

}