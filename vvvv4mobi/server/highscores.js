var http = require('http');
var fs = require('fs');

var highscores = {};
var port = process.env.C9_PORT || 1337;

function loadLevelHighscore(levelName) {
	var listByLevel = highscores[levelName];
	if (!listByLevel) {
		try {
			listByLevel = JSON.parse(fs.readFileSync(levelName + '.json', 'utf8'));
		} catch(err1) {
			listByLevel = [];
		}
		highscores[levelName] = listByLevel;
	}
	return listByLevel;
}

http.createServer(function (req, res) {
	req.setEncoding('utf8');
	if (req.method == "GET") {
		var matches = req.url.match(/\w+.vvvvvv/);
		if (matches != null) {
			var levelName = matches[0];
			res.end(JSON.stringify(loadLevelHighscore(levelName)));
		} else {
			res.end();
		}
		return;
	}
	
	req.on('data', function(data) {
		var highscore = JSON.parse(data);
		var listByLevel = loadLevelHighscore(highscore.levelName);
		
		// check for saveToken repeat
		for (var i = 0; i < listByLevel.length; i++) {
			var record = listByLevel[i];
			if (record.saveToken == highscore.saveToken && record.playerName == highscore.playerName) {
				res.end('You have already submit a record\nfrom the same checkpoint.\nPlease restart the level\nto make a new one.');
				return;
			}
		}
		
		listByLevel.push(highscore);
		res.writeHead(200, {'Content-Type': 'text/plain'});
		var json = JSON.stringify(listByLevel);
		try {
			fs.writeFileSync(highscore.levelName + '.json', json);
		} catch(err2) {
			res.end(err.toString());
			return;
		}
		res.end(json);
	});
}).listen(port, "0.0.0.0");

console.log('Server started.');

