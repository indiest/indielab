var fs = require('fs');
var path = require('path');
var xml2js = require('./lib/xml2js.js');

var levelsPath = process.argv.length > 2 ? process.argv[2] : "../bin/levels";

function generateLevelList() {
	var files = fs.readdirSync(levelsPath);
	//files.length = 3;
	var levels = [];
	for (var i = 0; i < files.length; i++) {
		if (path.extname(files[i]) == ".vvvvvv") {
			var content = fs.readFileSync(path.resolve(levelsPath, files[i]), 'utf8');
			var parser = new xml2js.Parser();
			parser.parseString(content, function(err, result) {
				var metaData = result.Data.MetaData;
				var desc1 = metaData.Desc1.length ? metaData.Desc1 : "";
				var desc2 = metaData.Desc2.length ? metaData.Desc2 : "";
				var desc3 = metaData.Desc3.length ? metaData.Desc3 : "";
				levels.push({
					File: files[i],
					Title: metaData.Title,
					Creator: metaData.Creator,
					Desc: desc1 + '\n' + desc2 + '\n' + desc3
				});
			});
		}
	}
	return levels;
}

var levels = generateLevelList();
//console.dir(levels);
fs.writeFileSync("levels.json", JSON.stringify(levels));