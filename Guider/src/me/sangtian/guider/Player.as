package me.sangtian.guider 
{
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Player extends FlxSprite 
	{
		
		public function Player() 
		{
			loadGraphic(Assets.OBJECTS, true, false, 16, 16);
			addAnimation("default", [1]);
			play("default");
			allowCollisions = ANY;
			
			maxVelocity.make(180, 300);
			acceleration.y = 2400;
			drag.x = maxVelocity.x * 15;
		}
		
		override public function update():void 
		{
			acceleration.x = 0;
			
			if (FlxG.keys.justPressed("W") && isTouching(FLOOR))
				velocity.y = -1000;
			if (FlxG.keys.A)
				acceleration.x = -maxVelocity.x * 15;
			if (FlxG.keys.D)
				acceleration.x += maxVelocity.x * 15;
				
		}
	}

}