package me.sangtian.guider 
{
	import me.sangtian.common.util.MathUtil;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxState;
	import org.flixel.FlxTilemap;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class PlayState extends FlxState 
	{
		private var _tilemap:FlxTilemap = new FlxTilemap();
		private var _objects:FlxGroup = new FlxGroup();
		private var _player:Player = new Player();
		
		public function PlayState() 
		{
			
		}
		
		override public function create():void 
		{
			_tilemap.loadMap(new Assets.TILE_LAYER_MAP_DATA, Assets.TILES, 16, 16);
			//_tilemap.x = -100;
			add(_tilemap);
			
			_player.x = 32;
			_player.y = 32;
			_objects.add(_player);
			add(_objects);
		}
		
		override public function update():void 
		{
			super.update();
			FlxG.collide(_tilemap, _objects);
			FlxG.camera.scroll.x = MathUtil.clamp(_player.x - FlxG.width * 0.5, 0, _tilemap.width - FlxG.width);
			
			if (_player.y > FlxG.height)
				FlxG.resetGame();
		}
	}

}