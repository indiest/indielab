package me.sangtian.guider 
{
	import org.flixel.FlxGame;
	
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Game extends FlxGame 
	{
		public static const SCREEN_WIDTH:uint = 320;
		public static const SCREEN_HEIGHT:uint = 240;
		public static const ZOOM:uint = 2;
		public static const TILE_WIDTH:uint = 16;
		public static const TILE_HEIGHT:uint = 16;
		public static const ROOM_WIDTH_IN_TILES:uint = SCREEN_WIDTH / TILE_WIDTH;
		public static const ROOM_HEIGHT_IN_TILES:uint = SCREEN_HEIGHT / TILE_HEIGHT;
		public static const OBJECT_MAP:Object = 
		{
			1: Player
		};
			
		public function Game() 
		{
			super(SCREEN_WIDTH, SCREEN_HEIGHT, PlayState, ZOOM);
		}
		
	}

}