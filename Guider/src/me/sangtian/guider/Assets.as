package me.sangtian.guider 
{
	/**
	 * ...
	 * @author Sang Tian
	 */
	public class Assets 
	{
		[Embed(source="/../assets/Tiles.png")]
		public static const TILES:Class;
		
		[Embed(source="/../assets/Objects.png")]
		public static const OBJECTS:Class;
		
		[Embed(source="/../maps/mapCSV_WorldMap_Tiles.csv", mimeType="application/octet-stream")]
		public static const TILE_LAYER_MAP_DATA:Class;
		
	}

}