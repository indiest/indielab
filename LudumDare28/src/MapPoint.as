package  
{
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class MapPoint 
	{
		private var _x:uint;
		private var _y:uint;
		
		public function get x():uint 
		{
			return _x;
		}
		
		public function get y():uint 
		{
			return _y;
		}
		
		public function MapPoint(x:uint, y:uint) 
		{
			_x = x;
			_y = y;
		}
		
	}

}