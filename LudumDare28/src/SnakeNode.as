package  
{
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class SnakeNode extends FlxSprite 
	{
		private var _mapX:int;
		private var _mapY:int;
		private var _prevNode:SnakeNode;
		
		public var snake:Snake;
		
		public function get mapX():int 
		{
			return _mapX;
		}
		
		public function set mapX(value:int):void 
		{
			_mapX = value;
			x = _mapX * Map.GRID_WIDTH_PX;
		}
		
		public function get mapY():int 
		{
			return _mapY;
		}
		
		public function set mapY(value:int):void 
		{
			_mapY = value;
			y = _mapY * Map.GRID_HEIGHT_PX;
		}
		
		public function get isHead():Boolean 
		{
			return prevNode == null;
		}
		
		public function get prevNode():SnakeNode 
		{
			return _prevNode;
		}
		
		public function set prevNode(value:SnakeNode):void 
		{
			_prevNode = value;
			if (value != null)
			{
				alpha = 0.7;
			}
			else
			{
				alpha = 1;
			}
		}
		
		public function SnakeNode(snake:Snake, mapX:int, mapY:int, prevNode:SnakeNode = null) 
		{
			makeGraphic(Map.GRID_WIDTH_PX, Map.GRID_HEIGHT_PX, 0xffffffff);
			this.snake = snake;
			this.mapX = mapX;
			this.mapY = mapY;
			//trace("Create snake node:", mapX, mapY);
			this.prevNode = prevNode;
		}
		
	}

}