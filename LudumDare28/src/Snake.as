package  
{
	import com.clewcat.common.Direction;
	import com.clewcat.common.util.Random;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.FlxTimer;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Snake extends FlxGroup 
	{
		public static const SPEED:Number = 1;
		
		public static var moveInterval:Number = 0.2;

		public var head:SnakeNode;
		public var nodes:Vector.<SnakeNode> = new Vector.<SnakeNode>();
		public var moveDir:uint;
		public var headDir:uint;
		public var growLength:uint;
		
		private var _moveTimer:Number = 0;
		
		public function get isPlayerSnake():Boolean
		{
			return (this == (FlxG.state as PlayState).playerSnake);
		}
		
		public function Snake(mapX:uint, mapY:uint, length:uint, dir:uint) 
		{
			head = new SnakeNode(this, mapX, mapY);
			addNode(head);
			var prevNode:SnakeNode = head;
			for (var i:uint = 1; i < length; i++)
			{
				mapX += Direction.getSignX(dir);
				mapY += Direction.getSignY(dir);
				var node:SnakeNode = new SnakeNode(this,
					mapX, mapY, prevNode);
				addNode(node);
				prevNode = node;
			}
			//this.moveDir = dir;
			if (length > 1)
				headDir = dir;
		}
		
		public function addNode(node:SnakeNode):void
		{
			nodes.push(node);
			add(node);
		}
		
		public function removeNode(node:SnakeNode):void
		{
			nodes.splice(nodes.indexOf(node), 1);
			remove(node);
		}
		
		public function setScale(scale:Number):void
		{
			for each(var node:SnakeNode in nodes)
			{
				node.scale.make(scale, scale);
			}
		}
		
		public function setColor(color:uint):void
		{
			for each(var node:SnakeNode in nodes)
			{
				node.color = color;
			}
		}
		
		public function updateMove():void
		{
			if (moveDir == Direction.NONE)
				return;
			
			for each(var node:SnakeNode in nodes)
			{
				if (node.isHead)
				{
					node.x += SPEED * Direction.getSignX(moveDir);
					node.y += SPEED * Direction.getSignY(moveDir);
				}
				else
				{
					node.x += SPEED * (node.prevNode.mapX - node.mapX);
					node.y += SPEED * (node.prevNode.mapY - node.mapY);
				}
				if (node.x % Map.GRID_WIDTH_PX == 0)
					node.mapX = node.x / Map.GRID_WIDTH_PX;
				if (node.y % Map.GRID_HEIGHT_PX == 0)
					node.mapY = node.y / Map.GRID_HEIGHT_PX;
			}
		}
		
		public function moveStep():void
		{
			if (moveDir == Direction.NONE)
				return;
			
			if (growLength > 0)
			{
				var newHead:SnakeNode = new SnakeNode(this, head.mapX, head.mapY);
				newHead.color = isPlayerSnake ? 0x00cc00 : 0xffffff;
				add(newHead);
				nodes.unshift(newHead);
				head.prevNode = newHead;
				head = newHead;
				growLength--;
			}
			else
			{
				var node:SnakeNode = nodes[nodes.length - 1];
				while (node.prevNode != null)
				{
					node.mapX = node.prevNode.mapX;
					node.mapY = node.prevNode.mapY;
					node = node.prevNode;
				}
			}
			
			// Growing snake has distinct color
			setScale(1 + growLength / 20);
			
			head.mapX += Direction.getSignX(moveDir);
			head.mapY += Direction.getSignY(moveDir);
			headDir = moveDir;
			
			if (head.mapX < 0)
			{
				if (isPlayerSnake)
				{
					head.mapX = Map.MAP_GRID_WIDTH - 1;
				}
				else
				{
					head.mapX = 0;
					moveDir = Random.randomElement(Direction.UP, Direction.DOWN);
				}
			}
			else if (head.mapX >= Map.MAP_GRID_WIDTH)
			{
				if (isPlayerSnake)
				{
					head.mapX = 0;
				}
				else
				{
					head.mapX = Map.MAP_GRID_WIDTH - 1;
					moveDir = Random.randomElement(Direction.UP, Direction.DOWN);
				}
			}
			if (head.mapY < 0)
			{
				if (isPlayerSnake)
				{
					head.mapY = Map.MAP_GRID_HEIGHT - 1;
				}
				else
				{
					head.mapY = 0;
					moveDir = Random.randomElement(Direction.LEFT, Direction.RIGHT);
				}
			}
			else if (head.mapY >= Map.MAP_GRID_HEIGHT)
			{
				if (isPlayerSnake)
				{
					head.mapY = 0;
				}
				else
				{
					head.mapY = Map.MAP_GRID_HEIGHT - 1;
					moveDir = Random.randomElement(Direction.LEFT, Direction.RIGHT);
				}
			}
		}
		
		override public function update():void 
		{
			super.update();
			
			_moveTimer -= FlxG.elapsed;
			if (_moveTimer <= 0)
			{
				moveStep();
				_moveTimer = moveInterval;
			}
			
			//for each(var node:SnakeNode in nodes)
			//{
				//if (node.mapX < 0)
					//node.mapX = Map.MAP_GRID_WIDTH - 1;
				//else if (node.mapX >= Map.MAP_GRID_WIDTH)
					//node.mapX = 0;
				//if (node.mapY < 0)
					//node.mapY = Map.MAP_GRID_HEIGHT - 1;
				//else if (node.mapY >= Map.MAP_GRID_HEIGHT)
					//node.mapY = 0;
			//}
		}
		
		public function checkHeadHit(snake:Snake):SnakeNode
		{
			var node:SnakeNode;
			for each(node in snake.nodes)
			{
				if (node == head)
					continue;
				if (node.mapX == head.mapX && node.mapY == head.mapY)
					return node;
			}
			return null;
		}
		
		public function combineTo(snake:Snake, linkNode:SnakeNode):void 
		{
			var node:SnakeNode = snake.nodes[snake.nodes.length - 1];
			while (node != linkNode)
			{
				snake.removeNode(node);
				node = node.prevNode;
			}
			
			head.prevNode = linkNode;
			for each(node in this.nodes)
			{
				snake.addNode(node);
			}
		}
		
		public function eat(snake:Snake):void 
		{
			growLength = snake.length;
		}
		
		private var _destroyCallback:Function;
		public function startDestroy(finishCallback:Function = null):void 
		{
			active = false;
			_destroyCallback = finishCallback;
			new FlxTimer().start(0.1, 10, updateDestroy);
		}
		
		private function updateDestroy(timer:FlxTimer):void 
		{
			for each(var node:SnakeNode in nodes)
			{
				node.alpha = timer.loopsLeft / timer.loops;
			}
			if (node.alpha <= 0)
			{
				kill();
				if (_destroyCallback != null)
					_destroyCallback();
			}
		}
	}

}