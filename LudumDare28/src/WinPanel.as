package  
{
	import org.flixel.FlxButton;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.FlxText;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class WinPanel extends FlxGroup 
	{
		public var bg:FlxSprite = new FlxSprite(60, 40);
		public var textTitle:FlxText = new FlxText(140, 50, 160, "You Get One Snake!");
		public var textScore:FlxText = new FlxText(100, 80, 80);
		public var textCoins:FlxText = new FlxText(180, 80, 80);
		public var buttonShorter:FlxButtonPlus = new FlxButtonPlus(100, 100, null, null, "", 120, 15);
		public var buttonSlower:FlxButtonPlus = new FlxButtonPlus(100, 120, null, null, "", 120, 15);
		public var buttonLife:FlxButtonPlus = new FlxButtonPlus(100, 140, null, null, "", 120, 15);
		public var buttonContinue:FlxButtonPlus = new FlxButtonPlus(100, 160, null, null, "Continue", 120, 15);
		
		public function WinPanel() 
		{
			bg.makeGraphic(200, 140, 0xff006600);
			add(bg);
			textTitle.scale.make(2, 2);
			add(textTitle);
			add(textScore);
			add(textCoins);
			add(buttonShorter);
			add(buttonSlower);
			add(buttonLife);
			add(buttonContinue);
		}
		
		override public function update():void 
		{
			super.update();
			if (FlxG.keys.SPACE)
			{
				(FlxG.state as PlayState).onClickContinue();
			}
		}
	}

}