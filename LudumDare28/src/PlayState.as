package  
{
	import com.clewcat.common.CSVParser;
	import com.clewcat.common.Direction;
	import com.clewcat.common.util.Random;
	import org.flixel.FlxCamera;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import org.flixel.system.FlxTile;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class PlayState extends FlxState 
	{
		public static var levelSettingData:CSVParser = new CSVParser(
			new Assets.LEVEL_SETTINGS(),
			{level: uint, initLength:uint, moveInterval:Number, snakes: uint, minSnakeLength: uint, maxSnakeLength: uint },
			"level");
		public static var priceData:CSVParser = new CSVParser(
			new Assets.PRICES(),
			{times: uint, life: uint, shorter: uint, slower: uint },
			"times");
		
		public var map:Map = new Map();
		//public var mapCamera:FlxCamera = new FlxCamera(0, 0, Map.MAP_WIDTH_PX, Map.MAP_HEIGHT_PX);

		public var playerSnake:Snake;
		public var snakes:Vector.<Snake> = new Vector.<Snake>();
		public var level:uint;
		public var lives:uint;
		public var score:uint;
		public var coins:uint;
		public var times:uint;
		
		public var speedFactor:Number;
		public var lengthDecrement:int;
		
		public var ui:FlxGroup = new FlxGroup();
		private var _textLevel:FlxText;
		private var _textLives:FlxText;
		private var _textScore:FlxText;
		private var _textCoins:FlxText;
		private var _winPanel:WinPanel;
		private var _losePanel:LosePanel;
		private var _paused:Boolean = false;
		
		public function get paused():Boolean 
		{
			return _paused;
		}
		
		public function set paused(value:Boolean):void 
		{
			_paused = value;
			map.active = !value;
		}
		
		public function PlayState() 
		{
			
		}
		
		override public function create():void 
		{
			//mapCamera.y = 48;
			//map.cameras = [mapCamera];
			add(map);
			
			_textLevel = new FlxText(0, Map.MAP_HEIGHT_PX, 80);
			ui.add(_textLevel);
			_textLives = new FlxText(_textLevel.x + _textLevel.width, Map.MAP_HEIGHT_PX, 80);
			ui.add(_textLives);
			_textScore = new FlxText(_textLives.x + _textLives.width, Map.MAP_HEIGHT_PX, 80);
			ui.add(_textScore);
			_textCoins = new FlxText(_textScore.x + _textScore.width, Map.MAP_HEIGHT_PX, 80);
			ui.add(_textCoins);
			add(ui);
			
			_winPanel = new WinPanel();
			_winPanel.buttonContinue.setOnClickCallback(onClickContinue);
			
			_losePanel = new LosePanel();
			_losePanel.buttonRestart.setOnClickCallback(onClickRestart);
			
			level = 1;
			lives = 3;
			score = 0;
			coins = 0;
			times = 0;
			speedFactor = 1;
			lengthDecrement = 0;
			start();
		}
		
		private function onClickShorter(price:uint):void 
		{
			coins -= price;
			lengthDecrement++;
			times++;
			onClickContinue();
			//updateWinPanel();
		}
		
		private function onClickSlower(price:uint):void 
		{
			coins -= price;
			speedFactor += 0.5;
			times++;
			onClickContinue();
			//updateWinPanel();
		}
		
		private function onClickLife(price:uint):void 
		{
			coins -= price;
			lives++;
			times++;
			onClickContinue();
			//updateWinPanel();
		}
		
		private function start():void 
		{
			map.snakes.clear();
			map.loadTerrain(new Assets["LEVEL" + level]);
			snakes.length = 0;
			
			_textLevel.text = "Level " + level;
			_textLives.text = "Lives: " + lives;
			_textScore.text = "Score: " + score;
			_textCoins.text = "Coins: " + coins;

			var levelSetting:Object = levelSettingData.getRowObjectById(level);
			Snake.moveInterval = levelSetting.moveInterval * speedFactor;
			playerSnake = new Snake(Map.MAP_GRID_WIDTH >> 1, Map.MAP_GRID_HEIGHT >> 1,
				1, Direction.getRandomOneDir());
			playerSnake.growLength = levelSetting.initLength - lengthDecrement;
			playerSnake.setColor(0x00cc00);
			playerSnake.moveDir = Direction.getRandomOneDir();
			map.snakes.add(playerSnake);
			snakes.push(playerSnake);
			
			while (snakes.length < levelSetting.snakes + 1)
			{
				var spawnPoint:MapPoint = map.spawnPoints.splice(Random.rangeInt(0, map.spawnPoints.length), 1)[0];
				var snake:Snake = new Snake(spawnPoint.x, spawnPoint.y, //Map.randomX, Map.randomY,
					1, Direction.getRandomOneDir());
				snake.growLength = Random.rangeInt(levelSetting.minSnakeLength, levelSetting.maxSnakeLength + 1);
				//if (FlxG.overlap(snake, map))
					//continue;
				snakes.push(snake);
				map.snakes.add(snake);
			}
			
			speedFactor = 1;
			lengthDecrement = 0;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (paused)
				return;
						
			if (FlxG.keys.W || FlxG.keys.UP)
			{
				if (playerSnake.headDir != Direction.DOWN)
					playerSnake.moveDir = Direction.UP;
			}
			else if (FlxG.keys.S || FlxG.keys.DOWN)
			{
				if (playerSnake.headDir != Direction.UP)
					playerSnake.moveDir = Direction.DOWN;
			}
			else if (FlxG.keys.A || FlxG.keys.LEFT)
			{
				if (playerSnake.headDir != Direction.RIGHT)
					playerSnake.moveDir = Direction.LEFT;
			}
			else if (FlxG.keys.D || FlxG.keys.RIGHT)
			{
				if (playerSnake.headDir != Direction.LEFT)
					playerSnake.moveDir = Direction.RIGHT;
			}
			CONFIG::debug
			{
				if (FlxG.keys.V)
				{
					updateWinPanel();
					add(_winPanel);
				}
			}
			
			//FlxG.overlap(map.snakes, map.terrain, onSnakeCollide);
			//map.terrain.overlayCallback = onSnakeCollide;
			//map.terrain.overlaps(map.snakes);
			updateAI();
			checkHit();
		}
		
		private function onSnakeCollide(node:SnakeNode, collision:FlxObject):void 
		{
			if (node.snake == playerSnake)
			{
			}
			else
			{
				changeMoveDir(node.snake);
			}
		}
		
		private function updateAI():void 
		{
			for each(var snake:Snake in snakes)
			{
				if (snake == playerSnake)
					continue;
				var dx:int = snake.head.mapX - playerSnake.head.mapX;
				var dy:int = snake.head.mapY - playerSnake.head.mapY;
				if (dx * dx + dy * dy <= 4)
				{
					snake.moveDir = Direction.fromVector(dx, dx == 0 ? dy : 0);
				}
				else
				{
					if (Random.randomBoolean(0.01))
					{
						changeMoveDir(snake);
					}
				}
			}
		}
		
		private function changeMoveDir(snake:Snake):void 
		{
			if (snake.moveDir == Direction.NONE)
			{
				snake.moveDir = Direction.getRandomOneDir();
			}
			else if ((snake.headDir & Direction.UP_DOWN) > 0)
			{
				snake.moveDir = Random.randomElement(Direction.NONE, Direction.LEFT, Direction.RIGHT);
			}
			else if ((snake.headDir & Direction.LEFT_RIGHT) > 0)
			{
				snake.moveDir = Random.randomElement(Direction.NONE, Direction.UP, Direction.DOWN);
			}
		}
		
		private function checkHit():void 
		{
			for (var i:uint = 0; i < snakes.length; i++)
			{
				var s1:Snake = snakes[i];
				if (!s1.active)
					continue;
				
				// Check collision to the walls
				if (map.terrain.getTile(s1.head.mapX, s1.head.mapY))
				{
					if (s1 == playerSnake)
					{
						//trace("Hit wall! Lose!");
						die("King Snake smashed into the wall...");
						return;
					}
				}
				else if (s1 != playerSnake)
				{
					// AI is gonna hit wall, steer away
					while (map.terrain.getTile(
						s1.head.mapX + Direction.getSignX(s1.moveDir),
						s1.head.mapY + Direction.getSignY(s1.moveDir))
					)
					{
						changeMoveDir(s1);
					}
				}
				
				// Check collision to player's snake own body
				if (s1 == playerSnake && s1.checkHeadHit(s1))
				{
					//trace("Hit own body! Lose!");
					die("King Snake was so hungry that it ate itself...");
					return;
				}
				
				// Check collision to other snakes
				for (var j:uint = 0; j < snakes.length; j++)
				{
					var s2:Snake = snakes[j];
					if (s1 == s2)
						continue;
					var s2Node:SnakeNode = s1.checkHeadHit(s2);
					//var s1Node:SnakeNode = s2.checkHeadHit(s1);
					if (s2Node != null)
					{
						if (s2 == playerSnake)
						{
							//trace("Hit by snake! Lose!");
							die("King Snake was eaten by its beloved subject...");
							return;
						}
						else if (s1 == playerSnake)
						{
							if (s2Node.isHead)
							{
								//trace("Hit in the head! Lose!");
								die("King Snake was knocked down in the head...");
								return;
							}
							else
							{
								win(s2);
							}
						}
						else
						{
							//trace("Snake combined.");
							//s1.combineTo(s2, s2Node);
							s1.eat(s2);
							s2.startDestroy();
							//s1.moveDir = Direction.NONE;
							snakes.splice(j, 1);
							j--;
						}
					}
				}
			}
		}
		
		private function updateWinPanel():void
		{
			if (times >= priceData.rows.length)
				times = priceData.rows.length - 1;
			var price:Object = priceData.getRowObjectById(times);
			_winPanel.buttonShorter.text = "Length - 1 (" + price.shorter + " Coins)";
			_winPanel.buttonShorter.active = coins >= price.shorter;
			_winPanel.buttonSlower.text = "Speed - 50% (" + price.slower + " Coins)";
			_winPanel.buttonSlower.active = coins >= price.slower;
			_winPanel.buttonLife.text = "Life + 1 (" + price.life + " Coins)";
			_winPanel.buttonLife.active = coins >= price.life;
			
			_winPanel.buttonShorter.setOnClickCallback(onClickShorter, [price.shorter]);
			_winPanel.buttonSlower.setOnClickCallback(onClickSlower, [price.slower]);
			_winPanel.buttonLife.setOnClickCallback(onClickLife, [price.life]);
		}
		
		public function onClickContinue():void
		{
			remove(_winPanel);
			ui.visible = true;
			paused = false;
			if (level == levelSettingData.rows.length)
			{
				level = 1;
			}
			else
			{
				level++;
			}
			start();
		}
		
		private function win(snake:Snake):void
		{
			var s:uint = snake.length * 2;
			//trace("Win! Score:", s);
			_winPanel.textScore.text = "Score: " + score + "+" + s;
			_winPanel.textCoins.text = "Coins: " + coins + "+" + s;
			
			this.score += s;
			this.coins += s;
			paused = true;
			updateWinPanel();
			snake.startDestroy(showWinPanel);
		}
		
		private function showWinPanel():void 
		{
			add(_winPanel);
			ui.visible = false;
		}
		
		private function updateLosePanel(reason:String):void 
		{
			_losePanel.textTitle.text = (lives == 0) ? "Game Over" : "You Died!";
			_losePanel.textReason.text = reason;
			_losePanel.textLives.text = lives > 0 ? "Lives Left: " + lives : "Final Score: " + score;
		}
		
		public function onClickRestart():void
		{
			remove(_losePanel);
			ui.visible = true;
			paused = false;
			if (lives == 0)
				create();
			else
				start();
		}
		
		private function die(reason:String = ""):void 
		{
			lives--;
			updateLosePanel(reason);
			paused = true;
			playerSnake.startDestroy(showLosePanel);
		}
		
		private function showLosePanel():void 
		{
			add(_losePanel);
			ui.visible = false;
		}
	}

}