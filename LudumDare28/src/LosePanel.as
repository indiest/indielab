package  
{
	import org.flixel.FlxButton;
	import org.flixel.FlxG;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.FlxText;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class LosePanel extends FlxGroup 
	{
		public var bg:FlxSprite = new FlxSprite(60, 40);
		public var textTitle:FlxText = new FlxText(160, 50, 100);
		public var textReason:FlxText = new FlxText(80, 80, 160);
		public var textLives:FlxText = new FlxText(130, 100, 80);
		public var buttonRestart:FlxButtonPlus = new FlxButtonPlus(100, 120, null, null, "Restart", 120, 15);
		
		public function LosePanel() 
		{
			bg.makeGraphic(200, 120, 0xff660000);
			add(bg);
			textTitle.scale.make(2, 2);
			add(textTitle);
			add(textReason);
			add(textLives);
			add(buttonRestart);
		}
		
		override public function update():void 
		{
			super.update();
			if (FlxG.keys.SPACE)
			{
				(FlxG.state as PlayState).onClickRestart();
			}
		}
	}

}