package com.clewcat.common.util 
{
	/**
	 * ...
	 * @author tiansang
	 */
	public class Random 
	{
		[Inline]
		public static function get value():Number
		{
			return Math.random();
		}

		//[Inline]
		public static function get angle():Number
		{
			return rangeNumber( -180, 180);
		}
		
		//[Inline]
		public static function get radian():Number
		{
			return rangeNumber( -Math.PI, Math.PI);
		}
		
		//[Inline]
		public static function get sign():int
		{
			return value > 0.5 ? 1 : -1;
		}
		
		//[Inline]
		/**
		 * Generate integer in [min, max)
		 */
		public static function rangeInt(min:int, max:int):int
		{
			return min + value * (max - min);
		}
		
		//[Inline]
		public static function rangeNumber(min:Number, max:Number):Number
		{
			return min + value * (max - min);
		}
		
		//[Inline]
		public static function randomBoolean(chance:Number = 0.5):Boolean 
		{
			return chance >= value;
		}
		
		//[Inline]
		public static function randomElement(...items):*
		{
			var index:int = value * items.length;
			return items[index];
		}
		
		//[Inline]
		public static function randomVectorElement(v:Object):*
		{
			var index:int = value * v.length;
			return v[index];
		}
		
		//[Inline]
		public static function draw(probabilities:Vector.<Number>):int
		{
			var v:Number = value;
			var p0:Number = 0;
			var p1:Number = 0;
			for (var i:int = 0; i < probabilities.length; i++)
			{
				p1 += probabilities[i];
				if (v > p0 && v <= p1)
					return i;
				p0 = p1;
			}
			return -1;
		}
	}

}