package com.clewcat.common 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public final class Direction 
	{
		public static const NONE:uint = 0;
		public static const UP:uint = 1 << 0;
		public static const DOWN:uint = 1 << 1;
		public static const LEFT:uint = 1 << 2;
		public static const RIGHT:uint = 1 << 3;
		public static const LEFT_RIGHT:uint = LEFT | RIGHT;
		public static const UP_DOWN:uint = UP | DOWN;
		public static const ALL:uint = UP_DOWN | LEFT_RIGHT;
		
		public static function fromVector(x:Number, y:Number):uint 
		{
			var dir:uint = 0;
			if (x < 0)
				dir |= LEFT;
			else if (x > 0)
				dir |= RIGHT;
			if (y < 0)
				dir |= UP;
			else if (y > 0)
				dir |= DOWN;
			return dir;
		}
		
		public static function getRandomOneDir():uint
		{
			return 1 << int(Math.random() * 4);
		}
		
		public static function getSignX(dir:uint):int
		{
			if ((dir & LEFT) > 0)
				return -1;
			else if ((dir & RIGHT) > 0)
				return 1;
			else
				return 0;
		}
		
		public static function getSignY(dir:uint):int
		{
			if ((dir & UP) > 0)
				return -1;
			else if ((dir & DOWN) > 0)
				return 1;
			else
				return 0;
		}
		
		public static function getAngle(dir:uint):Number
		{
			switch (dir)
			{
				case DOWN:
					return Math.PI;
				case LEFT:
					return Math.PI * 1.5;
				case RIGHT:
					return Math.PI * 0.5;
				default:
					return 0;
			}
		}
		
	}

}