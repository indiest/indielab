package com.clewcat.common 
{
	import flash.utils.Dictionary;
	/**
	 * Super light weight CSV loader and data container, with following features:
	 * 1. Converts cell data to specified type at loading time.
	 * 2. Stores and fast lookup row data by both column index and field name.
	 * 3. Stores and fast lookup row data by identical value if Id field(primary key) is indicated.
	 * 4. Row(s) matching by simply indicates keys and values
	 * 5. Sorting by file name.
	 * 
	 * @author Nicolas Tian
	 */
	public class CSVParser 
	{
		private static const CSV_LINE_SPLIT_REGEX:RegExp = /,(?!(?:[^",]|[^"],[^"])+")/;
		
		private var _headers:Vector.<String>;
		private var _rows:Vector.<Object> = new Vector.<Object>();
		private var _rowsById:Dictionary = new Dictionary();
		
		public function get headers():Vector.<String> 
		{
			return _headers;
		}
		
		public function get rows():Vector.<Object> 
		{
			return _rows;
		}
		
		public function CSVParser(content:String, typeConverters:Object = null, idField:String = null) 
		{
			var rows:Array = content.split(/\r?\n/g);
			if (rows.length == 0)
				return;
			_headers = Vector.<String>(rows[0].split(CSV_LINE_SPLIT_REGEX));
			for (var y:uint = 1; y < rows.length; y++)
			{
				var row:String = rows[y];
				if (row.length == 0)
					continue;
				var cells:Array = row.split(CSV_LINE_SPLIT_REGEX);
				var rowObject:Object = { };
				for (var x:uint = 0; x < _headers.length; x++)
				{
					var header:String = _headers[x];
					var cell:* = cells[x];
					// convert type
					if (typeConverters && typeConverters[header])
						cell = typeConverters[header](cell);
					// store row to dictionary
					if (header == idField)
						_rowsById[cell] = rowObject;
					rowObject[header] = cell;
					// store by column index as well
					rowObject[x] = cell;
				}
				_rows.push(rowObject);
			}
		}
		
		public function getValue(rowIndex:uint, colNameOrIndex:*):*
		{
			return getRowObject(rowIndex)[colNameOrIndex];
		}
		
		public function getRowValues(rowIndex:uint):Vector.<*>
		{
			var values:Vector.<*> = new Vector.<*>();
			var rowObject:Object = getRowObject(rowIndex);
			for each(var value:* in rowObject)
				values.push(value);
			return values;
		}
		
		public function getRowObject(rowIndex:uint):Object
		{
			return _rows[rowIndex];
		}
		
		public function getRowObjectById(id:*):Object
		{
			return _rowsById[id];
		}
		
		public function find(matcher:Object):Vector.<Object>
		{
			var result:Vector.<Object> = new Vector.<Object>();
			a: for each(var row:Object in rows)
			{
				for (var key:String in matcher)
				{
					if (!match(key, row[key], matcher[key]))
						continue a;
				}
				result.push(row);
			}
			return result;
		}
		
		[Inline]
		private function match(key:*, value:*, comparer:*):Boolean
		{
			if (comparer is Function)
			{
				return (comparer as Function).call(null, value);
			}
			else
			{
				return value == comparer;
			}
		}
		
		public function findOne(matcher:Object):Object
		{
			a: for each(var row:Object in rows)
			{
				for (var key:String in matcher)
				{
					if (!match(key, row[key], matcher[key]))
						continue a;
				}
				return row;
			}
			return null;
		}
		
		public function sort(field:String):Vector.<Object>
		{
			return rows.sort(function(row1:Object, row2:Object):Number
			{
				var value1:* = row1[field];
				var value2:* = row2[field];
				if (value1 > value2)
					return 1;
				else if (value1 < value2)
					return -1;
				else
					return 0;
			});
		}
		
		public static function parse(content:String, typeConverter:Object = null, idField:String = null):Vector.<Object>
		{
			return new CSVParser(content, typeConverter, idField).rows;
		}
		
		[Inline]
		public static function comparer_notEqual(num:Number):Function
		{
			return function(value:Number):Boolean
			{
				return value != num;
			};
		}
		
		[Inline]
		public static function comparer_greaterThan(num:Number):Function
		{
			return function(value:Number):Boolean
			{
				return value > num;
			};
		}
		
		[Inline]
		public static function comparer_lessThan(num:Number):Function
		{
			return function(value:Number):Boolean
			{
				return value < num;
			};
		}
		
		[Inline]
		public static function comparer_greaterThanOrEqual(num:Number):Function
		{
			return function(value:Number):Boolean
			{
				return value >= num;
			};
		}
		
		[Inline]
		public static function comparer_lessThanOrEqual(num:Number):Function
		{
			return function(value:Number):Boolean
			{
				return value <= num;
			};
		}
		
		[Inline]
		public static function comparer_range(min:Number, max:Number):Function
		{
			return function(value:Number):Boolean
			{
				return value >= min && value <= max;
			};
		}
		
		[Inline]
		public static function comparer_isIn(arr:Array):Function
		{
			return function(value:*):Boolean
			{
				if (arr == null)	return false;
				return arr.indexOf(value) >= 0;
			}
		}
		
		[Inline]
		public static function comparer_isNotIn(arr:Array):Function
		{
			return function(value:*):Boolean
			{
				if (arr == null)	return true;
				return arr.indexOf(value) < 0;
			}
		}
	}

}