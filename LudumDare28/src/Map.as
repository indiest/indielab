package  
{
	import com.clewcat.common.util.Random;
	import org.flixel.FlxGroup;
	import org.flixel.FlxTilemap;
	/**
	 * ...
	 * @author Nicolas Tian
	 */
	public class Map extends FlxGroup
	{
		public static const MAP_GRID_WIDTH:uint = 40;
		public static const MAP_GRID_HEIGHT:uint = 28;
		public static const MAP_GRID_COUNT:uint = MAP_GRID_WIDTH * MAP_GRID_WIDTH;
		public static const GRID_WIDTH_PX:uint = 8;
		public static const GRID_HEIGHT_PX:uint = 8;
		public static const MAP_WIDTH_PX:uint = MAP_GRID_WIDTH * GRID_WIDTH_PX;
		public static const MAP_HEIGHT_PX:uint = MAP_GRID_HEIGHT * GRID_HEIGHT_PX;
		
		public static function getGridIndex(mapX:uint, mapY:uint):uint
		{
			return mapX + mapY * MAP_GRID_WIDTH;
		}
		
		private static const _mapPoints:Vector.<MapPoint> = new Vector.<MapPoint>(MAP_GRID_COUNT, true);
		public static function getPoint(mapX:uint, mapY:uint):MapPoint
		{
			var index:uint = getGridIndex(mapX, mapY);
			var p:MapPoint = _mapPoints[index];
			if (p == null)
			{
				p = new MapPoint(mapX, mapY);
				_mapPoints[index] = p;
			}
			return p;
		}
		
		public static function get randomX():int
		{
			return Random.rangeInt(0, MAP_GRID_WIDTH);
		}
		
		public static function get randomY():int
		{
			return Random.rangeInt(0, MAP_GRID_HEIGHT);
		}
		
		public var terrain:FlxTilemap = new FlxTilemap();
		public var spawnPoints:Vector.<MapPoint> = new Vector.<MapPoint>();
		public var snakes:FlxGroup = new FlxGroup();
		
		public function Map() 
		{
			add(terrain);
			add(snakes);
		}
		
		public function reset():void
		{
			snakes.clear();
		}
		
		public function loadTerrain(data:String):void
		{
			var xml:XML = new XML(data);
			var mapData:String = xml.layer[0].data[0];
			terrain.loadMap(mapData, Assets.TERRAIN, GRID_WIDTH_PX, GRID_HEIGHT_PX);
			spawnPoints.length = 0;
			for each(var obj:XML in xml.objectgroup[0].object)
			{
				spawnPoints.push(getPoint(
					int(obj.@x) / GRID_WIDTH_PX,
					int(obj.@y) / GRID_HEIGHT_PX)
				);
			}
		}
	}

}