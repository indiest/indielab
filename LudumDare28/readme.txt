SnakEaten by IndieST
========

You only get one snake eaten!

You are the King of Snake in the forest, but you don't get a king stomach - once you eat a snake, no matter how small it is, you'll be full and hibernate until next hunting season.

Rules:
	1. Control your snake to eat other snake. Don't hit the wall or other snake's head, or you'll lose one life.
	2. You can only eat one snake in a level. The longer the snake is, more scores and coins you get.
	3. Other snakes can get longer by eating each other, as well. So be patient and use your tactic to maximize scores.
	4. Coins can be used to buy items when you finish each level.
	
Controls:
	WASD/Arrow Keys to Move